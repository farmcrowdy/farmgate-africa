$(document).ready(function(){


//========================= buy page .... checkboxes==============================

// $("#checkbox1").change(function(){  //"select all" change 
//     var status = this.checked; // "select all" checked status
//     $('input[name = northEast]').each(function(){ //iterate all listed checkbox items
//         this.checked = status; //change ".checkbox" checked status
//     });
// });

// $('input[name = northEast]').change(function(){ //".checkbox" change 
//     //uncheck "select all", if one of the listed checkbox item is unchecked
//     if(this.checked == false){ //if this item is unchecked
//         $("#checkbox1")[0].checked = false; //change "select all" checked status to false
//     }
    
//     //check "select all" if all checkbox items are checked
//     if ($('input[name = northEast]:checked').length == $('input[name = northEast]').length ){ 
//         $("#checkbox1")[0].checked = true; //change "select all" checked status to true
//     }
// });

// =============sidebar options
// display menu
$(".filter-link").click(function(){
    $("#sidemenuToggle").removeClass("d-none");
});
// hide menu
$(".hide-sidebar").click(function(){
    $("#sidemenuToggle").addClass("d-none");
    console.log("worked      jhskznjbhg");
})

// sidemenu for dashboard
// display menu
$(".dashboard-close").click(function(){
    $("#sidemenuToggle").removeClass("d-none");
});
// hide menu
$(".hide-sidebar").click(function(){
    $("#sidemenuToggle").addClass("d-none");
    console.log("worked      jhskznjbhg");
})
// -------------------------------------------

// ===========sidemenu accordion================




// ========== Change Password Form ============
$('form#changePasswordForm').submit(function(e) {

    var form = $(this);

    e.preventDefault();

    $.ajax({
        type: "POST",
        url: "/users/change-pass",
        data: form.serialize(), // <--- THIS IS THE CHANGE
        // dataType: "json",
        success: function(data){
            JSON.parse(data);
            $('#cp_feedback').fadeOut().empty();
            $('#cp_feedback').fadeIn(400).append('<p><b>'+data+'</b></p>');
        },
        error: function() { alert("Error communicating with our servers."); }
   });

});
$('#changePasswordModal').on('hidden.bs.modal', function (e) {
    $(this).find("input[type=password], textarea").val("");
    $('#cp_feedback').fadeOut().empty();
})

// ========== Contact Manager Form ============

$('form#contactManagerForm').submit(function(e) {

    var form = $(this);

    e.preventDefault();
    var formData = new FormData(this);

    $.ajax({
        type: "POST",
        url: "/users/contact-mgr",
        // data: form.serialize(), // <--- THIS IS THE CHANGE
        // dataType: "json",
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(data){
            JSON.parse(data);
            $('#cm_feedback').fadeOut(200).empty();
            $('#cm_feedback').fadeIn(400).append('<p><b>'+data+'</b></p>');
            form[0].reset();
        },
        error: function() { alert("Error communicating with our servers."); }
   });

});
$('.contact-manager-modal').on('hidden.bs.modal', function (e) {
    $(this).find("input[type=text], textarea").val("");
    reset($('#upload-file-for-manager'));
    $('#cm_feedback').fadeOut().empty();
    $('#profile-file-paceholder').fadeOut().empty();

})


// display chat list
$(".more-chats").click(function(){
    $("#chats").removeClass("d-none");
});
// hide menu
$(".hide-chats").click(function(){
    $("#chats").addClass("d-none");
    console.log("worked      jhskznjbhg");
})

// 
$('.menu-icon').click(function(e){
    $('#navlist').toggleClass('d-none');
});

});

// Reset Form elements
window.reset = function (e) {
    e.wrap('<form>').closest('form').get(0).reset();
    e.unwrap();
}
