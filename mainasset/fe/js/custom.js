// Limit text display on blog post
$(".details-container p .paragraph").text(function(index, currentText) {
    var maxLength = $(this).parent().attr('data-maxlength');
    if(currentText.length >= maxLength) {
    //   return currentText.substr(0, maxLength) + "...";
      return currentText.substr(0, maxLength);
    } else {
      return currentText
    } 
  });


$(".marketplace-slide").click(function(){
    $("#slide-out").removeClass("d-hide-filter").addClass("slideInLeft ");
    $(".body").addClass("overlay");
    setTimeout(function() {
        $("#slide-out").removeClass("slideInLeft slideInRight");
    }, 600);
});

// hide menu
$(".hide-marketplace").click(function(){
    $("#slide-out").addClass(" slideInRight");
    $(".body").removeClass("overlay");
    // console.log("worked      jhskznjbhg");
  });
 
// show dashboard
$(".dashboard-close").click(function(){
    $(".aside-mobile").removeClass("d-none").addClass("slideInLeft ");
    $(".body").addClass("overlay");
    setTimeout(function() {
        $(".aside-mobile").removeClass("slideInLeft slideInRight");
    }, 600);
});
// hide dashboard
$(".close-btn").click(function(){
    $(".aside-mobile").addClass(" slideInRight");
    $(".body").removeClass("overlay");
    console.log("worked      jhskznjbhg");
  });

// ========== Change Password Form ============
$('form#changePasswordForm').submit(function(e) {

    var form = $(this);

    e.preventDefault();

    $.ajax({
        type: "POST",
        url: "/users/change-pass",
        data: form.serialize(), // <--- THIS IS THE CHANGE
        // dataType: "json",
        success: function(data){
            console.log(data);
            JSON.parse(data);
            $('#cp_feedback').fadeOut().empty();
            $('#cp_feedback').fadeIn(400).append('<p><b>'+data+'</b></p>');
        },
        error: function() { alert("Error communicating with our servers."); }
   });

});
$('#changePasswordModal').on('hidden.bs.modal', function (e) {
    $(this).find("input[type=password], textarea").val("");
    $('#cp_feedback').fadeOut().empty();
})

// ========== Contact Manager Form ============

$('form#contactManagerForm').submit(function(e) {

    var form = $(this);

    e.preventDefault();
    var formData = new FormData(this);

    $.ajax({
        type: "POST",
        url: "/users/contact-mgr",
        // data: form.serialize(), // <--- THIS IS THE CHANGE
        // dataType: "json",
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(data){
            // console.log(data);
            var data = JSON.parse(data);
            $('#cm_feedback').fadeOut(200).empty();
            $('#cm_feedback').fadeIn(400).append('<p><b>'+data.message+'</b></p>');
            if(data.status === 'success'){
                form[0].reset();
            }
        },
        error: function() { alert("Error communicating with our servers."); }
   });

});
$('.contact-manager-modal').on('hidden.bs.modal', function (e) {
    $(this).find("input[type=text], textarea").val("");
    reset($('#upload-file-for-manager'));
    $('#cm_feedback').fadeOut().empty();
    $('#profile-file-paceholder').fadeOut().empty();

});

// $('.form_test').keyup(function(event){
//     console.log($(this).val());
// });

// Reset Form elements
window.reset = function (e) {
    e.wrap('<form>').closest('form').get(0).reset();
    e.unwrap();
}

// Contact Form Validation
$(document).ready(function() {
 
  $('#contact_form').submit(function(e) {
    e.preventDefault();
    var valid = true;

    var first_name = $('#contact_firstName').val();
    var last_name = $('#contact_lastName').val();
    var email = $('#contact_email').val().trim();
    var phone = $('#contact_phone').val();
    var company = $('#contact_company').val();
    var message = $('#contact_message').val();

    console.log({
        first_name,
        last_name,
        email,
        phone,
        company,
        message
    })

    var feedback_box = $('#contact_feedback');
    feedback_box.empty();
 
    if (first_name.length < 1) {
      $('#contact_firstName_error').text('This field is required').show();
      valid = false;
    }else{
        $('#contact_firstName_error').empty().hide();
    }
    if (last_name.length < 1) {
        $('#contact_lastName_error').text('This field is required').show();
        valid = false;
    }else{
        $('#contact_lastName_error').empty().hide();
    }

    if (email.length < 1) {
        $('#contact_email_error').text('This field is required!').show(); 
    } else if(email.length > 1) {
      if (!validateEmail(email)) {
        $('#contact_email_error').text('Enter a valid email!').show();
        valid = false;
      }
    }else{
        $('#contact_email_error').empty().hide();
    }

    if (phone.length < 1) {
        $('#contact_phone_error').text('This field is required!').show();
        valid = false;

    }else if (phone.length < 8) {
        $('#contact_phone_error').text('Must be at least 8 characters long!').show();
        valid = false;
    }else{
        $('#contact_phone_error').empty().hide();
    }
    if (company.length < 1) {
        $('#contact_company_error').text('This field is required!').show(); 
        valid = false;
    }else{
        $('#contact_company_error').empty().hide();
    }
    if(message.length < 1){
        $('#contact_message_error').text('This field is required!').show();
        valid = false;
    }
    else if (message.length < 15) {
        $('#contact_message_error').text('Must be at least 15 characters long!').show();
        valid = false;
    }else{
        $('#contact_message_error').empty().hide();
    }

    if (valid) this.submit();
  });
 
});

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;   
    return re.test(email);
}