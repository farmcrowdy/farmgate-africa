$(document).ready(function(){

    var statusButton = '';
    // ========== Update Order Form ============

$('form#updateOrderForm').submit(function(e) {

    var form = $(this);

    e.preventDefault();
    var formData = new FormData(this);

    $.ajax({
        type: "POST",
        url: "/admin/updateorder",
        data: form.serialize(), // <--- THIS IS THE CHANGE
        success: function(data){
            console.log(data);
            var data = JSON.parse(data);
            console.log(data);
            $('#ordstatus_feedback').fadeOut(200).empty();
            $('#ordstatus_feedback').fadeIn(400).append('<p><b>'+data.message+'</b></p>');
            if(data.value !== 0){
                statusButton.parent().prev().find('.or_status').text(data.value);
                statusButton.parent().siblings().find('.or_amount').text(data.amount);
            }

        },
        error: function() { alert("Error communicating with our servers."); }
   });

});
$('.order-status-modal').on('hidden.bs.modal', function (e) {
    $(this).find("input[type=text], textarea").val("");

})

$('.order-status-modal').on('show.bs.modal', function (e) {
    $(this).find("input[type=text], textarea").val("");

    statusButton = $(e.relatedTarget); // Button that triggered the modal
    var order_number = statusButton.attr('data-order-num');
    var order_status = statusButton.attr('data-order-status');
    var order_type =  statusButton.attr('data-order-type');
    var order_total = statusButton.attr('data-order-total');
    $('#order_number').val(order_number);
    $('#current_status').text(order_status);
    if(order_type === "lpo"){ 
        $('#lpo_order').show(); 
    }else{
        $('#lpo_order').hide();
    }
    $('#ordstatus_feedback').text('Order number: ' + order_number);
    $('#order_total').val(order_total);

})

$('.del-order').click(function(e){

    var response = confirm("Do you want to delete this order ?");
    if(!response){
        return false;
    }

});

$('.del-cluster').click(function(e){

    var response = confirm("Do you want to delete this cluster ?");
    if(!response){
        return false;
    }

});

$('.del-cc').click(function(e){

    var response = confirm("Do you want to delete this collection center ?");
    if(!response){
        return false;
    }

});

$('.del-wh').click(function(e){

    var response = confirm("Do you want to delete this warehouse ?");
    if(!response){
        return false;
    }

});

$('.del-farmer').click(function(e){

    var response = confirm("Do you want to delete this farmer ?");
    if(!response){
        return false;
    }

});

$('.del-user').click(function(e){
    
    var response = confirm("Do you want to delete this user ?");
    if(!response){
        return false;
    }

});

$('.del-bank').click(function(e){
    
    var response = confirm("Do you want to delete this bank ?");
    if(!response){
        return false;
    }

});

$('.del-purchase').click(function(e){
    
    var response = confirm("Do you want to delete this purchase ?");
    if(!response){
        return false;
    }

});

$('.del-pt').click(function(e){
    
    var response = confirm("Do you want to delete this produce transfer ?");
    if(!response){
        return false;
    }

});

$('.del-oo').click(function(e){
    
    var response = confirm("Do you want to delete this outgoing order ?");
    if(!response){
        return false;
    }

});

$('.del-inv').click(function(e){
    
    var response = confirm("Do you want to delete this investment ?");
    if(!response){
        return false;
    }

});

$('.del-comm').click(function(e){
    
    var response = confirm("Do you want to delete this commodity ?");
    if(!response){
        return false;
    }

});

$('.del-cusp').click(function(e){
    
    var response = confirm("Do you want to delete this customer purchase ?");
    if(!response){
        return false;
    }

});

});
