<?php
defined('BASEPATH') or exit('No direct script access allowed');

// ================ //
//    User Routes   //
// ================ //

$route['users/dashboard'] = 'Users/dashboard';

// == Products
$route['users/products'] = 'Users/products';
$route['users/products/(:num)'] = 'Users/products';
$route['users/products/(:num)/(:any)'] = 'Users/products';
$route['users/addproduct'] = 'Users/addProduct';
$route['users/editproduct/(:any)'] = 'Users/editProduct';
$route['users/delproduct/(:any)'] = 'Users/deleteProduct';

// == Orders
$route['users/orders'] = 'Users/orders';
$route['users/orders/(:num)'] = 'Users/orders';

// == Messages and MessageChat
$route['users/messages'] = 'Users/messages';
$route['users/messagechat/(:any)'] = 'Users/messageChat';
$route['users/postmessage'] = 'Users/postMessage'; //ajax
$route['users/loadmessages'] = 'Users/loadMessages'; //ajax
$route['users/newmessages'] = 'Users/newMessage'; //ajax

// == Profile
$route['users/profile'] = 'Users/profile';
$route['users/editprofile'] = 'Users/editprofile';

// == Purchases
$route['users/purchases'] = 'Users/Purchases';
$route['users/purchases/(:num)'] = 'Users/Purchases';
$route['users/purchases/(:num)/(:any)'] = 'Users/Purchases';

// == Investments
$route['users/investments'] = 'Users/Investments';
$route['users/investments/(:num)'] = 'Users/Investments';
$route['users/investments/(:num)/(:any)'] = 'Users/Investments';


// == Sample Request
$route['sample-request/(:any)'] = 'Users/sampleRequest';

// == LPO Form
$route['users/lpo'] = 'Users/lpo';

// == Change Password and Contact Manager
$route['users/change-pass'] = 'Users/changePassword';
$route['users/contact-mgr'] = 'Users/contactManager';

// == ForgotPassword and RecoverPassword
$route['users/forgotpassword'] = 'Users/forgotPassword';
$route['users/resetpassword/(:any)'] = 'Users/resetPassword';

// == Activation link
$route['activate/(:any)'] = 'Users/activate';

// == Set User Intent
$route['users/setIntent'] = 'Users/setIntent';

// LGAs route
$route['users/locals'] = 'Users/getLocals';

// Test Emails
$route['users/testmail'] = 'Users/testMails';

// == Logout
$route['logout'] = 'Users/logout';
//  ======================================================================================= //

// ================ //
//   Home Routes    //
// ================ //

$route['products'] = 'Home/allproducts';
$route['products/pg'] = 'Home/allproducts';
$route['products/pg/(:num)'] = 'Home/allproducts';
$route['products/pg/(:num)/(:any)'] = 'Home/allproducts';
$route['product/(:any)'] = 'Home/productdetails';

$route['investment-market'] = 'Home/invMarketPlace';
$route['investment-market/pg'] = 'Home/invMarketPlace';
$route['investment-market/pg/(:num)'] = 'Home/invMarketPlace';
$route['investment-market/pg/(:num)/(:any)'] = 'Home/invMarketPlace';
$route['investment-commodity/(:any)'] = 'Home/invCommodityDetails';
$route['investAjax'] = 'Home/investAjax'; // Ajax Request

// $route['marketplace'] = 'Home/MarketPlace';
// $route['marketplace/pg'] = 'Home/MarketPlace';
// $route['marketplace/pg/(:num)'] = 'Home/MarketPlace';
// $route['marketplace/pg/(:num)/(:any)'] = 'Home/MarketPlace';

$route['purchase-market'] = 'Home/purMarketPlace';
$route['purchase-market/pg'] = 'Home/purMarketPlace';
$route['purchase-market/pg/(:num)'] = 'Home/purMarketPlace';
$route['purchase-market/pg/(:num)/(:any)'] = 'Home/purMarketPlace';
$route['purchase-commodity/(:any)'] = 'Home/purCommodityDetails';
$route['searchCommodities'] = 'Home/searchCommodities';
$route['purchaseAjax'] = 'Home/purchaseAjax'; // Ajax Request

$route['about'] = 'Home/aboutUs';
$route['toc'] = 'Home/toc';
$route['privacy'] = 'Home/privacy';
$route['contact'] = 'Home/contactUs';
$route['how-it-works'] = 'Home/howItWorks';
$route['trans-success'] = 'Home/transSuccess';
$route['kenneth'] = 'Home/Kenneth';

$route['inv-commodities'] = 'Home/allInvestmentCommodities';
$route['inv-commodities/pg'] = 'Home/allInvestmentCommodities';
$route['inv-commodities/pg/(:num)'] = 'Home/allInvestmentCommodities';
$route['inv-commodities/pg/(:num)/(:any)'] = 'Home/allInvestmentCommodities';
$route['inv-commodity/(:any)'] = 'Home/investmentCommodityDetails';

$route['pur-commodities'] = 'Home/allPurchaseCommodities';
$route['pur-commodities/pg'] = 'Home/allPurchaseCommodities';
$route['pur-commodities/pg/(:num)'] = 'Home/allPurchaseCommodities';
$route['pur-commodities/pg/(:num)/(:any)'] = 'Home/allPurchaseCommodities';
$route['pur-commodity/(:any)'] = 'Home/purchaseCommodityDetails';

$route['commodities'] = 'Home/allCommodities';
$route['commodities/pg'] = 'Home/allCommodities';
$route['commodities/pg/(:num)'] = 'Home/allCommodities';
$route['commodities/pg/(:num)/(:any)'] = 'Home/allCommodities';
$route['commodity/(:any)'] = 'Home/CommodityDetails';

//  ======================================================================================= //

// ================ //
//   Admin Routes   //
// ================ //

$route['admin/dashboard'] = 'Admin/dashboard';

// == Users
$route['admin/users'] = 'Admin/Users';
$route['admin/users/(:num)'] = 'Admin/Users';
$route['admin/add-user'] = 'Admin/addUser';
$route['admin/user/(:any)'] = 'Admin/userDetail';
$route['admin/user/(:any)/(:num)'] = 'Admin/userDetail';
$route['admin/edituser/(:any)'] = 'Admin/editUser';
$route['admin/deluser/(:any)'] = 'Admin/deleteUser';

// == Clusters
$route['admin/clusters'] = 'Admin/Clusters';
$route['admin/clusters/(:num)'] = 'Admin/Clusters';
$route['admin/add-cluster'] = 'Admin/addCluster';
$route['admin/cluster/(:any)'] = 'Admin/clusterDetail';
$route['admin/editcluster/(:any)'] = 'Admin/editCluster';
$route['admin/delcluster/(:any)'] = 'Admin/deleteCluster';

// == Collection Centers
$route['admin/collection-centers'] = 'Admin/CollectionCenters';
$route['admin/collection-centers/(:num)'] = 'Admin/CollectionCenters';
$route['admin/add-collection-center'] = 'Admin/addCollectionCenter';
$route['admin/collection-center/(:any)'] = 'Admin/collectionCenterDetail';
$route['admin/edit-collection-center/(:any)'] = 'Admin/editCollectionCenter';
$route['admin/del-collection-center/(:any)'] = 'Admin/deleteCollectionCenter';
$route['admin/cc_managers'] = 'Admin/getCollectionCenterManagers'; // Ajax only

// == Warehouses
$route['admin/warehouses'] = 'Admin/Warehouses';
$route['admin/warehouses/(:num)'] = 'Admin/Warehouses';
$route['admin/add-warehouse'] = 'Admin/addWarehouse';
$route['admin/warehouse/(:any)'] = 'Admin/warehouseDetail';
$route['admin/editwarehouse/(:any)'] = 'Admin/editWarehouse';
$route['admin/delwarehouse/(:any)'] = 'Admin/deleteWarehouse';
$route['admin/wh_managers'] = 'Admin/getWarehouseManagers'; // Ajax only

// == Farmers
$route['admin/farmers'] = 'Admin/Farmers';
$route['admin/farmers/(:num)'] = 'Admin/Farmers';
$route['admin/add-farmer'] = 'Admin/addFarmer';
$route['admin/farmer/(:any)'] = 'Admin/farmerDetail';
$route['admin/editfarmer/(:any)'] = 'Admin/editFarmer';
$route['admin/delfarmer/(:any)'] = 'Admin/deleteFarmer';
$route['admin/get_farmers'] = 'Admin/getFarmers'; // Ajax only

// == Investments
$route['admin/investments'] = 'Admin/Investments';
$route['admin/investments/(:num)'] = 'Admin/Investments';
$route['admin/add-investment'] = 'Admin/addInvestment';
$route['admin/investment/(:any)'] = 'Admin/investmentDetail';
$route['admin/edit-investment/(:any)'] = 'Admin/editInvestment';
$route['admin/del-investment/(:any)'] = 'Admin/deleteInvestment';
$route['admin/print_investment/(:any)'] = 'Admin/printInvestment';
$route['admin/pay_investment'] = 'Admin/payInvestment'; // Ajax only
$route['admin/investor_paid'] = 'Admin/investorPaid'; // Ajax only
$route['admin/get_investors'] = 'Admin/getInvestors'; // Ajax only

// == CPurchases
$route['admin/cpurchases'] = 'Admin/CPurchases';
$route['admin/cpurchases/(:num)'] = 'Admin/CPurchases';
$route['admin/add-cpurchase'] = 'Admin/addCPurchase';
$route['admin/cpurchase/(:any)'] = 'Admin/CPurchaseDetail';
$route['admin/edit-cpurchase/(:any)'] = 'Admin/editCPurchase';
$route['admin/del-cpurchase/(:any)'] = 'Admin/deleteCPurchase';
$route['admin/print_cpurchase/(:any)'] = 'Admin/printCPurchase';
$route['admin/buyer_paid'] = 'Admin/buyerPaid'; // Ajax only
$route['admin/purchase_delivery_status'] = 'Admin/purchaseDeliveryStatus'; // Ajax only
$route['admin/getCPurchaseDetails'] = 'Admin/getCPurchaseDetails';// Ajax only


// == Banks
$route['admin/banks'] = 'Admin/Banks';
$route['admin/banks/(:num)'] = 'Admin/Banks';
$route['admin/add-bank'] = 'Admin/addBank';
$route['admin/bank/(:any)'] = 'Admin/bankDetail';
$route['admin/editbank/(:any)'] = 'Admin/editBank';
$route['admin/delbank/(:any)'] = 'Admin/deleteBank';
$route['admin/get_banks'] = 'Admin/getBanks'; // Ajax only

// == Purchases
$route['admin/purchases'] = 'Admin/Purchases';
$route['admin/purchases/(:num)'] = 'Admin/Purchases';
$route['admin/add-purchase'] = 'Admin/addPurchase';
$route['admin/purchase/(:any)'] = 'Admin/purchaseDetail';
$route['admin/editpurchase/(:any)'] = 'Admin/editPurchase';
$route['admin/delpurchase/(:any)'] = 'Admin/deletePurchase';
$route['admin/get_purchases'] = 'Admin/getPurchases'; // Ajax only
$route['admin/produce-categories'] = 'Admin/getProduceCategories'; // Ajax only
$route['admin/pay_purchase'] = 'Admin/payPurchase'; // Ajax only
$route['admin/print_purchase/(:any)'] = 'Admin/printPurchase';

// == Inventory
$route['admin/inventory'] = 'Admin/Inventory';

// == Logistics
$route['admin/logistics'] = 'Admin/Logistics';
// == Produce Transfers
$route['admin/produce-transfers'] = 'Admin/ProduceTransfers';
$route['admin/produce-transfers/(:num)'] = 'Admin/ProduceTransfers';
$route['admin/add-produce-transfer'] = 'Admin/addProduceTransfer';
$route['admin/produce-transfer/(:any)'] = 'Admin/produceTransferDetail';
$route['admin/edit-produce-transfer/(:any)'] = 'Admin/editProduceTransfer';
$route['admin/del-produce-transfer/(:any)'] = 'Admin/deleteProduceTransfer';
// == Outgoing Orders
$route['admin/outgoing-orders'] = 'Admin/OutgoingOrders';
$route['admin/outgoing-orders/(:num)'] = 'Admin/OutgoingOrders';
$route['admin/add-outgoing-order'] = 'Admin/addOutgoingOrder';
$route['admin/outgoing-order/(:any)'] = 'Admin/outgoingOrderDetail';
$route['admin/edit-outgoing-order/(:any)'] = 'Admin/editOutgoingOrder';
$route['admin/del-outgoing-order/(:any)'] = 'Admin/deleteOutgoingOrder';
$route['admin/get_order_details'] = 'Admin/getOrderDetails'; // Ajax only

// == Products
$route['admin/products'] = 'Admin/Products';
$route['admin/products/(:num)'] = 'Admin/Products';
$route['admin/add-product'] = 'Admin/addProduct';
$route['admin/product/(:any)'] = 'Admin/productDetail';
$route['admin/editproduct/(:any)'] = 'Admin/editProduct';
$route['admin/delproduct/(:any)'] = 'Admin/deleteProduct';

// == Investment Commodities
$route['admin/inv-commodities'] = 'Admin/InvestmentCommodities';
$route['admin/inv-commodities/pg'] = 'Admin/InvestmentCommodities';
$route['admin/inv-commodities/pg/(:num)'] = 'Admin/InvestmentCommodities';
$route['admin/inv-commodities/pg/(:num)/(:any)'] = 'Admin/InvestmentCommodities';
$route['admin/inv-commodity/(:any)'] = 'Admin/investmentCommodityDetails';
$route['admin/add-inv-commodity'] = 'Admin/addInvestmentCommodity';
$route['admin/edit-inv-commodity/(:any)'] = 'Admin/editInvestmentCommodity';
$route['admin/del-inv-commodity/(:any)'] = 'Admin/delInvestmentCommodity';

// == Purchase Commodities
$route['admin/pur-commodities'] = 'Admin/PurchaseCommodities';
$route['admin/pur-commodities/pg'] = 'Admin/PurchaseCommodities';
$route['admin/pur-commodities/pg/(:num)'] = 'Admin/PurchaseCommodities';
$route['admin/pur-commodities/pg/(:num)/(:any)'] = 'Admin/PurchaseCommodities';
$route['admin/pur-commodity/(:any)'] = 'Admin/purchaseCommodityDetails';
$route['admin/add-pur-commodity'] = 'Admin/addPurchaseCommodity';
$route['admin/edit-pur-commodity/(:any)'] = 'Admin/editPurchaseCommodity';
$route['admin/del-pur-commodity/(:any)'] = 'Admin/delPurchaseCommodity';

// == Commodities
$route['admin/commodities'] = 'Admin/Commodities';
$route['admin/commodities/pg'] = 'Admin/Commodities';
$route['admin/commodities/pg/(:num)'] = 'Admin/Commodities';
$route['admin/commodities/pg/(:num)/(:any)'] = 'Admin/Commodities';
$route['admin/commodity/(:any)'] = 'Admin/CommodityDetails';
$route['admin/add-commodity'] = 'Admin/addCommodity';
$route['admin/edit-commodity/(:any)'] = 'Admin/editCommodity';
$route['admin/del-commodity/(:any)'] = 'Admin/delCommodity';


// == Orders
$route['admin/orders'] = 'Admin/orders';
$route['admin/orders/(:num)'] = 'Admin/orders';
$route['admin/order/(:any)'] = 'Admin/orderDetails';
$route['admin/editorder/(:any)'] = 'Admin/editOrder';
$route['admin/updateorder'] = 'Admin/updateOrder';
$route['admin/delorder/(:any)'] = 'Admin/deleteOrder';

// == Contacts
$route['admin/contacts'] = 'Admin/contacts';
$route['admin/contacts/(:num)'] = 'Admin/contacts';
$route['admin/contact/(:any)'] = 'Admin/contactDetails';


// == Category
$route['admin/category'] = 'Admin/category';
$route['admin/loadcategory'] = 'Admin/loadCategory';
$route['admin/newcategory'] = 'Admin/newCategory';
$route['admin/removecategory'] = 'Admin/removeCategory';
$route['admin/latestcategories'] = 'Admin/latestCategories';

// == Logs
$route['admin/logs'] = 'Admin/Logs';
$route['admin/logs/(:num)'] = 'Admin/Logs';

// == Messages and MessageChat
$route['admin/messages'] = 'Admin/messages';
$route['admin/messagechat/(:any)'] = 'Admin/messageChat';
$route['admin/postmessage'] = 'Admin/postMessage'; //ajax
$route['admin/loadmessages'] = 'Admin/loadMessages'; //ajax
$route['admin/newmessages'] = 'Admin/newMessage'; //ajax

// Default Routes
$route['default_controller'] = 'Home/index';
$route['404_override'] = 'Home/error404';
$route['error403'] = 'Home/error403';
$route['translate_uri_dashes'] = false;
