<?php

require APPPATH . 'libraries/REST_Controller.php';

class Rsl extends REST_Controller
{

    private $test_db;

    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function __construct()
    {
        parent::__construct();
        // $this->test_db = $this->load->database('test', true);
        $this->test_db = $this->load->database('default', true);

    }

    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function commodities_get()
    {
        $limit = (int)$this->uri->segment(4);
        $offset = (int)$this->uri->segment(5);

        if (empty($limit)) {
            $limit = 10;
        }
        if (empty($offset)) {
            $offset = 0;
        }

        if ($offset == 0) {
            $this->test_db->limit($limit);
        } else {
            $this->test_db->limit($limit, $offset);
        }

        $this->test_db->select('comm_id, comm_name, comm_status, comm_price_per_unit, comm_cycle,comm_commodity_details,comm_date_created');
        $commodities = $this->test_db->get_where("commodities", ['comm_commodity_type' => 'trade' ])->result();
        $total_commodities = $this->test_db->where('comm_commodity_type', 'trade')->count_all_results('commodities');
        if(empty($commodities)){
            $this->response(array('status' => 'failure', 'message' => 'No Commodity available!'), REST_Controller::HTTP_BAD_REQUEST);
        }else{
            $this->response(array('status' => 'success', 'data' => ['total' => $total_commodities, 'commodities' => $commodities]), REST_Controller::HTTP_OK);
        }
    }

     /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function commodity_get($id = 0)
    {
        if (empty($id)) {
            $this->response(array('status' => 'failure', 'message' => 'No Commodity ID supplied'), REST_Controller::HTTP_BAD_REQUEST);
        }
        $this->test_db->select('comm_id, comm_name, comm_status, comm_price_per_unit,comm_cycle,comm_commodity_details,comm_date_created');
        $commodity = $this->test_db->get_where("commodities", ['comm_id' => $id, ])->row_array();
        if(empty($commodity)){
            $this->response(array('status' => 'failure', 'message' => 'Commodity ID does not exist'), REST_Controller::HTTP_BAD_REQUEST);
        }else{
            $this->response(array('status' => 'success', 'data' => $commodity), REST_Controller::HTTP_OK);
        }
    }

    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function send_payment_post()
    {

        $this->form_validation->set_rules('full_name', 'Full Name', 'trim|required|min_length[4]');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('commodity_id', 'Commodity ID', 'trim|integer|required');
        $this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|integer');

        $channel = $this->restkeys_model->getRestkeyUserName(getallheaders()['X-API-KEY'])->username;

        if ($this->form_validation->run() == false) {
            $this->response(array('status' => 'failure', 'message' => $this->validation_errors()), REST_Controller::HTTP_BAD_REQUEST);
        } else {
           
            $commodity_id = $this->input->post('commodity_id');
            $quantity = $this->input->post('quantity');
            $full_name = $this->input->post('full_name');
            $phone = $this->input->post('phone');
            $email = $this->input->post('email');

            $user = $this->user_model->getUserByEmail($email);
            $commodity = $this->commodity_model->getCommodity($commodity_id);
            
            // check if the user is new
            if (!empty($user)) { 
                // save the investment details
                $investment_id = $this->saveInvestment($user->id, $commodity, $quantity);
                if( $investment_id !== FALSE){
                    $this->test_db->select('iv_id as id, iv_user_id as user_id, iv_commodity_id as commodity_id, iv_commodity_name as commodity_name, iv_quantity as quantity ,iv_amount as amount, iv_pay_status as pay_status, iv_returns as expected_returns, iv_returns_status as returns_status, iv_due_date as due_date, iv_date_created as date_created');
                    $investment = $this->test_db->get_where("investments", ['iv_user_id' => $user->id, 'iv_id' => $investment_id ])->row_array();
                    $this->response(array('status' => 'success', 'data' => ['farmgate_user_id' => $user->id, 'investment' => $investment] ), REST_Controller::HTTP_OK);
                }else{
                    $this->response(array('status' => 'failure', 'message' => 'Commodity ID does not exist!'), REST_Controller::HTTP_BAD_REQUEST);
                }
                
            } else {
                // Create new user and save the investment details
                $password = trim($phone.$email);
                $hash_password = hash('sha512', $password);
                $slug = $this->user_model->generateUserSlug($full_name, $email);
                $uData = array(
                    'full_name' => $full_name,
                    'email' => $email,
                    'phone' => $phone,
                    'slug' => $slug,
                    'password' => $hash_password,
                    'channel' => $channel,
                    'active' => 'yes',
                    'date_created' => date('Y-m-d H:i:s'),
                );

                $newUserID = $this->user_model->newUser($uData);

                // save the investment details
                $investment_id = $this->saveInvestment($newUserID, $commodity, $quantity);
                if( $investment_id !== FALSE){
                    $this->test_db->select('iv_id as id, iv_user_id as user_id, iv_commodity_id as commodity_id, iv_commodity_name as commodity_name, iv_quantity as quantity ,iv_amount as amount, iv_pay_status as pay_status, iv_returns as expected_returns, iv_returns_status as returns_status, iv_due_date as due_date, iv_date_created as date_created');
                    $investment = $this->test_db->get_where("investments", ['iv_user_id' => $newUserID, 'iv_id' => $investment_id ])->row_array();
                    $this->response(array('status' => 'success', 'data' => ['farmgate_user_id' => $newUserID, 'investment' => $investment] ), REST_Controller::HTTP_OK);
                }else{
                    $this->response(array('status' => 'failure', 'message' => 'Commodity ID does not exist!'), REST_Controller::HTTP_BAD_REQUEST);
                }

            }
            $this->response(array('status' => 'success'), REST_Controller::HTTP_OK);
        }
    }

    /**
     * Helper method to save investment.
     *
     * @return Boolean or Investment Object
     */
    protected function saveInvestment($userID, $commodity, $quantity)
    {

        if(!empty($commodity)){

            $amount = (int) $commodity->comm_price_per_unit * $quantity;

            $newInvestmentData = array(
                'iv_user_id' => $userID,
                'iv_commodity_id' => $commodity->comm_id,
                'iv_commodity_name' => $commodity->comm_name,
                'iv_quantity' => $quantity,
                'iv_currency_id' => 1,
                'iv_amount' => $amount,
                'iv_pay_status' => 'pending',
                'iv_returns_status' => 'pending',
                'iv_slug' => $this->investment_model->generateSlug($userID),
                'iv_created_by' => $userID,
                'iv_date_created' => date('Y-m-d H:i:s'),
            );

            return $this->investment_model->newInvestment($newInvestmentData);
        } else {
            return FALSE;
        }
    }

    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function user_investments_get()
    {
        $user_id = (int)$this->uri->segment(4);
        if (empty($user_id)) {
            $this->response(array('status' => 'failure', 'message' => 'No User ID supplied'), REST_Controller::HTTP_BAD_REQUEST);
        }

        $limit = (int)$this->uri->segment(5);
        $offset = (int)$this->uri->segment(6);

        if (empty($limit)) {
            $limit = 10;
        }
        if (empty($offset)) {
            $offset = 0;
        }

        if ($offset == 0) {
            $this->test_db->limit($limit);
        } else {
            $this->test_db->limit($limit, $offset);
        }

        $this->test_db->select('iv_id as id, iv_user_id as user_id, iv_commodity_id as commodity_id, iv_commodity_name as commodity_name, iv_quantity as quantity ,iv_amount as amount, iv_pay_status as pay_status, iv_returns as expected_returns, iv_returns_status as returns_status, iv_due_date as due_date, iv_date_created as date_created');
        $investments = $this->test_db->get_where("investments", ['iv_user_id' => $user_id])->result();
        $total_investments = $this->test_db->where('iv_user_id', $user_id)->count_all_results('investments', ['iv_user_id' => $user_id]);
        if(empty($investments)){
            $this->response(array('status' => 'failure', 'message' => 'No Investment available!'), REST_Controller::HTTP_BAD_REQUEST);
        }else{
            $this->response(array('status' => 'success', 'data' => ['total' => $total_investments, 'investments' => $investments]), REST_Controller::HTTP_OK);
        }
    }

     /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function user_investment_get($user_id = '', $investment_id = '')
    {
        if (empty($user_id)) {
            $this->response(array('status' => 'failure', 'message' => 'No User ID supplied'), REST_Controller::HTTP_BAD_REQUEST);
        }

        if (empty($investment_id)) {
            $this->response(array('status' => 'failure', 'message' => 'No Investment ID supplied'), REST_Controller::HTTP_BAD_REQUEST);
        }

        $this->test_db->select('iv_id as id, iv_user_id as user_id, iv_commodity_id as commodity_id, iv_commodity_name as commodity_name, iv_quantity as quantity ,iv_amount as amount, iv_pay_status as pay_status, iv_returns as expected_returns, iv_returns_status as returns_status, iv_due_date as due_date, iv_date_created as date_created');
        $investment = $this->test_db->get_where("investments", ['iv_user_id' => $user_id, 'iv_id' => $investment_id ])->row_array();
        if(empty($investment)){
            $this->response(array('status' => 'failure', 'message' => 'Investment does not exist'), REST_Controller::HTTP_BAD_REQUEST);
        }else{
            $this->response(array('status' => 'success', 'data' => $investment), REST_Controller::HTTP_OK);
        }
    }


}