<?php
require_once __DIR__ . '/../../vendor/autoload.php';

class Admin extends CI_Controller
{

    private $mpdf = '';

    // View variables for feedback boxes
    private $open_delimiter = '<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>';
    private $open_delimiter_success = '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>';
    private $close_delimiter = '</strong></div>';

    // pagination configuration
    private $pagination_config = array(
        'use_page_numbers' => true,
        'reuse_query_string' => true,
        'full_tag_open' => '<nav aria-label="Page navigation"><ul class="pagination mt-4 justify-content-end d-flex">',
        'full_tag_close' => '</ul></nav>',
        'first_tag_open' => '<li class="page-item">',
        'first_tag_close' => '</li>',
        'first_link' => 'First',
        'attributes' => array('class' => 'page-link'),
        'cur_tag_open' => '<a class="page-link">',
        'cur_tag_close' => '</a>',
        'next_link' => '&raquo;',
        'prev_link' => '&laquo;',
        'num_links' => 2,
    );

    // Roles
    private $loggedInUserID;
    private $user;
    // var_dump($user->type);
    private $is_admin;
    private $is_cc_manager;
    private $is_wh_manager;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->loggedInUserID = $this->session->userdata('uid');
        if (empty($this->loggedInUserID)) {
            redirect('/');
            return;
        }

        $this->user = $this->user_model->getUser($this->loggedInUserID);
        // var_dump($user->type);
        $this->is_admin = ($this->user->type == "admin") ? true : false;
        $this->is_cc_manager = ($this->user->type == "collection_center_manager") ? true : false;
        $this->is_wh_manager = ($this->user->type == "warehouse_manager") ? true : false;
        
        // PDF Printer
        // $this->mpdf = new \Mpdf\Mpdf();

    }

    public function index()
    {
        if ($this->is_admin || $this->is_cc_manager || $this->is_wh_manager) {
            $user_id = $this->session->userdata('uid');
            // var_dump($user_id);
            // die();
            $user = $this->user_model->getUser($user_id);

            $data['user'] = $user;

            $this->load->view('admin/temp/header');
            $this->load->view('admin/temp/sidebar');
            $this->load->view('admin/index', $data);
            $this->load->view('admin/temp/footer');
            // Log Data
            $logData = array('lg_user_id' => $this->loggedInUserID);
            $logData['lg_message'] = "Viewed Index Page. ";
            $this->log_model->newLog($logData);
        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function addUser()
    {
        if ($this->is_admin) {

            $admin_user_id = $this->session->userdata('uid');

            $user = $this->user_model->getUser($admin_user_id);

            $data['user'] = $user;
            $data['statuses'] = $this->user_model->get_enum_values('fe_users', 'type');

            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
            $this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_check_email_exists');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');

            if (isset($_POST['submit'])) {

                if ($this->form_validation->run() == false) {
                    $this->loadAddUserView($data);
                } else {

                    $hash_password = hash('sha512', $this->input->post('password'));
                    $newUserData = array(
                        'full_name' => $this->input->post('full_name'),
                        'password' => $hash_password,
                        'email' => $this->input->post('email'),
                        'channel' => $admin_user_id,
                        'slug' => $this->user_model->generateUserSlug($this->input->post('email'), $admin_user_id),
                        'type' => $this->input->post('typeSelect'),
                        'active' => 'yes',
                        'date_created' => date('Y-m-d H:i:s'),
                    );

                    if (!empty($_FILES['userImageFile']['name'])) {
                        $user_id = $this->user_model->newUser($newUserData);
                        // Log Data
                        $logData = array('lg_user_id' => $this->loggedInUserID);
                        $logData['lg_message'] = "Added New User. "."Slug: ".$newUserData['slug'];
                        $this->log_model->newLog($logData);
                        $path = './mainasset/fe/images/users/';
                        $results = $this->do_upload($path, 'userImageFile');
                        if (!isset($results['errors'])) {
                            //add the images to the cusers table
                            $imageData = array(
                                'image' => $results['file_name'],
                                'date_updated' => date('Y-m-d H:i:s'),
                            );
                            $this->user_model->updateUser($user_id, $imageData);
                            //Show success message
                            $feedback = $this->open_delimiter_success . 'User Image has been successfully uploaded!' . $this->close_delimiter;
                            $this->session->set_flashdata('user_feedback', $feedback);
                        } else {
                            $data['errors'] = $results['errors'];
                            $this->session->set_flashdata('user_feedback', $results['errors']);
                        }
                    } else {
                        //Show error
                        $fail_message = $this->open_delimiter . 'Your have not selected any user Image file to upload!' . $this->close_delimiter;
                        $this->session->set_flashdata('user_feedback', $fail_message);
                    }
                    
                    redirect('admin/adduser');

                }
            } else {
                $this->loadAddUserView($data);
            }

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadAddUserView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/adduser', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/footer_close');
    }

    public function editUser()
    {
        if ($this->is_admin) {

            $admin_user_id = $this->session->userdata('uid');
            $admin_user = $this->user_model->getUser($admin_user_id);

            $user_slug = $this->uri->segment(3);

            // Check if the slug exists
            if (!$this->user_model->slug_exists($user_slug)) {
                show_404();
            } else {

                $user = $this->user_model->getUserBySlug($user_slug);

                $data['user'] = $user;
                $data['statuses'] = $this->user_model->get_enum_values('fe_users', 'type');

                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
                $this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
                $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');

                if (isset($_POST['submit'])) {

                    if ($this->form_validation->run() == false) {
                        $this->loadEditUserView($data);
                    } else {
                        $hash_password = hash('sha512', $this->input->post('password'));
                        $UserData = array(
                            'full_name' => $this->input->post('full_name'),
                            'password' => $hash_password,
                            'channel' => $admin_user_id,
                            'type' => $this->input->post('typeSelect'),
                            'active' => 'yes',
                            'date_updated' => date('Y-m-d H:i:s'),
                        );

                        $this->user_model->updateUser($user->id, $UserData);

                        $feedback = $this->open_delimiter_success . 'User Information has been successfully updated!' . $this->close_delimiter;
                        $this->session->set_flashdata('user_feedback', $feedback);

                        if (!empty($_FILES['userImageFile']['name'])) {

                            $path = './mainasset/fe/images/users/';
                            $results = $this->do_upload($path, 'userImageFile');
                            if (!isset($results['errors'])) {
                                //add the images to the cusers table
                                $imageData = array(
                                    'image' => $results['file_name'],
                                    'date_updated' => date('Y-m-d H:i:s'),
                                );
                                $this->user_model->updateUser($user->id, $imageData);
                            } else {
                                $data['errors'] = $results['errors'];
                                $this->session->set_flashdata('user_feedback', $results['errors']);
                            }
                        }
                        // Log Data
                        $logData = array('lg_user_id' => $this->loggedInUserID);
                        $logData['lg_message'] = "Edited Users. "."Slug: ".$user->slug;
                        $this->log_model->newLog($logData);
                        redirect('admin/edituser/' . $user->slug);
                    }
                } else {
                    $this->loadEditUserView($data);
                }
            }
        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadEditUserView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/edituser', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/scripts/edituser');
        $this->load->view('admin/temp/footer_close');
    }

    public function deleteUser()
    {
        if ($this->is_admin) {

            $data = array();
            $user_slug = $this->uri->segment(3);

            if (!$this->user_model->slug_exists($user_slug)) {
                show_404();
            } else {

                $user = $this->user_model->getuserBySlug($user_slug);
                $user_id = $this->session->userdata('uid');
                $logData = array('lg_user_id' => $this->loggedInUserID);

                if ($user !== false) {

                    $updateData = array(
                        'active' => 'no',
                        'date_updated' => date('Y-m-d H:i:s'),
                    );

                    $this->user_model->updateuser($user->id, $updateData);

                    $feedback = $this->open_delimiter_success . 'User ' . $user->full_name . '-' . $user->slug . ' has been successfully deactivated!' . $this->close_delimiter;
                    $this->session->set_flashdata('admin_del_user_feedback', $feedback);

                    // Log Data
                    $logData['lg_message'] = "Deactivated Users. "."Slug: ".$user->slug;
                    $this->log_model->newLog($logData);

                    redirect('admin/users');
                } else {
                    redirect('admin/users');
                }

            }
        }
    }

    public function Users()
    {

        // var_dump($this->is_admin);
        if ($this->is_admin) {

            $logData = array('lg_user_id' => $this->loggedInUserID);

            $users = array();
            $type = $this->input->get('type');
            if(empty($type)){
                $type = '';
            }
            $keyword = $this->input->get('keyword_us');
            if(empty($keyword)){
                $keyword = '';
            }

            $this->load->library('pagination');
            $config = $this->pagination_config;
            
            // var_dump($keyword);
            // die();
            // Add the search data in session to populate the fields again
            $this->session->set_userdata('type', $type);
            $this->session->set_userdata('keyword_us', $keyword);
            $data['type'] = $this->session->userdata('type');
            $data['keyword_us'] = $this->session->userdata('keyword_us');


            // Get the page number if it is set
            $page = $this->uri->segment(3);
            $per_page = 10;
            if (empty($page)) {
                $page = 1;
                $offset = 0;
            } else {
                $page = (int) $page;
                $offset = $page * $per_page;
            }

            if(empty($type) && empty($keyword)){
                $users = $this->user_model->getUsers($per_page, $offset);
                $config['total_rows'] = $this->user_model->getTotalUsers();
                $data['total_users'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Users Page. "."Page: ".$page;

            }elseif(empty($type) && !empty($keyword)){
                $users = $this->user_model->getUsersByName($keyword, $per_page, $offset);
                $config['total_rows'] = $this->user_model->getTotalUsersByName($keyword);
                $data['total_users'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Users Page. "."Page: ".$page.". Searched for: [".$keyword."]";

            }elseif(!empty($type) && empty($keyword)){
                $users = $this->user_model->getUsersByType($type, $per_page, $offset);
                $config['total_rows'] = $this->user_model->getTotalUsersByType($type);
                $data['total_users'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Users Page. "."Page: ".$page.". Type: ".$type;
                
            }elseif(!empty($type) && !empty($keyword)){
                $users = $this->user_model->getUsersByTypeAndName($type, $keyword, $per_page, $offset);
                $config['total_rows'] = $this->user_model->getTotalUsersByTypeAndName($type, $keyword);
                $data['total_users'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Users Page. "."Page: ".$page.". Type: ".$type.". Searched for [".$keyword."]";
            }

            $config['base_url'] = base_url() . "admin/users/";
            
            $this->pagination->initialize($config);

            $data['pagination_links'] = $this->pagination->create_links();
            $data['users'] = $users;
            $this->load->view('admin/temp/header');
            $this->load->view('admin/temp/sidebar');
            $this->load->view('admin/allusers', $data);
            $this->load->view('admin/temp/footer');
            // Log Data
            $this->log_model->newLog($logData);

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function userDetail()
    {
        if ($this->is_admin) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);
            $logData = array('lg_user_id' => $this->loggedInUserID);

            $data['admin_user'] = $user;
            $user_slug = $this->uri->segment(3);
            $dUser = $this->user_model->getUserBySlug($user_slug);
            if (!empty($dUser)) {
                $data['user'] = $dUser;
                // $dUser->local = $this->locals_model->getLocalName($dUser->local_id);
                // $dUser->state = $this->state_model->getStateName($dUser->state_id);
            }

            $this->load->view('admin/temp/header');
            $this->load->view('admin/temp/sidebar');
            $this->load->view('admin/userdetail', $data);
            $this->load->view('admin/temp/footer');
            // Log Data
            $logData['lg_message'] = "Viewed Users Details. "."Slug: ".$dUser->slug;
            $this->log_model->newLog($logData);

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function addCluster()
    {
        if ($this->is_admin) {

            $admin_user_id = $this->session->userdata('uid');

            $user = $this->user_model->getUser($admin_user_id);

            $data['user'] = $user;

            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
            $this->form_validation->set_rules('name', 'Cluster Name', 'trim|required');
            $this->form_validation->set_rules('address', 'Address', 'trim|required|min_length[8]');
            $this->form_validation->set_rules('products', 'Products Obtainable', 'trim|required|min_length[8]');
            $this->form_validation->set_rules('mgr_name', 'Manager Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('phone', 'Phone', 'trim|required');

            if (isset($_POST['submit'])) {

                if ($this->form_validation->run() == false) {
                    $this->loadAddClusterView($data);
                } else {

                    $newClusterData = array(
                        'cl_name' => $this->input->post('name'),
                        'cl_address' => $this->input->post('address'),
                        'cl_products' => $this->input->post('products'),
                        'cl_manager' => $this->input->post('mgr_name'),
                        'cl_manager_email' => $this->input->post('email'),
                        'cl_manager_phone' => $this->input->post('phone'),
                        'cl_created_by' => $admin_user_id,
                        'cl_slug' => $this->clusters_model->generateClusterSlug($admin_user_id),
                        'cl_date_created' => date('Y-m-d H:i:s'),
                    );
                    $this->clusters_model->newCluster($newClusterData);
                    //Show success message
                    $feedback = $this->open_delimiter_success . 'New Cluster has been created!  ' . $this->close_delimiter;
                    $this->session->set_flashdata('cluster_feedback', $feedback);

                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "New Cluster Added "."Slug: ".$newClusterData['cl_slug'];
                    $this->log_model->newLog($logData);

                    redirect('admin/add-cluster');

                }
            } else {
                $this->loadAddClusterView($data);
            }

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadAddClusterView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/add_cluster', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/footer_close');
    }

    public function Clusters()
    {
        if ($this->is_admin) {

            $logData = array('lg_user_id' => $this->loggedInUserID);

            $clusters = array();
            $keyword = $this->input->get('keyword_cl');

            $this->load->library('pagination');
            $config = $this->pagination_config;

            // Add the search data in session to populate the fields again
            $this->session->set_userdata('keyword_cl', $keyword);
            $data['keyword_cl'] = $this->session->userdata('keyword_cl');

            // Get the page number if it is set
            $page = $this->uri->segment(3);
            $per_page = 10;
            if (empty($page)) {
                $page = 1;
                $offset = 0;
            } else {
                $page = (int) $page;
                $offset = $page * $per_page;
            }

            if (!empty($keyword)) {
                $clusters = $this->clusters_model->getClustersWithName($keyword, $per_page, $offset);
                $config['total_rows'] = $this->clusters_model->getTotalClustersWithName($keyword);
                $data['total_clusters'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Clusters Page. "."Page: ".$page.". Searched for [".$keyword."]";


            } else {
                $clusters = $this->clusters_model->getClusters($per_page, $offset);
                $config['total_rows'] = $this->clusters_model->getTotalClusters();
                $data['total_clusters'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Clusters Page. "."Page: ".$page;

            }

            $data['clusters'] = $clusters;

            $config['base_url'] = base_url("admin/clusters/");
            $config['per_page'] = $per_page;

            $this->pagination->initialize($config);

            $data['pagination_links'] = $this->pagination->create_links();

            $this->load->view('admin/temp/header');
            $this->load->view('admin/temp/sidebar');
            $this->load->view('admin/all_clusters', $data);
            $this->load->view('admin/temp/footer');

            $this->log_model->newLog($logData);

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function editCluster()
    {
        if ($this->is_admin) {

            $cluster_slug = $this->uri->segment(3);

            if (!$this->clusters_model->slug_exists($cluster_slug)) {
                show_404();
            } else {
                $user_id = $this->session->userdata('uid');
                $user = $this->user_model->getUser($user_id);

                $cluster = $this->clusters_model->getClusterBySlug($cluster_slug);

                $data['cluster'] = $cluster;
                $data['user'] = $user;

                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
                $this->form_validation->set_rules('name', 'Cluster Name', 'trim|required');
                $this->form_validation->set_rules('address', 'Address', 'trim|required|min_length[8]');
                $this->form_validation->set_rules('products', 'Products Obtainable', 'trim|required|min_length[8]');
                $this->form_validation->set_rules('mgr_name', 'Manager Name', 'trim|required');
                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
                $this->form_validation->set_rules('phone', 'Phone', 'trim|required');

                if (isset($_POST['submit'])) {

                    if ($this->form_validation->run() == false) {
                        $this->loadEditClusterView($data);
                    } else {
                        $updateData = array(
                            'cl_name' => $this->input->post('name'),
                            'cl_address' => $this->input->post('address'),
                            'cl_products' => $this->input->post('products'),
                            'cl_manager' => $this->input->post('mgr_name'),
                            'cl_manager_email' => $this->input->post('email'),
                            'cl_manager_phone' => $this->input->post('phone'),
                            'cl_edited_by' => $admin_user_id,
                            'cl_date_created' => date('Y-m-d H:i:s'),
                        );
                        $this->clusters_model->updateCluster($cluster->cl_id, $updateData);
                        //Show success message
                        $feedback = $this->open_delimiter_success . 'Cluster has been updated!  ' . $this->close_delimiter;
                        $this->session->set_flashdata('cluster_feedback', $feedback);

                        $logData = array('lg_user_id' => $this->loggedInUserID);
                        $logData['lg_message'] = "Edited Cluster "."Slug: ".$cluster_slug;
                        $this->log_model->newLog($logData);

                        redirect('admin/editcluster/' . $cluster->cl_slug);

                    }
                } else {
                    $this->loadEditClusterView($data);
                }
            }

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadEditClusterView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/edit_cluster', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/footer_close');
    }

    public function deleteCluster()
    {
        if ($this->is_admin) {

            $data = array();
            $cluster_slug = $this->uri->segment(3);

            if (!$this->clusters_model->slug_exists($cluster_slug)) {
                show_404();
            } else {

                $cluster = $this->clusters_model->getClusterBySlug($cluster_slug);

                if ($cluster !== false) {

                    $this->clusters_model->hardDelete($cluster->cl_id);

                    $feedback = $this->open_delimiter_success . 'Cluster ' . $cluster->cl_name . '-' . $cluster->cl_slug . ' has been successfully deleted!' . $this->close_delimiter;
                    $this->session->set_flashdata('cluster_del_feedback', $feedback);

                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Deleted Cluster "."Slug: ".$cluster_slug;
                    $this->log_model->newLog($logData);

                }

                redirect('admin/clusters');

            }
        }
    }

    public function clusterDetail()
    {
        if ($this->is_admin) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);

            $data['user'] = $user;

            $cluster_slug = $this->uri->segment(3);
            if (!empty($cluster_slug)) {
                $cluster = $this->clusters_model->getClusterBySlug($cluster_slug);
                if (!empty($cluster)) {
                    # code...
                    $cluster->admin = $this->user_model->getUser($cluster->cl_created_by)->full_name;

                    $data['cluster'] = $cluster;

                    $this->load->view('admin/temp/header');
                    $this->load->view('admin/temp/sidebar');
                    $this->load->view('admin/cluster_details', $data);
                    $this->load->view('admin/temp/footer');

                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Viewed Cluster Details "."Slug: ".$cluster_slug;
                    $this->log_model->newLog($logData);

                } else {
                    show_404();
                }
            } else {
                show_404();
            }
        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function addCollectionCenter()
    {
        if ($this->is_admin) {
            $data = array();
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
            $this->form_validation->set_rules('name', 'Collection Center Name', 'trim|required');
            $this->form_validation->set_rules('address', 'Address', 'trim|required|min_length[8]');
            $this->form_validation->set_rules('products', 'Products Obtainable', 'trim|required|min_length[8]');
            $this->form_validation->set_rules('max_capacity', 'Maximum Capacity', 'trim|required|numeric');
            $this->form_validation->set_rules('manager_id', 'Manager ', 'trim|required');

            if (isset($_POST['submit'])) {

                if ($this->form_validation->run() == false) {
                    $this->loadAddCollectionCenterView($data);
                } else {

                    $newCollectionCenterData = array(
                        'cc_name' => $this->input->post('name'),
                        'cc_address' => $this->input->post('address'),
                        'cc_products' => $this->input->post('products'),
                        'cc_max_capacity' => $this->input->post('max_capacity'),
                        'cc_manager_id' => $this->input->post('manager_id'),
                        'cc_created_by' => $this->loggedInUserID,
                        'cc_slug' => $this->collectioncenter_model->generateSlug($this->loggedInUserID),
                        'cc_date_created' => date('Y-m-d H:i:s'),
                    );
                    $this->collectioncenter_model->newCollectionCenter($newCollectionCenterData);
                    //Show success message
                    $feedback = $this->open_delimiter_success . 'New Collection Center has been created!  ' . $this->close_delimiter;
                    $this->session->set_flashdata('collection_center_feedback', $feedback);

                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "New Collection Center Added "."Slug: ".$newCollectionCenterData['cc_slug'];
                    $this->log_model->newLog($logData);
                    redirect('admin/add-collection-center');

                }
            } else {
                $this->loadAddCollectionCenterView($data);
            }
        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadAddCollectionCenterView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/add_collection_center', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/scripts/add_collection_center');
        $this->load->view('admin/temp/footer_close');
    }

    public function getCollectionCenterManagers()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $response = array();
        $mgr_name = $this->input->post('mgr_name');

        // Get Managers with similar name
        $managers = $this->collectioncenter_model->getManagersWithName($mgr_name);
        if (empty($managers)) {
            $managers = array();
        }

        $logData = array('lg_user_id' => $this->loggedInUserID);
        $logData['lg_message'] = "Ajax Fetching Collection Center Managers";
        $this->log_model->newLog($logData);

        $response = array("" => 1, "managers" => $managers);

        echo json_encode($response);

    }

    public function CollectionCenters()
    {
        if ($this->is_admin) {

            $logData = array('lg_user_id' => $this->loggedInUserID);

            $clusters = array();
            $keyword = $this->input->get('keyword_cc');

            $this->load->library('pagination');
            $config = $this->pagination_config;

            // Add the search data in session to populate the fields again
            $this->session->set_userdata('keyword_cc', $keyword);
            $data['keyword_cc'] = $this->session->userdata('keyword_cc');

            // Get the page number if it is set
            $page = $this->uri->segment(3);
            $per_page = 10;
            if (empty($page)) {
                $page = 1;
                $offset = 0;
            } else {
                $page = (int) $page;
                $offset = $page * $per_page;
            }

            if (!empty($keyword)) {
                $collection_centers = $this->collectioncenter_model->getCollectionCentersWithName($keyword, $per_page, $offset);
                $config['total_rows'] = $this->collectioncenter_model->getTotalCollectionCentersWithName($keyword);
                $data['total_collection_centers'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Collection Center Page. "."Page: ".$page.". Searched for [".$keyword."]";

            } else {
                $collection_centers = $this->collectioncenter_model->getCollectionCenters($per_page, $offset);
                $config['total_rows'] = $this->collectioncenter_model->getTotalCollectionCenters();
                $data['total_collection_centers'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Collection Center Page. "."Page: ".$page;

            }

            if (!empty($collection_centers)) {
                foreach ($collection_centers as $collection_center) {
                    $manager = $this->user_model->getUser($collection_center->cc_manager_id);
                    $collection_center->manager = $manager->full_name;
                }
            }

            $data['collection_centers'] = $collection_centers;

            $config['base_url'] = base_url("admin/collection-centers/");
            $config['per_page'] = $per_page;

            $this->pagination->initialize($config);

            $data['pagination_links'] = $this->pagination->create_links();

            $this->load->view('admin/temp/header');
            $this->load->view('admin/temp/sidebar');
            $this->load->view('admin/all_collection_centers', $data);
            $this->load->view('admin/temp/footer');

            $this->log_model->newLog($logData);

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function editCollectionCenter()
    {
        if ($this->is_admin) {

            $collection_center_slug = $this->uri->segment(3);

            if (!$this->collectioncenter_model->slug_exists($collection_center_slug)) {
                show_404();
            } else {
                $user_id = $this->session->userdata('uid');
                $admin_user = $this->user_model->getUser($user_id);

                $collection_center = $this->collectioncenter_model->getCollectionCenterBySlug($collection_center_slug);
                $collection_center_manager = $this->user_model->getUser($collection_center->cc_manager_id);

                $data['collection_center'] = $collection_center;
                $data['cc_manager'] = $collection_center_manager;

                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
                $this->form_validation->set_rules('name', 'Collection Center Name', 'trim|required');
                $this->form_validation->set_rules('address', 'Address', 'trim|required|min_length[8]');
                $this->form_validation->set_rules('products', 'Products Obtainable', 'trim|required|min_length[8]');
                $this->form_validation->set_rules('max_capacity', 'Maximum Capacity', 'trim|required|numeric');
                $this->form_validation->set_rules('manager_id', 'Manager ', 'trim|required');

                if (isset($_POST['submit'])) {

                    if ($this->form_validation->run() == false) {
                        $this->loadEditCollectionCenterView($data);
                    } else {
                        $updateData = array(
                            'cc_name' => $this->input->post('name'),
                            'cc_address' => $this->input->post('address'),
                            'cc_products' => $this->input->post('products'),
                            'cc_max_capacity' => $this->input->post('max_capacity'),
                            'cc_manager_id' => $this->input->post('manager_id'),
                            'cc_edited_by' => $admin_user->id,
                            'cc_date_updated' => date('Y-m-d H:i:s'),
                        );
                        $this->collectioncenter_model->updateCollectionCenter($collection_center->cc_id, $updateData);
                        //Show success message
                        $feedback = $this->open_delimiter_success . 'Collection Center has been updated!  ' . $this->close_delimiter;
                        $this->session->set_flashdata('collection_center_feedback', $feedback);

                        $logData = array('lg_user_id' => $this->loggedInUserID);
                        $logData['lg_message'] = "Edited Collection Center "."Slug: ".$collection_center_slug;
                        $this->log_model->newLog($logData);

                        redirect('admin/edit-collection-center/' . $collection_center->cc_slug);

                    }
                } else {
                    $this->loadEditCollectionCenterView($data);
                }

            }

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadEditCollectionCenterView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/edit_collection_center', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/scripts/edit_collection_center');
        $this->load->view('admin/temp/footer_close');
    }

    public function deleteCollectionCenter()
    {
        if ($this->is_admin) {

            $data = array();
            $collection_center_slug = $this->uri->segment(3);

            if (!$this->collectioncenter_model->slug_exists($collection_center_slug)) {
                show_404();
            } else {

                $collection_center = $this->collectioncenter_model->getCollectionCenterBySlug($collection_center_slug);

                if ($collection_center !== false) {

                    $this->collectioncenter_model->hardDelete($collection_center->cc_id);
                    $feedback = $this->open_delimiter_success . 'Collection Center ' . $collection_center->cc_name . ' - ' . $collection_center->cc_slug . ' has been successfully deleted!' . $this->close_delimiter;
                    $this->session->set_flashdata('collection_center_del_feedback', $feedback);

                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Deleted Collection Center "."Slug: ".$collection_center_slug;
                    $this->log_model->newLog($logData);

                }

                redirect('admin/collection-centers');

            }
        }
    }

    public function collectionCenterDetail()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $collection_center_slug = $this->uri->segment(3);
            if (!empty($collection_center_slug)) {
                $collection_center = $this->collectioncenter_model->getCollectionCenterBySlug($collection_center_slug);
                if (!empty($collection_center)) {
                    # code...
                    $collection_center->manager = $this->user_model->getUser($collection_center->cc_manager_id)->full_name;
                    $collection_center->admin = $this->user_model->getUser($collection_center->cc_created_by)->full_name;

                    $data['collection_center'] = $collection_center;

                    $this->load->view('admin/temp/header');
                    $this->load->view('admin/temp/sidebar');
                    $this->load->view('admin/collection_center_details', $data);
                    $this->load->view('admin/temp/footer');

                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Viewed Collection Center Details "."Slug: ".$collection_center_slug;
                    $this->log_model->newLog($logData);

                } else {
                    show_404();
                }
            } else {
                show_404();
            }
        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function addWarehouse()
    {
        if ($this->is_admin) {

            $admin_user_id = $this->session->userdata('uid');

            $user = $this->user_model->getUser($admin_user_id);

            $data['user'] = $user;

            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
            $this->form_validation->set_rules('name', 'Warehouse Name', 'trim|required');
            $this->form_validation->set_rules('address', 'Address', 'trim|required|min_length[8]');
            $this->form_validation->set_rules('products', 'Products Obtainable', 'trim|required|min_length[8]');
            $this->form_validation->set_rules('max_capacity', 'Maximum Capacity', 'trim|required|numeric');
            $this->form_validation->set_rules('manager_id', 'Manager ', 'trim|required');

            if (isset($_POST['submit'])) {

                if ($this->form_validation->run() == false) {
                    $this->loadAddWarehouseView($data);
                } else {

                    $newWarehouseData = array(
                        'wh_name' => $this->input->post('name'),
                        'wh_address' => $this->input->post('address'),
                        'wh_products' => $this->input->post('products'),
                        'wh_max_capacity' => $this->input->post('max_capacity'),
                        'wh_manager_id' => $this->input->post('manager_id'),
                        'wh_created_by' => $admin_user_id,
                        'wh_slug' => $this->warehouse_model->generateSlug($admin_user_id),
                        'wh_date_created' => date('Y-m-d H:i:s'),
                    );
                    $this->warehouse_model->newWarehouse($newWarehouseData);
                    //Show success message
                    $feedback = $this->open_delimiter_success . 'New Warehouse has been created!  ' . $this->close_delimiter;
                    $this->session->set_flashdata('warehouse_feedback', $feedback);

                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "New Warehouse Added "."Slug: ".$newWarehouseData['wh_slug'];
                    $this->log_model->newLog($logData);

                    redirect('admin/add-warehouse');

                }
            } else {
                $this->loadAddWarehouseView($data);
            }

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadAddWarehouseView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/add_warehouse', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/scripts/add_warehouse');
        $this->load->view('admin/temp/footer_close');
    }

    public function getWarehouseManagers()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $response = array();
        $mgr_name = $this->input->post('mgr_name');

        // Get Managers with similar name
        $managers = $this->warehouse_model->getManagersWithName($mgr_name);
        if (empty($managers)) {
            $managers = array();
        }

        $logData = array('lg_user_id' => $this->loggedInUserID);
        $logData['lg_message'] = "Ajax Fetching Warehouse Managers";
        $this->log_model->newLog($logData); 

        $response = array("" => 1, "managers" => $managers);

        echo json_encode($response);

    }

    public function Warehouses()
    {
        if ($this->is_admin || $this->is_wh_manager) {

            $logData = array('lg_user_id' => $this->loggedInUserID);

            $clusters = array();
            $keyword = $this->input->get('keyword_wh');

            $this->load->library('pagination');
            $config = $this->pagination_config;

            // Add the search data in session to populate the fields again
            $this->session->set_userdata('keyword_wh', $keyword);
            $data['keyword_wh'] = $this->session->userdata('keyword_wh');

            // Get the page number if it is set
            $page = $this->uri->segment(3);
            $per_page = 10;
            if (empty($page)) {
                $page = 1;
                $offset = 0;
            } else {
                $page = (int) $page;
                $offset = $page * $per_page;
            }

            if (!empty($keyword)) {
                $warehouses = $this->warehouse_model->getWarehousesWithName($keyword, $per_page, $offset);
                $config['total_rows'] = $this->warehouse_model->getTotalWarehousesWithName($keyword);
                $data['total_warehouses'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Warehouses Page. "."Page: ".$page.". Searched for [".$keyword."]";

            } else {
                $warehouses = $this->warehouse_model->getWarehouses($per_page, $offset);
                $config['total_rows'] = $this->warehouse_model->getTotalWarehouses();
                $data['total_warehouses'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Warehouses Page. "."Page: ".$page;

            }

            if (!empty($warehouses)) {
                foreach ($warehouses as $warehouse) {
                    $manager = $this->user_model->getUser($warehouse->wh_manager_id);
                    $warehouse->manager = $manager->full_name;
                }
            }

            $data['warehouses'] = $warehouses;

            $config['base_url'] = base_url("admin/warehouses/");
            $config['per_page'] = $per_page;

            $this->pagination->initialize($config);

            $data['pagination_links'] = $this->pagination->create_links();

            $this->load->view('admin/temp/header');
            $this->load->view('admin/temp/sidebar');
            $this->load->view('admin/all_warehouses', $data);
            $this->load->view('admin/temp/footer');

            $this->log_model->newLog($logData);

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function editWarehouse()
    {
        if ($this->is_admin) {

            $warehouse_slug = $this->uri->segment(3);

            if (!$this->warehouse_model->slug_exists($warehouse_slug)) {
                show_404();
            } else {

                $warehouse = $this->warehouse_model->getWarehouseBySlug($warehouse_slug);
                $warehouse_manager = $this->user_model->getUser($warehouse->wh_manager_id);

                $data['warehouse'] = $warehouse;
                $data['wh_manager'] = $warehouse_manager;

                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
                $this->form_validation->set_rules('name', 'Collection Center Name', 'trim|required');
                $this->form_validation->set_rules('address', 'Address', 'trim|required|min_length[8]');
                $this->form_validation->set_rules('products', 'Products Obtainable', 'trim|required|min_length[8]');
                $this->form_validation->set_rules('max_capacity', 'Maximum Capacity', 'trim|required|numeric');
                $this->form_validation->set_rules('manager_id', 'Manager ', 'trim|required');

                if (isset($_POST['submit'])) {

                    if ($this->form_validation->run() == false) {
                        $this->loadEditWarehouseView($data);
                    } else {
                        $updateData = array(
                            'wh_name' => $this->input->post('name'),
                            'wh_address' => $this->input->post('address'),
                            'wh_products' => $this->input->post('products'),
                            'wh_max_capacity' => $this->input->post('max_capacity'),
                            'wh_manager_id' => $this->input->post('manager_id'),
                            'wh_edited_by' => $this->loggedInUserID,
                            'wh_date_updated' => date('Y-m-d H:i:s'),
                        );
                        $this->warehouse_model->updateWarehouse($warehouse->wh_id, $updateData);
                        //Show success message
                        $feedback = $this->open_delimiter_success . 'Warehouse has been updated!  ' . $this->close_delimiter;
                        $this->session->set_flashdata('warehouse_feedback', $feedback);

                        $logData = array('lg_user_id' => $this->loggedInUserID);
                        $logData['lg_message'] = "Edited Warehouse "."Slug: ".$warehouse_slug;
                        $this->log_model->newLog($logData);

                        redirect('admin/editwarehouse/' . $warehouse->wh_slug);

                    }
                } else {
                    $this->loadEditWarehouseView($data);
                }
            }

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadEditWarehouseView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/edit_warehouse', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/scripts/edit_warehouse');
        $this->load->view('admin/temp/footer_close');
    }

    public function deleteWarehouse()
    {
        if ($this->is_admin) {

            $data = array();
            $warehouse_slug = $this->uri->segment(3);

            if (!$this->warehouse_model->slug_exists($warehouse_slug)) {
                show_404();
            } else {

                $warehouse = $this->warehouse_model->getWarehouseBySlug($warehouse_slug);

                if ($warehouse !== false) {

                    $this->warehouse_model->hardDelete($warehouse->wh_id);

                    $feedback = $this->open_delimiter_success . 'Warehouse ' . $warehouse->wh_name . ' - ' . $warehouse->wh_slug . ' has been successfully deleted!' . $this->close_delimiter;
                    $this->session->set_flashdata('warehouse_del_feedback', $feedback);

                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Deleted Warehouse Details "."Slug: ".$warehouse_slug;
                    $this->log_model->newLog($logData);

                }
                redirect('admin/warehouses');
            }
        }
    }

    public function warehouseDetail()
    {
        if ($this->is_admin || $this->is_wh_manager) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);

            $data['user'] = $user;

            $warehouse_slug = $this->uri->segment(3);
            if (!empty($warehouse_slug)) {
                $warehouse = $this->warehouse_model->getWarehouseBySlug($warehouse_slug);
                if (!empty($warehouse)) {
                    # code...
                    $warehouse->manager = $this->user_model->getUser($warehouse->wh_manager_id)->full_name;
                    $warehouse->admin = $this->user_model->getUser($warehouse->wh_created_by)->full_name;

                    $data['warehouse'] = $warehouse;

                    $this->load->view('admin/temp/header');
                    $this->load->view('admin/temp/sidebar');
                    $this->load->view('admin/warehouse_details', $data);
                    $this->load->view('admin/temp/footer');

                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Viewed Warehouse Details "."Slug: ".$warehouse_slug;
                    $this->log_model->newLog($logData);

                } else {
                    show_404();
                }
            } else {
                show_404();
            }
        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function addFarmer()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $data['statuses'] = $this->farmer_model->get_enum_values('farmers', 'fm_status');
            $data['sexes'] = $this->farmer_model->get_enum_values('farmers', 'fm_gender');
            $data['banks'] = $this->bank_model->getAllBanks();

            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
            $this->form_validation->set_rules('name', 'Farmer Name', 'trim|required');
            $this->form_validation->set_rules('address', 'Address', 'trim|required|min_length[8]');
            $this->form_validation->set_rules('farm_size', 'Farm Size', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('phone', 'Phone', 'trim|required|min_length[8]');
            $this->form_validation->set_rules('age', 'Age', 'trim|required|numeric');
            $this->form_validation->set_rules('phone', 'Phone', 'trim|required|min_length[8]');
            $this->form_validation->set_rules('primary_produce', 'Primary Produce', 'trim|required');
            $this->form_validation->set_rules('est_annual_produce', 'Estimated Annual Produce', 'trim|required');
            $this->form_validation->set_rules('bank_acc_name', 'Bank Account Name', 'trim|required');
            $this->form_validation->set_rules('bank_acc_no', 'Bank Account Number', 'trim|required');

            if (isset($_POST['submit'])) {

                if ($this->form_validation->run() == false) {
                    $this->loadAddFarmerView($data);
                } else {

                    $newFarmerData = array(
                        'fm_full_name' => $this->input->post('name'),
                        'fm_phone' => $this->input->post('address'),
                        'fm_email' => $this->input->post('email'),
                        'fm_age' => $this->input->post('age'),
                        'fm_status' => $this->input->post('status'),
                        'fm_address' => $this->input->post('address'),
                        'fm_farm_size' => $this->input->post('farm_size'),
                        'fm_phone' => $this->input->post('phone'),
                        'fm_gender' => $this->input->post('gender'),
                        'fm_primary_produce' => $this->input->post('primary_produce'),
                        'fm_other_produce' => $this->input->post('other_produce'),
                        'fm_est_annual_produce' => $this->input->post('est_annual_produce'),
                        'fm_bank_acc_name' => $this->input->post('bank_acc_name'),
                        'fm_bank_acc_no' => $this->input->post('bank_acc_no'),
                        'fm_bank_id' => $this->input->post('bank'),
                        'fm_created_by' => $this->loggedInUserID,
                        'fm_slug' => $this->farmer_model->generateSlug($this->loggedInUserID),
                        'fm_date_created' => date('Y-m-d H:i:s'),
                    );

                    $this->farmer_model->newFarmer($newFarmerData);
                    //Show success message
                    $feedback = $this->open_delimiter_success . 'New Farmer has been created!  ' . $this->close_delimiter;
                    $this->session->set_flashdata('farmer_feedback', $feedback);

                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "New Farmer Added "."Slug: ".$newFarmerData['fm_slug'];
                    $this->log_model->newLog($logData);
                    redirect('admin/add-farmer');

                }
            } else {
                $this->loadAddFarmerView($data);
            }

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadAddFarmerView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/add_farmer', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/footer_close');
    }

    public function getFarmers()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $response = array();
        $fm_name = $this->input->post('fm_name');

        // Get Farmers
        $farmers = $this->farmer_model->getAllActiveFarmersWithName($fm_name);
        if (empty($farmers)) {
            $farmers = array();
        }

        $logData = array('lg_user_id' => $this->loggedInUserID);
        $logData['lg_message'] = "Ajax Fetching Farmers";
        $this->log_model->newLog($logData); 

        $response = array("" => 1, "farmers" => $farmers);

        echo json_encode($response);
    }

    public function Farmers()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $logData = array('lg_user_id' => $this->loggedInUserID);

            $farmers = array();
            $keyword = $this->input->get('keyword_fm');

            $this->load->library('pagination');
            $config = $this->pagination_config;

            // Add the search data in session to populate the fields again
            $this->session->set_userdata('keyword_fm', $keyword);
            $data['keyword_fm'] = $this->session->userdata('keyword_fm');

            // Get the page number if it is set
            $page = $this->uri->segment(3);
            $per_page = 10;
            if (empty($page)) {
                $page = 1;
                $offset = 0;
            } else {
                $page = (int) $page;
                $offset = $page * $per_page;
            }

            if (!empty($keyword)) {
                $farmers = $this->farmer_model->getFarmersWithName($keyword, $per_page, $offset);
                $config['total_rows'] = $this->farmer_model->getTotalFarmersWithName($keyword);
                $data['total_farmers'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Farmers Page. "."Page: ".$page.". Searched for [".$keyword."]";
            } else {
                $farmers = $this->farmer_model->getFarmers($per_page, $offset);
                $config['total_rows'] = $this->farmer_model->getTotalFarmers();
                $data['total_farmers'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Farmers Page. "."Page: ".$page;
            }

            if (!empty($farmers)) {
                foreach ($farmers as $farmer) {
                    $manager = $this->user_model->getUser($farmer->fm_created_by);
                    $farmer->manager = $manager->full_name;
                }
            }

            $data['farmers'] = $farmers;

            $config['base_url'] = base_url("admin/farmers/");
            $config['per_page'] = $per_page;

            $this->pagination->initialize($config);

            $data['pagination_links'] = $this->pagination->create_links();

            $this->load->view('admin/temp/header');
            $this->load->view('admin/temp/sidebar');
            $this->load->view('admin/all_farmers', $data);
            $this->load->view('admin/temp/footer');

            $this->log_model->newLog($logData);

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function editFarmer()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $farmer_slug = $this->uri->segment(3);

            if (!$this->farmer_model->slug_exists($farmer_slug)) {
                show_404();
            } else {

                $admin_user_id = $this->session->userdata('uid');
                $data['statuses'] = $this->farmer_model->get_enum_values('farmers', 'fm_status');
                $data['sexes'] = $this->farmer_model->get_enum_values('farmers', 'fm_gender');
                $data['banks'] = $this->bank_model->getAllBanks();

                $farmer = $this->farmer_model->getFarmerBySlug($farmer_slug);
                $farmer_manager = $this->user_model->getUser($farmer->fm_created_by);

                $data['farmer'] = $farmer;
                $data['fm_manager'] = $farmer_manager;

                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
                $this->form_validation->set_rules('name', 'Farmer Name', 'trim|required');
                $this->form_validation->set_rules('address', 'Address', 'trim|required|min_length[8]');
                $this->form_validation->set_rules('farm_size', 'Farm Size', 'trim|required');
                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
                $this->form_validation->set_rules('phone', 'Phone', 'trim|required|min_length[8]');
                $this->form_validation->set_rules('age', 'Age', 'trim|required|numeric');
                $this->form_validation->set_rules('phone', 'Phone', 'trim|required|min_length[8]');
                $this->form_validation->set_rules('primary_produce', 'Primary Produce', 'trim|required');
                $this->form_validation->set_rules('est_annual_produce', 'Estimated Annual Produce', 'trim|required');
                $this->form_validation->set_rules('bank_acc_name', 'Bank Account Name', 'trim|required');
                $this->form_validation->set_rules('bank_acc_no', 'Bank Account Number', 'trim|required');

                if (isset($_POST['submit'])) {

                    if ($this->form_validation->run() == false) {
                        $this->loadEditFarmerView($data);
                    } else {
                        $updateData = array(
                            'fm_full_name' => $this->input->post('name'),
                            'fm_phone' => $this->input->post('address'),
                            'fm_email' => $this->input->post('email'),
                            'fm_age' => $this->input->post('age'),
                            'fm_status' => $this->input->post('status'),
                            'fm_address' => $this->input->post('address'),
                            'fm_farm_size' => $this->input->post('farm_size'),
                            'fm_phone' => $this->input->post('phone'),
                            'fm_gender' => $this->input->post('gender'),
                            'fm_primary_produce' => $this->input->post('primary_produce'),
                            'fm_other_produce' => $this->input->post('other_produce'),
                            'fm_est_annual_produce' => $this->input->post('est_annual_produce'),
                            'fm_bank_acc_name' => $this->input->post('bank_acc_name'),
                            'fm_bank_acc_no' => $this->input->post('bank_acc_no'),
                            'fm_bank_id' => $this->input->post('bank'),
                            'fm_edited_by' => $admin_user_id,
                            'fm_date_created' => date('Y-m-d H:i:s'),
                        );
                        $this->farmer_model->updateFarmer($farmer->fm_id, $updateData);
                        //Show success message
                        $feedback = $this->open_delimiter_success . 'Farmer Data has been updated!  ' . $this->close_delimiter;
                        $this->session->set_flashdata('farmer_feedback', $feedback);

                        $logData = array('lg_user_id' => $this->loggedInUserID);
                        $logData['lg_message'] = "Edited Farmer Details "."Slug: ".$farmer->fm_slug;
                        $this->log_model->newLog($logData);

                        redirect('admin/editfarmer/' . $farmer->fm_slug);

                    }
                } else {

                    $this->loadEditFarmerView($data);
                }
            }
        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadEditFarmerView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/edit_farmer', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/scripts/edit_farmer');
        $this->load->view('admin/temp/footer_close');

    }

    public function deleteFarmer()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $data = array();
            $farmer_slug = $this->uri->segment(3);

            if (!$this->farmer_model->slug_exists($farmer_slug)) {
                show_404();
            } else {

                $farmer = $this->farmer_model->getFarmerBySlug($farmer_slug);

                if ($farmer !== false) {

                    $this->farmer_model->hardDelete($farmer->fm_id);

                    $feedback = $this->open_delimiter_success . 'Farmer ' . $farmer->fm_full_name . ' - ' . $farmer->fm_slug . ' has been successfully deleted!  ' . $this->close_delimiter;
                    $this->session->set_flashdata('farmer_del_feedback', $feedback);

                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Deleted Farmer"."Slug: ".$farmer_slug;
                    $this->log_model->newLog($logData);

                }

                redirect('admin/farmers');

            }
        }
    }

    public function farmerDetail()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $farmer_slug = $this->uri->segment(3);
            if (!empty($farmer_slug)) {
                $farmer = $this->farmer_model->getFarmerBySlug($farmer_slug);
                if (!empty($farmer)) {
                    # code...
                    $farmer->manager = $this->user_model->getUser($farmer->fm_created_by);
                    $farmer->bank = $this->bank_model->getBank($farmer->fm_bank_id)->bk_name;

                    $data['farmer'] = $farmer;

                    $this->load->view('admin/temp/header');
                    $this->load->view('admin/temp/sidebar');
                    $this->load->view('admin/farmer_details', $data);
                    $this->load->view('admin/temp/footer');

                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Viewed Farmer Details "."Slug: ".$farmer_slug;
                    $this->log_model->newLog($logData);

                } else {
                    show_404();
                }
            } else {
                show_404();
            }
        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function addBank()
    {
        if ($this->is_admin) {

            $data['statuses'] = $this->bank_model->get_enum_values('banks', 'bk_status');
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
            $this->form_validation->set_rules('name', 'Bank Name', 'trim|required');

            if (isset($_POST['submit'])) {

                if ($this->form_validation->run() == false) {
                    $this->loadAddBankView($data);
                } else {

                    $newBankData = array(
                        'bk_name' => $this->input->post('name'),
                        'bk_status' => $this->input->post('status'),
                        'bk_created_by' => $this->loggedInUserID,
                        'bk_slug' => $this->bank_model->generateSlug($admin_user_id),
                        'bk_date_created' => date('Y-m-d H:i:s'),
                    );

                    $this->bank_model->newBank($newBankData);
                    //Show success message
                    $feedback = $this->open_delimiter_success . 'New Bank has been created!  ' . $this->close_delimiter;
                    $this->session->set_flashdata('bank_feedback', $feedback);

                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "New Bank Created "."Slug: ".$newBankData['bk_slug'];
                    $this->log_model->newLog($logData);

                    redirect('admin/add-bank');
                }
            } else {
                $this->loadAddBankView($data);
            }

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadAddBankView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/add_bank', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/footer_close');
    }

    public function getBanks()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $response = array();
        $bk_name = $this->input->post('bk_name');

        // Get Banks
        $banks = $this->warehouse_model->getAllActiveBanksWithName($bk_name);
        if (empty($banks)) {
            $banks = array();
        }

        $logData = array('lg_user_id' => $this->loggedInUserID);
        $logData['lg_message'] = "Ajax Fetching Banks";
        $this->log_model->newLog($logData); 

        $response = array("" => 1, "banks" => $banks);

        echo json_encode($response);

    }

    public function Banks()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $logData = array('lg_user_id' => $this->loggedInUserID);

            $banks = array();
            $keyword = $this->input->get('keyword_bk');

            $this->load->library('pagination');
            $config = $this->pagination_config;

            // Add the search data in session to populate the fields again
            $this->session->set_userdata('keyword_bk', $keyword);
            $data['keyword_bk'] = $this->session->userdata('keyword_bk');

            // Get the page number if it is set
            $page = $this->uri->segment(3);
            $per_page = 10;
            if (empty($page)) {
                $page = 1;
                $offset = 0;
            } else {
                $page = (int) $page;
                $offset = $page * $per_page;
            }

            if (!empty($keyword)) {
                $banks = $this->bank_model->getBanksWithName($keyword, $per_page, $offset);
                $config['total_rows'] = $this->bank_model->getTotalBanksWithName($keyword);
                $data['total_banks'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Banks Page. "."Page: ".$page.". Searched for [".$keyword."]";

            } else {
                $banks = $this->bank_model->getBanks($per_page, $offset);
                $config['total_rows'] = $this->bank_model->getTotalBanks();
                $data['total_banks'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Banks Page. "."Page: ".$page;

            }

            if (!empty($banks)) {
                foreach ($banks as $bank) {
                    $manager = $this->user_model->getUser($bank->bk_created_by);
                    $bank->manager = $manager->full_name;
                }
            }

            $data['banks'] = $banks;

            $config['base_url'] = base_url("admin/banks/");
            $config['per_page'] = $per_page;

            $this->pagination->initialize($config);

            $data['pagination_links'] = $this->pagination->create_links();

            $this->load->view('admin/temp/header');
            $this->load->view('admin/temp/sidebar');
            $this->load->view('admin/all_banks', $data);
            $this->load->view('admin/temp/footer');
            // Log Data
            $this->log_model->newLog($logData);

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function editBank()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $bank_slug = $this->uri->segment(3);

            if (!$this->bank_model->slug_exists($bank_slug)) {
                show_404();
            } else {

                $admin_user_id = $this->session->userdata('uid');
                $data['statuses'] = $this->farmer_model->get_enum_values('banks', 'bk_status');

                $bank = $this->bank_model->getBankBySlug($bank_slug);
                $bank_manager = $this->user_model->getUser($bank->bk_created_by);

                $data['bank'] = $bank;
                $data['fm_manager'] = $bank_manager;

                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
                $this->form_validation->set_rules('name', 'Bank Name', 'trim|required');

                if (isset($_POST['submit'])) {

                    if ($this->form_validation->run() == false) {
                        $this->loadEditBankView($data);
                    } else {
                        $updateData = array(
                            'bk_name' => $this->input->post('name'),
                            'bk_status' => $this->input->post('status'),
                            'bk_edited_by' => $admin_user_id,
                            'bk_date_updated' => date('Y-m-d H:i:s'),
                        );
                        $this->bank_model->updateBank($bank->bk_id, $updateData);
                        //Show success message
                        $feedback = $this->open_delimiter_success . 'Bank Data has been updated!  ' . $this->close_delimiter;
                        $this->session->set_flashdata('bank_feedback', $feedback);

                        $logData = array('lg_user_id' => $this->loggedInUserID);
                        $logData['lg_message'] = "Edited Bank. "."Slug: ".$bank->bk_slug;
                        $this->log_model->newLog($logData);
                        redirect('admin/editbank/' . $bank->bk_slug);

                    }
                } else {
                    $this->loadEditBankView($data);

                }

            }

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadEditBankView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/edit_bank', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/scripts/edit_bank');
        $this->load->view('admin/temp/footer_close');
    }

    public function deleteBank()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $data = array();
            $bank_slug = $this->uri->segment(3);

            if (!$this->bank_model->slug_exists($bank_slug)) {
                show_404();
            } else {

                $bank = $this->bank_model->getBankBySlug($bank_slug);
                $logData = array('lg_user_id' => $this->loggedInUserID);


                if ($bank !== false) {

                    $this->bank_model->hardDelete($bank->bk_id);

                    $feedback = $this->open_delimiter_success . 'Bank ' . $bank->bk_name . ' - ' . $bank->bk_slug . ' has been successfully deleted!  ' . $this->close_delimiter;
                    $this->session->set_flashdata('bank_del_feedback', $feedback);

                    $logData['lg_message'] = "Deleted Bank. "."Slug: ".$bank->bk_slug;
                    $this->log_model->newLog($logData);

                }

                redirect('admin/banks');

            }
        }
    }

    public function bankDetail()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $logData = array('lg_user_id' => $this->loggedInUserID);
            $bank_slug = $this->uri->segment(3);
            if (!empty($bank_slug)) {
                $bank = $this->bank_model->getBankBySlug($bank_slug);
                if (!empty($bank)) {
                    # code...
                    $bank->manager = $this->user_model->getUser($bank->bk_created_by);

                    $data['bank'] = $bank;

                    $this->load->view('admin/temp/header');
                    $this->load->view('admin/temp/sidebar');
                    $this->load->view('admin/bank_details', $data);
                    $this->load->view('admin/temp/footer');

                    $logData['lg_message'] = "Viewed Bank Details. "."Slug: ".$bank->bk_slug;
                    $this->log_model->newLog($logData);
                } else {
                    show_404();
                }
            } else {
                show_404();
            }
        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function addPurchase()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $logData = array('lg_user_id' => $this->loggedInUserID);

            $data['statuses'] = $this->farmer_model->get_enum_values('farmers', 'fm_status');
            $data['payment_statuses'] = $this->farmer_model->get_enum_values('purchases', 'po_status');
            $data['banks'] = $this->bank_model->getAllBanks();
            $data['currencies'] = $this->currency_model->getAllCurrencies();
            $data['collection_centers'] = $this->collectioncenter_model->getAllCollectionCenters();
            $produce_types = $this->producetype_model->getAllProduceTypes();
            if (empty($produce_types)) {
                $produce_types = array();
            }
            $data['produce_types'] = $produce_types;

            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
            $this->form_validation->set_rules('farmer_name', 'Farmer Name', 'trim|required');
            $this->form_validation->set_rules('original_weight', 'Original Weight', 'trim|required');
            $this->form_validation->set_rules('final_weight', 'Final Weight', 'trim|required');
            $this->form_validation->set_rules('po_amount', 'Amount', 'trim|required');
            $this->form_validation->set_rules('po_quantity', 'Quantity', 'trim|required');

            if (isset($_POST['submit'])) {

                if ($this->form_validation->run() == false) {
                    $this->loadAddPurchaseView($data);
                } else {

                    $collection_center_id = $this->input->post('ccSelect');
                    $farmer_name = $this->input->post('farmer_name');
                    $quantity = (int) $this->input->post('po_quantity');
                    $original_weight = (int) $this->input->post('original_weight');
                    $final_weight = (int) $this->input->post('final_weight');
                    $farmer = $this->farmer_model->getAllActiveFarmersWithName($farmer_name);
                    if (empty($farmer)) {

                        $feedback = $this->open_delimiter . 'The Farmer is not found on the system .' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddPurchaseView($data);
                        return;

                    }

                    // Ensure that quantity is not zero or negative
                    if (($quantity <= 0)) {
                        // Tell the user
                        $feedback = $this->open_delimiter . 'Quantity cannot be zero.' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddPurchaseView($data);
                        return;

                    }

                    // Ensure that original weight is not zero or negative
                    if (($original_weight <= 0)) {
                        // Tell the user
                        $feedback = $this->open_delimiter . 'Original weight cannot be zero.' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddPurchaseView($data);
                        return;

                    }

                    // Ensure that final weight is not zero or negative
                    if (($final_weight <= 0)) {
                        // Tell the user
                        $feedback = $this->open_delimiter . 'Final weight cannot be zero.' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddPurchaseView($data);
                        return;

                    }

                    // Check that final weight is less than or equal to original weight
                    if ($final_weight > $original_weight) {
                        // Tell the user
                        $feedback = $this->open_delimiter . 'The Final weight is greater than Original weight.' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddPurchaseView($data);
                        return;

                    }

                    // Check that quantity is less than or equal to final weight
                    if ($quantity > $final_weight) {
                        // Tell the user
                        $feedback = $this->open_delimiter . 'The Quantity is greater than Final weight.' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddPurchaseView($data);
                        return;

                    }

                    // Add the quantity to the Collection Center where the manager is
                    // assigned to work. To do this, first check if the current user is the
                    // CC Manager or the Super Admin
                    $admin = $this->user_model->getUser($admin_user_id);
                    $collection_center = $this->collectioncenter_model->getCollectionCenter($collection_center_id);
                    //
                    $is_admin = ($admin->type == "admin") ? true : false;
                    $is_cc_manager = ($collection_center->cc_manager_id == $admin->id) ? true : false;
                    $check = ($is_admin || $is_cc_manager);
                    // var_dump($check);
                    if (!($check)) {
                        // Tell the user
                        $feedback = $this->open_delimiter . 'You do not have the permission to purchase commodities for this Collection Center.' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddPurchaseView($data);
                        return;
                    }
                    // Check the current capacity of the collection center and see if
                    // adding this purchase will exceed its maximum capacity
                    $current_capacity = (int) $collection_center->cc_current_capacity;
                    $max_capacity = (int) $collection_center->cc_max_capacity;
                    $total_capacity = $current_capacity + $quantity;

                    if ($total_capacity > $max_capacity) {
                        $diff = $total_capacity - $max_capacity;
                        $feedback = $this->open_delimiter . 'The quantity you want to purchase exceeds the limit of this Collection Center by ' . number_format($diff);
                        $feedback .= '.<br>The Maximum Capacity of ' . ucwords($collection_center->cc_name) . ' Collection Center is ' . number_format($max_capacity) . '.' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddPurchaseView($data);
                        return;
                    }

                    $newPurchaseData = array(
                        'po_farmer_id' => $this->input->post('farmer_id'),
                        'po_collection_center_id' => $collection_center_id,
                        'po_produce_type_id' => $this->input->post('categoryTypeSelect'),
                        'po_produce_id' => $this->input->post('categorySelect'),
                        'po_quantity' => $this->input->post('po_quantity'),
                        'po_original_weight' => $original_weight,
                        'po_purity' => $this->input->post('purity'),
                        'po_moisture_level' => $this->input->post('moisture_level'),
                        'po_foreign_material' => $this->input->post('foreign_material'),
                        'po_damaged_kernel' => $this->input->post('damaged_kernel'),
                        'po_hardness' => $this->input->post('hardness'),
                        'po_color' => $this->input->post('color'),
                        'po_final_weight' => $final_weight,
                        'po_currency_id' => $this->input->post('currencySelect'),
                        'po_amount' => $this->input->post('po_amount'),
                        'po_status' => $this->input->post('paySelect'),
                        'po_slug' => $this->purchase_model->generateSlug($admin_user_id),
                        'po_created_by' => $admin_user_id,
                        'po_date_created' => date('Y-m-d H:i:s'),
                    );

                    // Update this Collection Center current capacity
                    $ccData = array(
                        'cc_current_capacity' => $total_capacity,
                        'cc_date_updated' => date('Y-m-d H:i:s'),
                    );
                    $this->collectioncenter_model->updateCollectionCenter($collection_center->cc_id, $ccData);
                    $this->purchase_model->newPurchase($newPurchaseData);
                    //Show success message
                    $feedback = $this->open_delimiter_success . 'New Purchase has been created!  ' . $this->close_delimiter;
                    $this->session->set_flashdata('purchase_feedback', $feedback);
                    $logData['lg_message'] = "Created New Purchase. "."Slug: ".$newPurchaseData['po_slug'];
                    $this->log_model->newLog($logData);
                    redirect('admin/add-purchase');

                }
            } else {
                $this->loadAddPurchaseView($data);
            }

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadAddPurchaseView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/add_purchase', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/scripts/add_purchase');
        $this->load->view('admin/temp/footer_close');
    }

    public function Purchases()
    {
        if ($this->is_admin || $this->is_cc_manager) {
            $logData = array('lg_user_id' => $this->loggedInUserID);
            $purchases = array();
            $keyword = $this->input->get('keyword_puo');

            $this->load->library('pagination');
            $config = $this->pagination_config;

            // Add the search data in session to populate the fields again
            $this->session->set_userdata('keyword_puo', $keyword);
            $data['keyword_puo'] = $this->session->userdata('keyword_puo');

            // Get the page number if it is set
            $page = $this->uri->segment(3);
            $per_page = 10;
            if (empty($page)) {
                $page = 1;
                $offset = 0;
            } else {
                $page = (int) $page;
                $offset = $page * $per_page;
            }

            if (!empty($keyword)) {
                $purchases = $this->purchase_model->getPurchasesWithName($keyword, $per_page, $offset);
                $config['total_rows'] = $this->purchase_model->getTotalPurchasesWithName($keyword);
                $data['total_purchases'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Purchases Page. "."Page: ".$page.". Searched for [".$keyword."]";

            } else {
                $purchases = $this->purchase_model->getPurchases($per_page, $offset);
                $config['total_rows'] = $this->purchase_model->getTotalPurchases();
                $data['total_purchases'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Purchases Page. "."Page: ".$page;
            }

            if (!empty($purchases)) {
                foreach ($purchases as $purchase) {
                    $manager = $this->user_model->getUser($purchase->po_created_by);
                    $purchase->manager = $manager->full_name;

                    $farmer = $this->farmer_model->getFarmer($purchase->po_farmer_id);
                    if(!empty($farmer)){
                        $purchase->farmer_name = $farmer->fm_full_name;
                    }else{
                        $purchase->farmer_name = "Unknown/Deleted Farmer";
                    }


                    $category = $this->categories_model->getCategoryName($purchase->po_produce_id);
                    $purchase->category = $category;

                    $currency = $this->currency_model->getCurrency($purchase->po_currency_id);
                    $purchase->currency = $currency->cur_symbol;
                }
            }

            $data['purchases'] = $purchases;

            $config['base_url'] = base_url("admin/purchases/");
            $config['per_page'] = $per_page;

            $this->pagination->initialize($config);

            $data['pagination_links'] = $this->pagination->create_links();

            $this->load->view('admin/temp/header');
            $this->load->view('admin/temp/sidebar');
            $this->load->view('admin/all_purchases', $data);
            $this->load->view('admin/temp/footer');
            $this->load->view('admin/temp/scripts/all_purchases');
            $this->load->view('admin/temp/footer_close');

            // Log Data
            $this->log_model->newLog($logData);

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function payPurchase()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $response = array();
        $purchase_id = $this->input->post('purchase_id');

        // Check that the loggedIn Admin can actually update this purchase
        $purchase = $this->purchase_model->getPurchase($purchase_id);
        if (empty($purchase)) {
            $response['status'] = 'error';
            $response['message'] = 'The purchase does not exist.';
            $logData = array('lg_user_id' => $this->loggedInUserID);
            $logData['lg_message'] = "Failed Attempt to set non-existent Purchase to PAID. "."ID: ".$purchase_id;
            $this->log_model->newLog($logData);
            // Send response back to Ajax
            echo json_encode($response);
            return;
        }
        $collection_center_id = $purchase->po_collection_center_id;
        $collection_center = $this->collectioncenter_model->getCollectionCenter($collection_center_id);
        $cc_manager = $collection_center->cc_manager_id;

        if ($this->is_admin || $this->loggedInUserID == $cc_manager) {
            $updateData = array(
                'po_status' => 'paid',
                'po_date_updated' => date('Y-m-d H:i:s'),
                'po_edited_by' => $this->loggedInUserID,
            );

            $this->purchase_model->updatePurchase($purchase_id, $updateData);
            // Get the specific purchase
            $current_purchase = $this->purchase_model->getPurchase($purchase_id);
            $current_status = 'pending';
            $current_status = $current_purchase->po_status;
            $response['status'] = 'success';
            $response['message'] = $current_status;
            // Send response back to Ajax
            echo json_encode($response);
            return;
        } else {
            $response['status'] = 'error';
            $response['message'] = 'You do not have permissions to update this purchase.';
            $logData = array('lg_user_id' => $this->loggedInUserID);
            $logData['lg_message'] = "Failed Attempt to set Purchase to PAID. "."ID: ".$current_purchase->po_id;
            $this->log_model->newLog($logData);
            // Send response back to Ajax
            echo json_encode($response);
            return;
        }
    }

    public function editPurchase()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $purchase_slug = $this->uri->segment(3);

            if (!$this->purchase_model->slug_exists($purchase_slug)) {
                show_404();
            } else {
                $admin_user_id = $this->session->userdata('uid');

                $purchase = $this->purchase_model->getPurchaseBySlug($purchase_slug);
                $farmer = $this->farmer_model->getFarmer($purchase->po_farmer_id);
                $purchase->farmer_name = $farmer->fm_full_name;
                $data['purchase'] = $purchase;
                $data['statuses'] = $this->farmer_model->get_enum_values('farmers', 'fm_status');
                $data['payment_statuses'] = $this->farmer_model->get_enum_values('purchases', 'po_status');
                $data['banks'] = $this->bank_model->getAllBanks();
                $data['currencies'] = $this->currency_model->getAllCurrencies();
                $data['collection_centers'] = $this->collectioncenter_model->getAllCollectionCenters();
                $produce_types = $this->producetype_model->getAllProduceTypes();
                if (empty($produce_types)) {
                    $produce_types = array();
                }
                $data['produce_types'] = $produce_types;

                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
                $this->form_validation->set_rules('farmer_name', 'Farmer Name', 'trim|required');
                $this->form_validation->set_rules('original_weight', 'Original Weight', 'trim|required');
                $this->form_validation->set_rules('final_weight', 'Final Weight', 'trim|required');
                $this->form_validation->set_rules('po_amount', 'Amount', 'trim|required');
                $this->form_validation->set_rules('po_quantity', 'Quantity', 'trim|required');

                if (isset($_POST['submit'])) {

                    if ($this->form_validation->run() == false) {
                        $this->loadEditPurchaseView($data);
                    } else {

                        $collection_center_id = $this->input->post('ccSelect');
                        $farmer_name = $this->input->post('farmer_name');
                        $quantity = (int) $this->input->post('po_quantity');
                        $original_weight = (int) $this->input->post('original_weight');
                        $final_weight = (int) $this->input->post('final_weight');
                        $farmer = $this->farmer_model->getAllActiveFarmersWithName($farmer_name);
                        $pay_status = $this->input->post('paySelect');
                        if (empty($pay_status)) {
                            $pay_status = $purchase->po_status;
                        }

                        if (empty($farmer)) {

                            $feedback = $this->open_delimiter . 'The Farmer is not found on the system .' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadEditPurchaseView($data);
                            return;

                        }

                        // Ensure that quantity is not zero or negative
                        if (($quantity <= 0)) {
                            // Tell the user
                            $feedback = $this->open_delimiter . 'Quantity cannot be zero or negative.' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadEditPurchaseView($data);
                            return;

                        }

                        // Ensure that original weight is not zero or negative
                        if (($original_weight <= 0)) {
                            // Tell the user
                            $feedback = $this->open_delimiter . 'Original weight cannot be zero or negative.' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadEditPurchaseView($data);
                            return;

                        }

                        // Ensure that final weight is not zero or negative
                        if (($final_weight <= 0)) {
                            // Tell the user
                            $feedback = $this->open_delimiter . 'Final weight cannot be zero or negative.' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadEditPurchaseView($data);
                            return;

                        }

                        // Check that final weight is less than or equal to original weight
                        if ($final_weight > $original_weight) {
                            // Tell the user
                            $feedback = $this->open_delimiter . 'The Final weight is greater than Original weight.' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadEditPurchaseView($data);
                            return;

                        }

                        // Check that quantity is less than or equal to final weight
                        if ($quantity > $final_weight) {
                            // Tell the user
                            $feedback = $this->open_delimiter . 'The Quantity is greater than Final weight.' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadEditPurchaseView($data);
                            return;

                        }

                        // Add the quantity to the Collection Center where the manager is
                        // assigned to work. To do this, first check if the current user is the
                        // CC Manager or the Super Admin
                        $admin = $this->user_model->getUser($admin_user_id);
                        $collection_center = $this->collectioncenter_model->getCollectionCenter($collection_center_id);

                        $is_admin = ($admin->type == "admin") ? true : false;
                        $is_cc_manager = ($collection_center->cc_manager_id == $admin->id) ? true : false;
                        $check = ($is_admin || $is_cc_manager);

                        if (!($check)) {
                            // Tell the user
                            $feedback = $this->open_delimiter . 'You do not have the permission to purchase commodities for this Collection Center.' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadEditPurchaseView($data);
                            return;
                        }
                        // Check the current capacity of the collection center and see if
                        // adding this purchase will exceed its maximum capacity
                        $current_capacity = (int) $collection_center->cc_current_capacity;
                        $max_capacity = (int) $collection_center->cc_max_capacity;
                        $total_capacity = ((int) $collection_center->cc_current_capacity - (int) $purchase->po_quantity) + $quantity;

                        if ($total_capacity > $max_capacity) {
                            $diff = $total_capacity - $max_capacity;
                            $feedback = $this->open_delimiter . 'The quantity you want to purchase exceeds the limit of this Collection Center by ' . number_format($diff);
                            $feedback .= '.<br>The Maximum Capacity of ' . ucwords($collection_center->cc_name) . ' Collection Center is ' . number_format($max_capacity) . '.' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadEditPurchaseView($data);
                            return;
                        }

                        $updateData = array(
                            'po_farmer_id' => $this->input->post('farmer_id'),
                            'po_collection_center_id' => $this->input->post('ccSelect'),
                            'po_produce_type_id' => $this->input->post('categoryTypeSelect'),
                            'po_produce_id' => $this->input->post('categorySelect'),
                            'po_quantity' => $this->input->post('po_quantity'),
                            'po_original_weight' => $original_weight,
                            'po_purity' => $this->input->post('purity'),
                            'po_moisture_level' => $this->input->post('moisture_level'),
                            'po_foreign_material' => $this->input->post('foreign_material'),
                            'po_damaged_kernel' => $this->input->post('damaged_kernel'),
                            'po_hardness' => $this->input->post('hardness'),
                            'po_color' => $this->input->post('color'),
                            'po_final_weight' => $final_weight,
                            'po_currency_id' => $this->input->post('currencySelect'),
                            'po_amount' => $this->input->post('po_amount'),
                            'po_status' => $pay_status,
                            'po_edited_by' => $admin_user_id,
                            'po_date_updated' => date('Y-m-d H:i:s'),
                        );

                        // Update this Collection Center current capacity
                        $ccData = array(
                            'cc_current_capacity' => $total_capacity,
                            'cc_date_updated' => date('Y-m-d H:i:s'),
                        );
                        $this->collectioncenter_model->updateCollectionCenter($collection_center->cc_id, $ccData);
                        $this->purchase_model->updatePurchase($purchase->po_id, $updateData);
                        //Show success message
                        $feedback = $this->open_delimiter_success . 'Purchase Data has been updated!  ' . $this->close_delimiter;
                        $this->session->set_flashdata('purchase_feedback', $feedback);
                        // Log data
                        $logData = array('lg_user_id' => $this->loggedInUserID);
                        $logData['lg_message'] = "Edited Purchase. "."Slug: ".$purchase->po_slug;
                        $this->log_model->newLog($logData);
                        redirect('admin/editpurchase/' . $purchase->po_slug);

                    }
                } else {

                    $this->loadEditPurchaseView($data);
                }

            }

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadEditPurchaseView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/edit_purchase', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/scripts/edit_purchase');
        $this->load->view('admin/temp/footer_close');
    }

    public function deletePurchase()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $data = array();
            $purchase_slug = $this->uri->segment(3);

            if (!$this->purchase_model->slug_exists($purchase_slug)) {
                show_404();
            } else {

                $purchase = $this->purchase_model->getPurchaseBySlug($purchase_slug);

                if ($purchase !== false) {
                    // Remove this quantity from the Collection center where it was done
                    $collection_center = $this->collectioncenter_model->getCollectionCenter($purchase->po_collection_center_id);

                    // In the case where Collection center has been deleted, just delete the purchase
                    if(!empty($collection_center)){
                        $new_capacity = (int) $collection_center->cc_current_capacity - (int) $purchase->po_quantity;
                        $this->collectioncenter_model->updateCollectionCenter($collection_center->cc_id, array(
                            'cc_current_capacity' => $new_capacity,
                            'cc_date_updated' => date('Y-m-d H:i:s')));

                    }

                    $this->purchase_model->hardDelete($purchase->po_id);

                    $feedback = $this->open_delimiter_success . 'Purchase - ' . $purchase->po_slug . ' has been successfully deleted!  ' . $this->close_delimiter;
                    $this->session->set_flashdata('purchase_del_feedback', $feedback);

                    // Log data
                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Deleted Purchase. "."Slug: ".$purchase->po_slug;
                    $this->log_model->newLog($logData);

                }

                redirect('admin/purchases');

            }
        }
    }

    public function purchaseDetail()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);

            $data['user'] = $user;

            $purchase_slug = $this->uri->segment(3);
            if (!empty($purchase_slug)) {
                $purchase = $this->purchase_model->getPurchaseBySlug($purchase_slug);
                if (!empty($purchase)) {

                    $manager = $this->user_model->getUser($purchase->po_created_by);
                    $purchase->manager = $manager;

                    $edit_manager = $this->user_model->getUser($purchase->po_edited_by);
                    $purchase->edit_manager = $edit_manager;

                    $farmer = $this->farmer_model->getFarmer($purchase->po_farmer_id);
                    $purchase->farmer_name = $farmer->fm_full_name;

                    $category = $this->categories_model->getCategoryName($purchase->po_produce_id);
                    $purchase->category = $category;

                    $currency = $this->currency_model->getCurrency($purchase->po_currency_id);
                    $purchase->currency = $currency->cur_symbol;

                    $data['purchase'] = $purchase;

                    $this->load->view('admin/temp/header');
                    $this->load->view('admin/temp/sidebar');
                    $this->load->view('admin/purchase_details', $data);
                    $this->load->view('admin/temp/footer');

                    // Log data
                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Viewed Purchase Details. "."Slug: ".$purchase->po_slug;
                    $this->log_model->newLog($logData);

                } else {
                    show_404();
                }
            } else {
                show_404();
            }
        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function printPurchase()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);

            $data['user'] = $user;

            $purchase_slug = $this->uri->segment(3);
            if (!empty($purchase_slug)) {
                $purchase = $this->purchase_model->getPurchaseBySlug($purchase_slug);
                if (!empty($purchase)) {

                    $manager = $this->user_model->getUser($purchase->po_created_by);
                    $purchase->manager = $manager;

                    $edit_manager = $this->user_model->getUser($purchase->po_edited_by);
                    $purchase->edit_manager = $edit_manager;

                    $farmer = $this->farmer_model->getFarmer($purchase->po_farmer_id);
                    $purchase->farmer_name = $farmer->fm_full_name;

                    $category = $this->categories_model->getCategoryName($purchase->po_produce_id);
                    $purchase->category = $category;

                    $currency = $this->currency_model->getCurrency($purchase->po_currency_id);
                    $purchase->currency = $currency->cur_symbol;

                    $data['purchase'] = $purchase;

                    $purchase_receipt = $this->load->view('admin/temp/header', array(), true);
                    $purchase_receipt .= $this->load->view('admin/temp/sidebar', array(), true);
                    $purchase_receipt .= $this->load->view('admin/purchase_details', $data, true);
                    $purchase_receipt .= $this->load->view('admin/temp/footer', array(), true);
                    // Log data
                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Printed Purchase Details. "."Slug: ".$purchase->po_slug;
                    $this->log_model->newLog($logData);
                    // $this->mpdf->WriteHTML($this->load->view('admin/temp/header_2',array(),true));
                    // $this->mpdf->WriteHTML($this->load->view('admin/temp/sidebar',array(),true));
                    $this->mpdf->WriteHTML($this->load->view('admin/purchase_details', $data, true));
                    // $this->mpdf->WriteHTML($this->load->view('admin/temp/footer',array(), true));
                    $this->mpdf->Output();

                } else {
                    show_404();
                }
            } else {
                show_404();
            }
        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }

    }

    public function addInvestment()
    {
        if ($this->is_admin) {

            $admin_user_id = $this->session->userdata('uid');
            $data['commodities'] = $this->commodity_model->getCommoditiesByType(150, 0, 'trade', 'comm_id', 'ASC');
            $data['payment_statuses'] = $this->investment_model->get_enum_values('investments', 'iv_pay_status');
            $data['currencies'] = $this->currency_model->getAllCurrencies();
            $produce_types = $this->producetype_model->getAllProduceTypes();
            if (empty($produce_types)) {
                $produce_types = array();
            }
            $data['produce_types'] = $produce_types;

            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
            $this->form_validation->set_rules('investor_name', 'Investor Name', 'trim|required');
            $this->form_validation->set_rules('iv_quantity', 'Quantity', 'trim|required');
            $this->form_validation->set_rules('iv_amount', 'Amount', 'trim|required');
            $this->form_validation->set_rules('iv_returns', 'Returns Expected', 'trim|required');
            $this->form_validation->set_rules('iv_due_date', 'Due Date', 'trim|required');

            if (isset($_POST['submit'])) {

                if ($this->form_validation->run() == false) {
                    $this->loadAddInvestmentView($data);
                } else {

                    $investor_id = $this->input->post('investor_id');
                    $quantity = (int) $this->input->post('iv_quantity');
                    $amount = (int) $this->input->post('iv_amount');
                    $returns = (int) $this->input->post('iv_returns');
                    $investor = $this->user_model->getUser($investor_id);
                    $commodity_id = $this->input->post('commoditySelect');
                    $commodity = $this->commodity_model->getCommodity($commodity_id);
                    // var_dump($this->input->post());
                    // die();
                    if (empty($investor)) {
                        $feedback = $this->open_delimiter . 'The Investor is not found on the system .' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddInvestmentView($data);
                        return;

                    }

                    // Ensure that quantity is not zero or negative
                    if (($quantity <= 0)) {
                        // Tell the user
                        $feedback = $this->open_delimiter . 'Quantity cannot be zero or negative.' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddInvestmentView($data);
                        return;

                    }

                    // Ensure that amount is not zero or negative
                    if (($amount <= 0)) {
                        // Tell the user
                        $feedback = $this->open_delimiter . 'Amount cannot be zero or negative.' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddInvestmentView($data);
                        return;

                    }

                    // Ensure that returns is not zero or negative
                    if (($returns <= 0)) {
                        // Tell the user
                        $feedback = $this->open_delimiter . 'Returns Expected cannot be zero or negative.' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddInvestmentView($data);
                        return;

                    }

                    $newInvestmentData = array(
                        'iv_user_id' => $investor_id,
                        'iv_commodity_id' => $this->input->post('commoditySelect'),
                        'iv_commodity_name' => $commodity->comm_name,
                        'iv_quantity' => $quantity,
                        'iv_currency_id' => $this->input->post('currencySelect'),
                        'iv_amount' => $amount,
                        'iv_pay_status' => $this->input->post('paySelect'),
                        'iv_returns' => $returns,
                        'iv_returns_status' => $this->input->post('returnSelect'),
                        'iv_due_date' => $this->input->post('iv_due_date'),
                        'iv_slug' => $this->investment_model->generateSlug($admin_user_id),
                        'iv_created_by' => $admin_user_id,
                        'iv_date_created' => date('Y-m-d H:i:s'),
                    );

                    $this->investment_model->newInvestment($newInvestmentData);
                    //Show success message
                    $feedback = $this->open_delimiter_success . 'New Investment has been made!  ' . $this->close_delimiter;
                    $this->session->set_flashdata('investment_feedback', $feedback);
                    
                    // Log data
                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Created New Investment. "."Slug: ".$newInvestmentData['iv_slug'];
                    $this->log_model->newLog($logData);
                    redirect('admin/add-investment');

                }
            } else {
                $this->loadAddInvestmentView($data);
            }

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadAddInvestmentView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/add_investment', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/scripts/add_investment');
        $this->load->view('admin/temp/footer_close');
    }

    public function getInvestors()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $response = array();
        $investor_id = $this->input->post('investor_name');
        $logData = array('lg_user_id' => $this->loggedInUserID);

        // Get Investors
        $investors = $this->user_model->getInvestorsWithName($investor_id);
        if (empty($investors)) {
            $investors = array();
        }

        $response = array("" => 1, "investors" => $investors);
        // Log data
        $logData['lg_message'] = "Ajax Fetch of Investors. ";
        $this->log_model->newLog($logData);
        echo json_encode($response);
    }

    public function Investments()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $logData = array('lg_user_id' => $this->loggedInUserID);

            $investments = array();
            $keyword = $this->input->get('keyword_iv');

            $this->load->library('pagination');
            $config = $this->pagination_config;

            // Add the search data in session to populate the fields again
            $this->session->set_userdata('keyword_iv', $keyword);
            $data['keyword_iv'] = $this->session->userdata('keyword_iv');

            // Get the page number if it is set
            $page = $this->uri->segment(3);
            $per_page = 10;
            if (empty($page)) {
                $page = 1;
                $offset = 0;
            } else {
                $page = (int) $page;
                $offset = $page * $per_page;
            }

            if (!empty($keyword)) {
                $investments = $this->investment_model->getInvestmentsWithName($keyword, $per_page, $offset);
                $config['total_rows'] = $this->investment_model->getTotalInvestmentsWithName($keyword);
                $data['total_investments'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Investment Page. "."Page: ".$page.". Searched for [".$keyword."]";

            } else {
                $investments = $this->investment_model->getInvestments($per_page, $offset);
                $config['total_rows'] = $this->investment_model->getTotalInvestments();
                $data['total_investments'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Investment Page. "."Page: ".$page;
            }

            if (!empty($investments)) {
                foreach ($investments as $investment) {
                    $manager = $this->user_model->getUser($investment->iv_created_by);
                    if(!empty($manager)){
                        $investment->manager = $manager->full_name;
                    }else{
                        $investment->manager = "_";
                    }
                    

                    $investor = $this->user_model->getUser($investment->iv_user_id);
                    if(!empty($investor)){
                        $investment->investor_name = $investor->full_name;
                    }else{
                        $investment->investor_name = "_";
                    }
                    

                    $currency = $this->currency_model->getCurrency($investment->iv_currency_id);
                    $investment->currency = $currency->cur_symbol;
                }
            }

            $data['investments'] = $investments;

            $config['base_url'] = base_url("admin/investments/");
            $config['per_page'] = $per_page;

            $this->pagination->initialize($config);

            $data['pagination_links'] = $this->pagination->create_links();

            $this->load->view('admin/temp/header');
            $this->load->view('admin/temp/sidebar');
            $this->load->view('admin/all_investments', $data);
            $this->load->view('admin/temp/footer');
            $this->load->view('admin/temp/scripts/all_investments');
            $this->load->view('admin/temp/footer_close');

            // Log data
            $this->log_model->newLog($logData);

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function payInvestment()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $response = array();
        $investment_id = $this->input->post('investment_id');

        // Check that the loggedIn Admin can actually update this investment
        $investment = $this->investment_model->getInvestment($investment_id);
        if (empty($investment)) {
            $response['status'] = 'error';
            $response['message'] = 'The investment does not exist.';
            $logData = array('lg_user_id' => $this->loggedInUserID);
            $logData['lg_message'] = "Failed Attempt to set non-existent Investment Returns to PAID. "."ID: ".$investment_id;
            $this->log_model->newLog($logData);
            // Send response back to Ajax
            echo json_encode($response);
            return;
        }

        if ($this->is_admin) {
            $updateData = array(
                'iv_returns_status' => 'paid',
                'iv_date_updated' => date('Y-m-d H:i:s'),
                'iv_edited_by' => $this->loggedInUserID,
            );

            $this->investment_model->updateInvestment($investment_id, $updateData);
            // Get the specific investment
            $current_investment = $this->investment_model->getInvestment($investment_id);
            $current_status = 'pending';
            $current_status = $current_investment->iv_returns_status;
            $response['status'] = 'success';
            $response['message'] = $current_status;
            // Log data
            $logData = array('lg_user_id' => $this->loggedInUserID);
            $logData['lg_message'] = "Set Investment Returns to PAID. "."Slug: ".$current_investment->iv_slug;
            $this->log_model->newLog($logData); 
            // Send response back to Ajax
            echo json_encode($response);
            return;
        } else {
            $response['status'] = 'error';
            $response['message'] = 'You do not have permissions to update this investment.';
            $logData = array('lg_user_id' => $this->loggedInUserID);
            $logData['lg_message'] = "Failed Attempt to set Investment Returns to PAID. "."Slug: ".$investment->iv_slug;
            $this->log_model->newLog($logData); 
            // Send response back to Ajax
            echo json_encode($response);
            return;
        }
    }

    public function investorPaid()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $response = array();
        $investment_id = $this->input->post('investment_id');

        // Check that the loggedIn Admin can actually update this investment
        $investment = $this->investment_model->getInvestment($investment_id);
        if (empty($investment)) {
            $response['status'] = 'error';
            $response['message'] = 'The investment does not exist.';
            $logData = array('lg_user_id' => $this->loggedInUserID);
            $logData['lg_message'] = "Failed Attempt to set non-existent Investment to PAID. "."ID: ".$investment_id;
            $this->log_model->newLog($logData); 
            // Send response back to Ajax
            echo json_encode($response);
            return;
        }

        if ($this->is_admin) {
            $updateData = array(
                'iv_pay_status' => 'paid',
                'iv_date_updated' => date('Y-m-d H:i:s'),
                'iv_edited_by' => $this->loggedInUserID,
            );

            $this->investment_model->updateInvestment($investment_id, $updateData);
            // Get the specific investment
            $current_investment = $this->investment_model->getInvestment($investment_id);
            $current_status = 'pending';
            $current_status = $current_investment->iv_pay_status;
            $response['status'] = 'success';
            $response['message'] = $current_status;
            // Log data
            $logData = array('lg_user_id' => $this->loggedInUserID);
            $logData['lg_message'] = "Set Investment to PAID. "."Slug: ".$current_investment->iv_slug;
            $this->log_model->newLog($logData);           
            // Send response back to Ajax
            echo json_encode($response);
            return;
        } else {
            $response['status'] = 'error';
            $response['message'] = 'You do not have permissions to update this investment.';
            $logData = array('lg_user_id' => $this->loggedInUserID);
            $logData['lg_message'] = "Failed Attempt to set Investment to PAID. "."Slug: ".$investment->iv_slug;
            $this->log_model->newLog($logData); 
            // Send response back to Ajax
            echo json_encode($response);
            return;
        }
    }

    public function editInvestment()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $investment_slug = $this->uri->segment(3);

            if (!$this->investment_model->slug_exists($investment_slug)) {
                show_404();
            } else {

                $investment = $this->investment_model->getInvestmentBySlug($investment_slug);
                $investor = $this->user_model->getUser($investment->iv_user_id);
                $investment->investor_name = $investor->full_name;
                $data['investment'] = $investment;
                $data['commodities'] = $this->commodity_model->getCommoditiesByType(150, 0, 'trade', 'comm_id', 'ASC');
                $data['payment_statuses'] = $this->investment_model->get_enum_values('investments', 'iv_pay_status');
                $data['currencies'] = $this->currency_model->getAllCurrencies();
                $produce_types = $this->producetype_model->getAllProduceTypes();
                if (empty($produce_types)) {
                    $produce_types = array();
                }
                $data['produce_types'] = $produce_types;

                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
                $this->form_validation->set_rules('investor_name', 'Investor Name', 'trim|required');
                $this->form_validation->set_rules('iv_quantity', 'Quantity', 'trim|required');
                $this->form_validation->set_rules('iv_amount', 'Amount', 'trim|required');
                $this->form_validation->set_rules('iv_returns', 'Returns Expected', 'trim|required');
                $this->form_validation->set_rules('iv_due_date', 'Due Date', 'trim|required');

                if (isset($_POST['submit'])) {

                    if ($this->form_validation->run() == false) {
                        $this->loadEditInvestmentView($data);
                    } else {

                        $investor_id = $this->input->post('investor_id');
                        $quantity = (int) $this->input->post('iv_quantity');
                        $amount = (int) $this->input->post('iv_amount');
                        $returns = (int) $this->input->post('iv_returns');
                        $investor = $this->user_model->getUser($investor_id);
                        $pay_status = $this->input->post('paySelect');
                        if (empty($pay_status)) {
                            $pay_status = $investment->iv_pay_status;
                        }
                        $return_status = $this->input->post('returnSelect');
                        if (empty($return_status)) {
                            $return_status = $investment->iv_returns_status;
                        }

                        if (empty($investor)) {
                            $feedback = $this->open_delimiter . 'The Investor is not found on the system .' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadEditInvestmentView($data);
                            return;
    
                        }
    
                        // Ensure that quantity is not zero or negative
                        if (($quantity <= 0)) {
                            // Tell the user
                            $feedback = $this->open_delimiter . 'Quantity cannot be zero or negative.' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadEditInvestmentView($data);
                            return;
    
                        }
    
                        // Ensure that amount is not zero or negative
                        if (($amount <= 0)) {
                            // Tell the user
                            $feedback = $this->open_delimiter . 'Amount cannot be zero or negative.' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadEditInvestmentView($data);
                            return;
    
                        }
    
                        // Ensure that returns is not zero or negative
                        if (($returns <= 0)) {
                            // Tell the user
                            $feedback = $this->open_delimiter . 'Returns Expected cannot be zero or negative.' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadEditInvestmentView($data);
                            return;
    
                        }
    
                        $updateData = array(
                            'iv_user_id' => $investor_id,
                            'iv_quantity' => $quantity,
                            'iv_currency_id' => $this->input->post('currencySelect'),
                            'iv_amount' => $amount,
                            'iv_pay_status' => $pay_status,
                            'iv_returns' => $returns,
                            'iv_returns_status' => $return_status,
                            'iv_due_date' => $this->input->post('iv_due_date'),
                            'iv_edited_by' => $this->loggedInUserID,
                            'iv_date_created' => date('Y-m-d H:i:s'),
                        );
    
                        $this->investment_model->updateInvestment($investment->iv_id, $updateData);
                        //Show success message
                        $feedback = $this->open_delimiter_success . 'Investment has been updated!  ' . $this->close_delimiter;
                        $this->session->set_flashdata('investment_feedback', $feedback);
                        // Log data
                        $logData = array('lg_user_id' => $this->loggedInUserID);
                        $logData['lg_message'] = "Edited Investment. "."Slug: ".$investment->iv_slug;
                        $this->log_model->newLog($logData);
                        redirect('admin/edit-investment/'. $investment->iv_slug);
                    }
                } else {

                    $this->loadEditInvestmentView($data);
                }

            }

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadEditInvestmentView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/edit_investment', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/scripts/edit_investment');
        $this->load->view('admin/temp/footer_close');
    }

    public function deleteInvestment()
    {
        if ($this->is_admin) {

            $data = array();
            $investment_slug = $this->uri->segment(3);

            if (!$this->investment_model->slug_exists($investment_slug)) {
                show_404();
            } else {

                $investment = $this->investment_model->getInvestmentBySlug($investment_slug);

                if ($investment !== false) {
                    $this->investment_model->hardDelete($investment->iv_id);

                    $feedback = $this->open_delimiter_success . 'Investment - ' . $investment->iv_slug . ' has been successfully deleted!  ' . $this->close_delimiter;
                    $this->session->set_flashdata('investment_del_feedback', $feedback);

                    // Log data
                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Deleted Investment. "."Slug: ".$investment->iv_slug;
                    $this->log_model->newLog($logData);

                }

                redirect('admin/investments');

            }
        }
    }

    public function investmentDetail()
    {
        if ($this->is_admin) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);

            $data['user'] = $user;

            $investment_slug = $this->uri->segment(3);
            if (!empty($investment_slug)) {
                $investment = $this->investment_model->getInvestmentBySlug($investment_slug);
                if (!empty($investment)) {

                    $manager = $this->user_model->getUser($investment->iv_created_by);
                    $investment->manager = $manager;

                    $edit_manager = $this->user_model->getUser($investment->iv_edited_by);
                    $investment->manager = $edit_manager;

                    $investor = $this->user_model->getUser($investment->iv_user_id);
                    $investment->investor_name = $investor->full_name;
                    $investment->investor_email = $investor->email;
                    $investment->investor_phone = $investor->phone;
                    $investment->investor_address = $investor->address;


                    $category = $this->categories_model->getCategoryName($investment->iv_produce_id);
                    $investment->category = $category;

                    $currency = $this->currency_model->getCurrency($investment->iv_currency_id);
                    $investment->currency = $currency->cur_symbol;

                    $data['investment'] = $investment;

                    $this->load->view('admin/temp/header');
                    $this->load->view('admin/temp/sidebar');
                    $this->load->view('admin/investment_details', $data);
                    $this->load->view('admin/temp/footer');

                    // Log data
                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Viewed Investment Details. "."Slug: ".$investment->iv_slug;
                    $this->log_model->newLog($logData);

                } else {
                    show_404();
                }
            } else {
                show_404();
            }
        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function printInvestment()
    {
        if ($this->is_admin) {

            $investment_slug = $this->uri->segment(3);
            if (!empty($investment_slug)) {
                $investment = $this->investment_model->getInvestmentBySlug($investment_slug);
                if (!empty($investment)) {

                    $manager = $this->user_model->getUser($investment->iv_created_by);
                    $investment->manager = $manager;

                    $edit_manager = $this->user_model->getUser($investment->iv_edited_by);
                    $investment->manager = $edit_manager;

                    $investor = $this->user_model->getUser($investment->iv_user_id);
                    $investment->investor_name = $investor->full_name;

                    $category = $this->categories_model->getCategoryName($investment->iv_produce_id);
                    $investment->category = $category;

                    $currency = $this->currency_model->getCurrency($investment->iv_currency_id);
                    $investment->currency = $currency->cur_symbol;

                    $data['investment'] = $investment;

                    $investment_receipt = $this->load->view('admin/temp/header', array(), true);
                    $investment_receipt .= $this->load->view('admin/temp/sidebar', array(), true);
                    $investment_receipt .= $this->load->view('admin/investment_details', $data, true);
                    $investment_receipt .= $this->load->view('admin/temp/footer', array(), true);

                    // Log data
                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Printed an Investment Detail. "."Slug: ".$investment->iv_slug;
                    $this->log_model->newLog($logData);

                    // $this->mpdf->WriteHTML($this->load->view('admin/temp/header_2',array(),true));
                    // $this->mpdf->WriteHTML($this->load->view('admin/temp/sidebar',array(),true));
                    $this->mpdf->WriteHTML($this->load->view('admin/investment_details', $data, true));
                    // $this->mpdf->WriteHTML($this->load->view('admin/temp/footer',array(), true));
                    $this->mpdf->Output();

                } else {
                    show_404();
                }
            } else {
                show_404();
            }
        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }

    }

    public function CPurchases()
    {
        if ($this->is_admin) {

            $logData = array('lg_user_id' => $this->loggedInUserID);

            $cpurchases = array();
            $keyword = $this->input->get('keyword_cp');

            $this->load->library('pagination');
            $config = $this->pagination_config;

            // Add the search data in session to populate the fields again
            $this->session->set_userdata('keyword_cp', $keyword);
            $data['keyword_cp'] = $this->session->userdata('keyword_cp');

            // Get the page number if it is set
            $page = $this->uri->segment(3);
            $per_page = 10;
            if (empty($page)) {
                $page = 1;
                $offset = 0;
            } else {
                $page = (int) $page;
                $offset = $page * $per_page;
            }

            if (!empty($keyword)) {
                $cpurchases = $this->cpurchases_model->getCPurchasesWithName($keyword, $per_page, $offset);
                $config['total_rows'] = $this->cpurchases_model->getTotalCPurchasesWithName($keyword);
                $data['total_cpurchases'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed CPurchases Page. "."Page: ".$page.". Searched for [".$keyword."]";

            } else {
                $cpurchases = $this->cpurchases_model->getCPurchases($per_page, $offset, 'cusp_id', 'DESC');
                $config['total_rows'] = $this->cpurchases_model->getTotalCPurchases();
                $data['total_cpurchases'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed CPurchases Page. "."Page: ".$page;
            }

            if (!empty($cpurchases)) {
                foreach ($cpurchases as $cpurchase) {
                    $manager = $this->user_model->getUser($cpurchase->cusp_user_id);
                    $cpurchase->manager = $manager->full_name;

                    $investor = $this->user_model->getUser($cpurchase->cusp_user_id);
                    $cpurchase->investor_name = $investor->full_name;

                    $commodity = $this->commodity_model->getCommodity($cpurchase->cusp_commodity_id);
                    if(!empty($commodity)){
                        $cpurchase->commodity_name = $commodity->comm_name;
                    }else{
                        $cpurchase->commodity_name = 'Unavailable';
                    }
                    
                }
            }

            $data['cpurchases'] = $cpurchases;

            $config['base_url'] = base_url("admin/cpurchases/");
            $config['per_page'] = $per_page;

            $this->pagination->initialize($config);

            $data['pagination_links'] = $this->pagination->create_links();

            $this->load->view('admin/temp/header');
            $this->load->view('admin/temp/sidebar');
            $this->load->view('admin/all_cpurchases', $data);
            $this->load->view('admin/temp/footer');
            $this->load->view('admin/temp/scripts/all_cpurchases');
            $this->load->view('admin/temp/footer_close');

            // Log data
            $this->log_model->newLog($logData);

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function buyerPaid()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $response = array();
        $cpurchase_id = $this->input->post('cpurchase_id');

        // Check that the loggedIn Admin can actually update this cpurchase
        $cpurchase = $this->cpurchases_model->getCPurchase($cpurchase_id);
        if (empty($cpurchase)) {
            $response['status'] = 'error';
            $response['message'] = 'The customer purchase does not exist.';
            $logData = array('lg_user_id' => $this->loggedInUserID);
            $logData['lg_message'] = "Failed Attempt to set non-existent Investment Returns to PAID. "."ID: ".$investment_id;
            $this->log_model->newLog($logData);
            // Send response back to Ajax
            echo json_encode($response);
            return;
        }

        if ($this->is_admin) {
            $updateData = array(
                'cusp_pay_status' => 'paid',
                'cusp_date_updated' => date('Y-m-d H:i:s'),
                'cusp_edited_by' => $this->loggedInUserID,
            );

            $this->cpurchases_model->updateCPurchase($cpurchase_id, $updateData);
            // Get the specific investment
            $current_cpurchase = $this->cpurchases_model->getCPurchase($cpurchase_id);
            // $current_status = 'paid';
            $current_status = $current_cpurchase->cusp_pay_status;
            $response['status'] = 'success';
            $response['message'] = $current_status;
            // Log data
            $logData = array('lg_user_id' => $this->loggedInUserID);
            $logData['lg_message'] = "Set Customer Purchase to PAID. "."Slug: ".$cpurchase->cusp_slug;
            $this->log_model->newLog($logData); 
            // Send response back to Ajax
            echo json_encode($response);
            return;
        } else {
            $response['status'] = 'error';
            $response['message'] = 'You do not have permissions to update this customer purchase.';
            $logData = array('lg_user_id' => $this->loggedInUserID);
            $logData['lg_message'] = "Failed Attempt to set Customer Purchase  to PAID. "."Slug: ".$cpurchase->cusp_slug;
            $this->log_model->newLog($logData); 
            // Send response back to Ajax
            echo json_encode($response);
            return;
        }
    }

    public function purchaseDeliveryStatus()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $response = array();
        $cpurchase_id = $this->input->post('cpurchase_id');

        // Check that the loggedIn Admin can actually update this cpurchase
        $cpurchase = $this->cpurchases_model->getCPurchase($cpurchase_id);
        if (empty($cpurchase)) {
            $response['status'] = 'error';
            $response['message'] = 'The customer purchase does not exist.';
            $logData = array('lg_user_id' => $this->loggedInUserID);
            $logData['lg_message'] = "Failed Attempt to set non-existent Investment Returns to PAID. "."ID: ".$investment_id;
            $this->log_model->newLog($logData);
            // Send response back to Ajax
            echo json_encode($response);
            return;
        }

        if ($this->is_admin) {
            $updateData = array(
                'cusp_delivery_status' => 'delivered',
                'cusp_date_updated' => date('Y-m-d H:i:s'),
                'cusp_edited_by' => $this->loggedInUserID,
            );

            $this->cpurchases_model->updateCPurchase($cpurchase_id, $updateData);
            // Get the specific CPurchase
            $current_cpurchase = $this->cpurchases_model->getCPurchase($cpurchase_id);
            $current_status = 'pending';
            $current_status = $current_cpurchase->cusp_delivery_status;
            $response['status'] = 'success';
            $response['message'] = $current_status;
            // Log data
            $logData = array('lg_user_id' => $this->loggedInUserID);
            $logData['lg_message'] = "Set Customer Purchase to DELIVER. "."Slug: ".$cpurchase->cusp_slug;
            $this->log_model->newLog($logData); 
            // Send response back to Ajax
            echo json_encode($response);
            return;
        } else {
            $response['status'] = 'error';
            $response['message'] = 'You do not have permissions to update this customer purchase.';
            $logData = array('lg_user_id' => $this->loggedInUserID);
            $logData['lg_message'] = "Failed Attempt to set Customer Purchase  to PAID. "."Slug: ".$cpurchase->cusp_slug;
            $this->log_model->newLog($logData); 
            // Send response back to Ajax
            echo json_encode($response);
            return;
        }
    }

    public function editCPurchase()
    {
        if ($this->is_admin) {

            $cpurchase_slug = $this->uri->segment(3);

            if (!$this->cpurchases_model->slug_exists($cpurchase_slug)) {
                show_404();
            } else {

                $cpurchase = $this->cpurchases_model->getCPurchaseBySlug($cpurchase_slug);
                $investor = $this->user_model->getUser($cpurchase->cusp_user_id);
                $cpurchase->investor_name = $investor->full_name;
                $data['cpurchase'] = $cpurchase;
                $data['commodities'] = $this->commodity_model->getCommoditiesByType(150, 0, 'purchase', 'comm_id', 'ASC');
                $data['payment_statuses'] = $this->cpurchases_model->get_enum_values('customer_purchases', 'cusp_pay_status');
                $data['delivery_statuses'] = $this->cpurchases_model->get_enum_values('customer_purchases', 'cusp_delivery_status');
                $data['currencies'] = $this->currency_model->getAllCurrencies();
                $produce_types = $this->producetype_model->getAllProduceTypes();
                if (empty($produce_types)) {
                    $produce_types = array();
                }
                $data['produce_types'] = $produce_types;

                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
                $this->form_validation->set_rules('investor_name', 'Investor Name', 'trim|required');
                $this->form_validation->set_rules('cusp_quantity', 'Quantity', 'trim|required');
                $this->form_validation->set_rules('cusp_amount', 'Amount', 'trim|required');

                if (isset($_POST['submit'])) {

                    if ($this->form_validation->run() == false) {
                        $this->loadEditCPurchaseView($data);
                    } else {

                        $investor_id = $this->input->post('investor_id');
                        $quantity = (int) $this->input->post('cusp_quantity');
                        $amount = (int) $this->input->post('cusp_amount');
                        $commodity_id = $this->input->post('commoditySelect');

                        $investor = $this->user_model->getUser($investor_id);
                        $pay_status = $this->input->post('paySelect');
                        if (empty($pay_status)) {
                            $pay_status = $cpurchase->cusp_pay_status;
                        }
                        $delivery_status = $this->input->post('deliverySelect');
                        if (empty($delivery_status)) {
                            $delivery_status = $cpurchase->cusp_delivery_status;
                        }

                        if (empty($investor)) {
                            $feedback = $this->open_delimiter . 'The Investor is not found on the system .' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadEditCPurchaseView($data);
                            return;
    
                        }
    
                        // Ensure that quantity is not zero or negative
                        if (($quantity <= 0)) {
                            // Tell the user
                            $feedback = $this->open_delimiter . 'Quantity cannot be zero or negative.' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadEditCPurchaseView($data);
                            return;
    
                        }
    
                        // Ensure that amount is not zero or negative
                        if (($amount <= 0)) {
                            // Tell the user
                            $feedback = $this->open_delimiter . 'Amount cannot be zero or negative.' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadEditCPurchaseView($data);
                            return;
    
                        }
    
                        $updateData = array(
                            'cusp_user_id' => $investor_id,
                            'cusp_quantity' => $quantity,
                            'cusp_currency_id' => $this->input->post('currencySelect'),
                            'cusp_amount' => $amount,
                            'cusp_pay_status' => $pay_status,
                            'cusp_delivery_status' => $delivery_status,
                            'cusp_edited_by' => $this->loggedInUserID,
                            'cusp_date_updated' => date('Y-m-d H:i:s'),
                        );
    
                        $this->cpurchases_model->updateCPurchase($cpurchase->cusp_id, $updateData);
                        //Show success message
                        $feedback = $this->open_delimiter_success . 'Customer Purchase has been updated!  ' . $this->close_delimiter;
                        $this->session->set_flashdata('cpurchase_feedback', $feedback);
                        // Log data
                        $logData = array('lg_user_id' => $this->loggedInUserID);
                        $logData['lg_message'] = "Edited Customer Purchase. "."Slug: ".$cpurchase->cusp_slug;
                        $this->log_model->newLog($logData);
                        redirect('admin/edit-cpurchase/'. $cpurchase->cusp_slug);
                    }
                } else {

                    $this->loadEditCPurchaseView($data);
                }

            }

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadEditCPurchaseView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/edit_cpurchase', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/scripts/edit_cpurchase');
        $this->load->view('admin/temp/footer_close');
    }

    public function addCPurchase()
    {
        if ($this->is_admin) {
                
                $data['commodities'] = $this->commodity_model->getCommoditiesByType(150, 0, 'purchase', 'comm_id', 'ASC');
                $data['payment_statuses'] = $this->cpurchases_model->get_enum_values('customer_purchases', 'cusp_pay_status');
                $data['delivery_statuses'] = $this->cpurchases_model->get_enum_values('customer_purchases', 'cusp_delivery_status');
                $data['currencies'] = $this->currency_model->getAllCurrencies();
                $produce_types = $this->producetype_model->getAllProduceTypes();
                if (empty($produce_types)) {
                    $produce_types = array();
                }
                $data['produce_types'] = $produce_types;

                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
                $this->form_validation->set_rules('investor_name', 'Investor Name', 'trim|required');
                $this->form_validation->set_rules('cusp_quantity', 'Quantity', 'trim|required');
                $this->form_validation->set_rules('cusp_amount', 'Amount', 'trim|required');

                if (isset($_POST['submit'])) {

                    if ($this->form_validation->run() == false) {
                        $this->loadAddCPurchaseView($data);
                    } else {

                        $investor_id = $this->input->post('investor_id');
                        $quantity = (int) $this->input->post('cusp_quantity');
                        $amount = (int) $this->input->post('cusp_amount');
                        $commodity_id = $this->input->post('commoditySelect');
                        $commodity = $this->commodity_model->getCommodity($commodity_id);
                        $investor = $this->user_model->getUser($investor_id);
                        $pay_status = $this->input->post('paySelect');
                        $delivery_status = $this->input->post('deliverySelect');
                        if (empty($delivery_status)) {
                            $delivery_status = $cpurchase->cusp_delivery_status;
                        }

                        if (empty($investor)) {
                            $feedback = $this->open_delimiter . 'The Investor is not found on the system .' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadAddCPurchaseView($data);
                            return;
    
                        }
    
                        // Ensure that quantity is not zero or negative
                        if (($quantity <= 0)) {
                            // Tell the user
                            $feedback = $this->open_delimiter . 'Quantity cannot be zero or negative.' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadAddCPurchaseView($data);
                            return;
    
                        }
    
                        // Ensure that amount is not zero or negative
                        if (($amount <= 0)) {
                            // Tell the user
                            $feedback = $this->open_delimiter . 'Amount cannot be zero or negative.' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadAddCPurchaseView($data);
                            return;
    
                        }
    
                        $newData = array(
                            'cusp_user_id' => $investor_id,
                            'cusp_slug' => $this->cpurchases_model->generateCPurchaseSlug($this->loggedInUserID, $investor_id),
                            'cusp_commodity_id' => $commodity_id,
                            'cusp_commodity_name' => $commodity->comm_name,
                            'cusp_quantity' => $quantity,
                            'cusp_currency_id' => $this->input->post('currencySelect'),
                            'cusp_amount' => $amount,
                            'cusp_pay_status' => $pay_status,
                            'cusp_delivery_status' => $delivery_status,
                            'cusp_edited_by' => $this->loggedInUserID,
                            'cusp_date_created' => date('Y-m-d H:i:s'),
                            'cusp_date_updated' => date('Y-m-d H:i:s'),
                        );
    
                        $this->cpurchases_model->newCPurchase($newData);
                        //Show success message
                        $feedback = $this->open_delimiter_success . 'Customer Purchase has been created!  ' . $this->close_delimiter;
                        $this->session->set_flashdata('cpurchase_feedback', $feedback);
                        // Log data
                        $logData = array('lg_user_id' => $this->loggedInUserID);
                        $logData['lg_message'] = "New Customer Purchase. "."Slug: ".$newData['cusp_slug'];
                        $this->log_model->newLog($logData);
                        redirect('admin/add-cpurchase/');
                    }
                } else {

                    $this->loadAddCPurchaseView($data);
                }

            } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadAddCPurchaseView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/add_cpurchase', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/scripts/add_cpurchase');
        $this->load->view('admin/temp/footer_close');
    }

    public function CPurchaseDetail()
    {
        if ($this->is_admin) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);

            $data['user'] = $user;

            $cpurchase_slug = $this->uri->segment(3);
            if (!empty($cpurchase_slug)) {
                $cpurchase = $this->cpurchases_model->getCPurchaseBySlug($cpurchase_slug);
                if (!empty($cpurchase)) {

                    $manager = $this->user_model->getUser($cpurchase->cusp_user_id);
                    $cpurchase->manager = $manager;

                    $investor = $this->user_model->getUser($cpurchase->cusp_user_id);
                    $cpurchase->investor_name = $investor->full_name;

                    $commodity = $this->commodity_model->getCommodity($cpurchase->cusp_commodity_id);
                    if(!empty($commodity)){
                        $cpurchase->commodity_name = $commodity->comm_name;
                    }else{
                        $cpurchase->commodity_name = "Unavailable";
                    }

                    $data['cpurchase'] = $cpurchase;

                    $this->load->view('admin/temp/header');
                    $this->load->view('admin/temp/sidebar');
                    $this->load->view('admin/cpurchase_details', $data);
                    $this->load->view('admin/temp/footer');

                    // Log data
                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Viewed CPurchase Details. "."Slug: ".$cpurchase->cusp_slug;
                    $this->log_model->newLog($logData);

                } else {
                    show_404();
                }
            } else {
                show_404();
            }
        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function getCPurchaseDetails()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if ($this->is_admin) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);

            $data['user'] = $user;

            $cpurchase_slug = $this->input->post('cpurchase_slug');
            if (!empty($cpurchase_slug)) {
                $cpurchase = $this->cpurchases_model->getCPurchaseBySlug($cpurchase_slug);
                if (!empty($cpurchase)) {

                    $manager = $this->user_model->getUser($cpurchase->cusp_user_id);
                    $cpurchase->manager = $manager;

                    $investor = $this->user_model->getUser($cpurchase->cusp_user_id);
                    $cpurchase->investor_name = $investor->full_name;
                    $cpurchase->delivery_add = $investor->address;

                    $commodity = $this->commodity_model->getCommodityBySlug($cpurchase->cusp_commodity_id);
                    if(empty($commodity)){
                        $cpurchase->commodity_name = "Unnamed Commodity";
                    }else{
                        $cpurchase->commodity_name = $commodity->comm_name;
                    }

                    echo json_encode($cpurchase);

                    // Log data
                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Ajax call CPurchase Details. "."Slug: ".$cpurchase->cusp_slug;
                    $this->log_model->newLog($logData);

                } else {
                    echo json_encode("Customer Purchase does not exist!");
                }
            } else {
                echo json_encode("Customer Purchase does not exist!");
            }
        } else {
            echo json_encode("You do not have the privileges to perform this action.");
        }
    }

    public function deleteCPurchase()
    {
        if ($this->is_admin) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);

            $data['user'] = $user;

            $cpurchase_slug = $this->uri->segment(3);
            if (!empty($cpurchase_slug)) {
                $cpurchase = $this->cpurchases_model->getCPurchaseBySlug($cpurchase_slug);
                if (!empty($cpurchase)) {

                    $this->cpurchases_model->hardDelete($cpurchase->cusp_id);

                    // Return the quantity to the commodity 
                    $commodity = $this->commodity_model->getCommodity($cpurchase->cusp_commodity_id);
                    $this->commodity_model->updateCommodity($cpurchase->cusp_commodity_id, 
                    array('comm_quantity_left_to_buy' => ((int)$commodity->comm_quantity_left_to_buy + (int) $cpurchase->cusp_quantity ),
                        'comm_edited_by' => $this->loggedInUserID,
                        'comm_date_updated' => date('Y-m-d H:i:s') ));
                        
                    // Send Feedback to User
                    $feedback = $this->open_delimiter_success . 'Customer Purchase - ' . $cpurchase->cusp_slug . ' has been successfully deleted!  ' . $this->close_delimiter;
                    $this->session->set_flashdata('cpurchase_del_feedback', $feedback);

                    // Log data
                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Deleted Customer Purchase. "."Slug: ".$cpurchase->cusp_slug;
                    $this->log_model->newLog($logData);

                    redirect('admin/cpurchases');

                } else {
                    show_404();
                }
            } else {
                show_404();
            }
        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function addProduceTransfer()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $admin_user_id = $this->session->userdata('uid');

            $data['statuses'] = $this->producetransfer_model->get_enum_values('produce_transfers', 'pt_status');
            $data['payment_statuses'] = $this->producetransfer_model->get_enum_values('produce_transfers', 'pt_status');
            $data['wh_statuses'] = $this->producetransfer_model->get_enum_values('produce_transfers', 'pt_wh_received');
            $data['admin_statuses'] = $this->producetransfer_model->get_enum_values('produce_transfers', 'pt_admin_approved');
            $data['currencies'] = $this->currency_model->getAllCurrencies();
            $data['collection_centers'] = $this->collectioncenter_model->getAllCollectionCenters();
            $data['warehouses'] = $this->warehouse_model->getAllWarehouses();
            $produce_types = $this->producetype_model->getAllProduceTypes();
            if (empty($produce_types)) {
                $produce_types = array();
            }
            $data['produce_types'] = $produce_types;

            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
            $this->form_validation->set_rules('pt_transfer_cost', 'Transfer Cost', 'trim|required');
            $this->form_validation->set_rules('pt_quantity', 'Quantity', 'trim|required');

            if (isset($_POST['submit'])) {

                if ($this->form_validation->run() == false) {
                    $this->loadAddProduceTransferView($data);
                } else {

                    $newProduceTransferData = array();

                    $collection_center_id = $this->input->post('ccSelect');
                    $warehouse_id = $this->input->post('whSelect');
                    $quantity = (int) $this->input->post('pt_quantity');
                    $transfer_cost = (int) $this->input->post('pt_transfer_cost');

                    $collection_center = $this->collectioncenter_model->getCollectionCenter($collection_center_id);
                    $cc_current_capacity = (int) $collection_center->cc_current_capacity;
                    $cc_max_capacity = (int) $collection_center->cc_max_capacity;
                    $warehouse = $this->warehouse_model->getWarehouse($warehouse_id);
                    $wh_current_capacity = (int) $warehouse->wh_current_capacity;
                    $wh_max_capacity = (int) $warehouse->wh_max_capacity;

                    // Ensure that quantity is not zero or negative
                    if (($quantity <= 0)) {
                        // Tell the user
                        $feedback = $this->open_delimiter . 'Quantity cannot be zero or negative.' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddProduceTransferView($data);
                        return;

                    }

                    // Ensure that Transfer Cost is not zero or negative
                    if (($transfer_cost <= 0)) {
                        // Tell the user
                        $feedback = $this->open_delimiter . 'Transfer cannot be zero or negative.' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddProduceTransferView($data);
                        return;

                    }

                    // Ensure that Collection center has up to the quantity to be transferred
                    if (($quantity > $cc_current_capacity)) {
                        // Tell the user
                        $feedback = $this->open_delimiter . 'The Quantity is more than what is available at ';
                        $feedback .= ucwords($collection_center->cc_name) . '.<br>';
                        $feedback .= 'Current capacity is at ' . $cc_current_capacity . '.<br>';
                        $feedback .= $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddProduceTransferView($data);
                        return;

                    }

                    // Ensure that Warehouse has enough space for the transfer
                    $wh_total_capacity = $quantity + $wh_current_capacity;

                    if (($wh_total_capacity > $wh_max_capacity)) {
                        // Tell the user
                        $diff = $wh_total_capacity - $wh_max_capacity;
                        $feedback = $this->open_delimiter . 'The Quantity is more than the max capacity at ';
                        $feedback .= ucwords($warehouse->wh_name) . '.<br>';
                        $feedback .= 'Your transfer will exceed by ' . number_format($diff) . '.<br>';
                        $feedback .= 'Maximum capacity is ' . number_format($wh_max_capacity) . '.<br>';
                        $feedback .= $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddProduceTransferView($data);
                        return;

                    }

                    // Add the quantity to the Collection Center where the manager is
                    // assigned to work. To do this, first check if the current user is the
                    // CC Manager or the Super Admin
                    $admin = $this->user_model->getUser($admin_user_id);

                    $is_admin = ($admin->type == "admin") ? true : false;
                    $is_warehouse_manager = ($warehouse->wh_manager_id == $admin->id) ? true : false;
                    $is_cc_manager = ($collection_center->cc_manager_id == $admin->id) ? true : false;
                    $check = ($is_admin || $is_cc_manager);

                    if (!($check)) {
                        $feedback = $this->open_delimiter . 'You do not have the permission to transfer commodities from this Collection Center.' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddProduceTransferView($data);
                        return;
                    }

                    // Check if the current user is Warehouse manager
                    // And if he has received it, update it
                    // Admin can override
                    if (($is_warehouse_manager) && ($whmSelect == "yes") || ($is_admin) && ($whmSelect == "yes")) {
                        $newProduceTransferData['pt_wh_received'] = "yes";
                    }

                    // Check if the current user is Admin
                    // And if he has approved it, update it
                    if (($is_admin) && ($adminSelect == "yes")) {
                        $newProduceTransferData['pt_admin_approved'] = "yes";
                        $newProduceTransferData['pt_admin_user_id'] = $admin->id;
                    }

                    $newProduceTransferData['pt_current_location_id'] = $collection_center->cc_id;
                    $newProduceTransferData['pt_destination_id'] = $warehouse_id;
                    $newProduceTransferData['pt_produce_type_id'] = $this->input->post('categoryTypeSelect');
                    $newProduceTransferData['pt_produce_id'] = $this->input->post('categorySelect');
                    $newProduceTransferData['pt_cc_user_id'] = $admin_user_id;
                    $newProduceTransferData['pt_currency_id'] = $this->input->post('currencySelect');
                    $newProduceTransferData['pt_transfer_cost'] = $transfer_cost;
                    $newProduceTransferData['pt_unit_cost'] = floor($transfer_cost / $quantity);
                    $newProduceTransferData['pt_wh_user_id'] = $warehouse->wh_manager_id;
                    $newProduceTransferData['pt_status'] = $this->input->post('paySelect');
                    $newProduceTransferData['pt_quantity'] = $quantity;
                    $newProduceTransferData['pt_slug'] = $this->producetransfer_model->generateSlug($admin_user_id);
                    $newProduceTransferData['pt_created_by'] = $admin_user_id;
                    $newProduceTransferData['pt_date_created'] = date('Y-m-d H:i:s');

                    // Update this Collection Center current capacity
                    $ccData = array(
                        'cc_current_capacity' => ($cc_current_capacity - $quantity),
                        'cc_date_updated' => date('Y-m-d H:i:s'),
                    );
                    $this->collectioncenter_model->updateCollectionCenter($collection_center->cc_id, $ccData);

                    // Update this Warehouse current capacity
                    $whData = array(
                        'wh_current_capacity' => $wh_total_capacity,
                        'wh_date_updated' => date('Y-m-d H:i:s'),
                    );
                    $this->warehouse_model->updateWarehouse($warehouse->wh_id, $whData);

                    $this->producetransfer_model->newProduceTransfer($newProduceTransferData);
                    //Show success message
                    $feedback = $this->open_delimiter_success . 'New Produce Transfer has been initiated!  ' . $this->close_delimiter;
                    $this->session->set_flashdata('producetransfer_feedback', $feedback);
                    // Log data
                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Created an Produce Transfer. "."Slug: ".$newProduceTransferData['pt_slug'];
                    $this->log_model->newLog($logData);
                    redirect('admin/add-produce-transfer');

                }
            } else {
                $this->loadAddProduceTransferView($data);
            }

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadAddProduceTransferView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/add_produce_transfer', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/scripts/add_produce_transfer');
        $this->load->view('admin/temp/footer_close');
    }

    public function ProduceTransfers()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $logData = array('lg_user_id' => $this->loggedInUserID);

            $produce_transfers = array();
            $keyword = $this->input->get('keyword_pt');

            $this->load->library('pagination');
            $config = $this->pagination_config;

            // Add the search data in session to populate the fields again
            $this->session->set_userdata('keyword_pt', $keyword);
            $data['keyword_pt'] = $this->session->userdata('keyword_pt');

            // Get the page number if it is set
            $page = $this->uri->segment(3);
            $per_page = 10;
            if (empty($page)) {
                $page = 1;
                $offset = 0;
            } else {
                $page = (int) $page;
                $offset = $page * $per_page;
            }

            if (!empty($keyword)) {
                $produce_transfers = $this->producetransfer_model->getProduceTransfersWithName($keyword, $per_page, $offset);
                $config['total_rows'] = $this->producetransfer_model->getTotalProduceTransfersWithName($keyword);
                $data['total_produce_transfers'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Produce Transfer Page. "."Page: ".$page.". Searched for [".$keyword."]";

            } else {
                $produce_transfers = $this->producetransfer_model->getProduceTransfers($per_page, $offset);
                $config['total_rows'] = $this->producetransfer_model->getTotalProduceTransfers();
                $data['total_produce_transfers'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Produce Transfer Page. "."Page: ".$page;

            }

            if (!empty($produce_transfers)) {
                foreach ($produce_transfers as $produce_transfer) {
                    $manager = $this->user_model->getUser($produce_transfer->pt_created_by);
                    $produce_transfer->manager = $manager->full_name;

                    $category = $this->categories_model->getCategoryName($produce_transfer->pt_produce_id);
                    $produce_transfer->category = $category;
                    $collection_center = $this->collectioncenter_model->getCollectionCenter($produce_transfer->pt_current_location_id);
                    if(!empty($collection_center)){
                        $produce_transfer->collection_center = $collection_center->cc_name;
                    }else{
                        $produce_transfer->collection_center = "Unknown/Deleted Collection Center";
                    }


                    $warehouse = $this->warehouse_model->getWarehouse($produce_transfer->pt_destination_id);
                    if(!empty($warehouse)){
                        $produce_transfer->warehouse = $warehouse->wh_name;
                    }else{
                        $produce_transfer->warehouse = "Unknown/Deleted Warehouse";
                    }


                    $currency = $this->currency_model->getCurrency($produce_transfer->pt_currency_id);
                    $produce_transfer->currency = $currency->cur_symbol;
                }
            }

            $data['produce_transfers'] = $produce_transfers;

            $config['base_url'] = base_url("admin/produce-transfers/");
            $config['per_page'] = $per_page;

            $this->pagination->initialize($config);

            $data['pagination_links'] = $this->pagination->create_links();

            $this->load->view('admin/temp/header');
            $this->load->view('admin/temp/sidebar');
            $this->load->view('admin/all_produce_transfers', $data);
            $this->load->view('admin/temp/footer');

            // Log data
            $this->log_model->newLog($logData);

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function editProduceTransfer()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $produce_transfer_slug = $this->uri->segment(3);
            $logData = array('lg_user_id' => $this->loggedInUserID);

            if (!$this->producetransfer_model->slug_exists($produce_transfer_slug)) {
                show_404();
            } else {

                $produce_transfer = $this->producetransfer_model->getProduceTransferBySlug($produce_transfer_slug);
                $admin_user_id = $this->session->userdata('uid');

                $data['produce_transfer'] = $produce_transfer;
                $data['statuses'] = $this->producetransfer_model->get_enum_values('produce_transfers', 'pt_status');
                $data['payment_statuses'] = $this->producetransfer_model->get_enum_values('produce_transfers', 'pt_status');
                $data['wh_statuses'] = $this->producetransfer_model->get_enum_values('produce_transfers', 'pt_wh_received');
                $data['admin_statuses'] = $this->producetransfer_model->get_enum_values('produce_transfers', 'pt_admin_approved');
                $data['currencies'] = $this->currency_model->getAllCurrencies();
                $data['collection_centers'] = $this->collectioncenter_model->getAllCollectionCenters();
                $data['warehouses'] = $this->warehouse_model->getAllWarehouses();
                $produce_types = $this->producetype_model->getAllProduceTypes();
                if (empty($produce_types)) {
                    $produce_types = array();
                }
                $data['produce_types'] = $produce_types;

                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
                $this->form_validation->set_rules('pt_transfer_cost', 'Transfer Cost', 'trim|required');

                if (isset($_POST['submit'])) {

                    if ($this->form_validation->run() == false) {
                        $this->loadEditProduceTransferView($data);
                    } else {

                        $updateData = array();

                        $transfer_cost = (int) $this->input->post('pt_transfer_cost');
                        $whmSelect = $this->input->post('whmSelect');
                        $adminSelect = $this->input->post('adminSelect');

                        // var_dump("WHM_SELECT: ".$whmSelect);
                        // var_dump("ADMIN_SELECT: ".$adminSelect);
                        // die();

                        // Get Previous quantity of this produce transfer
                        $prev_quantity = (int) $produce_transfer->pt_quantity;
                        // Get Collection Center Manager
                        $collection_center = $this->collectioncenter_model->getCollectionCenter($produce_transfer->pt_current_location_id);
                        // Get Warehouse Manager
                        $warehouse = $this->warehouse_model->getWarehouse($produce_transfer->pt_destination_id);

                        // Add the quantity to the Collection Center where the manager is
                        // assigned to work. To do this, first check if the current user is the
                        // CC Manager or the Super Admin
                        $admin = $this->user_model->getUser($admin_user_id);

                        $is_admin = ($admin->type == "admin") ? true : false;
                        $is_warehouse_manager = ($warehouse->wh_manager_id == $admin->id) ? true : false;
                        $is_cc_manager = ($collection_center->cc_manager_id == $admin->id) ? true : false;
                        $check = ($is_admin || $is_cc_manager);

                        if (!($check)) {
                            $feedback = $this->open_delimiter . 'You do not have the permission to transfer commodities from this Collection Center.' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadEditProduceTransferView($data);
                            return;
                        }

                        // Check if the current user is Warehouse manager
                        // And if he has received it, update it
                        // Admin can override
                        if (($is_admin) && ($whmSelect == "yes") || ($is_warehouse_manager) && ($whmSelect == "yes")) {
                            $updateData['pt_wh_received'] = "yes";
                        }

                        // Check if the current user is Admin
                        // And if he has approved it, update it
                        if (($is_admin) && ($adminSelect == "yes")) {
                            $updateData['pt_admin_approved'] = "yes";
                            $updateData['pt_admin_user_id'] = $admin->id;
                        }

                        $updateData['pt_cc_user_id'] = $collection_center->cc_manager_id;
                        $updateData['pt_currency_id'] = $this->input->post('currencySelect');
                        $updateData['pt_transfer_cost'] = $transfer_cost;
                        $updateData['pt_unit_cost'] = floor($transfer_cost / $prev_quantity);
                        $updateData['pt_wh_user_id'] = $warehouse->wh_manager_id;
                        $updateData['pt_status'] = $this->input->post('paySelect');
                        $updateData['pt_edited_by'] = $admin_user_id;
                        $updateData['pt_date_updated'] = date('Y-m-d H:i:s');

                        $this->producetransfer_model->updateProduceTransfer($produce_transfer->pt_id, $updateData);
                        //Show success message
                        $feedback = $this->open_delimiter_success . 'Produce Transfer Data has been updated!  ' . $this->close_delimiter;
                        $this->session->set_flashdata('producetransfer_feedback', $feedback);

                        // Log data
                        $logData = array('lg_user_id' => $this->loggedInUserID);
                        $logData['lg_message'] = "Edited Produce Transfer. "."Slug: ".$updateData['pt_slug'];
                        $this->log_model->newLog($logData);

                        redirect('admin/edit-produce-transfer/' . $produce_transfer->pt_slug);
                    }
                } else {

                    $this->loadEditProduceTransferView($data);
                }

            }
        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadEditProduceTransferView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/edit_produce_transfer', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/scripts/edit_produce_transfer');
        $this->load->view('admin/temp/footer_close');
    }

    public function deleteProduceTransfer()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $data = array();
            $produce_transfer_slug = $this->uri->segment(3);

            if (!$this->producetransfer_model->slug_exists($produce_transfer_slug)) {
                show_404();
            } else {
                $admin_user_id = $this->session->userdata('uid');
                $produce_transfer = $this->producetransfer_model->getProduceTransferBySlug($produce_transfer_slug);

                if (!empty($produce_transfer)) {
                    // Get the Warehouse and Collection Center
                    $warehouse = $this->warehouse_model->getWarehouse($produce_transfer->pt_destination_id);
                    $collection_center = $this->collectioncenter_model->getCollectionCenter($produce_transfer->pt_current_location_id);

                    if(!empty($warehouse) && !empty($collection_center)){
                        // Get the new Warehouse and Collection Center capacities
                        $wh_new_capacity = (int) $warehouse->wh_current_capacity - (int) $produce_transfer->pt_quantity;
                        $cc_new_capacity = (int) $collection_center->cc_current_capacity + (int) $produce_transfer->pt_quantity;
                        $cc_max_capacity = (int) $collection_center->cc_max_capacity;

                        // Ensure that the quantity can be removed from the warehouse now
                        if ($wh_new_capacity < 0) {
                            $feedback = $this->open_delimiter_success . 'Produce Transfer - ' . $produce_transfer->pt_slug . ' cannot be deleted now as the Warehouse cannot deduct the quantity. ' . $this->close_delimiter;
                            $this->session->set_flashdata('produce_transfer_del_feedback', $feedback);
                            redirect('admin/produce-transfers');
                            return;
                        }

                        // Ensure the Collection Center Max Capacity is not exceeded
                        if ($cc_new_capacity > $cc_max_capacity) {
                            $feedback = $this->open_delimiter_success . 'Produce Transfer - ' . $produce_transfer->pt_slug . ' cannot be deleted now as the Collection Center maximum capacity will be exceeded. ' . $this->close_delimiter;
                            $this->session->set_flashdata('produce_transfer_del_feedback', $feedback);
                            redirect('admin/produce-transfers');
                            return;
                        }

                        // Return this quantity from the Collection center
                        $this->collectioncenter_model->updateCollectionCenter($collection_center->cc_id, array(
                            'cc_current_capacity' => $cc_new_capacity,
                            'cc_edited_by' => $admin_user_id,
                            'cc_date_updated' => date('Y-m-d H:i:s')));

                        // Remove this quantity from the Warehouse
                        $this->warehouse_model->updateWarehouse($warehouse->wh_id, array(
                            'wh_current_capacity' => $wh_new_capacity,
                            'wh_edited_by' => $admin_user_id,
                            'wh_date_updated' => date('Y-m-d H:i:s')));
                    }

                    $this->producetransfer_model->hardDelete($produce_transfer->pt_id);

                    $feedback = $this->open_delimiter_success . 'Produce Transfer- ' . $produce_transfer->pt_slug . ' has been successfully deleted!  ' . $this->close_delimiter;
                    $this->session->set_flashdata('produce_transfer_del_feedback', $feedback);

                    // Log Data
                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Deleted Produce Transfer. "."Slug: ".$produce_transfer_slug;
                    $this->log_model->newLog($logData);

                }

                redirect('admin/produce-transfers');
            }
        }
    }

    public function produceTransferDetail()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);

            $data['user'] = $user;

            $produce_transfer_slug = $this->uri->segment(3);
            if (!empty($produce_transfer_slug)) {
                $produce_transfer = $this->producetransfer_model->getProduceTransferBySlug($produce_transfer_slug);
                if (!empty($produce_transfer)) {

                    $manager = $this->user_model->getUser($produce_transfer->pt_created_by);
                    $produce_transfer->manager = $manager;

                    $edit_manager = $this->user_model->getUser($produce_transfer->pt_edited_by);
                    $produce_transfer->edit_manager = $edit_manager;

                    $category = $this->categories_model->getCategoryName($produce_transfer->pt_produce_id);
                    $produce_transfer->category = $category;

                    $collection_center = $this->collectioncenter_model->getCollectionCenter($produce_transfer->pt_current_location_id);
                    $produce_transfer->collection_center = $collection_center->cc_name;

                    $warehouse = $this->warehouse_model->getWarehouse($produce_transfer->pt_destination_id);
                    $produce_transfer->warehouse = $warehouse->wh_name;

                    $wh_manager = $this->user_model->getUser($produce_transfer->pt_wh_user_id);
                    $produce_transfer->wh_manager = $wh_manager->full_name;

                    $currency = $this->currency_model->getCurrency($produce_transfer->pt_currency_id);
                    $produce_transfer->currency = $currency->cur_symbol;

                    $data['produce_transfer'] = $produce_transfer;

                    $this->load->view('admin/temp/header');
                    $this->load->view('admin/temp/sidebar');
                    $this->load->view('admin/produce_transfer_details', $data);
                    $this->load->view('admin/temp/footer');

                    // Log Data
                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Viewed Produce Transfer Details. "."Slug: ".$produce_transfer_slug;
                    $this->log_model->newLog($logData);

                } else {
                    show_404();
                }
            } else {
                show_404();
            }
        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function getOrderDetails()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $order_number = $this->input->post('order_number');
        $order = $this->orders_model->getOrderByNumber($order_number);

        if (!empty($order)) {
            $order->buyer = $this->user_model->getUser($order->or_user_id)->full_name;
        } else {
            $order = array();
        }
        // Send response back to Ajax
        echo json_encode($order);
    }

    public function addOutgoingOrder()
    {
        if ($this->is_admin || $this->is_wh_manager) {

            $admin_user_id = $this->session->userdata('uid');

            $data['payment_statuses'] = $this->outgoingorder_model->get_enum_values('outgoing_orders', 'oo_pay_status');
            $data['delivery_statuses'] = $this->outgoingorder_model->get_enum_values('outgoing_orders', 'oo_transfer_status');
            $data['wh_statuses'] = $this->outgoingorder_model->get_enum_values('outgoing_orders', 'oo_wh_approved');
            $data['admin_statuses'] = $this->outgoingorder_model->get_enum_values('outgoing_orders', 'oo_admin_approved');
            $data['currencies'] = $this->currency_model->getAllCurrencies();
            $data['warehouses'] = $this->warehouse_model->getAllWarehouses();
            $data['cpurchases'] = $this->cpurchases_model->getUndeliveredPurchases();
            $produce_types = $this->producetype_model->getAllProduceTypes();
            if (empty($produce_types)) {
                $produce_types = array();
            }
            $data['produce_types'] = $produce_types;

            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
            // $this->form_validation->set_rules('oo_transfer_cost', 'Transfer Cost', 'trim|required');
            $this->form_validation->set_rules('oo_quantity', 'Quantity', 'trim|required');

            if (isset($_POST['submit'])) {

                if ($this->form_validation->run() == false) {
                    $this->loadAddOutgoingOrderView($data);
                } else {

                    $quantity = (int) $this->input->post('oo_quantity');
                    $cpurchase_slug = $this->input->post('orSelect');
                    $cpurchase = $this->cpurchases_model->getCPurchaseBySlug($cpurchase_slug);
                    $cpurchase_quantity = (int) $cpurchase->cusp_quantity;
                    $warehouse_id = $this->input->post('whSelect');
                    $whmSelect = $this->input->post('whmSelect');
                    $adminSelect = $this->input->post('adminSelect');
                    $warehouse = $this->warehouse_model->getWarehouse($warehouse_id);
                    $wh_current_capacity = (int) $warehouse->wh_current_capacity;
                    $transfer_cost = $this->input->post('oo_transfer_cost');
                    $delivery_status = $this->input->post('deliverySelect');

                    // Ensure that quantity is not zero or negative
                    if (($quantity <= 0)) {
                        // Tell the user
                        $feedback = $this->open_delimiter . 'Quantity cannot be zero or negative.' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddOutgoingOrderView($data);
                        return;
                    }

                    // Check that Quantity is not greater than Order quantity
                    if ($quantity > $cpurchase_quantity) {
                        // Tell the user
                        $feedback = $this->open_delimiter . 'The Quantity is greater than Order quantity.' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddOutgoingOrderView($data);
                        return;
                    }

                    // Check that quantity is not greater Warehouse current capacity
                    if ($quantity > $wh_current_capacity) {
                        // Tell the user
                        $feedback = $this->open_delimiter . 'The Quantity is greater than the current capacity at ';
                        $feedback .= '' . $warehouse->wh_name . '.<br>';
                        $feedback .= 'Current capacity is at ' . $wh_current_capacity . '.<br>';
                        $feedback .= $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddOutgoingOrderView($data);
                        return;
                    }

                    // Add the quantity to the Collection Center where the manager is
                    // assigned to work. To do this, first check if the current user is the
                    // WH Manager or the Super Admin
                    $admin = $this->user_model->getUser($admin_user_id);

                    $is_admin = ($admin->type == "admin") ? true : false;
                    $is_warehouse_manager = ($warehouse->wh_manager_id == $admin->id) ? true : false;
                    $check = ($is_admin || $is_warehouse_manager);

                    if (!($check)) {
                        $feedback = $this->open_delimiter . 'You do not have the permission to transfer commodities from this Warehouse.' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddOutgoingOrderView($data);
                        return;
                    }

                    // Check if the current user is Warehouse manager
                    // And if he has approved it, update it
                    // Admin can override
                    if (($is_admin) && ($whmSelect == "yes") || ($is_warehouse_manager) && ($whmSelect == "yes")) {
                        $newOutgoingOrder['oo_wh_approved'] = "yes";
                        $newOutgoingOrder['oo_wh_user_id'] = $warehouse->wh_manager_id;
                    }

                    // Check if the current user is Admin
                    // And if he has approved it, update it
                    if (($is_admin) && ($adminSelect == "yes")) {
                        $newOutgoingOrder['oo_admin_approved'] = "yes";
                        $newOutgoingOrder['oo_admin_user_id'] = $admin->id;
                    }

                    // Update the Order delivered quantity if the status is set to delivered
                    // Done by an Admin
                    if ((($is_admin) && ($delivery_status == "delivered")) || (($is_admin) && ($delivery_status == "pickup"))) {
                        // Check the customer quantity requested and the quantity delivered 
                        if($cpurchase->cusp_quantity == $cpurchase->cusp_quantity_delivered){
                            $this->cpurchases_model->updateCPurchase($cpurchase->cusp_id, array(
                                'cusp_quantity_delivered' => ((int) $cpurchase->cusp_quantity_delivered + $quantity),
                                'cusp_delivery_status' => 'delivered',
                                'cusp_date_updated' => date('Y-m-d H:i:s'),
                            ));
                        }else{
                            $this->cpurchases_model->updateCPurchase($cpurchase->cusp_id, array(
                                'cusp_quantity_delivered' => ((int) $cpurchase->cusp_quantity_delivered + $quantity),
                                'cusp_date_updated' => date('Y-m-d H:i:s'),
                            ));
                        }
                    }

                    $newOutgoingOrder['oo_order_number'] = $cpurchase_slug;
                    $newOutgoingOrder['oo_warehouse_id'] = $warehouse->wh_id;
                    $newOutgoingOrder['oo_quantity'] = $quantity;
                    $newOutgoingOrder['oo_currency_id'] = $this->input->post('currencySelect');
                    $newOutgoingOrder['oo_transfer_cost'] = $transfer_cost;
                    $newOutgoingOrder['oo_unit_cost'] = floor($transfer_cost / $quantity);
                    $newOutgoingOrder['oo_pay_status'] = $this->input->post('paySelect');
                    $newOutgoingOrder['oo_transfer_status'] = $delivery_status;
                    $newOutgoingOrder['oo_slug'] = $this->outgoingorder_model->generateSlug($admin_user_id);
                    $newOutgoingOrder['oo_created_by'] = $admin_user_id;
                    $newOutgoingOrder['oo_date_created'] = date('Y-m-d H:i:s');

                    // Update this Warehouse current capacity
                    $whData = array(
                        'wh_current_capacity' => ($wh_current_capacity - $quantity),
                        'wh_edited_by' => $admin_user_id,
                        'wh_date_updated' => date('Y-m-d H:i:s'),
                    );
                    $this->warehouse_model->updateWarehouse($warehouse->wh_id, $whData);

                    $this->outgoingorder_model->newOutgoingOrder($newOutgoingOrder);
                    //Show success message
                    $feedback = $this->open_delimiter_success . 'New Outgoing Order has been initiated!  ' . $this->close_delimiter;
                    $this->session->set_flashdata('outgoing_order_feedback', $feedback);

                    // Log data
                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Created an Outgoing Order. "."Slug: ".$newOutgoingOrder['oo_slug'];
                    $this->log_model->newLog($logData);
                    redirect('admin/add-outgoing-order');

                }
            } else {
                $this->loadAddOutgoingOrderView($data);
            }

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadAddOutgoingOrderView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/add_outgoing_order', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/scripts/add_outgoing_order');
        $this->load->view('admin/temp/footer_close');
    }

    public function OutgoingOrders()
    {
        if ($this->is_admin || $this->is_wh_manager) {

            $outgoing_orders = array();
            $keyword = $this->input->get('keyword_oo');

            $logData = array('lg_user_id' => $this->loggedInUserID);

            $this->load->library('pagination');
            $config = $this->pagination_config;

            // Add the search data in session to populate the fields again
            $this->session->set_userdata('keyword_oo', $keyword);
            $data['keyword_oo'] = $this->session->userdata('keyword_oo');

            // Get the page number if it is set
            $page = $this->uri->segment(3);
            $per_page = 10;
            if (empty($page)) {
                $page = 1;
                $offset = 0;
            } else {
                $page = (int) $page;
                $offset = $page * $per_page;
            }

            if (!empty($keyword)) {
                $outgoing_orders = $this->outgoingorder_model->getOutgoingOrdersWithName($keyword, $per_page, $offset);
                $config['total_rows'] = $this->outgoingorder_model->getTotalOutgoingOrdersWithName($keyword);
                $data['total_outgoing_orders'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Outgoing Order Page. "."Page: ".$page.". Searched for [".$keyword."]";

            } else {
                $outgoing_orders = $this->outgoingorder_model->getOutgoingOrders($per_page, $offset);
                $config['total_rows'] = $this->outgoingorder_model->getTotalOutgoingOrders();
                $data['total_outgoing_orders'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Outgoing Order Page. "."Page: ".$page;
            }

            if (!empty($outgoing_orders)) {
                foreach ($outgoing_orders as $outgoing_order) {
                    $manager = $this->user_model->getUser($outgoing_order->oo_created_by);
                    $outgoing_order->manager = $manager->full_name;

                    // Get the Customer Purchase
                    $cpurchase = $this->cpurchases_model->getCPurchaseBySlug($outgoing_order->oo_order_number);

                    $buyer = $this->user_model->getUser($cpurchase->cusp_user_id);
                    $outgoing_order->buyer = $buyer->full_name;
                    if (!empty($cpurchase->cusp_commodity_name)) {
                        $outgoing_order->produce = $cpurchase->cusp_commodity_name;
                    } else {
                        $outgoing_order->produce = "No Commodity";
                    }

                    $currency = $this->currency_model->getCurrency($outgoing_order->oo_currency_id);
                    $outgoing_order->currency = $currency->cur_symbol;
                }
            }

            $data['outgoing_orders'] = $outgoing_orders;

            $config['base_url'] = base_url("admin/outgoing-orders/");
            $config['per_page'] = $per_page;

            $this->pagination->initialize($config);

            $data['pagination_links'] = $this->pagination->create_links();

            $this->load->view('admin/temp/header');
            $this->load->view('admin/temp/sidebar');
            $this->load->view('admin/all_outgoing_orders', $data);
            $this->load->view('admin/temp/footer');

            // Log data
            $this->log_model->newLog($logData);

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function editOutgoingOrder()
    {
        if ($this->is_admin || $this->is_wh_manager) {

            $outgoingorder_slug = $this->uri->segment(3);

            if (!$this->outgoingorder_model->slug_exists($outgoingorder_slug)) {
                show_404();
            } else {
                $admin_user_id = $this->session->userdata('uid');

                $outgoing_order = $this->outgoingorder_model->getOutgoingOrderBySlug($outgoingorder_slug);
                $warehouse = $this->warehouse_model->getWarehouse($outgoing_order->oo_warehouse_id);
                $cpurchase = $this->cpurchases_model->getCPurchaseBySlug($outgoing_order->oo_order_number);
                $data['outgoing_order'] = $outgoing_order;
                $data['payment_statuses'] = $this->outgoingorder_model->get_enum_values('outgoing_orders', 'oo_pay_status');
                $data['delivery_statuses'] = $this->outgoingorder_model->get_enum_values('outgoing_orders', 'oo_transfer_status');
                $data['wh_statuses'] = $this->outgoingorder_model->get_enum_values('outgoing_orders', 'oo_wh_approved');
                $data['admin_statuses'] = $this->outgoingorder_model->get_enum_values('outgoing_orders', 'oo_admin_approved');
                $data['currencies'] = $this->currency_model->getAllCurrencies();
                $data['warehouse'] = $warehouse;
                $data['cpurchase'] = $cpurchase;
                $produce_types = $this->producetype_model->getAllProduceTypes();
                if (empty($produce_types)) {
                    $produce_types = array();
                }
                $data['produce_types'] = $produce_types;

                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
                $this->form_validation->set_rules('oo_transfer_cost', 'Transfer Cost', 'trim');

                if (isset($_POST['submit'])) {

                    if ($this->form_validation->run() == false) {

                        var_dump($this->input->post());
                        die();
                        $this->loadEditOutgoingOrderView($data);
                    } else {
                        $updateData = array();
                        $prev_quantity = (int) $outgoing_order->oo_quantity;
                        $prev_delivery_status = $outgoing_order->oo_transfer_status;
                        $prev_pay_status = $outgoing_order->oo_pay_status;
                        $order_number = $outgoing_order->oo_order_number;
                        // var_dump($order_number);
                        // die();

                        $cpurchase = $this->cpurchases_model->getCPurchaseBySlug($order_number);
                        $cpurchase_quantity = (int) $cpurchase->cusp_quantity;

                        $warehouse_id = $this->input->post('whSelect');
                        $whmSelect = $this->input->post('whmSelect');
                        $adminSelect = $this->input->post('adminSelect');
                        $transfer_cost = $this->input->post('oo_transfer_cost');
                        $delivery_status = $this->input->post('deliverySelect');
                        $pay_status = $this->input->post('paySelect');

                        if (empty($delivery_status)) {
                            $delivery_status = $prev_delivery_status;
                        }

                        if (empty($pay_status)) {
                            $pay_status = $prev_pay_status;
                        }

                        // Add the quantity to the Collection Center where the manager is
                        // assigned to work. To do this, first check if the current user is the
                        // WH Manager or the Super Admin
                        $admin = $this->user_model->getUser($admin_user_id);

                        $is_admin = ($admin->type == "admin") ? true : false;
                        $is_warehouse_manager = ($warehouse->wh_manager_id == $admin->id) ? true : false;
                        $check = ($is_admin || $is_warehouse_manager);

                        if (!($check)) {
                            $feedback = $this->open_delimiter . 'You do not have the permission to transfer commodities from this Warehouse.' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadEditOutgoingOrderView($data);
                            return;
                        }

                        // Check if the current user is Warehouse manager
                        // And if he has approved it, update it
                        // Admin can override
                        if (($is_admin) || ($is_warehouse_manager)) {
                            $updateData['oo_wh_approved'] = $whmSelect;
                            $updateData['oo_wh_user_id'] = $warehouse->wh_manager_id;
                        }

                        // Check if the current user is Admin
                        // And if he has approved it, update it
                        if ($is_admin) {
                            $updateData['oo_admin_approved'] = $adminSelect;
                            $updateData['oo_admin_user_id'] = $admin->id;
                        }

                        // Update the Order delivered quantity if the status is set to delivered
                        // Done by an Admin
                        if ((($is_admin) && ($delivery_status == "delivered")) || (($is_admin) && ($delivery_status == "pickup"))) {
                            // If the delivery status of the order has been set to 'delivered' or 'pickup'
                            // Then no change
                            if (!($prev_delivery_status == 'delivered') || !($prev_delivery_status == 'pickup')) {
                                // Check the customer quantity requested and the quantity delivered 
                                $current_quantity = (int) $cpurchase->cusp_quantity_delivered + $prev_quantity;
                                if( $cpurchase->cusp_quantity == $current_quantity ){
                                    var_dump("Quantity Now Equals Quantity-Delivered");
                                    // die();
                                    $this->cpurchases_model->updateCPurchase($cpurchase->cusp_id, array(
                                        'cusp_quantity_delivered' => ((int) $cpurchase->cusp_quantity_delivered + (int) $prev_quantity),
                                        'cusp_delivery_status' => 'delivered',
                                        'cusp_date_updated' => date('Y-m-d H:i:s'),
                                    ));
                                }else{
                                    var_dump("Quantity Now IS STILL NOT EQUAL Quantity-Delivered");
                                    // die();
                                    $this->cpurchases_model->updateCPurchase($cpurchase->cusp_id, array(
                                        'cusp_quantity_delivered' => ((int) $cpurchase->cusp_quantity_delivered + (int) $prev_quantity),
                                        'cusp_date_updated' => date('Y-m-d H:i:s'),
                                    ));
                                }

                            }
                        }

                        $updateData['oo_warehouse_id'] = $warehouse->wh_id;
                        $updateData['oo_currency_id'] = $this->input->post('currencySelect');
                        $updateData['oo_transfer_cost'] = $transfer_cost;
                        $updateData['oo_unit_cost'] = floor($transfer_cost / $prev_quantity);
                        $updateData['oo_pay_status'] = $pay_status;
                        $updateData['oo_transfer_status'] = $delivery_status;
                        $updateData['oo_edited_by'] = $admin_user_id;
                        $updateData['oo_date_updated'] = date('Y-m-d H:i:s');

                        $this->outgoingorder_model->updateOutgoingOrder($outgoing_order->oo_id, $updateData);
                        //Show success message
                        $feedback = $this->open_delimiter_success . 'Outgoing Order has been updated!  ' . $this->close_delimiter;
                        $this->session->set_flashdata('outgoing_order_feedback', $feedback);
                        
                        // Log Data
                        $logData = array('lg_user_id' => $this->loggedInUserID);
                        $logData['lg_message'] = "Edited Outgoing Order. "."Slug: ".$outgoing_order_slug;
                        $this->log_model->newLog($logData);
                        redirect('admin/edit-outgoing-order/' . $outgoing_order->oo_slug);

                    }
                } else {

                    $this->loadEditOutgoingOrderView($data);
                }

            }

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadEditOutgoingOrderView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/edit_outgoing_order', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/scripts/edit_outgoing_order');
        $this->load->view('admin/temp/footer_close');
    }

    public function deleteOutgoingOrder()
    {
        if ($this->is_admin || $this->is_wh_manager) {

            $data = array();
            $outgoing_order_slug = $this->uri->segment(3);
            if (!$this->outgoingorder_model->slug_exists($outgoing_order_slug)) {
                show_404();
            } else {

                $outgoing_order = $this->outgoingorder_model->getOutgoingOrderBySlug($outgoing_order_slug);
                $oDeliveryStatus = $outgoing_order->oo_transfer_status;
                $quantity = (int) $outgoing_order->oo_quantity;
                $warehouse = $this->warehouse_model->getWarehouse($outgoing_order->oo_warehouse_id);
                $cpurchase = $this->cpurchases_model->getCPurchaseBySlug($outgoing_order->oo_order_number);
                $cDeliveryStatus = $cpurchase->cusp_delivery_status;
                $current_value = (int) $cpurchase->cusp_quantity_delivered - $quantity;
                if($current_value < 0){
                    $current_value = 0;
                }
                if(($cDeliveryStatus == 'delivered') || ($oDeliveryStatus == 'delivered') || ($oDeliveryStatus == 'pickup') ){
                    // We have to deduct it from the CPurchase Quantity Sent value
                    $this->cpurchases_model->updateCPurchase($cpurchase->cusp_id, array(
                                    'cusp_quantity_delivered' => $current_value,
                                    'cusp_delivery_status' => 'pending',
                                    'cusp_date_updated' => date('Y-m-d H:i:s'),
                            ));
                }

                if(!empty($warehouse)){
                    $wh_current_capacity = (int) $warehouse->wh_current_capacity;
                    if ($outgoing_order !== false) {
                        // Add this quantity back to the Warehouse where it was done
                        // Update this Warehouse current capacity
                        $whData = array(
                            'wh_current_capacity' => ($wh_current_capacity + $quantity),
                            'wh_edited_by' => $this->loggedInUserID,
                            'wh_date_updated' => date('Y-m-d H:i:s'),
                        );
                        $this->warehouse_model->updateWarehouse($warehouse->wh_id, $whData);
                }
                    $this->outgoingorder_model->hardDelete($outgoing_order->oo_id);

                    $feedback = $this->open_delimiter_success . 'Outgoing Order - ' . $outgoing_order->oo_slug . ' has been successfully deleted!  ' . $this->close_delimiter;
                    $this->session->set_flashdata('outgoing_order_del_feedback', $feedback);

                    // Log Data
                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Deleted Outgoing Order. "."Slug: ".$outgoing_order_slug;
                    $this->log_model->newLog($logData);

                }

                redirect('admin/outgoing-orders');

            }
        }
    }

    public function outgoingOrderDetail()
    {
        if ($this->is_admin || $this->is_wh_manager) {

            $logData = array('lg_user_id' => $this->loggedInUserID);

            $outgoing_order_slug = $this->uri->segment(3);
            if (!empty($outgoing_order_slug)) {
                $outgoing_order = $this->outgoingorder_model->getOutgoingOrderBySlug($outgoing_order_slug);
                if (!empty($outgoing_order)) {

                    $manager = $this->user_model->getUser($outgoing_order->oo_created_by);
                    $outgoing_order->manager = $manager;

                    // Get the Customer Purchase
                    $cpurchase = $this->cpurchases_model->getCPurchaseBySlug($outgoing_order->oo_order_number);

                    $buyer = $this->user_model->getUser($cpurchase->cusp_user_id);
                    $outgoing_order->buyer = $buyer->full_name;
                    if (!empty($cpurchase->cusp_commodity_name)) {
                        $outgoing_order->produce = $cpurchase->cusp_commodity_name;
                    } else {
                        $outgoing_order->produce = "No Commodity";
                    }
                    $warehouse = $this->warehouse_model->getWarehouse($outgoing_order->oo_warehouse_id);
                    $outgoing_order->warehouse_name = $warehouse->wh_name;

                    // Buyer Address
                    $outgoing_order->buyer_address = $buyer->address;

                    $currency = $this->currency_model->getCurrency($outgoing_order->oo_currency_id);
                    $outgoing_order->currency = $currency->cur_symbol;

                    $data['outgoing_order'] = $outgoing_order;

                    $this->load->view('admin/temp/header');
                    $this->load->view('admin/temp/sidebar');
                    $this->load->view('admin/outgoing_order_details', $data);
                    $this->load->view('admin/temp/footer');

                    // Log Data
                    $logData['lg_message'] = "Viewed Outgoing Order Details Page. "."Slug: ".$outgoing_order_slug;
                    $this->log_model->newLog($logData);

                } else {
                    show_404();
                }
            } else {
                show_404();
            }
        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function Inventory()
    {
        if ($this->is_admin) {
            $collection_centers = $this->collectioncenter_model->getCollectionCenters(10, 0);
            if (!empty($collection_centers)) {
                foreach ($collection_centers as $collection_center) {
                    $manager = $this->user_model->getUser($collection_center->cc_manager_id);
                    $collection_center->manager = $manager->full_name;
                }
            }
            $warehouses = $this->warehouse_model->getWarehouses(10, 0);
            if (!empty($warehouses)) {
                foreach ($warehouses as $warehouse) {
                    $manager = $this->user_model->getUser($warehouse->wh_manager_id);
                    $warehouse->manager = $manager->full_name;
                }
            }
            $data['collection_centers'] = $collection_centers;
            $data['warehouses'] = $warehouses;
            $data['total_warehouses'] = $this->warehouse_model->getTotalWarehouses();
            $data['total_collection_centers'] = $this->collectioncenter_model->getTotalCollectionCenters();
            $data['pagination_links'] = '';
            // Show the Collection Centers
            $this->load->view('admin/temp/header');
            $this->load->view('admin/temp/sidebar');
            $this->load->view('admin/inventory', $data);
            $this->load->view('admin/temp/footer');

            // Log data
            $logData = array(
                'lg_user_id' => $this->loggedInUserID,
                'lg_message' => "Viewed Inventory Page",
                );
            $this->log_model->newLog($logData);

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }

    }

    public function Logs()
    {
        if ($this->is_admin) {

            $logs = array();
            $keyword = $this->input->get('keyword_lg');

            $this->load->library('pagination');
            $config = $this->pagination_config;

            // Add the search data in session to populate the fields again
            $this->session->set_userdata('keyword_lg', $keyword);
            $data['keyword_lg'] = $this->session->userdata('keyword_lg');

            // Get the page number if it is set
            $page = $this->uri->segment(3);
            $per_page = 10;
            if (empty($page)) {
                $page = 1;
                $offset = 0;
            } else {
                $page = (int) $page;
                $offset = $page * $per_page;
            }

            if (!empty($keyword)) {
                $logs = $this->log_model->getlogsWithUserID($keyword, $per_page, $offset);
                $config['total_rows'] = $this->log_model->getTotallogsWithUserID($keyword);
                $data['total_logs'] = $config['total_rows'];

            } else {
                $logs = $this->log_model->getlogs($per_page, $offset);
                $config['total_rows'] = $this->log_model->getTotallogs();
                $data['total_logs'] = $config['total_rows'];
            }

            if (!empty($logs)) {
                foreach ($logs as $log) {
                    $user = $this->user_model->getUser($log->lg_user_id);
                    if(!empty($user)){
                        $log->username = $user->full_name;
                    }else{
                        $log->username = "Guest User";
                    }   
                }
            }

            $data['logs'] = $logs;

            $config['base_url'] = base_url("admin/logs/");
            $config['per_page'] = $per_page;

            $this->pagination->initialize($config);

            $data['pagination_links'] = $this->pagination->create_links();

            $this->load->view('admin/temp/header');
            $this->load->view('admin/temp/sidebar');
            $this->load->view('admin/all_logs', $data);
            $this->load->view('admin/temp/footer');

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function addCommodity()
    {
        if ($this->is_admin) {

            $data['categories'] = $this->categories_model->getAllCategories();
            $data['units'] = $this->units_model->getAllUnits();
            $data['statuses'] = $this->commodity_model->get_enum_values('commodities', 'comm_status');
            $data['partners'] = $this->partner_model->getAllPartners();
            $data['types'] = $this->commodity_model->get_enum_values('commodities', 'comm_commodity_type');
            $produce_types = $this->producetype_model->getAllProduceTypes();

            if (empty($produce_types)) {
                $produce_types = array();
            }
            $data['produce_types'] = $produce_types;

            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
            $this->form_validation->set_rules('name', 'Commodity Name', 'trim|required');
            $this->form_validation->set_rules('comm_price_per_unit', 'Price per unit', 'trim|integer|required');
            $this->form_validation->set_rules('comm_cycle', 'Cycle', 'trim|required');
            $this->form_validation->set_rules('quantity', 'Quantity', 'trim|required');
            $this->form_validation->set_rules('quantity_left', 'Quantity Left', 'trim|required');
            $this->form_validation->set_rules('quantity_left_to_buy', 'Quantity Left To Buy', 'trim|required');
            $this->form_validation->set_rules('comm_commodity_details', 'Commodity Details', 'trim|required');
            $this->form_validation->set_rules('comm_insurance_cover', 'Insurance Cover', 'trim|required');
            $this->form_validation->set_rules('comm_google_sheet_link', 'Google Sheet Link', 'trim|required');

        
            if (isset($_POST['submit'])) {

                if ($this->form_validation->run() == false) {
                    $this->loadAddCommodityView($data);
                } else {

                    $name = trim($this->input->post('name'));
                    $type = $this->input->post('type');
                    $quantity = (int)$this->input->post('quantity');
                    $quantity_left = (int)$this->input->post('quantity_left');
                    $quantity_left_to_buy = (int)$this->input->post('quantity_left_to_buy');
                    $commodity_details = (trim($this->input->post('comm_commodity_details')));
                    $insurance_cover = (trim($this->input->post('comm_insurance_cover')));
                    $google_sheet_link = trim($this->input->post('comm_google_sheet_link'));

                    $name_exists = $this->commodity_model->name_exists($name);

                    // Check if Name already exists
                    if($name_exists){
                        $feedback = $this->open_delimiter . 'The name already exists. Please use another name' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddCommodityView($data);
                        return;
                    }

                    // Check if Quantity is zero or negative
                    if($quantity < 0){
                        $feedback = $this->open_delimiter . 'The quantity cannot be negative' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddCommodityView($data);
                        return;
                    }

                    // Check if Quantity Left is negative
                    if($quantity_left < 0){
                        $feedback = $this->open_delimiter . 'The Quantity Left cannot be negative' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddCommodityView($data);
                        return;
                    }

                    // Check if Google Sheet Link is a valid url
                    if(!filter_var($google_sheet_link, FILTER_VALIDATE_URL)){
                        $feedback = $this->open_delimiter . 'You entered an invalid URL for Google Sheets Link' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddCommodityView($data);
                        return;
                    }

                    $newICommodity = array(
                        'comm_name' => $name,
                        'comm_status' => $this->input->post('status'),
                        'comm_partner_id' => $this->input->post('partner_id'),
                        'comm_commodity_type'=> $type,
                        'comm_slug' => $this->commodity_model->generateCommoditySlug($name, $this->loggedInUserID),
                        'comm_produce_type_id' => $this->input->post('categoryTypeSelect'),
                        'comm_produce_id' => $this->input->post('categorySelect'),
                        'comm_price_per_unit' => $this->input->post('comm_price_per_unit'),
                        'comm_cycle' => $this->input->post('comm_cycle'),
                        'comm_unit_id' => $this->input->post('unitSelect'),
                        'comm_insurance_cover' => $insurance_cover,
                        'comm_commodity_details' => $commodity_details,
                        'comm_google_sheet_link' => $google_sheet_link,
                        'comm_quantity' => $quantity,
                        'comm_quantity_left' => $quantity_left,
                        'comm_quantity_left_to_buy' => $quantity_left_to_buy,
                        'comm_created_by' => $this->loggedInUserID,
                        'comm_date_created' => date('Y-m-d H:i:s'),
                    );

                    if (!empty($_FILES['productImageFile']['name'])) {
                        $path = './mainasset/fe/images/commodities/';
                        $results = $this->do_upload($path, 'productImageFile');
                        if (!isset($results['errors'])) {
                            $iCommodityID = $this->commodity_model->newCommodity($newICommodity);
                            //add the images to the cusers table
                            $imageData = array(
                                'comm_image' => $results['file_name'],
                                'comm_date_updated' => date('Y-m-d H:i:s'),
                            );
                            $this->commodity_model->updateCommodity($iCommodityID, $imageData);
                            //Show success message
                            $feedback = $this->open_delimiter_success . ucwords($type).' Commodity has been successfully uploaded!' . $this->close_delimiter;
                            $this->session->set_flashdata('commodity_feedback', $feedback);
                            // Log data
                            $logData = array(
                                'lg_user_id' => $this->loggedInUserID,
                                'lg_message' => "Added New ". ucwords($type)." Commodity. "."Slug: ".$newICommodity['comm_slug'],
                                );
                            $this->log_model->newLog($logData);
                        } else {
                            $image_errors = $results['errors'];
                            $image_errors_output = "";
                            if(!empty($image_errors)){
                                foreach ($image_errors as $error) {
                                    $image_errors_output .= $this->open_delimiter_success . $error . $this->close_delimiter;
                                } 
                            }
                            $data['image_error'] = $image_errors_output;
                            $this->loadAddCommodityView($data);
                            return;                  
                        }
                    } else {
                        //Show error
                        $feedback = $this->open_delimiter . 'You have not selected an image file for upload.' . $this->close_delimiter;
                        $data['error_feedback'] = $feedback;
                        $this->loadAddCommodityView($data);
                        return;
                    }
                    redirect('admin/add-commodity');

                }
            } else {

                $this->loadAddCommodityView($data);
            }

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    private function loadAddCommodityView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/add_commodity', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/scripts/add_commodity');
        $this->load->view('admin/temp/footer_close');
    }

    public function editCommodity()
    {
        if ($this->is_admin) {

            $commodity_slug = $this->uri->segment(3);

            if (!$this->commodity_model->slug_exists($commodity_slug)) {
                show_404();
            } else {

                $commodity = $this->commodity_model->getCommodityBySlug($commodity_slug);
                $data['commodity'] = $commodity;
                $data['categories'] = $this->categories_model->getAllCategories();
                $data['units'] = $this->units_model->getAllUnits();
                $data['statuses'] = $this->commodity_model->get_enum_values('commodities', 'comm_status');
                $data['partners'] = $this->partner_model->getAllPartners();
                $data['types'] = $this->commodity_model->get_enum_values('commodities', 'comm_commodity_type');
                $produce_types = $this->producetype_model->getAllProduceTypes();
    
                if (empty($produce_types)) {
                    $produce_types = array();
                }
                $data['produce_types'] = $produce_types;

                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
                $this->form_validation->set_rules('comm_price_per_unit', 'Price per unit', 'trim|required');
                $this->form_validation->set_rules('comm_cycle', 'Cycle', 'trim|required');
                $this->form_validation->set_rules('quantity', 'Quantity', 'trim|required');
                $this->form_validation->set_rules('quantity_left', 'Quantity Left', 'trim|required');
                $this->form_validation->set_rules('quantity_left_to_buy', 'Quantity Left To Buy', 'trim|required');
                $this->form_validation->set_rules('comm_commodity_details', 'Commodity Details', 'trim|required');
                $this->form_validation->set_rules('comm_insurance_cover', 'Insurance Cover', 'trim|required');
                $this->form_validation->set_rules('comm_google_sheet_link', 'Google Sheet Link', 'trim|required');

                if (isset($_POST['submit'])) {

                    if ($this->form_validation->run() == false) {
                        $this->loadEditCommodityView($data);
                    } else {

                        $type = $this->input->post('type');
                        $quantity = (int)$this->input->post('quantity');
                        $quantity_left = (int)$this->input->post('quantity_left');
                        $quantity_left_to_buy = (int)$this->input->post('quantity_left_to_buy');
                        $commodity_details = (trim($this->input->post('comm_commodity_details')));
                        $insurance_cover = (trim($this->input->post('comm_insurance_cover')));
                        $google_sheet_link = trim($this->input->post('comm_google_sheet_link'));
    
                        // Check if Quantity is negative
                        if($quantity < 0){
                            $feedback = $this->open_delimiter . 'The quantity cannot be negative' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadEditCommodityView($data);
                            return;
                        }
    
                        // Check if Quantity Left is negative
                        if($quantity_left < 0){
                            $feedback = $this->open_delimiter . 'The Quantity Left cannot be negative' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadEditCommodityView($data);
                            return;
                        }
    
                        // Check if Google Sheet Link is a valid url
                        if(!filter_var($google_sheet_link, FILTER_VALIDATE_URL)){
                            $feedback = $this->open_delimiter . 'You entered an invalid URL for Google Sheets Link' . $this->close_delimiter;
                            $data['error_feedback'] = $feedback;
                            $this->loadEditCommodityView($data);
                            return;
                        }

                        $updateICommodity = array(
                            'comm_status' => $this->input->post('status'),
                            'comm_partner_id' => $this->input->post('partner_id'),
                            'comm_commodity_type'=> $type,
                            'comm_price_per_unit' => $this->input->post('comm_price_per_unit'),
                            'comm_cycle' => $this->input->post('comm_cycle'),
                            'comm_unit_id' => $this->input->post('unitSelect'),
                            'comm_insurance_cover' => $insurance_cover,
                            'comm_commodity_details' => $commodity_details,
                            'comm_google_sheet_link' => $google_sheet_link,
                            'comm_quantity' => $quantity,
                            'comm_quantity_left' => $quantity_left,
                            'comm_quantity_left_to_buy' => $quantity_left_to_buy,
                            'comm_edited_by' => $this->loggedInUserID,
                            'comm_date_updated' => date('Y-m-d H:i:s'),
                        );

                        $this->commodity_model->updateCommodity($commodity->comm_id, $updateICommodity);
                        //Show success message
                        $feedback = $this->open_delimiter_success .ucwords($type). ' Commodity has been successfully edited!' . $this->close_delimiter;
                        $this->session->set_flashdata('commodity_feedback', $feedback);
                        // Log data
                        $logData = array(
                            'lg_user_id' => $this->loggedInUserID,
                            'lg_message' => "Edited ". ucwords($type). " Commodity. "."Slug: ".$commodity->comm_slug,
                            );
                        $this->log_model->newLog($logData);

                        if (!empty($_FILES['productImageFile']['name'])) {

                            $path = './mainasset/fe/images/commodities/';
                            $results = $this->do_upload($path, 'productImageFile');
                            if (!isset($results['errors'])) {
                                //add the image
                                $imageData = array(
                                    'comm_image' => $results['file_name'],
                                    'comm_date_updated' => date('Y-m-d H:i:s'),
                                );
                                $this->commodity_model->updateCommodity($commodity->comm_id, $imageData);
                                
                            } else {
                                $image_errors = $results['errors'];
                                $image_errors_output = "";
                                if(!empty($image_errors)){
                                    foreach ($image_errors as $error) {
                                        $image_errors_output .= $this->open_delimiter_success . $error . $this->close_delimiter;
                                    } 
                                }
                                $data['image_error'] = $image_errors_output;
                                $this->loadEditCommodityView($data);
                                return; 
                            }
                        }

                        redirect('admin/edit-commodity/' . $commodity->comm_slug);

                    }
                } else {
                    $this->loadEditCommodityView($data);
                }

            }

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }

    }

    private function loadEditCommodityView($data)
    {
        $this->load->view('admin/temp/header');
        $this->load->view('admin/temp/sidebar');
        $this->load->view('admin/edit_commodity', $data);
        $this->load->view('admin/temp/footer');
        $this->load->view('admin/temp/scripts/edit_commodity');
        $this->load->view('admin/temp/footer_close');
    }

    public function Commodities()
    {
        if ($this->is_admin || $this->is_cc_manager) {

            $logData = array('lg_user_id' => $this->loggedInUserID);

            $investment_commodities = array();
            $keyword = $this->input->get('keyword_invc');
            $type = $this->input->get('comm_type');
            if(empty($type)){
                $type = '';
            }

            $this->load->library('pagination');
            $config = $this->pagination_config;

            // Add the search data in session to populate the fields again
            $this->session->set_userdata('keyword_invc', $keyword);
            $this->session->set_userdata('comm_type', $type);
            $data['keyword_invc'] = $this->session->userdata('keyword_invc');
            $data['comm_type'] = $this->session->userdata('comm_type');

            // var_dump($type);
            // die();
            // Get the page number if it is set
            $page = $this->uri->segment(3);
            $per_page = 10;
            if (empty($page)) {
                $page = 1;
                $offset = 0;
            } else {
                $page = (int) $page;
                $offset = $page * $per_page;
            }

            if(empty($type) && empty($keyword)){
                $commodities = $this->commodity_model->getCommodities($per_page, $offset, 'comm_id', 'DESC');
                $config['total_rows'] = $this->commodity_model->getTotalCommodities();
                $data['total_commodities'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed '.ucwords($type).' Page. "."Page: ".$page;

            }elseif(empty($type) && !empty($keyword)){
                $commodities = $this->commodity_model->getCommoditiesWithName($keyword, $per_page, $offset);
                $config['total_rows'] = $this->commodity_model->getTotalCommoditiesWithName($keyword);
                $data['total_commodities'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed '.ucwords($type).' Page. "."Page: ".$page.". Searched for: [".$keyword."]";

            }elseif(!empty($type) && empty($keyword)){
                $commodities = $this->commodity_model->getCommoditiesByType($per_page, $offset, $type, 'comm_id', 'DESC');
                $config['total_rows'] = $this->commodity_model->getTotalCommoditiesByType($type);
                $data['total_commodities'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed '.ucwords($type).' Page. "."Page: ".$page.". Type: ".$type;
                
            }elseif(!empty($type) && !empty($keyword)){
                $commodities = $this->commodity_model->getCommoditiesWithNameAndType($keyword, $per_page, $offset, $type, 'comm_id', 'DESC');
                $config['total_rows'] = $this->commodity_model->getTotalCommoditiesWithNameAndType($keyword, $type);
                $data['total_commodities'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed '.ucwords($type).' Commodities Page. "."Page: ".$page.". Searched for [".$keyword."]";
            }

            // var_dump($commodities);
            // die();

            if (!empty($commodities)) {
                foreach ($commodities as $commodity) {
                    $manager = $this->user_model->getUser($commodity->comm_created_by);
                    $commodity->manager = $manager->full_name;

                    $category = $this->categories_model->getCategoryName($commodity->comm_produce_id);
                    $commodity->category = $category;

                    $unit = $this->units_model->getUnitName($commodity->comm_unit_id);
                    $commodity->unit = $unit;
                }
                $data['commodities'] = $commodities;
            }else{
                $data['commodities'] = array();
            }

            $data['commodities'] = $commodities;

            $config['base_url'] = base_url("admin/commodities/");
            $config['per_page'] = $per_page;

            $this->pagination->initialize($config);

            $data['pagination_links'] = $this->pagination->create_links();

            $this->load->view('admin/temp/header');
            $this->load->view('admin/temp/sidebar');
            $this->load->view('admin/all_commodities', $data);
            $this->load->view('admin/temp/footer');

            // Log data
            $this->log_model->newLog($logData);

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function delCommodity()
    {
        if ($this->is_admin) {

            $data = array();
            $commodity_slug = $this->uri->segment(3);
            if (!$this->commodity_model->slug_exists($commodity_slug)) {
                show_404();
            } else {

                $Commodity = $this->commodity_model->getCommodityBySlug($commodity_slug);

                if ($Commodity !== false) {
                    $this->commodity_model->hardDelete($Commodity->comm_id);

                    $feedback = $this->open_delimiter_success . 'Commodity - ' . $Commodity->comm_slug . ' has been successfully deleted!  ' . $this->close_delimiter;
                    $this->session->set_flashdata('commodity_del_feedback', $feedback);

                    // Log data
                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Deleted '.ucwords($Commodity->comm_commodity_type).' Commodity. "."Slug: ".$Commodity->comm_slug;
                    $this->log_model->newLog($logData);

                }

                redirect('admin/commodities');

            }
        }
    }

    public function CommodityDetails()
    {
        if ($this->is_admin) {

            $data = array();
            $commodity_slug = $this->uri->segment(3);
            if (!$this->commodity_model->slug_exists($commodity_slug)) {
                show_404();
            } else {

                $commodity = $this->commodity_model->getCommodityBySlug($commodity_slug);

                if ($commodity !== false) {
                    $manager = $this->user_model->getUser($commodity->comm_created_by);
                    $commodity->manager = $manager;

                    $category = $this->categories_model->getCategoryName($commodity->comm_produce_id);
                    $commodity->category = $category;

                    $unit = $this->units_model->getUnitName($commodity->comm_unit_id);
                    $commodity->unit = $unit;

                    $commodity->partner = $this->partner_model->getPartnerName($commodity->comm_partner_id);

                    $data['commodity'] = $commodity;

                    $this->load->view('admin/temp/header');
                    $this->load->view('admin/temp/sidebar');
                    $this->load->view('admin/commodity_details', $data);
                    $this->load->view('admin/temp/footer');

                    // Log data
                    $logData = array('lg_user_id' => $this->loggedInUserID);
                    $logData['lg_message'] = "Viewed '.ucwords($commodity->comm_commodity_type).'Commodity Details. "."Slug: ".$commodity->comm_slug;
                    $this->log_model->newLog($logData);

                }else{
                    show_404();
                }

            }
        }
    }

    public function contacts()
    {
        if ($this->is_admin) {

            $logData = array('lg_user_id' => $this->loggedInUserID);

            $contacts = array();
            $keyword = $this->input->get('keyword_contact');

            $this->load->library('pagination');
            $config = $this->pagination_config;

            // Add the search data in session to populate the fields again
            $this->session->set_userdata('keyword_contact', $keyword);
            $data['keyword_contact'] = $this->session->userdata('keyword_contact');

            // Get the page number if it is set
            $page = $this->uri->segment(3);
            $per_page = 10;
            if (empty($page)) {
                $page = 1;
                $offset = 0;
            } else {
                $page = (int) $page;
                $offset = $page * $per_page;
            }

            if (!empty($keyword)) {
                $contacts = $this->contacts_model->getContactsWithName($keyword, $per_page, $offset, 'con_id', 'DESC');
                $config['total_rows'] = $this->contacts_model->getTotalContactsWithName($keyword);
                $data['total_contacts'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Contacts Page. "."Page: ".$page.". Searched for [".$keyword."]";


            } else {
                $contacts = $this->contacts_model->getContacts($per_page, $offset, 'con_id', 'DESC');
                $config['total_rows'] = $this->contacts_model->getTotalContacts();
                $data['total_contacts'] = $config['total_rows'];
                $logData['lg_message'] = "Viewed Contacts Page. "."Page: ".$page;

            }

            if(!empty($contacts)){
                foreach($contacts as $contact){


                }

            }

            $data['contacts'] = $contacts;

            $config['base_url'] = base_url("admin/contacts/");
            $config['per_page'] = $per_page;

            $this->pagination->initialize($config);

            $data['pagination_links'] = $this->pagination->create_links();

            $this->load->view('admin/temp/header');
            $this->load->view('admin/temp/sidebar');
            $this->load->view('admin/all_contacts', $data);
            $this->load->view('admin/temp/footer');

            $this->log_model->newLog($logData);

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function contactDetails()
    {
        if ($this->is_admin) {

            $data = array();
            $contact_id = $this->uri->segment(3);

            $contact = $this->contacts_model->getContact($contact_id);

            if ($contact !== false) {

                $data['contact'] = $contact;

                $this->load->view('admin/temp/header');
                $this->load->view('admin/temp/sidebar');
                $this->load->view('admin/contact_details', $data);
                $this->load->view('admin/temp/footer');

                    // Log data
                $logData = array('lg_user_id' => $this->loggedInUserID);
                $logData['lg_message'] = "Viewed '.ucwords($contact->con_id).'Contact Details. ";
                $this->log_model->newLog($logData);

            } else {
                show_404();
            }
        }
    }
      
    public function getProduceCategories()
    {
        $response = array();
        if (!$this->input->is_ajax_request()) {
            return;
        } else {
            $produce_type_id = $this->input->post('produce_type_id');
            $categories = $this->categories_model->getAllSubCategoriesByProduceType($produce_type_id);
            if (empty($categories)) {
                $categories = array();
            }
            $response['code'] = 1;
            $response['categories'] = $categories;
        }
        echo json_encode($response);

    }

    public function category()
    {
        if ($this->is_admin) {

            $user_id = $this->session->userdata('uid');
            // var_dump($user_id);
            // die();
            $user = $this->user_model->getUser($user_id);

            $data['user'] = $user;

            $categories = $this->categories_model->getParentCategories();

            if (!empty($categories)) {
                foreach ($categories as $category) {
                    $category->subcategories = $this->categories_model->getSubCategories($category->c_id);
                }
            }

            $produce_types = $this->producetype_model->getAllProduceTypes();
            if (empty($produce_types)) {
                $produce_types = array();
            }

            $data['categories'] = $categories;
            $data['produce_types'] = $produce_types;

            $this->load->view('admin/temp/header');
            $this->load->view('admin/temp/sidebar');
            $this->load->view('admin/category', $data);
            $this->load->view('admin/temp/footer');
            $this->load->view('admin/temp/scripts/category');
            $this->load->view('admin/temp/footer_close');
            // Log data
            $logData = array(
                'lg_user_id' => $this->loggedInUserID,
                'lg_message' => "Viewed Category Page ",
                );
            $this->log_model->newLog($logData);

        } else {
            // Store the redirect path to a session variable
            $this->session->set_userdata('admin_redirect_url', $_SERVER['REQUEST_URI']);
            redirect('error403');
        }
    }

    public function latestCategories()
    {
        $categories = $this->categories_model->getParentCategories();

        if (!empty($categories)) {
            foreach ($categories as $category) {
                $category->subcategories = $this->categories_model->getSubCategories($category->c_id);
            }
        } else {
            $categories = array();
        }

        $data['categories'] = $categories;
        $this->load->view('admin/latest_categories', $data);
    }

    public function loadCategory()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $categories = $this->categories_model->getParentCategories();

        if (!empty($categories)) {
            foreach ($categories as $category) {
                $category->subcategories = $this->categories_model->getSubCategories($category->c_id);
            }
        }

        echo json_encode($categories);
    }

    public function newCategory()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $response = array();
        $c_parent_id = $this->input->post('c_parent_id');
        $c_name = ucwords($this->input->post('c_name'));
        $c_type_id = $this->input->post('c_type_id');
        // New Category
        $newCategoryData = array(
            'c_name' => $c_name,
            'c_parent_id' => $c_parent_id,
            'c_type_id' => $c_type_id,

        );
        // Check if the name exists already
        $cat_exists = $this->categories_model->categoryExists($c_name);
        if ($cat_exists) {
            $response = array("code" => 0, "message" => "Category : " . $c_name . " already exists!");
        } else {
            $this->categories_model->newCategory($newCategoryData);
            $response = array("code" => 1, "message" => "Category : " . $c_name . " has been created!");
            // Log data
            $logData = array(
                'lg_user_id' => $this->loggedInUserID,
                'lg_message' => "Created Category. "."Category Name: ".$c_name,
                );
            $this->log_model->newLog($logData);
        }

        echo json_encode($response);
    }

    public function removeCategory()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $response = array();
        $c_id = $this->input->post('c_id');

        // Get and delete the subcategories
        $subcategories = $this->categories_model->getSubCategories($c_id);
        if (!empty($subcategories)) {

            foreach ($subcategories as $subcategory) {
                $this->categories_model->hardDelete($subcategory->c_id);
            }
        }
        $this->categories_model->hardDelete($c_id);

        $response = array("code" => 1, "message" => "Category :" . $c_id . " has been deleted!");
        // Log data
        $logData = array(
            'lg_user_id' => $this->loggedInUserID,
            'lg_message' => "Deleted Category. "."Category ID: ".$c_id,
            );
        $this->log_model->newLog($logData);

        echo json_encode($response);
    }

    public function messages()
    {
        if ($this->is_admin) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);

            $data['user'] = $user;

            $orders = $this->orders_model->getOrders(100, 0);
            $data['orders'] = $orders;

            $this->load->view('admin/temp/header');
            $this->load->view('admin/temp/sidebar');
            $this->load->view('admin/messages', $data);
            $this->load->view('admin/temp/footer');
            $this->load->view('admin/temp/scripts/messages', $data);
            $this->load->view('admin/temp/footer_close');
            // Log data
            $logData = array(
                'lg_user_id' => $this->loggedInUserID,
                'lg_message' => "Viewed Messages Page. ",
                );
            $this->log_model->newLog($logData);

        } else {
            redirect('error403');
        }
    }

    public function messageChat()
    {
        if ($this->is_admin) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);

            $data['user'] = $user;

            $order_number = $this->uri->segment(3);
            $order = $this->orders_model->getOrderByNumber($order_number);
            $data['order'] = $order;

            $this->load->view('admin/messagechat', $data);
            $this->load->view('admin/temp/scripts/messagechat', $data);
        } else {
            redirect('error403');
        }
    }

    public function postMessage()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        // Get all the data
        $admin_id = $this->session->userdata('uid');
        // $admin_id = 0;
        $user_type = $this->user_model->getUser($admin_id)->type;
        $message = $this->input->post('message_text');
        $order_id = $this->input->post('order_id');
        $user_id = $this->orders_model->getOrder($order_id)->or_user_id;

        $responses = array();

        if (!empty($message)) {
            // Arrange the data
            $messageData = array(
                'm_order_id' => $order_id,
                'm_sender_id' => $admin_id,
                'm_sender_type' => $user_type,
                'm_receiver_id' => $user_id,
                'm_receiver_type' => 'user',
                'm_message' => $message,
            );
            // save it into the database
            $last_id = $this->messages_model->newMessage($messageData);

            // Send response back to Ajax
            $response = array('id' => $last_id, 'type' => 'text', 'content' => $message, 'cdate' => date('h:i a - d M Y'));
            array_push($responses, $response);
        }

        if (!empty($_FILES['messageFile']['name'])) {
            $path = './mainasset/fe/images/messages/';
            $results = $this->do_upload($path, 'messageFile');
            if (!isset($results['errors'])) {
                $imageData = array(
                    'm_order_id' => $order_id,
                    'm_sender_id' => $user_id,
                    'm_sender_type' => $user_type,
                    'm_receiver_id' => $admin_id,
                    'm_receiver_type' => 'admin',
                    'm_file' => $results['file_name'],
                );
                $last_id = $this->messages_model->newMessage($imageData);
                $response = array('id' => $last_id, 'sender' => $user_type, 'type' => 'image', 'content' => $results['file_name'], 'cdate' => date('h:i a - d M Y'));
                array_push($responses, $response);
            } else {
                $response = array('type' => 'errors', 'sender' => $user_type, 'content' => $results['errors'], 'cdate' => date('h:i a - d M Y'));
                array_push($responses, $response);
            }
        }
        echo json_encode($responses);
    }

    public function loadMessages()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        // Get all the data
        $user_id = $this->session->userdata('uid');
        $user_type = $this->user_model->getUser($user_id)->type;
        $last_id = $this->input->post('last_id');
        $order_id = $this->input->post('order_id');
        $initial_load = $this->input->post('initial_load');
        $limit = 10;

        $messages = '';
        $responses = array();
        if (!empty($initial_load)) {
            $messages = $this->messages_model->getMostRecentMessagesByOrder($order_id, $limit);
        } else {
            $messages = $this->messages_model->getPreviousMessagesOnOrder($order_id, $last_id, $limit);

        }
        if (!empty($messages)) {
            foreach ($messages as $message) {
                if (!empty($message->m_file)) {
                    $response = array('id' => $message->m_id, 'sender' => $message->m_sender_type, 'type' => 'image', 'content' => $message->m_file, 'cdate' => date('h:i a - d M Y', strtotime($message->m_date_sent)));
                    array_push($responses, $response);
                } else {
                    $response = array('id' => $message->m_id, 'sender' => $message->m_sender_type, 'type' => 'text', 'content' => $message->m_message, 'cdate' => date('h:i a - d M Y', strtotime($message->m_date_sent)));
                    array_push($responses, $response);
                }
            }
        }
        // Send response back to Ajax
        echo json_encode($responses);
    }

    public function newMessage()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        // Get all the data
        $user_id = $this->session->userdata('uid');
        $user_type = $this->user_model->getUser($user_id)->type;
        $last_id = $this->input->post('last_id');
        $order_id = $this->input->post('order_id');

        $messages = '';
        $responses = array();
        $message = $this->messages_model->getLastMessageByOrder($order_id);

        if (!empty($message)) {

            if ($last_id < $message->m_id) {
                if (!empty($message->m_file)) {
                    $response = array('id' => $message->m_id, 'sender' => $message->m_sender_type, 'type' => 'image', 'content' => $message->m_file, 'cdate' => date('h:i a - d M Y', strtotime($message->m_date_sent)));
                    array_push($responses, $response);
                } else {
                    $response = array('id' => $message->m_id, 'sender' => $message->m_sender_type, 'type' => 'text', 'content' => $message->m_message, 'cdate' => date('h:i a - d M Y', strtotime($message->m_date_sent)));
                    array_push($responses, $response);
                }
            }

        }
        // Send response back to Ajax
        echo json_encode($responses);
    }

    private function upload_files($path, $title, $files)
    {
        $config = array(
            'upload_path' => $path,
            'allowed_types' => 'jpg|jpeg|png|pdf',
            'overwrite' => false,
            'max_size' => 2048,
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name'] = $files['name'][$key];
            $_FILES['images[]']['type'] = $files['type'][$key];
            $_FILES['images[]']['tmp_name'] = $files['tmp_name'][$key];
            $_FILES['images[]']['error'] = $files['error'][$key];
            $_FILES['images[]']['size'] = $files['size'][$key];

            $fileName = $title . '_' . $image;

            $images[] = $fileName;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                $image_errors = array('errors' => $this->upload->display_errors());
                $image_errors['errors'] = $this->open_delimiter . $image_errors['errors'] . $this->close_delimiter;
                return $image_errors;
            }
        }

        return $images;
    }

    public function do_upload($path, $file)
    {

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'jpg|png|jpeg|pdf';
        $config['max_size'] = 2048;
        $config['overwrite'] = false;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $results = array();

        if ($this->upload->do_upload($file)) {
            $results = $this->upload->data();
            return $results;
        } else {
            $results = array('errors' => $this->upload->display_errors());
            $results['errors'] = $this->open_delimiter . $results['errors'] . $this->close_delimiter;
            return $results;
        }
    }

    public function check_email_exists($email)
    {
        $this->form_validation->set_message('check_email_exists', 'That email is taken. Please choose a different one');

        if ($this->user_model->check_email_exists($email)) {
            return true;
        } else {
            return false;
        }
    }

}
