<?php

class Home extends CI_Controller
{

    // Roles
    private $loggedIn;
    private $loggedInUserID;
    private $user;

    // pagination configuration
    private $pagination_config = array(
        'use_page_numbers' => true,
        'reuse_query_string' => true,
        'full_tag_open' => '<nav aria-label="Page navigation"><ul class="pagination mt-4 justify-content-end d-flex">',
        'full_tag_close' => '</ul></nav>',
        'first_tag_open' => '<li class="page-item">',
        'first_tag_close' => '</div>',
        'first_link' => 'First',
        'attributes' => array('class' => 'page-link'),
        'cur_tag_open' => '&nbsp,<a class="page-link">',
        'cur_tag_close' => '</a>',
        'next_link' => '&raquo;',
        'prev_link' => '&laquo;',
        'num_links' => 3,
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->loggedIn = $this->session->userdata('loggedIn');
        $this->loggedInUserID = $this->session->userdata('uid');
        $this->user = $this->user_model->getUser($this->loggedInUserID);
    }

    public function index()
    {

        $pcommodities = $this->pcommodity_model->getPCommodities(4, 0, 'purc_date_created', 'ASC');
        $icommodities = $this->icommodity_model->getICommodities(4, 0, 'invc_date_created', 'ASC');

        if (!empty($pcommodities)) {
            foreach ($pcommodities as $commodity) {
                # code...
                $commodity->unit = $this->units_model->getUnitName($commodity->purc_unit_id);
            }
        }
        if (!empty($icommodities)) {
            foreach ($icommodities as $commodity) {
                # code...
                $commodity->unit = $this->units_model->getUnitName($commodity->invc_unit_id);
            }
        }
        $data['pcommodities'] = $pcommodities;
        $data['icommodities'] = $icommodities;

        $this->loadHeader();
        $this->load->view('home/index', $data);
        $this->load->view('home/temp/footer');

    }

    public function aboutUs()
    {

        $pcommodities = $this->pcommodity_model->getPCommodities(4, 0, 'purc_date_created', 'ASC');
        $icommodities = $this->icommodity_model->getICommodities(4, 0, 'invc_date_created', 'ASC');

        if (!empty($pcommodities)) {
            foreach ($pcommodities as $commodity) {
                # code...
                $commodity->unit = $this->units_model->getUnitName($commodity->purc_unit_id);
            }
        }
        if (!empty($icommodities)) {
            foreach ($icommodities as $commodity) {
                # code...
                $commodity->unit = $this->units_model->getUnitName($commodity->invc_unit_id);
            }
        }
        $data['pcommodities'] = $pcommodities;
        $data['icommodities'] = $icommodities;

        $this->loadHeader();
        $this->load->view('home/aboutus', $data);
        $this->load->view('home/temp/footer');

    }

    public function contactUs()
    {

        $pcommodities = $this->pcommodity_model->getPCommodities(4, 0, 'purc_date_created', 'ASC');
        $icommodities = $this->icommodity_model->getICommodities(4, 0, 'invc_date_created', 'ASC');

        if (!empty($pcommodities)) {
            foreach ($pcommodities as $commodity) {
                # code...
                $commodity->unit = $this->units_model->getUnitName($commodity->purc_unit_id);
            }
        }
        if (!empty($icommodities)) {
            foreach ($icommodities as $commodity) {
                # code...
                $commodity->unit = $this->units_model->getUnitName($commodity->invc_unit_id);
            }
        }
        $data['pcommodities'] = $pcommodities;
        $data['icommodities'] = $icommodities;

        $this->loadHeader();
        $this->load->view('home/contactus', $data);
        $this->load->view('home/temp/footer');

    }

    public function transSuccess()
    {

        $pcommodities = $this->pcommodity_model->getPCommodities(4, 0, 'purc_date_created', 'ASC');
        $icommodities = $this->icommodity_model->getICommodities(4, 0, 'invc_date_created', 'ASC');

        if (!empty($pcommodities)) {
            foreach ($pcommodities as $commodity) {
                # code...
                $commodity->unit = $this->units_model->getUnitName($commodity->purc_unit_id);
            }
        }
        if (!empty($icommodities)) {
            foreach ($icommodities as $commodity) {
                # code...
                $commodity->unit = $this->units_model->getUnitName($commodity->invc_unit_id);
            }
        }
        $data['pcommodities'] = $pcommodities;
        $data['icommodities'] = $icommodities;

        $this->loadHeader();
        $this->load->view('home/trans_success', $data);
        $this->load->view('home/temp/footer');

    }

    public function howItWorks()
    {

        $pcommodities = $this->pcommodity_model->getPCommodities(4, 0, 'purc_date_created', 'ASC');
        $icommodities = $this->icommodity_model->getICommodities(4, 0, 'invc_date_created', 'ASC');

        if (!empty($pcommodities)) {
            foreach ($pcommodities as $commodity) {
                # code...
                $commodity->unit = $this->units_model->getUnitName($commodity->purc_unit_id);
            }
        }
        if (!empty($icommodities)) {
            foreach ($icommodities as $commodity) {
                # code...
                $commodity->unit = $this->units_model->getUnitName($commodity->invc_unit_id);
            }
        }
        $data['pcommodities'] = $pcommodities;
        $data['icommodities'] = $icommodities;

        $this->loadHeader();
        $this->load->view('home/howitworks', $data);
        $this->load->view('home/temp/footer');

    }

    public function toc()
    {

        // $pcommodities = $this->pcommodity_model->getPCommodities(4, 0, 'purc_date_created', 'ASC');
        // $icommodities = $this->icommodity_model->getICommodities(4, 0, 'invc_date_created', 'ASC');

        // if (!empty($pcommodities)) {
        //     foreach ($pcommodities as $commodity) {
        //         # code...
        //         $commodity->unit = $this->units_model->getUnitName($commodity->purc_unit_id);
        //     }
        // }
        // if (!empty($icommodities)) {
        //     foreach ($icommodities as $commodity) {
        //         # code...
        //         $commodity->unit = $this->units_model->getUnitName($commodity->invc_unit_id);
        //     }
        // }
        // $data['pcommodities'] = $pcommodities;
        // $data['icommodities'] = $icommodities;
        $data = array();

        $this->loadHeader();
        $this->load->view('home/toc', $data);
        $this->load->view('home/temp/footer');

    }

    public function privacy()
    {

        // $pcommodities = $this->pcommodity_model->getPCommodities(4, 0, 'purc_date_created', 'ASC');
        // $icommodities = $this->icommodity_model->getICommodities(4, 0, 'invc_date_created', 'ASC');

        // if (!empty($pcommodities)) {
        //     foreach ($pcommodities as $commodity) {
        //         # code...
        //         $commodity->unit = $this->units_model->getUnitName($commodity->purc_unit_id);
        //     }
        // }
        // if (!empty($icommodities)) {
        //     foreach ($icommodities as $commodity) {
        //         # code...
        //         $commodity->unit = $this->units_model->getUnitName($commodity->invc_unit_id);
        //     }
        // }
        // $data['pcommodities'] = $pcommodities;
        // $data['icommodities'] = $icommodities;
        $data = array();

        $this->loadHeader();
        $this->load->view('home/privacy', $data);
        $this->load->view('home/temp/footer');

    }

    public function allproducts()
    {
        $this->load->library('pagination');
        $config = $this->pagination_config;

        $products = array();
        $keyword = $this->input->get('keyword');
        $search_states = $this->input->get('states');
        $use_volume = $this->input->get('use_volume');
        $min_volume = 0;
        $max_volume = 0;
        if ($use_volume === "on") {
            $amount = trim($this->input->get('amount'));

            if (!empty($amount)) {
                $amount_array = explode(" ", $amount);
                $amount_length = count($amount_array);

                if ($amount_length > 0) {
                    $min_volume = $amount_array[0];
                    $max_volume = $amount_array[$amount_length - 2];
                }
            }
        } else {
            $amount = '';
        }

        // Add the search data in session to populate the fields again
        $this->session->set_userdata('keyword', $keyword);
        $this->session->set_userdata('search_states', $search_states);
        $this->session->set_userdata('use_volume', $use_volume);
        $this->session->set_userdata('min_volume', $min_volume);
        $this->session->set_userdata('max_volume', $max_volume);
        $data['keyword'] = $this->session->userdata('keyword');
        $data['use_volume'] = $this->session->userdata('use_volume');
        $data['min_volume'] = $this->session->userdata('min_volume');
        $data['max_volume'] = $this->session->userdata('max_volume');
        if (!empty($this->session->userdata('search_states'))) {
            $data['search_states'] = $this->session->userdata('search_states');
        } else {
            $data['search_states'] = array();
        }
        // Setup pagination info
        $currentURL = current_url();
        $params = $_SERVER['QUERY_STRING'];

        $fullURL = $currentURL . '?' . $params;
        // print_r($fullURL);
        $config['per_page'] = 6;
        $page = explode('/', $fullURL);
        $page = $this->uri->segment(3);

        if (!empty($page)) {
            $page = (int)$page;
            $offset = $config["per_page"] * $page;
        } else {
            $page = 1;
            $offset = 0;
        }

        // Only States
        if (empty($keyword) && !empty($search_states) && empty($use_volume)) {
            // var_dump("Search by States only");
            $products = $this->product_model->getProductsByState($search_states, $config['per_page'], $offset);
            $total_products = $this->product_model->getTotalProductsByState($search_states);
            $config['total_rows'] = $total_products;
            $data['total_products'] = $total_products;
        }
        // Only Name
        elseif (!empty($keyword) && empty($search_states) && empty($use_volume)) {
            // var_dump("Name search");
            $products = $this->product_model->getProductsByName($keyword, $config['per_page'], $offset);
            $total_products = $this->product_model->getTotalProductsByName($keyword);
            $config['total_rows'] = $total_products;
            $data['total_products'] = $total_products;
        }
        // Only Volume
        elseif ($use_volume === "on" && empty($keyword) && empty($search_states)) {
            // var_dump("Volume search");
            $products = $this->product_model->getProductsByVolume($min_volume, $max_volume, $config['per_page'], $offset);
            $total_products = $this->product_model->getTotalProductsByVolume($min_volume, $max_volume);
            $config['total_rows'] = $total_products;
            $data['total_products'] = $total_products;
        }
        // Name and State
        elseif (!empty($keyword) && !empty($search_states) && empty($use_volume)) {
            // var_dump("Name and States search");
            $products = $this->product_model->getProductsByNameAndStates($keyword, $search_states, $config['per_page'], $offset);
            $total_products = $this->product_model->getTotalProductsByNameAndStates($keyword, $search_states);
            $config['total_rows'] = $total_products;
            $data['total_products'] = $total_products;
        }

        // Name and Volume
        elseif (($use_volume === "on") && !empty($keyword) && empty($search_states)) {
            // var_dump("Name and Volume and search");
            $products = $this->product_model->getProductsByNameAndVolume($keyword, $min_volume, $max_volume, $config['per_page'], $offset);
            $total_products = $this->product_model->getTotalProductsByNameAndVolume($keyword, $min_volume, $max_volume);
            $config['total_rows'] = $total_products;
            $data['total_products'] = $total_products;
        }
        // State and Volume
        elseif ($use_volume === "on" && !empty($search_states) && empty($keyword)) {
            // var_dump("Volume and States search");
            $products = $this->product_model->getProductsByStatesAndVolume($search_states, $min_volume, $max_volume, $config['per_page'], $offset);
            $total_products = $this->product_model->getTotalProductsByStatesAndVolume($search_states, $min_volume, $max_volume);
            $config['total_rows'] = $total_products;
            $data['total_products'] = $total_products;
        }
        // Name, Volume and States
        elseif ($use_volume === "on" && !empty($keyword) && !empty($search_states)) {
            // var_dump("Volume and Name and States search");
            $products = $this->product_model->getProductsByNameAndStatesAndVolume($keyword, $search_states, $min_volume, $max_volume, $config['per_page'], $offset);
            $total_products = $this->product_model->getTotalProductsByNameAndStatesAndVolume($keyword, $search_states, $min_volume, $max_volume);
            $config['total_rows'] = $total_products;
            $data['total_products'] = $total_products;
        } else {
            //   var_dump("Default");
            $products = $this->product_model->getProducts($config['per_page'], $offset);
            $total_products = $this->product_model->getTotalProducts();
            $config['total_rows'] = $total_products;
            $data['total_products'] = $total_products;
        }

        if (!empty($products) && ($products !== false)) {
            foreach ($products as $product) {
                # code...
                $product->state = $this->state_model->getStateName($product->prod_states_id);
                $product->local = $this->locals_model->getLocalName($product->prod_locals_id);
                $product->unit = $this->units_model->getUnitName($product->prod_units_id);
                $product->category = $this->categories_model->getCategoryName($product->prod_category_id);
            }
            $data['products'] = $products;
        } else {
            $products = array();
        }

        $config['base_url'] = base_url('products') . "/pg/";

        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['categories'] = $this->categories_model->getAllCategories();

        $this->loadHeader();
        // $this->load->view('home/temp/subheader', $data);
        $this->load->view('home/trading_marketplace', $data);
        $this->load->view('home/temp/footer');
        $this->load->view('home/temp/scripts/search_products', $data);
        $this->load->view('home/temp/footer_close');

    }

    public function invMarketPlace()
    {
        $this->load->library('pagination');
        $config = $this->pagination_config;

        $commodities = array();
        $statuses = $this->icommodity_model->get_enum_values('investment_commodities', 'invc_status');
        $data['statuses'] = $statuses;
        $keyword = $this->input->get('keyword');

        $page = $this->uri->segment(3);
        $per_page = 9;
        $config['per_page'] = $per_page;
        if (!empty($page)) {
            $page = (int)$page;
            $offset = $per_page * $page;
        } else {
            $page = 1;
            $offset = 0;
        }

        $commodities = $this->icommodity_model->getICommodities($per_page, $offset, 'invc_date_created', 'ASC');
        $total_commodities = $this->icommodity_model->getTotalICommodities();
        $config['total_rows'] = $total_commodities;
        $data['total_commodities'] = $total_commodities;

        if (!empty($commodities)) {
            foreach ($commodities as $iCommodity) {
                # code...
                $iCommodity->unit = $this->units_model->getUnitName($iCommodity->invc_unit_id);
                $iCommodity->category = $this->categories_model->getCategoryName($iCommodity->invc_produce_id);
            }
        }
        $data['commodities'] = $commodities;
        $config['base_url'] = base_url('investment-market/pg/');

        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $this->loadHeader();
        $this->load->view('home/investment_marketplace', $data);
        $this->load->view('home/temp/footer');
        $this->load->view('home/temp/scripts/search_products', $data);
        $this->load->view('home/temp/footer_close');

    }

    public function purMarketPlace()
    {
        $this->load->library('pagination');
        $config = $this->pagination_config;

        $commodities = array();
        $statuses = $this->pcommodity_model->get_enum_values('purchase_commodities', 'purc_status');
        $data['statuses'] = $statuses;
        $keyword = $this->input->get('keyword_purc');

        $page = $this->uri->segment(3);
        $per_page = 9;
        $config['per_page'] = $per_page;
        if (!empty($page)) {
            $page = (int)$page;
            $offset = $per_page * $page;
        } else {
            $page = 1;
            $offset = 0;
        }

        $commodities = $this->pcommodity_model->getPCommodities($per_page, $offset, 'purc_date_created', 'ASC');
        $total_commodities = $this->pcommodity_model->getTotalPCommodities();
        $config['total_rows'] = $total_commodities;
        $data['total_commodities'] = $total_commodities;

        if (!empty($commodities)) {
            foreach ($commodities as $pCommodity) {
                # code...
                $pCommodity->unit = $this->units_model->getUnitName($pCommodity->purc_unit_id);
                $pCommodity->category = $this->categories_model->getCategoryName($pCommodity->purc_produce_id);
            }
        }
        $data['commodities'] = $commodities;
        $config['base_url'] = base_url('purchase-market/pg/');

        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        $this->loadHeader();
        $this->load->view('home/purchase_marketplace', $data);
        $this->load->view('home/temp/footer');
        $this->load->view('home/temp/scripts/search_products', $data);
        $this->load->view('home/temp/footer_close');

    }

    public function searchCommodities()
    {

        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library('pagination');
        $config = $this->pagination_config;

        $data = array();
        $commodities = array();
        $sortBy = $this->input->post('sortBy');
        $statuses = $this->input->post('statuses');
        $statuses = $this->remove_empty($statuses);
        $keyword = $this->input->post('keyword');
        $commodity_type = $this->input->post('comm_type');

        $per_page = 9;
        $config['per_page'] = $per_page;
        if (!empty($page)) {
            $page = (int)$page;
            $offset = $per_page * $page;
        } else {
            $page = 1;
            $offset = 0;
        }
        // echo json_encode($statuses);
        if ($commodity_type == "purc") {

        // Only Status
            if (empty($keyword) && !empty($statuses)) {
            // var_dump("Search by Status only");
                // echo json_encode('Here');
                $commodities = $this->pcommodity_model->getPCommoditiesByStatus($statuses, $config['per_page'], $offset, $sortBy, 'DESC');
                $total_commodities = $this->pcommodity_model->getTotalPCommoditiesByStatus($statuses);
                $config['total_rows'] = $total_commodities;
                $data['total_commodities'] = $total_commodities;
            }
        // Only Name
            elseif (!empty($keyword) && empty($statuses)) {
            // var_dump("Name search");
                $commodities = $this->pcommodity_model->getPCommoditiesByName($keyword, $config['per_page'], $offset, $sortBy, 'DESC');
                $total_commodities = $this->pcommodity_model->getTotalPCommoditiesByName($keyword);
                $config['total_rows'] = $total_commodities;
                $data['total_commodities'] = $total_commodities;
            }
            // Both Name and Status
            elseif (!empty($keyword) && !empty($statuses)) {
                // var_dump("Name And Status");
                $commodities = $this->pcommodity_model->getPCommoditiesByNameAndStatus($keyword, $statuses, $config['per_page'], $offset, $sortBy, 'DESC');
                $total_commodities = $this->pcommodity_model->getTotalPCommoditiesByNameAndStatus($keyword, $statuses);
                $config['total_rows'] = $total_commodities;
                $data['total_commodities'] = $total_commodities;
            }
        // // Only Sort
            else {
            // var_dump("Sort search");
                $commodities = $this->pcommodity_model->getPCommodities($config['per_page'], $offset, $sortBy, 'ASC');
                $total_commodities = $this->pcommodity_model->getTotalPCommodities();
                $config['total_rows'] = $total_commodities;
                $data['total_commodities'] = $total_commodities;
            }
            if (!empty($commodities)) {
                foreach ($commodities as $pCommodity) {
                    # code...
                    $pCommodity->unit = $this->units_model->getUnitName($pCommodity->purc_unit_id);
                }
            }

            $config['base_url'] = base_url("purchase-market/pg/");
            $config['per_page'] = $per_page;

        } elseif ($commodity_type == "invc") {
            // Only Status
            if (empty($keyword) && !empty($statuses)) {
                // var_dump("Search by Status only");
                // echo json_encode('Here');
                $commodities = $this->icommodity_model->getICommoditiesByStatus($statuses, $config['per_page'], $offset, $sortBy, 'DESC');
                $total_commodities = $this->icommodity_model->getTotalICommoditiesByStatus($statuses);
                $config['total_rows'] = $total_commodities;
                $data['total_commodities'] = $total_commodities;
            }
            // Only Name
            elseif (!empty($keyword) && empty($statuses)) {
                // var_dump("Name search");
                $commodities = $this->icommodity_model->getICommoditiesByName($keyword, $config['per_page'], $offset, $sortBy, 'DESC');
                $total_commodities = $this->icommodity_model->getTotalICommoditiesByName($keyword);
                $config['total_rows'] = $total_commodities;
                $data['total_commodities'] = $total_commodities;
            }
            // Both Name and Status
            elseif (!empty($keyword) && !empty($statuses)) {
                // var_dump("Name And Status");
                $commodities = $this->icommodity_model->getICommoditiesByNameAndStatus($keyword, $statuses, $config['per_page'], $offset, $sortBy, 'DESC');
                $total_commodities = $this->icommodity_model->getTotalICommoditiesByNameAndStatus($keyword, $statuses);
                $config['total_rows'] = $total_commodities;
                $data['total_commodities'] = $total_commodities;
            }
            // // Only Sort
            else {
                // var_dump("Sort search");
                $commodities = $this->icommodity_model->getICommodities($config['per_page'], $offset, $sortBy, 'ASC');
                $total_commodities = $this->icommodity_model->getTotalICommodities();
                $config['total_rows'] = $total_commodities;
                $data['total_commodities'] = $total_commodities;
            }
            if (!empty($commodities)) {
                foreach ($commodities as $pCommodity) {
                    # code...
                    $pCommodity->unit = $this->units_model->getUnitName($pCommodity->invc_unit_id);
                }
            }

            $config['base_url'] = base_url("investment-market/pg/");
            $config['per_page'] = $per_page;

        }



        $data['commodities'] = $commodities;


        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();

        echo json_encode($this->load->view('home/search_results', $data, true));
    }

    public function productsByCategory()
    {
        $this->load->library('pagination');

        $category = (int)$this->uri->segment(3);

        // Setup pagination info
        $config['per_page'] = 9;
        $page = $this->uri->segment(5);
        if (!empty($page)) {
            $page = (int)$page;
            $offset = $config["per_page"] * $page;
        } else {
            $page = 1;
            $offset = 0;
        }

        $products = array();
        $this->pagination->initialize($config);
        $data['categories'] = $this->categories_model->getAllCategories();

        if (!empty($category)) {
            $products = $this->product_model->getProductsByCategory($category, $config["per_page"], $offset);
        }
        if (!empty($products) && ($products !== false)) {
            foreach ($products as $product) {
                # code...
                $product->state = $this->state_model->getStateName($product->prod_states_id);
                $product->local = $this->locals_model->getLocalName($product->prod_locals_id);
                $product->unit = $this->units_model->getUnitName($product->prod_units_id);
                $product->category = $this->categories_model->getCategoryName($product->prod_category_id);
            }
            $data['products'] = $products;
        } else {
            $products = array();
        }

        $config['base_url'] = base_url('products/category/' . $category . '/pg/');
        $config['use_page_numbers'] = true;
        $config['full_tag_open'] = '<nav aria-label="Page navigation"><ul class="pagination mt-4 justify-content-end d-flex">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</div>';
        $config['first_link'] = 'First';
        $config['attributes'] = array('class' => 'page-link');
        $config['cur_tag_open'] = '&nbsp;<a class="page-link">';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = '&raquo;';
        $config['prev_link'] = '&laquo;';
        $config['num_links'] = 2;
        $config['uri_segment'] = 3;
        $config['total_rows'] = count($products);

        $data['total_products'] = count($products);
        $data['search_states'] = array();
        $data['categories'] = $this->categories_model->getAllCategories();

        $this->loadHeader();
        $this->load->view('home/temp/subheader', $data);
        $this->load->view('home/allproducts', $data);
        $this->load->view('home/temp/footer');
        $this->load->view('home/temp/scripts/search_products', $data);
        $this->load->view('home/temp/footer_close');

    }

    public function productdetails()
    {

        $product_slug = $this->uri->segment(2);
        if (!empty($product_slug)) {
            $product = $this->product_model->getProductBySlug($product_slug);
            if (!empty($product)) {
                # code...
                $product->state = $this->state_model->getStateName($product->prod_states_id);
                $product->local = $this->locals_model->getLocalName($product->prod_locals_id);
                $product->unit = $this->units_model->getUnitName($product->prod_units_id);
                $product->category = $this->categories_model->getCategoryName($product->prod_category_id);

                $user_id = $this->session->userdata('uid');
                $user = $this->user_model->getUser($user_id);
                $own_product = $this->product_model->isProductFromUser($user_id, $product_slug);

                $data['product'] = $product;
                if (!empty($user)) {
                    $data['own_product'] = $own_product;
                    $data['verified'] = $user->email_verified;
                }

                $products = $this->product_model->getRecentlyAddedProducts(5, 0);
                if (!empty($products)) {
                    foreach ($products as $product) {
                        # code...
                        $product->state = $this->state_model->getStateName($product->prod_states_id);
                        $product->local = $this->locals_model->getLocalName($product->prod_locals_id);
                        $product->unit = $this->units_model->getUnitName($product->prod_units_id);
                        $product->category = $this->categories_model->getCategoryName($product->prod_category_id);
                    }
                }
                $data['products'] = $products;
                $data['categories'] = $this->categories_model->getAllCategories();

                $this->loadHeader();
                $this->load->view('home/temp/subheader', $data);
                $this->load->view('home/productdetails', $data);
                $this->load->view('home/temp/footer');
            } else {
                show_404();
            }
        } else {
            show_404();
        }

    }

    public function purCommodityDetails()
    {
        $pur_commodity_slug = $this->uri->segment(2);
        if (!empty($pur_commodity_slug)) {
            $pcommodity = $this->pcommodity_model->getPCommodityBySlug($pur_commodity_slug);
            if (!empty($pcommodity)) {
                # code...
                $user = $this->user_model->getUser($this->loggedInUserID);
                $data['user'] = $user;
                $data['commodity'] = $pcommodity;

                // Load the related commodities
                $pcommodities = $this->pcommodity_model->getPCommodities(4, 0, 'purc_date_created', 'ASC');
                if (!empty($pcommodities)) {
                    foreach ($pcommodities as $commodity) {
                        # code...
                        $commodity->unit = $this->units_model->getUnitName($commodity->purc_unit_id);
                    }
                }
                $data['pcommodities'] = $pcommodities;
                $this->loadHeader();
                $this->load->view('home/pur_commodity_details', $data);
                $this->load->view('home/temp/footer');
                $this->load->view('home/temp/scripts/pur_commodity_details', $data);
                $this->load->view('home/temp/footer_close');
            } else {
                show_404();
            }
        } else {
            show_404();
        }

    }

    public function invCommodityDetails()
    {

        $inv_commodity_slug = $this->uri->segment(2);
        if (!empty($inv_commodity_slug)) {
            $icommodity = $this->icommodity_model->getICommodityBySlug($inv_commodity_slug);
            if (!empty($icommodity)) {
                # code...
                $user = $this->user_model->getUser($this->loggedInUserID);
                $data['user'] = $user;
                $data['commodity'] = $icommodity;

                // Load the related commodities
                $icommodities = $this->icommodity_model->getICommodities(4, 0, 'invc_date_created', 'ASC');
                // var_dump($icommodities);
                if (!empty($icommodities)) {
                    foreach ($icommodities as $commodity) {
                        # code...
                        $commodity->unit = $this->units_model->getUnitName($commodity->invc_unit_id);
                    }
                }
                $data['icommodities'] = $icommodities;
                $this->loadHeader();
                $this->load->view('home/inv_commodity_details', $data);
                $this->load->view('home/temp/footer');
                $this->load->view('home/temp/scripts/inv_commodity_details', $data);
                $this->load->view('home/temp/footer_close');

            } else {
                show_404();
            }
        } else {
            show_404();
        }

    }


    public function investAjax()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $response = array();
        $user_id = $this->input->post('user_id');
        $produce_type_id = $this->input->post('produce_type_id');
        $produce_id = $this->input->post('produce_id');
        $quantity = (int)$this->input->post('quantity');
        $commodity_id = $this->input->post("commodity_id");
        // $currency_id = $this->input->post('currencySelect');

        if($quantity <= 0){
            $response['message'] = "The Quantity cannot be zero or negative";
            return;
        }
        // Reduce the quantity left and send it to user
        $commodityData = array();
        $commodity = $this->icommodity_model->getICommodity($commodity_id);
        $current_quantity = (int) $commodity->invc_quantity_left;
        $new_quantity = $current_quantity - $quantity;
        $current_status = '';
        if($new_quantity <= 0 ){
            // set the status to sold out
            $commodityData['invc_status'] = 'sold-out';
            $commodityData['invc_quantity_left'] = 0;
            $this->icommodity_model->updateICommodity($commodity->invc_id, $commodityData);
            $response['qleft'] = 0;
        }else{            
            $commodityData['invc_quantity_left'] = $new_quantity;
            $this->icommodity_model->updateICommodity($commodity->invc_id, $commodityData);
            $response['qleft'] = $new_quantity;
        }
        
        $newInvestmentData = array(
            'iv_user_id' => $user_id,
            'iv_produce_type_id' => $produce_type_id,
            'iv_produce_id' => $produce_id,
            'iv_commodity_id'=> $commodity_id,
            'iv_quantity' => $quantity,
            'iv_currency_id' => 1,
            'iv_amount' => 0,
            'iv_pay_status' => 1,
            'iv_due_date' => date('Y-m-d H:i:s'),
            'iv_slug' => $this->investment_model->generateSlug($user_id),
            'iv_created_by' => $user_id,
            'iv_date_created' => date('Y-m-d H:i:s'),
        );
        $this->investment_model->newInvestment($newInvestmentData);
        
        $response['message'] = 'Your investment has been sent successfully. You will be contacted by the Admin shortly.';

        echo json_encode($response);

    }
    public function purchaseAjax()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $response = array();
        $user_id = $this->input->post('user_id');
        $produce_type_id = $this->input->post('produce_type_id');
        $produce_id = $this->input->post('produce_id');
        $quantity = (int)$this->input->post('quantity');
        $commodity_id = $this->input->post("commodity_id");
        // $currency_id = $this->input->post('currencySelect');

        if($quantity <= 0){
            $response['message'] = "The Quantity cannot be zero or negative";
            return;
        }
        // Reduce the quantity left and send it to user
        $commodityData = array();
        $commodity = $this->pcommodity_model->getPCommodity($commodity_id);
        $current_quantity = (int) $commodity->purc_quantity_left;
        $new_quantity = $current_quantity - $quantity;
        $current_status = '';
        if($new_quantity <= 0 ){
            // set the status to sold out
            $commodityData['purc_status'] = 'sold-out';
            $commodityData['purc_quantity_left'] = 0;
            $this->pcommodity_model->updatePCommodity($commodity->purc_id, $commodityData);
            $response['qleft'] = 0;
        }else{            
            $commodityData['purc_quantity_left'] = $new_quantity;
            $this->pcommodity_model->updatePCommodity($commodity->purc_id, $commodityData);
            $response['qleft'] = $new_quantity;
        }
        
        $newCPurchaseData = array(
            'cusp_user_id' => $user_id,
            'cusp_produce_type_id' => $produce_type_id,
            'cusp_produce_id' => $produce_id,
            'cusp_commodity_id'=> $commodity_id,
            'cusp_quantity' => $quantity,
            'cusp_currency_id' => 1,
            'cusp_amount' => 0,
            'cusp_pay_status' => 1,
            'cusp_due_date' => date('Y-m-d H:i:s'),
            'cusp_slug' => $this->cpurchases_model->generateCPurchaseSlug($user_id, $commodity->purc_id),
            'cusp_created_by' => $user_id,
            'cusp_date_created' => date('Y-m-d H:i:s'),
        );
        $this->cpurchases_model->newCPurchase($newCPurchaseData);
        
        $response['message'] = 'Your purchase has been sent successfully. You will be contacted by the Admin shortly.';

        echo json_encode($response);

    }

    public function error404()
    {
        $this->loadHeader();
        $this->load->view('home/temp/error404');
    }

    public function error403()
    {
        $this->loadHeader();
        $this->load->view('home/temp/error403');
    }

    public function loadHeader()
    {

        if ($this->loggedIn) {
            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);
            $data['user'] = $user;
            $this->load->view('users/temp/header', $data);
        } else {
            $this->load->view('home/temp/header');
        }
    }

    private function remove_empty($array)
    {
        $trimmedArray = array_map('trim', $array);
        return array_filter($trimmedArray, [$this, '_remove_empty_internal']);
    }

    private function _remove_empty_internal($value)
    {
        return !empty($value) || $value === 0;
    }

}
