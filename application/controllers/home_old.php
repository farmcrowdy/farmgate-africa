<?php

class Home extends CI_Controller
{

    // Roles
    private $loggedIn;
    private $loggedInUserID;
    private $user;

    // pagination configuration
    private $pagination_config = array(
        'use_page_numbers' => true,
        'reuse_query_string' => true,
        'full_tag_open' => '<nav aria-label="Page navigation"><ul class="pagination mt-4 justify-content-end d-flex">',
        'full_tag_close' => '</ul></nav>',
        'first_tag_open' => '<li class="page-item">',
        'first_tag_close' => '</li>',
        'first_link' => 'First',
        'attributes' => array('class' => 'page-link'),
        'cur_tag_open' => '<a class="page-link">',
        'cur_tag_close' => '</a>',
        'next_link' => '&raquo;',
        'prev_link' => '&laquo;',
        'num_links' => 2,
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->loggedIn = $this->session->userdata('loggedIn');
        $this->loggedInUserID = $this->session->userdata('uid');
        $this->user = $this->user_model->getUser($this->loggedInUserID);
    }

    public function index()
    {

        // $pcommodities = $this->commodity_model->getCommodities(4, 0, 'purchase', 'comm_date_created', 'ASC');
        $icommodities = $this->commodity_model->getCommoditiesByType(4, 0, 'trade', 'comm_date_created', 'DESC');
        $pcommodities = $this->commodity_model->getCommoditiesByType(4, 0, 'purchase', 'comm_date_created', 'DESC');


        if (!empty($pcommodities)) {
            foreach ($pcommodities as $commodity) {
                # code...
                $commodity->unit = $this->units_model->getUnitName($commodity->comm_unit_id);
            }
        }
        if (!empty($icommodities)) {
            foreach ($icommodities as $commodity) {
                # code...
                $commodity->unit = $this->units_model->getUnitName($commodity->comm_unit_id);
            }
        }
        $data['pcommodities'] = $pcommodities;
        $data['icommodities'] = $icommodities;
        $data['page_title'] = 'Home - FarmGate Africa';
        $this->loadHeader($data);
        $this->load->view('home/index', $data);
        $this->load->view('home/temp/footer');

    }

    public function aboutUs()
    {
        $data = array();
        $data['page_title'] = 'About Us - FarmGate Africa';
        $this->loadHeader($data);
        $this->load->view('home/aboutus', $data);
        $this->load->view('home/temp/footer');

    }

    public function contactUs()
    {
        $data = array();
        $data['page_title'] = 'Contact Us - FarmGate Africa';        
        $this->loadHeader($data);
        $this->load->view('home/contactus', $data);
        $this->load->view('home/temp/footer');
    }

    public function transSuccess()
    {
        $data = array();
        $data['page_title'] = 'Transaction Success - FarmGate Africa';        
        $this->loadHeader($data);
        $this->load->view('home/trans_success', $data);
        $this->load->view('home/temp/footer');

    }

    public function howItWorks()
    {
        $data = array();
        $data['page_title'] = 'How It Works - FarmGate Africa';
        $this->loadHeader($data);
        $this->load->view('home/howitworks', $data);
        $this->load->view('home/temp/footer');

    }

    public function privacy()
    {
        $data = array();
        $data['page_title'] = 'Privacy Policy - FarmGate Africa';
        $this->loadHeader($data);
        $this->load->view('home/privacy', $data);
        $this->load->view('home/temp/footer');

    }

    public function toc()
    {
        $data = array();
        $data['page_title'] = 'Terms and Conditions - FarmGate Africa';
        $this->loadHeader($data);
        $this->load->view('home/toc', $data);
        $this->load->view('home/temp/footer');

    }

    public function MarketPlace()
    {
        $this->load->library('pagination');
        $config = $this->pagination_config;

        $commodities = array();
        $statuses = $this->commodity_model->get_enum_values('commodities', 'comm_status');
        $data['statuses'] = $statuses;
        $keyword = $this->input->get('keyword');
        $type = $this->input->get('comm_type');
        if(empty($type)){
            $type = "trade";
        }
        $commodity_type = $type;

        $data['commodity_type'] = $commodity_type;

        $page = $this->uri->segment(3);
        $per_page = 9;
        $config['per_page'] = $per_page;
        if (!empty($page)) {
            $page = (int)$page;
            $offset = $per_page * $page;
        } else {
            $page = 1;
            $offset = 0;
        }

        $commodities = $this->commodity_model->getCommoditiesByType($per_page, $offset, $commodity_type , 'comm_date_created', 'ASC');
        if(empty($commodities)){
            $commodities = array();
        }
        $total_commodities = $this->commodity_model->getTotalCommoditiesByType($commodity_type);
        $config['total_rows'] = $total_commodities;
        $data['total_commodities'] = $total_commodities;

        if (!empty($commodities)) {
            foreach ($commodities as $iCommodity) {
                # code...
                $iCommodity->unit = $this->units_model->getUnitName($iCommodity->comm_unit_id);
                $iCommodity->category = $this->categories_model->getCategoryName($iCommodity->comm_produce_id);
            }
        }
        $data['commodities'] = $commodities;
        $config['base_url'] = base_url('marketplace/pg/');

        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['page_title'] = 'MarketPlace - '.ucwords($commodity_type).' - FarmGate Africa';
        $this->loadHeader($data);
        $this->load->view('home/marketplace', $data);
        $this->load->view('home/temp/footer');
        $this->load->view('home/temp/scripts/search_products', $data);
        $this->load->view('home/temp/footer_close');

    }

    public function searchCommodities()
    {

        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->library('pagination');
        $config = $this->pagination_config;

        $data = array();
        $commodities = array();
        $sortBy = $this->input->post('sortBy');
        $statuses = $this->input->post('statuses');
        $statuses = $this->remove_empty($statuses);
        $keyword = $this->input->post('keyword');
        $commodity_type = $this->input->post('comm_type');
        if(empty($commodity_type)){
            $commodity_type = 'trade';
        }

        $per_page = 9;
        $config['per_page'] = $per_page;
        if (!empty($page)) {
            $page = (int)$page;
            $offset = $per_page * $page;
        } else {
            $page = 1;
            $offset = 0;
        }
        // echo json_encode($statuses);

        // Only Status
            if (empty($keyword) && !empty($statuses)) {
            // var_dump("Search by Status only");
                // echo json_encode('Here');
                $commodities = $this->commodity_model->getCommoditiesByStatus($statuses, $config['per_page'], $offset, $commodity_type, $sortBy, 'DESC');
                $total_commodities = $this->commodity_model->getTotalCommoditiesByStatus($statuses, $commodity_type);
                $config['total_rows'] = $total_commodities;
                $data['total_commodities'] = $total_commodities;
            }
        // Only Name
            elseif (!empty($keyword) && empty($statuses)) {
            // var_dump("Name search");
                $commodities = $this->commodity_model->getCommoditiesByName($keyword, $config['per_page'], $offset, $commodity_type, $sortBy, 'DESC');
                $total_commodities = $this->commodity_model->getTotalCommoditiesByName($keyword, $commodity_type);
                $config['total_rows'] = $total_commodities;
                $data['total_commodities'] = $total_commodities;
            }
            // Both Name and Status
            elseif (!empty($keyword) && !empty($statuses)) {
                // var_dump("Name And Status");
                $commodities = $this->commodity_model->getCommoditiesByNameAndStatus($keyword, $statuses, $config['per_page'], $offset, $commodity_type, $sortBy, 'DESC');
                $total_commodities = $this->commodity_model->getTotalCommoditiesByNameAndStatus($keyword, $statuses, $commodity_type);
                $config['total_rows'] = $total_commodities;
                $data['total_commodities'] = $total_commodities;
            }
        // // Only Sort
            else {
            // var_dump("Sort search");
                $commodities = $this->commodity_model->getCommoditiesByType($config['per_page'], $offset, $commodity_type, $sortBy, 'ASC');
                $total_commodities = $this->commodity_model->getTotalCommoditiesByType($commodity_type);
                $config['total_rows'] = $total_commodities;
                $data['total_commodities'] = $total_commodities;
            }
            if (!empty($commodities)) {
                foreach ($commodities as $ICommodity) {
                    # code...
                    $ICommodity->unit = $this->units_model->getUnitName($ICommodity->comm_unit_id);
                }
            }

            $config['base_url'] = base_url("marketplace/pg/");
            $config['per_page'] = $per_page;

        $data['commodities'] = $commodities;


        $this->pagination->initialize($config);
        $data['pagination_links'] = $this->pagination->create_links();

        echo json_encode($this->load->view('home/search_results', $data, true));
    }

    public function purCommodityDetails()
    {
        $pur_commodity_slug = $this->uri->segment(2);
        if (!empty($pur_commodity_slug)) {
            $pcommodity = $this->commodity_model->getCommodityBySlug($pur_commodity_slug);
            if (!empty($pcommodity)) {
                # code...
                $user = $this->user_model->getUser($this->loggedInUserID);
                $data['user'] = $user;
                $data['commodity'] = $pcommodity;

                // Load the related commodities
                $pcommodities = $this->commodity_model->getCommodities(4, 0,'purchase', 'comm_date_created', 'ASC');
                if (!empty($pcommodities)) {
                    foreach ($pcommodities as $commodity) {
                        # code...
                        $commodity->unit = $this->units_model->getUnitName($commodity->comm_unit_id);
                    }
                }
                $data['commodities'] = $pcommodities;
                $this->loadHeader($data);
                $this->load->view('home/commodity_details', $data);
                $this->load->view('home/temp/footer');
                $this->load->view('home/temp/scripts/commodity_details', $data);
                $this->load->view('home/temp/footer_close');
            } else {
                show_404();
            }
        } else {
            show_404();
        }

    }

    public function CommodityDetails()
    {

        $inv_commodity_slug = $this->uri->segment(2);
        if (!empty($inv_commodity_slug)) {
            $icommodity = $this->commodity_model->getCommodityBySlug($inv_commodity_slug);
            if (!empty($icommodity)) {
                # code...
                $user = $this->user_model->getUser($this->loggedInUserID);
                $data['user'] = $user;
                $data['commodity'] = $icommodity;
                
                // Load the related commodities
                $icommodities = $this->commodity_model->getCommoditiesByType(4, 0, $icommodity->comm_commodity_type, 'comm_date_created', 'ASC');
                // var_dump($icommodities);
                if (!empty($icommodities)) {
                    foreach ($icommodities as $commodity) {
                        # code...
                        $commodity->unit = $this->units_model->getUnitName($commodity->comm_unit_id);
                    }
                }
                $data['commodities'] = $icommodities;
                $data['page_title'] = 'Commodity Details - '.ucwords($commodity->comm_name).' - FarmGate Africa';
                $this->loadHeader($data);
                $this->load->view('home/commodity_details', $data);
                $this->load->view('home/temp/footer');
                $this->load->view('home/temp/scripts/commodity_details', $data);
                $this->load->view('home/temp/footer_close');

            } else {
                show_404();
            }
        } else {
            show_404();
        }

    }


    public function investAjax()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $response = array();
        $user_id = $this->input->post('user_id');
        $quantity = (int)$this->input->post('quantity');
        $commodity_id = $this->input->post("commodity_id");
        $commodity_name = $this->input->post("commodity_name");
        // $currency_id = $this->input->post('currencySelect');

        if($quantity <= 0){
            $response['message'] = "The Quantity cannot be zero or negative";
            return;
        }
        // Reduce the quantity left and send it to user
        $commodityData = array();
        $commodity = $this->commodity_model->getCommodity($commodity_id);
        $current_quantity = (int) $commodity->comm_quantity_left;
        $new_quantity = $current_quantity - $quantity;
        $current_status = '';
        if($new_quantity <= 0 ){
            // set the status to sold out
            $commodityData['comm_status'] = 'sold-out';
            $commodityData['comm_quantity_left'] = 0;
            $this->commodity_model->updateCommodity($commodity->comm_id, $commodityData);
            $response['qleft'] = 0;
        }else{            
            $commodityData['comm_quantity_left'] = $new_quantity;
            $this->commodity_model->updateCommodity($commodity->comm_id, $commodityData);
            $response['qleft'] = $new_quantity;
        }
        
        $newInvestmentData = array(
            'iv_user_id' => $user_id,
            'iv_commodity_id'=> $commodity_id,
            'iv_commodity_name'=> $commodity_name,
            'iv_quantity' => $quantity,
            'iv_currency_id' => 1,
            'iv_amount' => 0,
            'iv_pay_status' => 1,
            'iv_due_date' => date('Y-m-d H:i:s'),
            'iv_slug' => $this->investment_model->generateSlug($user_id),
            'iv_created_by' => $user_id,
            'iv_date_created' => date('Y-m-d H:i:s'),
        );
        $this->investment_model->newInvestment($newInvestmentData);
        
        $response['message'] = 'Your investment has been sent successfully. You will be contacted by the Admin shortly.';

        echo json_encode($response);

    }
    public function purchaseAjax()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $response = array();
        $user_id = $this->input->post('user_id');
        $quantity = (int)$this->input->post('quantity');
        $commodity_id = $this->input->post("commodity_id");
        $commodity_name = $this->input->post("commodity_name");
        // $currency_id = $this->input->post('currencySelect');

        if($quantity <= 0){
            $response['message'] = "The Quantity cannot be zero or negative";
            return;
        }
        // Reduce the quantity left and send it to user
        $commodityData = array();
        $commodity = $this->commodity_model->getCommodity($commodity_id);
        $current_quantity = (int) $commodity->comm_quantity_left_to_buy;
        $new_quantity = $current_quantity - $quantity;
        $current_status = '';
        if($new_quantity <= 0 ){
            // set the status to sold out
            $commodityData['comm_status'] = 'sold-out';
            $commodityData['comm_quantity_left_to_buy'] = 0;
            $this->commodity_model->updateCommodity($commodity->comm_id, $commodityData);
            $response['qleft'] = 0;
        }else{            
            $commodityData['comm_quantity_left_to_buy'] = $new_quantity;
            $this->commodity_model->updateCommodity($commodity->comm_id, $commodityData);
            $response['qleft'] = $new_quantity;
        }
        
        $newCPurchaseData = array(
            'cusp_user_id' => $user_id,
            'cusp_commodity_id'=> $commodity_id,
            'cusp_commodity_name'=> $commodity_name,
            'cusp_quantity' => $quantity,
            'cusp_currency_id' => 1,
            'cusp_amount' => 0,
            'cusp_pay_status' => 1,
            'cusp_slug' => $this->cpurchases_model->generateCPurchaseSlug($user_id, $commodity->comm_id),
            'cusp_created_by' => $user_id,
            'cusp_date_created' => date('Y-m-d H:i:s'),
        );
        $this->cpurchases_model->newCPurchase($newCPurchaseData);
        
        $response['message'] = 'Your purchase has been sent successfully. You will be contacted by the Admin shortly.';

        echo json_encode($response);

    }

    public function error404()
    {
        $data = array();
        $this->loadHeader($data);
        $this->load->view('home/temp/error404');
    }

    public function error403()
    {
        $this->loadHeader($data);
        $this->load->view('home/temp/error403');
        // $this->load->view('home/temp/footer');
        // $this->load->view('home/temp/footer_close');

    }

    public function loadHeader($data = array())
    {

        if ($this->loggedIn) {
            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);
            $data['user'] = $user;
            $this->load->view('users/temp/header', $data);
        } else {
            $this->load->view('home/temp/header', $data);
        }
    }

    private function remove_empty($array)
    {
        $trimmedArray = array_map('trim', $array);
        return array_filter($trimmedArray, [$this, '_remove_empty_internal']);
    }

    private function _remove_empty_internal($value)
    {
        return !empty($value) || $value === 0;
    }

    public function Kenneth()
    {
        // $this->loadHeader();
        $this->load->view('home/kenneth');
    }

}
