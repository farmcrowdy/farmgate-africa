<?php

class Users extends CI_Controller
{

    // View variables for feedback boxes
    private $open_delimiter = '<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>';
    private $open_delimiter_success = '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>';
    private $close_delimiter = '</strong></div>';


    // pagination configuration
    private $pagination_config = array(
        'use_page_numbers' => true,
        'reuse_query_string' => true,
        'full_tag_open' => '<nav aria-label="Page navigation"><ul class="pagination mt-4 justify-content-end d-flex">',
        'full_tag_close' => '</ul></nav>',
        'first_tag_open' => '<li class="page-item">',
        'first_tag_close' => '</li>',
        'first_link' => 'First',
        'attributes' => array('class' => 'page-link'),
        'cur_tag_open' => '<a class="page-link">',
        'cur_tag_close' => '</a>',
        'next_link' => '&raquo;',
        'prev_link' => '&laquo;',
        'num_links' => 2,
    );

    public function login()
    {
        $data = array();
        $data['invalidLogin'] = '';
        $data['page_title'] = 'Login - FarmGate Africa';

        if ($this->session->userdata('loggedIn')) {
            redirect(base_url());
        } else {

            if (isset($_POST['login_submit'])) {

                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
                $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');

                if ($this->form_validation->run() == false) {
                    $this->load->view('home/temp/header', $data);
                    $this->load->view('users/login', $data);

                } else {

                    $email = strtolower($this->input->post('email'));

                    $password = $this->input->post('password');

                    $hash_password = hash('sha512', $password);

                    // Check for valid details
                    $validLogin = $this->user_model->validLogin($email, $hash_password);
                    $user = $this->user_model->getUserByEmail($email);

                    if ($validLogin) {
                        $user_id = $this->user_model->getUserIDByEmail($email);
                        $this->user_model->updateUserByEmail(
                            $email,
                            array('logged_in' => 'yes', 'last_login' => date('Y-m-d H:i:s'), 'date_updated' => date('Y-m-d H:i:s'))
                        );

                        if (($user->type == 'admin') || ($user->type == 'manager') || ($user->type == 'warehouse_manager') || ($user->type == 'collection_center_manager')) {
                            $this->session->set_userdata(array('loggedIn' => true, 'uid' => $user_id));
                            //Store in a variable so that can unset the session
                            $redirect_back = $this->session->userdata('admin_redirect_url');
                            $this->session->unset_userdata('admin_redirect_url');
                            // if(!empty($redirect_back)){
                            //     redirect( $redirect_back );
                            // }else{
                                // Log data
                                $logData = array('lg_user_id' => $user->id);
                                $logData['lg_message'] = "Logged In. ";
                                $this->log_model->newLog($logData);
                                redirect('admin');
                            // }

                        } else {

                            $this->session->set_userdata(array('loggedIn' => true, 'uid' => $user_id, 'verified' => $user->email_verified));
                            $redirect_back = $this->session->userdata('user_intent');
                            $this->session->unset_userdata('user_intent');

                            $logData = array('lg_user_id' => $user->id);
                            $logData['lg_message'] = "Logged In. ";
                            $this->log_model->newLog($logData);

                            if(!empty($redirect_back)){
                                redirect( $redirect_back );
                            }else{
                                redirect('users/dashboard');
                            }    
                        }
                    } else {

                        $data['invalidLogin'] = $this->open_delimiter . ' Your Email/Password combination is not valid. Please check it carefully and try again ' . $this->close_delimiter;
                        $this->load->view('home/temp/header', $data);
                        $this->load->view('users/login', $data);
                    }

                }
            } else {
                $this->load->view('home/temp/header', $data);
                $this->load->view('users/login', $data);
            }
        }

        //  $this->load->view('home/temp/footer');

    }

    public function forgotPassword()
    {

        $data = array();
        $data['page_title'] = 'Forgot Password - FarmGate Africa';

        if ($this->session->userdata('loggedIn')) {
            redirect(base_url());
        } else {

            if (isset($_POST['recover_submit'])) {

                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
                $this->form_validation->set_rules('rec_email', 'Email', 'trim|required|valid_email');

                if ($this->form_validation->run() == false) {
                    $this->load->view('home/temp/header', $data);
                    $this->load->view('users/forgotpassword', $data);

                } else {

                    $email = $this->input->post('rec_email');
                    $user = $this->user_model->getUserByEmail($email);

                    if (!empty($user)) {

                        // Generate password reset link for the user
                        $reset_password_hash = md5($user->email . mt_rand(10, time()));
                        $reset_password_link = base_url() . 'users/resetpassword/' . urlencode($reset_password_hash);

                        // Update the user record with the reset passwords link
                        $updateUserData = array(
                            'reset_password_link' => $reset_password_link,
                            'date_updated' => date('Y-m-d H:i:s'),
                        );
                        $this->user_model->updateUser($user->id, $updateUserData);

                        // Send Email to User with the reset link
                        $subject = 'Forgot Password - FarmGatePassword';
                        $to = $email;

                        $data['user'] = $this->user_model->getUser($user->id);
                        $message = $this->load->view('email_templates/forgot_password.php', $data, true);

                        // Always set content-type when sending HTML email
                        $headers = "MIME-Version: 1.0" . "\r\n";
                        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                        // More headers
                        $headers .= 'From: FarmGate Africa Team <info@farmgate.africa>' . "\r\n";

                        mail($to, $subject, $message, $headers);

                        $data['feedback'] = $this->open_delimiter_success . 'Your reset link has been sent to your email. Please click on the link to reset your password.<br><small></small> Thank you ' . $this->close_delimiter;
                    } else {
                        $data['feedback'] = $this->open_delimiter . 'The email does not exist on our platform. Please signup for a new account.' . $this->close_delimiter;
                    }

                    $this->load->view('home/temp/header', $data);
                    $this->load->view('users/forgotpassword', $data);

                }

            } else {
                $this->load->view('home/temp/header', $data);
                $this->load->view('users/forgotpassword', $data);
            }
        }

    }

    public function resetPassword()
    {
        $data = array();
        $data['page_title'] = 'Reset Password - FarmGate Africa';
        $reset_password_link = current_url() or "";
        $reset_password_hash = $this->uri->segment(3) or "";
        // var_dump($reset_password_link);
        $data['hash'] = $reset_password_hash;

        $user = $this->user_model->getUserByPasswordResetLink(trim($reset_password_link));
        if (!empty($user)) {

            if (isset($_POST['reset_pass_submit'])) {

                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
                $this->form_validation->set_rules('new_password', 'Password', 'trim|min_length[8]|required');
                $this->form_validation->set_rules('new_password_confirm', 'Confirm Password', 'required|matches[new_password]');

                if ($this->form_validation->run() == false) {
                    $this->load->view('home/temp/header', $data);
                    $this->load->view('users/reset_password', $data);
                    $this->load->view('home/temp/footer');

                } else {

                    $new_password = $this->input->post('new_password');
                    $hash_password = hash('sha512', $new_password);

                    // Update the user record with the new passwords link
                    // Clear the reset_password_link
                    $updateUserData = array(
                        'reset_password_link' => "",
                        'password' => $hash_password,
                        'date_updated' => date('Y-m-d H:i:s'),
                    );
                    $this->user_model->updateUser($user->id, $updateUserData);

                    $data['feedback'] = $this->open_delimiter_success . 'Your password has been reset successfully. Please login. Thank you ' . $this->close_delimiter;
                    $this->load->view('home/temp/header', $data);
                    $this->load->view('users/reset_password', $data);
                    $this->load->view('home/temp/footer');
                }

            } else {
                $this->load->view('home/temp/header', $data);
                $this->load->view('users/reset_password', $data);
                $this->load->view('home/temp/footer');

            }

        } else {
            show_404();
        }
    }

    public function activate()
    {
        $data['page_title'] = 'Activate - FarmGate Africa';
        $email_activation_link = current_url();
        $email_activation_hash = $this->uri->segment(2);
        // var_dump($email_activation_hash);
        $user_exists = $this->user_model->activationHashExists(trim($email_activation_hash));
        // var_dump($user_exists);
        if ($user_exists !== false) {

            // Update the user record by setting email activation to 'yes'
            $updateUserData = array(
                'email_verified' => "yes",
                'date_updated' => date('Y-m-d H:i:s'),
            );

            $this->user_model->updateUserByEmail($user_exists, $updateUserData);

            $this->load->view('home/temp/header', $data);
            $this->load->view('users/activation_success');
            $this->load->view('home/temp/footer');

        } else {
            show_404();
        }
    }

    public function regsuccess()
    {
        $data['page_title'] = 'Registeration Successful - FarmGate Africa';

        $this->load->view('home/temp/header', $data);
        // $this->load->view('home/temp/subheader');
        $this->load->view('users/regsuccess');
        $this->load->view('home/temp/footer');

    }

    public function register()
    {

        $data = array();
        $data['page_title'] = 'Registeration - FarmGate Africa';

        if ($this->session->userdata('loggedIn')) {
            redirect(base_url());
        } else {

            if (isset($_POST['register_submit'])) {

                $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
                $this->form_validation->set_rules('reg_fullname', 'Full Name', 'trim|required');
                $this->form_validation->set_rules('reg_email', 'Email', 'trim|required|valid_email|callback_check_email_exists');
                $this->form_validation->set_rules('reg_phone', 'Phone', 'trim|required|min_length[8]');
                $this->form_validation->set_rules('reg_password', 'Password', 'trim|min_length[8]|required');
                $this->form_validation->set_rules('reg_terms', 'Terms and Agreement', 'callback_reg_terms');

                if ($this->form_validation->run() == false) {

                    $this->load->view('home/temp/header', $data);
                    $this->load->view('users/register', $data);
                } else {

                    $email_activation_hash = hash('sha512', $this->input->post('email') . time());
                    $email_activation_link = base_url('activate/') . $email_activation_hash;
                    $full_name = $this->input->post('reg_fullname', true);
                    $email = strtolower($this->input->post('reg_email'));
                    $phone = $this->input->post('reg_phone');
                    $password = trim($this->input->post('reg_password'));
                    $hash_password = hash('sha512', $password);

                    $slug = $this->user_model->generateUserSlug($full_name, $email);
                    $uData = array(
                        'full_name' => $full_name,
                        'email' => $email,
                        'phone' => $phone,
                        'slug' => $slug,
                        'email_activation_hash' => $email_activation_hash,
                        'email_activation_link' => $email_activation_link,
                        'password' => $hash_password,
                        'active' => 'yes',
                        'date_created' => date('Y-m-d H:i:s'),
                        'last_login' => date('Y-m-d H:i:s'),
                    );

                    $newUserID = $this->user_model->newUser($uData);

                    $subject = 'Verify Your Email';
                    $to = $email;

                    $new_user = $this->user_model->getUser($newUserID);
                    $data['user'] = $new_user;
                    $message = $this->load->view('email_templates/verify_account.php', $data, true);

                    // Always set content-type when sending HTML email
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                    // More headers
                    $headers .= 'From: Farmgate Africa Team <info@farmgate.africa>' . "\r\n";

                    mail($to, $subject, $message, $headers);

                    // Send mail to the CRM Team
                    $to = 'crm@farmgate.africa';
                    $subject ='New User Signup';
                    $message = $this->load->view('email_templates/new_user_signup.php', $data, true);
                    mail($to, $subject, $message, $headers);
                    
                    redirect('users/regsuccess');

                }
            } else {
                // Load the view
                $this->load->view('home/temp/header', $data);
                $this->load->view('users/register', $data);
            }
        }
    }

    public function reg_terms()
    {

        if (isset($_POST['reg_terms'])) {
            return true;
        } else {
            $this->form_validation->set_message('reg_terms', 'Please read and accept our terms and conditions.');
            return false;
        }
    }

    public function check_email_exists($email)
    {
        $this->form_validation->set_message('check_email_exists', 'That email is taken. Please choose a different one');

        if ($this->user_model->check_email_exists($email)) {

            return true;
        } else {
            return false;
        }
    }

    public function productdetails()
    {

        $loggedIn = $this->session->userdata('loggedIn');
        if ($loggedIn) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);

            $data['user'] = $user;
            $data['total_products'] = $this->product_model->getTotalProductsByUser($user_id);
            $data['total_orders'] = $this->orders_model->getTotalOrdersByUser($user_id);

            $this->load->view('home/temp/header', $data);
            $this->load->view('home/temp/subheader');
            $this->load->view('home/productdetails', $data);
            $this->load->view('home/temp/footer');
        } else {
            redirect('login');
        }
    }

    public function dashboard()
    {

        $loggedIn = $this->session->userdata('loggedIn');
        if ($loggedIn) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);
            $data['page_title'] = 'Dashboard- FarmGate Africa';
            $data['user'] = $user;
            $data['total_purchases'] = $this->cpurchases_model->getTotalCPurchasesByUser($user_id);
            $data['total_investments'] = $this->investment_model->getTotalInvestmentsByUser($user_id);
            // var_dump($data['total_orders']);
            // die();
            $this->load->view('users/temp/header', $data);
            //$this->load->view('users/temp/subheader');
            $this->load->view('users/temp/sidebar');
            $this->load->view('users/dashboard', $data);
            $this->load->view('users/temp/footer');
        } else {
            redirect('login');
        }
    }

    public function addProduct()
    {

        $loggedIn = $this->session->userdata('loggedIn');
        if ($loggedIn) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);

            $data['user'] = $user;

            $data['categories'] = $this->categories_model->getAllCategories();
            $data['units'] = $this->units_model->getAllUnits();
            $data['states'] = $this->state_model->getAllStates();

            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
            $this->form_validation->set_rules('name', 'Product Name', 'trim|required');
            $this->form_validation->set_rules('categorySelect', 'Category', 'trim|required');
            $this->form_validation->set_rules('description', 'Description', 'trim|required');
            $this->form_validation->set_rules('price_per_unit', 'Price per unit', 'trim|required');
            $this->form_validation->set_rules('min_order', 'Minimum Order', 'trim|required');
            $this->form_validation->set_rules('quantity', 'Quantity', 'trim|required');
            $this->form_validation->set_rules('stateSelect', 'State Selection', 'trim|required');
            $this->form_validation->set_rules('localSelect', 'Local Government Selection', 'trim|required');
            $this->form_validation->set_rules('avail_from', 'Availability From', 'trim|required');
            $this->form_validation->set_rules('avail_to', 'Availability To', 'trim|required');

            if (isset($_POST['submit'])) {

                if ($this->form_validation->run() == false) {
                    $this->load->view('users/temp/header', $data);
                    //$this->load->view('users/temp/subheader');
                    $this->load->view('users/temp/sidebar');
                    $this->load->view('users/addproduct', $data);
                    $this->load->view('users/temp/footer');
                    $this->load->view('users/temp/scripts/addproduct');
                    $this->load->view('users/temp/footer_close');
                } else {

                    $newProductData = array(
                        'prod_name' => $this->input->post('name'),
                        'prod_user_id' => $user_id,
                        'prod_slug' => $this->product_model->generateProductSlug($this->input->post('name'), $user_id),
                        'prod_category_id' => $this->input->post('categorySelect'),
                        'prod_description' => $this->input->post('description'),
                        'prod_price_per_unit' => $this->input->post('price_per_unit'),
                        'prod_units_id' => $this->input->post('unitSelect'),
                        'prod_min_order' => $this->input->post('min_order'),
                        'prod_quantity' => $this->input->post('quantity'),
                        'prod_states_id' => $this->input->post('stateSelect'),
                        'prod_locals_id' => $this->input->post('localSelect'),
                        'prod_avail_from' => $this->input->post('avail_from'),
                        'prod_avail_to' => $this->input->post('avail_to'),
                        'prod_status' => 'pending',
                        'prod_date_created' => date('Y-m-d H:i:s'),
                    );

                    if (!empty($_FILES['productImageFile']['name'])) {
                        $product_id = $this->product_model->newProduct($newProductData);
                        $path = './mainasset/fe/images/products/';
                        $results = $this->do_upload($path, 'productImageFile');
                        if (!isset($results['errors'])) {
                            //add the images to the cusers table
                            $imageData = array(
                                'prod_image' => $results['file_name'],
                                'prod_date_updated' => date('Y-m-d H:i:s'),
                            );
                            $this->product_model->updateProduct($product_id, $imageData);
                            //Show success message
                            $success_message = $this->open_delimiter_success . 'Your Product has been successfully uploaded!' . $this->close_delimiter;
                            $this->session->set_flashdata('prod_feedback', $success_message);

                            $new_product = $this->product_model->getNewProduct($product_id);
                            $new_product->category = $this->categories_model->getCategoryName($new_product->prod_category_id);
                            $new_product->state = $this->state_model->getStateName($new_product->prod_states_id);
                            $data['product'] = $new_product;

                            // Send message to User

                            $message = $this->load->view('email_templates/new_product.php', $data, true);

                            $subject = 'New Product Uploaded !';
                            $to = $user->email;
                            // Always set content-type when sending HTML email
                            $headers = "MIME-Version: 1.0" . "\r\n";
                            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                            // More headers
                            $headers .= 'From: <contact@farmgatetrader.com>' . "\r\n";

                            mail($to, $subject, $message, $headers);

                            // Send to Admin
                            $subject = 'New Product Upload !';
                            $to = 'emmanuel.torty@farmcrowdy.com';
                            $message = $this->load->view('email_templates/admin_new_product.php', $data, true);
                            mail($to, $subject, $message, $headers);

                        } else {
                            $data['errors'] = $results['errors'];
                            $this->session->set_flashdata('prod_feedback', $results['errors']);
                        }
                    } else {
                        //Show error
                        $fail_message = $this->open_delimiter . 'Your have not selected any product Image file to upload!' . $this->close_delimiter;
                        $this->session->set_flashdata('prod_feedback', $fail_message);
                    }

                    $this->load->view('users/temp/header', $data);
                    //$this->load->view('users/temp/subheader');
                    $this->load->view('users/temp/sidebar');
                    $this->load->view('users/addproduct', $data);
                    $this->load->view('users/temp/footer');
                    $this->load->view('users/temp/scripts/addproduct');
                    $this->load->view('users/temp/footer_close');

                }
            } else {

                $this->load->view('users/temp/header', $data);
                //$this->load->view('users/temp/subheader');
                $this->load->view('users/temp/sidebar');
                $this->load->view('users/addproduct', $data);
                $this->load->view('users/temp/footer');
                $this->load->view('users/temp/scripts/addproduct');
                $this->load->view('users/temp/footer_close');
            }

        } else {
            redirect('login');
        }
    }

    public function editProduct()
    {

        $loggedIn = $this->session->userdata('loggedIn');
        if ($loggedIn) {

            $product_slug = $this->uri->segment(3);

            if (!$this->product_model->slug_exists($product_slug)) {
                show_404();
            } else {
                $user_id = $this->session->userdata('uid');
                $user = $this->user_model->getUser($user_id);

                // Is the product for this user ?
                $usersProduct = $this->product_model->isProductFromUser($user->id, $product_slug);
                if ($usersProduct) {
                    $product = $this->product_model->getProductBySlug($product_slug);
                } else {
                    return show_404();
                }

                $data['product'] = $product;
                $data['user'] = $user;
                $data['categories'] = $this->categories_model->getAllCategories();
                $data['units'] = $this->units_model->getAllUnits();
                $data['states'] = $this->state_model->getAllStates();

                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);
                $this->form_validation->set_rules('name', 'Product Name', 'trim|required');
                $this->form_validation->set_rules('categorySelect', 'Category', 'trim|required');
                $this->form_validation->set_rules('description', 'Description', 'trim|required');
                $this->form_validation->set_rules('price_per_unit', 'Price per unit', 'trim|required');
                $this->form_validation->set_rules('min_order', 'Minimum Order', 'trim|required');
                $this->form_validation->set_rules('quantity', 'Quantity', 'trim|required');
                $this->form_validation->set_rules('stateSelect', 'State Selection', 'trim|required');
                $this->form_validation->set_rules('localSelect', 'Local Government Selection', 'trim|required');
                $this->form_validation->set_rules('avail_from', 'Availability From', 'trim|required');
                $this->form_validation->set_rules('avail_to', 'Availability To', 'trim|required');

                if (isset($_POST['submit'])) {

                    if ($this->form_validation->run() == false) {
                        $this->load->view('users/temp/header', $data);
                        //$this->load->view('users/temp/subheader');
                        $this->load->view('users/temp/sidebar');
                        $this->load->view('users/editproduct', $data);
                        $this->load->view('users/temp/footer');
                        $this->load->view('users/temp/scripts/editproduct');
                        $this->load->view('users/temp/footer_close');
                    } else {

                        $ProductData = array(
                            'prod_name' => $this->input->post('name'),
                            'prod_category_id' => $this->input->post('categorySelect'),
                            'prod_description' => $this->input->post('description'),
                            'prod_price_per_unit' => $this->input->post('price_per_unit'),
                            'prod_units_id' => $this->input->post('unitSelect'),
                            'prod_min_order' => $this->input->post('min_order'),
                            'prod_quantity' => $this->input->post('quantity'),
                            'prod_states_id' => $this->input->post('stateSelect'),
                            'prod_locals_id' => $this->input->post('localSelect'),
                            'prod_avail_from' => $this->input->post('avail_from'),
                            'prod_avail_to' => $this->input->post('avail_to'),
                            'prod_date_updated' => date('Y-m-d H:i:s'),
                        );

                        $this->product_model->updateProduct($product->prod_id, $ProductData);
                        //Show success message
                        $success_message = $this->open_delimiter_success . 'Your Product has been successfully edited!' . $this->close_delimiter;
                        $this->session->set_flashdata('prod_feedback', $success_message);
                        if (!empty($_FILES['productImageFile']['name'])) {

                            $path = './mainasset/fe/images/products/';
                            $results = $this->do_upload($path, 'productImageFile');
                            if (!isset($results['errors'])) {
                                //add the images to the cusers table
                                $imageData = array(
                                    'prod_image' => $results['file_name'],
                                    'prod_date_updated' => date('Y-m-d H:i:s'),
                                );
                                $this->product_model->updateProduct($product->prod_id, $imageData);
                            } else {
                                $data['errors'] = $results['errors'];
                                $this->session->set_flashdata('prod_feedback', $results['errors']);
                            }
                        }

                        $this->load->view('users/temp/header', $data);
                        //$this->load->view('users/temp/subheader');
                        $this->load->view('users/temp/sidebar');
                        $this->load->view('users/editproduct', $data);
                        $this->load->view('users/temp/footer');
                        $this->load->view('users/temp/scripts/editproduct');
                        $this->load->view('users/temp/footer_close');

                    }
                } else {

                    $this->load->view('users/temp/header', $data);
                    //$this->load->view('users/temp/subheader');
                    $this->load->view('users/temp/sidebar');
                    $this->load->view('users/editproduct', $data);
                    $this->load->view('users/temp/footer');
                    $this->load->view('users/temp/scripts/editproduct');
                    $this->load->view('users/temp/footer_close');
                }

            }

        } else {
            redirect('users/login');
        }
    }

    public function deleteProduct()
    {

        $loggedIn = $this->session->userdata('loggedIn');
        if ($loggedIn) {

            $data = array();
            $product_slug = $this->uri->segment(3);

            if (!$this->product_model->slug_exists($product_slug)) {
                show_404();
            } else {

                $product = $this->product_model->getProductBySlug($product_slug);
                $user_id = $this->session->userdata('uid');

                if ($product !== false) {
                    // Check if the product belongs to this user before deleting it
                    $isForUser = $this->product_model->isProductFromUser($user_id, $product_slug);
                    if ($isForUser) {

                        $updateData = array(
                            'prod_status' => 'deleted',
                            'prod_date_updated' => date('Y-m-d H:i:s'),
                        );

                        $this->product_model->updateProduct($product->prod_id, $updateData);

                        $success_message = $this->open_delimiter_success . 'Your Product ' . $product->prod_name . '-' . $product->prod_slug . ' has been successfully deleted!' . $this->close_delimiter;
                        $this->session->set_flashdata('del_feedback', $success_message);

                    } else {

                        $success_message = $this->open_delimiter . 'That Product does not belong to you!' . $this->close_delimiter;
                        $this->session->set_flashdata('del_feedback', $success_message);

                    }
                    redirect('users/products');
                } else {
                    redirect('users/products');
                }

            }
        }
    }

    public function products()
    {

        $loggedIn = $this->session->userdata('loggedIn');
        if ($loggedIn) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);
            $data['user'] = $user;

            $products = array();
            $keyword = $this->input->get('keyword_p');

            $this->load->library('pagination');

            // Add the search data in session to populate the fields again
            $this->session->set_userdata('keyword_p', $keyword);
            $data['keyword_p'] = $this->session->userdata('keyword_p');

            // Get the page number if it is set
            $page = $this->uri->segment(3);
            $per_page = 5;
            if (empty($page)) {
                $page = 1;
                $offset = 0;
            } else {
                $page = (int) $page;
                $offset = $page * $per_page;
            }

            if (!empty($keyword)) {
                $products = $this->product_model->getProductsByUserWithName($user_id, $keyword, $per_page, $offset);
                $config['total_rows'] = $this->product_model->getTotalProductsByUserWithName($user_id, $keyword);
                $data['total_products'] = $config['total_rows'];

            } else {
                $products = $this->product_model->getProductsByUser($user_id, $per_page, $offset);
                $config['total_rows'] = $this->product_model->getTotalProductsByUser($user_id);
                $data['total_products'] = $config['total_rows'];
            }

            if (!empty($products) && ($products !== false)) {
                foreach ($products as $product) {
                    # code...
                    $product->state = $this->state_model->getStateName($product->prod_states_id);
                    $product->local = $this->locals_model->getLocalName($product->prod_locals_id);
                    $product->unit = $this->units_model->getUnitName($product->prod_units_id);
                    $product->category = $this->categories_model->getCategoryName($product->prod_category_id);
                }
                $data['products'] = $products;
            }

            $config['base_url'] = base_url() . "users/products/";
            $config['use_page_numbers'] = true;
            $config['reuse_query_string'] = true;
            $config['full_tag_open'] = '<nav aria-label="Page navigation"><ul class="pagination mt-4 justify-content-end d-flex">';
            $config['full_tag_close'] = '</ul></nav>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</div>';
            $config['first_link'] = 'First';
            $config['attributes'] = array('class' => 'page-link');
            $config['cur_tag_open'] = '&nbsp;<a class="page-link">';
            $config['cur_tag_close'] = '</a>';
            $config['next_link'] = '&raquo;';
            $config['prev_link'] = '&laquo;';
            $config['num_links'] = 3;
            $config['per_page'] = $per_page;

            $this->pagination->initialize($config);

            $data['pagination_links'] = $this->pagination->create_links();

            $this->load->view('users/temp/header', $data);
            //$this->load->view('users/temp/subheader');
            $this->load->view('users/temp/sidebar');
            $this->load->view('users/allproducts', $data);
            $this->load->view('users/temp/footer');
            $this->load->view('users/temp/footer_close');

        } else {
            redirect('login');
        }
    }

    public function Purchases()
    {
        $loggedIn = $this->session->userdata('loggedIn');
        if ($loggedIn) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);
            $data['user'] = $user;
            $data['page_title'] = 'Purchases - FarmGate Africa';
            $purchases = array();
            $keyword = $this->input->get('keyword_upur');

            $this->load->library('pagination');
            $config = $this->pagination_config;

            // Add the search data in session to populate the fields again
            $this->session->set_userdata('keyword_upur', $keyword);
            $data['keyword_upur'] = $this->session->userdata('keyword_upur');

            // Get the page number if it is set
            $page = $this->uri->segment(4);
            $per_page = 10;
            if (empty($page)) {
                $page = 1;
                $offset = 0;
            } else {
                $page = (int) $page;
                $offset = $page * $per_page;
            }

            if (!empty($keyword)) {
                $purchases = $this->cpurchases_model->getCPurchasesByUserWithName($user_id, $keyword, $per_page, $offset, 'cusp_id', 'DESC');
                $config['total_rows'] = $this->cpurchases_model->getTotalCPurchasesByUserWithName($user_id, $keyword);
                $data['total_purchases'] = $config['total_rows'];

            } else {
                $purchases = $this->cpurchases_model->getCPurchasesByUser($user_id, $per_page, $offset, 'cusp_id', 'DESC');
                $config['total_rows'] = $this->cpurchases_model->getTotalCPurchasesByUser($user_id);
                $data['total_purchases'] = $config['total_rows'];
            }

            if (empty($purchases) && ($purchases == false)) {
                $purchases = array();
                
            }

            $data['purchases'] = $purchases;

            $config['base_url'] = base_url() . "users/purchases/pg/";
            $config['per_page'] = $per_page;

            $this->pagination->initialize($config);

            $data['pagination_links'] = $this->pagination->create_links();

            $this->load->view('users/temp/header', $data);
            $this->load->view('users/temp/sidebar');
            $this->load->view('users/all_purchases', $data);
            $this->load->view('users/temp/footer');
            $this->load->view('users/temp/footer_close');

        } else {
            redirect('login');
        }
    }

    public function Investments()
    {

        $loggedIn = $this->session->userdata('loggedIn');
        if ($loggedIn) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);
            $data['user'] = $user;
            $data['page_title'] = 'Investments - FarmGate Africa';
            $investments = array();
            $keyword = $this->input->get('keyword_uinv');

            $this->load->library('pagination');
            $config = $this->pagination_config;

            // Add the search data in session to populate the fields again
            $this->session->set_userdata('keyword_uinv', $keyword);
            $data['keyword_uinv'] = $this->session->userdata('keyword_uinv');

            // Get the page number if it is set
            $page = $this->uri->segment(3);
            $per_page = 10;
            if (empty($page)) {
                $page = 1;
                $offset = 0;
            } else {
                $page = (int) $page;
                $offset = $page * $per_page;
            }

            if (!empty($keyword)) {
                $investments = $this->investment_model->getInvestmentsByUserWithName($user_id, $keyword, $per_page, $offset, 'iv_id', 'DESC');
                $config['total_rows'] = $this->investment_model->getTotalInvestmentsByUserWithName($user_id, $keyword);
                $data['total_investments'] = $config['total_rows'];

            } else {
                $investments = $this->investment_model->getInvestmentsByUser($user_id, $per_page, $offset, 'iv_id', 'DESC');
                $config['total_rows'] = $this->investment_model->getTotalInvestmentsByUser($user_id);
                $data['total_investments'] = $config['total_rows'];
            }

            if (!empty($investments) && ($investments !== false)) {
                foreach ($investments as $investment) {
                    # code...
                    $investment->currency = $this->currency_model->getCurrency($investment->iv_currency_id)->cur_symbol;
                }
                $data['investments'] = $investments;
            }

            $config['base_url'] = base_url() . "users/investments/pg/";
            $config['per_page'] = $per_page;

            $this->pagination->initialize($config);

            $data['pagination_links'] = $this->pagination->create_links();

            $this->load->view('users/temp/header', $data);
            $this->load->view('users/temp/sidebar');
            $this->load->view('users/all_investments', $data);
            $this->load->view('users/temp/footer');
            $this->load->view('users/temp/footer_close');

        } else {
            redirect('login');
        }
    }

    public function orders()
    {
        $loggedIn = $this->session->userdata('loggedIn');
        if ($loggedIn) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);
            $data['user'] = $user;

            $orders = array();
            $keyword = $this->input->get('keyword_o');

            $this->load->library('pagination');

            // Add the search data in session to populate the fields again
            $this->session->set_userdata('keyword_o', $keyword);
            $data['keyword_o'] = $this->session->userdata('keyword_o');

            // Get the page number if it is set
            $page = $this->uri->segment(3);
            $per_page = 5;
            if (empty($page)) {
                $page = 1;
                $offset = 0;
            } else {
                $page = (int) $page;
                $offset = $page * $per_page;
            }

            if (!empty($keyword)) {
                $orders = $this->orders_model->getOrdersByUserWithNumber($user_id, $keyword, $per_page, $offset);
                $config['total_rows'] = $this->orders_model->getTotalOrdersByUserWithNumber($user_id, $keyword);
                $data['total_orders'] = $config['total_rows'];

            } else {
                $orders = $this->orders_model->getOrdersByUser($user_id, $per_page, $offset);
                $config['total_rows'] = $this->orders_model->getTotalOrdersByUser($user_id);
                $data['total_orders'] = $config['total_rows'];
            }

            if (!empty($orders) && ($orders !== false)) {
                foreach ($orders as $order) {
                    # code...
                    $order->unit = $this->units_model->getUnitName($order->prod_units_id);
                }
                $data['orders'] = $orders;
            }

            $config['base_url'] = base_url('users/orders/');
            $config['use_page_numbers'] = true;
            $config['reuse_query_string'] = true;
            $config['full_tag_open'] = '<nav aria-label="Page navigation"><ul class="pagination mt-4 justify-content-end d-flex">';
            $config['full_tag_close'] = '</ul></nav>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</div>';
            $config['first_link'] = 'First';
            $config['attributes'] = array('class' => 'page-link');
            $config['cur_tag_open'] = '&nbsp;<a class="page-link">';
            $config['cur_tag_close'] = '</a>';
            $config['next_link'] = '&raquo;';
            $config['prev_link'] = '&laquo;';
            $config['num_links'] = 3;
            $config['per_page'] = $per_page;

            $this->pagination->initialize($config);

            $data['pagination_links'] = $this->pagination->create_links();

            $this->load->view('users/temp/header', $data);
            //$this->load->view('users/temp/subheader');
            $this->load->view('users/temp/sidebar');
            $this->load->view('users/orders', $data);
            $this->load->view('users/temp/footer');
            $this->load->view('users/temp/footer_close');
        } else {
            redirect('login');
        }
    }

    public function lpo()
    {
        $loggedIn = $this->session->userdata('loggedIn');
        if ($loggedIn) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);
            $data['user'] = $user;

            $orders = array();

            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);


            if (isset($_POST['lpo_submit'])) {

                $this->form_validation->set_rules('product_name', 'Product Name', 'trim|required|min_length[3]');
                $this->form_validation->set_rules('product_description', 'Product Description', 'trim|required|min_length[8]');
                $this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|numeric');
                $this->form_validation->set_rules('delivery_add', 'Delivery Address', 'trim|required|min_length[8]');
                $this->form_validation->set_rules('sample_terms', 'Terms and Conditions', 'trim|required');
                if ($this->form_validation->run() == false) {

                    $this->load->view('users/temp/header', $data);
                    //$this->load->view('users/temp/subheader');
                    $this->load->view('users/temp/sidebar');
                    $this->load->view('users/lpo_form', $data);
                    $this->load->view('users/temp/footer');
                    $this->load->view('users/temp/footer_close');

                } else {

                    $product_name = $this->input->post('product_name');
                    $product_description = $this->input->post('product_description');
                    $quantity = (int) $this->input->post('quantity');
                    $delivery_add = $this->input->post('delivery_add');

                    $orderData = array(
                        'product_name' => $product_name,
                        'product_description' => $product_description,
                        'order_type' => 'lpo',
                        'quantity' => $quantity,
                        'delivery_add' => $delivery_add,
                        'pid' => $this->input->post('pid'),
                        'price' => 0,
                        'total' => 0,
                    );

                    $this->order($orderData);

                    $success_message = $this->open_delimiter_success . 'Your LPO has been successfully placed!  ' . $this->close_delimiter;
                    $this->session->set_flashdata('lpo_feedback', $success_message);

                    redirect('users/lpo', $data);
                }

            } else {

                $this->load->view('users/temp/header', $data);
                //$this->load->view('users/temp/subheader');
                $this->load->view('users/temp/sidebar');
                $this->load->view('users/lpo_form', $data);
                $this->load->view('users/temp/footer');
                $this->load->view('users/temp/footer_close');

            }
        } else {
            redirect('login');
        }
    }

    public function profile()
    {

        $loggedIn = $this->session->userdata('loggedIn');
        if ($loggedIn) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);

            $data['user'] = $user;
            $data['page_title'] = 'Profile - FarmGate Africa';

            $this->load->view('users/temp/header', $data);
            // //$this->load->view('users/temp/subheader');
            $this->load->view('users/temp/sidebar');
            $this->load->view('users/profile', $data);
            $this->load->view('users/temp/footer');
        } else {
            redirect('login');
        }
    }

    public function editProfile()
    {

        $loggedIn = $this->session->userdata('loggedIn');
        if ($loggedIn) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);

            $data['user'] = $user;
            $data['page_title'] = 'Edit Profile - FarmGate Africa';

            $this->load->library('form_validation');

            $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);

            if (isset($_POST['account_btn'])) {

                $this->form_validation->set_rules('pro_full_name', 'Full Name', 'trim|required');
                $this->form_validation->set_rules('pro_phone', 'Phone', 'trim|required|numeric|min_length[10]');


                if ($this->form_validation->run() == false) {

                    $this->load->view('users/temp/header', $data);
                    $this->load->view('users/temp/sidebar');
                    $this->load->view('users/editprofile', $data);
                    $this->load->view('users/temp/footer');

                } else {
                    // process Account Update
                    $accountData = array(
                        'full_name' => $this->input->post('pro_full_name'),
                        'address' => $this->input->post('pro_address'),
                        'phone' => $this->input->post('pro_phone'),
                        'acc_name' => $this->input->post('pro_acc_name'),
                        'acc_number' => $this->input->post('pro_acc_number'),
                        'bank_name' => $this->input->post('pro_bank_name'),
                        'date_updated' => date('Y-m-d H:i:s'),
                    );

                    $this->user_model->updateUser($user->id, $accountData);

                    if (!empty($_FILES['userImageFile']['name'])) {
                        // $this->do_upload($user->id, $data);
                        $path = './mainasset/fe/images/users/';
                        $results = $this->do_upload($path, 'userImageFile');
                        if (!isset($results['errors'])) {
                            //add the images to the cusers table
                            $imageData = array(
                                'image' => $results['file_name'],
                                'date_updated' => date('Y-m-d H:i:s'),
                            );
                            $this->user_model->updateUser($user->id, $imageData);
                            //Show success message
                            $success_message = $this->open_delimiter_success . 'Your Account information has been updated successfully' . $this->close_delimiter;
                            $this->session->set_flashdata('acc_feedback', $success_message);
                        } else {
                            $data['errors'] = $results['errors'];
                            $this->session->set_flashdata('acc_feedback', $results['errors']);
                        }
                    } else {

                        //Show error
                        $fail_message = $this->open_delimiter . 'Your have not selected any Company Image file to upload!' . $this->close_delimiter;
                        $this->session->set_flashdata('acc_feedback', $fail_message);
                    }

                    $this->load->view('users/temp/header', $data);
                    $this->load->view('users/temp/sidebar');
                    $this->load->view('users/editprofile', $data);
                    $this->load->view('users/temp/footer');

                }

            } else {

                $this->load->view('users/temp/header', $data);
                $this->load->view('users/temp/sidebar');
                $this->load->view('users/editprofile', $data);
                $this->load->view('users/temp/footer');

            }

        } else {
            redirect('login');
        }
    }

    public function messages()
    {

        $loggedIn = $this->session->userdata('loggedIn');
        if ($loggedIn) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);

            $data['user'] = $user;
            $data['total_orders'] = $this->product_model->getTotalProductsByUser($user_id);
            $data['total_orders'] = $this->orders_model->getTotalOrdersByUser($user_id);

            $orders = $this->orders_model->getOrdersByUser($user_id, 40, 0);
            $data['orders'] = $orders;

            $this->load->view('users/temp/header', $data);
            //$this->load->view('users/temp/subheader');
            $this->load->view('users/temp/sidebar');
            $this->load->view('users/messages', $data);
            $this->load->view('users/temp/footer');
            $this->load->view('users/temp/scripts/messages', $data);
            $this->load->view('users/temp/footer_close');
        } else {
            redirect('login');
        }
    }

    public function messagechat()
    {

        $loggedIn = $this->session->userdata('loggedIn');
        if ($loggedIn) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);
            $order_number = $this->uri->segment(3);
            $order = $this->orders_model->getOrderByNumber($order_number);
            $data['order'] = $order;

            $data['user'] = $user;
            $data['total_products'] = $this->product_model->getTotalProductsByUser($user_id);
            $data['total_orders'] = $this->orders_model->getTotalOrdersByUser($user_id);
            $data['last_msg_id'] = $this->messages_model->getLastMessageByOrder($order->or_id);

            $this->load->view('users/messagechat', $data);
            $this->load->view('users/temp/scripts/messagechat', $data);

        } else {
            redirect('login');
        }

    }

    public function postMessage()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        // Get all the data
        $user_id = $this->session->userdata('uid');
        // $admin_id = $this->session->userdata('admin_uid');
        $admin_id = 0;
        $user_type = $this->user_model->getUser($user_id)->type;
        $message = $this->input->post('message_text');
        $order_id = $this->input->post('order_id');

        $responses = array();

        if (!empty($message)) {
            // Arrange the data
            $messageData = array(
                'm_order_id' => $order_id,
                'm_sender_id' => $user_id,
                'm_sender_type' => $user_type,
                'm_receiver_id' => $admin_id,
                'm_receiver_type' => 'admin',
                'm_message' => $message,
            );
            // save it into the database
            $last_id = $this->messages_model->newMessage($messageData);

            // Send response back to Ajax
            $response = array('id' => $last_id, 'type' => 'text', 'content' => $message, 'cdate' => date('h:i a - d M Y'));
            array_push($responses, $response);
        }

        if (!empty($_FILES['messageFile']['name'])) {
            $path = './mainasset/fe/images/messages/';
            $results = $this->do_upload($path, 'messageFile');
            if (!isset($results['errors'])) {
                $imageData = array(
                    'm_order_id' => $order_id,
                    'm_sender_id' => $user_id,
                    'm_sender_type' => $user_type,
                    'm_receiver_id' => $admin_id,
                    'm_receiver_type' => 'admin',
                    'm_file' => $results['file_name'],
                );
                $last_id = $this->messages_model->newMessage($imageData);
                $response = array('id' => $last_id, 'sender' => $user_type, 'type' => 'image', 'content' => $results['file_name'], 'cdate' => date('h:i a - d M Y'));
                array_push($responses, $response);
            } else {
                $response = array('type' => 'errors', 'sender' => $user_type, 'content' => $results['errors'], 'cdate' => date('h:i a - d M Y'));
                array_push($responses, $response);
            }
        }

        echo json_encode($responses);

    }

    public function loadMessages()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        // Get all the data
        $user_id = $this->session->userdata('uid');
        $user_type = $this->user_model->getUser($user_id)->type;
        $last_id = $this->input->post('last_id');
        $order_id = $this->input->post('order_id');
        $initial_load = $this->input->post('initial_load');
        $limit = 10;

        $messages = '';
        $responses = array();
        if (!empty($initial_load)) {
            $messages = $this->messages_model->getMostRecentMessagesByOrder($order_id, $limit);
        } else {
            $messages = $this->messages_model->getPreviousMessagesOnOrder($order_id, $last_id, $limit);

        }
        if (!empty($messages)) {
            foreach ($messages as $message) {
                if (!empty($message->m_file)) {
                    $response = array('id' => $message->m_id, 'sender' => $message->m_sender_type, 'type' => 'image', 'content' => $message->m_file, 'cdate' => date('h:i a - d M Y', strtotime($message->m_date_sent)));
                    array_push($responses, $response);
                } else {
                    $response = array('id' => $message->m_id, 'sender' => $message->m_sender_type, 'type' => 'text', 'content' => $message->m_message, 'cdate' => date('h:i a - d M Y', strtotime($message->m_date_sent)));
                    array_push($responses, $response);
                }
            }

        }
        // Send response back to Ajax
        echo json_encode($responses);

    }

    public function newMessage()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        // Get all the data
        $user_id = $this->session->userdata('uid');
        $user_type = $this->user_model->getUser($user_id)->type;
        $last_id = $this->input->post('last_id');
        $order_id = $this->input->post('order_id');

        $messages = '';
        $responses = array();
        $message = $this->messages_model->getLastMessageByOrder($order_id);

        if (!empty($message)) {

            if ($last_id < $message->m_id) {
                if (!empty($message->m_file)) {
                    $response = array('id' => $message->m_id, 'sender' => $message->m_sender_type, 'type' => 'image', 'content' => $message->m_file, 'cdate' => date('h:i a - d M Y', strtotime($message->m_date_sent)));
                    array_push($responses, $response);
                } else {
                    $response = array('id' => $message->m_id, 'sender' => $message->m_sender_type, 'type' => 'text', 'content' => $message->m_message, 'cdate' => date('h:i a - d M Y', strtotime($message->m_date_sent)));
                    array_push($responses, $response);
                }
            }

        }
        // Send response back to Ajax
        echo json_encode($responses);

    }

    public function logout()
    {
        $data = array();

        if ($this->session->userdata('admin_loggedIn')) {
            $this->user_model->updateUser($this->session->userdata('admin_uid'), array('logged_in' => 'no', 'date_updated' => date('Y-m-d H:i:s')));
            $logData = array('lg_user_id' => $this->session->userdata('admin_uid'));
            $logData['lg_message'] = "Logged Out. ";
            $this->log_model->newLog($logData);
            $data = array('admin_loggedIn', 'admin_uid');
        }
        
        if ($this->session->userdata('loggedIn')) {
            $this->user_model->updateUser($this->session->userdata('uid'), array('logged_in' => 'no', 'date_updated' => date('Y-m-d H:i:s')));
            $logData = array('lg_user_id' => $this->session->userdata('uid'));
            $logData['lg_message'] = "Logged Out. ";
            $this->log_model->newLog($logData);
            $data = array('loggedIn', 'uid');
        }

        $this->session->unset_userdata($data);
        redirect(base_url());
    }

    public function sampleRequest()
    {
        $data = array();
        $product_slug = $this->uri->segment(2);
        if (!empty($product_slug)) {
            $product = $this->product_model->getProductBySlug($product_slug);
            if (!empty($product)) {
                # code...
                $product->state = $this->state_model->getStateName($product->prod_states_id);
                $product->local = $this->locals_model->getLocalName($product->prod_locals_id);
                $product->unit = $this->units_model->getUnitName($product->prod_units_id);
                $product->category = $this->categories_model->getCategoryName($product->prod_category_id);

                $data['product'] = $product;

                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters($this->open_delimiter, $this->close_delimiter);

                if (isset($_POST['sample_submit'])) {

                    $this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|numeric');
                    $this->form_validation->set_rules('delivery_add', 'Delivery Address', 'trim|required|min_length[8]');
                    $this->form_validation->set_rules('sample_terms', 'Terms and Conditions', 'trim|required');
                    if ($this->form_validation->run() == false) {

                        $this->load->view('users/sample_request', $data);

                    } else {

                        $quantity = (int) $this->input->post('quantity');
                        $price_per_unit = (int) $this->input->post('price_per_unit');
                        $total = $quantity * $price_per_unit;

                        $orderData = array(
                            'quantity' => $quantity,
                            'delivery_add' => $this->input->post('delivery_add'),
                            'pid' => $this->input->post('pid'),
                            'price' => $price_per_unit,
                            'total' => $total,
                        );

                        $this->order($orderData);

                        $success_message = $this->open_delimiter_success . 'Your Order has been successfully added!' . $this->close_delimiter;
                        $this->session->set_flashdata('order_feedback', $success_message);

                        $this->load->view('users/sample_request_success', $data);
                    }

                } else {

                    $this->load->view('users/sample_request', $data);

                }

            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function order($data = array())
    {
        if (!empty($data)) {

            // Initialize email arguments
            $message = "";
            $subject = "";

            // Begin Order Processing
            $newOrder = array(
                'or_user_id' => $this->session->userdata('uid'),
                'or_date_created' => date('Y-m-d H:i:s'),
                'or_status' => 'new',
            );
            // Make changes if the Order type is an LPO

            if(!empty($data['order_type'])){
                $newOrder['or_order_type'] = 'lpo';
                $data['pid'] = 0;
            }else{
                $data['product_name'] = "";
                $data['product_description'] = "";
            }
            $order = $this->orders_model->newOrder($newOrder);

            // Add the Order Details
            // foreach ($data as $orderDetail) {

            $newOrderDetail = array(
                'ord_order_id' => $order->or_id,
                'ord_product_id' => $data['pid'],
                'ord_order_number' => $order->or_order_number,
                'ord_quantity' => $data['quantity'],
                'ord_delivery_add' => $data['delivery_add'],
                'ord_price' => $data['price'],
                'ord_total' => $data['total'],
                'ord_lpo_prod_name' => $data['product_name'],
                'ord_lpo_prod_desc' => $data['product_description'],
                'ord_date_created' => date('Y-m-d H:i:s'),
                'ord_active' => 'yes',
            );

            $this->orderdetails_model->newOrderDetail($newOrderDetail);
            $order = $this->orders_model->getOrderByNumber($order->or_order_number);

            if(!empty($order)){
                
                $customer = $this->user_model->getUser($order->or_user_id);
                if (!empty($customer)) {
                    $order->full_name = $customer->full_name;
                } else {
                    $order->full_name = "Guest User";
                }

            }

            $data['order'] = $order;
            // Send admin a mail notification with the Order Id and link
            if(!empty($data['order_type'])){
                $message = $this->load->view('email_templates/admin_new_lpo_order.php', $data, true);
                $subject = 'New LPO From Client';
            }else{
                $message = $this->load->view('email_templates/admin_new_order.php', $data, true);
                $subject = 'New Order From Client';
            }
            $to = 'emmanuel.torty@farmcrowdy.com';
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // More headers
            $headers .= 'From: <contact@farmgatetrader.com>' . "\r\n";

            mail($to, $subject, $message, $headers);

            // }
            return true;

        } else {
            return false;

        }

    }

    private function upload_files($path, $title, $files)
    {
        $config = array(
            'upload_path' => $path,
            'allowed_types' => 'jpg|jpeg|png|pdf',
            'overwrite' => false,
            'max_size' => 2048,
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name'] = $files['name'][$key];
            $_FILES['images[]']['type'] = $files['type'][$key];
            $_FILES['images[]']['tmp_name'] = $files['tmp_name'][$key];
            $_FILES['images[]']['error'] = $files['error'][$key];
            $_FILES['images[]']['size'] = $files['size'][$key];

            $fileName = $title . '_' . $image;

            $images[] = $fileName;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                $image_errors = array('errors' => $this->upload->display_errors());
                $image_errors['errors'] = $this->open_delimiter . $image_errors['errors'] . $this->close_delimiter;
                return $image_errors;
            }
        }

        return $images;
    }

    public function do_upload($path, $file)
    {

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'jpg|png|jpeg|pdf';
        $config['max_size'] = 2048;
        $config['overwrite'] = false;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $results = array();

        if ($this->upload->do_upload($file)) {
            $results = $this->upload->data();
            return $results;
        } else {
            $results = array('errors' => $this->upload->display_errors());
            $results['errors'] = $this->open_delimiter . $results['errors'] . $this->close_delimiter;
            return $results;
        }

    }

    public function changePassword()
    {

        $loggedIn = $this->session->userdata('loggedIn');
        if ($loggedIn) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);

            $current_password = $this->input->post('current_password');
            $new_password = $this->input->post('new_password');
            $new_password_confirm = $this->input->post('new_password_confirm');

            $hash_current_password = hash('sha512', $current_password);
            if ($user->password === $hash_current_password) {
                // echo json_encode("Correct Password! Proceed Mehn... ");
                // Check the length of the new passwords and they must match
                if (strlen($new_password) >= 8) {
                    // echo json_encode("New Password length is good ");
                    if ($new_password == $new_password_confirm) {
                        // echo json_encode("New Passwords match.. go on ");
                        $hash_new_password = hash('sha512', $new_password);
                        $uData = array('password' => $hash_new_password, 'date_updated' => date('Y-m-d H:i:s'));
                        $this->user_model->updateUser($user->id, $uData);

                        echo json_encode("Your Password has been changed successfully");

                    } else {
                        echo json_encode("New Passwords do not match. Check it and try again!");
                    }

                } else {
                    echo json_encode("New Password length is not good. It must be 8 or more characters long.");
                }

            } else {
                echo json_encode("Wrong Password supplied! ");
            }

        } else {

            echo json_encode("You are not logged in. ");
        }
    }

    public function contactManager()
    {

        $loggedIn = $this->session->userdata('loggedIn');
        $response = array();
        if ($loggedIn) {

            $user_id = $this->session->userdata('uid');
            $user = $this->user_model->getUser($user_id);
            $data['user'] = $user;

            $subject = $this->input->post('subject');
            $categorySelect = $this->input->post('categorySelect');
            $message = $this->input->post('message');

            if (strlen($subject) >= 15) {

                if (strlen($message) >= 15) {
                    // Post the content first then the image if it exists
                    $contactData = array(
                        'con_user_id' => $user_id,
                        'con_user_name'=> $user->full_name,
                        'con_subject' => $subject,
                        'con_category' => $categorySelect,
                        'con_message' => $message,
                        'con_date_created' => date('Y-m-d H:i:s'),
                    );

                    $contact_id = $this->contacts_model->newContact($contactData);
                    $contact = $this->contacts_model->getContact($contact_id);
                    $data['contact'] = $contact;
                    // var_dump($contact);

                    $response = array(
                        'message'=>'<br>Your Message has been sent! An Admin will contact you shortly.',
                        'status'=>'success');
                    echo json_encode($response);

                    // Upload Image if it exists
                    if (!empty($_FILES['contactImageFile']['name'])) {
                        $path = './mainasset/fe/images/contacts/';
                        $results = $this->do_upload($path, 'contactImageFile');
                        if (!isset($results['errors'])) {
                            $imageData = array(
                                'con_file' => $results['file_name'],
                            );
                            $this->contacts_model->updateContact($contact_id, $imageData);

                            // $success_message = '<br>Your attached file has also been sent!';
                            // $response['message'] = '<br>Your Message has been sent! An Admin will contact you shortly.';
                            // $response['status'] = 'success';
                            // echo json_encode($success_message);
                        } else {
                            $response = array(
                                'message'=>' There were file upload errors. Try again later  ',
                                'status'=>'error');
                            echo json_encode($response);
                        }
                    }

                } else {
                    $response = array(
                        'message'=>' You message is too short! It must be 15 characters or more. ',
                        'status'=>'error');
                    echo json_encode($response);
                }
            } else {
                $response = array(
                    'message'=>'The subject is too short! It must be 15 characters or more.',
                    'status'=>'error');
                echo json_encode($response);
            }

        } else {
            $response = array(
                'message'=>'You are not logged in. ',
                'status'=>'error');
            echo json_encode($response);
        }
    }

    public function getLocals()
    {
        $response = array();
        if (!$this->input->is_ajax_request()) {
            return;
        } else {

            $state_id = $this->input->post('state_id');
            $locals = $this->locals_model->getLocalsByState($state_id);
            $response['code'] = 1;
            $response['lgas'] = $locals;
        }
        echo json_encode($response);
    }

    public function testMails()
    {
        $data = array();
        $user_id = $this->session->userdata('uid');
        $user = $this->user_model->getUser($user_id);
        if (!empty($user)) {
            $data['user'] = $user;
        }

        $this->load->view('email_templates/verify_account.php', $data);
        $this->load->view('email_templates/new_user_signup.php', $data);
        $this->load->view('email_templates/forgot_password.php', $data);
        $this->load->view('email_templates/product_listed.php', $data);
        $this->load->view('email_templates/product_deleted.php', $data);
        $this->load->view('email_templates/new_product.php', $data);
        $this->load->view('email_templates/new_order.php', $data);
        $this->load->view('email_templates/order_deleted.php', $data);
        $this->load->view('email_templates/order_status.php', $data);
        $this->load->view('email_templates/admin_new_order.php', $data);

    }

    public function setIntent()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $response = FALSE;
        $current_url = $this->input->post('current_url');
        // var_dump($current_url);
        if(!empty($current_url)){
            $this->session->set_userdata('user_intent', $current_url);
            $response = TRUE;
        }

        echo json_encode($response);

    }

}
