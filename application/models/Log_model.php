<?php
    class Log_model extends CI_Model{

	function __construct()
	{
		parent::__construct('logs');
	}


	function newLog($data)
	{
		$this->db->insert('logs', $data);
		return $this->db->insert_id();
	}

	function getLog($id=0)
	{
		$query = $this->db->get_where('logs', array('lg_id' => $id));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return FALSE;
		}
    }
    
    public function getLogs($limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
        $this->db->order_by('lg_date_created','DESC');
		$query = $this->db->get('logs');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getlogsWithUserID($id, $limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
        $this->db->where('lg_user_id', $id);
        $this->db->order_by('lg_date_created','DESC');
		$query = $this->db->get('logs');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotallogsWithUserID($id)
    {
        $this->db->where('lg_user_id', $id);
        $query = $this->db->get('logs');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }

    public function getTotalLogs()
    {
        $query = $this->db->get('logs');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }

	function getAllLogs()
	{
		$query = $this->db->get_where('logs');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return FALSE;
		}
    }
    
	function updateLog($id, $data)
	{

		$this->db->where('lg_id', $id);
		$this->db->update('logs', $data);
	}

	function hardDelete($id)
	{
		$this->db->where('lg_id', $id);
		$this->db->delete('logs');
	}


}
