<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Languages_model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	function getLanguage($id = 0)
	{
		$query = $this->db->get_where('languages', array('id' => $id));
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	function getLanguageName($id = 0)
	{
		$query = $this->db->get_where('languages', array('id' => $id));
		if ($query->num_rows() > 0) {
			return $query->row()->name;
		} else {
			return false;
		}
	}

	function getAllLanguages()
	{
		$query = $this->db->get_where('languages');
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}


	function updateLanguage($id, $data)
	{

		$this->db->where('id', $id);
		$this->db->update('languages', $data);
	}

	function hardDelete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('languages');
	}



}