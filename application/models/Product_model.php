<?php
class Product_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function newProduct($data)
    {
        $this->db->insert('products', $data);
        return $this->db->insert_id();
    }

    public function getProduct($id = 0)
    {
        $query = $this->db->get_where('products', array('prod_id' => $id, 'prod_status' => 'available'));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getNewProduct($id = 0)
    {
        $query = $this->db->get_where('products', array('prod_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getProductBySlug($slug = "")
    {
        $query = $this->db->get_where('products', array('prod_slug' => $slug));

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getProducts($limit, $offset)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->where('prod_status', 'available');
        $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getAdminProducts($limit, $offset)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        // $this->db->where('prod_status', 'available');
        $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getAdminProductsWithName($keyword='', $limit, $offset)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(prod_name)', strtolower($keyword));
        $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalAdminProductsWithName($keyword='')
    {
        $this->db->like('LOWER(prod_name)', strtolower($keyword));
        $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array());
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function getTotalProducts()
    {
        $this->db->where('prod_status', 'available');
        $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array());

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function getTotalAdminProducts()
    {
        // $this->db->where('prod_status', 'available');
        $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array());

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function getProductsByCategory($cat_id = 0, $limit, $offset)
    {

        $this->db->where('prod_category_id', $cat_id);
        if ($offset == 0) {
            $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array('prod_status' => 'available'), $limit);
        } else {
            $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array('prod_status' => 'available'), $limit, $offset);
        }
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getProductsByUser($user_id = 0, $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->where('prod_user_id', $user_id);
        $this->db->where('prod_status !=', 'deleted');
        $this->db->order_by('prod_id', 'DESC');
        $query = $this->db->get('products');

        // var_dump($this->db->last_query());
        // die();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalProductsByUser($user_id = 0)
    {
        $this->db->where('prod_user_id', $user_id);
        $this->db->where('prod_status !=', 'deleted');
        $query = $this->db->get('products');
        // var_dump($this->db->last_query());
        // die();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }

    public function getProductsByUserWithName($user_id = 0, $keyword = '', $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->where('prod_user_id', $user_id);
        $this->db->like('LOWER(prod_name)', strtolower($keyword));
        $this->db->where('prod_status !=', 'deleted');
        $this->db->order_by('prod_id', 'DESC');

        $query = $this->db->get('products');

        // var_dump($this->db->last_query());
        // die();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalProductsByUserWithName($user_id = 0, $keyword = '')
    {

        $this->db->where('prod_user_id', $user_id);
        $this->db->like('LOWER(prod_name)', strtolower($keyword));
        $this->db->where('prod_status !=', 'deleted');
        
        $query = $this->db->get('products');

        // var_dump($this->db->last_query());
        // die();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function isProductFromUser($user_id = 0, $product_slug = '')
    {
        $this->db->where('prod_user_id', $user_id);
        $this->db->where('prod_slug', $product_slug);
        $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array());
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function getProductsByName($name = '', $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(prod_name)', strtolower($name));

        $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array('prod_status' => 'available'));
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalProductsByName($name = '')
    {
        $this->db->like('LOWER(prod_name)', strtolower($name));

        $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array('prod_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getProductsByState($state = array(), $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->where_in('prod_states_id', $state);

            $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array('prod_status' => 'available'));
        // var_dump($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalProductsByState($state = array())
    {
        $this->db->where_in('prod_states_id', $state);

        $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array('prod_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getProductsByNameAndStates($name = '', $state = array(), $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(prod_name)', strtolower($name));
        $this->db->where_in('prod_states_id', $state);

            $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array('prod_status' => 'available'));
        // var_dump($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalProductsByNameAndStates($name = '', $state = array())
    {
        $this->db->like('LOWER(prod_name)', strtolower($name));
        $this->db->where_in('prod_states_id', $state);

        $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array('prod_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getProductsByNameAndVolume($name = '', $min_volume, $max_volume, $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(prod_name)', strtolower($name));
        $this->db->where('CAST(prod_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(prod_min_order AS SIGNED ) >', $min_volume);

            $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array('prod_status' => 'available'));
        // var_dump($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalProductsByNameAndVolume($name = '', $min_volume, $max_volume)
    {
        $this->db->like('LOWER(prod_name)', strtolower($name));
        $this->db->where('CAST(prod_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(prod_min_order AS SIGNED ) >', $min_volume);

        $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array('prod_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getProductsByStatesAndVolume($state, $min_volume, $max_volume, $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->where_in('prod_states_id', $state);
        $this->db->where('CAST(prod_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(prod_min_order AS SIGNED ) >', $min_volume);
            $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array('prod_status' => 'available'), $limit);

        // var_dump($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalProductsByStatesAndVolume($state, $min_volume, $max_volume)
    {
        $this->db->where_in('prod_states_id', $state);
        $this->db->where('CAST(prod_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(prod_min_order AS SIGNED ) >', $min_volume);

        $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array('prod_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getProductsByVolume($min_volume = '', $max_volume = '', $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->where('CAST(prod_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(prod_min_order AS SIGNED ) >', $min_volume);

            $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array('prod_status' => 'available'));

        // var_dump($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalProductsByVolume($min_volume = '', $max_volume = '')
    {
        $this->db->where('CAST(prod_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(prod_min_order AS SIGNED ) >', $min_volume);

        $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array('prod_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getProductsByNameAndStatesAndVolume($name, $state, $min_volume = '', $max_volume = '', $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(prod_name)', strtolower($name));
        $this->db->where_in('prod_states_id', $state);
        $this->db->where('CAST(prod_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(prod_min_order AS SIGNED ) >', $min_volume);
            $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array('prod_status' => 'available'), $limit);

        // var_dump($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalProductsByNameAndStatesAndVolume($name, $state, $min_volume = '', $max_volume = '')
    {
        $this->db->like('LOWER(prod_name)', strtolower($name));
        $this->db->where_in('prod_states_id', $state);
        $this->db->where('CAST(prod_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(prod_min_order AS SIGNED ) >', $min_volume);

        $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array('prod_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getRecentlyAddedProducts($limit, $offset)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }

        $query = $this->db->order_by('prod_date_created', 'DESC')->join('states', 'states.state_id = products.prod_states_id')->get_where('products', array('products.prod_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getThrashedProducts($limit, $offset)
    {
        if ($offset == 0) {
            $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array('prod_status' => 'deleted'), $limit);
        } else {
            $query = $this->db->order_by('prod_date_created', 'DESC')->get_where('products', array('prod_status' => 'deleted'), $limit, $offset);
        }
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function generateProductSlug($product_name, $user_email)
    {

        // $slug = trim(strtolower( url_title($product_name,'dash', TRUE). '-' .md5($user_email) ));
        $product_short = strtolower(url_title(substr($product_name, 0, 2), 'dash', true));
        $rand_string = $user_email . mt_rand(10, 5688484);
        $unique_10 = substr(md5($rand_string), 0, 8);
        $slug = trim($product_short . $unique_10);

        return $slug;

    }

    public function slug_exists($slug = '')
    {
        $query = $this->db->get_where('products', array('prod_slug' => $slug));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function updateProduct($id, $data)
    {
        $this->db->where('prod_id', $id);
        $this->db->update('products', $data);
    }

    public function hardDelete($id)
    {
        $this->db->where('prod_id', $id);
        $this->db->delete('products');
    }

    public function get_enum_values($table, $field)
    {
        $type = $this->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }
}
