<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Farmer_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function newFarmer($data)
    {
        $this->db->insert('farmers', $data);
        return $this->db->insert_id();
    }

    public function getFarmer($id = 0)
    {
        $query = $this->db->get_where('farmers', array('fm_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getFarmerBySlug($slug = '')
    {
        $query = $this->db->get_where('farmers', array('fm_slug' => $slug));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getFarmers($limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
		$query = $this->db->get('farmers');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalFarmers()
    {
        $query = $this->db->get('farmers');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }

    public function getAllActiveFarmersWithName($keyword="")
    {
        $this->db->where('fm_status', 'active');
        $this->db->like('LOWER(fm_full_name)', strtolower($keyword));
		$query = $this->db->get('farmers');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalAllActiveFarmersWithName($keyword="")
    {
        $this->db->where('fm_status', 'active');
        $this->db->like('LOWER(fm_full_name)', strtolower($keyword));
		$query = $this->db->get('farmers');

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getFarmersWithName($keyword="")
    {
        $this->db->like('LOWER(fm_full_name)', strtolower($keyword));
        $query = $this->db->order_by('fm_date_created', 'DESC')->get_where('farmers', array());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalFarmersWithName($keyword="")
    {
        $this->db->like('LOWER(fm_full_name)', strtolower($keyword));
        $query = $this->db->order_by('fm_date_created', 'DESC')->get_where('farmers', array());
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function get_enum_values($table, $field)
    {
        $type = $this->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }

    public function updateFarmer($id, $data)
    {
        $this->db->where('fm_id', $id);
        $this->db->update('farmers', $data);
    }

    public function hardDelete($id)
    {
        $this->db->where('fm_id', $id);
        $this->db->delete('farmers');
    }

    public function generateSlug($fm_id = 0)
    {
        return 'FMR-' . substr(md5($fm_id . time()), 0, 8);
    }

    public function slug_exists($slug = '')
    {
        $query = $this->db->get_where('farmers', array('fm_slug' => $slug));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

}
