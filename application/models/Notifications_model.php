<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require_once(FUEL_PATH.'models/Base_module_model.php');

class Notifications_model extends Base_module_model {

	public $foreign_keys = array('user_id' => 'Feusers_model');

	function __construct()
	{
		parent::__construct('notifications');
	}

	function newNotification($data)
	{
		$this->db->insert('notifications', $data);	
		return $this->db->insert_id();
	}

	function getNotification($id=0)
	{
		$query = $this->db->get_where('notifications', array('id' => $id));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return FALSE;
		}
	}

	function getNotifications($limit, $offset)
    {
        if($offset == 0){
            $query = $this->db->order_by('date_created', 'DESC')->get_where('notifications', array('active' => 'yes'), $limit);      
        }else{
            $query = $this->db->order_by('date_created', 'DESC')->get_where('notifications', array('active' => 'yes'), $limit, $offset);
        }
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }

    }

    function getThrashedNotifications($limit, $offset)
    {
        if($offset == 0){
            $query = $this->db->order_by('date_created', 'DESC')->get_where('notifications', array('active' => 'deleted'), $limit);      
        }else{
            $query = $this->db->order_by('date_created', 'DESC')->get_where('notifications', array('active' => 'deleted'), $limit, $offset);
        }
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }

    }



	function updateNotification($id, $data)
	{

		$this->db->where('id', $id);
		$this->db->update('notifications', $data);
	}

	function hardDelete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('notifications');
	}

	function sendSMS()
	{

	}

	function sendEmail()
	{

	}

}

class Notification_model extends Base_module_record {

}