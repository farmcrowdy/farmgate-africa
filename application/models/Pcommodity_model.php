<?php
class Pcommodity_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function newPCommodity($data)
    {
        $this->db->insert('purchase_commodities', $data);
        return $this->db->insert_id();
    }

    public function getPCommodity($id = 0)
    {
        $query = $this->db->get_where('purchase_commodities', array('purc_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getNewPCommodity($id = 0)
    {
        $query = $this->db->get_where('purchase_commodities', array('purc_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getPCommodityBySlug($slug = "")
    {
        $query = $this->db->get_where('purchase_commodities', array('purc_slug' => $slug));

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getPCommodities($limit, $offset, $sortField, $sortOrder)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        return $this->sortBy($sortField, $sortOrder);

    }

    public function getAdminPCommodities($limit, $offset)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        // $this->db->where('purc_status', 'available');
        $query = $this->db->order_by('purc_date_created', 'DESC')->get_where('purchase_commodities', array());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getAdminPCommoditiesWithName($keyword='', $limit, $offset)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(purc_name)', strtolower($keyword));
        $query = $this->db->order_by('purc_date_created', 'DESC')->get_where('purchase_commodities', array());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalAdminPCommoditiesWithName($keyword='')
    {
        $this->db->like('LOWER(purc_name)', strtolower($keyword));
        $query = $this->db->order_by('purc_date_created', 'DESC')->get_where('purchase_commodities', array());
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function getTotalPCommodities()
    {
        $query = $this->db->order_by('purc_date_created', 'DESC')->get_where('purchase_commodities', array());

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function getTotalAdminPCommodities()
    {
        // $this->db->where('purc_status', 'available');
        $query = $this->db->order_by('purc_date_created', 'DESC')->get_where('purchase_commodities', array());

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function getPCommoditiesByCategory($cat_id = 0, $limit, $offset)
    {

        $this->db->where('purc_category_id', $cat_id);
        if ($offset == 0) {
            $query = $this->db->order_by('purc_date_created', 'DESC')->get_where('purchase_commodities', array('purc_status' => 'available'), $limit);
        } else {
            $query = $this->db->order_by('purc_date_created', 'DESC')->get_where('purchase_commodities', array('purc_status' => 'available'), $limit, $offset);
        }
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getPCommoditiesByUser($user_id = 0, $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->where('purc_user_id', $user_id);
        $this->db->where('purc_status !=', 'deleted');
        $this->db->order_by('purc_id', 'DESC');
        $query = $this->db->get('purchase_commodities');

        // var_dump($this->db->last_query());
        // die();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalPCommoditiesByUser($user_id = 0)
    {
        $this->db->where('purc_user_id', $user_id);
        $this->db->where('purc_status !=', 'deleted');
        $query = $this->db->get('purchase_commodities');
        // var_dump($this->db->last_query());
        // die();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }

    public function getPCommoditiesByUserWithName($user_id = 0, $keyword = '', $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->where('purc_user_id', $user_id);
        $this->db->like('LOWER(purc_name)', strtolower($keyword));
        $this->db->where('purc_status !=', 'deleted');
        $this->db->order_by('purc_id', 'DESC');

        $query = $this->db->get('purchase_commodities');

        // var_dump($this->db->last_query());
        // die();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalPCommoditiesByUserWithName($user_id = 0, $keyword = '')
    {

        $this->db->where('purc_user_id', $user_id);
        $this->db->like('LOWER(purc_name)', strtolower($keyword));
        $this->db->where('purc_status !=', 'deleted');
        
        $query = $this->db->get('purchase_commodities');

        // var_dump($this->db->last_query());
        // die();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function isPCommodityFromUser($user_id = 0, $product_slug = '')
    {
        $this->db->where('purc_user_id', $user_id);
        $this->db->where('purc_slug', $product_slug);
        $query = $this->db->order_by('purc_date_created', 'DESC')->get_where('purchase_commodities', array());
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function sortBy($field,$order='ASC')
    {
        if(empty($field)){
            $query = $this->db->get('purchase_commodities');
        }else{
            $query = $this->db->order_by($field, $order)->get('purchase_commodities');
        }
        // var_dump($this->db->last_query());
        // var_dump($query->result());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getPCommoditiesByName($name = '', $limit = 0, $offset = 0, $sortField, $sortOrder)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(purc_name)', strtolower($name));
        return $this->sortBy($sortField, $sortOrder);
    }

    public function getTotalPCommoditiesByName($name = '')
    {
        $this->db->like('LOWER(purc_name)', strtolower($name));
        $query = $this->db->get('purchase_commodities');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getPCommoditiesByStatus($status = array(), $limit = 0, $offset = 0, $sortField, $sortOrder)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }

        if(count($status) == 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->where('purc_status', $stat);
                }  
            }
        }elseif(count($status) > 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->or_where('purc_status', $stat);
                }  
            }
        }
        return $this->sortBy($sortField, $sortOrder);

    }

    public function getTotalPCommoditiesByStatus($status = array())
    {
        if(count($status) == 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->where('purc_status', $stat);
                }  
            }
        }elseif(count($status) > 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->or_where('purc_status', $stat);
                }  
            }
        }

        $query = $this->db->get('purchase_commodities');

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getPCommoditiesByNameAndStatus($name = '', $status = array(), $limit = 0, $offset = 0, $sortField, $sortOrder)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(purc_name)', strtolower($name));

        if(count($status) == 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->where('purc_status', $stat);
                }  
            }
        }elseif(count($status) > 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->or_where('purc_status', $stat);
                }  
            }
        }

        return $this->sortBy($sortField, $sortOrder);

    }

    public function getTotalPCommoditiesByNameAndStatus($name = '', $status = array())
    {
        $this->db->like('LOWER(purc_name)', strtolower($name));
        
        if(count($status) == 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->where('purc_status', $stat);
                }  
            }
        }elseif(count($status) > 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->or_where('purc_status', $stat);
                }  
            }
        }
        $query = $this->db->get('purchase_commodities');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getPCommoditiesByNameAndVolume($name = '', $min_volume, $max_volume, $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(purc_name)', strtolower($name));
        $this->db->where('CAST(purc_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(purc_min_order AS SIGNED ) >', $min_volume);

            $query = $this->db->order_by('purc_date_created', 'DESC')->get_where('purchase_commodities', array('purc_status' => 'available'));
        // var_dump($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalPCommoditiesByNameAndVolume($name = '', $min_volume, $max_volume)
    {
        $this->db->like('LOWER(purc_name)', strtolower($name));
        $this->db->where('CAST(purc_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(purc_min_order AS SIGNED ) >', $min_volume);

        $query = $this->db->order_by('purc_date_created', 'DESC')->get_where('purchase_commodities', array('purc_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getPCommoditiesByStatesAndVolume($state, $min_volume, $max_volume, $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->where_in('purc_states_id', $state);
        $this->db->where('CAST(purc_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(purc_min_order AS SIGNED ) >', $min_volume);
            $query = $this->db->order_by('purc_date_created', 'DESC')->get_where('purchase_commodities', array('purc_status' => 'available'), $limit);

        // var_dump($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalPCommoditiesByStatesAndVolume($state, $min_volume, $max_volume)
    {
        $this->db->where_in('purc_states_id', $state);
        $this->db->where('CAST(purc_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(purc_min_order AS SIGNED ) >', $min_volume);

        $query = $this->db->order_by('purc_date_created', 'DESC')->get_where('purchase_commodities', array('purc_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getPCommoditiesByVolume($min_volume = '', $max_volume = '', $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->where('CAST(purc_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(purc_min_order AS SIGNED ) >', $min_volume);

            $query = $this->db->order_by('purc_date_created', 'DESC')->get_where('purchase_commodities', array('purc_status' => 'available'));

        // var_dump($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalPCommoditiesByVolume($min_volume = '', $max_volume = '')
    {
        $this->db->where('CAST(purc_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(purc_min_order AS SIGNED ) >', $min_volume);

        $query = $this->db->order_by('purc_date_created', 'DESC')->get_where('purchase_commodities', array('purc_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getPCommoditiesByNameAndStatesAndVolume($name, $state, $min_volume = '', $max_volume = '', $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(purc_name)', strtolower($name));
        $this->db->where_in('purc_states_id', $state);
        $this->db->where('CAST(purc_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(purc_min_order AS SIGNED ) >', $min_volume);
            $query = $this->db->order_by('purc_date_created', 'DESC')->get_where('purchase_commodities', array('purc_status' => 'available'), $limit);

        // var_dump($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalPCommoditiesByNameAndStatesAndVolume($name, $state, $min_volume = '', $max_volume = '')
    {
        $this->db->like('LOWER(purc_name)', strtolower($name));
        $this->db->where_in('purc_states_id', $state);
        $this->db->where('CAST(purc_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(purc_min_order AS SIGNED ) >', $min_volume);

        $query = $this->db->order_by('purc_date_created', 'DESC')->get_where('purchase_commodities', array('purc_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getRecentlyAddedPCommodities($limit, $offset)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }

        $query = $this->db->order_by('purc_date_created', 'DESC')->join('states', 'states.state_id = purchase_commodities.purc_states_id')->get_where('purchase_commodities', array('purchase_commodities.purc_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getThrashedPCommodities($limit, $offset)
    {
        if ($offset == 0) {
            $query = $this->db->order_by('purc_date_created', 'DESC')->get_where('purchase_commodities', array('purc_status' => 'deleted'), $limit);
        } else {
            $query = $this->db->order_by('purc_date_created', 'DESC')->get_where('purchase_commodities', array('purc_status' => 'deleted'), $limit, $offset);
        }
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function generatePCommoditySlug($product_name, $user_email)
    {

        // $slug = trim(strtolower( url_title($product_name,'dash', TRUE). '-' .md5($user_email) ));
        $product_short = strtolower(url_title(substr($product_name, 0, 2), 'dash', true));
        $rand_string = $user_email . mt_rand(10, 5688484);
        $unique_10 = substr(md5($rand_string), 0, 8);
        $slug = trim($product_short . $unique_10);

        return $slug;

    }

    public function slug_exists($slug = '')
    {
        $query = $this->db->get_where('purchase_commodities', array('purc_slug' => $slug));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function name_exists($name = '')
    {
        $query = $this->db->get_where('purchase_commodities', array('purc_name' => $name));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function updatePCommodity($id, $data)
    {
        $this->db->where('purc_id', $id);
        $this->db->update('purchase_commodities', $data);
    }

    public function hardDelete($id)
    {
        $this->db->where('purc_id', $id);
        $this->db->delete('purchase_commodities');
    }

    public function get_enum_values($table, $field)
    {
        $type = $this->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }
}
