<?php
class Producetype_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function newProduceType($data)
    {
        $this->db->insert('produce_types', $data);
        return $this->db->insert_id();
    }

    public function getProduceType($id = 0)
    {
        $query = $this->db->get_where('produce_types', array('pts_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getProduceTypeName($id = 0)
    {
        $query = $this->db->get_where('produce_types', array('pts_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row()->pts_name;
        } else {
            return false;
        }
    }

    public function getAllProduceTypes()
    {
        $query = $this->db->get_where('produce_types');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getProduceTypeBySlug($slug = "")
    {
        $query = $this->db->get_where('produce_types', array('pts_slug' => $slug));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function updateProduceType($id, $data)
    {

        $this->db->where('pts_id', $id);
        $this->db->update('produce_types', $data);
    }

    public function hardDelete($id)
    {
        $this->db->where('pts_id', $id);
        $this->db->delete('produce_types');
    }

    public function produceTypeExists($category = '')
    {
        $query = $this->db->get_where('produce_types', array('pts_name' => $category));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

}
