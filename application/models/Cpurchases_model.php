<?php
class Cpurchases_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function newCPurchase($data)
    {
        $this->db->insert('customer_purchases', $data);
        return $this->db->insert_id();
    }

    public function getCPurchase($id = 0)
    {
        $query = $this->db->get_where('customer_purchases', array('cusp_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getNewCPurchase($id = 0)
    {
        $query = $this->db->get_where('customer_purchases', array('cusp_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getCPurchaseBySlug($slug = "")
    {
        $query = $this->db->get_where('customer_purchases', array('cusp_slug' => $slug));

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getCPurchases($limit, $offset, $sortField, $sortOrder)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        return $this->sortBy($sortField, $sortOrder);

    }

    public function getAdminCPurchases($limit, $offset)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        // $this->db->where('cusp_status', 'available');
        $query = $this->db->order_by('cusp_date_created', 'DESC')->get_where('customer_purchases', array());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getCPurchasesWithName($keyword='', $limit, $offset)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(cusp_slug)', strtolower($keyword));
        $query = $this->db->order_by('cusp_date_created', 'DESC')->get_where('customer_purchases', array());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalCPurchasesWithName($keyword='')
    {
        $this->db->like('LOWER(cusp_slug)', strtolower($keyword));
        $query = $this->db->order_by('cusp_date_created', 'DESC')->get_where('customer_purchases', array());
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function getTotalCPurchases()
    {
        $query = $this->db->order_by('cusp_date_created', 'DESC')->get_where('customer_purchases', array());

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function getTotalAdminCPurchases()
    {
        // $this->db->where('cusp_status', 'available');
        $query = $this->db->order_by('cusp_date_created', 'DESC')->get_where('customer_purchases', array());

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function getCPurchasesByCategory($cat_id = 0, $limit, $offset)
    {

        $this->db->where('cusp_category_id', $cat_id);
        if ($offset == 0) {
            $query = $this->db->order_by('cusp_date_created', 'DESC')->get_where('customer_purchases', array('cusp_status' => 'available'), $limit);
        } else {
            $query = $this->db->order_by('cusp_date_created', 'DESC')->get_where('customer_purchases', array('cusp_status' => 'available'), $limit, $offset);
        }
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getCPurchasesByUser($user_id = 0, $limit = 0, $offset = 0, $sortField, $sortOrder )
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->where('cusp_user_id', $user_id);
        return $this->sortBy($sortField, $sortOrder);
    }

    public function getTotalCPurchasesByUser($user_id = 0)
    {
        $this->db->where('cusp_user_id', $user_id);
        $query = $this->db->get('customer_purchases');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }

    public function getCPurchasesByUserWithName($user_id = 0, $keyword = '', $limit = 0, $offset = 0, $sortField, $sortOrder)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->where('cusp_user_id', $user_id);
        $this->db->like('LOWER(cusp_commodity_name)', strtolower($keyword));
        return $this->sortBy($sortField, $sortOrder);
    }

    public function getTotalCPurchasesByUserWithName($user_id = 0, $keyword = '')
    {

        $this->db->where('cusp_user_id', $user_id);
        $this->db->like('LOWER(cusp_commodity_name)', strtolower($keyword));        
        $query = $this->db->get('customer_purchases');

        // var_dump($this->db->last_query());
        // die();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function isCPurchasesFromUser($user_id = 0, $product_slug = '')
    {
        $this->db->where('cusp_user_id', $user_id);
        $this->db->where('cusp_slug', $product_slug);
        $query = $this->db->order_by('cusp_date_created', 'DESC')->get_where('customer_purchases', array());
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function sortBy($field,$order='ASC')
    {
        if(empty($field)){
            $query = $this->db->get('customer_purchases');
        }else{
            $query = $this->db->order_by($field, $order)->get('customer_purchases');
        }
        // var_dump($this->db->last_query());
        // var_dump($query->result());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getCPurchasesByName($name = '', $limit = 0, $offset = 0, $sortField, $sortOrder)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(cusp_name)', strtolower($name));
        return $this->sortBy($sortField, $sortOrder);

    }

    public function getTotalCPurchasesByName($name = '')
    {
        $this->db->like('LOWER(cusp_name)', strtolower($name));
        $query = $this->db->get('customer_purchases');

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getCPurchasesByStatus($status = array(), $limit = 0, $offset = 0, $sortField, $sortOrder)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }

        if(count($status) == 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->where('cusp_status', $stat);
                }  
            }
        }elseif(count($status) > 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->or_where('cusp_status', $stat);
                }  
            }
        }
        return $this->sortBy($sortField, $sortOrder);

    }

    public function getTotalCPurchasesByStatus($status = array())
    {
        if(count($status) == 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->where('cusp_status', $stat);
                }  
            }
        }elseif(count($status) > 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->or_where('cusp_status', $stat);
                }  
            }
        }

        $query = $this->db->get('customer_purchases');

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getCPurchasesByNameAndStatus($name = '', $status = array(), $limit = 0, $offset = 0, $sortField, $sortOrder)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(cusp_name)', strtolower($name));

        if(count($status) == 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->where('cusp_status', $stat);
                }  
            }
        }elseif(count($status) > 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->or_where('cusp_status', $stat);
                }  
            }
        }

        return $this->sortBy($sortField, $sortOrder);

    }

    public function getTotalCPurchasesByNameAndStatus($name = '', $status = array())
    {
        $this->db->like('LOWER(cusp_name)', strtolower($name));

        if(count($status) == 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->where('cusp_status', $stat);
                }  
            }
        }elseif(count($status) > 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->or_where('cusp_status', $stat);
                }  
            }
        }
        $query = $this->db->get('customer_purchases');

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getCPurchasesByNameAndVolume($name = '', $min_volume, $max_volume, $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(cusp_name)', strtolower($name));
        $this->db->where('CAST(cusp_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(cusp_min_order AS SIGNED ) >', $min_volume);

            $query = $this->db->order_by('cusp_date_created', 'DESC')->get_where('customer_purchases', array('cusp_status' => 'available'));
        // var_dump($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getUndeliveredPurchases()
    {
        $this->db->where('cusp_quantity_delivered < cusp_quantity ');
        $query = $this->db->get('customer_purchases');
        // var_dump($this->db->last_query());
        // die();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getRecentlyAddedCPurchases($limit, $offset)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }

        $query = $this->db->order_by('cusp_date_created', 'DESC')->join('states', 'states.state_id = customer_purchases.cusp_states_id')->get_where('customer_purchases', array('customer_purchases.cusp_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getThrashedCPurchases($limit, $offset)
    {
        if ($offset == 0) {
            $query = $this->db->order_by('cusp_date_created', 'DESC')->get_where('customer_purchases', array('cusp_status' => 'deleted'), $limit);
        } else {
            $query = $this->db->order_by('cusp_date_created', 'DESC')->get_where('customer_purchases', array('cusp_status' => 'deleted'), $limit, $offset);
        }
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function generateCPurchaseSlug($product_name, $user_email)
    {

        // $slug = trim(strtolower( url_title($product_name,'dash', TRUE). '-' .md5($user_email) ));
        $product_short = strtolower(url_title(substr($product_name, 0, 2), 'dash', true));
        $rand_string = $user_email . mt_rand(10, 5688484);
        $unique_10 = substr(md5($rand_string), 0, 8);
        $slug = trim($product_short . $unique_10);

        return $slug;

    }

    public function slug_exists($slug = '')
    {
        $query = $this->db->get_where('customer_purchases', array('cusp_slug' => $slug));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function name_exists($name = '')
    {
        $query = $this->db->get_where('customer_purchases', array('cusp_name' => $name));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function updateCPurchase($id, $data)
    {
        $this->db->where('cusp_id', $id);
        $this->db->update('customer_purchases', $data);
    }

    public function hardDelete($id)
    {
        $this->db->where('cusp_id', $id);
        $this->db->delete('customer_purchases');
    }

    public function get_enum_values($table, $field)
    {
        $type = $this->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }
}
