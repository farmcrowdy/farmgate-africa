<?php
class Commodity_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function newCommodity($data)
    {
        $this->db->insert('commodities', $data);
        return $this->db->insert_id();
    }

    public function getCommodity($id = 0)
    {
        $query = $this->db->get_where('commodities', array('comm_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getNewCommodity($id = 0)
    {
        $query = $this->db->get_where('commodities', array('comm_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getCommodityBySlug($slug = "")
    {
        $query = $this->db->get_where('commodities', array('comm_slug' => $slug));

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getCommodities($limit, $offset, $sortField, $sortOrder)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }

        $query = $this->db->order_by($sortField, $sortOrder)->get_where('commodities', array());

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return 0;
        }

    }

    public function getTotalCommodities()
    {
        $query = $this->db->order_by('comm_date_created', 'DESC')->get_where('commodities', array());

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function getCommoditiesWithName($keyword='', $limit, $offset, $sortField, $sortOrder)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(comm_name)', strtolower($keyword));
        
        return $this->sortBy($type, $sortField, $sortOrder);

    }

    public function getTotalCommoditiesWithName($keyword='')
    {
        $this->db->like('LOWER(comm_name)', strtolower($keyword));
        $query = $this->db->order_by('comm_id', 'DESC')->get_where('commodities', array());
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function getCommoditiesWithNameAndType($keyword='', $limit, $offset, $type, $sortField, $sortOrder)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(comm_name)', strtolower($keyword));
        
        return $this->sortBy($type, $sortField, $sortOrder);

    }

    public function getTotalCommoditiesWithNameAndType($keyword='', $type)
    {
        $this->db->like('LOWER(comm_name)', strtolower($keyword));
        $query = $this->db->order_by('comm_id', 'DESC')->get_where('commodities', array('comm_commodity_type'=>$type));
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function getCommoditiesByType($limit, $offset, $type, $sortField, $sortOrder)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }

        return $this->sortBy($type, $sortField, $sortOrder);

    }

    public function getTotalCommoditiesByType($type)
    {
        $query = $this->db->order_by('comm_id', 'DESC')->get_where('commodities', array('comm_commodity_type'=>$type, 'comm_partner_id' => 0));
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function getCommoditiesByUser($user_id = 0, $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->where('comm_user_id', $user_id);
        $this->db->order_by('comm_id', 'DESC');
        $query = $this->db->get('commodities');

        // var_dump($this->db->last_query());
        // die();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalCommoditiesByUser($user_id = 0)
    {
        $this->db->where('comm_user_id', $user_id);
        $this->db->where('comm_status !=', 'deleted');
        $query = $this->db->get('commodities');
        // var_dump($this->db->last_query());
        // die();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }

    public function getCommoditiesByUserWithName($user_id = 0, $keyword = '', $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->where('comm_user_id', $user_id);
        $this->db->like('LOWER(comm_name)', strtolower($keyword));
        $this->db->order_by('comm_id', 'DESC');

        $query = $this->db->get('commodities');

        // var_dump($this->db->last_query());
        // die();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalCommoditiesByUserWithName($user_id = 0, $keyword = '')
    {

        $this->db->where('comm_user_id', $user_id);
        $this->db->like('LOWER(comm_name)', strtolower($keyword));
        
        $query = $this->db->get('commodities');

        // var_dump($this->db->last_query());
        // die();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function isCommodityFromUser($user_id = 0, $product_slug = '')
    {
        $this->db->where('comm_user_id', $user_id);
        $this->db->where('comm_slug', $product_slug);
        $query = $this->db->order_by('comm_date_created', 'DESC')->get_where('commodities', array());
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function sortBy($type, $field, $order='ASC')
    {
        if(empty($field)){
            $query = $this->db->get_where('commodities', array('comm_commodity_type'=> $type));
        }else{
            $query = $this->db->order_by($field, $order)->get_where('commodities', array('comm_commodity_type'=> $type, 'comm_partner_id'=>0));
        }
        // var_dump($this->db->last_query());
        // var_dump($query->result());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getCommoditiesByName($name = '', $limit = 0, $offset = 0, $type, $sortField, $sortOrder)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(comm_name)', strtolower($name));
        return $this->sortBy($type, $sortField, $sortOrder);

    }

    public function getTotalCommoditiesByName($name = '', $type)
    {
        $this->db->like('LOWER(comm_name)', strtolower($name));
        $query = $this->db->get_where('commodities', array('comm_commodity_type' => $type));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getCommoditiesByStatus($status = array(), $limit = 0, $offset = 0, $type, $sortField, $sortOrder)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }

        $this->db->where_in('comm_status', $status );
        // $this->db->where_in('comm_status', $status );

        return $this->sortBy($type, $sortField, $sortOrder);

    }

    public function getTotalCommoditiesByStatus($status = array(), $type)
    {
        $this->db->where_in('comm_status', $status );

        $query = $this->db->get_where('commodities', array('comm_commodity_type'=>$type));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getCommoditiesByNameAndStatus($name = '', $status = array(), $limit = 0, $offset = 0, $type, $sortField, $sortOrder)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(comm_name)', strtolower($name));

        if(count($status) == 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->where('comm_status', $stat);
                }  
            }
        }elseif(count($status) > 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->or_where('comm_status', $stat);
                }  
            }
        }

        return $this->sortBy($type, $sortField, $sortOrder);

    }

    public function getTotalCommoditiesByNameAndStatus($name = '', $status = array(), $type)
    {
        $this->db->like('LOWER(comm_name)', strtolower($name));

        if(count($status) == 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->where('comm_status', $stat);
                }  
            }
        }elseif(count($status) > 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->or_where('comm_status', $stat);
                }  
            }
        }
        $query = $this->db->get_where('commodities', array('comm_commodity_type'=>$type));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getCommoditiesByNameAndVolume($name = '', $min_volume, $max_volume, $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(comm_name)', strtolower($name));
        $this->db->where('CAST(comm_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(comm_min_order AS SIGNED ) >', $min_volume);

            $query = $this->db->order_by('comm_date_created', 'DESC')->get_where('commodities', array('comm_status' => 'available'));
        // var_dump($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalCommoditiesByNameAndVolume($name = '', $min_volume, $max_volume)
    {
        $this->db->like('LOWER(comm_name)', strtolower($name));
        $this->db->where('CAST(comm_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(comm_min_order AS SIGNED ) >', $min_volume);

        $query = $this->db->order_by('comm_date_created', 'DESC')->get_where('commodities', array('comm_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getCommoditiesByStatesAndVolume($state, $min_volume, $max_volume, $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->where_in('comm_states_id', $state);
        $this->db->where('CAST(comm_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(comm_min_order AS SIGNED ) >', $min_volume);
            $query = $this->db->order_by('comm_date_created', 'DESC')->get_where('commodities', array('comm_status' => 'available'), $limit);

        // var_dump($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalCommoditiesByStatesAndVolume($state, $min_volume, $max_volume)
    {
        $this->db->where_in('comm_states_id', $state);
        $this->db->where('CAST(comm_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(comm_min_order AS SIGNED ) >', $min_volume);

        $query = $this->db->order_by('comm_date_created', 'DESC')->get_where('commodities', array('comm_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getCommoditiesByVolume($min_volume = '', $max_volume = '', $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->where('CAST(comm_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(comm_min_order AS SIGNED ) >', $min_volume);

            $query = $this->db->order_by('comm_date_created', 'DESC')->get_where('commodities', array('comm_status' => 'available'));

        // var_dump($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalCommoditiesByVolume($min_volume = '', $max_volume = '')
    {
        $this->db->where('CAST(comm_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(comm_min_order AS SIGNED ) >', $min_volume);

        $query = $this->db->order_by('comm_date_created', 'DESC')->get_where('commodities', array('comm_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getCommoditiesByNameAndStatesAndVolume($name, $state, $min_volume = '', $max_volume = '', $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(comm_name)', strtolower($name));
        $this->db->where_in('comm_states_id', $state);
        $this->db->where('CAST(comm_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(comm_min_order AS SIGNED ) >', $min_volume);
            $query = $this->db->order_by('comm_date_created', 'DESC')->get_where('commodities', array('comm_status' => 'available'), $limit);

        // var_dump($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalCommoditiesByNameAndStatesAndVolume($name, $state, $min_volume = '', $max_volume = '')
    {
        $this->db->like('LOWER(comm_name)', strtolower($name));
        $this->db->where_in('comm_states_id', $state);
        $this->db->where('CAST(comm_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(comm_min_order AS SIGNED ) >', $min_volume);

        $query = $this->db->order_by('comm_date_created', 'DESC')->get_where('commodities', array('comm_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getRecentlyAddedCommodities($limit, $offset)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }

        $query = $this->db->order_by('comm_date_created', 'DESC')->join('states', 'states.state_id = commodities.comm_states_id')->get_where('commodities', array('commodities.comm_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getThrashedCommodities($limit, $offset)
    {
        if ($offset == 0) {
            $query = $this->db->order_by('comm_date_created', 'DESC')->get_where('commodities', array('comm_status' => 'deleted'), $limit);
        } else {
            $query = $this->db->order_by('comm_date_created', 'DESC')->get_where('commodities', array('comm_status' => 'deleted'), $limit, $offset);
        }
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function generateCommoditySlug($product_name, $user_email)
    {

        // $slug = trim(strtolower( url_title($product_name,'dash', TRUE). '-' .md5($user_email) ));
        $product_short = strtolower(url_title(substr($product_name, 0, 2), 'dash', true));
        $rand_string = $user_email . mt_rand(10, 5688484);
        $unique_10 = substr(md5($rand_string), 0, 8);
        $slug = trim($product_short . $unique_10);

        return $slug;

    }

    public function slug_exists($slug = '')
    {
        $query = $this->db->get_where('commodities', array('comm_slug' => $slug));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function name_exists($name = '')
    {
        $query = $this->db->get_where('commodities', array('comm_name' => $name));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function updateCommodity($id, $data)
    {
        $this->db->where('comm_id', $id);
        $this->db->update('commodities', $data);
    }

    public function hardDelete($id)
    {
        $this->db->where('comm_id', $id);
        $this->db->delete('commodities');
    }

    public function get_enum_values($table, $field)
    {
        $type = $this->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }
}
