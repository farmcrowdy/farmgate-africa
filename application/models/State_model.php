<?php
    class State_model extends CI_Model{

	function __construct()
	{
		parent::__construct('states');
	}


	function newState($data)
	{
		$this->db->insert('states', $data);
		return $this->db->insert_id();
	}

	function getState($id=0)
	{
		$query = $this->db->get_where('states', array('state_id' => $id));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return FALSE;
		}
	}

	function getStateName($id=0)
	{
		$query = $this->db->get_where('states', array('state_id' => $id));
		if($query->num_rows() > 0){
			return $query->row()->name;
		}else{
			return FALSE;
		}
	}

	function getAllStates()
	{
		$query = $this->db->get_where('states');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return FALSE;
		}
	}



	function updateState($id, $data)
	{

		$this->db->where('state_id', $id);
		$this->db->update('states', $data);
	}

	function hardDelete($id)
	{
		$this->db->where('state_id', $id);
		$this->db->delete('states');
	}


}
