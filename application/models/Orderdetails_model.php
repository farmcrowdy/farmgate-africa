<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Orderdetails_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

	function newOrderDetail($data)
	{
		$this->db->insert('order_details', $data);
		return $this->db->insert_id();

	}

	function getOrderDetail($id = 0)
	{
		$query = $this->db->get_where('order_details', array('id' => $id));
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	function getOrderdetails($limit, $offset)
	{
		if ($offset == 0) {
			$query = $this->db->order_by('date_created', 'DESC')->get_where('order_details', array('active' => 'yes'), $limit);
		} else {
			$query = $this->db->order_by('date_created', 'DESC')->get_where('order_details', array('active' => 'yes'), $limit, $offset);
		}
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}

	}

	function getOrderdetailsByNumber($order_number = "", $limit, $offset)
	{
		if ($offset == 0) {
			$query = $this->db->order_by('date_created', 'DESC')->get_where('order_details', array('order_number' => $order_number, 'active' => 'yes'), $limit);
		} else {
			$query = $this->db->order_by('date_created', 'DESC')->get_where('order_details', array('order_number' => $order_number, 'active' => 'yes'), $limit, $offset);
		}
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}

	}


	function updateOrderDetail($id, $data)
	{

		$this->db->where('ord_id', $id);
		$this->db->update('order_details', $data);
	}

	function updateOrderDetailByNumber($order_number="", $data)
	{

		$this->db->where('ord_order_number', $order_number);
		$this->db->update('order_details', $data);
	}

	function rawQuery($sql = "")
	{
		$query = $this->db->query($sql);
		if ($query !== false) {
			return true;
		} else {
			return false;
		}
	}

	function hardDelete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('order_details');
	}


}