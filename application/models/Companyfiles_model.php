<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Companyfiles_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    function newCompanyfile($data)
    {
        $this->db->insert('user_company_files', $data);
        return $this->db->insert_id();
    }

    function getCompanyfile($id = 0)
    {
        $query = $this->db->get_where('user_company_files', array('cf_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function getCompanyfiles($limit, $offset)
    {
        if ($offset == 0) {
            $query = $this->db->order_by('cf_date_created', 'DESC')->get_where('user_company_files', array('cf_status' => 'active'), $limit);
        } else {
            $query = $this->db->order_by('cf_date_created', 'DESC')->get_where('user_company_files', array('cf_status' => 'active'), $limit, $offset);
        }
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    function getCompanyfilesForUser($user = '', $limit, $offset)
    {
        $this->db->where('cf_user_id', $user);
        if ($offset == 0) {
            $query = $this->db->order_by('cf_date_created', 'DESC')->get_where('user_company_files', array('cf_status' => 'active'), $limit);
        } else {
            $query = $this->db->order_by('cf_date_created', 'DESC')->get_where('user_company_files', array('cf_status' => 'active'), $limit, $offset);
        }
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    function getThrashedCompanyfiles($limit, $offset)
    {
        if ($offset == 0) {
            $query = $this->db->order_by('cf_date_created', 'DESC')->get_where('user_company_files', array('cf_status' => 'deleted'), $limit);
        } else {
            $query = $this->db->order_by('cf_date_created', 'DESC')->get_where('user_company_files', array('cf_status' => 'deleted'), $limit, $offset);
        }
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }



    function updateCompanyfile($id, $data)
    {

        $this->db->where('cf_id', $id);
        $this->db->update('user_company_files', $data);
    }

    function hardDelete($id)
    {
        $this->db->where('cf_id', $id);
        $this->db->delete('user_company_files');
    }

}