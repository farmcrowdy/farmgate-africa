<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Contacts_model extends CI_Model {

	public function __construct(){
		$this->load->database();
	 }

	function newContact($uData)
	{
		$this->db->insert('contacts', $uData);
		return $this->db->insert_id();
	}

	function getContact($id=0)
	{
		$query = $this->db->get_where('contacts', array('con_id' => $id));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return FALSE;
		}
	}

	function getContacts( $limit = 0, $offset = 0, $sortField, $sortOrder)
	{
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
		return $this->sortBy($sortField, $sortOrder);
	}

	function getTotalContacts()
	{
		$query = $this->db->get_where('contacts', array());
		if($query->num_rows() > 0){
			return $query->num_rows();
		}else{
			return FALSE;
		}
	}

	public function getContactsWithName($keyword='', $limit = 0, $offset = 0, $sortField, $sortOrder)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(con_user_name)', strtolower($keyword));
		return $this->sortBy($sortField, $sortOrder);

    }

	public function getTotalContactsWithName($keyword='')
    {
        $this->db->like('LOWER(con_user_name)', strtolower($keyword));
        $query = $this->db->order_by('con_date_created', 'DESC')->get_where('contacts', array());
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

	public function sortBy($field,$order='ASC')
    {
        if(empty($field)){
            $query = $this->db->get('contacts');
        }else{
            $query = $this->db->order_by($field, $order)->get('contacts');
        }
        // var_dump($this->db->last_query());
        // var_dump($query->result());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
	}
	
	function getAllContacts()
	{
		$query = $this->db->get_where('contacts');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return FALSE;
		}
	}

	function updateContact($id, $data)
	{

		$this->db->where('con_id', $id);
		$this->db->update('contacts', $data);
	}

	function hardDelete($id)
	{
		$this->db->where('con_id', $id);
		$this->db->delete('contacts');
	}



}