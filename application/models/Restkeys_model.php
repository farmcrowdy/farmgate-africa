<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Restkeys_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function newRestkey($data)
    {
        $this->db->insert('rest_keys', $data);
        return $this->db->insert_id();
    }

    public function getRestkeyUserName($api_key = '')
    {
        $query = $this->db->get_where('rest_keys', array('api_key' => $api_key));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

}
