<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Partner_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function newPartner($data)
    {
        $this->db->insert('partners', $data);
        return $this->db->insert_id();
    }

    public function getPartner($id = 0)
    {
        $query = $this->db->get_where('partners', array('id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getPartnerName($id = 0)
    {
        $query = $this->db->get_where('partners', array('id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row()->name;
        } else {
            return false;
        }
    }

    public function getPartnerBySlug($slug = '')
    {
        $query = $this->db->get_where('partners', array('bk_slug' => $slug, ));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getPartners($limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
		$query = $this->db->get('partners');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getAllPartners()
    {
		$query = $this->db->get('partners');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalPartners()
    {
        $query = $this->db->get('partners');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }

    public function getPartnersWithName($keyword="", $limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(bk_name)', strtolower($keyword));
        $query = $this->db->order_by('bk_date_created', 'DESC')->get_where('partners', array());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalPartnersWithName($keyword="")
    {
        $this->db->like('LOWER(bk_name)', strtolower($keyword));
        $query = $this->db->order_by('bk_date_created', 'DESC')->get_where('partners', array());
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function get_enum_values($table, $field)
    {
        $type = $this->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }

    public function updatePartner($id, $data)
    {
        $this->db->where('bk_id', $id);
        $this->db->update('partners', $data);
    }

    public function hardDelete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('partners');
    }

    public function generateSlug($bk_id = 0)
    {
        return 'PRT-' . substr(md5($bk_id . time()), 0, 8);
    }

    public function slug_exists($slug = '')
    {
        $query = $this->db->get_where('partners', array('slug' => $slug,));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

}
