<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Producetransfer_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function newProduceTransfer($data)
    {
        $this->db->insert('produce_transfers', $data);
        return $this->db->insert_id();
    }

    public function getProduceTransfer($id = 0)
    {
        $query = $this->db->get_where('produce_transfers', array('pt_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getProduceTransferBySlug($slug = '')
    {
        $query = $this->db->get_where('produce_transfers', array('pt_slug' => $slug));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getProduceTransfers($limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
        $this->db->order_by('pt_date_created','DESC');
		$query = $this->db->get('produce_transfers');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalProduceTransfers()
    {
        $query = $this->db->get('produce_transfers');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }

    public function getProduceTransfersWithName($keyword="", $limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(pt_slug)', strtolower($keyword));
        $this->db->order_by('pt_date_created','DESC');
        $query = $this->db->order_by('pt_date_created', 'DESC')->get_where('produce_transfers', array());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalProduceTransfersWithName($keyword="")
    {
        $this->db->like('LOWER(pt_slug)', strtolower($keyword));
        $query = $this->db->order_by('pt_date_created', 'DESC')->get_where('produce_transfers', array());
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function get_enum_values($table, $field)
    {
        $type = $this->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }

    public function updateProduceTransfer($id, $data)
    {
        $this->db->where('pt_id', $id);
        $this->db->update('produce_transfers', $data);
    }

    public function hardDelete($id)
    {
        $this->db->where('pt_id', $id);
        $this->db->delete('produce_transfers');
    }

    public function generateSlug($pt_id = 0)
    {
        return 'PTF-' . substr(md5($pt_id . time()), 0, 8);
    }

    public function slug_exists($slug = '')
    {
        $query = $this->db->get_where('produce_transfers', array('pt_slug' => $slug));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

}
