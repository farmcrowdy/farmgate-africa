<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Companysizes_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

	function getCompanySize($id = 0)
	{
		$query = $this->db->get_where('company_sizes', array('id' => $id));
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	function getCompanySizeName($id = 0)
	{
		$query = $this->db->get_where('company_sizes', array('id' => $id));
		if ($query->num_rows() > 0) {
			return $query->row()->name;
		} else {
			return false;
		}
	}

	function getAllCompanySizes()
	{
		$query = $this->db->get_where('company_sizes');
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}


	function updateCompanySizes($id, $data)
	{

		$this->db->where('id', $id);
		$this->db->update('company_sizes', $data);
	}

	function hardDelete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('company_sizes');
	}



}