<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


class Locals_model extends CI_Model {

	function __construct()
	{
		parent::__construct('locals');
	}

	function newLocal($data)
	{
		$this->db->insert('locals', $data);
	    return $this->db->insert_id();

	}

	function getLocal($id=0)
	{
		$query = $this->db->get_where('locals', array('local_id' => $id));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return FALSE;
		}
	}

	function getLocalName($id=0)
	{
		$query = $this->db->get_where('locals', array('local_id' => $id));
		if($query->num_rows() > 0){
			return $query->row()->local_name;
		}else{
			return FALSE;
		}
	}

	function getLocalsByState($state_id)
	{

		$query = $this->db->get_where('locals', array('state_id' => $state_id));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return FALSE;
		}
	}

	function updateLocal($id, $data)
	{

		$this->db->where('local_id', $id);
		$this->db->update('locals', $data);
	}

	function hardDelete($id)
	{
		$this->db->where('local_id', $id);
		$this->db->delete('locals');
	}


}
