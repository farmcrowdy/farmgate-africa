<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Companytypes_model extends CI_Model
{

	function __construct()
	{
		parent::__construct('company_types');
	}

	function getCompanyType($id = 0)
	{
		$query = $this->db->get_where('company_types', array('ct_id' => $id));
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	function getCompanyTypeName($id = 0)
	{
		$query = $this->db->get_where('company_types', array('ct_id' => $id));
		if ($query->num_rows() > 0) {
			return $query->row()->name;
		} else {
			return false;
		}
	}

	function getAllCompanyTypes()
	{
		$query = $this->db->get_where('company_types');
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}


	function updateCompanyTypes($id, $data)
	{

		$this->db->where('id', $id);
		$this->db->update('company_types', $data);
	}

	function hardDelete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('company_types');
	}



}
