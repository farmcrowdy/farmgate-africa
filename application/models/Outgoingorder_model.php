<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Outgoingorder_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function newOutgoingOrder($data)
    {
        $this->db->insert('outgoing_orders', $data);
        return $this->db->insert_id();
    }

    public function getOutgoingOrder($id = 0)
    {
        $query = $this->db->get_where('outgoing_orders', array('oo_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getOutgoingOrderBySlug($slug = '')
    {
        $query = $this->db->get_where('outgoing_orders', array('oo_slug' => $slug));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getOutgoingOrders($limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
        $this->db->order_by('oo_date_created','DESC');
		$query = $this->db->get('outgoing_orders');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalOutgoingOrders()
    {
        $query = $this->db->get('outgoing_orders');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }

    public function getOutgoingOrdersWithName($keyword="", $limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(oo_slug)', strtolower($keyword));
        $this->db->order_by('oo_date_created','DESC');
        $query = $this->db->order_by('oo_date_created', 'DESC')->get_where('outgoing_orders', array());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalOutgoingOrdersWithName($keyword="")
    {
        $this->db->like('LOWER(oo_slug)', strtolower($keyword));
        $query = $this->db->order_by('oo_date_created', 'DESC')->get_where('outgoing_orders', array());
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function get_enum_values($table, $field)
    {
        $type = $this->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }

    public function updateOutgoingOrder($id, $data)
    {
        $this->db->where('oo_id', $id);
        $this->db->update('outgoing_orders', $data);
    }

    public function hardDelete($id)
    {
        $this->db->where('oo_id', $id);
        $this->db->delete('outgoing_orders');
    }

    public function generateSlug($oo_id = 0)
    {
        return 'OOD-' . substr(md5($oo_id . time()), 0, 8);
    }

    public function slug_exists($slug = '')
    {
        $query = $this->db->get_where('outgoing_orders', array('oo_slug' => $slug));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

}
