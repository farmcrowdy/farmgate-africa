<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Messages_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function newMessage($data)
    {
        $this->db->insert('messages', $data);
        return $this->db->insert_id();
    }

    public function getMessage($id = 0)
    {
        $query = $this->db->get_where('messages', array('m_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getMessages($limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }

        $query = $this->db->order_by('m_date_sent', 'DESC')->get('messages');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getUserMessages($user_id, $limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
        $this->db->where('m_sender_id', $user_id);
        $query = $this->db->order_by('m_date_sent', 'DESC')->get('messages');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getMessagesOnOrder( $order_id, $limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }

        $this->db->where('m_order_id', $order_id);

        $query = $this->db->order_by('m_date_sent', 'DESC')->get('messages');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getPreviousMessagesOnOrder( $order_id, $last_id, $limit)
    {

        $this->db->limit($limit);
        $this->db->where('m_order_id', $order_id);
        $this->db->where('m_id <', $last_id);

        $query = $this->db->order_by('m_id', 'DESC')->get('messages');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getUserMessagesOnOrder( $user_id, $order_id, $limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }

        $this->db->where('m_order_id', $order_id);
        $this->db->where('m_sender_id', $user_id);

        $query = $this->db->order_by('m_date_sent', 'DESC')->get('messages');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getLastMessageByOrder( $order_id)
    {

        $this->db->limit(1);
        $this->db->where('m_order_id', $order_id);
        $query = $this->db->order_by('m_id', 'DESC')->get('messages');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
 
    public function getMostRecentMessagesByOrder( $order_id, $limit)
    {

        $this->db->limit($limit);
        $this->db->where('m_order_id', $order_id);
        $query = $this->db->order_by('m_id', 'DESC')->get('messages');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getThrashedMessages($limit, $offset)
    {
        if ($offset == 0) {
            $query = $this->db->order_by('date_created', 'DESC')->get_where('messages', array('active' => 'deleted'), $limit);
        } else {
            $query = $this->db->order_by('date_created', 'DESC')->get_where('messages', array('active' => 'deleted'), $limit, $offset);
        }
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function updateMessage($id, $data)
    {

        $this->db->where('id', $id);
        $this->db->update('messages', $data);
    }

    public function hardDelete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('messages');
    }

}
