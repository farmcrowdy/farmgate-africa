<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function newUser($uData)
    {
        $this->db->insert('fe_users', $uData);
        return $this->db->insert_id();

    }

    public function getUser($uid = 0)
    {
        $query = $this->db->get_where('fe_users', array('id' => $uid));

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }

    }

    public function getInvestorsWithName($keyword = '')
    {
        $this->db->like('LOWER(full_name)', strtolower($keyword));
        $this->db->select('full_name, phone, id');
        $query = $this->db->order_by('date_created', 'DESC')->get_where('fe_users', array('type'=>'user'));
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getUsers($limit, $offset)
    {

        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $query = $this->db->order_by('date_created', 'DESC')->get_where('fe_users', array('active' => 'yes'));
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getUsersByType($type, $limit, $offset)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $query = $this->db->order_by('date_created', 'DESC')->get_where('fe_users', array('active' => 'yes','type'=> $type));

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalUsersByType($type)
    {
        $query = $this->db->order_by('date_created', 'DESC')->get_where('fe_users', array('active' => 'yes','type'=> $type));
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getUsersByName($keyword, $limit, $offset)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(full_name)', strtolower($keyword));

        $query = $this->db->order_by('date_created', 'DESC')->get_where('fe_users', array('active' => 'yes'));

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalUsersByName($keyword)
    {
        $this->db->like('LOWER(full_name)', strtolower($keyword));
        $query = $this->db->order_by('date_created', 'DESC')->get_where('fe_users', array('active' => 'yes'));
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getUsersByTypeAndName($type, $keyword, $limit, $offset)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(full_name)', strtolower($keyword));
        $query = $this->db->order_by('date_created', 'DESC')->get_where('fe_users', array('active' => 'yes', 'type'=>$type));

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getTotalUsersByTypeAndName($type, $keyword)
    {
        $this->db->like('LOWER(full_name)', strtolower($keyword));
        $query = $this->db->order_by('date_created', 'DESC')->get_where('fe_users', array('active' => 'yes','type'=> $type));
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function getBannedUsers($limit, $offset)
    {
        if ($offset == 0) {
            $query = $this->db->order_by('date_created', 'DESC')->get_where('fe_users', array('active' => 'deleted'), $limit);
        } else {
            $query = $this->db->order_by('date_created', 'DESC')->get_where('fe_users', array('active' => 'deleted'), $limit, $offset);
        }
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getUserBySlug($slug = "")
    {
        $query = $this->db->get_where('fe_users', array('slug' => $slug));

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }

    }

    public function getUserByEmail($email = '')
    {
        $query = $this->db->get_where('fe_users', array('email' => $email));

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }

    }

    public function getUserByPasswordResetLink($pass_reset_link = '')
    {
        $query = $this->db->get_where('fe_users', array('reset_password_link' => $pass_reset_link));

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }

    }

    public function getUserIDByEmail($email = '')
    {
        $query = $this->db->get_where('fe_users', array('email' => $email));

        if ($query->num_rows() > 0) {
            return $query->row()->id;
        } else {
            return false;
        }
    }

    public function getTotalUsers()
    {
        $this->db->where('active', 'yes');
        $this->db->from("fe_users");
        return $this->db->count_all_results();
    }

    public function updateUser($uid = 0, $data = array())
    {
        $this->db->where('id', $uid);
        $this->db->update('fe_users', $data);
    }

    public function updateUserByEmail($email = "", $uData = array())
    {
        $this->db->where('email', $email);
        $this->db->update('fe_users', $uData);
    }

    public function hardDelete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('fe_users');
    }

    public function email_exists($email = '')
    {
        $query = $this->db->get_where('fe_users', array('email' => $email));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function slug_exists($slug = '')
    {
        $query = $this->db->get_where('fe_users', array('slug' => $slug));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function activationHashExists($activationHash = '')
    {
        $query = $this->db->get_where('fe_users', array('email_activation_hash' => $activationHash));

        if ($query->num_rows() > 0) {
            return $query->row()->email;
        } else {
            return false;
        }

    }

    public function passwordResetHashExists($passwordResetHash = '')
    {
        $query = $this->db->get_where('fe_users', array('password_reset_hash' => $passwordResetHash));

        if ($query->num_rows() > 0) {
            return $query->row()->email;
        } else {
            return false;
        }

    }

    public function validLogin($email = '', $password = '')
    {
        $query = $this->db->get_where('fe_users', array('email' => $email, 'password' => $password));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function check_email_exists($email)
    {
        $query = $this->db->get_where('fe_users', array('email' => $email));
        if (empty($query->row_array())) {
            return true;
        } else {
            return false;
        }
    }

    public function generateUserSlug($full_name, $email)
    {

        $product_short = strtolower(url_title(substr($full_name, 0, 2), 'dash', true));
        $rand_string = $email . mt_rand(10, 5688484);
        $unique_10 = substr(md5($rand_string), 0, 8);
        $slug = trim($product_short . $unique_10);

        return $slug;

    }

    public function get_enum_values($table, $field)
    {
        $type = $this->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }

}
