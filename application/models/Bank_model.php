<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Bank_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function newBank($data)
    {
        $this->db->insert('banks', $data);
        return $this->db->insert_id();
    }

    public function getBank($id = 0)
    {
        $query = $this->db->get_where('banks', array('bk_id' => $id, 'bk_status'=>'active'));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getBankBySlug($slug = '')
    {
        $query = $this->db->get_where('banks', array('bk_slug' => $slug, ));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getBanks($limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
		$query = $this->db->get('banks');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getAllBanks()
    {
		$query = $this->db->get('banks');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalBanks()
    {
        $query = $this->db->get('banks');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }

    public function getBanksWithName($keyword="", $limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(bk_name)', strtolower($keyword));
        $query = $this->db->order_by('bk_date_created', 'DESC')->get_where('banks', array());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalBanksWithName($keyword="")
    {
        $this->db->like('LOWER(bk_name)', strtolower($keyword));
        $query = $this->db->order_by('bk_date_created', 'DESC')->get_where('banks', array());
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function getAllActiveBanksWithName($keyword="")
    {
        $this->db->where('bk_status', 'active');
        $this->db->like('LOWER(bk_name)', strtolower($keyword));
		$query = $this->db->get('banks');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalAllActiveBanksWithName($keyword="")
    {
        $this->db->where('bk_status', 'active');
        $this->db->like('LOWER(bk_name)', strtolower($keyword));
		$query = $this->db->get('banks');

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function get_enum_values($table, $field)
    {
        $type = $this->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }

    public function updateBank($id, $data)
    {
        $this->db->where('bk_id', $id);
        $this->db->update('banks', $data);
    }

    public function hardDelete($id)
    {
        $this->db->where('bk_id', $id);
        $this->db->delete('banks');
    }

    public function generateSlug($bk_id = 0)
    {
        return 'BNK-' . substr(md5($bk_id . time()), 0, 8);
    }

    public function slug_exists($slug = '')
    {
        $query = $this->db->get_where('banks', array('bk_slug' => $slug,));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

}
