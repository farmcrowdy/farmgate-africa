<?php
class Icommodity_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function newICommodity($data)
    {
        $this->db->insert('investment_commodities', $data);
        return $this->db->insert_id();
    }

    public function getICommodity($id = 0)
    {
        $query = $this->db->get_where('investment_commodities', array('invc_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getNewICommodity($id = 0)
    {
        $query = $this->db->get_where('investment_commodities', array('invc_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getICommodityBySlug($slug = "")
    {
        $query = $this->db->get_where('investment_commodities', array('invc_slug' => $slug));

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getICommodities($limit, $offset, $sortField, $sortOrder)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        return $this->sortBy($sortField, $sortOrder);

    }

    public function getAdminICommodities($limit, $offset)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        // $this->db->where('invc_status', 'available');
        $query = $this->db->order_by('invc_date_created', 'DESC')->get_where('investment_commodities', array());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getAdminICommoditiesWithName($keyword='', $limit, $offset)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(invc_name)', strtolower($keyword));
        $query = $this->db->order_by('invc_date_created', 'DESC')->get_where('investment_commodities', array());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalAdminICommoditiesWithName($keyword='')
    {
        $this->db->like('LOWER(invc_name)', strtolower($keyword));
        $query = $this->db->order_by('invc_date_created', 'DESC')->get_where('investment_commodities', array());
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function getTotalICommodities()
    {
        $query = $this->db->order_by('invc_date_created', 'DESC')->get_where('investment_commodities', array());

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function getTotalAdminICommodities()
    {
        // $this->db->where('invc_status', 'available');
        $query = $this->db->order_by('invc_date_created', 'DESC')->get_where('investment_commodities', array());

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function getICommoditiesByCategory($cat_id = 0, $limit, $offset)
    {

        $this->db->where('invc_category_id', $cat_id);
        if ($offset == 0) {
            $query = $this->db->order_by('invc_date_created', 'DESC')->get_where('investment_commodities', array('invc_status' => 'available'), $limit);
        } else {
            $query = $this->db->order_by('invc_date_created', 'DESC')->get_where('investment_commodities', array('invc_status' => 'available'), $limit, $offset);
        }
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getICommoditiesByUser($user_id = 0, $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->where('invc_user_id', $user_id);
        $this->db->where('invc_status !=', 'deleted');
        $this->db->order_by('invc_id', 'DESC');
        $query = $this->db->get('investment_commodities');

        // var_dump($this->db->last_query());
        // die();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalICommoditiesByUser($user_id = 0)
    {
        $this->db->where('invc_user_id', $user_id);
        $this->db->where('invc_status !=', 'deleted');
        $query = $this->db->get('investment_commodities');
        // var_dump($this->db->last_query());
        // die();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }

    public function getICommoditiesByUserWithName($user_id = 0, $keyword = '', $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->where('invc_user_id', $user_id);
        $this->db->like('LOWER(invc_name)', strtolower($keyword));
        $this->db->where('invc_status !=', 'deleted');
        $this->db->order_by('invc_id', 'DESC');

        $query = $this->db->get('investment_commodities');

        // var_dump($this->db->last_query());
        // die();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalICommoditiesByUserWithName($user_id = 0, $keyword = '')
    {

        $this->db->where('invc_user_id', $user_id);
        $this->db->like('LOWER(invc_name)', strtolower($keyword));
        $this->db->where('invc_status !=', 'deleted');
        
        $query = $this->db->get('investment_commodities');

        // var_dump($this->db->last_query());
        // die();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function isICommodityFromUser($user_id = 0, $product_slug = '')
    {
        $this->db->where('invc_user_id', $user_id);
        $this->db->where('invc_slug', $product_slug);
        $query = $this->db->order_by('invc_date_created', 'DESC')->get_where('investment_commodities', array());
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function sortBy($field,$order='ASC')
    {
        if(empty($field)){
            $query = $this->db->get('investment_commodities');
        }else{
            $query = $this->db->order_by($field, $order)->get('investment_commodities');
        }
        // var_dump($this->db->last_query());
        // var_dump($query->result());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getICommoditiesByName($name = '', $limit = 0, $offset = 0, $sortField, $sortOrder)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(invc_name)', strtolower($name));
        return $this->sortBy($sortField, $sortOrder);

    }

    public function getTotalICommoditiesByName($name = '')
    {
        $this->db->like('LOWER(invc_name)', strtolower($name));
        $query = $this->db->get('investment_commodities');

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getICommoditiesByStatus($status = array(), $limit = 0, $offset = 0, $sortField, $sortOrder)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }

        if(count($status) == 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->where('invc_status', $stat);
                }  
            }
        }elseif(count($status) > 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->or_where('invc_status', $stat);
                }  
            }
        }
        return $this->sortBy($sortField, $sortOrder);

    }

    public function getTotalICommoditiesByStatus($status = array())
    {
        if(count($status) == 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->where('invc_status', $stat);
                }  
            }
        }elseif(count($status) > 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->or_where('invc_status', $stat);
                }  
            }
        }

        $query = $this->db->get('investment_commodities');

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getICommoditiesByNameAndStatus($name = '', $status = array(), $limit = 0, $offset = 0, $sortField, $sortOrder)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(invc_name)', strtolower($name));

        if(count($status) == 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->where('invc_status', $stat);
                }  
            }
        }elseif(count($status) > 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->or_where('invc_status', $stat);
                }  
            }
        }

        return $this->sortBy($sortField, $sortOrder);

    }

    public function getTotalICommoditiesByNameAndStatus($name = '', $status = array())
    {
        $this->db->like('LOWER(invc_name)', strtolower($name));

        if(count($status) == 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->where('invc_status', $stat);
                }  
            }
        }elseif(count($status) > 1){
            foreach($status as $stat){
                if(!empty($stat)){
                    $this->db->or_where('invc_status', $stat);
                }  
            }
        }
        $query = $this->db->get('investment_commodities');

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getICommoditiesByNameAndVolume($name = '', $min_volume, $max_volume, $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(invc_name)', strtolower($name));
        $this->db->where('CAST(invc_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(invc_min_order AS SIGNED ) >', $min_volume);

            $query = $this->db->order_by('invc_date_created', 'DESC')->get_where('investment_commodities', array('invc_status' => 'available'));
        // var_dump($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalICommoditiesByNameAndVolume($name = '', $min_volume, $max_volume)
    {
        $this->db->like('LOWER(invc_name)', strtolower($name));
        $this->db->where('CAST(invc_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(invc_min_order AS SIGNED ) >', $min_volume);

        $query = $this->db->order_by('invc_date_created', 'DESC')->get_where('investment_commodities', array('invc_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getICommoditiesByStatesAndVolume($state, $min_volume, $max_volume, $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->where_in('invc_states_id', $state);
        $this->db->where('CAST(invc_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(invc_min_order AS SIGNED ) >', $min_volume);
            $query = $this->db->order_by('invc_date_created', 'DESC')->get_where('investment_commodities', array('invc_status' => 'available'), $limit);

        // var_dump($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalICommoditiesByStatesAndVolume($state, $min_volume, $max_volume)
    {
        $this->db->where_in('invc_states_id', $state);
        $this->db->where('CAST(invc_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(invc_min_order AS SIGNED ) >', $min_volume);

        $query = $this->db->order_by('invc_date_created', 'DESC')->get_where('investment_commodities', array('invc_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getICommoditiesByVolume($min_volume = '', $max_volume = '', $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->where('CAST(invc_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(invc_min_order AS SIGNED ) >', $min_volume);

            $query = $this->db->order_by('invc_date_created', 'DESC')->get_where('investment_commodities', array('invc_status' => 'available'));

        // var_dump($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalICommoditiesByVolume($min_volume = '', $max_volume = '')
    {
        $this->db->where('CAST(invc_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(invc_min_order AS SIGNED ) >', $min_volume);

        $query = $this->db->order_by('invc_date_created', 'DESC')->get_where('investment_commodities', array('invc_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getICommoditiesByNameAndStatesAndVolume($name, $state, $min_volume = '', $max_volume = '', $limit = 0, $offset = 0)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(invc_name)', strtolower($name));
        $this->db->where_in('invc_states_id', $state);
        $this->db->where('CAST(invc_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(invc_min_order AS SIGNED ) >', $min_volume);
            $query = $this->db->order_by('invc_date_created', 'DESC')->get_where('investment_commodities', array('invc_status' => 'available'), $limit);

        // var_dump($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalICommoditiesByNameAndStatesAndVolume($name, $state, $min_volume = '', $max_volume = '')
    {
        $this->db->like('LOWER(invc_name)', strtolower($name));
        $this->db->where_in('invc_states_id', $state);
        $this->db->where('CAST(invc_min_order AS SIGNED ) <=', $max_volume);
        $this->db->where('CAST(invc_min_order AS SIGNED ) >', $min_volume);

        $query = $this->db->order_by('invc_date_created', 'DESC')->get_where('investment_commodities', array('invc_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getRecentlyAddedICommodities($limit, $offset)
    {
        if ($offset == 0) {
            $this->db->limit($limit);
        } else {
            $this->db->limit($offset, $limit);
        }

        $query = $this->db->order_by('invc_date_created', 'DESC')->join('states', 'states.state_id = investment_commodities.invc_states_id')->get_where('investment_commodities', array('investment_commodities.invc_status' => 'available'));

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getThrashedICommodities($limit, $offset)
    {
        if ($offset == 0) {
            $query = $this->db->order_by('invc_date_created', 'DESC')->get_where('investment_commodities', array('invc_status' => 'deleted'), $limit);
        } else {
            $query = $this->db->order_by('invc_date_created', 'DESC')->get_where('investment_commodities', array('invc_status' => 'deleted'), $limit, $offset);
        }
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function generateICommoditySlug($product_name, $user_email)
    {

        // $slug = trim(strtolower( url_title($product_name,'dash', TRUE). '-' .md5($user_email) ));
        $product_short = strtolower(url_title(substr($product_name, 0, 2), 'dash', true));
        $rand_string = $user_email . mt_rand(10, 5688484);
        $unique_10 = substr(md5($rand_string), 0, 8);
        $slug = trim($product_short . $unique_10);

        return $slug;

    }

    public function slug_exists($slug = '')
    {
        $query = $this->db->get_where('investment_commodities', array('invc_slug' => $slug));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function name_exists($name = '')
    {
        $query = $this->db->get_where('investment_commodities', array('invc_name' => $name));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function updateICommodity($id, $data)
    {
        $this->db->where('invc_id', $id);
        $this->db->update('investment_commodities', $data);
    }

    public function hardDelete($id)
    {
        $this->db->where('invc_id', $id);
        $this->db->delete('investment_commodities');
    }

    public function get_enum_values($table, $field)
    {
        $type = $this->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }
}
