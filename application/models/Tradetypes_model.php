<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


class Tradetypes_model extends CI_Model {

	public function __construct()
    {
        $this->load->database();
    }

	function getTradeType($id=0)
	{
		$query = $this->db->get_where('trade_types', array('id' => $id));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return FALSE;
		}
	}

	function getTradeTypeName($id=0)
	{
		$query = $this->db->get_where('trade_types', array('id' => $id));
		if($query->num_rows() > 0){
			return $query->row()->name;
		}else{
			return FALSE;
		}
	}

	function getAllTradeTypes()
	{
		$query = $this->db->get_where('trade_types');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return FALSE;
		}
	}


	function updateTradeType($id, $data)
	{

		$this->db->where('id', $id);
		$this->db->update('trade_types', $data);
	}

	function hardDelete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('trade_types');
	}



}

