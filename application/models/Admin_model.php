<?php

class Admin_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function register($enc_password) {
        //user data array
        $data = array(
            'u_fname' => $this->input->post('fname'),
            'u_sname' => $this->input->post('sname'),
            'u_email' => $this->input->post('email'),
            'u_gender' => $this->input->post('gender'),

            'u_pass' => $enc_password
        );
        return $this->db->insert('users', $data);
    }

    public function check_email_exists($email) {
        $query = $this->db->get_where('users', array('u_email' => $email));
        if (empty($query->row_array())) {
            return true;
        } else {
            return false;
        }
    }

    public function login($email) {

        $this->db->where('a_email', $email);

        $result = $this->db->get('admin');
        if ($result->num_rows() == 1) {
            return $result->row(0);
        } else {
            return false;
        }
    }

    public function get_admin_profile() {

        $aid = $this->session->userdata('aid');
    
        $query = $this->db->get_where('admin', array('a_id' => $aid));
        return $query->row_array();
    }

    public function update_profile($post_image) {

        $userid = $this->session->userdata('user_id');
        $data = array(
            'firstname' => ucwords($this->input->post('fname')),
            'surname' => ucwords($this->input->post('sname')),
            'mobile' => $this->input->post('mobile'),
            'address' => $this->input->post('address'),
            'about_me' => $this->input->post('aboutme'),
            'gender' => $this->input->post('gender'),
            'dob' => $this->input->post('dob'),
            'profile_image' => $post_image
        );
        $this->db->where('user_id', $userid);
        return $this->db->update('users', $data);
    }



}
