<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Clusters_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function newCluster($data)
    {
        $this->db->insert('clusters', $data);
        return $this->db->insert_id();
    }

    public function getCluster($id = 0)
    {
        $query = $this->db->get_where('clusters', array('cl_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getClusterBySlug($slug = '')
    {
        $query = $this->db->get_where('clusters', array('cl_slug' => $slug));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getClusters($limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }

		$query = $this->db->get('clusters');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalClusters()
    {
		$query = $this->db->get('clusters');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }

    public function getClustersWithName($keyword="", $limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(cl_name)', strtolower($keyword));
        $query = $this->db->order_by('cl_date_created', 'DESC')->get_where('clusters', array());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalClustersWithName($keyword="")
    {
        $this->db->like('LOWER(cl_name)', strtolower($keyword));
        $query = $this->db->order_by('cl_date_created', 'DESC')->get_where('clusters', array());
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function get_enum_values($table, $field)
    {
        $type = $this->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }

    public function updateCluster($id, $data)
    {
        $this->db->where('cl_id', $id);
        $this->db->update('clusters', $data);
    }

    public function order_exists($orderNumber = '')
    {
        $query = $this->db->get_where('clusters', array('or_order_number' => $orderNumber));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function hardDelete($id)
    {
        $this->db->where('cl_id', $id);
        $this->db->delete('clusters');
    }

    public function generateClusterSlug($cl_id = 0)
    {
        return 'CLS-' . substr(md5($cl_id . time()), 0, 8);
    }

    public function slug_exists($slug = '')
    {
        $query = $this->db->get_where('clusters', array('cl_slug' => $slug));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

}
