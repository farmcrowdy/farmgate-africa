<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Orders_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function newOrder($data)
    {
        $this->db->insert('orders', $data);
        $order_id = $this->db->insert_id();

        // Generate order number and update this order
        $updateData = array(
            'or_order_number' => $this->generateOrderNumber($order_id),
            'or_date_updated' => date('Y-m-d H:i:s'),
        );

        $this->updateOrder($order_id, $updateData);

        return $this->getOrder($order_id);
    }

    public function getOrder($id = 0)
    {
        $query = $this->db->get_where('orders', array('or_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getOrderByNumber($order_number = "")
    {
        $this->db->order_by('order_details.ord_date_created', 'DESC');
		$this->db->join('products', 'products.prod_id = order_details.ord_product_id', 'left');
		$this->db->join('orders', 'orders.or_id = order_details.ord_order_id');
        $this->db->where('orders.or_order_number', $order_number);

        $query = $this->db->get('order_details');

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getTotalOrdersByUser($user_id = 0)
    {

        $this->db->order_by('order_details.ord_date_created', 'DESC');
		$this->db->join('products', 'products.prod_id = order_details.ord_product_id', 'left');
		$this->db->join('orders', 'orders.or_id = order_details.ord_order_id');
        $this->db->where('orders.or_user_id', $user_id);
        $query = $this->db->get('order_details');
		// var_dump($this->db->last_query());
        // die();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function getOrdersByUser($user_id, $limit, $offset)
    {

        $this->db->order_by('order_details.ord_date_created', 'DESC');
		$this->db->join('products', 'products.prod_id = order_details.ord_product_id', 'left');
		$this->db->join('orders', 'orders.or_id = order_details.ord_order_id');
        $this->db->where('orders.or_user_id', $user_id);
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
		$query = $this->db->get('order_details');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalOrdersByUserWithNumber($user_id = 0, $order_num='')
    {

        $this->db->order_by('order_details.ord_date_created', 'DESC');
		$this->db->join('products', 'products.prod_id = order_details.ord_product_id', 'left');
		$this->db->join('orders', 'orders.or_id = order_details.ord_order_id');
        $this->db->where('orders.or_user_id', $user_id);
        $this->db->like('orders.or_order_number', $order_num);
        $query = $this->db->get('order_details');
		// var_dump($this->db->last_query());
        // die();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function getOrdersByUserWithNumber($user_id, $order_num='', $limit, $offset)
    {

        $this->db->order_by('order_details.ord_date_created', 'DESC');
		$this->db->join('products', 'products.prod_id = order_details.ord_product_id', 'left');
		$this->db->join('orders', 'orders.or_id = order_details.ord_order_id');
        $this->db->where('orders.or_user_id', $user_id);
        $this->db->like('orders.or_order_number', $order_num);
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
		$query = $this->db->get('order_details');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalOrdersWithNumber($order_num='')
    {

        $this->db->order_by('order_details.ord_date_created', 'DESC');
		$this->db->join('products', 'products.prod_id = order_details.ord_product_id', 'left');
		$this->db->join('orders', 'orders.or_id = order_details.ord_order_id');
        $this->db->like('orders.or_order_number', $order_num);
        $query = $this->db->get('order_details');
		// var_dump($this->db->last_query());
        // die();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function getOrdersWithNumber($order_num='', $limit, $offset)
    {

        $this->db->order_by('order_details.ord_date_created', 'DESC');
		$this->db->join('products', 'products.prod_id = order_details.ord_product_id', 'left');
		$this->db->join('orders', 'orders.or_id = order_details.ord_order_id');
        $this->db->like('orders.or_order_number', $order_num);
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
		$query = $this->db->get('order_details');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getOrderWithNumber($order_num='')
    {

        $this->db->order_by('order_details.ord_date_created', 'DESC');
		$this->db->join('products', 'products.prod_id = order_details.ord_product_id', 'left');
		$this->db->join('orders', 'orders.or_id = order_details.ord_order_id');
        $this->db->like('orders.or_order_number', $order_num);

		$query = $this->db->get('order_details');

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }

    }
    
    
    public function getTotalOrders()
    {

        $this->db->order_by('order_details.ord_date_created', 'DESC');
		$this->db->join('products', 'products.prod_id = order_details.ord_product_id', 'left');
		$this->db->join('orders', 'orders.or_id = order_details.ord_order_id');
        $this->db->where('orders.or_status <>', 'cancelled');
		$query = $this->db->get('order_details');

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getOrders($limit, $offset)
    {

        $this->db->join('products', 'products.prod_id = order_details.ord_product_id', 'left');
		$this->db->join('orders', 'orders.or_id = order_details.ord_order_id');
        $this->db->order_by('order_details.ord_date_created', 'DESC');
        $this->db->where('orders.or_status <>', 'cancelled');
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
		$query = $this->db->get('order_details');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getPendingOrders($limit, $offset)
    {

        $this->db->join('products', 'products.prod_id = order_details.ord_product_id', 'left');
		$this->db->join('orders', 'orders.or_id = order_details.ord_order_id');
        $this->db->order_by('order_details.ord_date_created', 'DESC');
        $this->db->where('orders.or_status', 'pending');
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
		$query = $this->db->get('order_details');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getAllPendingOrders()
    {

        $this->db->join('products', 'products.prod_id = order_details.ord_product_id', 'left');
		$this->db->join('orders', 'orders.or_id = order_details.ord_order_id');
        $this->db->order_by('order_details.ord_date_created', 'DESC');
        $this->db->where('orders.or_status', 'completed');
        $this->db->where('order_details.ord_quantity_delivered < order_details.ord_quantity ');
        $query = $this->db->get('order_details');
        // var_dump($this->db->last_query());
        // die();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getLastOrdersByUser($user_id)
    {

        $this->db->order_by('order_details.ord_date_created', 'DESC');
		$this->db->join('products', 'products.prod_id = order_details.ord_product_id', 'left');
		$this->db->join('orders', 'orders.or_id = order_details.ord_order_id');
        $this->db->where('orders.or_user_id', $user_id);

        $this->db->limit(1);

		$query = $this->db->get('order_details');

        if ($query->num_rows() > 0) {
            return $query->row()->or_id;
        } else {
            return false;
        }

    }

    public function ajax_getOrders()
    {
        $query = $this->db->select('or_id, or_order_number, or_user_id, or_date_created, or_status')->order_by('or_date_created', 'DESC')->get_where('orders', array('or_status !=' => 'cancelled'));
        if ($query->num_rows() > 0) {
            $response = array();
            $results = $query->result();
            foreach ($results as $order) {
                $user_full_name = (!empty($this->USERS->getUser($order->user_id)->full_name)) ? $this->USERS->getUser($order->user_id)->full_name : "Guest User!";
                $order_array = array(
                    $order->id,
                    $order->order_number,
                    $user_full_name,
                    $order->date_created,
                    $order->status,
                    '<a target="_blank" href="' . base_url('admin/orders/') . $order->order_number . '/edit' . '"><button class="btn btn-warning">Edit</button>' . '</a>&nbsp;&nbsp;<a target="_blank" href="' . base_url('admin/orders/') . $order->order_number . '/delete' . '"><button class="btn btn-danger">Delete</button></a>',
                );
                array_push($response, $order_array);
            }
            return $response;
        } else {
            return false;
        }

    }

    public function getThrashedOrders($limit, $offset)
    {
        if ($offset == 0) {
            $query = $this->db->order_by('or_date_created', 'DESC')->get_where('orders', array('or_status' => 'deleted'), $limit);
        } else {
            $query = $this->db->order_by('or_date_created', 'DESC')->get_where('orders', array('or_status' => 'deleted'), $limit, $offset);
        }
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function get_enum_values($table, $field)
    {
        $type = $this->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }

    public function updateOrder($id, $data)
    {

        $this->db->where('or_id', $id);
        $this->db->update('orders', $data);
    }

    public function order_exists($orderNumber = '')
    {
        $query = $this->db->get_where('orders', array('or_order_number' => $orderNumber));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function hardDelete($id)
    {
        $this->db->where('or_id', $id);
        $this->db->delete('orders');
    }

    public function generateOrderNumber($order_id = 0)
    {
        return 'FGX-' . substr(md5($order_id . time()), 0, 8);
    }

    public function object_to_array($data)
    {
        if (is_array($data) || is_object($data)) {
            $result = array();

            foreach ($data as $key => $value) {
                $result[$key] = $this->object_to_array($value);
            }

            return $result;
        }

        return $data;
    }

}
