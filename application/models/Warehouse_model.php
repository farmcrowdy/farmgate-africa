<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Warehouse_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function newWarehouse($data)
    {
        $this->db->insert('warehouses', $data);
        return $this->db->insert_id();
    }

    public function getWarehouse($id = 0)
    {
        $query = $this->db->get_where('warehouses', array('wh_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getAllWarehouses()
    {
        $query = $this->db->get_where('warehouses', array());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getWarehouseBySlug($slug = '')
    {
        $query = $this->db->get_where('warehouses', array('wh_slug' => $slug));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getManagersWithName($keyword="")
    {
        $this->db->like('LOWER(full_name)', strtolower($keyword));
        $query = $this->db->order_by('date_created', 'DESC')->get_where('fe_users', array('type'=>'warehouse_manager'));
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getWarehouses($limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }

		$query = $this->db->get('warehouses');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalWarehouses()
    {
		$query = $this->db->get('warehouses');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }

    public function getWarehousesWithName($keyword="", $limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(wh_name)', strtolower($keyword));
        $query = $this->db->order_by('wh_date_created', 'DESC')->get_where('warehouses', array());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalWarehousesWithName($keyword="")
    {
        $this->db->like('LOWER(wh_name)', strtolower($keyword));
        $query = $this->db->order_by('wh_date_created', 'DESC')->get_where('warehouses', array());
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function get_enum_values($table, $field)
    {
        $type = $this->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }

    public function updateWarehouse($id, $data)
    {
        $this->db->where('wh_id', $id);
        $this->db->update('warehouses', $data);
    }

    public function hardDelete($id)
    {
        $this->db->where('wh_id', $id);
        $this->db->delete('warehouses');
    }

    public function generateSlug($wh_id = 0)
    {
        return 'WHS-' . substr(md5($wh_id . time()), 0, 8);
    }

    public function slug_exists($slug = '')
    {
        $query = $this->db->get_where('warehouses', array('wh_slug' => $slug));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

}
