<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Purchase_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function newPurchase($data)
    {
        $this->db->insert('purchases', $data);
        return $this->db->insert_id();
    }

    public function getPurchase($id = 0)
    {
        $query = $this->db->get_where('purchases', array('po_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getPurchaseBySlug($slug = '')
    {
        $query = $this->db->get_where('purchases', array('po_slug' => $slug));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getPurchases($limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
        $this->db->order_by('po_date_created','DESC');
		$query = $this->db->get('purchases');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalPurchases()
    {
        $query = $this->db->get('purchases');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }

    public function getAllActivePurchasesWithName($keyword="")
    {
        $this->db->where('po_status', 'active');
        $this->db->order_by('po_date_created','DESC');
        $this->db->like('LOWER(po_full_name)', strtolower($keyword));
		$query = $this->db->get('purchases');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalAllActivePurchasesWithName($keyword="")
    {
        $this->db->where('po_status', 'active');
        $this->db->like('LOWER(po_full_name)', strtolower($keyword));
		$query = $this->db->get('purchases');

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }

    }

    public function getPurchasesWithName($keyword="")
    {
        $this->db->like('LOWER(po_slug)', strtolower($keyword));
        $this->db->order_by('po_date_created','DESC');
        $query = $this->db->order_by('po_date_created', 'DESC')->get_where('purchases', array());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalPurchasesWithName($keyword="")
    {
        $this->db->like('LOWER(po_slug)', strtolower($keyword));
        $query = $this->db->order_by('po_date_created', 'DESC')->get_where('purchases', array());
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function get_enum_values($table, $field)
    {
        $type = $this->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }

    public function updatePurchase($id, $data)
    {
        $this->db->where('po_id', $id);
        $this->db->update('purchases', $data);
    }

    public function hardDelete($id)
    {
        $this->db->where('po_id', $id);
        $this->db->delete('purchases');
    }

    public function generateSlug($po_id = 0)
    {
        return 'PUO-' . substr(md5($po_id . time()), 0, 8);
    }

    public function slug_exists($slug = '')
    {
        $query = $this->db->get_where('purchases', array('po_slug' => $slug));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

}
