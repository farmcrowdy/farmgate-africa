<?php
    class Currency_model extends CI_Model{
       public function __construct(){
           $this->load->database();
		}
		
	
	public function newCurrency($data)
    {
        $this->db->insert('currencies', $data);
        return $this->db->insert_id();
    }

	function getCurrency($id=0)
	{
		$query = $this->db->get_where('currencies', array('cur_id' => $id));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return FALSE;
		}
	}

	function getCurrencyName($id=0)
	{
		$query = $this->db->get_where('currencies', array('cur_id' => $id));
		if($query->num_rows() > 0){
			return $query->row()->cur_name;
		}else{
			return FALSE;
		}
	}

	function getAllCurrencies()
	{
		$query = $this->db->get_where('currencies');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return FALSE;
		}
	}

	function updateCurrency($id, $data)
	{

		$this->db->where('cur_id', $id);
		$this->db->update('currencies', $data);
	}

	function hardDelete($id)
	{
		$this->db->where('cur_id', $id);
		$this->db->delete('currencies');
	}

	public function currencyExists($cur_name = '')
    {
        $query = $this->db->get_where('currencies', array('cur_name' => $cur_name));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }



}
