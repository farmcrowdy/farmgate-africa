<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Collectioncenter_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function newCollectionCenter($data)
    {
        $this->db->insert('collection_centers', $data);
        return $this->db->insert_id();
    }

    public function getCollectionCenter($id = 0)
    {
        $query = $this->db->get_where('collection_centers', array('cc_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getAllCollectionCenters()
    {
        $query = $this->db->get_where('collection_centers', array());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getTotalAllCollectionCenters()
    {
        $query = $this->db->get_where('collection_centers', array());
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getCollectionCenterBySlug($slug = '')
    {
        $query = $this->db->get_where('collection_centers', array('cc_slug' => $slug));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getManagersWithName($keyword="")
    {
        $this->db->like('LOWER(full_name)', strtolower($keyword));
        $query = $this->db->order_by('date_created', 'DESC')->get_where('fe_users', array('type'=>'collection_center_manager'));
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getCollectionCenters($limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }

		$query = $this->db->get('collection_centers');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalCollectionCenters()
    {
		$query = $this->db->get('collection_centers');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }

    public function getCollectionCentersWithName($keyword="", $limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
        $this->db->like('LOWER(cc_name)', strtolower($keyword));
        $query = $this->db->order_by('cc_date_created', 'DESC')->get_where('collection_centers', array());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalCollectionCentersWithName($keyword="")
    {
        $this->db->like('LOWER(cc_name)', strtolower($keyword));
        $query = $this->db->order_by('cc_date_created', 'DESC')->get_where('collection_centers', array());
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function get_enum_values($table, $field)
    {
        $type = $this->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }

    public function updateCollectionCenter($id, $data)
    {
        $this->db->where('cc_id', $id);
        $this->db->update('collection_centers', $data);
    }

    public function hardDelete($id)
    {
        $this->db->where('cc_id', $id);
        $this->db->delete('collection_centers');
    }

    public function generateSlug($cc_id = 0)
    {
        return 'CLC-' . substr(md5($cc_id . time()), 0, 8);
    }

    public function slug_exists($slug = '')
    {
        $query = $this->db->get_where('collection_centers', array('cc_slug' => $slug));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

}
