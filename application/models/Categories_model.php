<?php
    class Categories_model extends CI_Model{
       public function __construct(){
           $this->load->database();
		}
		
	
	public function newCategory($data)
    {
        $this->db->insert('categories', $data);
        return $this->db->insert_id();
    }

	function getCategory($id=0)
	{
		$query = $this->db->get_where('categories', array('c_id' => $id));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return FALSE;
		}
	}

	function getCategoryName($id=0)
	{
		$query = $this->db->get_where('categories', array('c_id' => $id));
		if($query->num_rows() > 0){
			return $query->row()->c_name;
		}else{
			return FALSE;
		}
	}

	function getAllCategories()
	{
		$query = $this->db->get_where('categories');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return FALSE;
		}
	}

	function getAllSubCategoriesByProduceType($produce_type_id=0)
	{
		$query = $this->db->get_where('categories', array('c_type_id' => $produce_type_id, 'c_parent_id <>' => 0));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return FALSE;
		}
	}

	function getParentCategories()
	{
		$this->db->where('c_parent_id', 0 );
		$this->db->order_by('c_name', 'asc' );
		$query = $this->db->get('categories');
		// var_dump($this->db->last_query());
		// die();
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return FALSE;
		}
	}

	function getSubCategories($parent_id)
	{
		$this->db->where('c_parent_id', $parent_id);
		$query = $this->db->get('categories');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return FALSE;
		}
	}


	function getCategoryBySlug($slug="")
	{
		$query = $this->db->get_where('categories', array('c_slug' => $slug));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return FALSE;
		}
	}

	function updateCategory($id, $data)
	{

		$this->db->where('c_id', $id);
		$this->db->update('categories', $data);
	}

	function hardDelete($id)
	{
		$this->db->where('c_id', $id);
		$this->db->delete('categories');
	}

	public function categoryExists($category = '')
    {
        $query = $this->db->get_where('categories', array('c_name' => $category));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }



}
