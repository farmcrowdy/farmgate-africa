<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Investment_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function newInvestment($data)
    {
        $this->db->insert('investments', $data);
        return $this->db->insert_id();
    }

    public function getInvestment($id = 0)
    {
        $query = $this->db->get_where('investments', array('iv_id' => $id));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getInvestmentBySlug($slug = '')
    {
        $query = $this->db->get_where('investments', array('iv_slug' => $slug));
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getInvestments($limit, $offset)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
        $this->db->order_by('iv_date_created','DESC');
		$query = $this->db->get('investments');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalInvestments()
    {
        $query = $this->db->get('investments');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }


    public function getInvestmentsByUser($user_id = 0, $limit, $offset, $sortField, $sortOrder)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
        $this->db->where('iv_user_id', $user_id);
        return $this->sortBy($sortField, $sortOrder);
    }

    public function getTotalInvestmentsByUser($user_id = 0)
    {
        $this->db->where('iv_user_id', $user_id);
        $query = $this->db->get('investments');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }

    public function getInvestmentsByUserWithName($user_id = 0, $keyword = '', $limit, $offset, $sortField, $sortOrder)
    {
        if($offset==0){
            $this->db->limit($limit);
        }else{
            $this->db->limit($offset, $limit);
        }
        $this->db->where('iv_user_id', $user_id);
        $this->db->like('LOWER(iv_commodity_name)', strtolower($keyword));
        return $this->sortBy($sortField, $sortOrder);
    }

    public function getTotalInvestmentsByUserWithName($user_id = 0, $keyword = '')
    {
        $this->db->where('iv_user_id', $user_id);
        $this->db->like('LOWER(iv_commodity_name)', strtolower($keyword));
        $query = $this->db->get('investments');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }

    public function sortBy($field,$order='ASC')
    {
        if(empty($field)){
            $query = $this->db->get('investments');
        }else{
            $query = $this->db->order_by($field, $order)->get('investments');
        }
        // var_dump($this->db->last_query());
        // var_dump($query->result());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getInvestmentsWithName($keyword="")
    {
        $this->db->like('LOWER(iv_slug)', strtolower($keyword));
        $this->db->order_by('iv_date_created','DESC');
        $query = $this->db->order_by('iv_date_created', 'DESC')->get_where('investments', array());
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }

    }

    public function getTotalInvestmentsWithName($keyword="")
    {
        $this->db->like('LOWER(iv_slug)', strtolower($keyword));
        $query = $this->db->order_by('iv_date_created', 'DESC')->get_where('investments', array());
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }

    public function get_enum_values($table, $field)
    {
        $type = $this->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }

    public function updateInvestment($id, $data)
    {
        $this->db->where('iv_id', $id);
        $this->db->update('investments', $data);
    }

    public function hardDelete($id)
    {
        $this->db->where('iv_id', $id);
        $this->db->delete('investments');
    }

    public function generateSlug($iv_id = 0)
    {
        return 'INV-' . substr(md5($iv_id . time()), 0, 8);
    }

    public function slug_exists($slug = '')
    {
        $query = $this->db->get_where('investments', array('iv_slug' => $slug));

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

}
