<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


class Units_model extends CI_Model {

	function __construct()
	{
		parent::__construct('units');
	}

	function getUnit($id=0)
	{
		$query = $this->db->get_where('units', array('ut_id' => $id));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return FALSE;
		}
	}

	function getUnitName($id=0)
	{
		$query = $this->db->get_where('units', array('ut_id' => $id));
		if($query->num_rows() > 0){
			return $query->row()->ut_name;
		}else{
			return FALSE;
		}
	}

	function getAllUnits()
	{
		$query = $this->db->get_where('units');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return FALSE;
		}
	}

	function updateUnit($id, $data)
	{

		$this->db->where('ut_id', $id);
		$this->db->update('units', $data);
	}

	function hardDelete($id)
	{
		$this->db->where('ut_id', $id);
		$this->db->delete('units');
	}



}
