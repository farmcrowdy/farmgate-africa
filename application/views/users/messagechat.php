<ul id="message_board" class="pl-0 message-list chat-scrollbar1" style="list-style-type: none; overflow-y: scroll; height: 500px;">

</ul>
<div class="row">
	<div class="col-12 send-chat" style="background:#FAFAFA;">
		<div class="row">
			<form id="chat_form" class="row col-12" method="post" action="<?php echo base_url('users/postmessage'); ?>" enctype="multipart/form-data">
				<input name="order_id" id="m_order_id" type="hidden" value="<?php echo $order->or_id; ?>">
				<div class="col-10 offset-1 d-flex justify-content-center chat-textarea my-3">
					<label for="upload-picture-for-admin" class="btn h-100 upload-file-chat">
						<i class="fas fa-camera"></i>
					</label>
					<input id="upload-picture-for-admin" name="messageFile" accept="" type="file" class="hidden">
					<textarea id="message_text" name="message_text" class="form-control" aria-label="With textarea"></textarea>
					<button id="chatBtnz" type="submit">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate" viewBox="0 0 30 25"
						width="30" height="25">
							<defs>
								<clipPath id="_clipPath_gyJeUhUPZK0mDzwRUCvjny9R8qzzkACb">
									<rect width="30" height="25" />
								</clipPath>
							</defs>
							<g clip-path="url(#_clipPath_gyJeUhUPZK0mDzwRUCvjny9R8qzzkACb)">
								<clipPath id="_clipPath_OZWTecr3mGY6LYKw8M9cJwe2zbXNAzqu">
									<rect x="0.968" y="-1" width="29.032" height="25" transform="matrix(1,0,0,1,0,0)" fill="rgb(255,255,255)" />
								</clipPath>
								<g clip-path="url(#_clipPath_OZWTecr3mGY6LYKw8M9cJwe2zbXNAzqu)">
									<g id="Group">
										<path d=" M 3.172 1.206 L 3.172 11.049 L 24.411 12.603 L 3.172 14.158 L 3.172 24 L 30 12.603 L 3.172 1.206 Z " fill="rgb(216,216,216)"
										vector-effect="non-scaling-stroke" stroke-width="0.786" stroke="rgb(112,112,112)" stroke-opacity="100" stroke-linejoin="miter"
										stroke-linecap="butt" stroke-miterlimit="4" />
									</g>
								</g>
							</g>
						</svg>
					</button>
					<script>
						$('#upload-picture-for-admin').change(function () {
							var files = $(this).get(0).files;
							if (files[0]) {
								var reader = new FileReader();

								reader.onload = function (e) {
									var previewContainer = $('#image-preview');
									previewContainer.attr('src', e.target.result).show();
								};

								reader.readAsDataURL(files[0]);
							}
						})

					</script>
				</div>
			</form>
		</div>
		<div>
			<img id="image-preview" width="50px" height="50px" style="display:none;"/>
		</div>
	</div>
</div>
