<!-- ============body================= -->
<div class="container-fluid d-none d-lg-block">
    <div class="row" style="background: #f0f0f0;">
        <div class="col-10 offset-1">
            <ul class="ordered-list ordered-list1 m-0 p-0 h-100 nav nav-tabs" id="myTab" role="tablist">
                <li class="item-verified nav-item d-flex d-lg-inline align-items-center  " >
                    <a id="account-tab" class="nav-link  <?php if(!empty($acc_info)) {echo 'active show';} ?>" data-toggle="tab" href="#account" role="tab" aria-controls="account" aria-selected="false">
                        <p class="mb-0  px-4 px-md-5"><span class="d-none d-lg-inline">Account Information</span></p>
                    </a>
                </li>
                 <li class="item-verified nav-item  d-flex d-lg-inline align-items-center" >
                     <a id="company-tab" class="nav-link <?php if(!empty($coy_info)) {echo 'active show';} ?>" data-toggle="tab" href="#company" role="tab" aria-controls="company" aria-selected="false">
                        <p class="mb-0 px-4 px-md-5"><span >Company <span class="d-block d-md-inline">Information</span></span></p>
                     </a>
                </li>
                 <li class="item-verified active nav-item d-flex d-lg-inline align-items-center">
                    <a id="verify-tab" class="nav-link <?php if(!empty($verify)) {echo 'active';} ?>" data-toggle="tab" href="#verify" role="tab" aria-controls="verify" aria-selected="<?php if(!empty($verify)) {echo 'true';} ?>">
                        <p class="mb-0   px-4 px-md-5"><span class="d-none d-lg-inline">Get Verified</span></p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div> 
<section class="container-fluid tab-content" id="myTabContent">
<!-- Account Tab Content -->
   <div class="row tab-pane fade <?php if(!empty($acc_info)) {echo 'active show';} ?>" id="account" role="tabpanel" aria-labelledby="account-tab">
       <div class="col-12" >
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <p class="border-bottom accnt-info-title pt-5 pb-3">Account information</p>
                    <?php echo $this->session->flashdata('acc_feedback');?>
                </div>
            </div>
            <?php echo form_open_multipart('users/editprofile'); ?>
            <input type="hidden" name="account_form" id="account_form" value="account_form"/>
                <div class="col-md-10 offset-md-1 border-bottom">
                    <div class="row pt-4 pb-2">
                        <div class="col-lg-12 upload-photo" id='image-preview'>
                                <?php if(!empty($acc_image_errors)){
                                    foreach ($acc_image_errors as $error) {
                                         # code...
                                        echo $error;
                                     } 
                                } 
                                ?>
                                <img src="<?php if(!empty($user->image)) {echo base_url().'mainasset/fe/images/users/'.$user->image;} else{ echo "https://via.placeholder.com/350x150";} ?>" alt="<?php echo $user->full_name; ?>" class="img-fluid picture rounded">
                                <div class="user-placeholder hidden">.</div>
                                <label class="btn btn-default new-line" for="upload-file-selector">Upload New Photo</label>
                                <input id="upload-file-selector" name="userImageFile" type="file" class="hidden" accept="image/jpeg,image/png" multiple>
                                
                            <script>
                                $('#upload-file-selector').change(function(){
                                    var files = $(this).get(0).files;
                                    if (files[0]) {
                                        var reader = new FileReader();
        
                                        reader.onload = function (e) {
                                            var previewContainer = $('#image-preview');
                                            previewContainer.find('img').attr('src', e.target.result).removeClass('hidden');
                                            previewContainer.find('.user-placeholder').addClass('hidden');
                                        };
        
                                        reader.readAsDataURL(files[0]);
                                    }
                                })
                            </script>
                        </div>        
                    </div>
                    <div class="row mt-5">
                        <div class="col-md-6">
                            <label for="">First &#38; Last Name <span class="superscript-star">*</span></label>
                            <?php echo form_error('pro_full_name'); ?>
                            <input type="text" class="form-control" id="pro_full_name" name="pro_full_name" aria-describedby="fullName" placeholder="Enter First and Last name" value="<?php echo set_value('pro_fullname', $user->full_name); ?>">
                                
                        </div>
                        <div class=" col-md-6 col-lg-5 offset-lg-1 form-group">
                            <label for="langSelect">Language</label>
                            <select class="form-control" id="languageSelect" name="languageSelect">
                                <option value="0">Select Language</option>|| <?php echo $user->language_id; ?>
                                <?php foreach ($languages as $language) :?>
                                    <option 
                                      value="<?php echo $language->lang_id; ?>"> <?php echo $language->lang_name; ?> 
                                    </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="row py-1">
                        <div class="col-md-6">
                            <label for="">Your Position <span class="superscript-star">*</span></label>
                            <?php echo form_error('pro_position'); ?>
                            <input type="text" class="form-control" id="pro_position" name="pro_position" aria-describedby="Position" placeholder="Enter your position" value="<?php echo set_value('pro_position', $user->position); ?>">
                                
                        </div>
                        <div class=" col-md-6 col-lg-5 offset-lg-1 form-group">
                            <label for="">LinkedIn profile (link)</label>
                            <?php echo form_error('pro_linkedIn'); ?>
                            <input type="text" class="form-control" name="pro_linkedIn" id="pro_linkedIn" aria-describedby="linkedin" placeholder="Enter link here" value="<?php echo set_value('pro_linkedIn', $user->linkedIn); ?>">
                        </div>
                    </div>
                    <div class="row py-1">
                        <div class="col-md-6">
                            <label for="">Corporate email  <span class="superscript-star">*</span></label>
                            <?php echo form_error('pro_corp_email'); ?>
                            <input type="email" class="form-control" name="pro_corp_email" id="pro_corp_email" aria-describedby="email" placeholder="Enter your email" value="<?php echo set_value('pro_corp_email', $user->corp_email); ?>">
                            <div class="row pt-4">
                                <div class=" col-md-7 col-lg-6 form-group" >
                                    <label for="countryPhoneSelect">Phone Number <span class="superscript-star">*</span></label>
                                    <select class="form-control" id="countryPhoneSelect">
                                        <option>
                                            <span class="pr-5">Nigeria</span>
                                            <span class="pl-3">(+234)</span>
                                        </option>
                                    </select>
                                </div>
                                <div class="col-md-5 col-lg-6 form-group " >
                                    <label for="" style="color:white;">Phone</label>
                                    <?php echo form_error('pro_phone'); ?>
                                    <input type="text" class="form-control" name="pro_phone" id="pro_phone" aria-describedby="Phone" placeholder="Phone number" value="<?php echo set_value('pro_phone', $user->phone); ?>">
                                </div>
        
                            </div>
                        </div>
                        <div class=" col-md-6 col-lg-5 offset-lg-1 form-group">
                            <label for="">Short description about yourself</label>
                            <?php echo form_error('pro_description'); ?>
                            <textarea class="form-control" rows="5" aria-label="With textarea" name="pro_description"><?php echo trim(set_value('pro_description', $user->description)); ?></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center pb-5 pt-4">
                            <input class="link-to-next-page" type="submit" name="account_btn" value="Save and Continue"/>
                        </div>
                    </div>
                </div>
            </form>
        
            <div class="row">
                <div class="col-10 offset-md-1 py-3 pb-5">
                    <span class="superscript-star ml-md-5"> *</span><span>required fields</span>
                </div>
            </div>
       </div>
   </div>
<!-- End of Account Tab Content -->
<!-- Company Tab Content -->
    <div class="row tab-pane fade <?php if(!empty($coy_info)) {echo 'active show';} ?>" id="company" role="tabpanel" aria-labelledby="company-tab">
        <div class="col-12 ">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <p class="border-bottom accnt-info-title pt-5 pb-3">Company information</p>
                    <?php echo $this->session->flashdata('coy_feedback');?>
                </div>
            </div>
            <?php echo form_open_multipart('users/editprofile'); ?>
            <input type="hidden" name="account_form" id="company_form" value="company_form"/>
                <div class="col-md-10 offset-md-1 border-bottom">
                    <div class="row pt-4 pb-2">
                        <div class="col-lg-12 upload-photo" id='coy-image-preview'>
                            <?php if(!empty($coy_image_errors)){
                                    foreach ($coy_image_errors as $error) {
                                         # code...
                                        echo $error;
                                     } 
                                } 
                                ?>
                                <img src="<?php if(!empty($user->coy_image)) {echo base_url().'mainasset/fe/images/companies/'.$user->coy_image;} else{ echo 'https://via.placeholder.com/350x150';} ?>" alt="<?php echo $user->coy_name; ?>" class="img-fluid picture rounded">
                                <div id="coy-user-placeholder">.</div>
                                <label class="btn btn-default" for="coy-upload-file-selector">Upload Company Logo</label>
                                <input id="coy-upload-file-selector" name="coyImageFile" accept="image/jpeg,image/png" type="file" class="hidden">
                                
                            <script>
                            $('#coy-upload-file-selector').on('change', function(){ //on file input change
                                    if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
                                    {
                                        $('#coy-user-placeholder').html(''); //clear html of output element
                                        var data = $(this)[0].files; //this file data
                                        
                                        $.each(data, function(index, file){ //loop though each file
                                            if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
                                                var fRead = new FileReader(); //new filereader
                                                fRead.onload = (function(file){ //trigger function on successful read
                                                return function(e) {
                                                    // var img = $('<img width="160px"/>').addClass('thumb').attr('src', e.target.result); //create image element 
                                                    // $('#coy-user-placeholder').append(img); //append image to output element
                                                    var previewContainer = $('#coy-image-preview');
                                                    previewContainer.find('img').attr('src', e.target.result).removeClass('hidden');
                                                    previewContainer.find('#coy-user-placeholder').addClass('hidden');
                                                };
                                                })(file);
                                                fRead.readAsDataURL(file); //URL representing the file's data.
                                            }
                                        });
                                        
                                    }else{
                                        alert("Your browser doesn't support File API!"); //if File API is absent
                                    }
                                });
                            </script>
                        </div>
        
                    </div>
                    <div class="row mt-5">
                        <div class="col-md-6">
                            <label for="coy_name">Company Name <span class="superscript-star">*</span></label>
                            <?php echo form_error('coy_name'); ?>
                            <input type="text" class="form-control" name="coy_name" id="coy_name" aria-describedby="fullName" placeholder="Enter Company Name" value="<?php echo set_value('coy_name', $user->coy_name); ?>"
                            >
                                
                        </div>
                        <div class=" col-md-6 col-lg-5 offset-lg-1 form-group">
                            <label for="formControlSelect">Language</label>
                            <select class="form-control" id="coylanguageSelect" name="coylanguageSelect">
                                <option value="0">Select Language</option>|| <?php echo $user->coy_language_id; ?>
                                <?php foreach ($languages as $language) :?>
                                    <option 
                                      value="<?php echo $language->lang_id; ?>"> <?php echo $language->lang_name; ?> 
                                    </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="row py-1">
                        <div class="col-md-6">
                            <label for="">Company size <span class="superscript-star">*</span></label>
                            <select class="form-control" id="coySizeSelect" name="coySizeSelect">
                                <option value="0">Select Company Size</option>|| <?php echo $user->coy_size_id; ?>
                                <?php foreach ($company_sizes as $company_size) :?>
                                    <option 
                                      value="<?php echo $company_size->com_id; ?>"> <?php echo trim($company_size->com_size); ?> 
                                    </option>
                                <?php endforeach ?>
                            </select>     
                        </div>
                        <div class=" col-md-6 col-lg-5 offset-lg-1 form-group">
                            <label for="">Incorporation date <span style="color:#999999;">(format: mm-dd-yyyy)</span></label>
                            <?php echo form_error('coy_inc_date'); ?>
                            <input type="date" class="form-control" name="coy_inc_date" id="" aria-describedby="Incorporation Date" placeholder="Enter Date" value="<?php echo set_value('coy_inc_date', date('Y-m-d',strtotime($user->coy_inc_date))); ?>">
                        </div>
                    </div>
                    <div class="row py-1">
                        <div class="col-md-6">
                            <label for="">Company Type  <span class="superscript-star">*</span></label>
                            <select class="form-control" id="coyTypeSelect" name="coyTypeSelect">
                                <option value="0">Select Company Type</option>|| <?php echo $user->coy_type_id; ?>
                                <?php foreach ($company_types as $company_type) :?>
                                    <option 
                                      value="<?php echo $company_type->ct_id; ?>"> <?php echo $company_type->ct_type; ?> 
                                    </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-6 col-lg-5 offset-lg-1 form-group">
                            <label for="coy_website">Website </label>
                            <?php echo form_error('coy_website'); ?>
                            <input type="text" class="form-control" name="coy_website" id="coy_website" aria-describedby="website" placeholder="Enter website's link" value="<?php echo set_value('coy_website', $user->coy_website); ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class=" col-md-6 form-group">
                            <label for="tradeTypeSelect">Trade Type <sup class="superscript-star">*</sup></label>
                            <select class="form-control" id="tradeTypeSelect" name="tradeTypeSelect">
                                <option value="0">Select Trade Type</option>|| <?php echo $user->trade_type_id; ?>
                                <?php foreach ($trade_types as $trade_type) :?>
                                    <option 
                                      value="<?php echo $trade_type->id; ?>"> <?php echo $trade_type->type; ?> 
                                    </option>
                                <?php endforeach ?>
                            </select>
                            <div class="row py-1">
                                <div class="col-md-12">
                                    <label for="">Company Registration Number <span class="superscript-star">*</span></label>
                                    <?php echo form_error('coy_reg_num'); ?>
                                    <input type="text" class="form-control" name="coy_reg_num" id="coy_reg_num" aria-describedby="regNumber" placeholder="Enter company's registration number"
                                    value="<?php echo set_value('coy_reg_num', $user->coy_reg_num); ?>">
                                        
                                </div>
                            </div>
                        </div>
                        <div class=" col-md-6 col-lg-5 offset-lg-1 form-group">
                            <label for="">Company Description</label>
                            <?php echo form_error('coy_description'); ?>
                            <textarea class="form-control" rows="5" name="coy_description" id="coy_description" aria-label="With textarea"><?php echo set_value('coy_description', $user->coy_description); ?></textarea>
                        </div>
                    </div>
                        
                    <div class="row py-1 pb-5">
                        <div class="col-md-6">
                            <label for="address">Legal address <span class="superscript-star">*</span></label>
                            <?php echo form_error('coy_legal_add'); ?>
                            <input type="text" class="form-control" name="coy_legal_add" id="coy_legal_add" aria-describedby="address" placeholder="Enter address" value="<?php echo set_value('coy_legal_add', $user->coy_legal_add); ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-center pb-5 pt-4">
                        <input class="link-to-next-page" type="submit" name="company_btn" value="Save and Continue"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
<!-- End of Company Tab Content -->
<!-- Verify Tab Content -->
    <div class="row  tab-pane fade <?php if(!empty($verify)) {echo 'active show';} ?>" id="verify" role="tabpanel" aria-labelledby="verify-tab">
        <div class="tab-pane col-12 ">
            <div class="row">
                <div class="col-lg-10 offset-lg-1 verified-margin">
                    <p class="border-bottom accnt-info-title pt-5 pb-3">Get Verified</p>
                    <?php echo $this->session->flashdata('verify_feedback');?>
                </div>
            </div>
            <div class="row">
                <div class="verified-margin col-lg-10 offset-lg-1 ">
                    <div class="row">
                        <div class="col-lg-7 col-md-6">
                            <?php echo form_open_multipart('users/editprofile'); ?>
                            <input type="hidden" name="verify_form" id="verify_form" value="verify_form"/>
                                <div class="col-lg-11">
                                    <div class="verified-card card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-7 col-sm-6 col-6">
                                                    <p class="get-verified-p">Certificate of Incorporation <span style="font-family: 'Amino-Regular7',sans-serif; color:#888888;">(scan copy)</span></p>
                                                    <div id="verify-files-placeholder">

                                                        <?php if(!empty($user->coy_files)): ?>
                                                            <?php foreach ($user->coy_files as $coy_file): ?> 
                                                              <p><?php echo $coy_file->cf_file_path; ?></p>
                                                            <?php endforeach ?>
                                                        <?php else: ?>
                                                            No files
                                                        <?php endif ?>
                                                    </div>
                                                <?php if(!empty($verify_image_errors)){
                                                         foreach ($verify_image_errors as $error) {
                                                           echo $error;
                                                        } 
                                                     } 
                                                ?>
                                                    <input id="verify-upload-file-selector" name="verifyFiles" accept="image/jpeg,image/png" type="file" class="hidden" multiple>
                                                    <div class="row hide-textarea">
                                                        <div class=" col-12 form-group">
                                                            <label for="">Description for files</label>
                                                            <?php echo form_error('files_description'); ?>
                                                            <textarea name="files_description" class="form-control" rows="3" aria-label="With textarea"><?php echo set_value('coy_files_description', $user->coy_files_description); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-5 col-sm-6 col-6">
                                                        <label class="btn btn-default verify-link" for="verify-upload-file-selector"><i class="fas fa-upload"></i> Add Files</label>
                                                        <p class="verify-link verify-link-no1"><a href="#"> <i class="fas fa-edit"></i> Add description</a></p>
                                                </div>
                                            </div>
                                        </div>
                                <script>
                                    $('#verify-upload-file-selector').on('change', function(){ //on file input change
                                    if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
                                    {
                                        $('#verify-files-placeholder').html(''); //clear html of output element
                                        var data = $(this)[0].files; //this file data
                                        
                                        $.each(data, function(index, file){ //loop though each file
                                            if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
                                                var fRead = new FileReader(); //new filereader
                                                fRead.onload = (function(file){ //trigger function on successful read
                                                return function(e) {
                                                    var img = $('<img width="160px" class="img img-responsive" style="margin: 10px; background-color:#333;"/>').addClass('thumb').attr('src', e.target.result); //create image element 
                                                    $('#verify-files-placeholder').append(img); //append image to output element
                                                    // var previewContainer = $('#coy-image-preview');
                                                    // previewContainer.find('img').attr('src', e.target.result).removeClass('hidden');
                                                    // previewContainer.find('#coy-user-placeholder').addClass('hidden');
                                                };
                                                })(file);
                                                fRead.readAsDataURL(file); //URL representing the file's data.
                                            }
                                        });
                                        
                                    }else{
                                        alert("Your browser doesn't support File API!"); //if File API is absent
                                    }
                                });
                            </script>

                                    </div>
                
                                                
                                </div>
                                <div class="text-center py-5 col-11">
                                    <input class="link-to-next-page" type="submit" name="verify_btn" value="Send for Approval">
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-5 col-md-6">
                            <p>Transparency and trust between our customers is vital to us. We pay close attention to the customer experience and know how important it is to deal with a verified counterparty. With tier 1 verification we confirm the legal existence of the enterprise and ensure the company is a legitimate player in the agricultural commodities industry.</p>
                            <p>For tier 1 verification we require a scanned copy of Certificate of Incorporation.</p>
                            <p>Tier 1 Verification usually takes no longer than a few hours.</p>
                            <p>For more information please read our full article about the importance of our process <a href="#" style="color:#08bd51">(link)</a>.</p>
                        </div>
                    </div>
                        
                </div>
            </div>
        </div>
    </div>
<!-- End of Verify Tab Content -->
</section>

</div>
<script type="text/javascript">

    $(document).ready(function(){
        //add more description slide down
        $('.verify-link-no1').click(function(){
            $('.hide-textarea').slideDown(400);
            $('.verify-link-no1').hide();
        })
    
    $('.animated-icon3').click(function(){
        $(this).toggleClass('open');
    });
    $('#btn-menu').click(function(){
    $('#btn-menu').toggleClass("nav-button");
});
  
    })
</script>