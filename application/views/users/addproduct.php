
                    <!-- <div class="col-md-10" style="background: #fefefe;"> -->
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-12 my-4 ">
                                <div class="row">
                                    <div class="col-md-8 pl-4 col-12">
                                        <p class="order-title ">Add Products</p>
                                    </div>
                                    <div class="col-5 col-sm-4 d-md-none">
                                        <div class="row">
                                            <div class="col-10 p-0 m-0 dashboard  text-center">
                                                <!-- <i class="fas fa-chalkboard-teacher d-block"></i> -->
                                                <span class="dashboard-close  m-0 text-left">Dashboard &nbsp;
                                                    <i class="fas fa-angle-right pl-1"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-10 col-md-12 col-11">
                            <?php echo $this->session->flashdata('prod_feedback');?>
                                    <?php if(!empty($image_errors)){
                                        foreach ($image_errors as $error) {
                                             # code...
                                            echo $error;
                                         } 
                                        } 
                                    ?>
                                    <?php echo form_open_multipart('users/addproduct',array('class'=>'row acct-form mb-4')); ?>
                                    <div class="col-md-12 pad-dashboard">
                                        <div class="row pt-4 pb-2">
                                            <div class="col-lg-12 upload-photo" id='image-preview'>

                                                <img src="#" alt="User photo" class="img-fluid hidden picture rounded">
                                                <div class="user-placeholder">.</div>
                                                <label class="btn btn-default new-line" for="upload-file-selector">Upload product image</label>
                                                <input id="upload-file-selector" name="productImageFile" accept="image/jpeg,image/png" type="file" class="hidden">

                                                <script>
                                                    $('#upload-file-selector').change(function () {
                                                        var files = $(this).get(0).files;
                                                        if (files[0]) {
                                                            var reader = new FileReader();

                                                            reader.onload = function (e) {
                                                                var previewContainer = $('#image-preview');
                                                                previewContainer.find('img').attr('src', e.target.result).removeClass('hidden');
                                                                previewContainer.find('.user-placeholder').addClass('hidden');
                                                            };

                                                            reader.readAsDataURL(files[0]);
                                                        }
                                                    })
                                                </script>
                                            </div>

                                        </div>
                                        <div class="row mt-5">
                                            <div class="col-md-6">
                                                <label for="">Product Name
                                                    <span class="superscript-star">*</span>
                                                </label>
                                                <?php echo form_error('name'); ?>
                                                <input type="text" name="name" class="form-control" id="" aria-describedby="productName" 
                                                value="<?php echo set_value('name'); ?>">

                                            </div>
                                        </div>
                                        <div class="row py-1">
                                            <div class=" col-md-6 col-9 form-group  h-100">
                                                <label for="formControlSelect">Product category
                                                    <span class="superscript-star">*</span>
                                                </label><?php echo form_error('categorySelect'); ?>
                                                <select class="form-control" id="categorySelect" name="categorySelect">
                                                    <?php foreach ($categories as $category) :?>
                                                        <option 
                                                          value="<?php echo $category->c_id; ?>"> <?php echo $category->c_name; ?> 
                                                        </option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-7 col-6  h-100">
                                                        <label for="">Quantity
                                                            <span class="superscript-star">*</span>
                                                        </label>
                                                        <?php echo form_error('quantity'); ?>
                                                        <input type="number" min="1" class="form-control" name="quantity" id="" 
                                                        aria-describedby="quantity" value="<?php echo set_value('quantity'); ?>" >
                                                    </div>
                                                    <div class="col-md-5 col-6 h-100">
                                                        <label for="unitSelect" style="color:#efefef">Unit</label>
                                                        <?php echo form_error('unit_id'); ?>
                                                        <select class="form-control" id="unitSelect" name="unitSelect">
                                                            <?php foreach ($units as $unit) :?>
                                                                <option 
                                                                value="<?php echo $unit->ut_id; ?>"> <?php echo $unit->ut_name; ?> 
                                                                </option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class=" col-md-6 col-9 form-group">
                                                <label for="">Price per Unit
                                                    <span class="superscript-star">*</span>
                                                </label>
                                                <?php echo form_error('price_per_unit'); ?>
                                                <input type="text" class="form-control" id="price_per_unit" 
                                                name="price_per_unit" value="<?php echo set_value('price_per_unit'); ?>">
                                            </div>
                                        </div>

                                        <div class="row relative">
                                            <div class="col-md-6 ">
                                                <div class="row">
                                                    <div class="col-md-7 ">
                                                        <label class="d-md-none" for="">Minimum Order Quantity
                                                            <span class="superscript-star">*</span>
                                                        </label>
                                                        <?php echo form_error('min_order'); ?>
                                                        <label class="d-none d-md-block" for="">M.O.Q
                                                            <span class="superscript-star">*</span>
                                                        </label>
                                                        <input type="number" min="1" class="form-control" name="min_order" id="min_order" 
                                                        aria-describedby="minimum order" value="<?php echo set_value('min_order'); ?>">
                                                    </div>
                                                    <div class=" col-md-5 h-100 form-group">
                                                        <label for="">State
                                                            <span class="superscript-star">*</span>
                                                        </label>
                                                        <?php echo form_error('stateSelect'); ?>
                                                        <select class="form-control" id="stateSelect" name="stateSelect">
                                                            <?php foreach ($states as $state) :?>
                                                                <option 
                                                                  value="<?php echo $state->state_id; ?>"> <?php echo $state->name; ?>
                                                                </option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class=" col-md-4 h-100 form-group">
                                                <label for="localSelect">Local Government 
                                                    <span class="superscript-star">*</span>
                                                </label>
                                                <?php echo form_error('localSelect'); ?>
                                                <select class="form-control" id="localSelect" name="localSelect">
                                                </select>
                                            </div>
                                            <div class="col-12 d-none d-md-block absolute">
                                                <p class="mb-0 ml-3" style="color: #aaa; font-size:0.8em;">
                                                    <span>M.O.Q</span> - Minimum Order Quantity</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-6 col-9">
                                                        <label for="">Available from
                                                            <span class="superscript-star">*</span>
                                                        </label>
                                                        <?php echo form_error('avail_from'); ?>
                                                        <small><b><?php echo set_value('avail_from'); ?></b></small>
                                                        <input type="date" class="form-control" id="" aria-describedby="availableFrom" name="avail_from">
                                                    </div>
                                                    <div class="col-md-6 col-9">
                                                        <label for="formControlSelect">To</label>
                                                        <?php echo form_error('avail_to'); ?>
                                                        <small><b><?php echo set_value('avail_to'); ?></b></small>
                                                        <input type="date" class="form-control" id="" aria-describedby="availableTo" name="avail_to">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 form-group">
                                                <label for="">Description
                                                    <span class="superscript-star">*</span>
                                                </label>
                                                <?php echo form_error('description'); ?>
                                                <textarea name="description" class="form-control" rows="5" aria-label="With textarea"><?php echo trim(set_value('description')); ?></textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12  pb-5 pt-4">
                                            <button name="submit" type="submit" class="btn  btn-submit"> Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
