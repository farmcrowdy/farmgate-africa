<div class="col-md-10">
    <div class="row no-pad">
        <div class="col-12 my-md-4 mb-4 ">
            <div class="row no-pad ">
                <div class="col-md-8 col-12 pad-dashboard">
                    <p class="order-title">Profile</p>
                </div>
                <div class="col-5 col-sm-4 d-md-none">
                    <div class="row no-pad">
                        <div class="col-10 p-0 m-0 dashboard  text-center">
                            <!-- <i class="fas fa-chalkboard-teacher d-block"></i> -->
                            <span class="dashboard-close  m-0 text-left">Dashboard &nbsp; <i class="fas fa-angle-right pl-1"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row no-pad">
                <div class="col-md-12  ">
                    <div class="row no-pad pad-dashboard pad-dashboard-profile">
                        <div class="col-12" style="background: #fafafa;">
                            <div class="row no-pad py-4 ">
                                <div class="col-lg-4 col-md-5  profile-border mb-4 mb-lg-1">
                                    <div class="row no-pad">
                                        <div class="col-12 d-flex justify-content-center">
                                            <img src="<?php if(!empty($user->image)){ echo base_url('mainasset/fe/images/users/').$user->image;}else{ echo base_url('mainasset/fe/images/profile1.png');} ?>"
                                                class="img-fluid user-profile-pic" alt="profile picture">
                                        </div>
                                    </div>
                                    <div class="row no-pad">
                                        <div class="col-12 text-center">
                                            <p class="profile-name pt-2">
                                                <?php echo $user->full_name; ?>
                                            </p>
                                        </div>
                                    </div>

                                    <div class="row no-pad">
                                        <div class="col-12 text-center profile-edit mt-2">
                                            <span><a href="<?php echo base_url('users/editprofile'); ?>"> Edit Profile</a></span>
                                            <!-- Change Password trigger modal -->
                                            <span><a href="#" data-toggle="modal" data-target="#changePasswordModal">
                                                    Change Password</a></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-7">
                                    <div class="col-12">
                                        <nav>
                                            <div class="nav nav-tabs profile-tab-nav" id="nav-tab" role="tablist">
                                                <a class="nav-item nav-link active no-pad-link" id="nav-account-info-tab" data-toggle="tab" href="#nav-account-info" role="tab"
                                                    aria-controls="nav-account-info" aria-selected="true"><span
                                                        class="">Account</span>
                                                    Information</a>
                                                <a class="nav-item nav-link no-pad-link" id="nav-bank-details-tab" data-toggle="tab" href="#nav-bank-details" role="tab"
                                                    aria-controls="nav-bank-details" aria-selected="false"><span
                                                        class="">Bank</span> Details</a>
                                            </div>
                                        </nav>
                                        <div class="tab-content pt-4" id="nav-tabContent">
                                            <div class="tab-pane fade show active" id="nav-account-info" role="tabpanel" aria-labelledby="nav-account-info-tab">
                                                <p class="profile-view-title pb-0 mb-0">Email address
                                                </p>
                                                <p class="profile-view-body">
                                                    <?php echo $user->email; ?>
                                                </p>
                                                <p class="profile-view-title pb-0 mb-0">Phone</p>
                                                <p class="profile-view-body">
                                                    <?php echo $user->phone; ?>
                                                </p>
                                                <!-- <p class="profile-view-title pb-0 mb-0">Address</p> -->
                                                <!-- <p class="profile-view-body"></p> -->
                                            </div>

                                            <div class="tab-pane fade" id="nav-bank-details" role="tabpanel" aria-labelledby="nav-bank-details-tab">
                                                <p class="profile-view-title pb-0 mb-0">Account Name
                                                </p>
                                                <p class="profile-view-body">
                                                    <?php echo $user->acc_name; ?>
                                                </p>
                                                <p class="profile-view-title pb-0 mb-0">Bank Name</p>
                                                <p class="profile-view-body">
                                                    <?php echo $user->bank_name; ?>
                                                </p>
                                                <p class="profile-view-title pb-0 mb-0">Account Number
                                                </p>
                                                <p class="profile-view-body">
                                                    <?php echo $user->acc_number; ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
</div>
</div>
</div>