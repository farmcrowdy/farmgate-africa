<div class="container-fluid">
	<div class="row footer">
		<div class="col-lg-10 offset-lg-1 mt-4">
			<div class="row">
				<div class="col-md-6">
					<p>Subscribe to receive news first</p>
					<form class="row">
						<div class="form-group col-lg-9 col-sm-8 email-input">
							<input type="email" class="form-control" id="email" placeholder="Email">
						</div>
						<div class="col-lg-3 col-sm-4 submit-email">
							<button type="submit" class="btn ">SUBSCRIBE</button>
						</div>
					</form>
				</div>
				<div class="col-md-3 col-6 mt-3">
					<ul class="footer-list">
						<li>
							<a href="#">Home</a>
						</li>
						<li>
							<a href="#">About</a>
						</li>
						<li>
							<a href="#">How it works</a>
						</li>
						<li>
							<a href="#">Compliance</a>
						</li>
					</ul>
				</div>
				<div class="col-md-3 col-6  mt-3">
					<ul class="footer-list">
						<li>
							<a href="#">Terms of Service</a>
						</li>
						<li>
							<a href="#">Privacy Policy</a>
						</li>
						<li>
							<a href="#">Support</a>
						</li>
						<li>
							<a href="#">FAQ</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="row" style="background:#293737;">
		<div class="col-lg-10 offset-lg-1 my-2">
			<p class="copyrite mb-0"> &copy; <?php echo date('Y'); ?> Farmcrowdy - All rights reserved</p>
		</div>
	</div>
</div>
<!-- ============end of body================= -->


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
crossorigin="anonymous"></script>
<script src="<?php echo base_url();?>mainasset/fe/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>mainasset/fe/js/lity.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
<script src="<?php echo base_url();?>mainasset/fe/js/custom.js"></script>
<script type="text/javascript">
	$(document).ready(function () {


		$('.animated-icon3').click(function () {
			$(this).toggleClass('open');
		});
		$('#btn-menu').click(function () {
			$('#btn-menu').toggleClass("nav-button");
		});

	})

</script>
<!-- Contact manager modal -->
<div class="modal fade contact-manager-modal mt-5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg  ">
		<form class="modal-content" id="contactManagerForm" enctype="multipart/form-data">
			<div class="modal-header">
				<h5 class="modal-title pl-md-5 ml-md-3" id="changePasswordLabel">Contact Manager</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body container mt-3">
				<center style="color: #08BD51;">
					<div id="cm_feedback"></div>
				</center>
				<div class="row py-2">
					<div class="col-12 col-md-10 offset-md-1">
						<label for="">Subject</label>
						<input type="text" name="subject" class="form-control" id="subject" aria-describedby="Volume" placeholder="">
					</div>
				</div>
				<div class="row py-2">
					<div class="col-12 col-md-10 offset-md-1">
						<label for="">Category</label>
						<select name="categorySelect" class="form-control" id="categorySelect">
							<option value="Client">Client</option>
							<option value="General">General</option>
							<option value="Issues">Issues</option>
							<option value="Suggestions">Suggestions</option>
						</select>
					</div>
				</div>
				<div class="row py-2">
					<div class="col-12 col-md-10 offset-md-1">
						<label for="">Describe the question in detail</label>
						<textarea name="message" id="message" class="form-control" rows="5" aria-label="With textarea"></textarea>

					</div>
				</div>
				<div class="row py-2">
					<div class="col-12 col-md-10 offset-md-1">
						<p id="profile-file-paceholder" class="profile-file-paceholder mb-0 pb-0 pl-3 dashboard-activity-body"></p>
						<label for="upload-file-for-manager" class="btn btn-default manager-verify-link" style="color:#888888">
							<i class="fas fa-upload"></i> Add file</label>
						<input id="upload-file-for-manager" name="contactImageFile" accept="image/jpeg,image/png" type="file" class="hidden">
					</div>
					<script>
						// $(document).ready(function(){
						$("#upload-file-for-manager").change(function () {
							$("#profile-file-paceholder").text(this.files[0].name);
						});
						// });

					</script>
				</div>
			</div>
			<div class="modal-footer d-flex justify-content-center">
				<button type="button" class="btn btn-secondary px-4 py-2" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-profile py-2 px-4">Submit</button>
			</div>
		</form>
	</div>
</div>
<!-- End Contact Manager Modal -->