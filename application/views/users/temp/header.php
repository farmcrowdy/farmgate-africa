<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url();?>mainasset/fe/images/favicon.ico/ms-icon-144x144.png">
    <!-- <meta name="theme-color" content="#08BD51"> -->
    <meta name="theme-color" content="#66c157">
    <meta name="description" content="FarmGate Africa is a company focused on providing major processors and international buyers the opportunity to purchase commodities directly from farming clusters.">
    <meta name="Keywords" content="Agric Commodity purchase in Nigeria, Agric commodities, Agricultural Produce, Investing in Agriculture, Agriculture in Nigeria,  Digital Agriculture, High interest rate, Invest Money, FinTech, Online Agriculture platform, Digital Agriculture platform in Nigeria, Personal Finance, Investments, Wealth Management, fixed savings rate, fixed savings high interest, fixed deposit, fixed deposit account, money market, bonds, Agricultural Commodities Price index">
    <!--t -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@farmgateafrica">
    <meta name="twitter:creator" content="@farmgateafrica">
    <meta name="twitter:title" content="FarmGate.africa">
    <meta name="twitter:description" content="Innovating Market Access for Agriculture">
    <meta name="twitter:image" content="https://elleclen-cdn.sirv.com/Farmcrowdy/farmgate.jpg">
    <!--w -->
    <meta property="og:site_name" content="FarmGate.africa">
    <meta property="og:title" content="FarmGate Africa">
    <meta property="og:description" content="Innovating Market Access for Agriculture">
    <meta property="og:image" itemprop="image" content="https://elleclen-cdn.sirv.com/Farmcrowdy/farmgate.jpg">
    <meta property="og:type" content="website">
    <link rel="stylesheet" href="<?php echo base_url();?>mainasset/fe/css/fonts.css">
    <link rel="stylesheet" href="<?php echo base_url();?>mainasset/fe/css/index.css">
    <link rel="stylesheet" href="<?php echo base_url();?>mainasset/fe/css/main.min.css">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <!-- <script src="https://unpkg.com/scrollreveal"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
        crossorigin="anonymous">
            <!-- lity js -->
    <link rel="stylesheet" href="<?php echo base_url();?>mainasset/fe/css/lity.css">
    <script src="<?php echo base_url();?>mainasset/fe/js/lity.min.js"></script>
    <!-- Hotjar Tracking Code for www.farmgate.africa -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1098413,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-129587603-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-129587603-1');
    </script>
    <script type="text/javascript">
        // $(document).ready(function () {
        //     $('#label').click(function () {
        //         $('#navlist').toggleClass('d-hide d-show');
        //         console.log("all good")
        //     });

        // })
        // ScrollReveal({
        //     reset: true
        // });
    </script>
    <style>
        @media (max-width:991px){
            .d-hide{display:none}
            .d-show{display:block}
        }
    </style>
    <title><?php if(!empty($page_title)){ echo $page_title; }else{ echo "FarmGate Africa";} ?></title>
</head>


<body class="body">
<!-- ===============navbar=========== -->

<nav class="header nav-pad navbar nav-fixed after-login">
    <a class="navbar-brand" href="<?php echo base_url();?>">
        <img src="<?php echo base_url();?>mainasset/fe/images/farmgate-logo1.svg" alt="Farmgate Logo" class="img-fluid logo pl-sm-2 pl-md-5">
    </a>
    <input class="menu-btn" type="checkbox" id="menu-btn" />
    <label class="menu-icon" for="menu-btn" id="label">
        <span class="navicon"></span>
    </label>
    <div class="menu d-lg-flex d-hide" id="navlist">

        <ul class="ml-lg-auto mr-md-5 pad-left">
            <li class="d-lg-none profile-container-mobile">
                <div class="row profile-container">
                    <div class="profile-img mb-2">
                        <img src="<?php if(!empty($user->image)){ echo base_url('mainasset/fe/images/users/').$user->image;}else{ echo base_url('mainasset/fe/images/profile1.png');} ?>" alt="profile image">
                    </div>
                    <div class="pl-2">
                        <div class="profile-name"><?php echo $user->full_name; ?></div>
                        <div>
                            <a href="<?php echo base_url();?>logout">Logout</a>
                        </div>
                    </div>
                </div>
            </li>
            <li class="nav-item dropdown profileDropdown ">
                <!-- <a class="nav-link dropdown-toggle active" id="marketplaceDropdown" data-toggle="dropdown" data-hover="dropdown"
                    aria-haspopup="true" aria-expanded="false" href="#">
                    Marketplace
                </a>
                <div class="dropdown-menu py-2" aria-labelledby="marketplaceDropdown">

                    <a class="dropdown-item active" href="<?php // echo base_url('marketplace/?comm_type=trade'); ?>">Investment</a>
                    <a class="dropdown-item" href="<?php // echo base_url('marketplace/?comm_type=purchase'); ?>">Purchase</a>
                </div> -->
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url();?>how-it-works">
                    How it works
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" target="_blank" href="https://medium.com/@farmgate.africa/">
                    Blog
                </a>
            </li>
            <li class="nav-item help-pad">
                <a class="nav-link" href="<?php echo base_url();?>contact">
                    Contact
                </a>
            </li>
            <li class="nav-item help-pad d-lg-none">
                <a class="nav-link" href="<?php echo base_url('users/profile'); ?>">
                    Profile
                </a>
            </li>
            <li class="nav-item dropdown profileDropdown d-none d-lg-block">
                <a class="nav-link dropdown-toggle" id="profileDropdown" data-toggle="dropdown" data-hover="dropdown"
                    aria-haspopup="true" aria-expanded="false" href="<?php echo base_url('users/profile');?>">
                    Profile
                </a>
                <div class="dropdown-menu py-0" aria-labelledby="profileDropdown">
                    <div class="profile-img mb-2">
                        <img src="<?php if(!empty($user->image)){ echo base_url('mainasset/fe/images/users/').$user->image;}else{ echo base_url('mainasset/fe/images/profile1.png');} ?>" alt="profile image">
                        <span><?php echo $user->full_name; ?></span>
                    </div>
                    <a class="dropdown-item active" href="<?php echo base_url('users/profile'); ?>">View Profile</a>
                    <div class="dropdown-divider mb-0 py-0"></div>
                    <a class="dropdown-item" href="<?php echo base_url();?>logout">Logout</a>
                </div>
            </li>
        </ul>
    </div>
    <script>
        $('#label').click(function () {
            $('#navlist').toggleClass('d-hide d-show');
            console.log("all good");
        });
    </script>
</nav>
