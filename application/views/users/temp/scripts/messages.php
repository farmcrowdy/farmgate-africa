<script type="text/javascript">

$(document).ready(function(){


// alert('Hello Messaging people');

$('a.load_messages').click(function(event){
    
    event.preventDefault();

    // remove the active chat style and apply to this
    $(this).parent().siblings().removeClass('active-chat');
    $(this).parent().addClass('active-chat');

    // Get and load the message chat link dynamically
    var link = $(this).attr('href');
    $( "#message_area" ).empty();
    $( "#message_area" ).load( link, function() {
        // alert( "Load was performed." );
        
    });
});


});
// End of Document Ready Function

</script>