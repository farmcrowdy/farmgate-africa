<script type="text/javascript">

$(document).ready(function(){


// Load the Product State, LGA and Category on PageLoad for the editing ...
var current_state = '<?php if(!empty($product->prod_states_id)) {echo trim($product->prod_states_id);} else { echo "";} ?>';
var current_lga   = '<?php if(!empty($product->prod_locals_id)) {echo trim($product->prod_locals_id); } else {echo "";} ?>';
var current_category  = '<?php if(!empty($product->prod_category_id)) {echo trim($product->prod_category_id); } else {echo "";} ?>';
var current_units_id  = '<?php if(!empty($product->prod_units_id)) {echo trim($product->prod_units_id); } else {echo "";} ?>';

  $('#stateSelect').find("option[value="+current_state+"]").attr("selected","selected");
  //Load the LGAs for this state, so LGA can find a value
  var state = $("#stateSelect option:selected").val();
  $.ajax({
            url: "<?php echo base_url('users/locals'); ?>",
            type: 'POST',
            data:  {state_id: state},
            dataType: 'json',
            success: function(resp)
            {
                if (resp.code == 1) {
                    var lgas = resp.lgas;
                    if(lgas.length > 0){
                    $('#localSelect').empty();
                    }
                    for (var i = 0; i < lgas.length; i++) {
                        $('#localSelect').append($('<option>', { value: lgas[i].local_id, text: lgas[i].local_name }));
                    }
                    $('#localSelect').find("option[value="+current_lga+"]").attr("selected","selected");
                }
            },
            error: function(err) 
            {
                console.log(err);
            }  

        });

  $('#categorySelect').find("option[value="+current_category+"]").attr("selected","selected");
  $('#unitSelect').find("option[value="+current_units_id+"]").attr("selected","selected");
  

  // Normal State Change behaviour
  $('#stateSelect').change( function(){

        var state = $("#stateSelect option:selected").val();

        $.ajax({
            url: "<?php echo base_url('users/locals'); ?>",
            type: 'POST',
            data:  {state_id: state},
            dataType: 'json',
            success: function(resp)
            {
                if (resp.code == 1) {
                    var lgas = resp.lgas;
                    if(lgas.length > 0){
                    $('#localSelect').empty();
                    }
                    for (var i = 0; i < lgas.length; i++) {
                        $('#localSelect').append($('<option>', { value: lgas[i].local_id, text: lgas[i].local_name }));
                    }
                }
            },
            error: function(err) 
            {
                console.log(err);
            }  

        });
    });


});
// End of Document Ready Function

</script>