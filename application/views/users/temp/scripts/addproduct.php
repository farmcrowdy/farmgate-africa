<script type="text/javascript">

$(document).ready(function(){

  $('#stateSelect').change( function(){

        var state = $("#stateSelect option:selected").val();

        $.ajax({
            url: "<?php echo base_url('users/locals'); ?>",
            type: 'POST',
            data:  {state_id: state},
            dataType: 'json',
            success: function(resp)
            {
                if (resp.code == 1) {
                    var lgas = resp.lgas;
                    if(lgas.length > 0){
                    $('#localSelect').empty();
                    }
                    for (var i = 0; i < lgas.length; i++) {
                        $('#localSelect').append($('<option>', { value: lgas[i].local_id, text: lgas[i].local_name }));
                    }
                }
            },
            error: function(err) 
            {
                console.log(err);
            }  

        });
    });


});
// End of Document Ready Function

</script>