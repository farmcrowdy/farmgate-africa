<script type="text/javascript">

$(document).ready(function(){


    // Scrolling the Messageboard
    $.fn.scrollViewDown = function (element) {
        return this.each(function () {
            $(element).animate({
            scrollTop: $(this).prop("scrollHeight")
            }, 1000);
        });
    }

    $.fn.scrollViewUp= function (element) {
        return this.each(function () {
            $(element).animate({
            scrollTop: $(this).offset.top
            }, 1000);
        });
    }

    // alert('Hello Messaging people =========== See the Chats');
    $('form#chat_form').submit(function(event){
        event.preventDefault();
        
        var formData = new FormData(this);

        console.log(formData);

        // Disable button and clear input fields
        $('chatBtnz').prop('disabled',true);
        $(this)[0].reset();
        
        $.ajax({
            type: "post",
            url: "/users/postmessage/",
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(data){
                data = JSON.parse(data);
                console.log(data);
                console.log(data["type"]);
                for (var i = 0; i < data.length; i++) {

                    $('.no-chat-selected').removeClass('no-chat-selected');
                    if(data[i].type === "text"){
                        $('#message_text').val('');
                        $('#message_board').fadeIn(400).append(appendText(data[i].id, data[i].content, data[i].cdate));
                        $('#message_board').scrollViewDown('#message_board');
                    }
                    if(data[i].type === "image"){
                        $('#image-preview').fadeOut(200).attr('src', '#');
                        $('#upload-picture-for-admin').val("");
                        $('#message_board').fadeIn(400).append(appendImage(data[i].id, data[i].content, data[i].cdate));
                        $('#message_board').scrollViewDown('#message_board');
                    }
                }

                // Enable button
               $('chatBtnz').prop('disabled', false);

                
            },
            error: function() { alert("Error communicating with our servers."); }
        });
    })

    $('#message_board').scroll(function() {
        var scrollPosition = $(this).scrollTop();
        // var iScrollHeight = $(this).prop("scrollHeight");
        // console.log("The current scroll position: "+scrollPosition);
        // console.log("The current scroll height: "+iScrollHeight);
        if(scrollPosition < 3) {
            var last_id = $(".post-id:first").attr("id");
            loadMoreData(last_id);
        }
    });

    function loadMoreData(last_id){

        var order_id = $('#m_order_id').val();
        $.ajax({
            type: "post",
            url: "/users/loadmessages/",
            data: { "last_id": last_id, "order_id": order_id},
            success: function(data){
                data = JSON.parse(data);
                for (var i = 0; i < data.length; i++) {

                    if(data[i].type === "text"){
                        if(data[i].sender === "user"){
                            $('#message_board').fadeIn(400).prepend(appendText(data[i].id, data[i].content, data[i].cdate));
                        }else{
                            $('#message_board').fadeIn(400).prepend(appendLeftText(data[i].id, data[i].content, data[i].cdate));
                        }
                        $('#message_board').scrollViewUp('#message_board');
                    }
                    if(data[i].type === "image"){
                        if(data[i].sender === "user"){
                            $('#message_board').fadeIn(400).prepend(appendImage(data[i].id, data[i].content, data[i].cdate));
                        }else{
                            $('#message_board').fadeIn(400).prepend(appendLeftImage(data[i].id, data[i].content, data[i].cdate));
                        }
                        $('#message_board').scrollViewUp('#message_board');
                    }
                }
                
            },
            error: function() { alert("Error communicating with our servers."); }
        });
    }

    function loadInitialData(last_id){

        var order_id = $('#m_order_id').val();
        $.ajax({
            type: "post",
            url: "/users/loadmessages/",
            data: { "last_id": last_id, "order_id": order_id, "initial_load": 8},
            success: function(data){
                data = JSON.parse(data);
                for (var i = 0; i < data.length; i++) {

                    if(data[i].type === "text"){
                        if(data[i].sender === "user"){
                            $('#message_board').fadeIn(400).prepend(appendText(data[i].id, data[i].content, data[i].cdate));
                        }else{
                            $('#message_board').fadeIn(400).prepend(appendLeftText(data[i].id, data[i].content, data[i].cdate));
                        }
                        $('#message_board').scrollViewUp('#message_board');
                    }
                    if(data[i].type === "image"){
                        if(data[i].sender === "user"){
                            $('#message_board').fadeIn(400).prepend(appendImage(data[i].id, data[i].content, data[i].cdate));
                        }else{
                            $('#message_board').fadeIn(400).prepend(appendLeftImage(data[i].id, data[i].content, data[i].cdate));
                        }
                        $('#message_board').scrollViewUp('#message_board');
                    }
                }
                
            },
            error: function() { alert("Error communicating with our servers."); }
        });
    }

    function loadNewMessage(){

        var order_id = $('#m_order_id').val();
        var last_id = $(".post-id:last").attr("id");
        $.ajax({
            type: "post",
            url: "/users/newmessage/",
            data: { "order_id": order_id, "last_id": last_id },
            success: function(data){
                data = JSON.parse(data);
                for (var i = 0; i < data.length; i++) {

                    if(data[i].type === "text"){
                        if(data[i].sender === "user"){
                            $('#message_board').fadeIn(400).append(appendText(data[i].id, data[i].content, data[i].cdate));
                        }else{
                            $('#message_board').fadeIn(400).append(appendLeftText(data[i].id, data[i].content, data[i].cdate));
                        }
                        $('#message_board').scrollViewDown('#message_board');
                    }
                    if(data[i].type === "image"){
                        if(data[i].sender === "user"){
                            $('#message_board').fadeIn(400).append(appendImage(data[i].id, data[i].content, data[i].cdate));
                        }else{
                            $('#message_board').fadeIn(400).append(appendLeftImage(data[i].id, data[i].content, data[i].cdate));
                        }
                        $('#message_board').scrollViewDown('#message_board');
                    }
                }

                setTimeout(loadNewMessage, 3000);
                
            },
            error: function() { alert("Error communicating with our servers."); setTimeout(loadNewMessage, 3000); }
        });
    }

    function appendText( id, content, cdate ){

        var html = '<li class="d-flex justify-content-end my-2 post-id" id="';
        html += id;
        html += '"><div class="speech-bubble-left my-2"><p class="p-2 mb-0 chat-text">';
        html += content;
        html +='</p><div class="speech-bubble-border-left"></div></div><p class="chat-date-right">';
        html += String(cdate);
        html +='</p></li>';
        return html;
    }


    function appendLeftText( id, content, cdate ){

        var html = '<li class="d-flex justify-content-start my-2 post-id" id="';
        html += id;
        html +='"><div class="speech-bubble-right"><p class="p-2 mb-0 chat-text">';
        html += content;
        html +='</p><div class="speech-bubble-border-right"></div></div><p class="chat-date-left">';
        html += String(cdate);
        html +='</p></li>';
        return html;     
    }

    function appendImage( id, content, cdate ){

        var html = '<li class="d-flex justify-content-end my-2 post-id" id="';
        html +=id;
        html +='"><div class="speech-bubble-left my-2"><a data-fancybox="gallery" href="<?php echo base_url('mainasset/fe/images/messages/'); ?>'+content+'" >';
        html += '<img class="img-fluid img-fit" data-lightbox="image-'+id+'" src="<?php echo base_url('mainasset/fe/images/messages/'); ?>';
        html += content;
        html +='" /></a>';
        html += '<div class="speech-bubble-border-left"></div></div><p class="chat-date-right">';
        html += cdate;
        html += '</p></li>';
        return html;
    }

    function appendLeftImage( id, content, cdate ){

        var html = '<li class="d-flex justify-content-start my-2 post-id" id="';
        html +=id;
        html +='"><div class="speech-bubble-right"><a data-fancybox="gallery" href="<?php echo base_url('mainasset/fe/images/messages/'); ?>'+content+'">';
        html += '<img class="img-fluid img-fit" data-lightbox="image-'+id+'" src="<?php echo base_url('mainasset/fe/images/messages/'); ?>';
        html += content;
        html +='" /></a>';
        html += '<div class="speech-bubble-border-right"></div></div><p class="chat-date-left">';
        html += cdate;
        html += '</p></li>';
        return html;	
    }

    // Get the last order id to load 
    var last_msg_id = '<?php if(!empty($last_msg_id)) {echo trim($last_msg_id->m_id);} else { echo "";} ?>';
    loadInitialData(last_msg_id);

    // SetTimeout to check for new messages
    setTimeout(loadNewMessage, 3000);

});
// End of Document Ready Function

</script>