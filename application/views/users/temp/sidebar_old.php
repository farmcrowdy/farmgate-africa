
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 col-md-12 offset-lg-1 pt-4">
                <div class="row">
                    <aside class="col-md-2 px-0 aside-mobile accordion slideInRight animatedSlide slideInFromLeft d-none d-md-block " style="background:#293737;"
                        id="sidemenuToggle">
                        <div class=" pb-5 close-btn d-md-none">
                            <button type="button" class="close" aria-label="Close">
                                <span aria-hidden="true" class=" hide-sidebar" style="color:#fff;">&times;</span>
                            </button>
                        </div>
                        <ul class="nav flex-column orders-aside">
                            <li class="nav-item side-bar">
                                <a class="nav-link py-3 d-flex active side-bar" href="<?php echo base_url();?>users/dashboard">
                                    <svg class="align-self-center" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate"
                                        viewBox="0 0 16 16" width="16" height="16">
                                        <defs>
                                            <clipPath id="_clipPath_GA04HI733UwQB1jPmthcub1XpisaTCdI">
                                                <rect width="16" height="16" />
                                            </clipPath>
                                        </defs>
                                        <g clip-path="url(#_clipPath_GA04HI733UwQB1jPmthcub1XpisaTCdI)">
                                            <path d=" M 2.103 14.222 C 1.608 14.222 1.155 13.949 0.923 13.512 C 0.334 12.397 0 11.127 0 9.778 C 0 5.359 3.582 1.778 8 1.778 C 12.418 1.778 16 5.359 16 9.778 C 16 11.127 15.666 12.397 15.077 13.512 C 14.845 13.949 14.392 14.222 13.897 14.222 L 2.103 14.222 Z  M 14.222 8.889 C 13.731 8.889 13.333 9.287 13.333 9.778 C 13.333 10.269 13.731 10.667 14.222 10.667 C 14.713 10.667 15.111 10.269 15.111 9.778 C 15.111 9.287 14.713 8.889 14.222 8.889 Z  M 8 4.444 C 8.491 4.444 8.889 4.046 8.889 3.556 C 8.889 3.065 8.491 2.667 8 2.667 C 7.509 2.667 7.111 3.065 7.111 3.556 C 7.111 4.046 7.509 4.444 8 4.444 Z  M 1.778 8.889 C 1.287 8.889 0.889 9.287 0.889 9.778 C 0.889 10.269 1.287 10.667 1.778 10.667 C 2.269 10.667 2.667 10.269 2.667 9.778 C 2.667 9.287 2.269 8.889 1.778 8.889 Z  M 3.6 4.489 C 3.109 4.489 2.711 4.887 2.711 5.378 C 2.711 5.869 3.109 6.267 3.6 6.267 C 4.091 6.267 4.489 5.869 4.489 5.378 C 4.489 4.887 4.091 4.489 3.6 4.489 Z  M 12.4 4.489 C 11.909 4.489 11.511 4.887 11.511 5.378 C 11.511 5.869 11.909 6.267 12.4 6.267 C 12.891 6.267 13.289 5.869 13.289 5.378 C 13.289 4.887 12.891 4.489 12.4 4.489 Z  M 9.981 5.365 C 9.63 5.253 9.255 5.446 9.143 5.797 L 7.867 9.783 C 6.947 9.851 6.222 10.618 6.222 11.556 C 6.222 12.537 7.018 13.333 8 13.333 C 8.982 13.333 9.778 12.537 9.778 11.556 C 9.778 11.006 9.529 10.515 9.137 10.189 L 10.413 6.203 C 10.525 5.852 10.332 5.477 9.981 5.365 L 9.981 5.365 Z "
                                                fill="rgb(255,255,255)" />
                                        </g>
                                    </svg>
                                    <span class="pt-1 pl-3 ">Dashboard</span>
                                </a>
                            </li>
                            <li class="nav-item side-bar  " id="product">
                                <a class="nav-link py-3 d-flex  collapse-sidebar side-bar collapsed" id="svg" href="javascript:void(0)" data-toggle="collapse"
                                    data-target="#productId" aria-expanded="true" aria-controls="collapseOne">
                                    <svg class="align-self-center" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate"
                                        viewBox="0 0 16 16" width="16" height="16">
                                        <defs>
                                            <clipPath id="_clipPath_NOh5sdFxxqAZMZe2EEE4ZZOhxtbA2DnF">
                                                <rect width="16" height="16" />
                                            </clipPath>
                                        </defs>
                                        <g clip-path="url(#_clipPath_NOh5sdFxxqAZMZe2EEE4ZZOhxtbA2DnF)">
                                            <clipPath id="_clipPath_hS1TwJtDk3m53Evpw88SfcXPaqkzziyh">
                                                <rect x="0" y="0" width="16" height="16" transform="matrix(1,0,0,1,0,0)" fill="rgb(255,255,255)"
                                                />
                                            </clipPath>
                                            <g clip-path="url(#_clipPath_hS1TwJtDk3m53Evpw88SfcXPaqkzziyh)">
                                                <g id="Group">
                                                    <path d=" M 2.111 0 C 1.112 0.004 0.303 0.812 0.299 1.812 L 0.299 13.889 C 0.303 14.888 1.112 15.696 2.111 15.7 L 14.188 15.7 C 15.187 15.696 15.996 14.887 16 13.889 L 16 1.812 C 15.996 0.813 15.187 0.004 14.188 0.001 L 2.111 0 Z  M 2.111 1.208 L 14.188 1.208 C 14.35 1.206 14.505 1.269 14.618 1.382 C 14.732 1.496 14.795 1.651 14.793 1.812 L 14.793 13.889 C 14.795 14.051 14.732 14.206 14.618 14.319 C 14.505 14.433 14.35 14.496 14.188 14.493 L 2.111 14.493 C 1.95 14.496 1.795 14.433 1.681 14.319 C 1.568 14.206 1.505 14.051 1.507 13.889 L 1.507 1.812 C 1.505 1.651 1.568 1.496 1.681 1.382 C 1.795 1.269 1.95 1.206 2.111 1.208 L 2.111 1.208 Z  M 3.923 3.627 L 3.923 4.835 L 5.131 4.835 L 5.131 3.627 L 3.923 3.627 Z  M 6.342 3.627 L 6.342 4.835 L 12.381 4.835 L 12.381 3.627 L 6.342 3.627 Z  M 3.923 7.25 L 3.923 8.458 L 5.131 8.458 L 5.131 7.246 L 3.923 7.25 Z  M 6.342 7.25 L 6.342 8.458 L 12.381 8.458 L 12.381 7.246 L 6.342 7.25 Z  M 3.923 10.873 L 3.923 12.081 L 5.131 12.081 L 5.131 10.873 L 3.923 10.873 Z  M 6.342 10.873 L 6.342 12.081 L 12.381 12.081 L 12.381 10.873 L 6.342 10.873 Z "
                                                        fill="rgb(255,255,255)" />
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    <span class="pt-1 pl-3">My Products</span>
                                    <!-- <i class="fas fa-angle-right align-self-center"></i> -->

                                    <!-- <svg  class="pr-0 mr-0" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate" viewBox="0 0 12 12" width="12" height="12"><defs><clipPath id="_clipPath_JMxFm7N84VAb0BObUiu0dgtI66WeboEn"><rect width="12" height="12"/></clipPath></defs><g clip-path="url(#_clipPath_JMxFm7N84VAb0BObUiu0dgtI66WeboEn)"><path d=" M 5.999 6.822 L 10.382 2.529 C 10.577 2.336 10.89 2.336 11.085 2.529 L 11.853 3.296 C 11.947 3.39 12 3.517 12 3.651 C 12 3.784 11.947 3.911 11.853 4.005 L 6.354 9.469 C 6.158 9.664 5.842 9.664 5.646 9.469 L 0.147 4.005 C 0.053 3.911 0 3.784 0 3.651 C 0 3.517 0.053 3.39 0.147 3.296 L 0.915 2.529 C 1.11 2.336 1.423 2.336 1.618 2.529 L 5.999 6.822 Z " fill="rgb(255,255,255)"/></g></svg>      -->
                                </a>
                                <!-- =======sub-order starts========= -->

                                <ul class="collapse px-0 side-bar-submenu" aria-labelledby="headingOne" data-parent="#sidemenuToggle" id="productId">
                                    <li class="sidebar-item nav-item">
                                        <a href="<?php echo base_url();?>users/products" class="sidebar-link nav-link py-2">
                                            <span class="hide-menu pl-2"> Products </span>
                                        </a>
                                    </li>
                                    <li class="sidebar-item nav-item">
                                        <a href="<?php echo base_url();?>users/addproduct" class="sidebar-link py-2 nav-link">
                                            <span class="hide-menu pl-2"> Add Product </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item svg side-bar">
                                <a class="nav-link  py-3 d-flex side-bar" href="<?php echo base_url();?>products">
                                    <svg class="align-self-center" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate"
                                        viewBox="0 0 16 16" width="16" height="16">
                                        <defs>
                                            <clipPath id="_clipPath_rImc54fUYBZE9nD6mxBYCAwd7R0vUoL0">
                                                <rect width="16" height="16" />
                                            </clipPath>
                                        </defs>
                                        <g clip-path="url(#_clipPath_rImc54fUYBZE9nD6mxBYCAwd7R0vUoL0)">
                                            <path d=" M 12.5 2 L 10 2 C 10 0.897 9.103 0 8 0 C 6.897 0 6 0.897 6 2 L 3.5 2 C 2.672 2 2 2.672 2 3.5 L 2 14.5 C 2 15.328 2.672 16 3.5 16 L 12.5 16 C 13.328 16 14 15.328 14 14.5 L 14 3.5 C 14 2.672 13.328 2 12.5 2 Z  M 5 13.25 C 4.584 13.25 4.25 12.916 4.25 12.5 C 4.25 12.084 4.584 11.75 5 11.75 C 5.416 11.75 5.75 12.084 5.75 12.5 C 5.75 12.916 5.416 13.25 5 13.25 Z  M 5 10.25 C 4.584 10.25 4.25 9.916 4.25 9.5 C 4.25 9.084 4.584 8.75 5 8.75 C 5.416 8.75 5.75 9.084 5.75 9.5 C 5.75 9.916 5.416 10.25 5 10.25 Z  M 5 7.25 C 4.584 7.25 4.25 6.916 4.25 6.5 C 4.25 6.084 4.584 5.75 5 5.75 C 5.416 5.75 5.75 6.084 5.75 6.5 C 5.75 6.916 5.416 7.25 5 7.25 Z  M 8 1.25 C 8.416 1.25 8.75 1.584 8.75 2 C 8.75 2.416 8.416 2.75 8 2.75 C 7.584 2.75 7.25 2.416 7.25 2 C 7.25 1.584 7.584 1.25 8 1.25 Z  M 12 12.75 C 12 12.888 11.888 13 11.75 13 L 7.25 13 C 7.113 13 7 12.888 7 12.75 L 7 12.25 C 7 12.113 7.113 12 7.25 12 L 11.75 12 C 11.888 12 12 12.113 12 12.25 L 12 12.75 Z  M 12 9.75 C 12 9.888 11.888 10 11.75 10 L 7.25 10 C 7.113 10 7 9.888 7 9.75 L 7 9.25 C 7 9.113 7.113 9 7.25 9 L 11.75 9 C 11.888 9 12 9.113 12 9.25 L 12 9.75 Z  M 12 6.75 C 12 6.888 11.888 7 11.75 7 L 7.25 7 C 7.113 7 7 6.888 7 6.75 L 7 6.25 C 7 6.113 7.113 6 7.25 6 L 11.75 6 C 11.888 6 12 6.113 12 6.25 L 12 6.75 Z "
                                                fill="rgb(255,255,255)" />
                                        </g>
                                    </svg>
                                    <span class="pl-3 pt-1">Other Products</span>
                                </a>
                            </li>
                            <li class="nav-item svg side-bar">
                                <a class="nav-link  py-3 d-flex side-bar" href="<?php echo base_url();?>users/orders">
                                    <svg class="align-self-center" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate"
                                        viewBox="0 0 16 16" width="16" height="16">
                                        <defs>
                                            <clipPath id="_clipPath_rImc54fUYBZE9nD6mxBYCAwd7R0vUoL0">
                                                <rect width="16" height="16" />
                                            </clipPath>
                                        </defs>
                                        <g clip-path="url(#_clipPath_rImc54fUYBZE9nD6mxBYCAwd7R0vUoL0)">
                                            <path d=" M 12.5 2 L 10 2 C 10 0.897 9.103 0 8 0 C 6.897 0 6 0.897 6 2 L 3.5 2 C 2.672 2 2 2.672 2 3.5 L 2 14.5 C 2 15.328 2.672 16 3.5 16 L 12.5 16 C 13.328 16 14 15.328 14 14.5 L 14 3.5 C 14 2.672 13.328 2 12.5 2 Z  M 5 13.25 C 4.584 13.25 4.25 12.916 4.25 12.5 C 4.25 12.084 4.584 11.75 5 11.75 C 5.416 11.75 5.75 12.084 5.75 12.5 C 5.75 12.916 5.416 13.25 5 13.25 Z  M 5 10.25 C 4.584 10.25 4.25 9.916 4.25 9.5 C 4.25 9.084 4.584 8.75 5 8.75 C 5.416 8.75 5.75 9.084 5.75 9.5 C 5.75 9.916 5.416 10.25 5 10.25 Z  M 5 7.25 C 4.584 7.25 4.25 6.916 4.25 6.5 C 4.25 6.084 4.584 5.75 5 5.75 C 5.416 5.75 5.75 6.084 5.75 6.5 C 5.75 6.916 5.416 7.25 5 7.25 Z  M 8 1.25 C 8.416 1.25 8.75 1.584 8.75 2 C 8.75 2.416 8.416 2.75 8 2.75 C 7.584 2.75 7.25 2.416 7.25 2 C 7.25 1.584 7.584 1.25 8 1.25 Z  M 12 12.75 C 12 12.888 11.888 13 11.75 13 L 7.25 13 C 7.113 13 7 12.888 7 12.75 L 7 12.25 C 7 12.113 7.113 12 7.25 12 L 11.75 12 C 11.888 12 12 12.113 12 12.25 L 12 12.75 Z  M 12 9.75 C 12 9.888 11.888 10 11.75 10 L 7.25 10 C 7.113 10 7 9.888 7 9.75 L 7 9.25 C 7 9.113 7.113 9 7.25 9 L 11.75 9 C 11.888 9 12 9.113 12 9.25 L 12 9.75 Z  M 12 6.75 C 12 6.888 11.888 7 11.75 7 L 7.25 7 C 7.113 7 7 6.888 7 6.75 L 7 6.25 C 7 6.113 7.113 6 7.25 6 L 11.75 6 C 11.888 6 12 6.113 12 6.25 L 12 6.75 Z "
                                                fill="rgb(255,255,255)" />
                                        </g>
                                    </svg>
                                    <span class="pl-3 pt-1">My Orders</span>
                                </a>
                            </li>
                            <li class="nav-item side-bar">
                                <a class="nav-link py-3 side-bar d-flex" href="<?php echo base_url();?>users/messages">
                                    <svg class="align-self-center" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate"
                                        viewBox="0 0 18 18" width="18" height="18">
                                        <defs>
                                            <clipPath id="_clipPath_UA6ZWV00CoqrqqKeWwBMGD4bxKPPvCsL">
                                                <rect width="18" height="18" />
                                            </clipPath>
                                        </defs>
                                        <g clip-path="url(#_clipPath_UA6ZWV00CoqrqqKeWwBMGD4bxKPPvCsL)">
                                            <clipPath id="_clipPath_wuuVLbYgxw47n1NfKcADjAsprHN0qGM0">
                                                <rect x="0" y="0" width="18" height="18" transform="matrix(1,0,0,1,0,0)" fill="rgb(255,255,255)"
                                                />
                                            </clipPath>
                                            <g clip-path="url(#_clipPath_wuuVLbYgxw47n1NfKcADjAsprHN0qGM0)">
                                                <g id="Group">
                                                    <clipPath id="_clipPath_5Re36l7IRg0ySpiv5O2nzDj6B7BcnARq">
                                                        <rect x="0" y="0" width="18" height="18" transform="matrix(1,0,0,1,0,0)"
                                                            fill="rgb(255,255,255)" />
                                                    </clipPath>
                                                    <g clip-path="url(#_clipPath_5Re36l7IRg0ySpiv5O2nzDj6B7BcnARq)">
                                                        <g id="Group">
                                                            <g id="Group">
                                                                <g id="message">
                                                                    <g id="Group_118">
                                                                        <g id="Group_117">
                                                                            <path d=" M 16.2 1.682 L 8.4 1.682 C 7.406 1.682 6.6 2.489 6.6 3.482 L 6.6 7.082 L 1.8 7.082 C 1.322 7.082 0.865 7.272 0.528 7.61 C 0.189 7.947 0 8.405 0 8.882 L 0 13.082 C -0.001 13.961 0.632 14.714 1.5 14.861 L 1.5 15.786 C 1.499 15.909 1.573 16.019 1.685 16.065 C 1.799 16.111 1.928 16.085 2.015 15.999 L 3.126 14.886 L 9.6 14.886 C 10.594 14.886 11.4 14.081 11.4 13.086 L 11.4 9.486 L 14.876 9.486 L 15.989 10.599 C 16.074 10.685 16.203 10.711 16.316 10.665 C 16.429 10.619 16.503 10.509 16.502 10.386 L 16.502 9.461 C 17.369 9.313 18.001 8.561 18 7.683 L 18 3.482 C 18 2.489 17.195 1.682 16.2 1.682 Z  M 10.8 13.083 C 10.8 13.745 10.262 14.282 9.6 14.282 L 3 14.282 C 2.921 14.282 2.845 14.315 2.789 14.371 L 2.1 15.058 L 2.1 14.582 C 2.1 14.417 1.965 14.282 1.8 14.282 C 1.137 14.282 0.601 13.745 0.6 13.083 L 0.6 8.882 C 0.601 8.219 1.137 7.683 1.8 7.683 L 9.6 7.683 C 10.262 7.683 10.8 8.219 10.8 8.882 L 10.8 13.083 Z  M 17.4 7.683 C 17.399 8.345 16.863 8.882 16.2 8.882 C 16.035 8.882 15.9 9.017 15.9 9.182 L 15.9 9.658 L 15.211 8.971 C 15.155 8.915 15.08 8.882 15 8.882 L 11.4 8.882 C 11.4 7.889 10.594 7.082 9.6 7.082 L 7.2 7.082 L 7.2 3.482 C 7.2 2.819 7.738 2.282 8.4 2.282 L 16.2 2.282 C 16.863 2.282 17.4 2.819 17.4 3.482 L 17.4 7.683 Z "
                                                                                fill="rgb(255,255,255)" />
                                                                        </g>
                                                                    </g>
                                                                    <g id="Group_120">
                                                                        <g id="Group_119">
                                                                            <path d=" M 3.3 10.082 C 2.935 10.082 2.608 10.302 2.468 10.638 C 2.329 10.974 2.405 11.361 2.663 11.619 C 2.921 11.877 3.309 11.953 3.645 11.814 C 3.98 11.675 4.2 11.347 4.2 10.982 C 4.2 10.485 3.797 10.082 3.3 10.082 L 3.3 10.082 Z  M 3.3 11.283 C 3.178 11.283 3.069 11.21 3.022 11.098 C 2.976 10.984 3.002 10.856 3.088 10.771 C 3.174 10.684 3.302 10.658 3.416 10.706 C 3.527 10.752 3.6 10.862 3.6 10.982 C 3.6 11.062 3.569 11.139 3.512 11.195 C 3.456 11.251 3.38 11.284 3.3 11.284 L 3.3 11.283 Z "
                                                                                fill="rgb(255,255,255)" />
                                                                        </g>
                                                                    </g>
                                                                    <g id="Group_122">
                                                                        <g id="Group_121">
                                                                            <path d=" M 5.7 10.082 C 5.336 10.082 5.009 10.302 4.868 10.638 C 4.73 10.974 4.806 11.361 5.064 11.619 C 5.321 11.877 5.708 11.953 6.045 11.814 C 6.381 11.675 6.6 11.347 6.6 10.982 C 6.6 10.485 6.198 10.082 5.7 10.082 L 5.7 10.082 Z  M 5.7 11.283 C 5.579 11.283 5.469 11.21 5.423 11.098 C 5.376 10.984 5.401 10.856 5.489 10.771 C 5.574 10.684 5.703 10.658 5.815 10.706 C 5.928 10.752 6.001 10.862 6.001 10.982 C 6.001 11.062 5.969 11.139 5.913 11.195 C 5.857 11.251 5.78 11.284 5.7 11.284 L 5.7 11.283 Z "
                                                                                fill="rgb(255,255,255)" />
                                                                        </g>
                                                                    </g>
                                                                    <g id="Group_124">
                                                                        <g id="Group_123">
                                                                            <path d=" M 8.1 10.082 C 7.736 10.082 7.408 10.302 7.269 10.638 C 7.129 10.974 7.206 11.361 7.463 11.619 C 7.721 11.877 8.108 11.953 8.444 11.814 C 8.781 11.675 9 11.347 9 10.982 C 9 10.485 8.597 10.082 8.1 10.082 L 8.1 10.082 Z  M 8.1 11.283 C 7.978 11.283 7.868 11.21 7.822 11.098 C 7.776 10.984 7.802 10.856 7.889 10.771 C 7.974 10.684 8.102 10.658 8.215 10.706 C 8.327 10.752 8.4 10.862 8.4 10.982 C 8.4 11.062 8.369 11.139 8.313 11.195 C 8.256 11.251 8.18 11.284 8.1 11.284 L 8.1 11.283 Z "
                                                                                fill="rgb(255,255,255)" />
                                                                        </g>
                                                                    </g>
                                                                    <g id="Group_126">
                                                                        <g id="Group_125">
                                                                            <rect x="8.4" y="3.483" width="5.4" height="0.6"
                                                                                transform="matrix(1,0,0,1,0,0)" fill="rgb(255,255,255)"
                                                                            />
                                                                        </g>
                                                                    </g>
                                                                    <g id="Group_128">
                                                                        <g id="Group_127">
                                                                            <rect x="14.4" y="3.483" width="1.8" height="0.6"
                                                                                transform="matrix(1,0,0,1,0,0)" fill="rgb(255,255,255)"
                                                                            />
                                                                        </g>
                                                                    </g>
                                                                    <g id="Group_130">
                                                                        <g id="Group_129">
                                                                            <rect x="8.4" y="4.683" width="1.8" height="0.6"
                                                                                transform="matrix(1,0,0,1,0,0)" fill="rgb(255,255,255)"
                                                                            />
                                                                        </g>
                                                                    </g>
                                                                    <g id="Group_132">
                                                                        <g id="Group_131">
                                                                            <rect x="10.8" y="4.683" width="3.6" height="0.6"
                                                                                transform="matrix(1,0,0,1,0,0)" fill="rgb(255,255,255)"
                                                                            />
                                                                        </g>
                                                                    </g>
                                                                    <g id="Group_134">
                                                                        <g id="Group_133">
                                                                            <rect x="8.4" y="5.883" width="7.8" height="0.6"
                                                                                transform="matrix(1,0,0,1,0,0)" fill="rgb(255,255,255)"
                                                                            />
                                                                        </g>
                                                                    </g>
                                                                    <g id="Group_136">
                                                                        <g id="Group_135">
                                                                            <rect x="11.4" y="7.083" width="2.4" height="0.6"
                                                                                transform="matrix(1,0,0,1,0,0)" fill="rgb(255,255,255)"
                                                                            />
                                                                        </g>
                                                                    </g>
                                                                    <g id="Group_138">
                                                                        <g id="Group_137">
                                                                            <rect x="14.4" y="7.083" width="1.8" height="0.6"
                                                                                transform="matrix(1,0,0,1,0,0)" fill="rgb(255,255,255)"
                                                                            />
                                                                        </g>
                                                                    </g>
                                                                    <g id="Group_140">
                                                                        <g id="Group_139">
                                                                            <rect x="15" y="4.683" width="1.2" height="0.6" transform="matrix(1,0,0,1,0,0)"
                                                                                fill="rgb(255,255,255)" />
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    <span class="pl-3 pt-1">Messages</span>
                                </a>
                            </li>
                            <!-- <li class="nav-item side-bar " id="profile">
                                <a class="nav-link py-3 d-flex  side-bar collapse-sidebar collapsed" id="svg" href="javascript:void(0)"  data-toggle="collapse" data-target="#profileId" aria-expanded="true" aria-controls="collapseOne">
                                    <svg class="align-self-center" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate" viewBox="0 0 16 16" width="16" height="16"><defs><clipPath id="_clipPath_LQ0Nah6kYJD8wH2cR6NmldO9cERnKCA3"><rect width="16" height="16"/></clipPath></defs><g clip-path="url(#_clipPath_LQ0Nah6kYJD8wH2cR6NmldO9cERnKCA3)"><clipPath id="_clipPath_tfcGFrgwNTPvHCNAAWMu0HEZ8Bh3haLW"><rect x="0" y="0" width="16" height="16" transform="matrix(1,0,0,1,0,0)" fill="rgb(255,255,255)"/></clipPath><g clip-path="url(#_clipPath_tfcGFrgwNTPvHCNAAWMu0HEZ8Bh3haLW)"><g id="Group"><clipPath id="_clipPath_Ltul5wmsiixZFd4vf0ygpLmAmXqSaXQV"><rect x="0" y="0" width="16" height="16" transform="matrix(1,0,0,1,0,0)" fill="rgb(255,255,255)"/></clipPath><g clip-path="url(#_clipPath_Ltul5wmsiixZFd4vf0ygpLmAmXqSaXQV)"><g id="Group"><g id="Group"><path d=" M 7.687 -0.139 C 6.638 -0.149 5.634 0.29 4.929 1.067 C 4.223 1.844 3.883 2.886 3.993 3.93 C 4.029 4.432 4.246 5.42 4.283 5.922 C 4.283 5.93 4.283 5.945 4.283 5.951 C 4.572 7.35 6.167 8.173 7.687 8.173 C 9.165 8.178 10.503 7.305 11.094 5.951 C 11.098 5.944 11.094 5.931 11.094 5.922 C 11.24 5.267 11.336 4.6 11.382 3.93 C 11.622 2.355 10.556 0.879 8.986 0.611 L 8.612 -0.139 L 7.687 -0.139 Z  M 7.687 10.019 C 2.076 10.019 0.299 13.714 0.299 13.714 L 0.299 15.56 L 15.077 15.56 L 15.077 13.715 C 15.077 13.715 13.301 10.021 7.687 10.021 L 7.687 10.019 Z " fill="rgb(255,255,255)"/></g></g></g></g></g></g></svg>
                                    <span class="pt-1 pl-3  ">Profile</span>  -->
                            <!-- <i class="fas fa-angle-right align-self-center"></i> -->
                            <!-- </a> -->
                            <!-- =======sub-Profile starts========= -->

                            <!-- <ul class="collapse px-0 side-bar-submenu" aria-labelledby="headingOne" data-parent="#sidemenuToggle" id="profileId" >
                                    <li class="sidebar-item nav-item" >
                                        <a href="#" class="sidebar-link nav-link py-2">
                                            <span class="hide-menu pl-2"> View Profile  </span>
                                        </a>
                                    </li>
                                    <li class="sidebar-item nav-item">
                                        <a href="#" class="sidebar-link py-2 nav-link">
                                            <span class="hide-menu pl-2"> Change Password  </span>
                                        </a>
                                    </li>
                                </ul>
                            </li> -->
                            <li class="nav-item side-bar">
                                <a class="nav-link py-3 d-flex side-bar" href="<?php echo base_url();?>users/profile">
                                    <svg class="align-self-center" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate"
                                        viewBox="0 0 16 16" width="16" height="16">
                                        <defs>
                                            <clipPath id="_clipPath_LQ0Nah6kYJD8wH2cR6NmldO9cERnKCA3">
                                                <rect width="16" height="16" />
                                            </clipPath>
                                        </defs>
                                        <g clip-path="url(#_clipPath_LQ0Nah6kYJD8wH2cR6NmldO9cERnKCA3)">
                                            <clipPath id="_clipPath_tfcGFrgwNTPvHCNAAWMu0HEZ8Bh3haLW">
                                                <rect x="0" y="0" width="16" height="16" transform="matrix(1,0,0,1,0,0)" fill="rgb(255,255,255)"
                                                />
                                            </clipPath>
                                            <g clip-path="url(#_clipPath_tfcGFrgwNTPvHCNAAWMu0HEZ8Bh3haLW)">
                                                <g id="Group">
                                                    <clipPath id="_clipPath_Ltul5wmsiixZFd4vf0ygpLmAmXqSaXQV">
                                                        <rect x="0" y="0" width="16" height="16" transform="matrix(1,0,0,1,0,0)"
                                                            fill="rgb(255,255,255)" />
                                                    </clipPath>
                                                    <g clip-path="url(#_clipPath_Ltul5wmsiixZFd4vf0ygpLmAmXqSaXQV)">
                                                        <g id="Group">
                                                            <g id="Group">
                                                                <path d=" M 7.687 -0.139 C 6.638 -0.149 5.634 0.29 4.929 1.067 C 4.223 1.844 3.883 2.886 3.993 3.93 C 4.029 4.432 4.246 5.42 4.283 5.922 C 4.283 5.93 4.283 5.945 4.283 5.951 C 4.572 7.35 6.167 8.173 7.687 8.173 C 9.165 8.178 10.503 7.305 11.094 5.951 C 11.098 5.944 11.094 5.931 11.094 5.922 C 11.24 5.267 11.336 4.6 11.382 3.93 C 11.622 2.355 10.556 0.879 8.986 0.611 L 8.612 -0.139 L 7.687 -0.139 Z  M 7.687 10.019 C 2.076 10.019 0.299 13.714 0.299 13.714 L 0.299 15.56 L 15.077 15.56 L 15.077 13.715 C 15.077 13.715 13.301 10.021 7.687 10.021 L 7.687 10.019 Z "
                                                                    fill="rgb(255,255,255)" />
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    <span class="pl-3 pt-1">Profile</span>
                                </a>
                            </li>
                            <li class="nav-item svg side-bar">
                                <a class="nav-link  py-3 d-flex side-bar" href="<?php echo base_url('users/lpo');?>">
                                    <svg class="align-self-center" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate"
                                        viewBox="0 0 16 16" width="16" height="16">
                                        <defs>
                                            <clipPath id="_clipPath_rImc54fUYBZE9nD6mxBYCAwd7R0vUoL0">
                                                <rect width="16" height="16" />
                                            </clipPath>
                                        </defs>
                                        <g clip-path="url(#_clipPath_rImc54fUYBZE9nD6mxBYCAwd7R0vUoL0)">
                                            <path d=" M 12.5 2 L 10 2 C 10 0.897 9.103 0 8 0 C 6.897 0 6 0.897 6 2 L 3.5 2 C 2.672 2 2 2.672 2 3.5 L 2 14.5 C 2 15.328 2.672 16 3.5 16 L 12.5 16 C 13.328 16 14 15.328 14 14.5 L 14 3.5 C 14 2.672 13.328 2 12.5 2 Z  M 5 13.25 C 4.584 13.25 4.25 12.916 4.25 12.5 C 4.25 12.084 4.584 11.75 5 11.75 C 5.416 11.75 5.75 12.084 5.75 12.5 C 5.75 12.916 5.416 13.25 5 13.25 Z  M 5 10.25 C 4.584 10.25 4.25 9.916 4.25 9.5 C 4.25 9.084 4.584 8.75 5 8.75 C 5.416 8.75 5.75 9.084 5.75 9.5 C 5.75 9.916 5.416 10.25 5 10.25 Z  M 5 7.25 C 4.584 7.25 4.25 6.916 4.25 6.5 C 4.25 6.084 4.584 5.75 5 5.75 C 5.416 5.75 5.75 6.084 5.75 6.5 C 5.75 6.916 5.416 7.25 5 7.25 Z  M 8 1.25 C 8.416 1.25 8.75 1.584 8.75 2 C 8.75 2.416 8.416 2.75 8 2.75 C 7.584 2.75 7.25 2.416 7.25 2 C 7.25 1.584 7.584 1.25 8 1.25 Z  M 12 12.75 C 12 12.888 11.888 13 11.75 13 L 7.25 13 C 7.113 13 7 12.888 7 12.75 L 7 12.25 C 7 12.113 7.113 12 7.25 12 L 11.75 12 C 11.888 12 12 12.113 12 12.25 L 12 12.75 Z  M 12 9.75 C 12 9.888 11.888 10 11.75 10 L 7.25 10 C 7.113 10 7 9.888 7 9.75 L 7 9.25 C 7 9.113 7.113 9 7.25 9 L 11.75 9 C 11.888 9 12 9.113 12 9.25 L 12 9.75 Z  M 12 6.75 C 12 6.888 11.888 7 11.75 7 L 7.25 7 C 7.113 7 7 6.888 7 6.75 L 7 6.25 C 7 6.113 7.113 6 7.25 6 L 11.75 6 C 11.888 6 12 6.113 12 6.25 L 12 6.75 Z "
                                                fill="rgb(255,255,255)" />
                                        </g>
                                    </svg>
                                    <span class="pl-3 pt-1">LPO</span>
                                </a>
                            </li>
                            <li class="nav-item side-bar">
                                <a class="nav-link py-3 d-flex side-bar" href="#"  data-toggle="modal" data-target=".contact-manager-modal">
                                    <svg class="align-self-center" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate"
                                        viewBox="0 0 16 16" width="16" height="16">
                                        <defs>
                                            <clipPath id="_clipPath_ylcDmd5ZoEsNwJ1dX1NK3Owqr9VYumO3">
                                                <rect width="16" height="16" />
                                            </clipPath>
                                        </defs>
                                        <g clip-path="url(#_clipPath_ylcDmd5ZoEsNwJ1dX1NK3Owqr9VYumO3)">
                                            <clipPath id="_clipPath_jybidIyl9sbCYQiqFCpvmTBTi0QcY7ZK">
                                                <rect x="0" y="0" width="16" height="16" transform="matrix(1,0,0,1,0,0)" fill="rgb(255,255,255)"
                                                />
                                            </clipPath>
                                            <g clip-path="url(#_clipPath_jybidIyl9sbCYQiqFCpvmTBTi0QcY7ZK)">
                                                <g id="Group">
                                                    <clipPath id="_clipPath_jn2HGg2PLCD3bkFmpf6y639CxdxOIvAL">
                                                        <rect x="0" y="0" width="16" height="16" transform="matrix(1,0,0,1,0,0)"
                                                            fill="rgb(255,255,255)" />
                                                    </clipPath>
                                                    <g clip-path="url(#_clipPath_jn2HGg2PLCD3bkFmpf6y639CxdxOIvAL)">
                                                        <g id="Group">
                                                            <g id="Group">
                                                                <path d=" M 7.246 0.299 C 1.519 0.299 1.227 4.068 1.207 7.066 C 0.5 7.331 0.022 7.997 0 8.754 C 0 9.716 0.723 11.169 1.812 11.169 C 1.905 11.169 2.004 11.16 2.104 11.15 C 2.991 13.9 5.181 16 7.246 16 C 8.338 15.937 9.368 15.473 10.139 14.698 C 9.781 14.758 9.42 14.789 9.058 14.793 L 7.246 14.793 C 5.826 14.793 3.843 13.033 3.152 10.414 L 2.953 9.726 L 2.388 9.868 C 2.2 9.922 2.007 9.953 1.812 9.961 C 1.618 9.937 1.207 9.249 1.207 8.754 C 1.246 8.423 1.517 8.168 1.849 8.149 L 2.416 8.112 L 2.416 7.546 C 2.416 3.789 2.742 1.507 7.247 1.507 L 10.2 1.516 L 9.788 2.354 C 9.549 2.858 9.188 3.293 8.738 3.623 C 7.851 4.363 6.312 5.132 3.623 5.132 L 3.623 6.34 C 6.548 6.34 8.403 5.467 9.511 4.556 C 9.975 4.176 10.372 3.721 10.685 3.21 C 11.926 3.843 12.077 5.098 12.077 7.546 L 12.077 10.523 C 11.94 12.042 9.857 12.377 9.057 12.377 L 8.453 12.377 C 8.453 11.71 7.913 11.169 7.245 11.169 C 6.579 11.169 6.038 11.71 6.038 12.377 C 6.038 13.044 6.579 13.584 7.245 13.584 L 9.058 13.584 C 10.723 13.584 12.557 12.876 13.119 11.32 L 13.889 11.32 C 14.887 11.316 15.697 10.507 15.7 9.508 L 15.7 8.149 C 15.697 7.151 14.887 6.342 13.889 6.339 L 13.26 6.339 C 13.2 4.606 12.903 2.98 11.245 2.13 L 12.143 0.319 L 11.171 0.314 L 7.246 0.299 Z  M 5.432 7.546 C 5.099 7.546 4.828 7.815 4.828 8.149 C 4.828 8.483 5.099 8.753 5.432 8.753 C 5.765 8.753 6.036 8.483 6.036 8.149 C 6.036 7.988 5.972 7.836 5.859 7.722 C 5.746 7.609 5.592 7.546 5.432 7.546 L 5.432 7.546 Z  M 9.058 7.546 C 8.724 7.546 8.454 7.815 8.454 8.149 C 8.454 8.483 8.724 8.753 9.058 8.753 C 9.391 8.753 9.661 8.483 9.661 8.149 C 9.661 7.988 9.597 7.836 9.484 7.722 C 9.372 7.609 9.218 7.546 9.058 7.546 L 9.058 7.546 Z  M 13.285 7.546 L 13.889 7.546 C 14.05 7.543 14.204 7.606 14.317 7.72 C 14.431 7.834 14.495 7.988 14.492 8.149 L 14.492 9.508 C 14.495 9.668 14.431 9.823 14.317 9.937 C 14.204 10.051 14.05 10.114 13.889 10.111 L 13.285 10.111 L 13.285 7.546 Z "
                                                                    fill="rgb(255,255,255)" />
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    <span class="pl-3 pt-1">Contact Manager</span>
                                </a>
                            </li>
                            <li class="nav-item svg side-bar">
                                <a class="nav-link py-3 d-flex side-bar" href="<?php echo base_url('logout'); ?>">
                                    <svg class="align-self-center" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate"
                                        viewBox="0 0 16 16" width="16" height="16">
                                        <defs>
                                            <clipPath id="_clipPath_gfr5cR1LVzlqxuecPlYfZos0lBRNS3tQ">
                                                <rect width="16" height="16" />
                                            </clipPath>
                                        </defs>
                                        <g clip-path="url(#_clipPath_gfr5cR1LVzlqxuecPlYfZos0lBRNS3tQ)">
                                            <path d=" M 15.779 8.665 L 10.445 13.999 C 9.969 14.475 9.143 14.142 9.143 13.459 L 9.143 10.411 L 4.826 10.411 C 4.403 10.411 4.064 10.071 4.064 9.649 L 4.064 6.601 C 4.064 6.179 4.403 5.84 4.826 5.84 L 9.143 5.84 L 9.143 2.792 C 9.143 2.112 9.966 1.776 10.445 2.252 L 15.779 7.586 C 16.074 7.884 16.074 8.367 15.779 8.665 Z  M 6.096 13.84 L 6.096 12.57 C 6.096 12.36 5.924 12.189 5.715 12.189 L 3.048 12.189 C 2.486 12.189 2.032 11.735 2.032 11.173 L 2.032 5.078 C 2.032 4.516 2.486 4.062 3.048 4.062 L 5.715 4.062 C 5.924 4.062 6.096 3.89 6.096 3.681 L 6.096 2.411 C 6.096 2.201 5.924 2.03 5.715 2.03 L 3.048 2.03 C 1.365 2.03 0 3.395 0 5.078 L 0 11.173 C 0 12.856 1.365 14.221 3.048 14.221 L 5.715 14.221 C 5.924 14.221 6.096 14.049 6.096 13.84 Z "
                                                fill="rgb(255,255,255)" />
                                        </g>
                                    </svg>
                                    <span class="pl-3 pt-1">Logout</span>
                                </a>
                            </li>
                        </ul>
                    </aside>
