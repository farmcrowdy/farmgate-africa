
<!doctype html>
<html lang="en">

<head>
    <title>Farmgate-Dashboard</title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url();?>mainasset/fe/images/favicon.ico/ms-icon-144x144.png">
    <meta name="theme-color" content="#08BD51">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>mainasset/fe/css/animation.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>mainasset/fe/css/fonts.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>mainasset/fe/css/lity.css">
    <link rel="stylesheet" href="<?php echo base_url();?>mainasset/fe/css/jquery-ui.css">
    <!-- Add jQuery library -->
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
        crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url();?>mainasset/fe/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url();?>mainasset/fe/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>mainasset/fe/css/owl.theme.default.min.css">
    <script src="<?php echo base_url();?>mainasset/fe/js/jquery.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- <link rel="stylesheet" href="css/jquery-ui.css"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>mainasset/fe/css/index.css">
    <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
    <style>
        .input-group>.custom-select:not(:last-child),
        .input-group>.form-control:not(:last-child) {
            border: none !important;
        }
    </style>
</head>

<body>
    <!-- ==============Top nav========== -->



    <!-- ==================end of top nav============== -->
    <!-- ===============navbar=========== -->

    <nav class="header nav-pad  navbar nav-fixed after-login">
        <a class="navbar-brand" href="<?php echo base_url();?>">
            <img src="<?php echo base_url();?>mainasset/fe/images/farmgate-logo.svg" class="img-fluid pl-sm-2 pl-md-5">
        </a>
        <input class="menu-btn" type="checkbox" id="menu-btn" />
        <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>
        <ul class="menu margin-left top-nav p-3 d-none d-lg-block " id="navlist" >
            <li class="small-screen-profile d-lg-none pt-4">
                <div>
                    <img src="http://via.placeholder.com/50x50" alt="profile image">
                    <strong class="small-screen-user-name "><?php echo $user->full_name; ?></strong>
                    <span class="small-screen-company-name d-block"><?php echo $user->coy_name; ?></span>
                    <a class="small-screen-logout " id="small-screen-logout" href="<?php echo base_url('logout'); ?>">Logout</a>
                </div>

            </li>
            <li class="nav-item ">
                <a class="nav-link order-pg" href="<?php echo base_url('users/products');?>">
                    <img src="<?php echo base_url();?>mainasset/fe/images/menu.svg" alt="product icon" class="" style="height: 15px; width:15px;"> &nbsp;My Products
                    <span class="sr-only">(current)</span>
                </a>
            </li>

            <li class="nav-item no-profile-drop-border dropdown profileDropdown">
                <a class="nav-link btn order-pg dropdown-toggle" role="button" id="profileDropdown" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false" href="<?php echo base_url('users/profile');?>">
                    <img src="<?php echo base_url();?>mainasset/fe/images/profile.svg" class="" alt="profile icon" style="height: 15px; width:15px;"> &nbsp;Profile
                </a>
                <div class="dropdown-menu pt-0" aria-labelledby="profileDropdown">
                    <div class="user-dropdown-img dropdown-item d-none d-lg-block">
                        <img src="<?php echo base_url();?>mainasset/fe/images/users/<?php echo $user->image; ?>" alt="<?php echo $user->full_name; ?>">
                        <strong class="user-name "><?php echo $user->full_name; ?></strong>
                        <span class="company-name d-block"><?php echo $user->coy_name; ?></span>
                    </div>
                    <a class="dropdown-item" href="<?php echo base_url('users/profile'); ?>">View Profile</a>
                    <a class="dropdown-item" href="<?php echo base_url('users/editprofile'); ?>">Account Information</a>
                    <a class="dropdown-item" href="<?php echo base_url('users/editprofile'); ?>">Company Information</a>
                    <a class="dropdown-item" href="<?php echo base_url('users/editprofile'); ?>">Get Verified</a>
                    <div class="dropdown-divider d-none d-lg-block"></div>
                    <a class="dropdown-item d-none d-lg-block" href="<?php echo base_url('logout'); ?>">Logout</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link order-pg no-modal-bg" href="#" data-toggle="modal" data-target=".contact-manager-modal">
                    <img src="<?php echo base_url();?>mainasset/fe/images/contact-manager.svg" class="" alt="contact manager's icon" style="height: 15px; width:15px;"> &nbsp;Contact Manager
                </a>
            </li>
            <li class="nav-item  main-login-link">
                <a class="nav-link loginClass order-pg product " href="<?php echo base_url('users/addproduct'); ?>">Add Product</a>
            </li>
        </ul>
    </nav>    <!-- ===============end of navbar=========== -->
