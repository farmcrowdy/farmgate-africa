

                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-12 my-4 ">
                                <div class="row ">
                                    <div class="col-md-8 col-12 pad-dashboard">
                                        <p class="order-title">Profile</p>
                                    </div>
                                    <div class="col-5 col-sm-4 d-md-none">
                                        <div class="row">
                                            <div class="col-10 p-0 m-0 dashboard  text-center">
                                                <!-- <i class="fas fa-chalkboard-teacher d-block"></i> -->
                                                <span class="dashboard-close  m-0 text-left">Dashboard &nbsp; <i class="fas fa-angle-right pl-1"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12  ">
                                       <div class="row pad-dashboard pad-dashboard-profile">
                                           <div class="col-12"  style="background: #fafafa;">
                                                <div class="row py-4 ">
                                                    <div class="col-lg-4 col-md-10 offset-md-1 offset-lg-0 profile-border mb-4 mb-lg-1">
                                                        <div class="row">
                                                            <div class="col-12 d-flex justify-content-center">
                                                                <img src="<?php echo base_url();?>mainasset/fe/images/users/<?php echo $user->image; ?>" class="img-fluid user-profile-pic" alt="profile picture">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 text-center">
                                                                <p class="profile-name pt-2"><?php echo $user->full_name; ?></p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 text-center">
                                                                <span class="profile-status profile-status-buyer mb-2">Buyer</span>
                                                                <span class="profile-status profile-status-seller mb-2">Seller</span>
                                                                <span class="profile-status profile-status-flagged mb-2">Flagged</span>
                                                                <!-- <span class="profile-status profile-status-pending mb-2">Pending</span> -->
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 text-center profile-edit mt-4">
                                                                <span><a href="<?php echo base_url('users/editprofile'); ?>" > Edit Profile</a></span>
                                                                <!-- Change Password trigger modal -->
                                                                <span><a href="#"  data-toggle="modal" data-target="#changePasswordModal"> Change Password</a></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Change Password Modal -->
                                                    <div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="changePasswordLabel" aria-hidden="true">
                                                        <div class="modal-dialog  modal-dialog-centered" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="changePasswordLabel">Change Password</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body container">
                                                                <center style="color: #08BD51;"><div id="cp_feedback"></div></center>
                                                                    <form id="changePasswordForm">
                                                                    <div class="row py-2">
                                                                        <div class="col-12 col-md-10 offset-md-1">
                                                                            <label for="">Old Password <span class="superscript-star">*</span></label>
                                                                            <input type="password" name="current_password" class="form-control" id="current_password" aria-describedby="Volume" placeholder="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row py-2">
                                                                        <div class="col-12 col-md-10 offset-md-1">
                                                                            <label for="">New Password <span class="superscript-star">*</span></label>
                                                                            <input type="password" name="new_password" class="form-control" id="new_password" aria-describedby="Volume" placeholder="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row py-2">
                                                                        <div class="col-12 col-md-10 offset-md-1">
                                                                            <label for="">Confirm New Password <span class="superscript-star">*</span></label>
                                                                            <input type="password" name="new_password_confirm" class="form-control" id="new_password_confirm" aria-describedby="Volume" placeholder="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer d-flex justify-content-center">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                <button type="submit" name="reset_pass_submit" id="reset_pass_submit" class="btn btn-profile">Submit</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- End Change Password -->
                                                    <div class="col-lg-8 offset-lg-0 col-md-10 offset-md-1">
                                                        <div class="col-12">
                                                            <nav>
                                                                <div class="nav nav-tabs profile-tab-nav" id="nav-tab" role="tablist">
                                                                    <a class="nav-item nav-link active" id="nav-account-info-tab" data-toggle="tab" href="#nav-account-info" role="tab" aria-controls="nav-account-info" aria-selected="true"><span class="profile-break">Account</span> Information</a>
                                                                    <a class="nav-item nav-link " id="nav-company-info-tab" data-toggle="tab" href="#nav-company-info" role="tab" aria-controls="nav-company-info" aria-selected="false"><span class="profile-break">Company</span> Information</a>
                                                                    <a class="nav-item nav-link " id="nav-bank-details-tab" data-toggle="tab" href="#nav-bank-details" role="tab" aria-controls="nav-bank-details" aria-selected="false"><span class="profile-break">Bank</span> Details</a>
                                                                </div>
                                                            </nav>
                                                            <div class="tab-content pt-4" id="nav-tabContent">
                                                                <div class="tab-pane fade show active" id="nav-account-info" role="tabpanel" aria-labelledby="nav-account-info-tab">
                                                                    <p class="profile-view-title pb-0 mb-0">Email address</p>
                                                                    <p class="profile-view-body"><?php echo $user->email; ?></p>
                                                                    <p class="profile-view-title pb-0 mb-0">Phone</p>
                                                                    <p class="profile-view-body"><?php echo $user->phone; ?></p>
                                                                    <p class="profile-view-title pb-0 mb-0">Address</p>
                                                                    <p class="profile-view-body"><?php echo $user->coy_legal_add; ?></p>
                                                                </div>
                                                                <div class="tab-pane fade" id="nav-company-info" role="tabpanel" aria-labelledby="nav-company-info-tab">
                                                                        <p class="profile-view-title pb-0 mb-0">Company Name</p>
                                                                        <p class="profile-view-body"><?php echo $user->coy_name; ?></p>
                                                                        <p class="profile-view-title pb-0 mb-0">Country</p>
                                                                        <p class="profile-view-body">Nigeria</p>
                                                                        <p class="profile-view-title pb-0 mb-0">Address</p>
                                                                        <p class="profile-view-body"><?php echo $user->coy_legal_add; ?></p>
                                                                </div>
                                                                <div class="tab-pane fade" id="nav-bank-details" role="tabpanel" aria-labelledby="nav-bank-details-tab">
                                                                        <p class="profile-view-title pb-0 mb-0">Account Name</p>
                                                                        <p class="profile-view-body"><?php echo $user->full_name; ?></p>
                                                                        <p class="profile-view-title pb-0 mb-0">Bank Name</p>
                                                                        <p class="profile-view-body">GtBank</p>
                                                                        <p class="profile-view-title pb-0 mb-0">Account Number</p>
                                                                        <p class="profile-view-body">099763536375</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                               </div>
                                           </div>
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
