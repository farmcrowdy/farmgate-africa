<style>
        label, .sample-label{margin-bottom: 10px; margin-top: 15px;}
        input { margin-bottom: 10px;}
</style>
<div class="col-md-10">
    <div class="row px-3">
        <div class="col-12 col-lg-10 offset-lg-1 pt-5">
            <div class="row">
                <div class="col-12 border-bottom">
                    <h4 class="sample sample-title">LPO Form</h4>
                </div>
                <?php echo $this->session->flashdata('lpo_feedback'); ?>
            </div>
            <?php echo form_open('users/lpo', array('class' => 'form')); ?>
                <div class="col-md-12  mt-2">
                    <label for="">Nature of Purchase <span class="superscript-star">*</span></label>
                    <select class="form-control" id="formControlSelect">
                        <option>Nigeria</option>
                        <option>International</option>
                    </select>
                </div>
                <div class="col-md-12 ">
                    <label for="product_name"> Product Name <span class="superscript-star">*</span></label>
                    <?php echo form_error('product_name'); ?>
                    <input class="form-control" name="product_name" id="product_name" value="<?php echo set_value('product_name'); ?>" aria-label="With textarea">
                </div>
                <div class=" col-md-12 top top1">
                    <label for="product_description">Product Description <span class="superscript-star">*</span></label>
                    <?php echo form_error('product_description'); ?>
                    <textarea style="margin-bottom: 30px;" class="form-control" name="product_description" id="product_description" rows="5" aria-label="With textarea"><?php echo set_value('product_description'); ?></textarea>
                </div>
                <div class=" col-md-12 top">
                    <div class="row">
                        <div class="col-8 col-md-10">
                            <label for="" class=" pt-0 mt-0">Required Quantity for Purchase in MT<span class="superscript-star">*</span></label>
                            <?php echo form_error('quantity'); ?>
                            <input type="number" name="quantity" class="form-control" id="quantity" min="<?php // echo $product->prod_min_order; ?>" aria-describedby="quantity" placeholder="Enter quantity" value="<?php echo set_value('quantity'); ?>">
                        </div>
                    </div> 
                </div>
                <div class="col-md-12 top top1">
                    <label for="">Delivery Address <span class="superscript-star">*</span></label>
                    <?php echo form_error('delivery_add'); ?>
                    <textarea style="margin-bottom: 30px;" class="form-control" name="delivery_add" id="delivery_add" rows="5" aria-label="With textarea"><?php echo set_value('delivery_add'); ?></textarea>
                </div>
                <div class="col-md-12 top">
                    <p class="sample-label">Product Sample Delivery Cost *</p>
                    <p class="sample sample-total-price mb-0">$359.00</p>
                    <p class="sample-small unit mb-0">* Only 30% of the Sample delievry cost is refundable in a case</p>
                    <div class="form-group pl-1">
                        <?php echo form_error('sample_terms'); ?>
                        <input style="margin-top: 1.2em;" type="checkbox" name="sample_terms" class="form-check-input move-check-down" 
                        id="sample_terms" value="1" <?php echo set_checkbox('sample_terms', '1'); ?>>
                        <label class="form-check-label ml-3 sample-small unit" for="terms">I agree to the Terms of Service</label>
                    </div>
                </div>
                <div class="col-12 text-center my-3" >
                    <button type="submit" class="btn btn-submit" name="lpo_submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>