    <!-- ============body================= -->
    <div class="container-fluid body-bg body-bg-login" id="auth-bg">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12 login-bg">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">
                                <div class="p-5">
                                    <h5 class="text-center login-color">Password Reset</h5>
                                    <p class="text-center login-title">Enter your email and reset instructions will be sent to you</p>
                                    <?php echo form_open('users/forgotpassword'); ?>
                                   <?php echo form_error('rec_email'); ?><?php if(!empty($feedback)) {echo $feedback;} ?>
                                    <form>
                                        <div class="input-group mb-3 login-border">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="far fa-envelope"></i>
                                                </span>
                                            </div>
                                            <input type="email" class="form-control" name="rec_email" aria-label="Email" aria-describedby="basic-addon1"
                                            value="<?php echo set_value('rec_email'); ?>">
                                        </div>

                                        <button type="submit" name="recover_submit" class="btn login-btn-red">Submit</button>
                                        <div class="row">
                                            <div class="col-12 text-center pt-2">
                                                <span>Back to<a href="<?php echo base_url();?>users/login" class="link-color login-forgot-pass"> login</a></span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- <div class="container-fluid login-top">
        <div class="row footer">
            <div class="col-lg-10 offset-lg-1 mt-4">
                <div class="row">
                    <div class="col-md-6">
                        <p>Subscribe to receive news first</p>
                        <form class="row">
                            <div class="form-group col-lg-9 col-sm-8 email-input">
                                <input type="email" class="form-control" id="email" placeholder="Email">
                            </div>
                            <div class="col-lg-3 col-sm-4 submit-email">
                                <button type="submit" class="btn ">SUBSCRIBE</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3 col-6 mt-3">
                        <ul class="footer-list">
                            <li>
                                <a href="#">Home</a>
                            </li>
                            <li>
                                <a href="#">About</a>
                            </li>
                            <li>
                                <a href="#">How it works</a>
                            </li>
                            <li>
                                <a href="#">Compliance</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-6  mt-3">
                        <ul class="footer-list">
                            <li>
                                <a href="#">Terms of Service</a>
                            </li>
                            <li>
                                <a href="#">Privacy Policy</a>
                            </li>
                            <li>
                                <a href="#">Support</a>
                            </li>
                            <li>
                                <a href="#">FAQ</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="background:#293737;">
            <div class="col-lg-10 offset-lg-1 my-2">
                <p class="copyrite mb-0"> &copy; 2018 Farmcrowdy - All rights reserved</p>
            </div>
        </div>
    </div> -->
    <!-- ============end of body================= -->


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <script src="<?php echo base_url();?>mainasset/fe/js/owl.carousel.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            //  carousel center present
            // $('.loop').owlCarousel({
            //     center: true,
            //     items:2,
            //     loop:true,
            //     margin:10,
            //     responsive:{
            //         600:{
            //             items:4
            //         }
            //     }
            // });
            $('.animated-icon3').click(function () {
                $(this).toggleClass('open');
            });
            $('#btn-menu').click(function () {
                $('#btn-menu').toggleClass("nav-button");
            });
            $('.owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1,
                        nav: true
                    },
                    600: {
                        items: 3,
                        nav: false
                    },
                    1000: {
                        items: 4,
                        nav: true,
                        loop: false
                    }
                }
            })
            // add event listener (next and prev btns)
            var owl = $('.owl-carousel');
            owl.owlCarousel();
            // Go to the next item
            $('.owl-next').click(function () {
                owl.trigger('next.owl.carousel');
            })
            // Go to the previous item
            $('.owl-prev').click(function () {
                // With optional speed parameter
                // Parameters has to be in square bracket '[]'
                owl.trigger('prev.owl.carousel', [300]);
            })
        })
    </script>

    <!-- <script type="text/javascript" src="js/fontawesome-all.js"></script> -->
</body>

</html>
