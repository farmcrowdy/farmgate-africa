<div class="container-fluid">
        <div class="row no-pad page-title-bg page-title-bg1">
            <div class="col-12">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mb-5 no-pad login">
            <div class="col-lg-4 col-10 offset-1 col-md-6 mt-5 mb-4 offset-md-3 offset-lg-4 text-center ">
                <h4 class="mobile">Recover Lost Password</h4>
                <span>Please enter your username or email address. You will receive a link to create a new password via email.</span>
            </div>
            <div class="col-lg-4 col-sm-10 col-12 offset-sm-1 col-md-6 offset-md-3 pt-5 pb-4 offset-lg-4 login-bg">
            <?php echo form_error('rec_email'); ?><?php if(!empty($feedback)) {echo $feedback;} ?>

                <form class="px-2" action="forgotpassword" method="post">
                    <div class="form-group">
                        <input type="email" class="form-control" id="exampleInputEmail1" name="rec_email" aria-describedby="emailHelp"
                            placeholder="Email " value="<?php echo set_value('rec_email'); ?>">
                    </div>
                    <button type="submit" name="recover_submit" class="btn login">Reset Password</button>
                </form>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <script>
        // w3.includeHTML();
        // ScrollReveal().reveal('.more-products', {
        //     duration: 2000
        // });
        // ScrollReveal().reveal('.products', {
        //     interval: 500
        // });
    </script>