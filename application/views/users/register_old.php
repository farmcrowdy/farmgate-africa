    <!-- Contact manager modal -->
    <div class="modal fade contact-manager-modal mt-5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg  ">
            <form class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title pl-md-5 ml-md-3" id="changePasswordLabel">Contact Manager</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body container mt-3">
                    <div class="row py-2">
                        <div class="col-12 col-md-10 offset-md-1">
                            <label for="">Subject</label>
                            <input type="text" class="form-control" id="" aria-describedby="Volume" placeholder="">
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-12 col-md-10 offset-md-1">
                            <label for="">Category</label>
                            <select class="form-control" id="formControlSelect">
                                <option>Client</option>
                                <option>General</option>
                                <option>Issues</option>
                                <option>Suggestions</option>
                            </select>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-12 col-md-10 offset-md-1">
                            <label for="">Describe the question in detail</label>
                            <textarea class="form-control" rows="5" aria-label="With textarea"></textarea>

                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-12 col-md-10 offset-md-1">
                            <p class="profile-file-paceholder mb-0 pb-0 pl-3 dashboard-activity-body"></p>
                            <label for="upload-file-for-manager" class="btn btn-default manager-verify-link" style="color:#888888">
                                <i class="fas fa-upload"></i> Add files</label>
                            <input id="upload-file-for-manager" name="pic" accept="" type="file" class="hidden">
                        </div>
                        <script>
                            $("#upload-file-for-manager").change(function () {
                                $(".profile-file-paceholder").text(this.files[0].name);
                            });
                        </script>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-secondary px-4 py-2" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-profile py-2 px-4">Submit</button>
                </div>
            </form>
        </div>
    </div>
    <!-- ============body================= -->
    <div class="container-fluid body-bg  body-bg-login" id="auth-bg">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class=" login-bg p-0">
                        <div class="card border-0 w-100">
                            <h5 class="card-header auth text-center">Register</h5>
                            <div class="card-body">
                                <p class="text-center  login-title">Create an account to get started</p>
                                <form action="register" method="post">
                                  <?php echo form_error('reg_fullname'); ?>
                                  <?php echo form_error('reg_email'); ?>
                                  <?php echo form_error('reg_phone'); ?>
                                  <?php echo form_error('reg_password'); ?>
                                  <?php echo form_error('reg_password_conf'); ?>
                                  <?php echo form_error('reg_terms'); ?>
                                    <div class="input-group mb-3 login-border">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="fa fa-user"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" name="reg_fullname"  value="<?php echo set_value('reg_fullname'); ?>" aria-label="Fullname" aria-describedby="basic-addon1">
                                    </div>
                                    <div class="input-group mb-3 login-border">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="far fa-envelope"></i>
                                            </span>
                                        </div>
                                        <input type="email" class="form-control" name="reg_email"  value="<?php echo set_value('reg_email'); ?>" aria-label="Email" aria-describedby="basic-addon1">
                                    </div>
                                    <div class="input-group mb-3 login-border">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="fas fa-phone"></i>
                                            </span>
                                        </div>
                                        <input type="tel" class="form-control" name="reg_phone"  value="<?php echo set_value('reg_phone'); ?>" aria-label="Phone" aria-describedby="basic-addon1">
                                    </div>
                                    <div class="input-group mb-3 login-border">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate" viewBox="140 202 16 21"
                                                    width="16" height="21">
                                                    <path d=" M 148 202 C 144.676 202 142 204.676 142 208 L 142 209 C 140.898 209 140 209.898 140 211 L 140 221 C 140 222.102 140.898 223 142 223 L 154 223 C 155.102 223 156 222.102 156 221 L 156 211 C 156 209.898 155.102 209 154 209 L 154 208 C 154 204.676 151.324 202 148 202 Z  M 148 204 C 150.277 204 152 205.723 152 208 L 152 209 L 144 209 L 144 208 C 144 205.723 145.723 204 148 204 Z  M 148 214 C 149.102 214 150 214.898 150 216 C 150 217.102 149.102 218 148 218 C 146.898 218 146 217.102 146 216 C 146 214.898 146.898 214 148 214 Z "
                                                        fill="rgb(0,0,0)" />
                                                </svg>
                                            </span>
                                        </div>
                                        <input type="password" class="form-control" id="reg_password" name="reg_password"  value="<?php echo set_value('reg_password'); ?>" aria-label="Password" aria-describedby="basic-addon1">
                                        
                                        <!-- An element to toggle between password visibility -->
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2"><i onclick="togglePassword('reg_password');" class="fas fa-eye"></i></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 form-group form-check">
                                            <input type="checkbox" class="form-check-input" name="reg_terms"  value="1" <?php echo set_checkbox('reg_terms'); ?> id="exampleCheck1">
                                            <label class="form-check-label ml-4 link-color1" for="">I agree with the Terms &amp; Conditions.</label>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn login-btn" name="register_submit" >Submit</button>
                                    <p class="pt-4 text-center" style="color:#666666;">Already have an account? &nbsp;
                                        <a href="<?php echo base_url();?>users/login" class="login-forgot-pass">Login</a>
                                    </p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
// toggle password field type given string , id
function togglePassword(id) {
    var x = document.getElementById(id);
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
</script>

    <!-- <div class="container-fluid login-top register-login-top">
        <div class="row footer">
            <div class="col-lg-10 offset-lg-1 mt-4">
                <div class="row">
                    <div class="col-md-6">
                        <p>Subscribe to receive news first</p>
                        <form class="row">
                            <div class="form-group col-lg-9 col-sm-8 email-input">
                                <input type="email" class="form-control" id="email" placeholder="Email">
                            </div>
                            <div class="col-lg-3 col-sm-4 submit-email">
                                <button type="submit" class="btn ">SUBSCRIBE</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3 col-6 mt-3">
                        <ul class="footer-list">
                            <li>
                                <a href="#">Home</a>
                            </li>
                            <li>
                                <a href="#">About</a>
                            </li>
                            <li>
                                <a href="#">How it works</a>
                            </li>
                            <li>
                                <a href="#">Compliance</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-6  mt-3">
                        <ul class="footer-list">
                            <li>
                                <a href="#">Terms of Service</a>
                            </li>
                            <li>
                                <a href="#">Privacy Policy</a>
                            </li>
                            <li>
                                <a href="#">Support</a>
                            </li>
                            <li>
                                <a href="#">FAQ</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="background:#293737;">
            <div class="col-lg-10 offset-lg-1 my-2">
                <p class="copyrite mb-0"> &copy; 2018 Farmcrowdy - All rights reserved</p>
            </div>
        </div>
    </div> -->
    <!-- ============end of body================= -->


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <script src="<?php echo base_url();?>mainasset/fe/js/owl.carousel.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            //  carousel center present
            // $('.loop').owlCarousel({
            //     center: true,
            //     items:2,
            //     loop:true,
            //     margin:10,
            //     responsive:{
            //         600:{
            //             items:4
            //         }
            //     }
            // });
            $('.animated-icon3').click(function () {
                $(this).toggleClass('open');
            });
            $('#btn-menu').click(function () {
                $('#btn-menu').toggleClass("nav-button");
            });
            $('.owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1,
                        nav: true
                    },
                    600: {
                        items: 3,
                        nav: false
                    },
                    1000: {
                        items: 4,
                        nav: true,
                        loop: false
                    }
                }
            })
            // add event listener (next and prev btns)
            var owl = $('.owl-carousel');
            owl.owlCarousel();
            // Go to the next item
            $('.owl-next').click(function () {
                owl.trigger('next.owl.carousel');
            })
            // Go to the previous item
            $('.owl-prev').click(function () {
                // With optional speed parameter
                // Parameters has to be in square bracket '[]'
                owl.trigger('prev.owl.carousel', [300]);
            })
        })
    </script>

</body>

</html>
