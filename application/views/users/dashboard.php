<div class="col-md-10 mb-5">
    <div class="row no-pad">
        <div class="col-12 my-4 ">
            <div class="row no-pad ">
                <div class="col-md-8 col-12 pad-dashboard">
                    <p class="order-title">Dashboard</p>
                </div>
                <div class="col-5 col-sm-4 d-md-none">
                    <div class="row no-pad">
                        <div class="col-10 p-0 m-0 dashboard  text-center">
                            <!-- <i class="fas fa-chalkboard-teacher d-block"></i> -->
                            <span class="dashboard-close  m-0 text-left">Dashboard &nbsp;
                                <i class="fas fa-angle-right pl-1"></i>
                            </span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="row no-pad">
        <div class="col-12 pad-dashboard border-bottom my-2">
            <p class="order-title">Welcome back,
                <span style="color:#888888;">
                    <?php echo $user->full_name; ?></span>
            </p>
            <!-- <p class="dashboard-message">
                                    <span class="dashboard-message-sub pt-1">You have</span>
                                    (
                                    <span>0</span>) unread messages

                                </p> -->
        </div>
    </div>
    <div class="row no-pad pad-dashboard-card pad-order border-bottom py-4">
        <div class="col-lg-4 col-md-6 pb-3 px-sm-5 px-md-2">
            <a href="<?php echo base_url('users/investments'); ?>" class="dashboard-main-link">
                <div class=" dashboard-card dashboard-card1 h-100">
                    <div class="d-flex justify-content-between p-md-4  px-4 py-3">
                        <div>
                            <p class="mb-0 dashboard-no"><?php if(!empty($total_investments)){ echo number_format($total_investments); }else{ echo "0"; } ?></p>
                            <p class="mb-0 dashboard-sub-text">Investments</p>
                        </div>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate" viewBox="0 0 70 70"
                            width="70" height="70">
                            <defs>
                                <clipPath id="_clipPath_SOpfSq23ctXwVIL2wsTnQgpzOA3jExND">
                                    <rect width="70" height="70" />
                                </clipPath>
                            </defs>
                            <g clip-path="url(#_clipPath_SOpfSq23ctXwVIL2wsTnQgpzOA3jExND)">
                                <path d=" M 59.704 1.375 C 59.413 1.133 59.046 1 58.667 1 L 37.619 1 L 37.619 9.096 L 68.967 9.096 L 59.704 1.375 Z  M 13.331 1 C 12.953 1 12.586 1.133 12.295 1.375 L 3.031 9.096 L 34.381 9.096 L 34.381 1 L 13.331 1 Z  M 35.998 12.334 L 2 12.334 L 2 67.381 C 2 67.811 2.171 68.222 2.474 68.526 C 2.778 68.83 3.19 69 3.619 69 L 68.381 69 C 68.81 69 69.222 68.83 69.526 68.526 C 69.829 68.222 70 67.811 70 67.381 L 70 12.334 L 35.998 12.334 Z  M 50.569 49.572 L 47.331 49.572 L 47.331 56.048 L 44.093 56.048 L 44.093 49.572 L 40.855 49.572 L 45.712 44.715 L 50.569 49.572 Z  M 27.903 22.857 C 27.935 21.53 29.004 20.461 30.331 20.429 L 41.665 20.429 C 43.006 20.429 44.093 21.516 44.093 22.857 C 44.093 24.199 43.006 25.286 41.665 25.286 L 30.333 25.286 C 29.006 25.254 27.937 24.185 27.905 22.857 L 27.903 22.857 Z  M 61.903 60.905 L 40.857 60.905 L 40.857 57.667 L 61.905 57.667 L 61.903 60.905 Z  M 58.665 49.572 L 58.665 56.048 L 55.427 56.048 L 55.427 49.572 L 52.188 49.572 L 57.046 44.715 L 61.903 49.572 L 58.665 49.572 Z "
                                    fill="rgb(255,255,255)" />
                            </g>
                        </svg>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-4 col-md-6  pb-3 px-sm-5 px-md-2">
            <a href="<?php echo base_url('users/purchases'); ?>" class="dashboard-main-link">
                <div class=" dashboard-card dashboard-card2 h-100">
                    <div class="d-flex justify-content-between  p-md-4 px-4 py-3">
                        <div class="align-self-center">
                            <p class="mb-0 dashboard-no"><?php if(!empty($total_purchases)){ echo number_format($total_purchases); }else{ echo "0"; } ?></p>
                            <p class="mb-0 dashboard-sub-text">Purchases</p>
                        </div>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate" viewBox="0 0 70 70"
                            width="70" height="70">
                            <defs>
                                <clipPath id="_clipPath_1bQ7H8XXzLLZj4uxfvdSYUE1EZN0SstV">
                                    <rect width="70" height="70" />
                                </clipPath>
                            </defs>
                            <g clip-path="url(#_clipPath_1bQ7H8XXzLLZj4uxfvdSYUE1EZN0SstV)">
                                <path d=" M 63.111 17.359 L 11.443 17.359 L 63.111 8.72 L 61.75 5.33 C 60.539 2.304 57.373 0.54 54.162 1.104 L 6.8 9.437 C 3.509 10.015 1.108 12.874 1.109 16.216 L 1.109 62.111 C 1.109 65.916 4.194 69 7.998 69 L 63.111 69 C 66.916 69 70 65.916 70 62.111 L 70 24.22 C 70.001 22.395 69.275 20.645 67.982 19.358 C 66.689 18.07 64.936 17.351 63.111 17.359 L 63.111 17.359 Z  M 57.944 48.332 C 55.091 48.332 52.777 46.019 52.777 43.165 C 52.777 40.312 55.091 37.999 57.944 37.999 C 60.798 37.999 63.111 40.312 63.111 43.165 C 63.11 46.018 60.797 48.331 57.944 48.332 L 57.944 48.332 Z "
                                    fill="rgb(255,255,255)" />
                            </g>
                        </svg>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
</div>
                                    </div>