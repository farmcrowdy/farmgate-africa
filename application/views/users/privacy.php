<div class="container-fluid sub-head-main">
        <div class="row no-pad page-title-bg">
            <div class="col-12 sub-head-main">
                <ul class="page-title-list">
                    <li>
                        <a href="<?php echo base_url();?>">Home</a> /
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>privacy">Privacy</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 col-sm-10 offset-sm-1 col-12 px-4 px-sm-3 terms-conditions">
            
                <p>We hold the portfolios of our clients in the highest confidence. 
                    This Privacy Policy governs the manner in which Farmgate collects, uses, maintains and discloses information collected from users (each, a “User”) on the http://www.farmgate.africa website (“Site”). 
                    This privacy policy applies to the Site and all products and services offered by Farmgate.</p>
                <h4>Personal identification information</h4>
                <p>We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, register on the site, place an order, fill out a form, and in connection with other activities, services, features or resources we make available on our Site. As deemed appropriate, Users may be asked for; name, email address, mailing address, phone number, account number. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. 
                    Users can always refuse to supply identification information personally, except that it may prevent them from engaging in specific Site related activities.</p>
                <h4>Non-personal identification information</h4>
                <p>We may collect non-personal identification information about Users whenever they interact with our Site. The non-personal identification information may include the browser name, the type of computer and technical information about the Users means of connection to our Site, such as the operating system and other similar information.</p>
                <h4>Web browser cookies</h4>
                <p>Our Site may use “cookies” to enhance User experience. User’s web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. The user may choose to set their web browser to refuse cookies or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p>
                <h4>How we use collected information</h4>
                <p>Farmgate may collect and use Users personal information for the following purposes:
                    <li>To run and operate our Site</li>
                    <li>We may need your information to display content on the Site correctly.</li>
                    <li>To improve customer service</li>
                    <li>TRespond to customer service requests and support needs more efficiently.</li>
                    <li>To personalize user experience</li>
                </p>

            </div>
        </div>
    </div>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="js/custom.js"></script>
    <script>
        AOS.init();
    </script>