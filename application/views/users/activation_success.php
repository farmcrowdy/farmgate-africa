    <div class="container-fluid main-contact-head">
        <div class="row no-pad sub-header-bg">
            <div class="col-lg-6 offset-lg-3 col-md-6 offset-md-3 col-10 offset-1 text-center">
                <h4>Activation Successful</h4>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row no-pad after-sign-up-pad">
            <div class="col-lg-8 offset-lg-2 my-sm-5 mb-3 order-received-bg">
                <div class="row no-pad">
                    <div class="col-12 py-4 text-center">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate"
                            viewBox="0 0 60 60" width="60" height="60">
                            <defs>
                                <clipPath id="_clipPath_y6Qx7OBQ7mwp5fVkozj02agCv3RfeaXf">
                                    <rect width="60" height="60" />
                                </clipPath>
                            </defs>
                            <g clip-path="url(#_clipPath_y6Qx7OBQ7mwp5fVkozj02agCv3RfeaXf)">
                                <clipPath id="_clipPath_R4VDgr34fxGpdkjXzVpYZE7d4OqcM5cB">
                                    <rect x="0" y="0" width="60" height="60" transform="matrix(1,0,0,1,0,0)" fill="rgb(255,255,255)" />
                                </clipPath>
                                <g clip-path="url(#_clipPath_R4VDgr34fxGpdkjXzVpYZE7d4OqcM5cB)">
                                    <g>
                                        <path d=" M 29.999 0 C 32.697 -0.01 35.383 0.348 37.985 1.063 C 48.177 3.859 56.141 11.823 58.937 22.016 C 60.354 27.245 60.354 32.755 58.937 37.984 C 56.142 48.178 48.178 56.142 37.985 58.937 C 32.756 60.354 27.245 60.354 22.016 58.937 C 11.823 56.141 3.859 48.177 1.063 37.984 C -0.354 32.755 -0.354 27.244 1.063 22.016 C 3.862 11.825 11.825 3.862 22.016 1.063 C 24.617 0.349 27.303 -0.009 29.999 0 Z  M 29.999 55.999 C 32.328 56.005 34.647 55.69 36.89 55.062 C 39.066 54.454 41.154 53.571 43.106 52.433 C 46.965 50.181 50.177 46.97 52.429 43.11 C 53.562 41.162 54.445 39.077 55.053 36.906 C 56.303 32.392 56.303 27.624 55.053 23.109 C 54.447 20.934 53.565 18.844 52.429 16.891 C 50.178 13.029 46.966 9.816 43.106 7.563 C 41.153 6.426 39.062 5.545 36.885 4.938 C 32.371 3.688 27.604 3.688 23.09 4.938 C 20.919 5.546 18.834 6.428 16.885 7.563 C 13.026 9.814 9.814 13.026 7.563 16.885 C 6.426 18.839 5.544 20.929 4.936 23.106 C 3.687 27.615 3.687 32.379 4.936 36.887 C 5.543 39.064 6.426 41.154 7.563 43.107 C 9.814 46.966 13.026 50.178 16.885 52.429 C 18.834 53.563 20.919 54.446 23.09 55.056 C 25.337 55.686 27.661 56.001 29.994 55.993 L 29.999 55.999 Z  M 43.593 17.591 L 46.406 20.404 L 25.002 41.841 L 13.597 30.403 L 16.409 27.591 L 25.003 36.184 L 43.594 17.591 L 43.593 17.591 Z "
                                            fill-rule="evenodd" fill="rgb(255,255,255)" />
                                    </g>
                                </g>
                            </g>
                        </svg>
                        <p>Activation Successful</p>
                        <span>Your account has been activated successfully.</span>
                        <span>You can now login to your account. Thank you.</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 back-arrow text-center">
                <a href="<?php echo base_url();?>users/login" class="back-arrow-link"> Login</a>
        </div>
    </div>

