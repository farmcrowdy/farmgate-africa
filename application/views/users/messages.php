<!-- Contact manager modal -->
<div class="modal fade contact-manager-modal mt-5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg  ">
		<form class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title pl-md-5 ml-md-3" id="changePasswordLabel">Contact Manager</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body container mt-3">
				<div class="row py-2">
					<div class="col-12 col-md-10 offset-md-1">
						<label for="">Subject</label>
						<input type="text" class="form-control" id="" aria-describedby="Volume" placeholder="">
					</div>
				</div>
				<div class="row py-2">
					<div class="col-12 col-md-10 offset-md-1">
						<label for="">Category</label>
						<select class="form-control" id="formControlSelect">
							<option>Client</option>
							<option>General</option>
							<option>Issues</option>
							<option>Suggestions</option>
						</select>
					</div>
				</div>
				<div class="row py-2">
					<div class="col-12 col-md-10 offset-md-1">
						<label for="">Describe the question in detail</label>
						<textarea class="form-control" rows="5" aria-label="With textarea"></textarea>

					</div>
				</div>
				<div class="row py-2">
					<div class="col-12 col-md-10 offset-md-1">
						<p class="profile-file-paceholder mb-0 pb-0 pl-3 dashboard-activity-body"></p>
						<label for="upload-file-for-manager" class="btn btn-default manager-verify-link" style="color:#888888">
							<i class="fas fa-upload"></i> Add files</label>
						<input id="upload-file-for-manager" name="pic" accept="" type="file" class="hidden">
					</div>
					<script>
						$("#upload-file-for-manager").change(function () {
							$(".profile-file-paceholder").text(this.files[0].name);
						});

					</script>
				</div>
			</div>
			<div class="modal-footer d-flex justify-content-center">
				<button type="button" class="btn btn-secondary px-4 py-2" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-profile py-2 px-4">Submit</button>
			</div>
		</form>
	</div>
</div>

<div class="col-md-10">
	<div class="row">
		<div class="col-12 my-4 ">
			<div class="row ">
				<div class="col-md-8 col-12 pad-dashboard">
					<p class="order-title">Messages</p>
					<!-- <center> -->
						<div class="ajax-load offset-7" style="display:none;">
							<p><img width="64px" src="<?php echo base_url('mainasset/fe/images/loader.gif'); ?>">
							Loading More posts</p>
						</div>
					<!-- </center> -->
				</div>
				<div class="col-5 col-sm-4 d-md-none">
					<div class="row">
						<div class="col-10 p-0 m-0 dashboard  text-center">
							<!-- <i class="fas fa-chalkboard-teacher d-block"></i> -->
							<span class="dashboard-close  m-0 text-left">Dashboard &nbsp;
								<i class="fas fa-angle-right pl-1"></i>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 pad-dashboard mb-2">
					<p class="dashboard-message">
						<span class="dashboard-message-sub pt-1">You have</span>
						(<span>0</span>) unread messages
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-12 height-100  px-md-5 px-3">
					<div class="row pt-4 h-100 mb-0 pb-0" style="background: #ffffff;">
						<div class="col-md-4 col-10 col-sm-5 d-none d-md-block slideInRight animatedSlide slideInFromLeft " id="chats">
							<div class=" pb-5 close-btn d-md-none">
								<button type="button" class="close" aria-label="Close">
									<span aria-hidden="true" class=" hide-chats" style="color:#888;">&times;</span>
								</button>
							</div>
							<ul class="pl-0 listed-chats chat-scrollbar pull-chat-right touch-list-width" style="list-style-type: none; overflow-y: scroll; height: 500px;">
								<?php if(!empty($orders)): ?>
								<?php foreach($orders as $order) : ?>
								<li class="border-bottom pt-4">
									<a href="<?php echo base_url();?>users/messagechat/<?php echo $order->or_order_number;?>" class="chat-profile-link load_messages">
										<img src="<?php echo base_url();?>mainasset/fe/images/products/<?php echo $order->prod_image; ?>" class=" select-chat image-fluid"
										alt="<?php echo $order->prod_name; ?>">
										<span class="personal-chat-list pl-1">
											<?php echo $order->prod_name; ?> : <small><?php echo $order->or_order_number;?></small>
										</span>
										<p class="personal-chat-list-date">
											<?php echo date('h:i a - d M Y', strtotime($order->or_date_created));?>
										</p>
									</a>
								</li>
								<?php endforeach ?>
								<?php else: ?>
								<p> There are no orders here</p>
								<?php endif ?>
							</ul>
						</div>
						<div class="col-md-8 col-12 border-left">
							<div class="more-chats d-md-none">
								<div class="rotate-icon p-2">
									<i class="fab fa-first-order fa-2x"></i>
									<!-- <i class="fab fa-first-order-alt fa-2x "></i> -->
									<span class="">Chats</span>
									<!-- <i class="fab fa-first-order fa-2x"></i> -->
								</div>
							</div>
							<div id="message_area">
								<div class="row">
									<div class="col-md-8 offset-md-2">
										<div class="text-center no-chat-selected">
											<p class="no-chat-title  mb-0">No conversation selected</p>
											<p class=" mb-0 ">Select a conversation on the left to read and respond to messages</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
</div>
