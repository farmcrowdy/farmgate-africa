<div class="container-fluid">
    <div class="row page-title-bg page-title-bg1 no-pad">
        <div class="col-12">
        </div>
    </div>
</div>
<div class="container">
    <div class="row mb-5 no-pad login">
        <div class="col-lg-4 col-10 offset-1 col-md-6 mt-5 mb-4 offset-md-3 offset-lg-4 text-center ">
            <h4 class="mobile">Log in to your account</h4>
            <span>Enter your email and password to log in</span>
        </div>
        <div class="col-lg-4 col-sm-10 col-12 offset-sm-1 col-md-6 offset-md-3 pt-5 pb-4 offset-lg-4 login-bg">
            <?php echo form_error('email'); ?><?php echo form_error('password'); ?>
            <?php if(!empty($invalidLogin)){ echo $invalidLogin; }  ?>
            <form class="px-2" action="login" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" name="email" value="<?php echo set_value('email');?>" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email">
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" autocomplete="on">
                </div>
                <div class="row ml-0 no-pad">
                    <div class="col-5 form-group form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Remember me</label>
                    </div>

                    <div class="col-7 text-right">
                        <a href="<?php echo base_url();?>users/forgotpassword" class="forgot-pass">Forgot Password?</a>
                    </div>
                </div>
                <button name="login_submit" type="submit" class="btn login">Login</button>
                <p class="account text-center">Don’t have an account? <a href="<?php echo base_url();?>users/register">Sign up here</a></p>
            </form>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
<script>
    // w3.includeHTML();
    // ScrollReveal().reveal('.more-products', {
    //     duration: 2000
    // });
    // ScrollReveal().reveal('.products', {
    //     interval: 500
    // });
</script>