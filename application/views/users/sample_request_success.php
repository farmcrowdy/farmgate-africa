<!doctype html>
<html lang="en">

<head>
    <title>Farmgate</title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url();?>mainasset/fe/images/favicon.ico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url();?>mainasset/fe/images/favicon.ico/ms-icon-144x144.png">
    <meta name="theme-color" content="#08BD51">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
    <!-- <link rel="shortcut icon" href="https://www.farmcrowdy.com/wp-content/themes/farmcrowdy/images/favicon.ico?ver=1501577976"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>mainasset/fe/css/animation.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>mainasset/fe/css/fonts.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
        crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url();?>mainasset/fe/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url();?>mainasset/fe/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>mainasset/fe/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>mainasset/fe/css/jquery-ui.css">

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>mainasset/fe/css/index.css">

    <link rel="stylesheet" href="<?php echo base_url();?>mainasset/fe/css/lity.css">
    <script src="<?php echo base_url();?>mainasset/fe/js/lity.js"></script>
    <script src="<?php echo base_url();?>mainasset/fe/js/jquery.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <style>
    </style>
</head>

<body>
    <!-- ==============Top nav========== -->
    <div class="container-fluid">
    <div class="row px-3">
        <div class="col-12 col-lg-10 offset-lg-1 pt-5">
            <div class="row">
                <div class="col-12 border-bottom">
                    <h4 class="sample sample-title">Product Sample Request Successful</h4>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ============end of body================= -->


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script type="text/javascript">


    </script>

    <!-- <script type="text/javascript" src="js/fontawesome-all.js"></script> -->
    <script type="text/javascript">
        $(document).ready(function(){

              $('#quantity').change(function(){
                   var quantity = $(this).val();
                   quantity = Number(quantity);

                   var unit_price = $('#price_per_unit').text().trim();
                   unit_price = Number(unit_price);

                   var total = unit_price * quantity;
                   $('#total_order').text("N"+total);

              });
        });
    </script>
  </body>
</html>