
    <!-- ============body================= -->
    <div class="container-fluid body-bg  body-bg-login" id="auth-bg">
        <div class="row">
            <div class="col-12 ">
                <div class="row">
                    <div class=" login-bg p-0">
                        <div class="card border-0 w-100">
                            <h5 class="card-header auth text-center">Login</h5>
                            <div class="card-body">
                              <?php echo form_error('email'); ?><?php echo form_error('password'); ?>
                                 <?php if(!empty($invalidLogin)){ echo $invalidLogin; }  ?>
                                <p class="text-center login-title">Enter your email and and password to login</p>
                                <form action="login" method="post">
                                    <div class="input-group mb-3 login-border">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="fa fa-user"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" name="email" value="<?php echo set_value('email');?>" aria-label="Username" aria-describedby="basic-addon1">
                                    </div>
                                    <div class="input-group mb-3 login-border">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate" viewBox="140 202 16 21"
                                                    width="16" height="21">
                                                    <path d=" M 148 202 C 144.676 202 142 204.676 142 208 L 142 209 C 140.898 209 140 209.898 140 211 L 140 221 C 140 222.102 140.898 223 142 223 L 154 223 C 155.102 223 156 222.102 156 221 L 156 211 C 156 209.898 155.102 209 154 209 L 154 208 C 154 204.676 151.324 202 148 202 Z  M 148 204 C 150.277 204 152 205.723 152 208 L 152 209 L 144 209 L 144 208 C 144 205.723 145.723 204 148 204 Z  M 148 214 C 149.102 214 150 214.898 150 216 C 150 217.102 149.102 218 148 218 C 146.898 218 146 217.102 146 216 C 146 214.898 146.898 214 148 214 Z "
                                                        fill="rgb(0,0,0)" />
                                                </svg>
                                            </span>
                                        </div>
                                        <input type="password" name="password" class="form-control" placeholder="Password" aria-label="Password" aria-describedby="basic-addon1">
                                    </div>
                                    <div class="row">
                                        <div class="col-6 form-group form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                            <label class="form-check-label ml-4 link-color1" for="">Remember me</label>
                                        </div>

                                        <div class="col-6">
                                            <a href="<?php echo base_url();?>users/forgotpassword" class="login-forgot-pass">Forgot Password?</a>
                                        </div>
                                    </div>
                                    <button name="login_submit" type="submit" class="btn login-btn">Submit</button>
                                    <p class="pt-4 text-center" style="color:#666666;">Don't have an account? &nbsp;
                                        <a href="<?php echo base_url();?>users/register" class="login-forgot-pass">Register</a>
                                    </p>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <!-- <div class="container-fluid login-top ">
        <div class="row footer">
            <div class="col-lg-10 offset-lg-1 mt-4">
                <div class="row">
                    <div class="col-md-6">
                        <p>Subscribe to receive news first</p>
                        <form class="row">
                            <div class="form-group col-lg-9 col-sm-8 email-input">
                                <input type="email" class="form-control" id="email" placeholder="Email">
                            </div>
                            <div class="col-lg-3 col-sm-4 submit-email">
                                <button type="submit" class="btn ">SUBSCRIBE</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3 col-6 mt-3">
                        <ul class="footer-list">
                            <li>
                                <a href="#">Home</a>
                            </li>
                            <li>
                                <a href="#">About</a>
                            </li>
                            <li>
                                <a href="#">How it works</a>
                            </li>
                            <li>
                                <a href="#">Compliance</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-6  mt-3">
                        <ul class="footer-list">
                            <li>
                                <a href="#">Terms of Service</a>
                            </li>
                            <li>
                                <a href="#">Privacy Policy</a>
                            </li>
                            <li>
                                <a href="#">Support</a>
                            </li>
                            <li>
                                <a href="#">FAQ</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="background:#293737;">
            <div class="col-lg-10 offset-lg-1 my-2">
                <p class="copyrite mb-0"> &copy; 2018 Farmcrowdy - All rights reserved</p>
            </div>
        </div>
    </div> -->
    <!-- ============end of body================= -->


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <script src="<?php echo base_url();?>mainasset/fe/js/owl.carousel.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            //  carousel center present
            // $('.loop').owlCarousel({
            //     center: true,
            //     items:2,
            //     loop:true,
            //     margin:10,
            //     responsive:{
            //         600:{
            //             items:4
            //         }
            //     }
            // });
            $('.animated-icon3').click(function () {
                $(this).toggleClass('open');
            });
            $('#btn-menu').click(function () {
                $('#btn-menu').toggleClass("nav-button");
            });
            $('.owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1,
                        nav: true
                    },
                    600: {
                        items: 3,
                        nav: false
                    },
                    1000: {
                        items: 4,
                        nav: true,
                        loop: false
                    }
                }
            })
            // add event listener (next and prev btns)
            var owl = $('.owl-carousel');
            owl.owlCarousel();
            // Go to the next item
            $('.owl-next').click(function () {
                owl.trigger('next.owl.carousel');
            })
            // Go to the previous item
            $('.owl-prev').click(function () {
                // With optional speed parameter
                // Parameters has to be in square bracket '[]'
                owl.trigger('prev.owl.carousel', [300]);
            })
        })
    </script>

    <!-- <script type="text/javascript" src="js/fontawesome-all.js"></script> -->
</body>

</html>
