<div class="col-md-10">
    <?php echo $this->session->flashdata('del_feedback'); ?>
    <div class="row">
        <div class="col-12 my-4 ">
            <div class="row ">
                <div class="col-md-8 col-12">
                    <p class="order-title">My Investments ( <?php if(!empty($total_investments)){echo $total_investments;}else{echo "0"; } ?> )
                    </p>
                </div>
                <div class="col-5 col-sm-4 d-md-none">
                    <div class="row">
                        <div class="col-10 p-0 m-0 dashboard  text-center">
                            <!-- <i class="fas fa-chalkboard-teacher d-block"></i> -->
                            <span class="dashboard-close  m-0 text-left">Dashboard &nbsp;
                                <i class="fas fa-angle-right pl-1"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="input-group col-md-4 col-sm-8 col-7   pr-0 order-search">
                    <form class="form-inline" method="get" action="<?php echo base_url('users/investments'); ?>">
                        <input name="keyword_uinv" type="search" class="form-control" placeholder="Commodity Name" aria-label="Username" aria-describedby="basic-addon1"
                            value="<?php if(!empty($keyword_uinv)){echo $keyword_uinv;} ?>">
                        <div class="input-group-append">
                            <button class="btn search-btn" style="background: none;">
                                <i class="fas fa-search"></i>
                            </button>
                            <!-- <i class="fas fa-search m-auto"></i> -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="background:#fefefe;">
        <div class="col-12">
            <div class="row d-none d-md-block">
                <div class="col-12">
                    <div class="row py-2 order-nav-title" style="background: #EFEFEF; border-bottom:1px solid rgb(199, 199, 199);">
                        <div class="col-12">
                            <div class="row pad-order">
                                <div class="col-lg-2 col-md-2">Date</div>
                                <div class="col-lg-2 col-md-2">Commodity</div>
                                <div class="col-md-2">Amount Invested</div>
                                <div class="col-md-2">Quantity</div>
                                <div class="col-md-2">Pay Status</div>
                                <div class="col-md-1">Returns</div>
                                <div class="col-md-1">Returns Status</div>
                                <!-- <div class="col-md-2">Due Date</div> -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php if(!empty($investments)): ?>
            <?php foreach($investments as $investment) : ?>
            <div class="row pad-order">
                <div class="col-12 order-body">
                    <div class="row border-bottom pb-2">
                        <div class="col-lg-2 col-md-2 col-6">
                            <?php echo date('d M Y',strtotime($investment->iv_date_created)); ?>
                        </div>
                        <div class="col-md-2 col-lg-2 order-id col-6 text-right text-md-left">
                            <?php echo strtoupper($investment->iv_commodity_name); ?> ||
                            <?php echo strtolower($investment->iv_slug); ?>
                        </div>
                        <div class="col-md-2 col-4">
                            <?php echo $investment->currency; ?>
                            <?php echo number_format($investment->iv_amount); ?>
                        </div>
                        <div class="col-md-2 col-4">
                            <?php echo number_format($investment->iv_quantity); ?>
                            
                        </div>
                        <!-- <img src="images/categories1.svg" alt="" style="height: 21.4px; width:21.4px;"> -->
                        <div class="col-lg-2 col-4 col-md-2 order-nav-title bg-status sold">
                            <?php echo strtoupper(str_replace('_',' ',$investment->iv_pay_status)); ?>
                        </div>
                        <div class="col-lg-1 col-4 col-md-1 order-nav-title bg-status sold">
                            <?php echo strtoupper(str_replace('_',' ',$investment->iv_returns)); ?>
                        </div>
                        <div class="col-lg-1 col-4 col-md-1 order-nav-title bg-status sold">
                            <?php echo strtoupper(str_replace('_',' ',$investment->iv_returns_status)); ?>
                        </div>
                        <!-- <div class="col-lg-2 col-4 col-md-2 order-nav-title bg-status sold">
                            <?php //echo date('d M Y',strtotime($investment->iv_due_date)); ?>
                        </div> -->
                    </div>
                    <!-- <div class="row pt-2">
                        <div class="col-lg-3 col-md-4 order-nav-title">
                            <?php //echo $investment->iv_name; ?>
                        </div>
                        <div class="col-lg-5 col-md-6 offset-md-2 offset-lg-4 pt-2 pb-3 text-right">
                            <a href="<?php //echo base_url('investment/'.$investment->iv_slug); ?>" target="_blank" class="ml-1">View
                                investment</a>
                        </div>
                    </div> -->
                </div>
            </div>
            <?php endforeach ?>
            <?php echo $pagination_links;?>
            <?php else: ?>
            <div class="row pad-order">
                <p> <b>There are no investments to display here. </b></p>
            </div>
            <?php endif ?>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>