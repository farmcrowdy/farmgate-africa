<div class="container-fluid body-bg body-bg-login">
        <div class="row">
            <div class="col-12 col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2 col-md-10  offset-md-1 col-sm-8  offset-sm-2 login-bg">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">
                                <div class="p-5">
                                    <h5 class="text-center login-color">Password Reset</h5>
                                    <p class="text-center login-title">Enter Your New Password</p>
                                   <?php echo form_open('users/resetpassword/'.$hash); ?>
                                   <?php echo form_error('new_password'); ?>
                                   <?php echo form_error('new_password_confirm');?>
                                   <?php if(!empty($feedback)) {echo $feedback;} ?>
                                        <div class="input-group mb-3 login-border">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="far fa-envelope"></i></span>
                                            </div>
                                            <input name="new_password" type="password" class="form-control" placeholder="Enter New Password" aria-label="Email" aria-describedby="basic-addon1" value="<?php echo set_value('new_password'); ?>">
                                        </div>
                                  
                                        <div class="input-group mb-3 login-border">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="far fa-envelope"></i></span>
                                            </div>
                                            <input name="new_password_confirm" type="password" class="form-control" placeholder="Confirm New Password" aria-label="Password" aria-describedby="basic-addon1" value="<?php echo set_value('new_password_confirm'); ?>">
                                        </div>
                                  
                                        <button type="submit" class="btn login-btn-red" name="reset_pass_submit">Reset Password</button>
                                        <div class="row">
                                            <div class="col-12 text-center pt-2">
                                                <a href="<?php echo base_url('users/login'); ?>" class="link-color"> Back to login</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    