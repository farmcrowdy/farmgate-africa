<div class="col-md-10">
    <div class="row no-pad">
        <div class="col-12 my-md-4 mb-4 ">
            <div class="row no-pad ">
                <div class="col-md-8 col-12 pad-dashboard">
                    <p class="order-title">Edit Profile</p>
                </div>
                <div class="col-5 col-sm-4 d-md-none">
                    <div class="row no-pad">
                        <div class="col-10 p-0 m-0 dashboard  text-center">
                            <!-- <i class="fas fa-chalkboard-teacher d-block"></i> -->
                            <span class="dashboard-close  m-0 text-left">Dashboard &nbsp; <i class="fas fa-angle-right pl-1"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row no-pad">
                <div class="col-lg-10 col-md-12 col-11">
                    <!-- <form class="row no-pad acct-form mb-4" action=""> -->
                    <?php echo form_open_multipart('users/editprofile', array('class'=>'row no-pad acct-form mb-4')); ?>
                        <div class="col-md-12 pad-dashboard">
                            <div class="row no-pad pt-4 pb-2">
                                <div class="col-lg-12 upload-photo" id='image-preview'>
                                    <?php if(!empty($acc_image_errors)){
                                        foreach ($acc_image_errors as $error) {
                                            # code...
                                            echo $error;
                                        } 
                                    } 
                                    ?>
                                    <img src="<?php if(!empty($user->image)){ echo base_url('mainasset/fe/images/users/').$user->image;}else{ echo base_url('mainasset/fe/images/profile1.png');} ?>" alt="<?php echo $user->full_name; ?>" class="img-fluid picture rounded">
                                    <div class="user-placeholder hidden"></div>
                                    <label class="btn btn-default new-line" for="upload-file-selector">Upload Your image</label>
                                    <input id="upload-file-selector" name="userImageFile" accept="image/jpeg,image/png" type="file" class="hidden">

                                    <script>
                                        $('#upload-file-selector').change(function () {
                                            var files = $(this).get(0).files;
                                            if (files[0]) {
                                                var reader = new FileReader();

                                                reader.onload = function (e) {
                                                    var previewContainer = $('#image-preview');
                                                    previewContainer.find('img').attr('src', e.target.result).removeClass('hidden');
                                                    previewContainer.find('.user-placeholder').addClass('hidden');
                                                };

                                                reader.readAsDataURL(files[0]);
                                            }
                                        })
                                    </script>
                                </div>

                            </div>
                            <div class="row no-pad mt-md-5">
                                <div class="col-sm-6 ">
                                    <label for="">Full Name</label>
                                    <?php echo form_error('pro_full_name'); ?>
                                    <input type="text" class="form-control" id="" name="pro_full_name" aria-describedby="firstname" placeholder=""
                                    value="<?php echo set_value('pro_fullname', $user->full_name); ?>">

                                </div>
                            </div>
                            <div class="row no-pad">
                                <div class="col-md-5 col-sm-8">
                                    <label for="">Phone Number</label>
                                    <?php echo form_error('pro_phone'); ?>
                                    <input type="tel" class="form-control" name="pro_phone" id="pro_phone" aria-describedby="phoneNumber" placeholder=""
                                    value="<?php echo set_value('pro_phone', $user->phone); ?>">
                                </div>
                            </div>
                            <div class="row no-pad">
                                <div class=" col-12  form-group">
                                    <label for="">Address</label>
                                    <?php echo form_error('pro_address'); ?>
                                    <textarea name="pro_address" class="form-control" rows="5" aria-label="With textarea"><?php echo set_value('pro_address', $user->address); ?></textarea>
                                </div>
                            </div>
                            <p class="mt-md-5" style="color:#FF9914">Account Information</p>
                            <div class="row no-pad">
                                <div class="col-md-6">
                                    <label for="">Account Name</label>

                                    <input type="text" class="form-control" id="pro_acc_name" name="pro_acc_name" aria-describedby="accountName" placeholder=""
                                    value="<?php echo set_value('pro_acc_name', $user->acc_name); ?>">

                                </div>
                                <div class="col-md-6">
                                    <label for="">Account Number</label>

                                    <input type="text" class="form-control" id="pro_acc_number" name="pro_acc_number" aria-describedby="accountNumber" placeholder=""
                                    value="<?php echo set_value('pro_acc_number', $user->acc_number); ?>">
                                </div>
                            </div>
                            <div class="row no-pad mt-md-5">
                                <div class="col-md-6">
                                    <label for="">Bank Name</label>

                                    <input type="text" class="form-control" id="pro_bank_name" name="pro_bank_name" aria-describedby="banktName" placeholder=""
                                    value="<?php echo set_value('pro_bank_name', $user->bank_name); ?>">
                                </div>
                            </div>
                            <div class="row no-pad">
                                <div class="col-12  pb-5 pt-4">
                                    <button type="submit" name="account_btn" class="btn  btn-submit"> Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
</div>