<div class="col-md-10">
    <div class="row">
        <div class="col-12 my-4 ">
            <div class="row ">
                <div class="col-md-8 col-12">
                    <p class="order-title">My Orders (<?php if(!empty($total_orders)){echo $total_orders;}else{echo "0"; } ?>)</p>
                </div>
                <div class="col-5 col-sm-4 d-md-none">
                    <div class="row">
                        <div class="col-10 p-0 m-0 dashboard  text-center">
                            <!-- <i class="fas fa-chalkboard-teacher d-block"></i> -->
                            <span class="dashboard-close  m-0 text-left">Dashboard &nbsp;
                                <i class="fas fa-angle-right pl-1"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="input-group col-md-4 col-sm-8 col-7   pr-0 order-search">
                    <form class="form-inline" method="get" action="<?php echo base_url('users/orders'); ?>">
                        <input name="keyword_o" type="search" class="form-control" placeholder="Search by Order No" aria-label="Username" aria-describedby="basic-addon1"
                        value="<?php if(!empty($keyword_o)){echo $keyword_o;} ?>">
                        <div class="input-group-append">
                            <button type="submit" class="btn search-btn" style="background: none;">
                                <i class="fas fa-search"></i>
                            </button>
                            <!-- <i class="fas fa-search m-auto"></i> -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="row d-none d-md-block">
                            <div class="col-12 mb-4">
                                <ul class="nav order-sub-nav">
                                    <li class="nav-item">
                                        <a class="nav-link  active" href="#">All</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Being Processed</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Shipped</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Delivered</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Cancelled</a>
                                    </li>
                                </ul>
                            </div>
                        </div> -->
    <!-- <div class="row pad-order d-md-none ">
                            <div class="input-group">
                                <select class="custom-select" id="">
                                    <option selected>All</option>
                                    <option value="1">Being Processed</option>
                                    <option value="2">Shipped</option>
                                    <option value="3">Delivered</option>
                                    <option value="4">Cancelled</option>
                                </select>
                            </div>
                        </div> -->
    <div class="row" style="background:#fefefe;">
        <div class="col-12">
            <div class="row d-none d-md-block">
                <div class="col-12">
                    <div class="row py-2 order-nav-title" style="background: #EFEFEF; border-bottom:1px solid rgb(199, 199, 199);">
                        <div class="col-12">
                            <div class="row pad-order">
                                <div class="col-lg-2 col-md-3"> Order Date</div>
                                <div class="col-lg-2 col-md-3">Order No</div>
                                <div class="col-md-2">Amount</div>
                                <div class="col-md-2">Volume</div>
                                <div class="col-md-2">Status</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php if(!empty($orders)): ?>
            <?php foreach($orders as $order) : ?>
            <div class="row pad-order">
                <div class="col-12 order-body">
                    <div class="row border-bottom pb-2">
                        <div class="col-lg-2 col-md-3 col-6">
                            <?php echo date('d M Y',strtotime($order->or_date_created)); ?>
                        </div>
                        <div class="col-md-3 col-lg-2 order-id col-6 text-right text-md-left">
                            <?php echo strtoupper($order->or_order_number); ?>
                        </div>
                        <div class="col-md-2 col-4">
                            <?php echo number_format($order->ord_total); ?>
                        </div>
                        <div class="col-md-2 col-4">
                            <?php echo number_format($order->ord_quantity); ?>
                            <?php if(!empty($order->unit)){echo $order->unit;}else{ echo " MT";} ?>
                        </div>
                        <img src="images/categories1.svg" alt="" style="height: 21.4px; width:21.4px;">
                        <div class="col-lg-3 col-4 col-md-2 order-nav-title bg-status sold">
                            <?php echo strtoupper(str_replace('_',' ',$order->or_status)); ?>
                        </div>
                    </div>
                    <div class="row pt-2">
                        <div class="col-lg-3 col-md-4 order-nav-title">
                            <?php 
                            if(!empty($order->prod_name)){echo $order->prod_name;}else{ if(!empty($order->ord_lpo_prod_name)){ echo $order->ord_lpo_prod_name; }} ?>
                        </div>
                        <div class="col-lg-5 col-md-6 offset-md-2 offset-lg-4 pt-2 pb-3 text-right">
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach ?>
            <?php echo $pagination_links;?>
            <?php else: ?>
            <div class="row pad-order">
                <p> <b>There are no orders to display here. </b></p>
            </div>
            <?php endif ?>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>