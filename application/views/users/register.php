<div class="container-fluid">
    <div class="row page-title-bg page-title-bg1 no-pad">
        <div class="col-12">
        </div>
    </div>
</div>
<div class="container">
    <div class="row mb-5 login no-pad">
        <div class="col-lg-4 col-10 offset-1 col-md-6 mt-5 mb-4 offset-md-3 offset-lg-4 text-center ">
            <h4 class="mobile">Sign up for a new account</h4>
            <span>Create an account to get started</span>
        </div>
        <div class="col-lg-4 col-sm-10 col-12 offset-sm-1 col-md-6 offset-md-3 pt-5 pb-4 offset-lg-4 login-bg">
            <?php echo form_error('reg_fullname'); ?>
            <?php echo form_error('reg_email'); ?>
            <?php echo form_error('reg_phone'); ?>
            <?php echo form_error('reg_password'); ?>
            <?php echo form_error('reg_terms'); ?>
            <form class="px-2" action="register" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" id="reg_fullname" name="reg_fullname" value="<?php echo set_value('reg_fullname'); ?>"
                        aria-describedby="textHelp" placeholder="Full Name">
                </div>
                <div class="form-group">
                    <input type="tel" class="form-control" id="reg_phone" name="reg_phone" value="<?php echo set_value('reg_phone'); ?>" aria-describedby="textHelp"
                        placeholder="Phone +234">
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="reg_email" name="reg_email" value="<?php echo set_value('reg_email'); ?>" aria-describedby="emailHelp"
                        placeholder="Email">
                </div>
                <div class="input-group">
                    <input type="password" class="form-control" id="reg_password" name="reg_password" value="<?php echo set_value('reg_password'); ?>"
                        placeholder="Password" autocomplete="on">
                    <!-- An element to toggle between password visibility -->
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2"><i id="toggle_pass" onclick="togglePassword('reg_password');" class="fas fa-eye"></i></span>
                    </div>
                </div>
                <br style="height:12px;">
                <div class="row ml-0 no-pad">
                    <div class="col-12 form-group form-check">
                        <input type="checkbox" class="form-check-input" id="reg_terms" name="reg_terms"  value="1" <?php echo set_checkbox('reg_terms'); ?> >
                        <label class="form-check-label" for="exampleCheck1">I have read and agree to the <a href="<?php echo base_url();?>toc" class="label"
                                target="_blank">terms and conditions</a></label>
                    </div>

                </div>
                <button type="submit" name="register_submit" class="btn login">Register</button>
                <p class="account text-center">Already have an account? <a href="<?php echo base_url();?>users/login">Login</a></p>
            </form>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
<script type="text/javascript">
// togglePassword('reg_password');
    // toggle password field type given string , id
    function togglePassword(id) {
        var x = document.getElementById(id);
        if (x.type === "text") {
            x.type = "password";
        } else {
            x.type = "text";
        }
    }

    $('#toggle_pass').click(function(e){
        $(this).toggleClass('fa-eye-slash');
    });
</script>