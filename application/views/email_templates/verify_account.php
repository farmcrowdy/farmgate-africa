<!DOCTYPE html>
<html>

<head>
    <title>Verify Your Email</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">

</head>

<body style="width: 100% !important; -webkit-text-size-adjust: 100%; overflow-wrap:break-word; word-wrap:break-word;hyphens:auto; background: #ECECFF !important; -ms-text-size-adjust: 100%; margin: 0;
padding: 0; font-family: 'Poppins', sans-serif; line-height: 160%; font-size: .9em;color: #222;">

    <table style="margin: 0 auto; width:500px" cellspacing="0" cellpadding="0">
        <tbody>

            <tr>
                <td style="height: 60px; ">
                    <div></div>
                </td>
            </tr>
            <tr>
                <td style="background-color:#fafafa;">
                    <div style="display: block;	text-align:center; margin: 20px; ">
                        <div><img style="width:200px; height:auto;" src="<?php echo base_url('mainasset/fe/images/farmgate.png'); ?>" alt="Farmgate logo"></div>

                    </div>

                </td>
            </tr>
            <tr>
                <td style="background-color:#fff; font-family: 'Poppins', sans-serif; ">
                    <div style="display: block; margin: 0px; padding:30px; background-color:#fff; font-family: 'Poppins', sans-serif; ">
                        <h2 style="color:#08BD51; margin-bottom:30px;">We're glad to meet you!</h2>
                        <p>Hello
                            <?php if (!empty($user->full_name)) {
                                    echo $user->full_name;
                                } else {
                                    echo "";
                                } ?> and Welcome!,
                        </p>
                        <p>Thank you for signing up with FarmGate Africa. We are so glad to meet you.</p>
                        <p>We founded FarmGate Africa because we wanted to create a platform that would give you convenient
                            access to an array of Agricultural commodities. From now on, you will get regular updates from
                            us regarding commodity purchase and trade.</p>
                        <p>In the meantime, confirm your email below and let’s get started.</p><br>
                        <a href="<?php if (!empty($user->email_activation_hash)) {
                                    echo base_url('activate/') . $user->email_activation_hash;
                                } else {
                                    echo " ";
                                } ?>" style="text-align:center;text-decoration:none; border-radius:3px; padding:5px 20px; color:#222; background-color:#08BD51; color:#fefefe;">Confirm
                            Email</a>
                        <br><br>
                        <p>If you are unable to use the button above, please try this alternative by copying the link below
                            into your address bar:</p>
                        <p><a style="text-decoration:none; color:#08BD51; word-wrap:break-word;word-break:break-word;" 
                            href="<?php if (!empty($user->email_activation_hash)) {
                                    echo base_url('activate/') . $user->email_activation_hash;
                                } else {
                                    echo "";
                                } ?>">
                                <?php if (!empty($user->email_activation_hash)) {
                                    echo base_url('activate/') . $user->email_activation_hash;
                                } else {
                                    echo "";
                                } ?></a>
                        </p>
                        <br>
                        <p>Connect with us</p>
                        <div style="display:block; margin-bottom:20px;">
                            <a href="https://web.facebook.com/farmgateafrica/" target="_blank"><img src="<?php echo base_url('mainasset/fe/images/facebook.png'); ?>"
                                    style="width:30px; height:30px;" alt="Facebook"></a>
                            <a href="https://www.twitter.com/farmgateafrica/" target="_blank"><img src="<?php echo base_url('mainasset/fe/images/twitter.png'); ?>"
                                    style="width:30px; height:30px;" alt="Twitter"></a>
                            <a href="https://www.instagram.com/farmgateafrica/" target="_blank"><img src="<?php echo base_url('mainasset/fe/images/instagram.png'); ?>"
                                    style="width:30px; height:30px;" alt="Instagram"></a>
                        </div>
                        <p><br><br>Talk soon,<br> FarmGate Africa Team<br></p>
                        <p style="font-size:0.8em;line-height:140%; margin-bottom:20px; ">If you did not sign up or request
                            for email confirmation, please ignore this email.</p>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding:30px; text-align:center; background-color:#fafafa; color:#222; font-family: 'Poppins', sans-serif; ">
                    <div>
                        <center>
                            <img style="width:200px; height:auto;" src="<?php echo base_url('mainasset/fe/images/farmgate.png'); ?>" alt="Farmgate">
                        </center>
                        <p>Innovating Market Access for Agriculture</p>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="height: 60px; ">
                    <div></div>
                </td>
            </tr>
        </tbody>
    </table>


</body>

</html>