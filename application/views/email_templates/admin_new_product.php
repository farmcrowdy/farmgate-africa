
<!DOCTYPE html>
<html>

<head>
    <title>New Product</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
   
</head>

<body style="width: 100% !important; -webkit-text-size-adjust: 100%; overflow-wrap:break-word; word-wrap:break-word;hyphens:auto; background: #ECECFF !important; -ms-text-size-adjust: 100%; margin: 0;
padding: 0; font-family: 'Poppins', sans-serif; line-height: 160%; font-size: .9em;color: #222;">

    <table style="margin: 0 auto" width="500px" cellspacing="0" cellpadding="0">
        <tbody>

            <tr>
                <td style="height: 60px; ">
                    <div></div>
                </td>
            </tr>
            <tr>
                <td style="background-color:#fafafa;">
                    <div style="display: block;	text-align:center; margin: 20px; ">
                        <div><img src="<?php echo base_url('mainasset/fe/images/farmgate.png'); ?>" alt="Farmgate Logo"></div>
                        <!-- <img style="width:100%; height:auto;" src="img/farmgate.png" alt="Farmgate"> -->

                    </div>

                </td>
            </tr>
            <tr>
                <td style="background-color:#fff; font-family: 'Poppins', sans-serif; ">
                    <div style="display: block; margin: 0px; padding:30px; background-color:#fff; font-family: 'Poppins', sans-serif; ">
                        <h2 style="color:#08BD51; margin-bottom:30px;">New Product!</h2>
                        <p>
                            <span style="font-weight:bold; color:#666; font-size:1.15em; text-shadow:1px 2px 3px rgba(0, 0, 0, 0.1);"><?php if(!empty($user->full_name)){ echo $user->full_name; }else{ echo "";} ?> </span> uploaded a new product with its details below;
                        </p>
                        <p>Product details;</p>
                        <p style="border-bottom:1px solid #ddd; padding-bottom:10px; position: relative;"> <span style="font-weight: 600; color:#666;">Product Name :</span> <span  style="position: absolute; left: 27%; color:#888; text-shadow: 1px 2px 3px rgba(0, 0, 0, 0.04), 1px 2px 3px rgba(160, 110, 110, 0.04);"><?php if(!empty($product->prod_name)){ echo $product->prod_name; }else{ echo "";} ?></span></p>

                        <p style="border-bottom:1px solid #ddd; padding-bottom:10px; position: relative;"> <span  style="font-weight: 600; color:#666;">Product ID :</span> <span  style="position: absolute; left: 27%; color:#888; text-shadow: 1px 2px 3px rgba(0, 0, 0, 0.04), 1px 2px 3px rgba(160, 110, 110, 0.04);"><?php if(!empty($product->prod_slug)){ echo $product->prod_slug; }else{ echo "";} ?></span></p>

                        <p style="border-bottom:1px solid #ddd; padding-bottom:10px; position: relative;"> <span style="font-weight: 600; color:#666;">Location :</span> <span  style="position: absolute; left: 27%; color:#888; text-shadow: 1px 2px 3px rgba(0, 0, 0, 0.04), 1px 2px 3px rgba(160, 110, 110, 0.04);"><?php if(!empty($product->state)){ echo $product->state; }else{ echo "";} ?></span></p>

                        <p style="border-bottom:1px solid #ddd; padding-bottom:10px; position: relative;"> <span style="font-weight: 600; color:#666;">Availability :</span> <span  style="position: absolute; left: 27%; color:#888; text-shadow: 1px 2px 3px rgba(0, 0, 0, 0.04), 1px 2px 3px rgba(160, 110, 110, 0.04);"><?php if(!empty($product->prod_avail_from)){ echo date('d/m/Y',strtotime($product->prod_avail_from)); }?> - <?php if(!empty($product->prod_avail_to)){ echo date('d/m/Y',strtotime($product->prod_avail_to)); }?></span></p>

                        <p style="border-bottom:1px solid #ddd; padding-bottom:10px; position: relative;"> <span style="font-weight: 600; color:#666;">Category :</span> <span style="position: absolute; left: 27%; color:#888; text-shadow: 1px 2px 3px rgba(0, 0, 0, 0.04), 1px 2px 3px rgba(160, 110, 110, 0.04);"><?php if(!empty($product->category)){ echo $product->category; }else{ echo "";} ?></span></p>

                        <p style="border-bottom:1px solid #ddd; padding-bottom:10px; position: relative;"> <span style="font-weight: 600; color:#888;">Description :</span> <span class="">
                        <?php if(!empty($product->prod_description)){ echo $product->prod_description; }else{ echo "";} ?>
                        </span></p>
                        <a href="<?php if(!empty($product->prod_slug)){ echo base_url('admin/product/').$product->prod_slug; }else{ echo "#"; } ?>" style="text-decoration:none; border-radius:3px; padding:5px 20px; background-color:#08BD51; color:#fefefe;">View Products</a>
                        <br>
                        <br>
                        <p> <div class="4">Lorem ipsum dolor sit amet, consectetur adipisicing elit?</div> <br>Keep up!<br> Farmgate Trader Admin<br></p>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding:30px; text-align:center; background-color:#fafafa; color:#222; font-family: 'Poppins', sans-serif; ">
                    <div style="display:block; margin-bottom:20px;">
                        <a href="https://www.facebook.com/farmcrowdy/"><img src="<?php echo base_url('mainasset/fe/images/facebook.png'); ?>" style="width:30px; height:30px;" alt="Facebook"></a>
                        <a href="https://www.twitter.com/farmcrowdy/"><img src="<?php echo base_url('mainasset/fe/images/twitter.png'); ?>" style="width:30px; height:30px;" alt="Twitter"></a>
                        <a href="https://www.instagram.com/farmcrowdyng/"><img src="<?php echo base_url('mainasset/fe/images/instagram.png'); ?>" style="width:30px; height:30px;" alt="Instagram"></a>
                    </div>
                    
                    <div><img src="img/farmgate.png" alt="Farmgate"></div>
                    <div>
                        <p style="font-size:0.7em;line-height:140%; margin-bottom:20px;"><div class="10">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sed explicabo consectetur rerum temporibus, placeat quae, numquam perferendis animi.</div> </p>
                        <p style="font-size:0.8em;line-height:140%;"><strong>Our address is: </strong><br> 14B Robert Nebolisa street, Idado Town, Lekki, Lagos <br> +2349075791999 <br> info@farmcrowdy.com </p>
                    </div>

                </td>
            </tr>
            <tr>
                <td style="height: 60px; ">
                    <div></div>
                </td>
            </tr>
        </tbody>
    </table>


</body>

</html>