<!DOCTYPE html>
<html>

<head>
    <title>Forgot Passsword</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">

</head>

<body style="width: 100% !important; -webkit-text-size-adjust: 100%; overflow-wrap:break-word; word-wrap:break-word;hyphens:auto; background: #ECECFF !important; -ms-text-size-adjust: 100%; margin: 0;
padding: 0; font-family: 'Poppins', sans-serif; line-height: 160%; font-size: .9em;color: #222;">

    <table style="margin: 0 auto; width:500px" cellspacing="0" cellpadding="0">
        <tbody>

            <tr>
                <td style="height: 60px; ">
                    <div></div>
                </td>
            </tr>
            <tr>
                <td style="background-color:#fafafa;">
                    <div style="display: block;	text-align:center; margin: 20px; ">
                        <div><img style="width:200px; height:auto;" src="<?php echo base_url('mainasset/fe/images/farmgate.png'); ?>" alt="Farmgate logo"></div>

                    </div>

                </td>
            </tr>
            <tr>
                <td style="background-color:#fff; font-family: 'Poppins', sans-serif; ">
                    <div style="display: block; margin: 0px; padding:30px; background-color:#fff; font-family: 'Poppins', sans-serif; ">
                        <h2 style="color:#08BD51; margin-bottom:30px;">Password Reset!</h2>
                        <p>
                            You have requested a password reset, please follow the link below to reset your password.
                        </p>
                        <p>
                            Please ignore this email if you did not request a password change.
                        </p>
        
                        <a href="<?php if(!empty($user->reset_password_link)){ echo $user->reset_password_link; }else{ echo "#"; }?>" style="text-align:center;text-decoration:none; border-radius:3px; padding:5px 20px; color:#222; background-color:#f7931e;">Reset Password</a>
                        <br><br>
                        <p>If you are unable to use the button above, please try this alternative by copying the link below into your address bar:</p>
                        <p><a style="text-decoration:none; color:#08BD51; word-wrap:break-word;word-break:break-word;" href="<?php if(!empty($user->reset_password_link)){ echo $user->reset_password_link; }else{ echo "#"; }?>"><?php if(!empty($user->reset_password_link)){ echo $user->reset_password_link; }else{ echo ""; }?></a>
                        </p>
                        <br>
                        <br>
                        <p>Connect with us</p>
                        <div style="display:block; margin-bottom:20px;">
                            <a href="https://web.facebook.com/farmgateafrica/" target="_blank"><img src="<?php echo base_url('mainasset/fe/images/facebook.png'); ?>"
                                    style="width:30px; height:30px;" alt="Facebook"></a>
                            <a href="https://www.twitter.com/farmgateafrica/" target="_blank"><img src="<?php echo base_url('mainasset/fe/images/twitter.png'); ?>"
                                    style="width:30px; height:30px;" alt="Twitter"></a>
                            <a href="https://www.instagram.com/farmgateafrica/" target="_blank"><img src="<?php echo base_url('mainasset/fe/images/instagram.png'); ?>"
                                    style="width:30px; height:30px;" alt="Instagram"></a>
                        </div>
                        <p><br> FarmGate Africa Team<br></p>
                        <p style="font-size:0.8em;line-height:140%; margin-bottom:20px; ">If you did not request
                            for password change, please ignore this email.</p>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding:30px; text-align:center; background-color:#fafafa; color:#222; font-family: 'Poppins', sans-serif; ">
                    <div>
                        <center>
                            <img style="width:200px; height:auto;" src="<?php echo base_url('mainasset/fe/images/farmgate.png'); ?>" alt="Farmgate">
                        </center>
                        <p>Innovating Market Access for Agriculture</p>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="height: 60px; ">
                    <div></div>
                </td>
            </tr>
        </tbody>
    </table>


</body>

</html>