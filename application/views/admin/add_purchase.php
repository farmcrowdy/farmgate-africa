        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">Purchases</li>
                                    <li class="breadcrumb-item active" aria-current="page">Add Purchase</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <div class="m-r-10">
                                <div class="lastmonth"></div>
                            </div>
                            <div class="">
                                <small>LAST MONTH</small>
                                <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Add Cluster -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-lg-10 col-md-12 col-11">
                    <?php echo $this->session->flashdata('purchase_feedback');?>
                    <?php if(!empty($error_feedback)){echo $error_feedback; }?>
                        <?php echo form_open('admin/add-purchase',array('class'=>'row acct-form mb-4', 'autocomplete'=>"off")); ?>
                            <div class="col-md-12 pad-dashboard">
                                <div class="row mt-5">
                                    <div class="col-md-6">
                                        <label for="">Farmer Name
                                            <span class="superscript-star">*</span>
                                        </label><small><em><span id="fm_search"></span></em></small>
                                        <?php echo form_error('farmer_name'); ?>
                                        <div class="col-md-2" id="farmer_name_loader" style="display:none;">
                                            <img src="<?php echo base_url('mainasset/fe/images/loader.gif'); ?>" width="30px" />
                                        </div>
                                        <input type="hidden" class="form-control" id="farmer_id" name="farmer_id" value="<?php echo set_value('farmer_id'); ?>" >
                                        <input type="text" class="form-control" id="farmer_name" name="farmer_name" aria-describedby="productName"
                                        value="<?php echo set_value('farmer_name'); ?>">
                                        <div class="" style="background-color:#fff;" id="suggestion-box"></div>
                                    </div>
                                    <div class="col-4 form-group">
                                        <label for=""> Collection Center </label>
										<select class="form-control" id="ccSelect" name="ccSelect">
                                           <?php if(!empty($collection_centers)): ?>
											<?php foreach ($collection_centers as $collection_center) :?>
											<option value="<?php echo $collection_center->cc_id; ?>">
												<?php echo ucwords($collection_center->cc_name); ?>
											</option>
											<?php endforeach ?>
                                            <?php endif ?>
										</select>
                                    </div>
                                </div>
                                <div class="row py-1">
                                    <div class="col-2 form-group">
                                        <label for=""> Produce Type</label>
										<select class="form-control" id="categoryTypeSelect" name="categoryTypeSelect">
											<?php foreach ($produce_types as $produce_type) :?>
											<option value="<?php echo $produce_type->pts_id; ?>">
												<?php echo ucwords($produce_type->pts_name); ?>
											</option>
											<?php endforeach ?>
										</select>
                                    </div>
                                    <div class="col-4 form-group">
                                        <label for=""> Produce</label>
                                        <select class="form-control" id="categorySelect" name="categorySelect">

										</select>
                                    </div>
                                    <div class="col-4 form-group">
                                        <label for="">Quantity (<span id="prod_quantity">MT</span>)
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('po_quantity'); ?>
                                        <input name="po_quantity" class="form-control" value="<?php echo trim(set_value('po_quantity')); ?>">
                                    </div>

                                </div>
                                <div class="row">
                                    <h4 class="page-breadcrumb page-title h4"> Grading </h4>
                                </div>
                                <div class="row">
                                    <div class="col-4 form-group">
                                        <label for="">Original Weight
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('original_weight'); ?>
                                        <input type="text" class="form-control" min="1" id="" name="original_weight" aria-describedby="productName"
                                        value="<?php echo set_value('original_weight'); ?>">
                                    </div>
                                    <div class="col-4 form-group">
                                        <label for="">Purity (%)</label>
                                        <?php echo form_error('purity'); ?>
                                        <input type="text" class="form-control" min="1" id="" name="purity" aria-describedby="productName"
                                        value="<?php echo set_value('purity'); ?>">
                                    </div>
                                    <div class="col-4 form-group">
                                        <label for="">Moisture Level</label>
                                        <?php echo form_error('moisture_level'); ?>
                                        <input type="text" class="form-control" min="1" id="" name="moisture_level" aria-describedby="productName"
                                        value="<?php echo set_value('moisture_level'); ?>">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4 form-group">
                                        <label for="">Foreign Material</label>
                                        <?php echo form_error('foreign_material'); ?>
                                        <input type="text" class="form-control" min="1" id="" name="foreign_material" aria-describedby="productName"
                                        value="<?php echo set_value('foreign_material'); ?>">
                                    </div>
                                    <div class="col-4 form-group">
                                        <label for="">Damaged Kernel</label>
                                        <?php echo form_error('damaged_kernel'); ?>
                                        <input type="text" class="form-control" min="1" id="" name="damaged_kernel" aria-describedby="productName"
                                        value="<?php echo set_value('damaged_kernel'); ?>">
                                    </div>
                                    <div class="col-4 form-group">
                                        <label for="">Hardness</label>
                                        <?php echo form_error('hardness'); ?>
                                        <input type="text" class="form-control" min="1" id="" name="hardness" aria-describedby="productName"
                                        value="<?php echo set_value('hardness'); ?>">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="">Color</label>
                                        <?php echo form_error('color'); ?>
                                        <input type="text" class="form-control" min="1" id="" name="color" aria-describedby="productName"
                                        value="<?php echo set_value('color'); ?>">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Final Weight
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('final_weight'); ?>
                                        <input type="text" class="form-control" min="1" id="" name="final_weight" aria-describedby="productName"
                                        value="<?php echo set_value('final_weight'); ?>">
                                    </div>
                                </div>
                                <div class="row">
                                    <h4 class="page-breadcrumb page-title h4"> Payment Information </h4>
                                </div>
                                <div class="row relative">
                                    <div class="col-md-4">
                                        <label for="">Currency
                                        </label>
										<select class="form-control" id="currencySelect" name="currencySelect">
											<?php foreach ($currencies as $currency) :?>
											<option value="<?php echo $currency->cur_id; ?>">
												<?php echo ucwords($currency->cur_name); ?>
											</option>
											<?php endforeach ?>
										</select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Amount
                                        </label>
                                        <?php echo form_error('po_amount'); ?>
                                        <input type="number" class="form-control" min="1" id="" name="po_amount" aria-describedby="productName"
                                        value="<?php echo set_value('po_amount'); ?>">
                                    </div>
                                </div>
                                <div class="row relative">
                                    <div class="col-md-4">
                                        <label for="">Pay Status
                                        </label>
                                        <select class="form-control" id="paySelect" name="paySelect">
											<?php foreach ($payment_statuses as $payment_status) :?>
											<option value="<?php echo $payment_status; ?>">
												<?php echo ucwords($payment_status); ?>
											</option>
											<?php endforeach ?>
										</select>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 pb-5 pt-4">
                                    <button name="submit" type="submit" class="btn  btn-submit"> Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            
