<!-- Page wrapper -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item"> Commodities</li>
                            <li class="breadcrumb-item active" aria-current="page">Commodity Details</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Orders -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-sm-12 col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs no-nav-border" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#specification" role="tab" aria-controls="order-specification" aria-selected="true">Commodity Details</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content card-body" id="myTabContent">
                        <div class="tab-pane fade show active" id="specification" role="tabpanel" aria-labelledby="order-specification-tab">
                            <table class="table no-table-border">
                                <tbody class=" no-table-border">
                                    <tr class=" no-table-border">
                                        <td class="product-title"> Name</td>
                                        <td><?php echo $commodity->comm_name; ?></a></td>
                                    </tr>
                                    <?php if(!empty($commodity->partner)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Partner :</td>
                                        <td><?php echo ucwords($commodity->partner); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($commodity->category)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Produce :</td>
                                        <td><?php echo ucwords($commodity->category); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($commodity->comm_commodity_type)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Commodity Type :</td>
                                        <td><?php echo ucwords($commodity->comm_commodity_type); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(isset($commodity->comm_quantity)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Quantity:</td>
                                        <td><?php echo number_format($commodity->comm_quantity); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(isset($commodity->comm_quantity_left)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Quantity Left:</td>
                                        <td><?php echo number_format($commodity->comm_quantity_left); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(isset($commodity->comm_quantity_left_to_buy)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Quantity Left To Buy:</td>
                                        <td><?php echo number_format($commodity->comm_quantity_left_to_buy); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(isset($commodity->comm_commodity_details)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Commodity Details:</td>
                                        <td><?php echo $commodity->comm_commodity_details; ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($commodity->comm_insurance_cover)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Insurance Cover:</td>
                                        <td><?php echo $commodity->comm_insurance_cover; ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($commodity->comm_due_date)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Due Date:</td>
                                        <td>
                                        <?php echo date('d/m/Y',strtotime($commodity->comm_due_date)); ?>
                                        </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($commodity->comm_date_created)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Created on:</td>
                                        <td>
                                        <?php echo date('d/m/Y',strtotime($commodity->comm_date_created)); ?>
                                        </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($commodity->manager)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Created By:</td>
                                        <td><?php echo $commodity->manager->full_name; ?> (<?php echo ucwords(str_replace('_',' ',$commodity->manager->type)); ?>)</td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($commodity->comm_date_updated)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Updated on:</td>
                                        <td>
                                        <?php echo date('d/m/Y',strtotime($commodity->comm_date_updated)); ?>
                                        </td>
                                    </tr>
                                    <?php endif ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Row -->
        </div>

        <!-- ============================================================== -->
        <!-- End Container fluid -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->