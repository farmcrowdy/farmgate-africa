<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item"> Commodities</li>
                            <li class="breadcrumb-item active" aria-current="page">Edit Commodity</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <div class="m-r-10">
                        <div class="lastmonth"></div>
                    </div>
                    <div class="">
                        <small>LAST MONTH</small>
                        <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Add Investment Commodities -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-lg-10 col-md-12 col-11">
                <?php echo $this->session->flashdata('commodity_feedback');?>
                <?php if(!empty($error_feedback)){echo $error_feedback; }?>
                <?php if(!empty($image_errors)){
                            echo ($image_errors); 
                        } 
                ?>
                <?php echo form_open_multipart('admin/edit-commodity/'.$commodity->comm_slug,array('class'=>'row acct-form mb-4', 'autocomplete'=>'off')); ?>
                <div class="col-md-12 pad-dashboard">
                    <div class="row pt-4 pb-2">
                        <div class="col-lg-12 upload-photo" id='image-preview'>
                            <img src="<?php echo base_url('mainasset/fe/images/commodities/').$commodity->comm_image; ?>" alt="commodity photo" class="img-fluid picture rounded">
                            <div class="user-placeholder <?php if(!empty($commodity->comm_image)){echo "hidden";} ?> ">.</div>
                            <label class="btn btn-default new-line" for="upload-file-selector">Upload Commodity image</label>
                            <input id="upload-file-selector" name="productImageFile" accept="image/jpeg,image/png" type="file" class="hidden">
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-md-4">
                            <label for=""> Name
                                <span class="superscript-star">*</span>
                            </label>
                            <?php echo form_error('name'); ?>
                            <input type="text" class="form-control" id="" name="name" aria-describedby="productName" 
                            disabled="disabled" value="<?php echo set_value('name', $commodity->comm_name); ?>">
                        </div>
                        <div class="col-md-3">
                            <label for=""> Status
                                <span class="superscript-star">*</span>
                            </label>
                            <?php echo form_error('status'); ?>
                            <select class="form-control" id="status" name="status">
                                <?php foreach ($statuses as $status) :?>
                                <option value="<?php echo $status; ?>">
                                    <?php echo ucwords(str_replace('_',' ',$status)); ?>
                                </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for=""> Type
                                <span class="superscript-star">*</span>
                            </label>
                            <?php echo form_error('type'); ?>
                            <select class="form-control" id="type" name="type">
                                <?php foreach ($types as $type) :?>
                                <option value="<?php echo $type; ?>">
                                    <?php echo ucwords($type); ?>
                                </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="row py-1">
                        <div class="col-2 form-group">
                            <label for=""> Produce Type</label>
                            <select class="form-control" id="categoryTypeSelect" name="categoryTypeSelect">
                                <?php foreach ($produce_types as $produce_type) :?>
                                <option value="<?php echo $produce_type->pts_id; ?>">
                                    <?php echo ucwords($produce_type->pts_name); ?>
                                </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-4 form-group">
                            <label for=""> Produce Category</label>
                            <select class="form-control" id="categorySelect" name="categorySelect">

                            </select>
                        </div>
                        <div class="col-4 form-group">
                            <label for=""> Partner </label>
                            <select class="form-control" id="partner_id" name="partner_id">
                                <option value="0">None</option>
                                <?php foreach ($partners as $partner) :?>
                                <option value="<?php echo $partner->id; ?>" <?php if($commodity->comm_partner_id == $partner->id){echo "selected"; }?>>
                                    <?php echo ucwords($partner->name); ?>
                                </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-4 h-100">
                            <label for="">Quantity
                                <span class="superscript-star">*</span>
                            </label>
                            <?php echo form_error('quantity'); ?>
                            <input type="number" class="form-control" name="quantity" id="" aria-describedby="Quantity" placeholder="" value="<?php echo set_value('quantity', $commodity->comm_quantity); ?>">
                        </div>
                        <div class="col-md-4 col-4  h-100">
                            <label for="">Quantity Left To Trade
                                <span class="superscript-star">*</span>
                            </label>
                            <?php echo form_error('quantity_left'); ?>
                            <input type="number" class="form-control" name="quantity_left" id="" aria-describedby="Quantity" placeholder="" value="<?php echo set_value('quantity_left', $commodity->comm_quantity_left); ?>">
                        </div>
                        <div class="col-md-4 col-4  h-100">
                            <label for="">Quantity Left To Buy
                                <span class="superscript-star">*</span>
                            </label>
                            <?php echo form_error('quantity_left_to_buy'); ?>
                            <input type="number" class="form-control" name="quantity_left_to_buy" id="" aria-describedby="Quantity" placeholder="" value="<?php echo set_value('quantity_left_to_buy', $commodity->comm_quantity_left_to_buy); ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-4 h-100">
                            <label for="unitSelect">Unit Type</label>
                            <?php echo form_error('unit_id'); ?>
                            <select class="form-control" id="unitSelect" name="unitSelect">
                                <?php foreach ($units as $unit) :?>
                                <option value="<?php echo $unit->ut_id; ?>">
                                    <?php echo $unit->ut_name; ?>
                                </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class=" col-md-4 col-4 form-group">
                            <label for="">Price per Unit
                                <span class="superscript-star">*</span>
                            </label>
                            <?php echo form_error('comm_price_per_unit'); ?>
                            <input type="number" class="form-control" id="comm_price_per_unit" name="comm_price_per_unit" value="<?php echo set_value('comm_price_per_unit', $commodity->comm_price_per_unit); ?>">
                        </div>
                        <div class=" col-md-4 col-4 form-group">
                            <label for="">Cycle (Months)
                                <span class="superscript-star">*</span>
                            </label>
                            <?php echo form_error('comm_cycle'); ?>
                            <input type="number" class="form-control" id="comm_cycle" name="comm_cycle" value="<?php echo set_value('comm_cycle', $commodity->comm_cycle); ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class=" col-md-9 col-9 form-group">
                            <label for="comm_google_sheet_link">Google Sheets Data Link
                                <span class="superscript-star">*</span>
                            </label>
                            <?php echo form_error('comm_google_sheet_link'); ?>
                            <input type="text" class="form-control" id="comm_google_sheet_link" name="comm_google_sheet_link" value="<?php echo set_value('comm_google_sheet_link', $commodity->comm_google_sheet_link); ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class=" col-12  form-group">
                            <label for="">Commodity Details
                                <span class="superscript-star">*</span>
                            </label>
                            <?php echo form_error('comm_commodity_details'); ?>
                            <textarea id="cd-editor" name="comm_commodity_details" class="form-control" rows="5" aria-label="With textarea"><?php echo trim(set_value('comm_commodity_details', $commodity->comm_commodity_details)); ?></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class=" col-12  form-group">
                            <label for="">Insurance Cover
                                <span class="superscript-star">*</span>
                            </label>
                            <?php echo form_error('comm_insurance_cover'); ?>
                            <textarea id="ic-editor" name="comm_insurance_cover" class="form-control" rows="5" aria-label="With textarea"><?php echo trim(set_value('comm_insurance_cover', $commodity->comm_insurance_cover)); ?></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12  pb-5 pt-4">
                            <button name="submit" type="submit" class="btn  btn-submit"> Submit</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->