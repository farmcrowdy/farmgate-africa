
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <?php 
                            $loggedInUserID = $this->session->userdata('uid');
                            $user = $this->user_model->getUser($loggedInUserID);
                            // var_dump($user->type);
                            $is_admin = ($user->type == "admin") ? true : false;
                            $is_cc_manager = ($user->type == "collection_center_manager") ? true : false;
                            $is_wh_manager = ($user->type == "warehouse_manager") ? true : false;

                        ?>
                        <li class="sidebar-item pt-2">
                            <a class="sidebar-link waves-effect waves-dark" href="<?php echo base_url('admin/index');?>" aria-expanded="false">
                                <i class="mdi mdi-view-dashboard"></i>
                                <span class="hide-menu">Dashboard </span>
                            </a>
                        </li>
                        <?php if($is_admin) : ?>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark" href="<?php echo base_url('admin/clusters');?>" aria-expanded="false">
                            <i class="far fa-snowflake"></i>
                                <span class="hide-menu">Clusters </span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a class="sidebar-link " href="<?php echo base_url('admin/clusters');?>">
                                        <i class="fas fa-users"></i>
                                        <span class="hide-menu">All Clusters </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="<?php echo base_url('admin/add-cluster');?>" class="sidebar-link">
                                        <i class="fas fa-user-plus"></i>
                                        <span class="hide-menu">Add Cluster </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php endif ?>
                        <?php if($is_admin || $is_cc_manager): ?>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark" href="<?php echo base_url('admin/collection-centers');?>" aria-expanded="false">
                            <i class="fas fa-shopping-basket"></i>
                                <span class="hide-menu">Collection Centers </span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a class="sidebar-link " href="<?php echo base_url('admin/collection-centers');?>">
                                        <i class="fas fa-users"></i>
                                        <span class="hide-menu">All Collection Centers </span>
                                    </a>
                                </li>
                                <?php if($is_admin): ?>
                                <li class="sidebar-item">
                                    <a href="<?php echo base_url('admin/add-collection-center');?>" class="sidebar-link">
                                        <i class="fas fa-user-plus"></i>
                                        <span class="hide-menu">Add Collection Center </span>
                                    </a>
                                </li>
                                <?php endif ?>
                            </ul>
                        </li>
                        <?php endif ?>
                        <?php if($is_admin || $is_wh_manager): ?>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark" href="<?php echo base_url('admin/warehouses');?>" aria-expanded="false">
                                <i class="fas fa-warehouse"></i>
                                <span class="hide-menu">Warehouses </span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a class="sidebar-link " href="<?php echo base_url('admin/warehouses');?>">
                                        <i class="fas fa-users"></i>
                                        <span class="hide-menu">All Warehouses </span>
                                    </a>
                                </li>
                                <?php if($is_admin): ?>
                                <li class="sidebar-item">
                                    <a href="<?php echo base_url('admin/add-warehouse');?>" class="sidebar-link">
                                        <i class="fas fa-user-plus"></i>
                                        <span class="hide-menu">Add Warehouse </span>
                                    </a>
                                </li>
                                <?php endif ?>
                            </ul>
                        </li>
                        <?php endif ?>
                        <?php if($is_admin || $is_cc_manager): ?>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark" href="<?php echo base_url('admin/farmers');?>" aria-expanded="false">
                                <i class="fas fa-users"></i>
                                <span class="hide-menu">Farmers </span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a class="sidebar-link " href="<?php echo base_url('admin/farmers');?>">
                                        <i class="fas fa-users"></i>
                                        <span class="hide-menu">All Farmers </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="<?php echo base_url('admin/add-farmer');?>" class="sidebar-link">
                                        <i class="fas fa-user-plus"></i>
                                        <span class="hide-menu">Add Farmer </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php endif ?>
                        <?php if($is_admin) : ?>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark" href="<?php echo base_url('admin/investments');?>" aria-expanded="false">
                            <i class="far fa-snowflake"></i>
                                <span class="hide-menu">Investments </span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a class="sidebar-link " href="<?php echo base_url('admin/investments');?>">
                                        <i class="fas fa-users"></i>
                                        <span class="hide-menu">All Investments </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="<?php echo base_url('admin/add-investment');?>" class="sidebar-link">
                                        <i class="fas fa-user-plus"></i>
                                        <span class="hide-menu">Add Investment </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php endif ?>
                        <?php if($is_admin) : ?>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark" href="<?php echo base_url('admin/cpurchases');?>" aria-expanded="false">
                            <i class="far fa-snowflake"></i>
                                <span class="hide-menu">Customer Purchases </span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a class="sidebar-link " href="<?php echo base_url('admin/cpurchases');?>">
                                        <i class="fas fa-users"></i>
                                        <span class="hide-menu">All Customer Purchases </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="<?php echo base_url('admin/add-cpurchase');?>" class="sidebar-link">
                                        <i class="fas fa-user-plus"></i>
                                        <span class="hide-menu">Add Customer Purchase </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php endif ?>
                        <?php if($is_admin) : ?>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                            <i class="fas fa-shopping-basket"></i>
                                <span class="hide-menu">Commodities </span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a class="sidebar-link " href="<?php echo base_url('admin/commodities/');?>">
                                        <i class="fas fa-users"></i>
                                        <span class="hide-menu">All Commodities </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a class="sidebar-link " href="<?php echo base_url('admin/commodities/?comm_type=trade');?>">
                                        <i class="fas fa-users"></i>
                                        <span class="hide-menu">All Investment Commodities </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a class="sidebar-link " href="<?php echo base_url('admin/commodities/?comm_type=purchase');?>">
                                        <i class="fas fa-users"></i>
                                        <span class="hide-menu">All Purchase Commodities </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="<?php echo base_url('admin/add-commodity');?>" class="sidebar-link">
                                        <i class="fas fa-user-plus"></i>
                                        <span class="hide-menu">Add Commodity </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php endif ?>
                        <?php if($is_admin || $is_cc_manager): ?>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark" href="<?php echo base_url('admin/banks');?>" aria-expanded="false">
                                <i class="fas fa-university"></i>
                                <span class="hide-menu">Banks </span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a class="sidebar-link " href="<?php echo base_url('admin/banks');?>">
                                        <i class="fas fa-university"></i>
                                        <span class="hide-menu">All Banks </span>
                                    </a>
                                </li>
                                <?php if($is_admin): ?>
                                <li class="sidebar-item">
                                    <a href="<?php echo base_url('admin/add-bank');?>" class="sidebar-link">
                                        <i class="fas fa-user-plus"></i>
                                        <span class="hide-menu">Add Bank </span>
                                    </a>
                                </li>
                                <?php endif ?>
                            </ul>
                        </li>
                        <?php endif ?>
                        <?php if($is_admin || $is_cc_manager): ?>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark" href="<?php echo base_url('admin/purchases');?>" aria-expanded="false">
                            <i class="fas fa-shopping-bag"></i>
                                <span class="hide-menu">Purchases </span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a class="sidebar-link  waves-effect waves-dark" href="<?php echo base_url('admin/purchases');?>" aria-expanded="false">
                                        <i class="mdi mdi-inbox-arrow-down"></i>
                                        <span class="hide-menu"> All Purchases </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="<?php echo base_url('admin/add-purchase');?>" class="sidebar-link">
                                        <i class="mdi mdi-book-plus"></i>
                                        <span class="hide-menu"> New Purchase </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php endif ?>
                        <?php if($is_admin): ?>
                        <li class="sidebar-item pt-2">
                            <a class="sidebar-link waves-effect waves-dark" href="<?php echo base_url('admin/inventory');?>" aria-expanded="false">
                            <i class="fab fa-elementor"></i>
                                <span class="hide-menu">Inventory </span>
                            </a>
                        </li>
                        <?php endif ?>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark" href="<?php echo base_url('admin/logistics');?>" aria-expanded="false">
                            <i class="fas fa-truck"></i>
                                <span class="hide-menu">Logistics </span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                            <?php if($is_admin || $is_cc_manager): ?>
                                <li class="sidebar-item">
                                    <a href="<?php echo base_url('admin/produce-transfers');?>" class="sidebar-link">
                                        <i class="fas fa-user-plus"></i>
                                        <span class="hide-menu">Produce Transfers </span>
                                    </a>
                                </li>

                                <li class="sidebar-item">
                                    <a href="<?php echo base_url('admin/add-produce-transfer');?>" class="sidebar-link">
                                        <i class="fas fa-user-plus"></i>
                                        <span class="hide-menu">New Produce Transfer </span>
                                    </a>
                                </li>
                                <?php endif ?>
                                <?php if($is_admin || $is_wh_manager): ?>
                                <li class="sidebar-item">
                                    <a href="<?php echo base_url('admin/outgoing-orders');?>" class="sidebar-link">
                                       <i class="far fa-paper-plane"></i>
                                        <span class="hide-menu">Outgoing Orders </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="<?php echo base_url('admin/add-outgoing-order');?>" class="sidebar-link">
                                        <i class="fas fa-user-plus"></i>
                                        <span class="hide-menu">New Outgoing Order </span>
                                    </a>
                                </li>
                                <?php endif ?>
                            </ul>
                        </li>
                        <?php if($is_admin): ?>
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark" href="<?php echo base_url('admin/users');?>" aria-expanded="false">
                                <i class="fas fa-users"></i>
                                <span class="hide-menu">Users </span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a class="sidebar-link " href="<?php echo base_url('admin/users');?>">
                                        <i class="fas fa-users"></i>
                                        <span class="hide-menu"> All Users </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="<?php echo base_url('admin/users?type=user');?>" class="sidebar-link">
                                        <i class="fas fa-user-plus"></i>
                                        <span class="hide-menu">Users </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="<?php echo base_url('admin/users?type=warehouse_manager');?>" class="sidebar-link">
                                        <i class="fas fa-user-plus"></i>
                                        <span class="hide-menu">Warehouse Managers </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="<?php echo base_url('admin/users?type=collection_center_manager');?>" class="sidebar-link">
                                        <i class="fas fa-user-plus"></i>
                                        <span class="hide-menu">Collection Center Managers </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="<?php echo base_url('admin/users?type=sales_manager');?>" class="sidebar-link">
                                        <i class="fas fa-user-plus"></i>
                                        <span class="hide-menu">Sales Managers </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="<?php echo base_url('admin/add-user');?>" class="sidebar-link">
                                        <i class="fas fa-user-plus"></i>
                                        <span class="hide-menu">Add User </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php endif ?>
                        <?php if($is_admin): ?>
                        <!-- <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark" href="<?php // echo base_url('admin/products');?>" aria-expanded="false">
                            <i class="fas fa-shopping-bag"></i>
                                <span class="hide-menu">Products </span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item">
                                    <a class="sidebar-link  waves-effect waves-dark" href="<?php // echo base_url('admin/products');?>" aria-expanded="false">
                                        <i class="mdi mdi-inbox-arrow-down"></i>
                                        <span class="hide-menu"> All Products </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="<?php // echo base_url('admin/add-product');?>" class="sidebar-link">
                                        <i class="mdi mdi-book-plus"></i>
                                        <span class="hide-menu"> Add Product </span>
                                    </a>
                                </li>
                            </ul>
                        </li> -->
                        <li class="sidebar-item">
                            <a class="sidebar-link  waves-effect waves-dark" href="<?php echo base_url('admin/contacts');?>" aria-expanded="false">
                            <i class="far fa-paper-plane"></i>
                                <span class="hide-menu">Contacts </span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link  waves-effect waves-dark" href="<?php echo base_url('admin/category');?>" aria-expanded="false">
                                <i class="fas fa-plus-circle"></i>
                                <span class="hide-menu"> New Category</span>
                            </a>
                        </li>
                        <!-- <li class="sidebar-item">
                            <a class="sidebar-link  waves-effect waves-dark" href="<?php echo base_url('admin/messages');?>" aria-expanded="false">
                                <i class="far fa-envelope"></i>
                                <span class="hide-menu">Messages </span>
                            </a>
                        </li> -->
                        <li class="sidebar-item">
                            <a class="sidebar-link  waves-effect waves-dark" href="<?php echo base_url('admin/logs');?>" aria-expanded="false">
                                <i class="far fa-snowflake"></i>
                                <span class="hide-menu">Logs </span>
                            </a>
                        </li>
                        <?php endif ?>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('users/logout');?>" aria-expanded="false">
                                <i class="mdi mdi-directions"></i>
                                <span class="hide-menu">Log Out</span>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
