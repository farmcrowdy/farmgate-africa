<footer class="footer text-center">
    All Rights Reserved by Farmtable admin.
</footer>
<!-- ============================================================== -->
<!-- End footer -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
</div>

<div class="chat-windows"></div>
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="<?php echo base_url();?>mainasset/be/assets/libs/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="<?php echo base_url();?>mainasset/be/assets/libs/popper.js/dist/umd/popper.min.js"></script>
<script src="<?php echo base_url();?>mainasset/be/assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- apps -->
<script src="<?php echo base_url();?>mainasset/be/dist/js/app.min.js"></script>
<script src="<?php echo base_url();?>mainasset/be/dist/js/app.init.light-sidebar.js"></script>
<script src="<?php echo base_url();?>mainasset/be/dist/js/app-style-switcher.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="<?php echo base_url();?>mainasset/be/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
<!-- <script src="<?php echo base_url();?>mainasset/be/assets/extra-libs/sparkline/sparkline.js"></script> -->
<!--Wave Effects -->
<script src="<?php echo base_url();?>mainasset/be/dist/js/waves.js"></script>
<!--Menu sidebar -->
<script src="<?php echo base_url();?>mainasset/be/dist/js/sidebarmenu.js"></script>
<!--Custom JavaScript -->
<script src="<?php echo base_url();?>mainasset/be/dist/js/custom.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
<!--Custom JavaScript - Order -->
<script src="<?php echo base_url();?>mainasset/be/dist/js/custom_order.js"></script>
<!-- <script src="<?php echo base_url();?>mainasset/be/dist/js/lightbox.min.js"></script> -->
<!--This page JavaScript -->
<!--Morris JavaScript -->
<!--c3 charts -->
<script src="<?php echo base_url();?>mainasset/be/assets/extra-libs/c3/d3.min.js"></script>
<script src="<?php echo base_url();?>mainasset/be/assets/extra-libs/c3/c3.min.js"></script>
<script src="<?php echo base_url();?>mainasset/be/dist/js/pages/dashboards/dashboard5.js"></script>
<!-- Order Status modal -->
<div class="modal fade order-status-modal contact-manager-modal mt-5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg  ">
		<form class="modal-content" id="updateOrderForm">
            <input type="hidden" name="order_number" id="order_number" value="" />
			<div class="modal-header">
				<h5 class="modal-title pl-md-5 ml-md-3" id="changePasswordLabel">Update Order Status</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body container mt-3">
				<center style="color: #08BD51;">
					<div id="ordstatus_feedback"></div>
				</center>
				<div class="row py-2">
					<div class="col-12 col-md-10 offset-md-1">
						<label for="">Current Status</label>
						<p id="current_status">will be here</p>
					</div>
				</div>
				<div class="row py-2">
					<div class="col-12 col-md-10 offset-md-1">
						<label for="">Change Status</label>
						<select name="orderStatusSelect" class="form-control" id="orderStatusSelect">
						<?php if(!empty($statuses)): ?>
                        <?php foreach ($statuses as $status) :?>
                            <option value="<?php echo $status; ?>"> 
                            <?php echo strtoupper(str_replace('_',' ',$status)); ?> 
                            </option>
                        <?php endforeach ?>
						<?php endif ?>
						</select>
					</div>
				</div>
				<div id="lpo_order" class="row py-2 " style="display:none;">
					<div class="col-12 col-md-10 offset-md-1">
						<label for="">Amount</label>
						<input type="number" name="order_total" class="form-control" id="order_total" />
					</div>
				</div>
			</div>
			<div class="modal-footer d-flex justify-content-center">
				<button type="button" class="btn btn-secondary px-4 py-2" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-profile py-2 px-4">Submit</button>
			</div>
		</form>
	</div>
</div>
<!-- End Order Status Modal -->
