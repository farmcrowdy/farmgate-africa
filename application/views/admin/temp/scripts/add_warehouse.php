<script type="text/javascript">

$(document).ready(function(){

    $('#manager_name').keyup( function(){

        $.ajax({
            url: "<?php echo base_url('admin/wh_managers'); ?>",
            type: 'POST',
            data:  {mgr_name : $(this).val()},
            dataType: 'json',
            beforeSend: function(){
                $("#manager_name_loader").fadeIn(200).show();
            },
            success: function(resp)
            {
                if(resp.managers.length > 0){
                    $("#manager_name_loader").fadeIn(300).hide();
                    $("#mgr_search").text("");
                    $("#suggestion-box").show();
                    $("#suggestion-box").fadeIn(300).html(populateSuggestions(resp.managers));
                    $("#manager_name").css("background","#FFF");
                }else if(resp.managers.length === 0){
                    $("#mgr_search").text("No Manager found...");
                }
            },
            error: function(err)
            {
                $("#manager_name_loader").fadeIn(300).hide();
            }

        });
    });



function populateSuggestions(managers)
{
    var html = '<ul style="background-color: #9ceabb;">';
    for (var i = 0; i < managers.length; i++) {
        html += '<li style="list-style:none; background-color: #9ceabb;" onClick="selectManager(\''+managers[i].full_name+'\','+managers[i].id+');">'+managers[i].full_name+'</li>';
    }
    html +='</ul>';
    return html;
}



});
// End of Document Ready Function

function selectManager(manager,id)
{
    $("#manager_name").val(manager);
    $("#manager_id").val(id);
    $("#suggestion-box").fadeIn(300).hide();
    $("#manager_name_loader").fadeIn(300).hide();
}

</script>