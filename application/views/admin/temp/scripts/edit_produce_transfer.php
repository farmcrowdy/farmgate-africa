<script type="text/javascript">

$(document).ready(function(){

    // Get the default outgoing_order values
    var produce_type_id = '<?php if(!empty($produce_transfer->pt_produce_type_id)) {echo trim($produce_transfer->pt_produce_type_id); } else {echo "";} ?>';
    var produce_id = '<?php if(!empty($produce_transfer->pt_produce_id)) {echo trim($produce_transfer->pt_produce_id); } else {echo "";} ?>';
    var currency_id   = '<?php if(!empty($produce_transfer->pt_currency_id)) {echo trim($produce_transfer->pt_currency_id); } else {echo "";} ?>';
    var cc_id   = '<?php if(!empty($produce_transfer->pt_current_location_id)) {echo trim($produce_transfer->pt_current_location_id); } else {echo "";} ?>';
    var wh_id   = '<?php if(!empty($produce_transfer->pt_destination_id)) {echo trim($produce_transfer->pt_destination_id); } else {echo "";} ?>';
    var pay_status  = '<?php if(!empty($produce_transfer->pt_status)) {echo trim($produce_transfer->pt_status); } else {echo "";} ?>';
    var whmStatus  = '<?php if(!empty($produce_transfer->pt_wh_received)) {echo trim($produce_transfer->pt_wh_received); } else {echo "";} ?>';
    var adminStatus  = '<?php if(!empty($produce_transfer->pt_admin_approved)) {echo trim($produce_transfer->pt_admin_approved); } else {echo "";} ?>';



    // Set the values for the dropdowns
    $('#categoryTypeSelect').find("option[value="+produce_type_id+"]").attr("selected","selected");
    // Initial Loading of Produce
    loadProduce();
    $('#categoryTypeSelect').prop('disabled', true);
    $('#currencySelect').find("option[value="+currency_id+"]").attr("selected","selected");  
    $('#ccSelect').find("option[value="+cc_id+"]").attr("selected","selected")
    $('#ccSelect').prop('disabled', true);
    $('#whSelect').find("option[value="+wh_id+"]").attr("selected","selected")
    $('#whSelect').prop('disabled', true);
    $('#paySelect').find("option[value="+pay_status+"]").attr("selected","selected");
    $('#whmSelect').find("option[value="+whmStatus+"]").attr("selected","selected");
    $('#adminSelect').find("option[value="+adminStatus+"]").attr("selected","selected");
    $('#pt_quantity').prop('disabled', true);




  //Load the Produce given the Produce Type ID
  $('#categoryTypeSelect').change(loadProduce);

  function loadProduce()
  {
    var produce_type_id = $("#categoryTypeSelect option:selected").val();

    // Set the Quantity field label and type
    if(produce_type_id === "1"){
       $('#prod_quantity').empty().text('MT');
    }else{
       $('#prod_quantity').empty().text('#');
    }

    $.ajax({
        url: "<?php echo base_url('admin/produce-categories'); ?>",
        type: 'POST',
        data:  {"produce_type_id": produce_type_id},
        dataType: 'json',
        success: function(resp)
        {
            if (resp.code == 1) {
                var categories = resp.categories;
                if(categories.length >= 0){
                $('#categorySelect').empty();
                }
                for (var i = 0; i < categories.length; i++) {
                    $('#categorySelect').append($('<option>', { value: categories[i].c_id, text: categories[i].c_name }));
                }
                $('#categorySelect').find("option[value="+produce_id+"]").attr("selected","selected");
                $('#categorySelect').prop('disabled', true);
                
            }
        },
        error: function(err) 
        {
            console.log(err);
        }  

    });

  }


});
// End of Document Ready Function
</script>