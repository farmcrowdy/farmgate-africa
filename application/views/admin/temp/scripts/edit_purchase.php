<script type="text/javascript">

$(document).ready(function(){

    // Get the default purchase order values
    var produce_type_id = '<?php if(!empty($purchase->po_produce_type_id)) {echo trim($purchase->po_produce_type_id);} else { echo "";} ?>';
    var produce_id = '<?php if(!empty($purchase->po_produce_id)) {echo trim($purchase->po_produce_id);} else { echo "";} ?>';
    var currency_id   = '<?php if(!empty($purchase->po_currency_id)) {echo trim($purchase->po_currency_id); } else {echo "";} ?>';
    var cc_id   = '<?php if(!empty($purchase->po_collection_center_id)) {echo trim($purchase->po_collection_center_id); } else {echo "";} ?>';
    var pay_status  = '<?php if(!empty($purchase->po_status)) {echo trim($purchase->po_status); } else {echo "";} ?>';
    var delivery_status  = '<?php if(!empty($purchase->po_delivery_status)) {echo trim($purchase->po_delivery_status); } else {echo "";} ?>';

    // Set the values for the dropdowns
    $('#categoryTypeSelect').find("option[value="+produce_type_id+"]").attr("selected","selected");
    // Initial Loading of Produce
    loadProduce(); 
    $('#currencySelect').find("option[value="+currency_id+"]").attr("selected","selected");
    $('#ccSelect').find("option[value="+cc_id+"]").attr("selected","selected");
    $('#paySelect').find("option[value="+pay_status+"]").attr("selected","selected");
    if(pay_status === 'paid'){
        $('#paySelect').prop('disabled', true);
    }
    $('#deliverySelect').find("option[value="+delivery_status+"]").attr("selected","selected");



    $('#farmer_name').keyup( function(){

        $.ajax({
            url: "<?php echo base_url('admin/get_farmers'); ?>",
            type: 'POST',
            data:  {fm_name : $(this).val()},
            dataType: 'json',
            beforeSend: function(){
                $("#farmer_name_loader").fadeIn(200).show();
            },
            success: function(resp)
            {
                if(resp.farmers.length > 0){
                    $("#farmer_name_loader").fadeIn(300).hide();
                    $("#fm_search").text("");
                    $("#suggestion-box").show();
                    $("#suggestion-box").fadeIn(300).html(populateSuggestions(resp.farmers));
                    $("#farmer_name").css("background","#FFF");
                }else if(resp.farmers.length === 0){
                    $("#fm_search").text("No Active Farmer found with that entry...");
                }
            },
            error: function(err)
            {
                $("#farmer_name_loader").fadeIn(300).hide();
            }

        });
    });


function populateSuggestions(farmers)
{
    var html = '<ul style="background-color: #9ceabb;">';
    for (var i = 0; i < farmers.length; i++) {
        html += '<li style="list-style:none; background-color: #9ceabb;" onClick="selectFarmer(\''+farmers[i].fm_full_name+'\','+farmers[i].fm_id+');">'+farmers[i].fm_full_name+' : ID-'+ farmers[i].fm_id+' : Phone-'+farmers[i].fm_phone+' </li>';
    }
    html +='</ul>';
    return html;
}

  //Load the Produce given the Produce Type ID
  $('#categoryTypeSelect').change(loadProduce);

  function loadProduce()
  {
    var produce_type_id = $("#categoryTypeSelect option:selected").val();

    // Set the Quantity field label and type
    if(produce_type_id === "1"){
       $('#prod_quantity').empty().text('MT');
    }else{
       $('#prod_quantity').empty().text('#');
    }

    $.ajax({
        url: "<?php echo base_url('admin/produce-categories'); ?>",
        type: 'POST',
        data:  {"produce_type_id": produce_type_id},
        dataType: 'json',
        success: function(resp)
        {
            if (resp.code == 1) {
                var categories = resp.categories;
                if(categories.length >= 0){
                $('#categorySelect').empty();
                }
                for (var i = 0; i < categories.length; i++) {
                    $('#categorySelect').append($('<option>', { value: categories[i].c_id, text: categories[i].c_name }));
                }
                $('#categorySelect').find("option[value="+produce_id+"]").attr("selected","selected");
            }
        },
        error: function(err) 
        {
            console.log(err);
        }  

    });

  }


});
// End of Document Ready Function

function selectFarmer(farmer,id)
{
    $("#farmer_name").val(farmer);
    $("#farmer_id").val(id);
    $("#suggestion-box").fadeIn(300).hide();
    $("#farmer_name_loader").fadeIn(300).hide();
}

</script>