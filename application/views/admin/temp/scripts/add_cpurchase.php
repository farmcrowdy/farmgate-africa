<script type="text/javascript">

$(document).ready(function(){

    $('#investor_name').keyup( function(){

        $.ajax({
            url: "<?php echo base_url('admin/get_investors'); ?>",
            type: 'POST',
            data:  {"investor_name" : $(this).val()},
            dataType: 'json',
            beforeSend: function(){
                $("#investor_name_loader").fadeIn(200).show();
            },
            success: function(resp)
            {
                if(resp.investors.length > 0){
                    $("#investor_name_loader").fadeIn(300).hide();
                    $("#cusp_search").text("");
                    $("#suggestion-box").empty().show();
                    $("#suggestion-box").fadeIn(300).html(populateSuggestions(resp.investors));
                    $("#investor_name").css("background","#FFF");
                }else if(resp.investors.length === 0){
                    $("#cusp_search").text("No Active Investor found with that entry...");
                }
            },
            error: function(err)
            {
                $("#investor_name_loader").fadeIn(300).hide();
            }

        });
    });


function populateSuggestions(investors)
{
    var html = '<ul style="background-color: #9ceabb;">';
    for (var i = 0; i < investors.length; i++) {
        html += '<li style="list-style:none; background-color: #9ceabb;" onClick="selectInvestor(\''+investors[i].full_name+'\','+investors[i].id+');">'+investors[i].full_name+' : ID-'+ investors[i].id+' : Phone-'+investors[i].phone+' </li>';
    }
    html +='</ul>';
    return html;
}

});
// End of Document Ready Function

function selectInvestor(investor,id)
{
    $("#investor_name").val(investor);
    $("#investor_id").val(id);
    $("#suggestion-box").fadeIn(300).hide();
    $("#investor_name_loader").fadeIn(300).hide();
}

</script>