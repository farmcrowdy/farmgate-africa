<script type="text/javascript">

    // Get the default outgoing_order values
    var produce_type_id = '<?php if(!empty($investment->iv_produce_type_id)) {echo trim($investment->iv_produce_type_id); } else {echo "";} ?>';
    var produce_id = '<?php if(!empty($investment->iv_produce_id)) {echo trim($investment->iv_produce_id); } else {echo "";} ?>';
    var currency_id   = '<?php if(!empty($investment->iv_currency_id)) {echo trim($investment->iv_currency_id); } else {echo "";} ?>';
    var pay_status  = '<?php if(!empty($investment->iv_pay_status)) {echo trim($investment->iv_pay_status); } else {echo "";} ?>';
    var returnStatus  = '<?php if(!empty($investment->iv_returns_status)) {echo trim($investment->iv_returns_status); } else {echo "";} ?>';
    var commodity_id = '<?php if(!empty($investment->iv_commodity_id)) {echo trim($investment->iv_commodity_id); } else {echo "";} ?>';



    // Set the values for the dropdowns
    $('#categoryTypeSelect').find("option[value="+produce_type_id+"]").attr("selected","selected");
    // Initial Loading of Produce
    loadProduce();
    $('#categoryTypeSelect').prop('disabled', true);
    $('#commoditySelect').find("option[value="+commodity_id+"]").attr("selected","selected").prop('disabled',true); 
    $('#currencySelect').find("option[value="+currency_id+"]").attr("selected","selected");  
    $('#paySelect').find("option[value="+pay_status+"]").attr("selected","selected");
    if(pay_status === 'paid'){
        $('#paySelect').prop('disabled', true);
    }
    $('#returnSelect').find("option[value="+returnStatus+"]").attr("selected","selected");
    if(returnStatus === 'paid'){
        $('#returnSelect').prop('disabled', true);
    }
    $('#iv_quantity').prop('disabled', true);



$(document).ready(function(){

    $('#investor_name').keyup( function(){

        $.ajax({
            url: "<?php echo base_url('admin/get_investors'); ?>",
            type: 'POST',
            data:  {"investor_name" : $(this).val()},
            dataType: 'json',
            beforeSend: function(){
                $("#investor_name_loader").fadeIn(200).show();
            },
            success: function(resp)
            {
                if(resp.investors.length > 0){
                    $("#investor_name_loader").fadeIn(300).hide();
                    $("#iv_search").text("");
                    $("#suggestion-box").show();
                    $("#suggestion-box").fadeIn(300).html(populateSuggestions(resp.investors));
                    $("#investor_name").css("background","#FFF");
                }else if(resp.investors.length === 0){
                    $("#iv_search").text("No Active Investor found with that entry...");
                }
            },
            error: function(err)
            {
                $("#investor_name_loader").fadeIn(300).hide();
            }

        });
    });


function populateSuggestions(investors)
{
    var html = '<ul style="background-color: #9ceabb;">';
    for (var i = 0; i < investors.length; i++) {
        html += '<li style="list-style:none; background-color: #9ceabb;" onClick="selectInvestor(\''+investors[i].full_name+'\','+investors[i].id+');">'+investors[i].full_name+' : ID-'+ investors[i].id+' : Phone-'+investors[i].phone+' </li>';
    }
    html +='</ul>';
    return html;
}

//Load the Produce given the Produce Type ID
$('#categoryTypeSelect').change(loadProduce);

});
// End of Document Ready Function
function loadProduce()
  {
    var produce_type_id = $("#categoryTypeSelect option:selected").val();

    // Set the Quantity field label and type
    if(produce_type_id === "1"){
       $('#prod_quantity').empty().text('MT');
    }else{
       $('#prod_quantity').empty().text('#');
    }

    $.ajax({
        url: "<?php echo base_url('admin/produce-categories'); ?>",
        type: 'POST',
        data:  {"produce_type_id": produce_type_id},
        dataType: 'json',
        success: function(resp)
        {
            if (resp.code == 1) {
                var categories = resp.categories;
                if(categories.length >= 0){
                $('#categorySelect').empty();
                }
                for (var i = 0; i < categories.length; i++) {
                    $('#categorySelect').append($('<option>', { value: categories[i].c_id, text: categories[i].c_name }));
                }
                $('#categorySelect').prop('disabled', true);
            }
        },
        error: function(err) 
        {
            console.log(err);
        }  

    });

  }

function selectInvestor(investor,id)
{
    $("#investor_name").val(investor);
    $("#investor_id").val(id);
    $("#suggestion-box").fadeIn(300).hide();
    $("#investor_name_loader").fadeIn(300).hide();
}

</script>