<script type="text/javascript">

$(document).ready(function(){

  // Initial Loading of Produce
   loadOrder(); 
  //Load the Produce given the Produce Type ID
  $('#orSelect').change(loadOrder);

  function loadOrder()
  {
    var cpurchase_slug = $("#orSelect option:selected").text().trim();
    console.log({cpurchase_slug});

    $.ajax({
        url: "<?php echo base_url('admin/getCPurchaseDetails'); ?>",
        type: 'POST',
        data:  {"cpurchase_slug": cpurchase_slug},
        dataType: 'json',
        success: function(resp)
        {
            console.log({resp});
            // resp = JSON.parse(resp);
            // console.log(resp);
            var quantityToDeliver = Number(resp.cusp_quantity) - Number(resp.cusp_quantity_delivered);
            $('#oo_quantity').fadeIn(200).val(quantityToDeliver).attr('max',quantityToDeliver);

            $('#order_details').empty();
            $('#order_details').fadeIn(100).append(appendOrderDetail("Buyer",resp.investor_name));
            $('#order_details').fadeIn(100).append(appendOrderDetail("Quantity",resp.cusp_quantity));
            $('#order_details').fadeIn(100).append(appendOrderDetail("Delivered",resp.cusp_quantity_delivered));
            $('#order_details').fadeIn(100).append(appendOrderDetail("Address",resp.delivery_add));

        },
        error: function(err) 
        {
            console.log(err);
        }  

    });

  }

function appendOrderDetail(key, value)
{
    var html = '';
    html += '<tr class=" no-table-border">';
    html += '<td class="product-title">'+key+':</td>';
    html += '<td>'+value+'</td>';
    html += '</tr>';
    return html;
}

});
// End of Document Ready Function

</script>