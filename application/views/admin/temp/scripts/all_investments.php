<script type="text/javascript">

 function payInvestorROI(iv_id)
 {
    var edit = confirm("Do you want to set this to paid ? \nThis operation cannot be reversed once done.");
    console.log({edit});
    // return false;
    if(edit){
        $.ajax({
            url: "<?php echo base_url('admin/pay_investment'); ?>",
            type: 'POST',
            data:  {"investment_id": iv_id},
            dataType: 'json',
            success: function(resp)
            {
                if (resp.status !== 'error') {
                    // Set the buttons to Paid status
                    $("button[data-piv_id="+iv_id+"]").fadeOut(200).removeClass('btn-primary').fadeIn(200)
                    .addClass('btn-success').removeAttr('onclick').text(capitalizeFirstLetter(resp.message));
                }else{
                    alert(resp.message);
                }
            },
            error: function(err) 
            {
                console.log(err);
            }  

        });
    }
 }

 function investorPaid(iv_id)
 {
    var edit = confirm("Do you want to set this to paid ? \nThis operation cannot be reversed once done.");
    console.log({edit});
    // return false;
    if(edit){
        $.ajax({
            url: "<?php echo base_url('admin/investor_paid'); ?>",
            type: 'POST',
            data:  {"investment_id": iv_id},
            dataType: 'json',
            success: function(resp)
            {
                if (resp.status !== 'error') {
                    // Set the buttons to Paid status
                    $("button[data-iv_id="+iv_id+"]").fadeOut(200).removeClass('btn-primary').fadeIn(200)
                    .addClass('btn-success').removeAttr('onclick').text(capitalizeFirstLetter(resp.message));
                }else{
                    alert(resp.message);
                }
            },
            error: function(err) 
            {
                console.log(err);
            }  

        });
    }
 }

function capitalizeFirstLetter(string) 
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}
</script>