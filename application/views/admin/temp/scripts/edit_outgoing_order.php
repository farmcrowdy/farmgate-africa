<script type="text/javascript">

$(document).ready(function(){

    // Get the default produce_transfer order values

    var currency_id    = '<?php if(!empty($outgoing_order->oo_currency_id)) {echo trim($outgoing_order->oo_currency_id); } else {echo "";} ?>';
    var order_number   = '<?php if(!empty($outgoing_order->oo_order_number)) {echo trim($outgoing_order->oo_order_number); } else {echo "";} ?>';
    var wh_id          = '<?php if(!empty($outgoing_order->oo_warehouse_id)) {echo trim($outgoing_order->oo_warehouse_id); } else {echo "";} ?>';
    var pay_status     = '<?php if(!empty($outgoing_order->oo_pay_status)) {echo trim($outgoing_order->oo_pay_status); } else {echo "";} ?>';
    var whmStatus      = '<?php if(!empty($outgoing_order->oo_wh_approved)) {echo trim($outgoing_order->oo_wh_approved); } else {echo "";} ?>';
    var adminStatus    = '<?php if(!empty($outgoing_order->oo_admin_approved)) {echo trim($outgoing_order->oo_admin_approved); } else {echo "";} ?>';
    var deliveryStatus = '<?php if(!empty($outgoing_order->oo_transfer_status)) {echo trim($outgoing_order->oo_transfer_status); } else {echo "";} ?>';

    // Set the values for the dropdowns
    $('#currencySelect').find("option[value="+currency_id+"]").attr("selected","selected")
    $('#whSelect').find("option[value="+wh_id+"]").attr("selected","selected")
    $('#whSelect').prop('disabled', true);
    $('#orSelect').find("option[value="+order_number+"]").attr("selected","selected").prop('disabled', true);
    $('#paySelect').find("option[value="+pay_status+"]").attr("selected","selected");
    $('#whmSelect').find("option[value="+whmStatus+"]").attr("selected","selected");
    $('#adminSelect').find("option[value="+adminStatus+"]").attr("selected","selected");
    $('#deliverySelect').find("option[value="+deliveryStatus+"]").attr("selected","selected");
    // Lock the Delivery Status if the status is 'delivered' and 'pickup'
    if(deliveryStatus === 'delivered' || deliveryStatus === 'pickup'){
        $('#deliverySelect').prop('disabled', true);
    }
    // Lock the Pay Status if the status is 'delivered' and 'pickup'
    if(pay_status === 'paid'){
        $('#paySelect').prop('disabled', true);
    }
    // $('#oo_quantity').prop('disabled', true);




  // Initial Loading of Produce
  loadOrder(); 
  //Load the Produce given the Produce Type ID
  $('#orSelect').change(loadOrder);

  function loadOrder()
  {
    var cpurchase_slug = $("#orSelect option:selected").text().trim();
    console.log({cpurchase_slug});
    $('#orSelect').prop('disabled', false);
    $.ajax({
        url: "<?php echo base_url('admin/getCPurchaseDetails'); ?>",
        type: 'POST',
        data:  {"cpurchase_slug": cpurchase_slug},
        dataType: 'json',
        success: function(resp)
        {
            console.log({resp});

            $('#order_details').empty();
            $('#order_details').fadeIn(100).append(appendOrderDetail("Buyer",resp.investor_name));
            $('#order_details').fadeIn(100).append(appendOrderDetail("Quantity",resp.cusp_quantity));
            $('#order_details').fadeIn(100).append(appendOrderDetail("Delivered",resp.cusp_quantity_delivered));
            $('#order_details').fadeIn(100).append(appendOrderDetail("Address",resp.delivery_add));
            $('#oo_quantity').prop('disabled', true);
            // $('#orSelect').prop('disabled', true);

        },
        error: function(err) 
        {
            console.log(err);
        }  

    });

  }

function appendOrderDetail(key, value)
{
    var html = '';
    html += '<tr class=" no-table-border">';
    html += '<td class="product-title">'+key+':</td>';
    html += '<td>'+value+'</td>';
    html += '</tr>';
    return html;
}


});
// End of Document Ready Function
</script>