<script type = "text/javascript" >

	$(document).ready(function () {

        showCategoryArea();

        $('#new_category').click(function (e) {
            e.preventDefault();
            addCategory();
        });

        function addCategory(){

            var parent_category = $('#categorySelect').val();
            var category_name = $('#category_name').val();
            var category_type = $('#categoryTypeSelect').val();
            if(category_name.length < 1){
                alert("The Category Name is empty!");
                return false;
            }

            $.ajax({
                type: "post",
                url: "/admin/newcategory/",
                data: {"c_parent_id": parent_category, "c_name": category_name, "c_type_id": category_type},
                success: function(data){
                    data = JSON.parse(data);
                    console.log(data);
                    alert(data.message);
                    if(data.code === 1){
                        // A new category has been added 
                        // should load the menu and tables
                        loadCategoriesMenu();
                        showCategoryArea();
                    } 
                },
                error: function() { alert("Error communicating with our servers."); }
            });

        }

        function loadCategoriesMenu()
        {
            $.ajax({
                type: "post",
                url: "/admin/loadcategory/",
                data: {},
                success: function(data){
                    data = JSON.parse(data);
                    console.log(data);

                    $('#categorySelect').fadeOut(400).empty();
                    $('#categorySelect').fadeIn(300).append(setupCategoriesMenu(data));
                    
                },
                error: function() { alert("Error communicating with our servers."); }
            });		

        }

        function setupCategoriesMenu(data)
        {
            var html = '<option value="0">Set As Parent Category</option>';
            for(var j = 0; j < data.length; j++){
                html += '<option value="'+data[j].c_id+'">';
                html += data[j].c_name;
                html += '</option>'; 
            }
            return html;
        }

        function removeCategory(c_id)
        {
            
            $.ajax({
                type: "post",
                url: "/admin/removecategory/",
                data: { "c_id": c_id },
                success: function(data){
                    data = JSON.parse(data);
                    if(data.code === 1){
                        // A new category has been removed 
                        // should load the menu and tables
                        loadCategoriesMenu();
                        showCategoryArea();
                    }               
                },
                error: function() { alert("Error communicating with our servers."); }
            });
        }

        function showCategoryArea()
        {
            var link = '<?php echo base_url('admin/latestcategories'); ?>';
            $( "#categories_area" ).empty();
            $( "#categories_area" ).load( link, function() {
                // alert( "Load was performed." ); 
                $('.del-caty').click(function (e){
                    var cid = $(this).attr('data-cid');
                    var response = confirm("Do you want to delete this category ? ");
                    if(response){
                        $(this).parent().fadeOut(200).hide();
                        removeCategory(cid);
                    }else{
                        return false;
                    }
                });
            });
        }

	});
// End of Document Ready Function
</script>
