<script type="text/javascript">

$(document).ready(function(){

  $('#stateSelect').change( function(){

        var state = $("#stateSelect option:selected").val();

        $.ajax({
            url: "<?php echo base_url('users/locals'); ?>",
            type: 'POST',
            data:  {state_id: state},
            dataType: 'json',
            success: function(resp)
            {
                if (resp.code == 1) {
                    var lgas = resp.lgas;
                    if(lgas.length > 0){
                    $('#localSelect').empty();
                    }
                    for (var i = 0; i < lgas.length; i++) {
                        $('#localSelect').append($('<option>', { value: lgas[i].local_id, text: lgas[i].local_name }));
                    }
                }
            },
            error: function(err) 
            {
                console.log(err);
            }  

        });
    });


    $('#upload-file-selector').change(function () {
        var files = $(this).get(0).files;
        if (files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                var previewContainer = $('#image-preview');
                previewContainer.find('img').attr('src', e.target.result).removeClass('hidden');
                previewContainer.find('.user-placeholder').addClass('hidden');
            };

            reader.readAsDataURL(files[0]);
        }
    })


});
// End of Document Ready Function

ClassicEditor
    .create( document.querySelector( '#editor' ) )
    .then( editor => {
        console.log( editor );
    } )
    .catch( error => {
        console.error( error );
    } );

</script>