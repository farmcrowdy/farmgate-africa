<script type="text/javascript">

    // Get the default outgoing_order values
    var currency_id   = '<?php if(!empty($cpurchase->cusp_currency_id)) {echo trim($cpurchase->cusp_currency_id); } else {echo "";} ?>';
    var pay_status  = '<?php if(!empty($cpurchase->cusp_pay_status)) {echo trim($cpurchase->cusp_pay_status); } else {echo "";} ?>';
    var deliveryStatus  = '<?php if(!empty($cpurchase->cusp_delivery_status)) {echo trim($cpurchase->cusp_delivery_status); } else {echo "";} ?>';
    var commodity_id = '<?php if(!empty($cpurchase->cusp_commodity_id)) {echo trim($cpurchase->cusp_commodity_id); } else {echo "";} ?>';



    // Set the values for the dropdowns

    $('#currencySelect').find("option[value="+currency_id+"]").attr("selected","selected"); 
    $('#commoditySelect').find("option[value="+commodity_id+"]").attr("selected","selected").prop('disabled',true);  
    $('#paySelect').find("option[value="+pay_status+"]").attr("selected","selected");
    if(pay_status === 'paid'){
        $('#paySelect').prop('disabled', true);
    }
    $('#deliverySelect').find("option[value="+deliveryStatus+"]").attr("selected","selected");
    if(deliveryStatus === 'delivered'){
        $('#deliverySelect').prop('disabled', true);
    }


$(document).ready(function(){

    $('#investor_name').keyup( function(){

        $.ajax({
            url: "<?php echo base_url('admin/get_investors'); ?>",
            type: 'POST',
            data:  {"investor_name" : $(this).val()},
            dataType: 'json',
            beforeSend: function(){
                $("#investor_name_loader").fadeIn(200).show();
            },
            success: function(resp)
            {
                if(resp.investors.length > 0){
                    $("#investor_name_loader").fadeIn(300).hide();
                    $("#cusp_search").text("");
                    $("#suggestion-box").show();
                    $("#suggestion-box").fadeIn(300).html(populateSuggestions(resp.investors));
                    $("#investor_name").css("background","#FFF");
                }else if(resp.investors.length === 0){
                    $("#cusp_search").text("No Active Investor found with that entry...");
                }
            },
            error: function(err)
            {
                $("#investor_name_loader").fadeIn(300).hide();
            }

        });
    });


function populateSuggestions(investors)
{
    var html = '<ul style="background-color: #9ceabb;">';
    for (var i = 0; i < investors.length; i++) {
        html += '<li style="list-style:none; background-color: #9ceabb;" onClick="selectInvestor(\''+investors[i].full_name+'\','+investors[i].id+');">'+investors[i].full_name+' : ID-'+ investors[i].id+' : Phone-'+investors[i].phone+' </li>';
    }
    html +='</ul>';
    return html;
}

});
// End of Document Ready Function

function selectInvestor(investor,id)
{
    $("#investor_name").val(investor);
    $("#investor_id").val(id);
    $("#suggestion-box").fadeIn(300).hide();
    $("#investor_name_loader").fadeIn(300).hide();
}

</script>