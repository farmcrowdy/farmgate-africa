<script type="text/javascript">

$(document).ready(function(){

    $('#investor_name').keyup( function(){

        $.ajax({
            url: "<?php echo base_url('admin/get_investors'); ?>",
            type: 'POST',
            data:  {"investor_name" : $(this).val()},
            dataType: 'json',
            beforeSend: function(){
                $("#investor_name_loader").fadeIn(200).show();
            },
            success: function(resp)
            {
                if(resp.investors.length > 0){
                    $("#investor_name_loader").fadeIn(300).hide();
                    $("#iv_search").text("");
                    $("#suggestion-box").empty().show();
                    $("#suggestion-box").fadeIn(300).html(populateSuggestions(resp.investors));
                    $("#investor_name").css("background","#FFF");
                }else if(resp.investors.length === 0){
                    $("#iv_search").text("No Active Investor found with that entry...");
                }
            },
            error: function(err)
            {
                $("#investor_name_loader").fadeIn(300).hide();
            }

        });
    });


function populateSuggestions(investors)
{
    var html = '<ul style="background-color: #9ceabb;">';
    for (var i = 0; i < investors.length; i++) {
        html += '<li style="list-style:none; background-color: #9ceabb;" onClick="selectInvestor(\''+investors[i].full_name+'\','+investors[i].id+');">'+investors[i].full_name+' : ID-'+ investors[i].id+' : Phone-'+investors[i].phone+' </li>';
    }
    html +='</ul>';
    return html;
}

  // Initial Loading of Produce
   loadProduce(); 
  //Load the Produce given the Produce Type ID
  $('#categoryTypeSelect').change(loadProduce);

  function loadProduce()
  {
    var produce_type_id = $("#categoryTypeSelect option:selected").val();

    // Set the Quantity field label and type
    if(produce_type_id === "1"){
       $('#prod_quantity').empty().text('MT');
    }else{
       $('#prod_quantity').empty().text('#');
    }

    $.ajax({
        url: "<?php echo base_url('admin/produce-categories'); ?>",
        type: 'POST',
        data:  {"produce_type_id": produce_type_id},
        dataType: 'json',
        success: function(resp)
        {
            if (resp.code == 1) {
                var categories = resp.categories;
                if(categories.length >= 0){
                $('#categorySelect').empty();
                }
                for (var i = 0; i < categories.length; i++) {
                    $('#categorySelect').append($('<option>', { value: categories[i].c_id, text: categories[i].c_name }));
                }
            }
        },
        error: function(err) 
        {
            console.log(err);
        }  

    });

  }


});
// End of Document Ready Function

function selectInvestor(investor,id)
{
    $("#investor_name").val(investor);
    $("#investor_id").val(id);
    $("#suggestion-box").fadeIn(300).hide();
    $("#investor_name_loader").fadeIn(300).hide();
}

</script>