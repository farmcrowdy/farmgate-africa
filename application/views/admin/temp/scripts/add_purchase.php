<script type="text/javascript">

$(document).ready(function(){

    $('#farmer_name').keyup( function(){

        $.ajax({
            url: "<?php echo base_url('admin/get_farmers'); ?>",
            type: 'POST',
            data:  {fm_name : $(this).val()},
            dataType: 'json',
            beforeSend: function(){
                $("#farmer_name_loader").fadeIn(200).show();
            },
            success: function(resp)
            {
                if(resp.farmers.length > 0){
                    $("#farmer_name_loader").fadeIn(300).hide();
                    $("#fm_search").text("");
                    $("#suggestion-box").show();
                    $("#suggestion-box").fadeIn(300).html(populateSuggestions(resp.farmers));
                    $("#farmer_name").css("background","#FFF");
                }else if(resp.farmers.length === 0){
                    $("#fm_search").text("No Active Farmer found with that entry...");
                }
            },
            error: function(err)
            {
                $("#farmer_name_loader").fadeIn(300).hide();
            }

        });
    });


function populateSuggestions(farmers)
{
    var html = '<ul style="background-color: #9ceabb;">';
    for (var i = 0; i < farmers.length; i++) {
        html += '<li style="list-style:none; background-color: #9ceabb;" onClick="selectFarmer(\''+farmers[i].fm_full_name+'\','+farmers[i].fm_id+');">'+farmers[i].fm_full_name+' : ID-'+ farmers[i].fm_id+' : Phone-'+farmers[i].fm_phone+' </li>';
    }
    html +='</ul>';
    return html;
}

  // Initial Loading of Produce
   loadProduce(); 
  //Load the Produce given the Produce Type ID
  $('#categoryTypeSelect').change(loadProduce);

  function loadProduce()
  {
    var produce_type_id = $("#categoryTypeSelect option:selected").val();

    // Set the Quantity field label and type
    if(produce_type_id === "1"){
       $('#prod_quantity').empty().text('MT');
    }else{
       $('#prod_quantity').empty().text('#');
    }

    $.ajax({
        url: "<?php echo base_url('admin/produce-categories'); ?>",
        type: 'POST',
        data:  {"produce_type_id": produce_type_id},
        dataType: 'json',
        success: function(resp)
        {
            if (resp.code == 1) {
                var categories = resp.categories;
                if(categories.length >= 0){
                $('#categorySelect').empty();
                }
                for (var i = 0; i < categories.length; i++) {
                    $('#categorySelect').append($('<option>', { value: categories[i].c_id, text: categories[i].c_name }));
                }
            }
        },
        error: function(err) 
        {
            console.log(err);
        }  

    });

  }


});
// End of Document Ready Function

function selectFarmer(farmer,id)
{
    $("#farmer_name").val(farmer);
    $("#farmer_id").val(id);
    $("#suggestion-box").fadeIn(300).hide();
    $("#farmer_name_loader").fadeIn(300).hide();
}

</script>