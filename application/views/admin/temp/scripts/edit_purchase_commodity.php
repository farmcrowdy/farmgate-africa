<script type="text/javascript">

$(document).ready(function(){

    // Get the default Investment Commodity values
    var produce_type_id = '<?php if(!empty($pCommodity->purc_produce_type_id)) {echo trim($pCommodity->purc_produce_type_id); } else {echo "";} ?>';
    var produce_id = '<?php if(!empty($pCommodity->purc_produce_id)) {echo trim($pCommodity->purc_produce_id); } else {echo "";} ?>';
    var ic_status  = '<?php if(!empty($pCommodity->purc_status)) {echo trim($pCommodity->purc_status); } else {echo "";} ?>';
    var unit_id  = '<?php if(!empty($pCommodity->purc_unit_id)) {echo trim($pCommodity->purc_unit_id); } else {echo "";} ?>';

    // Set the values for the dropdowns
    $('#categoryTypeSelect').find("option[value="+produce_type_id+"]").attr("selected","selected");
    $('#status').find("option[value="+ic_status+"]").attr("selected","selected");
    $('#unitSelect').find("option[value="+unit_id+"]").attr("selected","selected");
    
    // Initial Loading of Produce
    loadProduce();

  //Load the Produce given the Produce Type ID
  $('#categoryTypeSelect').change(loadProduce);

    $('#upload-file-selector').change(function () {
        var files = $(this).get(0).files;
        if (files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                var previewContainer = $('#image-preview');
                previewContainer.find('img').attr('src', e.target.result).removeClass('hidden');
                previewContainer.find('.user-placeholder').addClass('hidden');
            };

            reader.readAsDataURL(files[0]);
        }
    })


  function loadProduce()
  {
    var produce_type_id = $("#categoryTypeSelect option:selected").val();

    // Set the Quantity field label and type
    if(produce_type_id === "1"){
       $('#prod_quantity').empty().text('MT');
    }else{
       $('#prod_quantity').empty().text('#');
    }

    $.ajax({
        url: "<?php echo base_url('admin/produce-categories'); ?>",
        type: 'POST',
        data:  {"produce_type_id": produce_type_id},
        dataType: 'json',
        success: function(resp)
        {
            if (resp.code == 1) {
                var categories = resp.categories;
                if(categories.length >= 0){
                $('#categorySelect').empty();
                }
                for (var i = 0; i < categories.length; i++) {
                    $('#categorySelect').append($('<option>', { value: categories[i].c_id, text: categories[i].c_name }));
                }
                // Set the values for the dropdowns
                $('#categorySelect').find("option[value="+produce_id+"]").attr("selected","selected");
                $('#categorySelect').prop('disabled',true);
                $('#categoryTypeSelect').prop('disabled', true);
            }
        },
        error: function(err)
        {
            console.log(err);
        }

    });

  }

});
// End of Document Ready Function

ClassicEditor
    .create( document.querySelector( '#cd-editor' ),
    {
        toolbar: [ 'Heading','bold', 'Link',  'italic', 'bulletedList', 'numberedList', 'blockQuote','Enter', 'Typing', 'undo', 'redo'],
    } )
    .then( editor => {
        console.log( editor );
        // editor.setData(commodity_details);
    })
    .catch( error => {
        console.error( error );
    } );

ClassicEditor
    .create( document.querySelector( '#ic-editor' ),
    {
        toolbar: [ 'Heading','bold', 'Link',  'italic', 'bulletedList', 'numberedList', 'blockQuote','Enter', 'Typing', 'undo', 'redo'],

    })
    .then( editor => {
        console.log( editor );
    } )
    .catch( error => {
        console.error( error );
    } );

</script>