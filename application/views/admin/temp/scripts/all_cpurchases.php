<script type="text/javascript">

 function BuyerPaid(cusp_id)
 {
    var edit = confirm("Do you want to set this to paid ? \nThis operation cannot be reversed once done.");
    console.log({edit});
    // return false;
    if(edit){
        $.ajax({
            url: "<?php echo base_url('admin/buyer_paid'); ?>",
            type: 'POST',
            data:  {"cpurchase_id": cusp_id},
            dataType: 'json',
            success: function(resp)
            {
                if (resp.status !== 'error') {
                    // Set the buttons to Paid status
                    $("button[data-cusp_id="+cusp_id+"]").fadeOut(200).removeClass('btn-primary').fadeIn(200)
                    .addClass('btn-success').removeAttr('onclick').text(capitalizeFirstLetter(resp.message));
                }else{
                    alert(resp.message);
                }
            },
            error: function(err) 
            {
                console.log(err);
            }  

        });
    }
 }

 function purchaseDeliveryStatus(cusp_id)
 {
    var edit = confirm("Do you want to set this to delivered ? \nThis operation cannot be reversed once done.");
    console.log({edit});
    // return false;
    if(edit){
        $.ajax({
            url: "<?php echo base_url('admin/purchase_delivery_status'); ?>",
            type: 'POST',
            data:  {"cpurchase_id": cusp_id},
            dataType: 'json',
            success: function(resp)
            {
                if (resp.status !== 'error') {
                    // Set the buttons to Paid status
                    $("button[data-cusp_id_ds="+cusp_id+"]").fadeOut(200).removeClass('btn-primary').fadeIn(200)
                    .addClass('btn-success').removeAttr('onclick').text(capitalizeFirstLetter(resp.message));
                }else{
                    alert(resp.message);
                }
            },
            error: function(err) 
            {
                console.log(err);
            }  

        });
    }
 }

function capitalizeFirstLetter(string) 
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}
</script>