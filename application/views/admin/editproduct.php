        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">Products</li>
                                    <li class="breadcrumb-item active" aria-current="page">Add Product</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <div class="m-r-10">
                                <div class="lastmonth"></div>
                            </div>
                            <div class="">
                                <small>LAST MONTH</small>
                                <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Add Products -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-lg-10 col-md-12 col-11">
                    <?php echo $this->session->flashdata('prod_feedback');?>
                            <?php if(!empty($image_errors)){
                                foreach ($image_errors as $error) {
                                        # code...
                                    echo $error;
                                    } 
                                } 
                            ?>
                            <?php echo form_open_multipart('admin/editproduct/'.$product->prod_slug,array('class'=>'row acct-form mb-4')); ?>
                            <div class="col-md-12 pad-dashboard">
                                <div class="row pt-4 pb-2">
                                    <div class="col-lg-12 upload-photo" id='image-preview'>

                                        <img src="<?php echo base_url('/mainasset/fe/images/products/').$product->prod_image;?>" alt="<?php echo $product->prod_name. ' -'.$product->prod_slug; ?>" class="img-fluid picture rounded">
                                        <div class="user-placeholder hidden">.</div>
                                        <label class="btn btn-default new-line" for="upload-file-selector">Change product image</label>
                                        <input id="upload-file-selector" name="productImageFile" accept="image/jpeg,image/png" type="file" class="hidden">

                                        <!-- <script>
                                        $(document).ready(function(){
                                            $('#upload-file-selector').change(function () {
                                                console.log('Selecting a new file');
                                                var files = $(this).get(0).files;
                                                if (files[0]) {
                                                    var reader = new FileReader();

                                                    reader.onload = function (e) {
                                                        var previewContainer = $('#image-preview');
                                                        previewContainer.find('img').attr('src', e.target.result).removeClass('hidden');
                                                        previewContainer.find('.user-placeholder').addClass('hidden');
                                                    };

                                                    reader.readAsDataURL(files[0]);
                                                }
                                            })
                                        });
                                        </script> -->
                                    </div>

                                </div>
                                <div class="row mt-5">
                                    <div class="col-md-6">
                                        <label for="">Product Name
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('name'); ?>
                                        <input type="text" class="form-control" id="" name="name" aria-describedby="productName" placeholder=""
                                        value="<?php echo set_value('name', $product->prod_name); ?>">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="statusSelect">Product Status
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('status'); ?>
                                        <select class="form-control" id="statusSelect" name="status">
                                        <?php foreach ($statuses as $status) :?>
                                                <option value="<?php echo $status; ?>"> 
                                                <?php echo strtoupper(str_replace('_',' ',$status)); ?> 
                                                </option>
                                        <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row py-1">
                                    <div class=" col-md-6 col-9 form-group  h-100">
                                        <label for="formControlSelect">Product category
                                            <span class="superscript-star">*</span>
                                        </label><?php echo form_error('categorySelect'); ?>
                                        <select class="form-control" id="categorySelect" name="categorySelect">
                                        <?php foreach ($categories as $category) :?>
                                                <option 
                                                    value="<?php echo $category->c_id; ?>"> <?php echo $category->c_name; ?> 
                                                </option>
                                        <?php endforeach ?>
                                        </select>
                                    </div>
                                    <!-- <div class=" col-md-6 col-9 form-group  h-100">
                                        <label for="formControlSelect">Subcategory
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <select class="form-control" id="formControlSelect">
                                            <option>Rice</option>
                                            <option>Beans</option>
                                            <option>Cassava</option>
                                            <option>Maize</option>
                                            <option>Yam</option>
                                        </select>
                                    </div> -->
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-7 col-6  h-100">
                                                <label for="">Quantity
                                                    <span class="superscript-star">*</span>
                                                </label>
                                                <?php echo form_error('quantity'); ?>
                                                <input type="number" class="form-control" name="quantity" id="" aria-describedby="Quantity" placeholder=""
                                                value="<?php echo set_value('quantity',$product->prod_quantity); ?>">
                                            </div>
                                            <div class="col-md-5 col-6 h-100">
                                            <label for="unitSelect" >Unit</label>
                                                <?php echo form_error('unit_id'); ?>
                                                <select class="form-control" id="unitSelect" name="unitSelect">
                                                    <?php foreach ($units as $unit) :?>
                                                        <option 
                                                        value="<?php echo $unit->ut_id; ?>"> <?php echo $unit->ut_name; ?> 
                                                        </option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <div class=" col-md-6 col-9 form-group">
                                        <label for="">Price per Metric ton
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('price_per_unit'); ?>
                                        <input type="text" class="form-control" id="price_per_unit" 
                                        name="price_per_unit" value="<?php echo set_value('price_per_unit', $product->prod_price_per_unit); ?>">
                                    </div>
                                </div>

                                <div class="row relative">
                                    <div class="col-md-4 ">
                                        <div class="row">
                                            <div class="col-md-9 ">
                                                <label class="d-md-none" for="">Minimum Order Quantity
                                                    <span class="superscript-star">*</span>
                                                </label>
                                                <?php echo form_error('min_order'); ?>
                                                <label class="d-none d-md-block" for="">M.O.Q
                                                    <span class="superscript-star">*</span>
                                                </label>
                                                <input type="number" min="1" class="form-control" name="min_order" id="min_order" 
                                                aria-describedby="minimum order" value="<?php echo set_value('min_order',$product->prod_min_order); ?>">
                                            </div>
                                            <!-- <div class="col-md-5 col-6 h-100">
                                                <label for="formControlSelect" style="color:#efefef">.</label>
                                                <select class="form-control" id="formControlSelect">
                                                    <option>Metric Tonne</option>
                                                    <option>Metric Tonne</option>
                                                    <option>Metric Tonne</option>
                                                    <option>Metric Tonne</option>
                                                    <option>Metric Tonne</option>
                                                </select>
                                            </div> -->
                                        </div>
                                    </div>
                                    <div class="col-md-4 h-100 form-group">
                                        <label for="">State
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('stateSelect'); ?>
                                        <select class="form-control" id="stateSelect" name="stateSelect">
                                            <?php foreach ($states as $state) :?>
                                                <option 
                                                    value="<?php echo $state->state_id; ?>"> <?php echo $state->name; ?>
                                                </option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                    <div class=" col-md-4 h-100 form-group">
                                        <label for="localSelect">Local Government 
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('localSelect'); ?>
                                        <select class="form-control" id="localSelect" name="localSelect">
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-6 col-9">
                                                <label for="">Available from
                                                    <span class="superscript-star">*</span>
                                                </label>
                                                <?php echo form_error('avail_from'); ?>
                                                <small><b><?php echo set_value('avail_from', date('Y-m-d',strtotime($product->prod_avail_from))); ?></b></small>
                                                <input type="date" class="form-control" id="" aria-describedby="availableFrom" name="avail_from"
                                                value="<?php echo set_value('avail_from', date('Y-m-d',strtotime($product->prod_avail_from))); ?>">
                                            </div>
                                            <div class="col-md-6 col-9">
                                                <label for="formControlSelect">To</label>
                                                <?php echo form_error('avail_to'); ?>
                                                <small><b><?php echo set_value('avail_to', date('Y-m-d',strtotime($product->prod_avail_to))); ?></b></small>
                                                <input type="date" class="form-control" id="" aria-describedby="availableTo" name="avail_to"
                                                value="<?php echo set_value('avail_to', date('Y-m-d',strtotime($product->prod_avail_to))); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class=" col-md-6 col-lg-6 form-group">
                                        <label for="">Location
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <input type="text" class="form-control" id="" aria-describedby="price">
                                    </div> -->
                                </div>
                                <div class="row">
                                    <div class=" col-12  form-group">
                                        <label for="">Description
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('description'); ?>
                                        <textarea name="description" class="form-control" rows="5" aria-label="With textarea"><?php echo trim(set_value('description',$product->prod_description)); ?></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12  pb-5 pt-4">
                                    <button name="submit" type="submit" class="btn  btn-submit"> Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            
