<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
	<!-- ============================================================== -->
	<!-- Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<div class="page-breadcrumb">
		<div class="row">
			<div class="col-5 align-self-center">
				<h4 class="page-title">All Users</h4>
				<div class="d-flex align-items-center">
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb">
							<li class="breadcrumb-item">
								<a href="#">Home</a>
							</li>
							<li class="breadcrumb-item active" aria-current="page">Users (<?php if(!empty($total_users)){echo $total_users;}else{echo "0"; } ?>)
							</li>
						</ol>
					</nav>
				</div>
			</div>
			<div class="col-7 align-self-center">
				<div class="d-flex no-block justify-content-end align-items-center">
					<div class="m-r-10">
						<div class="lastmonth"></div>
					</div>
					<div class="">
						<small>LAST MONTH</small>
						<h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Users -->
		<!-- ============================================================== -->
		<div class="row">
			<?php echo $this->session->flashdata('admin_del_user_feedback'); ?>
		</div>
		<div class="row">
			<!-- Column -->
			<div class="col-sm-12">
				<div class="card">
					<div class="card-body">
						<div class="row">
                            </div>
							<div class="col-sm-12 col-md-6">
								<form class="form-inline" method="get" action="<?php echo base_url('admin/users'); ?>">
									<input type="hidden" name="type" value="<?php if(!empty($type)){echo $type; } ?>" />
									<input name="keyword_us" type="search" class="form-control" placeholder="Search by Username" aria-label="Username" aria-describedby="basic-addon1"
									 value="<?php if(!empty($keyword_us)){echo $keyword_us;} ?>">
									<div class="input-group-append">
										<button type="submit" class="btn search-btn" style="background: none;">
											<i class="fas fa-search"></i>
										</button>
										<!-- <i class="fas fa-search m-auto"></i> -->
									</div>
								</form>
							</div>
						</div>
						<!-- title -->
						<div class="table-responsive  scrollable m-t-10" style="height:400px;">
							<table class="table v-middle">
								<thead>
									<tr>
										<th class="border-top-0">User ID</th>
										<th class="border-top-0">Full Name</th>
										<th class="border-top-0">Photo</th>
										<th class="border-top-0">Type</th>
										<th class="border-top-0">Channel</th>
										<th class="border-top-0">Email</th>
										<th class="border-top-0">Join Date</th>
										<th class="border-top-0">Status</th>
										<th class="border-top-0">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php if(!empty($users)): ?>
									<?php foreach($users as $user): ?>
									<tr>
										<td>
											<div class="d-flex align-items-center">
												<a href="<?php echo base_url('admin/user/'.$user->slug); ?>" target="_blank" class=" item-order-link">
													<h5 class="m-b-0 font-16 font-medium">
														<?php if(!empty($user->slug)){echo strtoupper($user->slug); }?>
													</h5>
												</a>
											</div>
										</td>
										<td>
											<?php if(!empty($user->full_name)){echo $user->full_name;} elseif(!empty($user->ord_lpo_full_name)){ echo $user->ord_lpo_full_name;} ?>
										</td>
										<td>
											<img width="40" src="<?php if(!empty($user->image)){echo base_url('mainasset/fe/images/users/').$user->image;}else{ echo 'http://via.placeholder.com/450x450'; } ?>"
											 class="rounded-circle img-fluid" alt="<?php if(!empty($user->full_name)){echo $user->full_name;}else{echo 'User'; }?>">
										</td>
										<td>
											<span class="or_amount">
												<?php if(!empty($user->type)){echo ucwords(str_replace('_',' ',$user->type));}?>
											</span>
										</td>
										<td>
											<span class="or_amount">
												<?php if(!empty($user->channel)){echo ucwords(str_replace('_',' ',$user->channel));}?>
											</span>
										</td>
										<td>
											<span class="or_amount">
												<?php if(!empty($user->email)){echo ($user->email);}?>
											</span>
										</td>
										<td>
											<?php echo date('dS F, Y', strtotime($user->date_created));?>
										</td>
										<td>
											<?php if(!empty($user->active)){ echo ucwords($user->active); }?>
										</td>
										<td>
											<a href="<?php echo base_url('admin/edituser/'.$user->slug); ?>" class="icon-pad1">
												<i class="far fa-edit"></i></a>
											<a href="<?php echo base_url('admin/deluser/'.$user->slug); ?>" class="icon-pad2 del-user">
												<i class="fas fa-times" style="color: #f00;"></i></a>
										</td>
									</tr>
									<?php endforeach ?>

									<?php else: ?>
									<p> There are no users to display</p>
									<?php endif ?>

								</tbody>

							</table>
						</div>
						<?php echo $pagination_links; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- footer -->