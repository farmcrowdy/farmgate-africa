<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Produce Transfers (<?php if(!empty($total_produce_transfers)){echo $total_produce_transfers;}else{echo "0"; } ?>)</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <div class="m-r-10">
                        <div class="lastmonth"></div>
                    </div>
                    <div class=""><small>LAST MONTH</small>
                        <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Orders -->
        <!-- ============================================================== -->
        <div class="row">
            <?php echo $this->session->flashdata('produce_transfer_del_feedback'); ?>
        </div>
        <div class="row">
            <!-- Column -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <!-- <div class="col-sm-12 col-md-6">
                                <div class="dataTables_length" id="zero_config_length">
                                    <label>Show <select name="zero_config_length" aria-controls="zero_config" class="form-control form-control-sm">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select> entries
                                    </label>
                                </div>
                            </div> -->
                            <div class="col-sm-12 col-md-6">
                                <form class="form-inline" method="get" action="<?php echo base_url('admin/produce-transfers'); ?>">
                                    <input name="keyword_pt" type="search" class="form-control" placeholder="Find by Name" aria-label="Username" aria-describedby="basic-addon1"
                                        value="<?php if(!empty($keyword_pt)){echo $keyword_pt;} ?>">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn search-btn" style="background: none;">
                                            <i class="fas fa-search"></i>
                                        </button>
                                        <!-- <i class="fas fa-search m-auto"></i> -->
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- title -->
                        <div class="table-responsive  scrollable m-t-10" style="height:400px;">
                            <table class="table v-middle">
                                <thead>
                                    <tr>
                                        <th class="border-top-0">ID</th>
                                        <th class="border-top-0">Collection Center (Source)</th>
                                        <th class="border-top-0">Warehouse (Destination)</th>
                                        <th class="border-top-0">Pay Status</th>
                                        <th class="border-top-0">Delivered</th>
                                        <th class="border-top-0">Produce</th>
                                        <th class="border-top-0">Quantity</th>
                                        <th class="border-top-0">Cost</th>
                                        <th class="border-top-0">Created By</th>
                                        <th class="border-top-0">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($produce_transfers)): ?>
                                    <?php foreach($produce_transfers as $produce_transfer): ?>
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <a href="<?php echo base_url('admin/produce-transfer/'.$produce_transfer->pt_slug); ?>" target="_blank" class=" item-order-link">
                                                    <h5 class="m-b-0 font-16 font-medium">
                                                        <?php echo strtoupper($produce_transfer->pt_slug); ?>
                                                    </h5>
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            <?php if(!empty($produce_transfer->collection_center)){echo $produce_transfer->collection_center;} ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($produce_transfer->warehouse)){echo $produce_transfer->warehouse;} ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($produce_transfer->pt_status)){echo ucwords($produce_transfer->pt_status);} ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($produce_transfer->pt_wh_received)){echo ucwords($produce_transfer->pt_wh_received);} ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($produce_transfer->category)){echo ucwords($produce_transfer->category);} ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($produce_transfer->pt_quantity)){echo number_format($produce_transfer->pt_quantity);} ?> MT
                                        </td>                                                                                                                       
                                        <td>
                                            <?php if(!empty($produce_transfer->pt_transfer_cost)){echo $produce_transfer->currency.number_format($produce_transfer->pt_transfer_cost);} ?>
                                        </td>
                                        <td>
                                            <?php echo $produce_transfer->manager ;?>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url('admin/edit-produce-transfer/'.$produce_transfer->pt_slug); ?>" class="icon-pad1">
                                            <i class="far fa-edit"></i></a>
                                            <a href="<?php echo base_url('admin/del-produce-transfer/'.$produce_transfer->pt_slug); ?>" class="icon-pad2 del-pt">
                                            <i class="fas fa-times" style="color: #f00;"></i></a>
                                        </td>
                                    </tr>
                                    <?php endforeach ?>

                                    <?php else: ?>
                                    <p> There are no produce transfers to display</p>
                                    <?php endif ?>

                                </tbody>

                            </table>
                        </div>
                        <?php echo $pagination_links; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->