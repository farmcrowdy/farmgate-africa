<!-- Page wrapper -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item">Contacts</li>
                            <li class="breadcrumb-item active" aria-current="page">Contact Details</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Orders -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-sm-12 col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs no-nav-border" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#specification" role="tab" aria-controls="order-specification" aria-selected="true">Contact Details</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content card-body" id="myTabContent">
                        <div class="tab-pane fade show active" id="specification" role="tabpanel" aria-labelledby="order-specification-tab">
                            <table class="table no-table-border">
                                <tbody class=" no-table-border">
                                    <tr class=" no-table-border">
                                        <td class="product-title">User Name</td>
                                        <td><?php echo $contact->con_user_name; ?></a></td>
                                    </tr>
                                    <?php if(!empty($contact->category)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Category :</td>
                                        <td><?php echo ucwords($contact->category); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(isset($contact->con_subject)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Subject:</td>
                                        <td><?php echo ($contact->con_subject); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(isset($contact->con_message)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Message:</td>
                                        <td><?php echo ($contact->con_message); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($contact->con_file)): ?>
                                    <tr class=" no-table-border">
                                        <?php 
                                            $file_ext = substr(strrchr($contact->con_file,'.'),1);
                                            $allowed_images =  array('gif', 'png' ,'jpg');
                                            $allowed_files = array('pdf');
                                        ?>
                                        <?php if(in_array($file_ext,$allowed_images)): ?>
                                        <td class="product-title">Image:</td>
                                        <td><img src="<?php echo base_url('mainasset/fe/images/contacts/').$contact->con_file; ?>" width="300px" /> 
                                        <?php elseif(in_array($file_ext,$allowed_files)):?>
                                        <td class="product-title">File:</td>
                                        <td><a target="_blank" href="<?php echo base_url('mainasset/fe/images/contacts/').$contact->con_file; ?>"> <?php echo ucwords($contact->con_file); ?> </a></td>
                                        <?php endif ?>
                                    </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($contact->con_date_created)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Created on:</td>
                                        <td>
                                        <?php echo date('d/m/Y',strtotime($contact->con_date_created)); ?>
                                        </td>
                                    </tr>
                                    <?php endif ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Row -->
        </div>

        <!-- ============================================================== -->
        <!-- End Container fluid -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->