<!-- Page wrapper -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item">Investments</li>
                            <li class="breadcrumb-item active" aria-current="page">Investment Details</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Orders -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-sm-12 col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs no-nav-border" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#specification" role="tab" aria-controls="order-specification" aria-selected="true">Investment Details</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content card-body" id="myTabContent">
                        <div class="tab-pane fade show active" id="specification" role="tabpanel" aria-labelledby="order-specification-tab">
                            <table class="table no-table-border">
                                <tbody class=" no-table-border">
                                    <tr class=" no-table-border">
                                        <td class="product-title">Investor Name</td>
                                        <td><?php echo $investment->investor_name; ?></a></td>
                                    </tr>
                                    <?php if(!empty($investment->investor_email)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Email :</td>
                                        <td><?php echo ucwords($investment->investor_email); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($investment->investor_phone)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Phone :</td>
                                        <td><?php echo ucwords($investment->investor_phone); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($investment->investor_address)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Address :</td>
                                        <td><?php echo ucwords($investment->investor_address); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($investment->iv_commodity_name)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Commodity :</td>
                                        <td><?php echo ucwords($investment->iv_commodity_name); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(isset($investment->iv_quantity)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Quantity:</td>
                                        <td><?php echo number_format($investment->iv_quantity); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(isset($investment->iv_amount)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Amount:</td>
                                        <td><?php echo $investment->currency.number_format($investment->iv_amount); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(isset($investment->iv_returns)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Returns:</td>
                                        <td><?php echo $investment->currency.number_format($investment->iv_returns); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($investment->iv_pay_status)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Pay Status:</td>
                                        <td><?php echo ucwords($investment->iv_pay_status); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($investment->iv_returns_status)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Returns Status:</td>
                                        <td><?php echo ucwords($investment->iv_returns_status); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($investment->iv_due_date)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Due Date:</td>
                                        <td>
                                        <?php echo date('d/m/Y',strtotime($investment->iv_due_date)); ?>
                                        </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($investment->iv_date_created)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Created on:</td>
                                        <td>
                                        <?php echo date('d/m/Y',strtotime($investment->iv_date_created)); ?>
                                        </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($investment->manager)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Created By:</td>
                                        <td><?php echo $investment->manager->full_name; ?> (<?php echo ucwords(str_replace('_',' ',$investment->manager->type)); ?>)</td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($investment->iv_date_updated)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Updated on:</td>
                                        <td>
                                        <?php echo date('d/m/Y',strtotime($investment->iv_date_updated)); ?>
                                        </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($investment->edit_manager)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Updated By:</td>
                                        <td><?php echo $investment->edit_manager->full_name; ?> (<?php echo ucwords(str_replace('_',' ',$investment->edit_manager->type)); ?>)</td>
                                    </tr>
                                    <?php endif ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Row -->
        </div>

        <!-- ============================================================== -->
        <!-- End Container fluid -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->