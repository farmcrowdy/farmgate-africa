<!-- Page wrapper -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item">Outgoing Orders</li>
                            <li class="breadcrumb-item active" aria-current="page">Outgoing Order Details</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <div class="m-r-10">
                        <div class="lastmonth"></div>
                    </div>
                    <div class="">
                        <small>LAST MONTH</small>
                        <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Orders -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-sm-12 col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs no-nav-border" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#specification" role="tab" aria-controls="order-specification" aria-selected="true">Outgoing Order Details</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content card-body" id="myTabContent">
                        <div class="tab-pane fade show active" id="specification" role="tabpanel" aria-labelledby="order-specification-tab">
                            <table class="table no-table-border">
                                <tbody class=" no-table-border">
                                    <tr class=" no-table-border">
                                        <td class="product-title">Buyer</td>
                                        <td><?php echo $outgoing_order->buyer; ?></a></td>
                                    </tr>
                                    <?php if(!empty($outgoing_order->produce)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Produce :</td>
                                        <td><?php echo ucwords($outgoing_order->produce); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(isset($outgoing_order->oo_quantity)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Quantity:</td>
                                        <td><?php echo ($outgoing_order->oo_quantity); ?> MT </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(isset($outgoing_order->oo_amount)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Amount:</td>
                                        <td><?php echo $outgoing_order->currency.number_format($outgoing_order->oo_amount); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($outgoing_order->oo_pay_status)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Pay Status:</td>
                                        <td><?php echo ucwords($outgoing_order->oo_pay_status); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($outgoing_order->oo_transfer_status)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Delivery:</td>
                                        <td><?php echo ucwords($outgoing_order->oo_transfer_status); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($outgoing_order->oo_wh_approved)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Warehouse Mgr. Approved:</td>
                                        <td><?php echo ucwords($outgoing_order->oo_wh_approved); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($outgoing_order->oo_admin_approved)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Admin Approved:</td>
                                        <td><?php echo ucwords($outgoing_order->oo_admin_approved); ?> </td>
                                    </tr>
                                    <?php endif ?>                                    
                                    <?php if(!empty($outgoing_order->warehouse_name)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Warehouse (Source):</td>
                                        <td><?php echo ucwords($outgoing_order->warehouse_name); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($outgoing_order->buyer_address)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Destination:</td>
                                        <td><?php echo ucwords($outgoing_order->buyer_address); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($outgoing_order->oo_date_created)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Created on:</td>
                                        <td>
                                        <?php echo date('d / m / Y',strtotime($outgoing_order->oo_date_created)); ?>
                                        </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($outgoing_order->manager)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Created By:</td>
                                        <td><?php echo $outgoing_order->manager->full_name; ?> (<?php echo ucwords(str_replace('_',' ',$outgoing_order->manager->type)); ?>)</td>
                                    </tr>
                                    <?php endif ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Row -->
        </div>

        <!-- ============================================================== -->
        <!-- End Container fluid -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->