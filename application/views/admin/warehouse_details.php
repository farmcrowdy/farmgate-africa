<!-- Page wrapper -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item">Warehouses</li>
                            <li class="breadcrumb-item active" aria-current="page">Warehouse Details</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <div class="m-r-10">
                        <div class="lastmonth"></div>
                    </div>
                    <div class="">
                        <small>LAST MONTH</small>
                        <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Orders -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-sm-12 col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs no-nav-border" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#specification" role="tab" aria-controls="order-specification" aria-selected="true">Warehouse Details</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content card-body" id="myTabContent">
                        <div class="tab-pane fade show active" id="specification" role="tabpanel" aria-labelledby="order-specification-tab">
                            <table class="table no-table-border">
                                <tbody class=" no-table-border">
                                    <tr class=" no-table-border">
                                        <td class="product-title">Warehouse Name</td>
                                        <td><?php echo $warehouse->wh_name; ?></a></td>
                                    </tr>
                                    <?php if(!empty($warehouse->wh_address)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Address:</td>
                                        <td><?php echo $warehouse->wh_address; ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($warehouse->wh_products)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Products Obtainable:</td>
                                        <td><?php echo $warehouse->wh_products; ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(isset($warehouse->wh_current_capacity)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Current Capacity (MT):</td>
                                        <td><?php echo number_format($warehouse->wh_current_capacity); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($warehouse->wh_max_capacity)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Max Capacity (MT):</td>
                                        <td><?php echo number_format($warehouse->wh_max_capacity); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($warehouse->manager)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Manager:</td>
                                        <td><?php echo $warehouse->manager; ?></td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($warehouse->wh_manager_email)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Email:</td>
                                        <td><?php echo $warehouse->wh_manager_email; ?></td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($warehouse->wh_date_created)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Created on:</td>
                                        <td>
                                        <?php echo date('d/m/Y',strtotime($warehouse->wh_date_created)); ?>
                                        </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($warehouse->admin)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Created By:</td>
                                        <td><?php echo $warehouse->admin; ?> (Administrator)</td>
                                    </tr>
                                    <?php endif ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Row -->
        </div>

        <!-- ============================================================== -->
        <!-- End Container fluid -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->