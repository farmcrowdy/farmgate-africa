<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Logs (<?php if(!empty($total_logs)){echo $total_logs;}else{echo "0"; } ?>)</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <div class="m-r-10">
                        <div class="lastmonth"></div>
                    </div>
                    <div class=""><small>LAST MONTH</small>
                        <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Orders -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- Column -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <form class="form-inline" method="get" action="<?php echo base_url('admin/logs'); ?>">
                                    <input name="keyword_lg" type="search" class="form-control" placeholder="Find by User ID" aria-label="Username" aria-describedby="basic-addon1"
                                        value="<?php if(!empty($keyword_lg)){echo $keyword_lg;} ?>">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn search-btn" style="background: none;">
                                            <i class="fas fa-search"></i>
                                        </button>
                                        <!-- <i class="fas fa-search m-auto"></i> -->
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- title -->
                        <div class="table-responsive  scrollable m-t-10" style="height:400px;">
                            <table class="table v-middle">
                                <thead>
                                    <tr>
                                        <th class="border-top-0">ID</th>
                                        <th class="border-top-0">User ID</th>
                                        <th class="border-top-0">User Name</th>
                                        <th class="border-top-0">Message</th>
                                        <th class="border-top-0">Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($logs)): ?>
                                    <?php foreach($logs as $log): ?>
                                    <tr>
                                        <td>
                                            <?php if(!empty($log->lg_id)){echo $log->lg_id;} ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($log->lg_user_id)){echo ucwords($log->lg_user_id);} ?>
                                        </td>                                                                               
                                        <td>
                                            <?php if(!empty($log->username)){echo ($log->username);} ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($log->lg_message)){echo ($log->lg_message);} ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($log->lg_date_created)){echo date('h:i:sa dS F, Y ',strtotime($log->lg_date_created));} ?>
                                        </td>
                                    </tr>
                                    <?php endforeach ?>

                                    <?php else: ?>
                                    <p> There are no logs to display</p>
                                    <?php endif ?>

                                </tbody>

                            </table>
                        </div>
                        <?php echo $pagination_links; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->