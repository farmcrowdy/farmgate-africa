<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Orders (<?php if(!empty($total_orders)){echo $total_orders;}else{echo "0"; } ?>)</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <div class="m-r-10">
                        <div class="lastmonth"></div>
                    </div>
                    <div class=""><small>LAST MONTH</small>
                        <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Orders -->
        <!-- ============================================================== -->
        <div class="row">
            <?php echo $this->session->flashdata('order_del_feedback'); ?>
        </div>
        <div class="row">
            <!-- Column -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <!-- <div class="col-sm-12 col-md-6">
                                <div class="dataTables_length" id="zero_config_length">
                                    <label>Show <select name="zero_config_length" aria-controls="zero_config" class="form-control form-control-sm">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select> entries
                                    </label>
                                </div>
                            </div> -->
                            <div class="col-sm-12 col-md-6">
                                <form class="form-inline" method="get" action="<?php echo base_url('admin/orders'); ?>">
                                    <input name="keyword_ao" type="search" class="form-control" placeholder="Search by Order No" aria-label="Username" aria-describedby="basic-addon1"
                                        value="<?php if(!empty($keyword_ao)){echo $keyword_ao;} ?>">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn search-btn" style="background: none;">
                                            <i class="fas fa-search"></i>
                                        </button>
                                        <!-- <i class="fas fa-search m-auto"></i> -->
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- title -->
                        <div class="table-responsive  scrollable m-t-10" style="height:400px;">
                            <table class="table v-middle">
                                <thead>
                                    <tr>
                                        <th class="border-top-0">Order ID</th>
                                        <th class="border-top-0">Products</th>
                                        <th class="border-top-0">Photo</th>
                                        <th class="border-top-0">Total</th>
                                        <th class="border-top-0">Date</th>
                                        <th class="border-top-0">Customer</th>
                                        <th class="border-top-0">Status</th>
                                        <th class="border-top-0">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($orders)): ?>
                                    <?php foreach($orders as $order): ?>
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <a href="<?php echo base_url('admin/order/'.$order->or_order_number); ?>" target="_blank" class=" item-order-link">
                                                    <h5 class="m-b-0 font-16 font-medium">
                                                        <?php echo strtoupper($order->or_order_number); ?>
                                                    </h5>
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            <?php if(!empty($order->prod_name)){echo $order->prod_name;} elseif(!empty($order->ord_lpo_prod_name)){ echo $order->ord_lpo_prod_name;} ?>
                                        </td>
                                        <td>
                                            <div class=""><img src="<?php echo base_url();?>mainasset/fe/images/products/<?php echo $order->prod_image; ?>"
                                                    alt="<?php echo $order->prod_name. '-'.$order->prod_slug ; ?>" class="circle"
                                                    width="60" /></div>
                                        </td>
                                        <td>&#8358;
                                            <span class="or_amount">
                                                <?php echo number_format($order->ord_total);?>
                                            </span>
                                        </td>
                                        <td width="100">
                                            <?php echo date('d/m/Y', strtotime($order->ord_date_created));?>
                                        </td>
                                        <td>
                                            <?php if(!empty($order->customer_name)) echo $order->customer_name; ?>
                                        </td>
                                        <td class=" order-pending "><span><small class="or_status">
                                                    <?php echo str_replace('_', ' ', $order->or_status); ?></small></span></td>
                                        <td>
                                            <a href="#" class="icon-pad1" data-toggle="modal" data-order-total="<?php echo trim($order->ord_total); ?>" data-order-type="<?php echo trim($order->or_order_type); ?>" data-target=".order-status-modal" data-order-num="<?php echo trim($order->or_order_number); ?>"
                                                data-order-status="<?php echo trim(strtoupper(str_replace('_',' ',$order->or_status))); ?>"><i
                                                    class="far fa-edit"></i></a>
                                            <a href="<?php echo base_url('admin/delorder/'.$order->or_order_number); ?>" class="icon-pad2 del-order"><i
                                                    class="fas fa-times" style="color: #f00;"></i></a>
                                        </td>
                                    </tr>
                                    <?php endforeach ?>

                                    <?php else: ?>
                                    <p> There are no orders to display</p>
                                    <?php endif ?>

                                </tbody>

                            </table>
                        </div>
                        <?php echo $pagination_links; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->