<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item">Users</li>
                            <li class="breadcrumb-item active" aria-current="page">User's Page</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <div class="m-r-10">
                        <div class="lastmonth"></div>
                    </div>
                    <div class=""><small>LAST MONTH</small>
                        <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Earnings -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- Column -->
            <div class="col-sm-12  col-md-8 offset-md-2 offset-lg-0 col-lg-4">
                <div class="card">
                    <div class="card-body border-bottom">
                        <a href="#" class="justify-content-end d-flex pb-2"><i class="fas fa-ellipsis-v"></i></a>
                        <div class="row">
                            <div class="col-4 offset-4 ">
                                <img src="<?php if(!empty($user->image)){echo base_url('mainasset/fe/images/users/').$user->image;}else{ echo 'http://via.placeholder.com/450x450'; } ?>"
                                    class="rounded-circle img-fluid" alt="<?php if(!empty($user->full_name)){echo $user->full_name;}else{echo 'User'; }?>">
                            </div>
                        </div>
                        <p class="text-center individual-product-name">
                            <?php echo $user->full_name; ?>
                        </p>
                        <div class="row">
                            <div class="col-12 text-center">
                                <span class="Buyer">Buyer</span>
                                <span class="Seller">Seller</span>
                                <span class="Flaged">Flaged</span>
                                <span class="Pending">Pending</span>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <!-- <div class="col-4">
                                <i class="fa fa-circle text-primary"></i>
                                <h3 class="m-b-0 font-medium">
                                    <?php //echo $total_products; ?>
                                </h3>
                                <span>Products <br>Listed</span>
                            </div>
                            <div class="col-4">
                                <i class="fa fa-circle text-info"></i>
                                <h3 class="m-b-0 font-medium">9</h3>
                                <span>Products <br>Bought</span>
                            </div>
                            <div class="col-4">
                                <i class="fa fa-circle text-orange"></i>
                                <h3 class="m-b-0 font-medium">7</h3>
                                <span>Products <br>Sold</span>
                            </div> -->
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="">
                            <?php if(!empty($user->channel)): ?>
                            <span class="usersdate">Channel</span>
                            <p class="users-product-title">
                                <?php echo ucwords($user->channel); ?>
                            </p>
                            <?php endif ?>
                            <span class="usersdate">Email Adress</span>
                            <p class="users-product-title">
                                <?php echo $user->email; ?>
                            </p>
                            <span class="usersdate">Phone Number</span>
                            <p class="users-product-title">
                                <?php echo $user->phone; ?>
                            </p>
                            <span class="usersdate">Address</span>
                            <address>
                                <?php echo $user->address; ?></address>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->