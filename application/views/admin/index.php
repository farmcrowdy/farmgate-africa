
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <div class="m-r-10">
                                <div class="lastmonth"></div>
                            </div>
                            <div class="">
                                <small>LAST MONTH</small>
                                <h4 class="text-info m-b-0 font-medium">&#8358;0.00</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Earnings -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-sm-12 col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Earnings</h4>
                                <h5 class="card-subtitle">Total Earnings for the Month</h5>
                                <h2 class="font-medium">&#8358;0.00</h2>
                            </div>
                            <div class="earningsbox m-t-5" style="height:78px; width:100%;"></div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-sm-12 col-lg-8">
                        <div class="card">
                            <div class="card-body border-bottom">
                                <h4 class="card-title">Overview</h4>
                                <h5 class="card-subtitle">Total Earnings for the Month</h5>
                            </div>
                            <div class="card-body">
                                <div class="row m-t-10">
                                    <!-- col -->
                                    <div class="col-md-6 col-sm-12 col-lg-4">
                                        <div class="d-flex align-items-center">
                                            <div class="m-r-10">
                                                <span class="farmgate-color1 display-5">
                                                    <i class="mdi mdi-wallet"></i>
                                                </span>
                                            </div>
                                            <div>
                                                <span class="text-muted">Net Profit</span>
                                                <h3 class="font-medium m-b-0">&#8358;0.00</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- col -->
                                    <!-- col -->
                                    <div class="col-md-6 col-sm-12 col-lg-4">
                                        <div class="d-flex align-items-center">
                                            <div class="m-r-10">
                                                <span class="farmgate-color1  display-5">
                                                    <i class="mdi mdi-basket"></i>
                                                </span>
                                            </div>
                                            <div>
                                                <span class="text-muted">Product sold</span>
                                                <h3 class="font-medium m-b-0">0</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- col -->
                                    <!-- col -->
                                    <div class="col-md-6 col-sm-12 col-lg-4">
                                        <div class="d-flex align-items-center">
                                            <div class="m-r-10">
                                                <span class="display-5 farmgate-color1 ">
                                                    <i class="mdi mdi-account-box"></i>
                                                </span>
                                            </div>
                                            <div>
                                                <span class="text-muted">New Customers</span>
                                                <h3 class="font-medium m-b-0">&#8358;0.00</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- col -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Recent activity</h4>
                                <h5 class="card-subtitle">Total Earnings for the Month</h5>
                                <h2 class="font-medium">&#8358;0.00</h2>
                            </div>
                            <div class=" m-t-5" style="height:78px; width:100%;"></div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-4">
                        <div class="card">
                            <div class="card-body border-bottom">
                                <h4 class="card-title">Order Stats</h4>
                                <h5 class="card-subtitle">Overview of orders</h5>
                                <div class="status m-t-30" style="height:280px; width:100%"></div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-4">
                                        <i class="fa fa-circle text-primary"></i>
                                        <h3 class="m-b-0 font-medium">0</h3>
                                        <span>Success</span>
                                    </div>
                                    <div class="col-4">
                                        <i class="fa fa-circle text-info"></i>
                                        <h3 class="m-b-0 font-medium">0</h3>
                                        <span>Pending</span>
                                    </div>
                                    <div class="col-4">
                                        <i class="fa fa-circle text-orange"></i>
                                        <h3 class="m-b-0 font-medium">0</h3>
                                        <span>Failed</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Trading Index</h4>
                                <h5 class="card-subtitle">Live feeding of trade index</h5>
                                <!-- <h2 class="font-medium">&#8358;43,567.53</h2> -->
                            </div>
                            <div class=" m-t-5" style="height:78px; width:100%;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
