<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Investment Commodities (<?php if(!empty($total_investment_commodities)){echo $total_investment_commodities;}else{echo "0"; } ?>)</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <div class="m-r-10">
                        <div class="lastmonth"></div>
                    </div>
                    <div class=""><small>LAST MONTH</small>
                        <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Orders -->
        <!-- ============================================================== -->
        <div class="row">
            <?php echo $this->session->flashdata('investment_commodity_del_feedback'); ?>
        </div>
        <div class="row">
            <!-- Column -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <!-- <div class="col-sm-12 col-md-6">
                                <div class="dataTables_length" id="zero_config_length">
                                    <label>Show <select name="zero_config_length" aria-controls="zero_config" class="form-control form-control-sm">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select> entries
                                    </label>
                                </div>
                            </div> -->
                            <div class="col-sm-12 col-md-6">
                                <form class="form-inline" method="get" action="<?php echo base_url('admin/inv-commodities'); ?>">
                                    <input name="keyword_invc" type="search" class="form-control" placeholder="Find by Name" aria-label="Username" aria-describedby="basic-addon1"
                                        value="<?php if(!empty($keyword_invc)){echo $keyword_invc;} ?>">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn search-btn" style="background: none;">
                                            <i class="fas fa-search"></i>
                                        </button>
                                        <!-- <i class="fas fa-search m-auto"></i> -->
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- title -->
                        <div class="table-responsive  scrollable m-t-10" style="height:400px;">
                            <table class="table v-middle">
                                <thead>
                                    <tr>
                                        <th class="border-top-0">ID</th>
                                        <th class="border-top-0">Name</th>
                                        <th class="border-top-0">Produce</th>
                                        <th class="border-top-0">Status</th>
                                        <th class="border-top-0">Price/Unit</th>
                                        <th class="border-top-0">Quantity</th>
                                        <th class="border-top-0">Quantity Left</th>
                                        <th class="border-top-0">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($investment_commodities)): ?>
                                    <?php foreach($investment_commodities as $investment_commodity): ?>
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <a href="<?php echo base_url('admin/inv-commodity/'.$investment_commodity->invc_slug); ?>" target="_blank" class=" item-order-link">
                                                    <h5 class="m-b-0 font-16 font-medium">
                                                        <?php echo strtoupper($investment_commodity->invc_slug); ?>
                                                    </h5>
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            <?php if(!empty($investment_commodity->invc_name)){echo $investment_commodity->invc_name;} ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($investment_commodity->category)){echo ucwords($investment_commodity->category);} ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($investment_commodity->invc_status)){echo ucwords(str_replace('_',' ',$investment_commodity->invc_status));} ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($investment_commodity->invc_price_per_unit)){echo number_format($investment_commodity->invc_price_per_unit);} ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($investment_commodity->invc_quantity)){echo number_format($investment_commodity->invc_quantity)." ". $investment_commodity->unit;} ?> 
                                        </td>
                                        <td>
                                            <?php if(!empty($investment_commodity->invc_quantity_left)){echo number_format($investment_commodity->invc_quantity_left)." ".$investment_commodity->unit;}else{echo "0 ".$investment_commodity->unit;} ?>
                                        </td>                                                                                                                       
                                        <td>
                                            <a href="<?php echo base_url('admin/edit-inv-commodity/'.$investment_commodity->invc_slug); ?>" class="icon-pad1">
                                            <i class="far fa-edit"></i></a>
                                            <a href="<?php echo base_url('admin/del-inv-commodity/'.$investment_commodity->invc_slug); ?>" class="icon-pad2 del-invc">
                                            <i class="fas fa-times" style="color: #f00;"></i></a>
                                        </td>
                                    </tr>
                                    <?php endforeach ?>

                                    <?php else: ?>
                                    <p> There are no Investment Commodities to display</p>
                                    <?php endif ?>

                                </tbody>

                            </table>
                        </div>
                        <?php echo $pagination_links; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->