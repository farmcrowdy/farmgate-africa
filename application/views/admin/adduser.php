
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item">Users</li>
                                    <li class="breadcrumb-item active" aria-current="page">Add Users</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <div class="m-r-10">
                                <div class="lastmonth"></div>
                            </div>
                            <div class=""><small>LAST MONTH</small>
                                <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Earnings -->
                <!-- ============================================================== -->
                <div class="row">

                    <div class="col-sm-12 col-lg-8">
                    <?php echo $this->session->flashdata('user_feedback');?>
                        <?php if(!empty($image_errors)){
                            foreach ($image_errors as $error) {
                                    # code...
                                echo $error;
                                } 
                            } 
                        ?>
                        <?php echo form_open_multipart('admin/adduser'); ?>
                            <div class="form-group row">
                                <div class="form-group col-md-6">
                                    <label for="full_name">Full Name</label>
                                    <?php echo form_error('full_name'); ?>
                                    <input type="text" class="form-control" id="full_name" name="full_name"
                                    value="<?php echo set_value('full_name'); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group col-md-8">
                                    <label for="email">Email address</label>
                                    <?php echo form_error('email'); ?>
                                    <input type="email" name="email" id="email" class="form-control" aria-describedby="emailHelp"
                                    value="<?php echo set_value('email'); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group col-md-4">
                                    <label for="password">Password</label>
                                    <?php echo form_error('password'); ?>
                                    <input type="password" name="password" class="form-control" id="password"
                                    value="<?php echo set_value('password'); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group col-md-4">
                                    <label for="typeSelect">Type</label>
                                    <?php echo form_error('status'); ?>
                                    <select class="form-control" id="typeSelect" name="typeSelect">
                                    <?php foreach ($statuses as $status) :?>
                                            <option value="<?php echo $status; ?>"> 
                                            <?php echo strtoupper(str_replace('_',' ',$status)); ?> 
                                            </option>
                                    <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Upload Photo</span>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="userImageFile" name="userImageFile">
                                        <label class="custom-file-label" for="userImageFile">Choose file</label>
                                    </div>
                                    </div>
                            <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                        <!-- Row -->
                </div>
            </div>
                            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            
