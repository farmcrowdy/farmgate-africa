        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">Banks</li>
                                    <li class="breadcrumb-item active" aria-current="page">Add Bank</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <div class="m-r-10">
                                <div class="lastmonth"></div>
                            </div>
                            <div class="">
                                <small>LAST MONTH</small>
                                <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Add Cluster -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-lg-10 col-md-12 col-11">
                    <?php echo $this->session->flashdata('bank_feedback');?>
                        <?php echo form_open('admin/add-bank',array('class'=>'row acct-form mb-4', 'autocomplete'=>"off")); ?>
                            <div class="col-md-12 pad-dashboard">
                                <div class="row mt-5">
                                    <div class="col-md-6">
                                        <label for="">Bank Name
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('name'); ?>
                                        <input type="text" class="form-control" id="" name="name" aria-describedby="productName"
                                        value="<?php echo set_value('name'); ?>">
                                    </div>
                                    <div class="col-md-4">
                                        <label for=""> Status
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('status'); ?>
                                        <select class="form-control" id="status" name="status">
                                        <?php foreach ($statuses as $status) :?>
                                                <option value="<?php echo $status; ?>"> 
                                                <?php echo strtoupper(str_replace('_',' ',$status)); ?> 
                                                </option>
                                        <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 pb-5 pt-4">
                                    <button name="submit" type="submit" class="btn  btn-submit"> Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            
