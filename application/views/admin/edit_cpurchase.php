<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item">Customer Purchases</li>
                            <li class="breadcrumb-item active" aria-current="page">Edit Customer Purchase</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <div class="m-r-10">
                        <div class="lastmonth"></div>
                    </div>
                    <div class="">
                        <small>LAST MONTH</small>
                        <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Add Cluster -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-lg-10 col-md-12 col-11">
                <?php echo $this->session->flashdata('cpurchase_feedback'); ?>
                <?php if (!empty($error_feedback)) {echo $error_feedback;}?>
                <?php echo form_open('admin/edit-cpurchase/'.$cpurchase->cusp_slug, array('class' => 'row acct-form mb-4', 'autocomplete' => "off")); ?>
                <div class="col-md-12 pad-dashboard">
                    <div class="row mt-5"> 
                        <div class="col-md-6">
                            <label for="">Investor Name
                                <span class="superscript-star">*</span>
                            </label><small><em><span id="cusp_search"></span></em></small>
                            <?php echo form_error('investor_name'); ?>
                            <div class="col-md-2" id="investor_name_loader" style="display:none;">
                                <img src="<?php echo base_url('mainasset/fe/images/loader.gif'); ?>" width="30px" />
                            </div>
                            <input type="hidden" class="form-control" id="investor_id" name="investor_id" value="<?php echo set_value('investor_id', $cpurchase->cusp_user_id); ?>">
                            <input type="text" class="form-control" id="investor_name" name="investor_name" aria-describedby="productName" value="<?php echo set_value('investor_name', $cpurchase->investor_name); ?>">
                            <div class="" style="background-color:#fff;" id="suggestion-box"></div>
                        </div>
                    </div>
                    <div class="row py-1">
                        <div class="col-md-4">
                        <label for="">Commodity</label>
                            <select class="form-control" id="commoditySelect" name="commoditySelect">
                                <?php foreach ($commodities as $commodity): ?>
                                <option value="<?php echo $commodity->comm_id; ?>">
                                    <?php echo ucwords($commodity->comm_name); ?> || <?php echo ucwords($commodity->comm_slug); ?>
                                </option>
                                <?php endforeach?>
                            </select>
                        </div>
                        <div class="col-4 form-group">
                            <label for="">Quantity (<span id="prod_quantity">Units</span>)
                                <span class="superscript-star">*</span>
                            </label>
                            <?php echo form_error('cusp_quantity'); ?>
                            <input name="cusp_quantity" class="form-control" value="<?php echo trim(set_value('cusp_quantity', $cpurchase->cusp_quantity)); ?>">
                        </div>

                    </div>
                    <div class="row">
                        <h4 class="page-breadcrumb page-title h4"> Payment Information </h4>
                    </div>
                    <div class="row relative">
                        <div class="col-md-2">
                            <label for="">Currency
                            </label>
                            <select class="form-control" id="currencySelect" name="currencySelect">
                                <?php foreach ($currencies as $currency): ?>
                                <option value="<?php echo $currency->cur_id; ?>">
                                    <?php echo ucwords($currency->cur_name); ?>
                                </option>
                                <?php endforeach?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="">Amount Paid
                            </label>
                            <?php echo form_error('cusp_amount'); ?>
                            <input type="number" class="form-control" min="1" id="" name="cusp_amount" aria-describedby="productName" value="<?php echo set_value('cusp_amount', $cpurchase->cusp_amount); ?>">
                        </div>
                        <div class="col-md-4">
                            <label for="">Pay Status
                            </label>
                            <select class="form-control" id="paySelect" name="paySelect">
                                <?php foreach ($payment_statuses as $payment_status): ?>
                                <option value="<?php echo $payment_status; ?>">
                                    <?php echo ucwords($payment_status); ?>
                                </option>
                                <?php endforeach?>
                            </select>
                        </div>
                    </div>
                    <div class="row relative">
                        <div class="col-md-4">
                            <label for="">Delivery Status
                            </label>
                            <select class="form-control" id="deliverySelect" name="deliverySelect">
                                <?php foreach ($delivery_statuses as $delivery_status): ?>
                                <option value="<?php echo $delivery_status; ?>">
                                    <?php echo ucwords($delivery_status); ?>
                                </option>
                                <?php endforeach?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 pb-5 pt-4">
                            <button name="submit" type="submit" class="btn  btn-submit"> Submit</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->