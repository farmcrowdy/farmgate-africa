<!-- Page wrapper -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item">Orders</li>
                            <li class="breadcrumb-item active" aria-current="page">Order Details</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <div class="m-r-10">
                        <div class="lastmonth"></div>
                    </div>
                    <div class="">
                        <small>LAST MONTH</small>
                        <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Orders -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-sm-12 col-lg-4">
                <div class="card">
                    <div class="card-body border-bottom">
                        <a href="#" class="justify-content-end d-flex pb-2">
                            <i class="fas fa-ellipsis-v"></i>
                        </a>
                        <?php //var_dump($order); ?>
                        <div class="product--status product--status-available">
                            <img src="<?php if(!empty($order->prod_image)){echo base_url('mainasset/fe/images/products/').$order->prod_image; }else{ echo 'https://via.placeholder.com/350x150'; } ?>" class="img-fluid" alt="<?php if(!empty($order->prod_image)) {echo $order->prod_name;} else{ echo "LPO Product";} ?>">
                        </div>
                        <p class="text-center individual-product-name"><?php echo $order->prod_name; ?></p>
                        <p class="text-center individual-product-state"><?php echo $order->local; ?>, <?php echo $order->state; ?></p>
                        <p class="text-center individual-product-amount">&#8358;<?php echo number_format($order->ord_total); ?></p>
                    </div>
                    <!-- <div class="card-body">
                        <div class="row">
                            <div class="col-4">
                                <i class="fa fa-circle text-primary"></i>
                                <h3 class="m-b-0 font-medium">5489</h3>
                                <span>Views</span>
                            </div>
                            <div class="col-4">
                                <i class="fa fa-circle text-info"></i>
                                <h3 class="m-b-0 font-medium">954</h3>
                                <span>Enquiries</span>
                            </div>
                            <div class="col-4">
                                <i class="fa fa-circle text-orange"></i>
                                <h3 class="m-b-0 font-medium">7</h3>
                                <span>Bookmarks</span>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
            <div class="col-sm-12 col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs no-nav-border" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#specification" role="tab" aria-controls="order-specification" aria-selected="true">Specifications</a>
                            </li>
                            <!-- <li class="nav-item">
 <a class="nav-link" data-toggle="tab" href="#orderTimeline" role="tab" aria-controls="order-Timeline" aria-selected="false">Order Timeline</a>
 </li>
 <li class="nav-item">
 <a class="nav-link" data-toggle="tab" href="#messageThread" role="tab" aria-controls="order-Message" aria-selected="false">Message Thread</a>
 </li> -->
                        </ul>
                    </div>
                    <div class="tab-content card-body" id="myTabContent">
                        <div class="tab-pane fade show active" id="specification" role="tabpanel" aria-labelledby="order-specification-tab">
                            <table class="table no-table-border">
                                <tbody class=" no-table-border">
                                    <tr class=" no-table-border">
                                        <td class="product-title">Ordered by:</td>
                                        <td><a style="color: blue;" href="<?php echo base_url('admin/user/').$order->customer_slug; ?>" target="_blank"><?php echo $order->customer_name; ?></a></td>
                                    </tr>
                                    <?php if(!empty($order->prod_name)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Product Name:</td>
                                        <td><?php echo $order->prod_name; ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($order->ord_lpo_prod_name)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Product Name:</td>
                                        <td><?php echo $order->ord_lpo_prod_name; ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($order->ord_quantity)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Quantity</td>
                                        <td><?php echo number_format($order->ord_quantity); ?> <?php echo $order->unit; ?></td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($order->ord_quantity_delivered)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Quantity Delivered</td>
                                        <td><?php echo number_format($order->ord_quantity_delivered); ?> <?php echo $order->unit; ?></td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($order->prod_price_per_unit)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Price per unit</td>
                                        <td><?php echo number_format($order->prod_price_per_unit); ?> / <?php echo $order->unit; ?></td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($order->ord_total)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Total</td>
                                        <td>&#8358;<?php echo number_format($order->ord_total); ?></td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($order->ord_date_created)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Ordered on:</td>
                                        <td>
                                        <?php echo date('d/m/Y',strtotime($order->ord_date_created)); ?>
                                        </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($order->prod_avail_from)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Availability</td>
                                        <td><?php echo date('d/m/Y',strtotime($order->prod_avail_from)); ?> - <?php echo date('d/m/Y',strtotime($order->prod_avail_to)); ?></td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($order->ord_delivery_add)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Delivery Address</td>
                                        <td>
                                            <div class="10"><?php echo $order->ord_delivery_add; ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endif ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Row -->
        </div>

        <!-- ============================================================== -->
        <!-- End Container fluid -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->