        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">Clusters</li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit Cluster</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <div class="m-r-10">
                                <div class="lastmonth"></div>
                            </div>
                            <div class="">
                                <small>LAST MONTH</small>
                                <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Add Cluster -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-lg-10 col-md-12 col-11">
                    <?php echo $this->session->flashdata('cluster_feedback');?>
                        <?php echo form_open('admin/editcluster/'.$cluster->cl_slug,array('class'=>'row acct-form mb-4')); ?>
                            <div class="col-md-12 pad-dashboard">
                                <div class="row mt-5">
                                    <div class="col-md-6">
                                        <label for="">Cluster Name
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('name'); ?>
                                        <input type="text" class="form-control" id="" name="name" aria-describedby="productName"
                                        value="<?php echo set_value('name', $cluster->cl_name); ?>">
                                    </div>
                                </div>
                                <div class="row py-1">
                                    <div class="col-8 form-group">
                                        <label for="">Address
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('address'); ?>
                                        <textarea name="address" class="form-control" rows="5" aria-label="With textarea"><?php echo trim(set_value('address', $cluster->cl_address)); ?></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-8 form-group">
                                        <label for="">Products Obtainable
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('products'); ?>
                                        <textarea name="products" class="form-control" rows="5" aria-label="With textarea"><?php echo trim(set_value('products', $cluster->cl_products)); ?></textarea>
                                    </div>
                                </div>
                                <div class="row relative">
                                    <div class="col-md-6">
                                        <label for="">Manager Name
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('mgr_name'); ?>
                                        <input type="text" class="form-control" id="" name="mgr_name" aria-describedby="productName"
                                        value="<?php echo set_value('mgr_name',$cluster->cl_manager); ?>">
                                    </div>
                                </div>
                                <div class="row relative">
                                    <div class="col-md-6">
                                        <label for="">Phone
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('phone'); ?>
                                        <input type="text" class="form-control" id="" name="phone" aria-describedby="productName"
                                        value="<?php echo set_value('phone', $cluster->cl_manager_phone); ?>">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <label for="">Email
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('email'); ?>
                                        <input type="text" class="form-control" id="" name="email" aria-describedby="productName"
                                        value="<?php echo set_value('email', $cluster->cl_manager_email); ?>">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 pb-5 pt-4">
                                    <button name="submit" type="submit" class="btn  btn-submit"> Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            
