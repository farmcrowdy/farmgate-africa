        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">Products</li>
                                    <li class="breadcrumb-item active" aria-current="page">Product Page</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <div class="m-r-10">
                                <div class="lastmonth"></div>
                            </div>
                            <div class="">
                                <small>LAST MONTH</small>
                                <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Orders -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-sm-12 col-lg-4">
                        <div class="card">
                            <div class="card-body border-bottom">
                                <a href="#" class="justify-content-end d-flex pb-2">
                                    <i class="fas fa-ellipsis-v"></i>
                                </a>
                                <div class="product--status product--status-<?php if(!empty($product->prod_status)){ echo $product->prod_status;} else { echo "sold";} ?>">
                                    <img src="<?php echo base_url();?>mainasset/fe/images/products/<?php echo $product->prod_image; ?>" class="img-fluid" alt="Cassava">
                                </div>
                                <p class="text-center individual-product-name"><?php echo $product->prod_name; ?></p>
                                <p class="text-center individual-product-state"><?php echo $product->local; ?>, <?php echo $product->state; ?></p>
                                <p class="text-center individual-product-amount">&#8358;<?php echo number_format($product->prod_price_per_unit); ?> / <?php echo $product->unit; ?></p>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-4">
                                        <i class="fa fa-circle text-primary"></i>
                                        <h3 class="m-b-0 font-medium">5489</h3>
                                        <span>Views</span>
                                    </div>
                                    <div class="col-4">
                                        <i class="fa fa-circle text-info"></i>
                                        <h3 class="m-b-0 font-medium">954</h3>
                                        <span>Enquiries</span>
                                    </div>
                                    <div class="col-4">
                                        <i class="fa fa-circle text-orange"></i>
                                        <h3 class="m-b-0 font-medium">7</h3>
                                        <span>Bookmarks</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <ul class="nav nav-tabs no-nav-border" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#specification" role="tab" aria-controls="order-specification" aria-selected="true">Specification</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#orderTimeline" role="tab" aria-controls="order-Timeline" aria-selected="false">Order Timeline</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#messageThread" role="tab" aria-controls="order-Message" aria-selected="false">Message Thread</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-content card-body" id="myTabContent">
                                <div class="tab-pane fade show active" id="specification" role="tabpanel" aria-labelledby="order-specification-tab">
                                    <table class="table no-table-border">
                                        <tbody class=" no-table-border">
                                            <tr class=" no-table-border">
                                                <td class="product-title">Volume</td>
                                                <td><?php echo $product->prod_quantity; ?> / <?php echo $product->unit; ?></td>
                                            </tr>
                                            <tr class=" no-table-border">
                                                <td class="product-title">Availability</td>
                                                <td><?php echo date('d/m/Y',strtotime($product->prod_avail_from)); ?> - <?php echo date('d/m/Y',strtotime($product->prod_avail_to)); ?></td>
                                            </tr>
                                            <tr class=" no-table-border">
                                                <td class="product-title">Origin</td>
                                                <td><?php echo $product->state; ?></td>
                                            </tr>
                                            <tr class=" no-table-border">
                                                <td class="product-title">Description</td>
                                                <td>
                                                    <div class="10"><?php echo $product->prod_description; ?></div>  
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="orderTimeline" role="tabpanel" aria-labelledby="order-Timeline-tab">
                                    <div class="row">
                                        <section id="conference-timeline">
                                            <!-- <div class="timeline-start">Start</div> -->
                                            <div class="conference-center-line"></div>
                                            <div class="conference-timeline-content">
                                                <!-- Article -->
                                                <div class="timeline-article">
                                                    <div class="content-right-container">
                                                        <div class="content-right">
                                                            <p class="py-0 my-0 product-listed-date">Product listed</p>
                                                            <p class="py-0 my-0 product-listed-body">FCX9083984098</p>
                                                        </div>
                                                        <span class="timeline-author product-listed-date timeline-author-right">23 May 2018</span>
                                                    </div>
                                                    <div class="meta-date meta-date-left">
                                                    </div>
                                                </div>
                                                <!-- // Article -->

                                                <!-- Article -->
                                                <div class="timeline-article">
                                                    <div class="content-left-container">
                                                        <div class="content-left">
                                                            <p class="py-0 my-0 product-listed-date">Sample Requested by Buyer</p>
                                                            <p class="py-0 my-0 product-listed-body">Adamu Ibrahim requested for product sample.</p>
                                                        </div>
                                                        <span class="timeline-author product-listed-date timeline-author-left">29 May 2018</span>
                                                    </div>
                                                    <div class="meta-date meta-date-right">
                                                    </div>
                                                </div>
                                                <!-- // Article -->

                                                <!-- Article -->
                                                <div class="timeline-article">
                                                    <div class="content-right-container">
                                                        <div class="content-right py-2">
                                                            <p class="py-0 my-0 product-listed-date">Product Sample Recieved </p>
                                                            <p class="py-0 my-0 product-listed-body">Product sample received by FarmgateHQ</p>
                                                        </div>
                                                        <span class="timeline-author product-listed-date timeline-author-right">29 June 2018</span>
                                                    </div>
                                                    <div class="meta-date meta-date-left"></div>
                                                </div>
                                                <!-- // Article -->
                                                <!-- Article -->
                                                <div class="timeline-article">
                                                    <div class="content-left-container">
                                                        <div class="content-left py-2">
                                                            <p class="py-0 my-0 product-listed-date">Product Sample Recieved </p>
                                                            <p class="py-0 my-0 product-listed-body">Product sample received by FarmgateHQ</p>
                                                        </div>
                                                        <span class="timeline-author product-listed-date timeline-author-left">29 June 2018</span>
                                                    </div>
                                                    <div class="meta-date meta-date-right"></div>
                                                </div>
                                                <!-- // Article -->
                                                <!-- Article -->
                                                <div class="timeline-article">
                                                    <div class="content-right-container">
                                                        <div class="content-right py-2">
                                                            <p class="py-0 my-0 product-listed-date">Product Sample Recieved </p>
                                                            <p class="py-0 my-0 product-listed-body">Product sample received by FarmgateHQ</p>
                                                        </div>
                                                        <span class="timeline-author product-listed-date timeline-author-right">29 June 2018</span>
                                                    </div>
                                                    <div class="meta-date meta-date-left"></div>
                                                </div>
                                                <!-- // Article -->
                                            </div>
                                        </section>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="messageThread" role="tabpanel" aria-labelledby="order-Message-tab">
                                    <div class="row">
                                        <div class="col-12  px-md-5 px-3">
                                            <div class="row pt-4 mb-0 pb-0" style="background: #ffffff;">
                                                <div class="col-md-4">
                                                    <ul class="pl-0 listed-chats chat-scrollbar" style="list-style-type: none;">
                                                        <li class="border-bottom pt-4">
                                                            <a href="#" class="chat-profile-link">
                                                                <img src="<?php echo base_url();?>mainasset/be/assets/images/img2.jpg" class=" select-chat image-fluid" alt="profile image">
                                                                <span class="personal-chat-list pl-1">Chukwudi Adamu</span>
                                                                <p class="personal-chat-list-date">2:09PM - 3 Jun 2018</p>
                                                            </a>
                                                        </li>
                                                        <li class="border-bottom  pt-4">
                                                            <a href="#" class="chat-profile-link">
                                                                <img src="<?php echo base_url();?>mainasset/be/assets/images/img3.jpg" class=" select-chat image-fluid" alt="profile image">
                                                                <span class="personal-chat-list pl-1">Chukwudi Adamu</span>
                                                                <p class="personal-chat-list-date">2:09PM - 3 Jun 2018</p>
                                                            </a>
                                                        </li>
                                                        <li class="border-bottom  pt-4">
                                                            <a href="#" class="chat-profile-link">
                                                                <img src="<?php echo base_url();?>mainasset/be/assets/images/img2.jpg" class=" select-chat image-fluid" alt="profile image">
                                                                <span class="personal-chat-list pl-1">Coscharis Lateef</span>
                                                                <p class="personal-chat-list-date">2:09PM - 3 Jun 2018</p>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-8 border-left">
                                                    <ul class="pl-0 message-list chat-scrollbar" style="list-style-type: none;">
                                                        <li class="d-flex justify-content-start my-2">
                                                            <div class="speech-bubble-right">
                                                                <p class="p-2 mb-0 chat-text">Hi Admin</p>
                                                                <div class="speech-bubble-border-right"></div>
                                                            </div>
                                                            <img src="<?php echo base_url();?>mainasset/be/assets/images/img2.jpg" class=" chat-profile-left image-fluid" alt="profile image">
                                                            <p class="chat-date-left">2:09PM - 3 Jun 2018</p>
                                                        </li>
                                                        <!-- second chat -->
                                                        <li class="d-flex justify-content-end my-2">
                                                            <div class="speech-bubble-left my-2">
                                                                <p class="p-2 mb-0 chat-text">Hello, how may I help you today?</p>
                                                                <div class="speech-bubble-border-left"></div>
                                                            </div>
                                                            <!-- <img src="<?php echo base_url();?>mainasset/be/assets/images/img3.jpg" class=" chat-profile-right image-fluid" alt="profile image"> -->
                                                            <p class="chat-date-right">2:09PM - 3 Jun 2018</p>
                                                        </li>
                                                        <li class="d-flex justify-content-end my-2">
                                                            <div class="speech-bubble-left my-2">
                                                                <p class="p-2 mb-0 chat-text">Hello, are you there?</p>
                                                                <div class="speech-bubble-border-left"></div>
                                                            </div>
                                                            <!-- <img src="<?php echo base_url();?>mainasset/be/assets/images/img3.jpg" class=" chat-profile-right image-fluid" alt="profile image"> -->
                                                            <p class="chat-date-right">2:09PM - 3 Jun 2018</p>
                                                        </li>

                                                        <li class="d-flex justify-content-start my-2">
                                                            <div class="speech-bubble-right">
                                                                <p class="p-2 mb-0 chat-text">I'm having issues with my account, it does
                                                                    not display all my purchased items again.
                                                                    please help!!!</p>
                                                                <div class="speech-bubble-border-right"></div>
                                                            </div>
                                                            <img src="<?php echo base_url();?>mainasset/be/assets/images/img2.jpg" class=" chat-profile-left image-fluid" alt="profile image">
                                                            <p class="chat-date-left">2:09PM - 3 Jun 2018</p>
                                                        </li>
                                                    </ul>
                                                    <div class="row">
                                                        <div class="col-12" style="background:#FAFAFA;">
                                                            <div class="row">
                                                                <div class="col-10 offset-1 d-flex justify-content-center chat-textarea my-3">
                                                                    <label for="upload-picture-for-admin" class="btn h-100 upload-file-chat">
                                                                        <i class="fas fa-camera"></i>
                                                                    </label>
                                                                    <input id="upload-picture-for-admin" name="file" accept="" type="file" class="hidden">
                                                                    <textarea class="form-control" aria-label="With textarea"></textarea>
                                                                    <button type="submit" class="btn btn-send-chat">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate" viewBox="0 0 30 25"
                                                                            width="30" height="25">
                                                                            <defs>
                                                                                <clipPath id="_clipPath_gyJeUhUPZK0mDzwRUCvjny9R8qzzkACb">
                                                                                    <rect width="30" height="25" />
                                                                                </clipPath>
                                                                            </defs>
                                                                            <g clip-path="url(#_clipPath_gyJeUhUPZK0mDzwRUCvjny9R8qzzkACb)">
                                                                                <clipPath id="_clipPath_OZWTecr3mGY6LYKw8M9cJwe2zbXNAzqu">
                                                                                    <rect x="0.968" y="-1" width="29.032" height="25" transform="matrix(1,0,0,1,0,0)" fill="rgb(255,255,255)" />
                                                                                </clipPath>
                                                                                <g clip-path="url(#_clipPath_OZWTecr3mGY6LYKw8M9cJwe2zbXNAzqu)">
                                                                                    <g id="Group">
                                                                                        <path d=" M 3.172 1.206 L 3.172 11.049 L 24.411 12.603 L 3.172 14.158 L 3.172 24 L 30 12.603 L 3.172 1.206 Z " fill="rgb(216,216,216)"
                                                                                            vector-effect="non-scaling-stroke"
                                                                                            stroke-width="0.786"
                                                                                            stroke="rgb(112,112,112)"
                                                                                            stroke-opacity="100"
                                                                                            stroke-linejoin="miter"
                                                                                            stroke-linecap="butt"
                                                                                            stroke-miterlimit="4"
                                                                                        />
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </svg>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Row -->
                </div>

                <!-- ============================================================== -->
                <!-- End Container fluid  -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- footer -->
