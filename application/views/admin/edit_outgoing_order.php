<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item">Outgoing Orders</li>
                            <li class="breadcrumb-item active" aria-current="page">Edit Outgoing Order</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <div class="m-r-10">
                        <div class="lastmonth"></div>
                    </div>
                    <div class="">
                        <small>LAST MONTH</small>
                        <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Add Cluster -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-lg-10 col-md-12 col-11">
                <?php echo $this->session->flashdata('outgoing_order_feedback');?>
                <?php if(!empty($error_feedback)){echo $error_feedback; }?>
                <?php echo form_open('admin/edit-outgoing-order/'.$outgoing_order->oo_slug,array('class'=>'row acct-form mb-4', 'autocomplete'=>"off")); ?>
                <div class="col-md-12 pad-dashboard">
                    <div class="row mt-5">
                        <div class="col-4 form-group">
                            <label for=""> Location (Warehouse) </label>
                            <select class="form-control" id="whSelect" name="whSelect">
                                <?php if(!empty($warehouse)): ?>
                                <option value="<?php echo $warehouse->wh_id; ?>">
                                    <?php echo ucwords($warehouse->wh_name); ?>
                                </option>
                                <?php endif ?>
                            </select>
                        </div>
                        <div class="col-4 form-group">
                            <label for=""> Order Number </label>
                            <select class="form-control" id="orSelect" name="orSelect">
                            <?php if(!empty($cpurchase)): ?>
                                <option value="<?php echo trim($cpurchase->cusp_slug); ?>">
                                    <?php echo ucwords($cpurchase->cusp_slug); ?>
                                </option>
                            <?php endif ?>
                            </select>
                        </div>
                    </div>
                    <div class="row py-1">
                        <div class="col-4 form-group">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <ul class="nav nav-tabs no-nav-border" id="myTab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#specification" role="tab" aria-controls="order-specification" aria-selected="true">Purchase
                                                    Details</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="tab-content card-body" id="myTabContent">
                                        <div class="tab-pane fade show active" id="specification" role="tabpanel" aria-labelledby="order-specification-tab">
                                            <table class="table no-table-border">
                                                <tbody id="order_details" class=" no-table-border">
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-4 form-group">
                            <label for="">Quantity to Deliver(<span id="prod_quantity">MT</span>)
                                <span class="superscript-star">*</span>
                            </label>
                            <?php echo form_error('oo_quantity'); ?>
                            <input type="number" id="oo_quantity" name="oo_quantity" class="form-control" value="<?php echo trim(set_value('oo_quantity', $outgoing_order->oo_quantity)); ?>" min="0">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4 form-group">
                            <label for="whmSelect">Warehouse Manager Approved</label>
                            <select class="form-control" id="whmSelect" name="whmSelect">
                                <?php foreach ($wh_statuses as $wh_status) :?>
                                <option value="<?php echo $wh_status; ?>">
                                    <?php echo ucwords($wh_status); ?>
                                </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4 form-group">
                            <label for="adminSelect">Administrator Approved</label>
                            <select class="form-control" id="adminSelect" name="adminSelect">
                                <?php foreach ($admin_statuses as $admin_status) :?>
                                <option value="<?php echo $admin_status; ?>">
                                    <?php echo ucwords($admin_status); ?>
                                </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <h4 class="page-breadcrumb page-title h4"> Payment Information </h4>
                    </div>
                    <div class="row relative">
                        <div class="col-md-4">
                            <label for="">Currency</label>
                            <select class="form-control" id="currencySelect" name="currencySelect">
                                <?php foreach ($currencies as $currency) :?>
                                <option value="<?php echo $currency->cur_id; ?>">
                                    <?php echo ucwords($currency->cur_name); ?>
                                </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="">Transfer Cost</label>
                            <?php echo form_error('oo_transfer_cost'); ?>
                            <input type="number" class="form-control" id="oo_transfer_cost" name="oo_transfer_cost" aria-describedby="productName" value="<?php echo set_value('oo_transfer_cost', $outgoing_order->oo_transfer_cost); ?>">
                        </div>
                    </div>
                    <div class="row relative">
                        <div class="col-md-4">
                            <label for="">Pay Status
                            </label>
                            <select class="form-control" id="paySelect" name="paySelect">
                                <?php foreach ($payment_statuses as $payment_status) :?>
                                <option value="<?php echo $payment_status; ?>">
                                    <?php echo ucwords($payment_status); ?>
                                </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="">Delivery Status
                            </label>
                            <select class="form-control" id="deliverySelect" name="deliverySelect">
                                <?php foreach ($delivery_statuses as $delivery_status) :?>
                                <option value="<?php echo $delivery_status; ?>">
                                    <?php echo ucwords($delivery_status); ?>
                                </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 pb-5 pt-4">
                            <button name="submit" type="submit" class="btn  btn-submit">Submit</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->