<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Inventory - Collection Centers (<?php if(!empty($total_collection_centers)){echo $total_collection_centers;}else{echo "0"; } ?>) || Warehouses (<?php if(!empty($total_warehouses)){echo $total_warehouses;}else{echo "0"; } ?>)
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <div class="m-r-10">
                        <div class="lastmonth"></div>
                    </div>
                    <div class=""><small>LAST MONTH</small>
                        <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Orders -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- Column -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <!-- <div class="col-sm-12 col-md-6">
                                <div class="dataTables_length" id="zero_config_length">
                                    <label>Show <select name="zero_config_length" aria-controls="zero_config" class="form-control form-control-sm">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select> entries
                                    </label>
                                </div>
                            </div> -->
                            <div class="col-sm-12 col-md-6">
                            <h3> Collection Centers <span>  </span></h3>
                                <form class="form-inline" method="get" action="<?php echo base_url('admin/collection-centers'); ?>">
                                    <input name="keyword_cc" type="search" class="form-control" placeholder="Find by Name" aria-label="Username" aria-describedby="basic-addon1"
                                        value="<?php if(!empty($keyword_cc)){echo $keyword_cc;} ?>">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn search-btn" style="background: none;">
                                            <i class="fas fa-search"></i>
                                        </button>
                                        <!-- <i class="fas fa-search m-auto"></i> -->
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- title -->
                        <div class="table-responsive  scrollable m-t-10" style="height:400px;">
                            <table class="table v-middle">
                                <thead>
                                    <tr>
                                        <th class="border-top-0">ID</th>
                                        <th class="border-top-0">Name</th>
                                        <th class="border-top-0">Products</th>
                                        <th class="border-top-0">Current Capacity</th>
                                        <th class="border-top-0">Max Capacity</th>
                                        <th class="border-top-0">Address</th>
                                        <th class="border-top-0">Manager</th>
                                        <th class="border-top-0">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($collection_centers)): ?>
                                    <?php foreach($collection_centers as $collection_center): ?>
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <a href="<?php echo base_url('admin/collection-center/'.$collection_center->cc_slug); ?>" target="_blank" class=" item-order-link">
                                                    <h5 class="m-b-0 font-16 font-medium">
                                                        <?php echo strtoupper($collection_center->cc_slug); ?>
                                                    </h5>
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            <?php if(!empty($collection_center->cc_name)){echo $collection_center->cc_name;} ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($collection_center->cc_products)){echo $collection_center->cc_products; }?>
                                        </td>
                                        <td>
                                            <?php if(!empty($collection_center->cc_current_capacity)){echo number_format($collection_center->cc_current_capacity); }else{ echo number_format(0);}?> MT
                                        </td>
                                        <td>
                                            <?php if(!empty($collection_center->cc_max_capacity)){echo number_format($collection_center->cc_max_capacity); }?> MT
                                        </td>
                                        <td>
                                            <?php if(!empty($collection_center->cc_address)){echo $collection_center->cc_address; }?>
                                        </td>
                                        <td>
                                            <?php if(!empty($collection_center->manager)){echo $collection_center->manager ; }?>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url('admin/edit-collection-center/'.$collection_center->cc_slug); ?>" class="icon-pad1">
                                                <i class="far fa-edit"></i></a>
                                            <a href="<?php echo base_url('admin/del-collection-center/'.$collection_center->cc_slug); ?>" class="icon-pad2 del-cc">
                                                <i class="fas fa-times" style="color: #f00;"></i></a>
                                        </td>
                                    </tr>
                                    <?php endforeach ?>

                                    <?php else: ?>
                                    <p> There are no collection centers to display</p>
                                    <?php endif ?>

                                </tbody>

                            </table>
                        </div>
                        <?php echo $pagination_links; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <!-- <div class="col-sm-12 col-md-6">
                                <div class="dataTables_length" id="zero_config_length">
                                    <label>Show <select name="zero_config_length" aria-controls="zero_config" class="form-control form-control-sm">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select> entries
                                    </label>
                                </div>
                            </div> -->
                            <div class="col-sm-12 col-md-6">
                                <h3> Warehouses </h3>
                                <form class="form-inline" method="get" action="<?php echo base_url('admin/warehouses'); ?>">
                                    <input name="keyword_wh" type="search" class="form-control" placeholder="Find by Name" aria-label="Username" aria-describedby="basic-addon1"
                                        value="<?php if(!empty($keyword_wh)){echo $keyword_wh;} ?>">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn search-btn" style="background: none;">
                                            <i class="fas fa-search"></i>
                                        </button>
                                        <!-- <i class="fas fa-search m-auto"></i> -->
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- title -->
                        <div class="table-responsive  scrollable m-t-10" style="height:400px;">
                            <table class="table v-middle">
                                <thead>
                                    <tr>
                                        <th class="border-top-0">ID</th>
                                        <th class="border-top-0">Name</th>
                                        <th class="border-top-0">Products</th>
                                        <th class="border-top-0">Current Capacity</th>
                                        <th class="border-top-0">Max Capacity</th>
                                        <th class="border-top-0">Address</th>
                                        <th class="border-top-0">Manager</th>
                                        <th class="border-top-0">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($warehouses)): ?>
                                    <?php foreach($warehouses as $warehouse): ?>
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <a href="<?php echo base_url('admin/warehouse/'.$warehouse->wh_slug); ?>" target="_blank" class=" item-order-link">
                                                    <h5 class="m-b-0 font-16 font-medium">
                                                        <?php echo strtoupper($warehouse->wh_slug); ?>
                                                    </h5>
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            <?php if(!empty($warehouse->wh_name)){echo $warehouse->wh_name;} ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($warehouse->wh_products)){echo $warehouse->wh_products;} ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($warehouse->wh_current_capacity)){echo number_format($warehouse->wh_current_capacity) ;}else{ echo "0";}?> MT
                                        </td>
                                        <td>
                                            <?php if(!empty($warehouse->wh_max_capacity)){echo number_format($warehouse->wh_max_capacity) ;}else{ echo "0";}?> MT
                                        </td>
                                        <td>
                                            <?php if(!empty($warehouse->wh_address)){echo $warehouse->wh_address ;}?>
                                        </td>
                                        <td>
                                            <?php if(!empty($warehouse->manager)){echo $warehouse->manager ;}?>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url('admin/editwarehouse/'.$warehouse->wh_slug); ?>" class="icon-pad1">
                                                <i class="far fa-edit"></i></a>
                                            <a href="<?php echo base_url('admin/delwarehouse/'.$warehouse->wh_slug); ?>" class="icon-pad2 del-wh">
                                                <i class="fas fa-times" style="color: #f00;"></i></a>
                                        </td>
                                    </tr>
                                    <?php endforeach ?>

                                    <?php else: ?>
                                    <p> There are no warehouses to display</p>
                                    <?php endif ?>

                                </tbody>

                            </table>
                        </div>
                        <?php echo $pagination_links; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->