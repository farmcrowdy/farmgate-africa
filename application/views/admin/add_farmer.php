        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">Farmers</li>
                                    <li class="breadcrumb-item active" aria-current="page">Add Farmer</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <div class="m-r-10">
                                <div class="lastmonth"></div>
                            </div>
                            <div class="">
                                <small>LAST MONTH</small>
                                <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Add Cluster -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-lg-10 col-md-12 col-11">
                    <?php echo $this->session->flashdata('farmer_feedback');?>
                        <?php echo form_open('admin/add-farmer',array('class'=>'row acct-form mb-4', 'autocomplete'=>"off")); ?>
                            <div class="col-md-12 pad-dashboard">
                                <div class="row mt-5">
                                    <div class="col-md-6">
                                        <label for="">Farmer Name
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('name'); ?>
                                        <input type="text" class="form-control" id="" name="name" aria-describedby="productName"
                                        value="<?php echo set_value('name'); ?>">
                                    </div>
                                    <div class="col-md-2">
                                        <label for="">Gender
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('gender'); ?>
                                        <select class="form-control" id="gender" name="gender">
                                        <?php foreach ($sexes as $sex) :?>
                                                <option value="<?php echo $sex; ?>"> 
                                                <?php echo ucwords(str_replace('_',' ',$sex)); ?> 
                                                </option>
                                        <?php endforeach ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Farmer Status
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('status'); ?>
                                        <select class="form-control" id="status" name="status">
                                        <?php foreach ($statuses as $status) :?>
                                                <option value="<?php echo $status; ?>"> 
                                                <?php echo strtoupper(str_replace('_',' ',$status)); ?> 
                                                </option>
                                        <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row py-1">
                                    <div class="col-6 form-group">
                                        <label for="">Email
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('email'); ?>
                                        <input name="email" class="form-control" value="<?php echo trim(set_value('email')); ?>">
                                    </div>
                                    <div class="col-4 form-group">
                                        <label for="">Phone
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('phone'); ?>
                                        <input name="phone" class="form-control" value="<?php echo trim(set_value('phone')); ?>">
                                    </div>
                                    <div class="col-2 form-group">
                                        <label for="">Age
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('age'); ?>
                                        <input type="number" name="age" class="form-control" value="<?php echo trim(set_value('age')); ?>">
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-8 form-group">
                                        <label for="">Address
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('address'); ?>
                                        <textarea name="address" class="form-control" rows="5" aria-label="With textarea"><?php echo trim(set_value('address')); ?></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Farm Size
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('farm_size'); ?>
                                        <input type="text" class="form-control" min="1" id="" name="farm_size" aria-describedby="productName"
                                        value="<?php echo set_value('farm_size'); ?>">
                                    </div>
                                </div>
                                <div class="row relative">
                                    <div class="col-md-6">
                                        <label for="">Primary Produce
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('primary_produce'); ?>
                                        <input type="text" class="form-control" min="1" id="" name="primary_produce" aria-describedby="productName"
                                        value="<?php echo set_value('primary_produce'); ?>">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Other Produce
                                        </label>
                                        <?php echo form_error('other_produce'); ?>
                                        <input type="text" class="form-control" min="1" id="" name="other_produce" aria-describedby="productName"
                                        value="<?php echo set_value('other_produce'); ?>">
                                    </div>
                                </div>
                                <div class="row relative">
                                    <div class="col-md-6">
                                        <label for="">Estimated Annual Produce (MT)
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('est_annual_produce'); ?>
                                        <input type="text" class="form-control" min="1" id="" name="est_annual_produce" aria-describedby="productName"
                                        value="<?php echo set_value('est_annual_produce'); ?>">
                                    </div>
                                </div>
                                <div class="row relative">
                                    <div class="col-md-6">
                                        <label for="">Bank Account Name
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('bank_acc_name'); ?>
                                        <input type="text" class="form-control" min="1" id="" name="bank_acc_name" aria-describedby="productName"
                                        value="<?php echo set_value('bank_acc_name'); ?>">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Bank Account Number
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('bank_acc_no'); ?>
                                        <input type="text" class="form-control" min="1" id="" name="bank_acc_no" aria-describedby="productName"
                                        value="<?php echo set_value('bank_acc_no'); ?>">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                    <label for="">Select Bank 
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('bank'); ?>
                                        <select class="form-control" id="bank" name="bank">
                                        <?php if(!empty($banks)): ?>
                                        <?php foreach ($banks as $bank) :?>
                                                <option value="<?php echo $bank->bk_id; ?>"> 
                                                <?php echo strtoupper(str_replace('_',' ',$bank->bk_name)); ?> 
                                                </option>
                                        <?php endforeach ?>
                                        <?php endif ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 pb-5 pt-4">
                                    <button name="submit" type="submit" class="btn  btn-submit"> Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            
