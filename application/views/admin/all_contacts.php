<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Contacts (<?php if(!empty($total_contacts)){echo $total_contacts;}else{echo "0"; } ?>)</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <div class="m-r-10">
                        <div class="lastmonth"></div>
                    </div>
                    <div class=""><small>LAST MONTH</small>
                        <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Orders -->
        <!-- ============================================================== -->
        <div class="row">
            <?php echo $this->session->flashdata('contact_del_feedback'); ?>
        </div>
        <div class="row">
            <!-- Column -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <!-- <div class="col-sm-12 col-md-6">
                                <div class="dataTables_length" id="zero_config_length">
                                    <label>Show <select name="zero_config_length" aria-controls="zero_config" class="form-control form-control-sm">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select> entries
                                    </label>
                                </div>
                            </div> -->
                            <div class="col-sm-12 col-md-6">
                                <form class="form-inline" method="get" action="<?php echo base_url('admin/contacts'); ?>">
                                    <input name="keyword_contact" type="search" class="form-control" placeholder="Find by Name" aria-label="Username" aria-describedby="basic-addon1"
                                        value="<?php if(!empty($keyword_contact)){echo $keyword_contact;} ?>">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn search-btn" style="background: none;">
                                            <i class="fas fa-search"></i>
                                        </button>
                                        <!-- <i class="fas fa-search m-auto"></i> -->
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- title -->
                        <div class="table-responsive  scrollable m-t-10" style="height:400px;">
                            <table class="table v-middle">
                                <thead>
                                    <tr>
                                        <th class="border-top-0">ID</th>
                                        <th class="border-top-0">User</th>
                                        <th class="border-top-0">Category</th>
                                        <th class="border-top-0">Subject</th>
                                        <th class="border-top-0">Message</th>
                                        <th class="border-top-0">Date Sent</th>
                                        <!-- <th class="border-top-0">Action</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($contacts)): ?>
                                    <?php foreach($contacts as $contact): ?>
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <a href="<?php echo base_url('admin/contact/'.$contact->con_id); ?>" target="_blank" class=" item-order-link">
                                                    <h5 class="m-b-0 font-16 font-medium">
                                                        <?php echo strtoupper($contact->con_id); ?>
                                                    </h5>
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            <?php if(!empty($contact->con_user_name)){echo $contact->con_user_name;} ?>
                                        </td>
                                        <td>
                                        <?php if(!empty($contact->con_category)): ?>
                                            <?php echo $contact->con_category; ?>
                                        <?php endif ?>
                                        </td>
                                        <td>
                                        <?php if(!empty($contact->con_subject)): ?>
                                            <?php echo $contact->con_subject; ?>
                                        <?php endif ?>
                                        </td>
                                        <td>
                                        <?php if(!empty($contact->con_message)): ?>
                                            <?php echo $contact->con_message; ?>
                                        <?php endif ?>
                                        </td>
                                        <td>
                                        <?php if(!empty($contact->con_date_created)): ?>
                                            <?php echo date('d/m/Y',strtotime($contact->con_date_created)); ?>
                                        <?php endif ?>
                                        </td>
                                        <!-- <td>
                                            <a href="<?php // echo base_url('admin/edit-contact/'.$contact->con_id); ?>" class="icon-pad1">
                                            <i class="far fa-edit"></i></a>
                                            <a href="<?php // echo base_url('admin/del-contact/'.$contact->con_id); ?>" class="icon-pad2 del-cusp">
                                            <i class="fas fa-times" style="color: #f00;"></i></a>
                                        </td> -->
                                    </tr>
                                    <?php endforeach ?>

                                    <?php else: ?>
                                    <p> There are no Contacts to display</p>
                                    <?php endif ?>

                                </tbody>

                            </table>
                        </div>
                        <?php echo $pagination_links; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->