        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">Produce Transfers</li>
                                    <li class="breadcrumb-item active" aria-current="page">Add Produce Transfer</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <div class="m-r-10">
                                <div class="lastmonth"></div>
                            </div>
                            <div class="">
                                <small>LAST MONTH</small>
                                <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Add Cluster -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-lg-10 col-md-12 col-11">
                    <?php echo $this->session->flashdata('producetransfer_feedback');?>
                    <?php if(!empty($error_feedback)){echo $error_feedback; }?>
                        <?php echo form_open('admin/add-produce-transfer',array('class'=>'row acct-form mb-4', 'autocomplete'=>"off")); ?>
                            <div class="col-md-12 pad-dashboard">
                                <div class="row mt-5">
                                    <div class="col-md-4">
                                        <label for="">Current Location (Collection Center)
                                            <span class="superscript-star">*</span>
                                        </label>
										<select class="form-control" id="ccSelect" name="ccSelect">
                                           <?php if(!empty($collection_centers)): ?>
											<?php foreach ($collection_centers as $collection_center) :?>
											<option value="<?php echo $collection_center->cc_id; ?>">
												<?php echo ucwords($collection_center->cc_name); ?>
											</option>
											<?php endforeach ?>
                                            <?php endif ?>
										</select>
                                    </div>
                                    <div class="col-4 form-group">
                                        <label for=""> Destination (Warehouse) </label>
										<select class="form-control" id="whSelect" name="whSelect">
                                           <?php if(!empty($warehouses)): ?>
											<?php foreach ($warehouses as $warehouse) :?>
											<option value="<?php echo $warehouse->wh_id; ?>">
												<?php echo ucwords($warehouse->wh_name); ?>
											</option>
											<?php endforeach ?>
                                            <?php endif ?>
										</select>
                                    </div>
                                </div>
                                <div class="row py-1">
                                    <div class="col-2 form-group">
                                        <label for=""> Produce Type</label>
										<select class="form-control" id="categoryTypeSelect" name="categoryTypeSelect">
											<?php foreach ($produce_types as $produce_type) :?>
											<option value="<?php echo $produce_type->pts_id; ?>">
												<?php echo ucwords($produce_type->pts_name); ?>
											</option>
											<?php endforeach ?>
										</select>
                                    </div>
                                    <div class="col-4 form-group">
                                        <label for=""> Produce</label>
                                        <select class="form-control" id="categorySelect" name="categorySelect">

										</select>
                                    </div>
                                    <div class="col-4 form-group">
                                        <label for="">Quantity (<span id="prod_quantity">MT</span>)
                                            <span class="superscript-star">*</span>
                                        </label>
                                        <?php echo form_error('pt_quantity'); ?>
                                        <input name="pt_quantity" class="form-control" value="<?php echo trim(set_value('pt_quantity')); ?>">
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-4 form-group">
                                        <label for="whmSelect">Warehouse Manager Received</label>
                                        <select class="form-control" id="whmSelect" name="whmSelect">
											<?php foreach ($wh_statuses as $wh_status) :?>
											<option value="<?php echo $wh_status; ?>">
												<?php echo ucwords($wh_status); ?>
											</option>
											<?php endforeach ?>
										</select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4 form-group">
                                        <label for="adminSelect">Administrator Approved</label>
                                        <select class="form-control" id="adminSelect" name="adminSelect">
											<?php foreach ($admin_statuses as $admin_status) :?>
											<option value="<?php echo $admin_status; ?>">
												<?php echo ucwords($admin_status); ?>
											</option>
											<?php endforeach ?>
										</select>
                                    </div>
                               </div>
                                <div class="row">
                                    <h4 class="page-breadcrumb page-title h4">Payment Information</h4>
                                </div>
                                <div class="row relative">
                                    <div class="col-md-4">
                                        <label for="currencySelect">Currency</label>
										<select class="form-control" id="currencySelect" name="currencySelect">
											<?php foreach ($currencies as $currency) :?>
											<option value="<?php echo $currency->cur_id; ?>">
												<?php echo ucwords($currency->cur_name); ?>
											</option>
											<?php endforeach ?>
										</select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="pt_transfer_cost">Transfer Cost</label>
                                        <?php echo form_error('pt_transfer_cost'); ?>
                                        <input type="number" class="form-control" min="1" id="" name="pt_transfer_cost" aria-describedby="productName"
                                        value="<?php echo set_value('pt_transfer_cost'); ?>">
                                    </div>
                                </div>
                                <div class="row relative">
                                    <div class="col-md-4">
                                        <label for="">Pay Status</label>
                                        <select class="form-control" id="paySelect" name="paySelect">
											<?php foreach ($payment_statuses as $payment_status) :?>
											<option value="<?php echo $payment_status; ?>">
												<?php echo ucwords($payment_status); ?>
											</option>
											<?php endforeach ?>
										</select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 pb-5 pt-4">
                                    <button name="submit" type="submit" class="btn  btn-submit">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            
