<div class="container">
  <div id="accordion">
  <?php if(!empty($categories)): ?>
  <?php foreach($categories as $category): ?>
    <div class="card">
      <div class="card-header">
        <a class="card-link" data-toggle="collapse" href="#collapse<?php echo $category->c_id; ?>">
        <?php echo $category->c_name; ?>
        </a>
        <i class="fas fa-times del-caty" style="color: #f00;" data-cid="<?php echo $category->c_id; ?>"></i>
      </div>
      <div id="collapse<?php echo $category->c_id; ?>" class="collapse" data-parent="#accordion">
        <div class="card-body">
        <?php if(!empty($category->subcategories)): ?>
        <?php foreach($category->subcategories as $subcategory): ?>
        <li data-cid="<?php echo $subcategory->c_id; ?>">
            <?php echo $subcategory->c_name; ?>
            <i class="fas fa-times del-caty" style="color: #f00;" data-cid="<?php echo $subcategory->c_id; ?>"></i>
        </li>
        <?php endforeach ?>
        <?php endif ?>
        </div>
      </div>
    </div>
    <?php endforeach ?>
    <?php endif ?>
  </div>
</div>