<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Products (<?php if(!empty($total_products)){echo $total_products;}else{echo "0"; } ?>)</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <div class="m-r-10">
                        <div class="lastmonth"></div>
                    </div>
                    <div class=""><small>LAST MONTH</small>
                        <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Orders -->
        <!-- ============================================================== -->
        <div class="row">
            <?php echo $this->session->flashdata('admin_del_feedback'); ?>
            <form class="form-inline" method="get" action="<?php echo base_url('admin/products'); ?>">
                <input name="keyword_ap" type="search" class="form-control" placeholder="Search by Name" aria-label="Username" aria-describedby="basic-addon1"
                    value="<?php if(!empty($keyword_ap)){echo $keyword_ap;} ?>">
                <div class="input-group-append">
                    <button type="submit" class="btn search-btn" style="background: none;">
                        <i class="fas fa-search"></i>
                    </button>
                    <!-- <i class="fas fa-search m-auto"></i> -->
                </div>
            </form>
        </div><br>
        <div class="row">
            <div class="col-12">
                <!-- Row -->
                <div class="row">
                    <!-- column -->

                    <?php if($products): ?>
                    <?php foreach($products as $product): ?>
                    <div class="col-lg-3 col-md-6">
                        <!-- Card -->
                        <div class="card">
                            <a target="_blank" href="<?php echo base_url('admin/product/').$product->prod_slug; ?>">
                                <div class="pad-product product--status product--status-<?php if(!empty($product->prod_status)){ echo $product->prod_status;} else { echo "
                                    sold ";} ?>">
                                    <img class="card-img-top img-responsive resize-img-product" src="<?php echo base_url();?>mainasset/fe/images/products/<?php echo $product->prod_image; ?>"
                                        alt="<?php echo $product->prod_name; ?>">
                                    <div class="card-body">
                                        <h4 class="card-title">
                                            <?php echo $product->prod_name; ?>
                                        </h4>
                                        <p class="card-text small product-id">
                                            <?php echo strtoupper($product->prod_slug); ?>
                                        </p>
                                        <p class="unit">
                                            <?php echo number_format($product->prod_quantity); ?> <span class="small">
                                                <?php echo $product->unit; ?></span></p>
                                        <p class="price-per-mt">&#8358;
                                            <?php echo number_format($product->prod_price_per_unit); ?> <span>per
                                                <?php echo $product->unit; ?></span></p>
                                    </div>
                                </div>
                                <div class="col-10 offset-2">
                                    <a href="<?php echo base_url('admin/product/'.$product->prod_slug); ?>" target="_blank" class="col-3" title="View Product"><i
                                            class="fas fa-eye fa-2x"></i> </a>
                                    <a href="<?php echo base_url('admin/editproduct/'.$product->prod_slug); ?>" target="_blank" class="col-3" title="Edit Product"><i
                                            class="far fa-edit fa-2x"></i> </a>
                                    <a href="<?php echo base_url('admin/delproduct/'.$product->prod_slug); ?>" class="col-3" title="Delete Product"><i
                                            class="far fa-trash-alt fa-2x"></i></a>
                                </div>
                            </a>
                        </div>
                        <!-- Card -->
                    </div>
                    <?php endforeach ?>

                    <?php else: ?>

                    <p> There are no products to display</p>

                    <?php endif ?>
                </div>
                <div>
                    <?php echo $pagination_links;?>
                </div>

                <!-- Row -->

            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->