<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Investments (<?php if(!empty($total_investments)){echo $total_investments;}else{echo "0"; } ?>)</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <div class="m-r-10">
                        <div class="lastmonth"></div>
                    </div>
                    <div class=""><small>LAST MONTH</small>
                        <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Orders -->
        <!-- ============================================================== -->
        <div class="row">
            <?php echo $this->session->flashdata('investment_del_feedback'); ?>
        </div>
        <div class="row">
            <!-- Column -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <!-- <div class="col-sm-12 col-md-6">
                                <div class="dataTables_length" id="zero_config_length">
                                    <label>Show <select name="zero_config_length" aria-controls="zero_config" class="form-control form-control-sm">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select> entries
                                    </label>
                                </div>
                            </div> -->
                            <div class="col-sm-12 col-md-6">
                                <form class="form-inline" method="get" action="<?php echo base_url('admin/investments'); ?>">
                                    <input name="keyword_iv" type="search" class="form-control" placeholder="Find by Name" aria-label="Username" aria-describedby="basic-addon1"
                                        value="<?php if(!empty($keyword_iv)){echo $keyword_iv;} ?>">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn search-btn" style="background: none;">
                                            <i class="fas fa-search"></i>
                                        </button>
                                        <!-- <i class="fas fa-search m-auto"></i> -->
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- title -->
                        <div class="table-responsive  scrollable m-t-10" style="height:400px;">
                            <table class="table v-middle">
                                <thead>
                                    <tr>
                                        <th class="border-top-0">ID</th>
                                        <th class="border-top-0">Investor</th>
                                        <th class="border-top-0">Pay Status</th>
                                        <th class="border-top-0">Produce</th>
                                        <th class="border-top-0">Amount</th>
                                        <th class="border-top-0">Returns</th>
                                        <th class="border-top-0">Return Status</th>
                                        <th class="border-top-0">Due Date</th>
                                        <th class="border-top-0">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($investments)): ?>
                                    <?php foreach($investments as $investment): ?>
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <a href="<?php echo base_url('admin/investment/'.$investment->iv_slug); ?>" target="_blank" class=" item-order-link">
                                                    <h5 class="m-b-0 font-16 font-medium">
                                                        <?php echo strtoupper($investment->iv_slug); ?>
                                                    </h5>
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            <?php if(!empty($investment->investor_name)){echo $investment->investor_name;} ?>
                                        </td>
                                        <td>
                                        <?php if(!empty($investment->iv_pay_status)): ?>
                                            <?php if($investment->iv_pay_status == "pending"): ?>
                                                <button class="btn btn-primary" onclick="investorPaid(<?php echo $investment->iv_id; ?>)" data-iv_id="<?php echo $investment->iv_id; ?>"><?php echo ucwords($investment->iv_pay_status); ?></button>                                            
                                            <?php elseif($investment->iv_pay_status == "paid"): ?>
                                                <button class="btn btn-success"><?php echo ucwords($investment->iv_pay_status); ?></button>
                                            <?php endif?>
                                        <?php endif ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($investment->iv_commodity_name)){echo ucwords($investment->iv_commodity_name);} ?>
                                        </td>                                                                               
                                        <td>
                                            <?php if(!empty($investment->iv_amount)){echo $investment->currency.number_format($investment->iv_amount);} ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($investment->iv_returns)){echo $investment->currency.number_format($investment->iv_returns);} ?>
                                        </td>
                                        <td>
                                        <?php if(!empty($investment->iv_returns_status)): ?>
                                            <?php if($investment->iv_returns_status == "pending"): ?>
                                                <button class="btn btn-primary" onclick="payInvestorROI(<?php echo $investment->iv_id; ?>)" data-piv_id="<?php echo $investment->iv_id; ?>"><?php echo ucwords($investment->iv_returns_status); ?></button>                                            
                                            <?php elseif($investment->iv_returns_status == "paid"): ?>
                                                <button class="btn btn-success"><?php echo ucwords($investment->iv_returns_status); ?></button>
                                            <?php endif?>
                                        <?php endif ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($investment->iv_due_date)){echo date('d-m-Y', strtotime($investment->iv_due_date));} ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url('admin/print_investment/'.$investment->iv_slug); ?>" target="_blank" class="icon-pad1">
                                            <i class="fas fa-print"></i></a>
                                            <a href="<?php echo base_url('admin/edit-investment/'.$investment->iv_slug); ?>" class="icon-pad1">
                                            <i class="far fa-edit"></i></a>
                                            <a href="<?php echo base_url('admin/del-investment/'.$investment->iv_slug); ?>" class="icon-pad2 del-inv">
                                            <i class="fas fa-times" style="color: #f00;"></i></a>
                                        </td>
                                    </tr>
                                    <?php endforeach ?>

                                    <?php else: ?>
                                    <p> There are no investments to display</p>
                                    <?php endif ?>

                                </tbody>

                            </table>
                        </div>
                        <?php echo $pagination_links; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->