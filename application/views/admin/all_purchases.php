<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Purchases (<?php if(!empty($total_purchases)){echo $total_purchases;}else{echo "0"; } ?>)</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <div class="m-r-10">
                        <div class="lastmonth"></div>
                    </div>
                    <div class=""><small>LAST MONTH</small>
                        <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Orders -->
        <!-- ============================================================== -->
        <div class="row">
            <?php echo $this->session->flashdata('purchase_del_feedback'); ?>
        </div>
        <div class="row">
            <!-- Column -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <!-- <div class="col-sm-12 col-md-6">
                                <div class="dataTables_length" id="zero_config_length">
                                    <label>Show <select name="zero_config_length" aria-controls="zero_config" class="form-control form-control-sm">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select> entries
                                    </label>
                                </div>
                            </div> -->
                            <div class="col-sm-12 col-md-6">
                                <form class="form-inline" method="get" action="<?php echo base_url('admin/purchases'); ?>">
                                    <input name="keyword_puo" type="search" class="form-control" placeholder="Find by Name" aria-label="Username" aria-describedby="basic-addon1"
                                        value="<?php if(!empty($keyword_puo)){echo $keyword_puo;} ?>">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn search-btn" style="background: none;">
                                            <i class="fas fa-search"></i>
                                        </button>
                                        <!-- <i class="fas fa-search m-auto"></i> -->
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- title -->
                        <div class="table-responsive  scrollable m-t-10" style="height:400px;">
                            <table class="table v-middle">
                                <thead>
                                    <tr>
                                        <th class="border-top-0">ID</th>
                                        <th class="border-top-0">Name</th>
                                        <th class="border-top-0">Pay Status</th>
                                        <th class="border-top-0">Produce</th>
                                        <th class="border-top-0">Amount</th>
                                        <th class="border-top-0">Created By</th>
                                        <th class="border-top-0">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($purchases)): ?>
                                    <?php foreach($purchases as $purchase): ?>
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <a href="<?php echo base_url('admin/purchase/'.$purchase->po_slug); ?>" target="_blank" class=" item-order-link">
                                                    <h5 class="m-b-0 font-16 font-medium">
                                                        <?php echo strtoupper($purchase->po_slug); ?>
                                                    </h5>
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            <?php if(!empty($purchase->farmer_name)){echo $purchase->farmer_name;} ?>
                                        </td>
                                        <td>
                                        <?php if(!empty($purchase->po_status)): ?>
                                            <?php if($purchase->po_status == "pending"): ?>
                                                <button class="btn btn-primary" onclick="payFarmer(<?php echo $purchase->po_id; ?>)" data-po_id="<?php echo $purchase->po_id; ?>"><?php echo ucwords($purchase->po_status); ?></button>                                            
                                            <?php elseif($purchase->po_status == "paid"): ?>
                                                <button class="btn btn-success"><?php echo ucwords($purchase->po_status); ?></button>
                                            <?php endif?>
                                        <?php endif ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($purchase->category)){echo ucwords($purchase->category);} ?>
                                        </td>                                                                               
                                        <td>
                                            <?php if(!empty($purchase->po_amount)){echo $purchase->currency.number_format($purchase->po_amount);} ?>
                                        </td>
                                        <td>
                                            <?php echo $purchase->manager ;?>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url('admin/print_purchase/'.$purchase->po_slug); ?>" target="_blank" class="icon-pad1">
                                            <i class="fas fa-print"></i></a>
                                            <a href="<?php echo base_url('admin/editpurchase/'.$purchase->po_slug); ?>" class="icon-pad1">
                                            <i class="far fa-edit"></i></a>
                                            <a href="<?php echo base_url('admin/delpurchase/'.$purchase->po_slug); ?>" class="icon-pad2 del-purchase">
                                            <i class="fas fa-times" style="color: #f00;"></i></a>
                                        </td>
                                    </tr>
                                    <?php endforeach ?>

                                    <?php else: ?>
                                    <p> There are no purchases to display</p>
                                    <?php endif ?>

                                </tbody>

                            </table>
                        </div>
                        <?php echo $pagination_links; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->