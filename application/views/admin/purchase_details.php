<!-- Page wrapper -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item">Purchases</li>
                            <li class="breadcrumb-item active" aria-current="page">Purchase Details</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Orders -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-sm-12 col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs no-nav-border" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#specification" role="tab" aria-controls="order-specification" aria-selected="true">Purchase Details</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content card-body" id="myTabContent">
                        <div class="tab-pane fade show active" id="specification" role="tabpanel" aria-labelledby="order-specification-tab">
                            <table class="table no-table-border">
                                <tbody class=" no-table-border">
                                    <tr class=" no-table-border">
                                        <td class="product-title">Farmer Name</td>
                                        <td><?php echo $purchase->farmer_name; ?></a></td>
                                    </tr>
                                    <?php if(!empty($purchase->category)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Produce :</td>
                                        <td><?php echo ucwords($purchase->category); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(isset($purchase->po_quantity)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Quantity:</td>
                                        <td><?php echo ($purchase->po_quantity); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(isset($purchase->po_amount)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Amount:</td>
                                        <td><?php echo $purchase->currency.number_format($purchase->po_amount); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($purchase->po_status)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Status:</td>
                                        <td><?php echo ucwords($purchase->po_status); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($purchase->po_original_weight)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Original Weight:</td>
                                        <td><?php echo ucwords($purchase->po_original_weight); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($purchase->po_final_weight)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Final Weight:</td>
                                        <td><?php echo ($purchase->po_final_weight); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($purchase->po_purity)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Purity:</td>
                                        <td><?php echo number_format($purchase->po_purity); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($purchase->po_moisture_level)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Moisture Level %:</td>
                                        <td><?php echo number_format($purchase->po_moisture_level); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($purchase->po_foreign_material)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Foreign Material:</td>
                                        <td><?php echo $purchase->po_foreign_material; ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($purchase->po_damaged_kernel)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Damaged Kernel:</td>
                                        <td><?php echo ($purchase->po_damaged_kernel); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($purchase->po_hardness)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Hardness:</td>
                                        <td><?php echo ucwords($purchase->po_hardness); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($purchase->po_color)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Color:</td>
                                        <td><?php echo ucwords($purchase->po_color); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($purchase->bank)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Bank:</td>
                                        <td><?php echo ucwords($purchase->bank); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($purchase->po_date_created)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Created on:</td>
                                        <td>
                                        <?php echo date('d/m/Y',strtotime($purchase->po_date_created)); ?>
                                        </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($purchase->manager)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Created By:</td>
                                        <td><?php echo $purchase->manager->full_name; ?> (<?php echo ucwords(str_replace('_',' ',$purchase->manager->type)); ?>)</td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($purchase->po_date_updated)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Updated on:</td>
                                        <td>
                                        <?php echo date('d/m/Y',strtotime($purchase->po_date_updated)); ?>
                                        </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($purchase->edit_manager)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Updated By:</td>
                                        <td><?php echo $purchase->edit_manager->full_name; ?> (<?php echo ucwords(str_replace('_',' ',$purchase->edit_manager->type)); ?>)</td>
                                    </tr>
                                    <?php endif ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Row -->
        </div>

        <!-- ============================================================== -->
        <!-- End Container fluid -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->