<!-- Page wrapper -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item">Farmers</li>
                            <li class="breadcrumb-item active" aria-current="page">Farmer Details</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <div class="m-r-10">
                        <div class="lastmonth"></div>
                    </div>
                    <div class="">
                        <small>LAST MONTH</small>
                        <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Orders -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-sm-12 col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs no-nav-border" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#specification" role="tab" aria-controls="order-specification" aria-selected="true">Farmer Details</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content card-body" id="myTabContent">
                        <div class="tab-pane fade show active" id="specification" role="tabpanel" aria-labelledby="order-specification-tab">
                            <table class="table no-table-border">
                                <tbody class=" no-table-border">
                                    <tr class=" no-table-border">
                                        <td class="product-title">Farmer Name</td>
                                        <td><?php echo $farmer->fm_full_name; ?></a></td>
                                    </tr>
                                    <?php if(!empty($farmer->fm_gender)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Gender:</td>
                                        <td><?php echo ucwords($farmer->fm_gender); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(isset($farmer->fm_age)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Age:</td>
                                        <td><?php echo ($farmer->fm_age); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($farmer->fm_phone)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Phone:</td>
                                        <td><?php echo ucwords($farmer->fm_phone); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($farmer->fm_email)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Email:</td>
                                        <td><?php echo ($farmer->fm_email); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($farmer->fm_status)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Status:</td>
                                        <td><?php echo ucwords($farmer->fm_status); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($farmer->fm_address)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Address:</td>
                                        <td><?php echo $farmer->fm_address; ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($farmer->fm_farm_size)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Farm Size:</td>
                                        <td><?php echo ucwords($farmer->fm_farm_size); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($farmer->fm_primary_produce)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Primary Produce:</td>
                                        <td><?php echo $farmer->fm_primary_produce; ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($farmer->fm_other_produce)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Other Produce:</td>
                                        <td><?php echo $farmer->fm_other_produce; ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($farmer->fm_est_annual_produce)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Estimated Annual Produce:</td>
                                        <td><?php echo ($farmer->fm_est_annual_produce); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($farmer->fm_bank_acc_name)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Bank Account Name:</td>
                                        <td><?php echo ucwords($farmer->fm_bank_acc_name); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($farmer->fm_bank_acc_no)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Bank Account Number:</td>
                                        <td><?php echo ucwords($farmer->fm_bank_acc_no); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($farmer->bank)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Bank:</td>
                                        <td><?php echo ucwords($farmer->bank); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($farmer->fm_date_created)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Created on:</td>
                                        <td>
                                        <?php echo date('d/m/Y',strtotime($farmer->fm_date_created)); ?>
                                        </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($farmer->manager->full_name)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Created By:</td>
                                        <td><?php echo $farmer->manager->full_name; ?> (<?php echo ucwords(str_replace('_',' ',$farmer->manager->type)); ?>)</td>
                                    </tr>
                                    <?php endif ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Row -->
        </div>

        <!-- ============================================================== -->
        <!-- End Container fluid -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->