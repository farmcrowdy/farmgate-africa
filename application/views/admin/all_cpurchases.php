<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Customer Purchases (<?php if(!empty($total_cpurchases)){echo $total_cpurchases;}else{echo "0"; } ?>)</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <div class="m-r-10">
                        <div class="lastmonth"></div>
                    </div>
                    <div class=""><small>LAST MONTH</small>
                        <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Orders -->
        <!-- ============================================================== -->
        <div class="row">
            <?php echo $this->session->flashdata('cpurchase_del_feedback'); ?>
        </div>
        <div class="row">
            <!-- Column -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <!-- <div class="col-sm-12 col-md-6">
                                <div class="dataTables_length" id="zero_config_length">
                                    <label>Show <select name="zero_config_length" aria-controls="zero_config" class="form-control form-control-sm">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select> entries
                                    </label>
                                </div>
                            </div> -->
                            <div class="col-sm-12 col-md-6">
                                <form class="form-inline" method="get" action="<?php echo base_url('admin/cpurchases'); ?>">
                                    <input name="keyword_cp" type="search" class="form-control" placeholder="Find by Name" aria-label="Username" aria-describedby="basic-addon1"
                                        value="<?php if(!empty($keyword_cp)){echo $keyword_cp;} ?>">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn search-btn" style="background: none;">
                                            <i class="fas fa-search"></i>
                                        </button>
                                        <!-- <i class="fas fa-search m-auto"></i> -->
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- title -->
                        <div class="table-responsive  scrollable m-t-10" style="height:400px;">
                            <table class="table v-middle">
                                <thead>
                                    <tr>
                                        <th class="border-top-0">ID</th>
                                        <th class="border-top-0">Buyer</th>
                                        <th class="border-top-0">Pay Status</th>
                                        <th class="border-top-0">Commodity</th>
                                        <th class="border-top-0">Qty</th>
                                        <th class="border-top-0">Qty Sent</th>
                                        <th class="border-top-0">Delivered</th>
                                        <th class="border-top-0">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($cpurchases)): ?>
                                    <?php foreach($cpurchases as $cpurchase): ?>
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <a href="<?php echo base_url('admin/cpurchase/'.$cpurchase->cusp_slug); ?>" target="_blank" class=" item-order-link">
                                                    <h5 class="m-b-0 font-16 font-medium">
                                                        <?php echo strtoupper($cpurchase->cusp_slug); ?>
                                                    </h5>
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            <?php if(!empty($cpurchase->investor_name)){echo $cpurchase->investor_name;} ?>
                                        </td>
                                        <td>
                                        <?php if(!empty($cpurchase->cusp_pay_status)): ?>
                                            <?php if($cpurchase->cusp_pay_status == "pending"): ?>
                                                <button class="btn btn-primary" onclick="BuyerPaid(<?php echo $cpurchase->cusp_id; ?>)" data-cusp_id="<?php echo $cpurchase->cusp_id; ?>"><?php echo ucwords($cpurchase->cusp_pay_status); ?></button>                                            
                                            <?php elseif($cpurchase->cusp_pay_status == "paid"): ?>
                                                <button class="btn btn-success"><?php echo ucwords($cpurchase->cusp_pay_status); ?></button>
                                            <?php endif?>
                                        <?php endif ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($cpurchase->commodity_name)){echo ucwords($cpurchase->commodity_name);} ?>
                                        </td>                                                                               
                                        <td>
                                            <?php if(!empty($cpurchase->cusp_quantity)){ if($cpurchase->cusp_quantity > 1){echo number_format($cpurchase->cusp_quantity). " units";}else{ echo number_format($cpurchase->cusp_quantity). " unit";}} ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($cpurchase->cusp_quantity_delivered)){ if($cpurchase->cusp_quantity_delivered > 1){echo number_format($cpurchase->cusp_quantity_delivered). " units";}else{ echo number_format($cpurchase->cusp_quantity_delivered). " unit";}} ?>
                                        </td>
                                        <td>
                                        <?php if(!empty($cpurchase->cusp_delivery_status)): ?>
                                            <?php if($cpurchase->cusp_delivery_status == "pending"): ?>
                                                <button class="btn btn-primary" onclick="purchaseDeliveryStatus(<?php echo $cpurchase->cusp_id; ?>)" data-cusp_id_ds="<?php echo $cpurchase->cusp_id; ?>"><?php echo ucwords($cpurchase->cusp_delivery_status); ?></button>                                            
                                            <?php elseif($cpurchase->cusp_delivery_status == "delivered"): ?>
                                                <button class="btn btn-success"><?php echo ucwords($cpurchase->cusp_delivery_status); ?></button>
                                            <?php endif?>
                                        <?php endif ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url('admin/print_cpurchase/'.$cpurchase->cusp_slug); ?>" target="_blank" class="icon-pad1">
                                            <i class="fas fa-print"></i></a>
                                            <a href="<?php echo base_url('admin/edit-cpurchase/'.$cpurchase->cusp_slug); ?>" class="icon-pad1">
                                            <i class="far fa-edit"></i></a>
                                            <a href="<?php echo base_url('admin/del-cpurchase/'.$cpurchase->cusp_slug); ?>" class="icon-pad2 del-cusp">
                                            <i class="fas fa-times" style="color: #f00;"></i></a>
                                        </td>
                                    </tr>
                                    <?php endforeach ?>

                                    <?php else: ?>
                                    <p> There are no Customer Purchases to display</p>
                                    <?php endif ?>

                                </tbody>

                            </table>
                        </div>
                        <?php echo $pagination_links; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->