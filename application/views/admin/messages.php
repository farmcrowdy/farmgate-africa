<!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">Messages</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <div class="m-r-10">
                                <div class="lastmonth"></div>
                            </div>
                            <div class="">
                                <small>LAST MONTH</small>
                                <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Message -->
                <!-- ============================================================== -->
                <div class="row">

                    <div class="col-sm-12 col-lg-10 offset-lg-1">
                        <div class="card">
                            <div class="card-body">
                                <div class="col-12  px-md-5 px-3">
                                    <div class="row pt-4 mb-0 pb-0" style="background: #ffffff;">
                                        <div class="col-md-4">
                                            <ul class="pl-0 listed-chats chat-scrollbar pull-chat-right touch-list-width" style="list-style-type: none; overflow-y: scroll; height: 500px;">
                                            <?php if(!empty($orders)): ?>
								            <?php foreach($orders as $order) : ?>
                                                <li class="border-bottom  pt-4">
                                                    <a href="<?php echo base_url();?>admin/messagechat/<?php echo $order->or_order_number;?>" class="chat-profile-link load_messages">
                                                        <img src="<?php if(!empty($order->prod_image)){echo base_url();?>mainasset/fe/images/products/<?php echo $order->prod_image;}else{ echo 'http://via.placeholder.com/450x450';} ?>" class=" select-chat image-fluid" alt="<?php echo $order->prod_name; ?>">
                                                        <span class="personal-chat-list pl-1"><?php echo $order->prod_name; ?>: <small><?php echo $order->or_order_number;?></small></span>
                                                        <p class="personal-chat-list-date">
                                                        <?php echo date('h:i a - d M Y', strtotime($order->or_date_created));?>
                                                        </p>
                                                    </a>
                                                </li>
                                                <?php endforeach ?>
                                                <?php else: ?>
                                                <p> There are no orders here</p>
                                                <?php endif ?>
                                            </ul>
                                        </div>
                                        <div class="col-md-8 col-12 border-left">
                                            <div class="more-chats d-md-none">
                                                <div class="rotate-icon p-2">
                                                    <i class="fab fa-first-order fa-2x"></i>
                                                    <!-- <i class="fab fa-first-order-alt fa-2x "></i> -->
                                                    <span class="">Chats</span>
                                                    <!-- <i class="fab fa-first-order fa-2x"></i> -->
                                                </div>
                                            </div>
                                            <div id="message_area">
                                                <div class="row">
                                                    <div class="col-md-8 offset-md-2">
                                                        <div class="text-center no-chat-selected">
                                                            <p class="no-chat-title  mb-0">No conversation selected</p>
                                                            <p class=" mb-0 ">Select a conversation on the left to read and respond to messages</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Row -->
                </div>

                <!-- ============================================================== -->
                <!-- End Container fluid  -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- footer -->
