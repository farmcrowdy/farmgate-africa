<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
	<!-- ============================================================== -->
	<!-- Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<div class="page-breadcrumb">
		<div class="row">
			<div class="col-5 align-self-center">
				<h4 class="page-title">Dashboard</h4>
				<div class="d-flex align-items-center">
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb">
							<li class="breadcrumb-item">
								<a href="#">Home</a>
							</li>
							<li class="breadcrumb-item">Products</li>
							<li class="breadcrumb-item active" aria-current="page">Add Category</li>
						</ol>
					</nav>
				</div>
			</div>
			<div class="col-7 align-self-center">
				<div class="d-flex no-block justify-content-end align-items-center">
					<div class="m-r-10">
						<div class="lastmonth"></div>
					</div>
					<div class="">
						<small>LAST MONTH</small>
						<h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Add Products -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-lg-10 col-md-12 col-11">
				<div class="row">
					<div class="col-md-5">
						<form class="row acct-form mb-4" action="#">
							<div class="col-md-12 pad-dashboard">

								<div class="row mt-2">
									<div class="col-md-12">
										<label for="">Category Name
											<span class="superscript-star">*</span>
										</label>
										<input type="text" class="form-control" name="category_name" id="category_name" aria-describedby="categoryName">
									</div>
								</div>
								
								<div class="row mt-2">
									<div class="col-md-12">
										<label for="">Category Type
											<span class="superscript-star">*</span>
										</label>
										<select class="form-control" id="categoryTypeSelect" name="categoryTypeSelect">
											<?php foreach ($produce_types as $produce_type) :?>
											<option value="<?php echo $produce_type->pts_id; ?>">
												<?php echo ucwords($produce_type->pts_name); ?>
											</option>
											<?php endforeach ?>
										</select>
									</div>
								</div>
								<div class="row mt-2">
									<div class="col-md-12">
										<label for="">Parent Category
											<span class="superscript-star">*</span>
										</label>
										<select class="form-control" id="categorySelect" name="categorySelect">
											<option value="0">
												Set As Parent Category
											</option>
											<?php foreach ($categories as $category) :?>
											<option value="<?php echo $category->c_id; ?>">
												<?php echo $category->c_name; ?>
											</option>
											<?php endforeach ?>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-12  pb-5 pt-4">
										<button id="new_category" type="submit" class="btn  btn-submit"> Submit</button>
									</div>
								</div>								
							</div>
						</form>
					</div>
					<div class="col-md-6 offset-md-1">
						<div class="row">
							<div class="col-12 category-border  p-0 m-0">
								<div class="category-title">
									<p class="pl-3 py-2">Categories</p>
								</div>
								<div class="col-md-2" id="cat_loader" style="display:none;">
									<img src="<?php echo base_url('mainasset/fe/images/loader.gif'); ?>" width="30px" />
								</div>
								<div id="categories_area">

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- footer -->