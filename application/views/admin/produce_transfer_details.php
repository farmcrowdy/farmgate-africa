<!-- Page wrapper -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item">Produce Transfers</li>
                            <li class="breadcrumb-item active" aria-current="page">Produce Transfer Details</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <div class="m-r-10">
                        <div class="lastmonth"></div>
                    </div>
                    <div class="">
                        <small>LAST MONTH</small>
                        <h4 class="text-info m-b-0 font-medium">&#8358;58,256</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Orders -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-sm-12 col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs no-nav-border" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#specification" role="tab" aria-controls="order-specification" aria-selected="true">Produce Transfer Details</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content card-body" id="myTabContent">
                        <div class="tab-pane fade show active" id="specification" role="tabpanel" aria-labelledby="order-specification-tab">
                            <table class="table no-table-border">
                                <tbody class=" no-table-border">
                                    <?php if(!empty($produce_transfer->category)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Collection Center (Source) :</td>
                                        <td><?php echo ucwords($produce_transfer->collection_center); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($produce_transfer->category)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Warehouse (Destination) :</td>
                                        <td><?php echo ucwords($produce_transfer->warehouse); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($produce_transfer->category)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Produce :</td>
                                        <td><?php echo ucwords($produce_transfer->category); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(isset($produce_transfer->pt_quantity)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Quantity:</td>
                                        <td><?php echo ($produce_transfer->pt_quantity); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(isset($produce_transfer->pt_amount)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Amount:</td>
                                        <td><?php echo $produce_transfer->currency.number_format($produce_transfer->pt_amount); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($produce_transfer->pt_status)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Payment Status:</td>
                                        <td><?php echo ucwords($produce_transfer->pt_status); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($produce_transfer->pt_wh_received)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Delivered:</td>
                                        <td><?php echo ucwords($produce_transfer->pt_wh_received); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($produce_transfer->pt_date_created)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Created on:</td>
                                        <td>
                                        <?php echo date('d/m/Y',strtotime($produce_transfer->pt_date_created)); ?>
                                        </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($produce_transfer->manager)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Created By:</td>
                                        <td><?php echo $produce_transfer->manager->full_name; ?> (<?php echo ucwords(str_replace('_',' ',$produce_transfer->manager->type)); ?>)</td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!isset($produce_transfer->pt_date_updated)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Updated on:</td>
                                        <td>
                                        <?php echo date('d/m/Y',strtotime($produce_transfer->pt_date_updated)); ?>
                                        </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($produce_transfer->edit_manager)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Updated By:</td>
                                        <td><?php echo $produce_transfer->edit_manager->full_name; ?> (<?php echo ucwords(str_replace('_',' ',$produce_transfer->edit_manager->type)); ?>)</td>
                                    </tr>
                                    <?php endif ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Row -->
        </div>

        <!-- ============================================================== -->
        <!-- End Container fluid -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->