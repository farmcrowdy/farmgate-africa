<!-- Page wrapper -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item">Customer Purchases</li>
                            <li class="breadcrumb-item active" aria-current="page">Customer Purchase Details</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Orders -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-sm-12 col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs no-nav-border" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#specification" role="tab" aria-controls="order-specification" aria-selected="true">cpurchase Details</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content card-body" id="myTabContent">
                        <div class="tab-pane fade show active" id="specification" role="tabpanel" aria-labelledby="order-specification-tab">
                            <table class="table no-table-border">
                                <tbody class=" no-table-border">
                                    <tr class=" no-table-border">
                                        <td class="product-title">Investor Name</td>
                                        <td><?php echo $cpurchase->investor_name; ?></a></td>
                                    </tr>
                                    <?php if(!empty($cpurchase->commodity_name)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Commodity Name :</td>
                                        <td><?php echo ucwords($cpurchase->commodity_name); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(isset($cpurchase->cusp_quantity)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Quantity:</td>
                                        <td><?php echo number_format($cpurchase->cusp_quantity); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($cpurchase->cusp_pay_status)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Pay Status:</td>
                                        <td><?php echo ucwords($cpurchase->cusp_pay_status); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($cpurchase->cusp_delivery_status)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Delivery Status:</td>
                                        <td><?php echo ucwords($cpurchase->cusp_delivery_status); ?> </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($cpurchase->cusp_due_date)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Due Date:</td>
                                        <td>
                                        <?php echo date('d/m/Y',strtotime($cpurchase->cusp_due_date)); ?>
                                        </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($cpurchase->cusp_date_created)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Created on:</td>
                                        <td>
                                        <?php echo date('d/m/Y',strtotime($cpurchase->cusp_date_created)); ?>
                                        </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($cpurchase->manager)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Created By:</td>
                                        <td><?php echo $cpurchase->manager->full_name; ?> (<?php echo ucwords(str_replace('_',' ',$cpurchase->manager->type)); ?>)</td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($cpurchase->cusp_date_updated)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Updated on:</td>
                                        <td>
                                        <?php echo date('d/m/Y',strtotime($cpurchase->cusp_date_updated)); ?>
                                        </td>
                                    </tr>
                                    <?php endif ?>
                                    <?php if(!empty($cpurchase->edit_manager)): ?>
                                    <tr class=" no-table-border">
                                        <td class="product-title">Updated By:</td>
                                        <td><?php echo $cpurchase->edit_manager->full_name; ?> (<?php echo ucwords(str_replace('_',' ',$cpurchase->edit_manager->type)); ?>)</td>
                                    </tr>
                                    <?php endif ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Row -->
        </div>

        <!-- ============================================================== -->
        <!-- End Container fluid -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->