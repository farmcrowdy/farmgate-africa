<div class="container-fluid sub-head-main">
        <div class="row no-pad page-title-bg">
            <div class="col-12 sub-head-main">
                <ul class="page-title-list">
                    <li>
                        <a href="<?php echo base_url();?>">Home</a> /
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>toc"> Terms &amp; Conditions</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 col-sm-10 offset-sm-1 col-12 px-4 px-sm-3 terms-conditions">
                <h4>Terms &amp; Conditions</h4>
            
                <p><a href="http://www.farmgate.africa" target="_blank">http://www.farmgate.africa</a> is provided by FarmGate A Limited</p>
                <h4>Introduction</h4>
                <p>This agreement outlines the terms and conditions (the “Terms”) between you (a “Sponsor,” “you”) and FarmGate Limited (“Farmgate,” the “Company,”  “we,” or “us,”) 
                    under which you agree to invest in commodities through our platform.</p>
                <p>It is crucial that you read these Terms carefully. We strongly recommend that you seek independent advice when considering whether to invest your money through the Platform.</p>
                <p>We do not provide any advice, nor do we make any recommendations for you. The Company solely allows you to trade in commodities on the Platform. 
                    It is your responsibility to assess whether the Platform is suitable or appropriate for your needs and any decision made to use our Services is done so entirely at your own risk. 
                    Please be aware that execution of these Terms does not imply that we have confirmed that use of the Platform and our Services are suitable or appropriate for you. 
                    We advise you to consider whether trading a commodity through the Platform meets your required risk levels and objectives, and you should only commit such funds that you can financially bear the risk of losing considering your other financial commitments.</p>
                <p>By using the Platform, you confirm that you have the necessary experience and knowledge to understand the risks involved. You confirm your agreement to these Terms either by registering as a sponsor on our website or by signing a copy of these Terms. 
                    By continuing on our website, you agree to the following terms together with the Terms of Use, consent to our privacy policy, agree to transact business with us and receive communications relating to the sponsorship electronically.</p>
                    <h4>Legal and Regulatory Information</h4>
                <p>Farmgate Limited is an agro-investment company providing innovative and digital solutions for its users to invest in commodities and for farmers to gain direct access to their revenue without the hassle of the middleman.</p>
                <h4>Access and Use</h4>
                <p>The Platform and Website are, at all times, subject to our Website Terms of Use. Your username and password provides secure access to the Platform. 
                    These log-in credentials are unique to you and are the primary method of securely identifying you when delivering our Services to you. It is therefore imperative that you keep your username and password secure at all times. 
                    If you suspect that your secure access to the Platform has been compromised in any way, it is your responsibility to contact us.</p>
                <p>You confirm that you will only use our Platform for the purposes set out in these Terms. You confirm that you will not attempt to gain unauthorized access to the secure areas of the Website or Platform and furthermore you will not attempt to use code or software to manipulate or automate functions available on the Website. 
                    You understand that we may store your IP address information and may monitor your use of the Website under our cookie policy.</p>
                <p>Access to the Platform may be restricted at the discretion of the Company, particularly during periods of maintenance and updating.We have the right not to act on your instructions including where we deem that your instruction was not sufficiently clear, or we could not verify your identity to our satisfaction, or you did not make the instruction, or we believe that the instruction may be related to illegal activity.</p>
                <h4> Anti-money laundering</h4>
                <p>Where we discover that the Platform is being used to launder money or for any suspicious transactions, we must report your activities to the relevant authorities without recourse to you.</p>
                <h4>Foreign Exchange Risk</h4>
                <p>Where a Sponsor contributes in a currency other than the Naira, foreign exchange gain or loss and fluctuations in currency rates may have an impact on the profit made on such contribution by the Sponsor.</p>
                <p>All profits on contribution shall be converted and returned in Naira based on the Central Bank of Nigeria (CBN) official exchange rate. The Sponsor, therefore, enters into any transactions in currencies other than the Naira at their sole risk and shall bear all responsibilities for any gains or loss as a result. Unexpected changes in currency exchange market conditions may have an impact on the extent of profit the Investor would be exposed to such as when there is an upward or downward movement in the applicable rates.</p>
                <p>The Investor understands and can assume the risk of loss associated and agrees to be liable for any resulting deficit this might have on contributions made or profits earned.</p>
                <h4>Term and Termination</h4>
                <p>Except as otherwise terminated under the provisions below, these Terms shall commence on the date you register to sponsor a farm with the Company and shall remain valid and binding for as long as you have outstanding sponsored farming cycles. Any amended Terms may supersede these Terms. You may terminate these Terms provided that you have no outstanding sponsored farming cycle. Notice of termination must be submitted in writing to info@farmgate.com</p>
                <p>The Company may terminate these Terms at any time without notice.</p>
                <p>Privacy</p>
                <p>You confirm that you have read and understood the Company’s Privacy Policy which provides information on how we use and store the personal information that you provide to us and you further acknowledge that the Company may amend and update this policy from time to time.</p>
                <h4>Dispute Resolution</h4>
                <p>This Agreement shall be governed by the Laws of the Federal Republic of Nigeria. 
                    Any dispute arising out of this Agreement, which cannot be settled, by mutual agreement/negotiation within 30 days shall be referred to arbitration by a single arbitrator at the Lagos Court of Arbitration (“LCA”) and governed by the Arbitration and Conciliation Act, Cap A10, Laws of the Federal Republic of Nigeria.
                    The arbitrator shall be appointed by the Parties, where Parties are unable to agree on the choice of an arbitrator, the decision of arbitration shall be referred to the LCA. The findings of the arbitrator and subsequent award shall be binding on the Parties and may be enforced through a Nigerian court of law. 
                    Each Party shall bear its cost in connection with the Arbitration.</p>
            </div>
        </div>
    </div>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="js/custom.js"></script>
    <script>
        AOS.init();
    </script>