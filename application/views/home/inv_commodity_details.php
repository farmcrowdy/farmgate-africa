<div class="container-fluid sub-head-main">
        <div class="row page-title-bg no-pad">
            <div class="col-12 sub-head-main">
                <ul class="page-title-list">
                    <li>
                        <a href="#">Home</a> /
                    </li>
                    <li>
                        <a href="#"> Investment </a> /
                    </li>
                    <li>
                        <a class="active" href="#"><?php echo $commodity->invc_name; ?></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid" id="feedback">
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="individual-head col-md-10 offset-md-1 col-12">
                <div class="row px-1 px-md-0">
                    <div class="col-lg-2 col-md-3 col-sm-4 col-7 img-div">
                        <img src="<?php echo base_url(); ?>mainasset/fe/images/icommodities/<?php echo $commodity->invc_image; ?>" class="img-fluid w-100" alt="Wheat">
                    </div>
                    <div class="number-input col-lg-5 col-md-9 col-sm-8 pt-4 pt-md-0">
                        <h4 class="title"><?php echo $commodity->invc_name; ?></h4>
                        <!-- <p class="duration"><span>25% </span>returns in <span>8</span> <span>months</span></p> -->
                        <p class="no-left"><span id="noLeft"><?php if($commodity->invc_status !== "sold-out"){ echo $commodity->invc_quantity_left;}else{ echo "0";} ?></span> units left</p>
                        <p class="product-price"> <span> <?php echo number_format($commodity->invc_quantity_left); ?> </span> units available</p>
                        <?php if($commodity->invc_status == "now-selling"): ?>
                        <div class="button">
                            <p>Quantity</p>
                            <button class="minus getval" mid="7" p="800" onclick="this.parentNode.querySelector('input[type=number]').stepDown()">&#x2212;
                            </button>
                            <input id="i_noLeft" class="quantity ctnumber7" min="1" name="quantity" value="1" type="number" max="<?php echo $commodity->invc_quantity_left; ?>">
                            <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus getval"
                                mid="7" p="800"> &#x2b; </button>
                            <span class="trade-span"></span>
                            <?php if(empty($user)): ?>
                            <button class="trade" onclick="check();">Trade</button>
                            <?php else: ?>
                            <button class="trade" onclick="confirmTrade();">Trade</button>
                            <?php endif ?>
                            <span class="contact-manager-span"></span>
                            <a class="contact-manager" href="<?php echo base_url('contact'); ?>">Contact Manager</a>
                        </div>
                        <?php endif ?>
                    </div>
                    <div class="offset-lg-1 col-lg-4 col-md-10 offset-md-1 col-12 border">
                        <!-- <canvas id="myChart" style="height:100%; width:100%;"></canvas> -->
                        <iframe width="450" height="370" seamless frameborder="0" scrolling="yes" 
                        src="<?php echo $commodity->invc_google_sheet_link; ?>"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row simulator-bg">
            <div class="col-lg-10 offset-lg-1">
                <ul class="nav nav-pills mb-3 simulate px-md-5 pl-sm-0 px-lg-0 " id="pills-tab" role="tablist">
                    <li class="nav-item nav-item-tab ml-sm-4  ml-md-0 pl-md-0">
                        <a class="nav-link first active" id="product-detail" data-toggle="pill" href="#details" role="tab"
                            aria-controls="details" aria-selected="true"><span class="d-block d-sm-inline">Commodity</span>
                            Details</a>
                    </li>
                    <li class="nav-item nav-item-tab ">
                        <a class="nav-link" id="product-insurance" data-toggle="pill" href="#insurance" role="tab"
                            aria-controls="insurance" aria-selected="false"><span class="d-block d-sm-inline">Insurance</span>
                            Cover</a>
                    </li>
                </ul>
                <div class="tab-content pt-4 pb-5  px-md-5 px-lg-0" id="pills-tabContent">
                    <div class="tab-pane product-tab fade show active" id="details" role="tabpanel" aria-labelledby="product-detail">
                        <!-- <div class="tab-pane product-tab fade show active" id="details" role="tabpanel" aria-labelledby="product-detail"  data-aos="fade-up" data-aos-duration="3000"> -->
                        <?php echo ($commodity->invc_commodity_details); ?>
                    </div>
                    <div class="tab-pane fade" id="insurance" role="tabpanel" aria-labelledby="product-insurance"
                        data-aos="fade-up" data-aos-duration="3000"><?php echo ($commodity->invc_insurance_cover); ?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid products-container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1 col-md-8 offset-md-2 col-sm-10 offset-sm-1 col-12">
                <div class="row text-center text-lg-left">
                    <div class="col-12 py-4">
                        <h4 class="mobile">Related Commodities</h4>
                    </div>
                </div>
                <div class="row">
                <?php if(!empty($icommodities)): ?>
                    <?php foreach($icommodities as $icommodity): ?>
                    <div class="col-lg-3 col-sm-4 col-6">
                        <div class="row products">
                            <div class="col-12 product-border px-0">
                                <a href="<?php echo base_url('investment-commodity/').$icommodity->invc_slug; ?>">
                                    <div class="img-div">
                                        <img src="<?php echo base_url();?>mainasset/fe/images/icommodities/<?php echo ($icommodity->invc_image); ?>" class="img-fluid product-img" alt="Farm produce">
                                        <span class="<?php echo ($icommodity->invc_status); ?>"><?php echo ucwords(str_replace('-', '',$icommodity->invc_status)); ?></span>
                                    </div>
                                    <div>
                                        <p class="product-name"><?php echo ($icommodity->invc_name); ?></p>

                                        <p class="product-price"><span>₦ <?php echo number_format($icommodity->invc_price_per_unit); ?></span> <?php echo ($icommodity->unit); ?></p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
                    <?php  endif ?>
                </div>
                <!-- <div class="row">
                    <div class="col-12 text-center more-products px-0">
                        <a href="#">View all available commodities</a>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row subscribe-bg" data-aos="fade-up" data-aos-anchor-placement="center-bottom">
            <div class="col-12 offset-md-0 col-md-12 text-center">
                <h4 class="mobile">Get notified when commodities are available to trade </h4>
            </div>
            <form class="form-inline  col-md-6 offset-md-3 col-10 offset-1 col-sm-8 offset-sm-2 col-lg-4 offset-lg-4">
                <div class="input-group w-100 pad-sub mb-3">
                    <input type="email" class="form-control email-subscribe" placeholder="Enter your email" aria-label="email"
                        aria-describedby="button-addon2">
                    <div class="input-group-append">
                        <button class="btn " type="submit" id="button-addon2">Sign up</button>
                    </div>
                </div>
            </form>

        </div>
        <div class="row security-bg security-bg1">
            <div class="col-12" data-aos="fade-up" data-aos-duration="3000">
                <div class="row">
                    <div class="col-lg-6 offset-lg-3 col-12 px-4 px-sm-0 col-md-8 offset-md-2 col-sm-10 offset-sm-1 text-center">
                        <h4>Safe, insured and secured.</h4>
                        <p>Farmgate Trader was built with security of funds and data in mind. We work with a PCIDSS
                            compliant payment processor for the security of data. Our assets are held with reputable
                            SEC registered firms, who ensure investors’ funds are covered by invested asset.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-8 offset-md-2 col-12 offset-lg-4 pt-4">
                        <div class="row">
                            <div class="col-md-4 col-6 text-center">
                                <div class=" d-flex justify-content-center">
                                    <img src="<?php echo base_url(); ?>mainasset/fe/images/icons/fund.svg" alt="funds icon">
                                </div>

                                <span>Funds Secured</span>
                            </div>
                            <div class="col-md-4 col-6 text-center">
                                <div class=" d-flex justify-content-center">
                                    <img src="<?php echo base_url(); ?>mainasset/fe/images/icons/data.svg" alt="data icon">
                                </div>
                                <span>Data Protected</span>
                            </div>
                            <div class="col-md-4 offset-3 offset-md-0 col-6 pt-4 pt-md-0 text-center">
                                <div class=" d-flex justify-content-center">
                                    <img src="<?php echo base_url(); ?>mainasset/fe/images/icons/investment.svg" alt="investment icon">
                                </div>
                                <span>Investment Insured</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    