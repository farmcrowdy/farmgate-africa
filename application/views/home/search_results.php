<p>Showing <span><?php if (!empty($total_commodities)) {
    echo number_format($total_commodities);}
    else {echo "0";}?>
</span>
<span> commodit<?php if(!empty($commodities)){
    if (count($commodities) > 1){
        echo "ies";
    } else {
        echo "y";
    }
}else{echo "y";}
?></span> </p>
    <div class="row pt-3 px-2 px-md-0 pt-md-0">
        <?php if(!empty($commodities)) : ?>
        <?php foreach($commodities as $Commodity): ?>
        <div class="col-lg-4 col-md-6 col-sm-4 col-6">
            <div class="row products">
                <div class="col-12 product-border px-0">
                    <a href="<?php echo base_url('commodity/').$Commodity->comm_slug; ?>">
                        <div class="img-div">
                            <img src="<?php echo base_url();?>/mainasset/fe/images/commodities/<?php echo $Commodity->comm_image; ?>" class="img-fluid product-img w-100"
                                alt="Farm produce">
                            <span class="<?php echo $Commodity->comm_status; ?>">
                                <?php echo ucwords(str_replace('-',' ',($Commodity->comm_status))); ?></span>

                        </div>
                        <div>
                            <p class="product-name">
                                <?php echo ucwords($Commodity->comm_name); ?>
                            </p>
                            <?php if($Commodity->comm_commodity_type == "purchase"): ?>
                            <p class="product-price"><span>
                                    <?php echo number_format($Commodity->comm_quantity_left_to_buy); ?></span> units available
                            </p>
                            <!-- <p class="product-price"><span>₦
                                    <?php // echo number_format($Commodity->comm_price_per_unit); ?></span> per
                                <?php // echo ucwords($Commodity->unit); ?>
                            </p> -->
                            <?php else: ?>
                            <p class="product-price"><span>
                                    <?php echo number_format($Commodity->comm_quantity_left); ?></span> units available
                            </p>
                            <?php endif ?>
                        </div>
                    </a>
                </div>
            </div>
        </div> 
        <?php endforeach ?>
        <?php else: ?>
        <p>No Commodity Found</p>
        <?php endif ?>
    </div>
    <?php if(!empty($pagination)) echo $pagination; ?>
