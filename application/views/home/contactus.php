    <div class="container-fluid sub-head-main">
        <div class="row page-title-bg no-pad">
            <div class="col-12 sub-head-main">
                <ul class="page-title-list">
                    <li>
                        <a href="<?php echo base_url();?>">Home</a> /
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>contact"> Contact Us</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 offset-lg-1 col-12 col-sm-10 offset-sm-1 col-md-12 offset-md-0 contact-us">
                <div class="row">
                    <div class="col-sm-8 col-11 col-md-5 pl-md-5 pl-4 pl-sm-3 pl-lg-3">
                        <h4>Contact us</h4>
                        <p>You can always reach out to us on the following channels.</p>
                        <div class="contact-sub">
                            <p class="contact-sub-title">Phone lines - available 9am - 5pm (WAT) </p>
                            <a href="tel:+2348093774283" class="tel">+234 909 0904 030</a>
                        </div>
                        <div class="contact-sub">
                            <p class="contact-sub-title">Email address</p>
                            <a href="mailto:info@farmgate.africa" class="tel">info@farmgate.africa</a>
                        </div>
                        <div class="contact-sub contact-sub1">
                        <a target="_blank" href="https://goo.gl/maps/bxPfjhhSkf32" class="contact-sub-title link">Office address</a>
                            <p>Civic Innovation Lab</p>
                            <p>50 Adetokunbo Ademola Crescent,</p>
                            <p>Beside H-Medix Pharmacy,</p>
                            <p>Wuse II, Abuja, Nigeria.</p>
                        </div>
                        <div class="contact-sub">
                            <p class="contact-sub-title">We’re social</p>
                            <ul class="socials">
                                <li>
                                    <a href="https://web.facebook.com/farmgateafrica/" target="_blank">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                            style="isolation:isolate" viewBox="0 0 20.79 20.79" width="20.79" height="20.79">
                                            <defs>
                                                <clipPath id="_clipPath_IGLxjPJ3POZKyS9NbjPZE2IV9ZDq0aKs">
                                                    <rect width="20.79" height="20.79" />
                                                </clipPath>
                                            </defs>
                                            <g clip-path="url(#_clipPath_IGLxjPJ3POZKyS9NbjPZE2IV9ZDq0aKs)">
                                                <path d=" M 18.388 0.401 L 2.388 0.401 C 1.285 0.404 0.391 1.298 0.388 2.401 L 0.388 18.401 C 0.391 19.504 1.285 20.398 2.388 20.401 L 18.388 20.401 C 19.491 20.398 20.385 19.504 20.388 18.401 L 20.388 2.401 C 20.385 1.298 19.491 0.404 18.388 0.401 L 18.388 0.401 Z  M 16.786 5.799 L 15.387 5.799 C 14.489 5.799 14.387 6.1 14.387 6.799 L 14.387 8.101 L 16.489 8.101 L 16.387 10.404 L 14.489 10.404 L 14.489 17.404 L 11.286 17.404 L 11.286 10.404 L 10.087 10.404 L 10.087 8.004 L 11.288 8.004 L 11.288 6.503 C 11.288 4.503 12.088 3.401 14.389 3.401 L 16.787 3.401 L 16.786 5.799 Z "
                                                    fill="rgb(221,221,221)" />
                                            </g>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.twitter.com/farmgateafrica/" target="_blank">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                            style="isolation:isolate" viewBox="0 0 20.79 20.79" width="20.79" height="20.79">
                                            <defs>
                                                <clipPath id="_clipPath_bKB0DNkoLhEOrCSJPQMI5cecxdIoI45b">
                                                    <rect width="20.79" height="20.79" />
                                                </clipPath>
                                            </defs>
                                            <g clip-path="url(#_clipPath_bKB0DNkoLhEOrCSJPQMI5cecxdIoI45b)">
                                                <path d=" M 163.806 -96.999 C 160.602 -96.993 158.006 -94.398 158 -91.194 L 158 -82.804 C 158.006 -79.6 160.602 -77.005 163.806 -76.999 L 172.195 -76.999 C 175.398 -77.005 177.994 -79.601 178 -82.804 L 178 -91.195 C 177.994 -94.398 175.398 -96.994 172.195 -97 L 163.806 -96.999 Z  M 163.806 -95.46 L 172.195 -95.46 C 173.328 -95.463 174.415 -95.015 175.216 -94.214 C 176.017 -93.413 176.465 -92.326 176.462 -91.193 L 176.462 -82.803 C 176.465 -81.67 176.017 -80.583 175.216 -79.782 C 174.415 -78.982 173.328 -78.533 172.195 -78.536 L 163.806 -78.536 C 162.673 -78.533 161.586 -78.982 160.785 -79.782 C 159.984 -80.583 159.536 -81.67 159.539 -82.803 L 159.539 -91.195 C 159.536 -92.328 159.984 -93.415 160.785 -94.216 C 161.586 -95.017 162.673 -95.465 163.806 -95.462 L 163.806 -95.46 Z  M 173.769 -93.92 C 173.302 -93.92 172.881 -93.639 172.702 -93.208 C 172.524 -92.776 172.622 -92.28 172.953 -91.95 C 173.283 -91.62 173.779 -91.521 174.211 -91.699 C 174.642 -91.878 174.923 -92.299 174.923 -92.766 C 174.923 -93.404 174.407 -93.92 173.769 -93.92 L 173.769 -93.92 Z  M 167.997 -92.381 C 165.818 -92.381 163.853 -91.069 163.019 -89.055 C 162.185 -87.042 162.646 -84.725 164.187 -83.184 C 165.728 -81.643 168.045 -81.183 170.058 -82.016 C 172.071 -82.85 173.384 -84.815 173.384 -86.994 C 173.384 -89.969 170.972 -92.381 167.997 -92.381 L 167.997 -92.381 Z  M 167.997 -90.842 C 169.826 -90.837 171.397 -89.542 171.75 -87.747 C 172.103 -85.953 171.14 -84.158 169.45 -83.461 C 167.759 -82.763 165.811 -83.356 164.796 -84.877 C 163.781 -86.398 163.981 -88.425 165.274 -89.718 C 165.997 -90.439 166.976 -90.843 167.997 -90.842 Z "
                                                    fill="rgb(221,221,221)" />
                                                <path d=" M 20.79 4.418 C 20.022 4.769 19.202 4.994 18.363 5.085 C 19.234 4.572 19.905 3.777 20.267 2.833 C 19.44 3.323 18.533 3.662 17.587 3.834 C 16.731 2.99 15.581 2.511 14.379 2.499 C 12.074 2.465 10.175 4.301 10.131 6.606 C 10.113 6.916 10.142 7.226 10.218 7.527 C 5.751 7.527 2.689 5.012 0.611 2.498 C 0.191 3.113 -0.021 3.847 0.005 4.592 C 0.03 5.983 0.745 7.27 1.913 8.026 C 1.254 7.961 0.611 7.793 0.005 7.527 L 0.005 7.606 C 0.012 9.586 1.434 11.278 3.384 11.626 C 3.015 11.728 2.635 11.785 2.253 11.795 C 1.991 11.809 1.729 11.782 1.476 11.716 C 1.998 13.393 4.416 15.319 6.32 15.319 C 4.775 16.662 2.788 17.386 0.741 17.35 L 0 17.35 C 1.985 18.44 4.235 18.955 6.496 18.835 C 9.669 18.92 12.743 17.725 15.025 15.519 C 17.307 13.312 18.604 10.281 18.626 7.107 L 18.626 6.608 C 19.469 6.012 20.202 5.272 20.789 4.423"
                                                    fill="rgb(221,221,221)" />
                                                <path d=" M 156.209 -50.784 C 155.962 -51.846 155.11 -52.66 154.039 -52.859 C 151.489 -53.283 148.907 -53.477 146.322 -53.437 C 143.722 -53.476 141.124 -53.283 138.559 -52.859 C 137.479 -52.676 136.619 -51.855 136.388 -50.784 C 136.124 -49.109 135.995 -47.416 136.002 -45.721 C 135.982 -44.022 136.127 -42.326 136.435 -40.656 C 136.683 -39.596 137.535 -38.782 138.605 -38.582 C 141.172 -38.162 143.77 -37.969 146.371 -38.004 C 148.971 -37.968 151.57 -38.162 154.137 -38.582 C 155.216 -38.766 156.075 -39.586 156.307 -40.656 C 156.584 -42.331 156.745 -44.023 156.79 -45.721 C 156.724 -47.421 156.53 -49.113 156.209 -50.784 L 156.209 -50.784 Z  M 143.719 -42.345 L 143.719 -49.097 L 149.602 -45.721 L 143.719 -42.345 Z "
                                                    fill="rgb(221,221,221)" />
                                            </g>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.youtube.com/channel/UC_ujWVvgm8-jxeD17SQlMkQ?disable_polymer=true" target="_blank">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                            style="isolation:isolate" viewBox="0 0 20.79 20.79" width="20.79" height="20.79">
                                            <defs>
                                                <clipPath id="_clipPath_C2LRpKpfymy8aHeS6YVlEGPVY2RGV4yW">
                                                    <rect width="20.79" height="20.79" />
                                                </clipPath>
                                            </defs>
                                            <g clip-path="url(#_clipPath_C2LRpKpfymy8aHeS6YVlEGPVY2RGV4yW)">
                                                <path d=" M 19.945 5.341 C 19.698 4.279 18.846 3.465 17.775 3.267 C 15.225 2.842 12.643 2.648 10.058 2.688 C 7.458 2.649 4.86 2.842 2.294 3.267 C 1.215 3.449 0.355 4.271 0.124 5.341 C -0.14 7.016 -0.269 8.709 -0.262 10.405 C -0.282 12.103 -0.137 13.799 0.171 15.469 C 0.419 16.53 1.271 17.343 2.341 17.543 C 4.908 17.963 7.506 18.157 10.107 18.121 C 12.707 18.157 15.306 17.963 17.873 17.543 C 18.952 17.36 19.811 16.539 20.043 15.469 C 20.32 13.794 20.481 12.102 20.526 10.405 C 20.46 8.704 20.266 7.012 19.945 5.341 L 19.945 5.341 Z  M 7.455 13.78 L 7.455 7.028 L 13.338 10.404 L 7.455 13.78 Z "
                                                    fill="rgb(221,221,221)" />
                                            </g>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/farmgateafrica/" target="_blank">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                            style="isolation:isolate" viewBox="0 0 20.79 20.79" width="20.79" height="20.79">
                                            <defs>
                                                <clipPath id="_clipPath_Kznzii7hY9QskBBRg9mc2cWgECFSSvgJ">
                                                    <rect width="20.79" height="20.79" />
                                                </clipPath>
                                            </defs>
                                            <g clip-path="url(#_clipPath_Kznzii7hY9QskBBRg9mc2cWgECFSSvgJ)">
                                                <path d=" M 6.241 0.395 C 3.037 0.401 0.441 2.997 0.435 6.2 L 0.435 14.59 C 0.441 17.794 3.037 20.39 6.241 20.395 L 14.63 20.395 C 17.833 20.389 20.428 17.794 20.435 14.59 L 20.435 6.2 C 20.428 2.996 17.833 0.401 14.63 0.395 L 6.241 0.395 Z  M 6.241 1.935 L 14.63 1.935 C 15.762 1.931 16.849 2.38 17.65 3.181 C 18.451 3.982 18.9 5.069 18.896 6.201 L 18.896 14.591 C 18.9 15.724 18.451 16.811 17.65 17.612 C 16.849 18.413 15.762 18.861 14.63 18.858 L 6.241 18.858 C 5.108 18.861 4.021 18.413 3.22 17.612 C 2.419 16.811 1.971 15.724 1.974 14.591 L 1.974 6.2 C 1.971 5.067 2.419 3.98 3.22 3.179 C 4.021 2.378 5.108 1.929 6.241 1.933 L 6.241 1.935 Z  M 16.203 3.474 C 15.737 3.474 15.316 3.755 15.137 4.187 C 14.958 4.618 15.057 5.115 15.387 5.445 C 15.717 5.775 16.214 5.874 16.645 5.695 C 17.077 5.516 17.358 5.095 17.358 4.628 C 17.358 3.991 16.841 3.474 16.203 3.474 L 16.203 3.474 Z  M 10.431 5.013 C 8.252 5.013 6.288 6.326 5.454 8.339 C 4.62 10.352 5.081 12.669 6.622 14.21 C 8.162 15.751 10.48 16.212 12.493 15.378 C 14.506 14.544 15.819 12.58 15.819 10.401 C 15.819 7.425 13.407 5.013 10.431 5.013 L 10.431 5.013 Z  M 10.431 6.553 C 12.26 6.557 13.831 7.853 14.185 9.647 C 14.538 11.442 13.575 13.236 11.884 13.934 C 10.194 14.632 8.246 14.039 7.231 12.517 C 6.215 10.996 6.415 8.97 7.709 7.676 C 8.431 6.955 9.411 6.551 10.431 6.553 Z "
                                                    fill="rgb(221,221,221)" />
                                            </g>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.linkedin.com/in/farmgate-africa-7958b5174/" target="_blank">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                            style="isolation:isolate" viewBox="0 0 20.79 20.79" width="20.79" height="20.79">
                                            <defs>
                                                <clipPath id="_clipPath_MT2VO9TobMPrj9q0WFyhXrmSBwUjyqIv">
                                                    <rect width="20.79" height="20.79" />
                                                </clipPath>
                                            </defs>
                                            <g clip-path="url(#_clipPath_MT2VO9TobMPrj9q0WFyhXrmSBwUjyqIv)">
                                                <path d=" M 16.37 0.395 L 4.371 0.395 C 2.163 0.396 0.373 2.186 0.371 4.395 L 0.371 16.395 C 0.373 18.604 2.163 20.394 4.371 20.395 L 16.37 20.395 C 18.578 20.393 20.368 18.604 20.37 16.395 L 20.37 4.395 C 20.368 2.186 18.578 0.397 16.37 0.395 L 16.37 0.395 Z  M 6.77 14.352 L 6.77 17.193 L 3.571 17.193 L 3.571 7.997 L 6.77 7.997 L 6.77 14.352 Z  M 5.171 6.997 C 4.753 7.02 4.345 6.87 4.043 6.582 C 3.74 6.294 3.569 5.894 3.571 5.477 C 3.571 5.063 3.744 4.669 4.048 4.388 C 4.351 4.107 4.758 3.966 5.171 3.997 C 5.581 3.973 5.984 4.116 6.286 4.396 C 6.588 4.675 6.763 5.065 6.77 5.477 C 6.778 5.896 6.61 6.3 6.305 6.588 C 6.001 6.877 5.589 7.024 5.171 6.994 L 5.171 6.997 Z  M 17.17 17.194 L 13.97 17.194 L 13.97 11.993 C 14.03 11.589 13.909 11.178 13.64 10.87 C 13.371 10.562 12.979 10.388 12.57 10.394 C 12.168 10.409 11.791 10.59 11.526 10.892 C 11.261 11.195 11.133 11.593 11.171 11.993 L 11.171 17.194 L 7.971 17.194 L 7.971 7.997 L 11.171 7.997 L 11.171 9.238 C 11.721 8.395 12.687 7.919 13.691 7.997 C 15.615 7.997 17.171 9.317 17.171 11.997 L 17.17 17.194 Z "
                                                    fill="rgb(221,221,221)" />
                                            </g>
                                        </svg>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 offset-lg-1 col-md-7 pr-lg-3 pr-md-5 px-0 px-sm-3">
                        <div id="contact_feedback"></div>
                        <form id="contact_form" method="POST" action="https://formspree.io/info@farmgate.africa" class="contact-form p-lg-5 p-4">
                            <div class="row no-pad">
                                <div class="col-6 form-group">
                                    <label for="firstName">First name</label>
                                    <input type="text" class="form-control" id="contact_firstName" name="firstName">
                                    <small> <span style="color: red; display: none;" id="contact_firstName_error"></span></small>
                                </div>

                                <div class="col-6">
                                    <label for="lastName">Last name</label>
                                    <input type="text" class="form-control" id="contact_lastName" name="lastName">
                                    <small> <span style="color: red; display: none;" id="contact_lastName_error"></span></small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email">Business email</label>
                                <input type="email" class="form-control" id="contact_email" name="email" aria-describedby="emailHelp">
                                    <small> <span style="color: red; display: none;" id="contact_email_error"></span></small>
                            </div>
                            <div class="form-group">
                                <label for="tel">Phone number</label>
                                <input type="tel" class="form-control" id="contact_phone" name="phone" aria-describedby="telHelp">
                                <small> <span style="color: red; display: none;" id="contact_phone_error"></span></small>
                            </div>
                            <div class="form-group">
                                <label for="company">Company</label>
                                <input type="text" class="form-control" id="contact_company" name="company" aria-describedby="textHelp">
                                <small> <span style="color: red; display: none;" id="contact_company_error"></span></small>
                            </div>
                            <div class="form-group">
                                <label for="">Message</label>
                                <textarea class="form-control" name="message" id="contact_message" rows="5" aria-label="With textarea" placeholder="What would you like to ask?"></textarea>
                                <small> <span style="color: red; display: none;" id="contact_message_error"></span></small>
                            </div>
                            <button type="submit" class="send-message">Send Message</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>