<div class="container-fluid jumbotron-bg jumbotron-bg-hidden">
        <div class="row no-pad re-order-divs">
            <div class="col-md-6 title-left order-1">
                <div class="row ">
                    <div class="offset-lg-2 col-lg-10">
                        <div class="title-container-left ">
                            <p class="head-title">COMMODITY TRADING</p>
                            <h4>Trade in agricultural commodities in a much smarter way.</h4>
                            <p class="marg-b">Invest in large scale agricultural commodity trading and earn returns on your money.</p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-6 title-right text-md-right order-3 order-md-2">
                <div class="row ">
                    <div class="col-lg-10">
                        <div class="title-container-right ">
                            <p class="head-title">COMMODITY PURCHASE</p>
                            <h4>Instant access to high grade agricultural commodities</h4>
                            <p class="align-commodity  marg-b">Purchase high quality farm produce conveniently and at fair prices directly from smallholder farmers.</p>
                        </div>
                    </div>
                </div>
            </div>
         <div class="col-md-6 order-2 order-md-3">
             <div class="row ">
                <div class="col-sm-8 offset-sm-2 col-md-12 offset-md-0 col-lg-10 offset-lg-1 how-it-works-sub-left">
                    <div>
                        <p class=""><span class="number-left">1</span>
                            <span class="span-content">Select from our range of agricultural
                                commodities to trade.</span>
                        </p>
                        <div class="line-under"></div>
                    </div>
                    <div>
                        <p class=""><span class="number-left">2</span>
                            <span class="span-content">Specify your desired trade volume and your assigned account manager will be in touch to close the deal.</span>
                        </p>
                        <div class="line-under"></div>
                    </div>
                    <div>
                        <p class=""><span class="number-left">3</span>
                            <span class="span-content">Get back your capital and yield at the end of the investment period.</span>
                        </p>
                    </div>
                </div>
             </div>
         </div>
           <div class="col-md-6 order-4 how-it-works-sub-right-bg">
               <div class="row">
                <div class="col-sm-8 offset-sm-2 col-md-12 offset-md-0 col-lg-10 offset-lg-1  how-it-works-sub-right">
                    <div>
                        <p class=""><span class="number-right">1</span>
                            <span class="span-content">Select from our range of agricultural commodities available for purchase.</span>
                        </p>
                        <div class="line-under"></div>
                    </div>
                    <div>
                        <p class=""><span class="number-right">2</span>
                            <span class="span-content">Specify your desired purchase volume and your assigned account manager will contact you.</span>
                        </p>
                        <div class="line-under"></div>
                    </div>
                    <div>
                        <p class=""><span class="number-right">3</span>
                            <span class="span-content">Your order is processed and shipped to your specified delivery address.</span>
                        </p>
                    </div>
                </div>
               </div>
           </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- <div class="container-fluid jumbotron-bg">
        <div class="row no-pad">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12 left-bg">
                        <div class="row ">
                            <div class="offset-lg-2 col-lg-10">
                                <div class="title-container-left ">
                                    <p class="head-title">COMMODITY TRADING</p>
                                    <h4>Earn up to 32% returns as a commodity trader</h4>
                                    <p class="marg-b">Invest in large scale agricultural commodity trading and earn
                                        returns on
                                        your money.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-8 offset-2 how-it-works-sub-left">
                        <div>
                            <p class=""><span class="number-left">1</span>
                                <span class="span-content">Select from our range of agricultural 
                                    commodities (e.g. brown & white cow pea).</span>
                            </p>
                            <div class="line-under"></div>
                        </div>
                        <div>
                            <p class=""><span class="number-left">2</span>
                                <span class="span-content">Choose from our range of agricultural 
                                    commodities (e.g. brown & white cow pea).</span>
                            </p>
                            <div class="line-under"></div>
                        </div>
                        <div>
                            <p class=""><span class="number-left">3</span>
                                <span class="span-content">Choose from our range of agricultural 
                                    commodities (e.g. brown & white cow pea).</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="row">
                    <div class="col-md-12 text-md-right right-bg">
                        <div class="row ">
                            <div class="col-lg-10">
                                <div class="title-container-right ">
                                    <p class="head-title">COMMODITY PURCHASE</p>
                                    <h4>Earn up to 32% returns as a commodity trader</h4>
                                    <p class="align-commodity  marg-b">Invest in large scale agricultural commodity
                                        trading and
                                        earn
                                        returns on your money.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-8 offset-2 how-it-works-sub-right">
                        <div>
                            <p class=""><span class="number-right">1</span>
                                <span class="span-content">Select from our range of agricultural 
                                    commodities (e.g. brown & white cow pea).</span>
                            </p>
                            <div class="line-under"></div>
                        </div>
                        <div>
                            <p class=""><span class="number-right">2</span>
                                <span class="span-content">Choose from our range of agricultural 
                                    commodities (e.g. brown & white cow pea).</span>
                            </p>
                            <div class="line-under"></div>
                        </div>
                        <div>
                            <p class=""><span class="number-right">3</span>
                                <span class="span-content">Choose from our range of agricultural 
                                    commodities (e.g. brown & white cow pea).</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->