
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon.ico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicon.ico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon.ico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon.ico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon.ico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon.ico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon.ico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon.ico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon.ico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="images/favicon.ico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon.ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon.ico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.ico/favicon-16x16.png">
    <link rel="manifest" href="images/favicon.ico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="images/favicon.ico/ms-icon-144x144.png">
    <meta name="theme-color" content="#08BD51">
    <link rel="stylesheet" href="<?php echo base_url('mainasset/fe/'); ?>css/fonts.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('mainasset/fe/'); ?>css/index.css">
    <link rel="stylesheet" href="<?php echo base_url('mainasset/fe/'); ?>css/main.css">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <script src="https://unpkg.com/scrollreveal"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js" integrity="sha384-kW+oWsYx3YpxvjtZjFXqazFpA7UP/MbiY4jvs+RWZo2+N94PFZ36T6TFkc9O3qoB"
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
        crossorigin="anonymous">
          <!-- lity js -->
    <link rel="stylesheet" href="<?php echo base_url('mainasset/fe/'); ?>css/lity.css">
    <script src="<?php echo base_url('mainasset/fe/'); ?>js/lity.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url('mainasset/fe/'); ?>css/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- <script src="partials/js_partial.js"></script> -->
    <body>
    <section class="container-fluid">
    <div class="row">
        <div class="col-12 team-profile-container">
            <div class="row">
                <div class="col-12 col-md-4 pl-md-0 text-center">
                    <img src="<?php echo base_url('mainasset/fe/images/kenneth-profile.jpg'); ?>" class="img-fluid" alt="team image">
                    <h4 class="team-name">Kenneth Obiajulu</h4>
                    <p class="person-title">Managing Director</p>
                </div>
                <div class="col-md-8 pl-md-4 scrollable">
                    <p>Kenneth is a First-Class Graduate of Economics and Business Studies (Marketing Specialization) from Redeemer’s University, Postgraduate Diploma Holder from the Institute of Marketing of Nigeria, an Associate Member of the same Institute and a PhD scholarship recipient from the Delta State Government. 
                        Kenneth has 8 years’ cognate experience on national and international levels with strong focus on agribusiness development, value chain assessment, project management, innovation, market intelligence and strategic operations management.</p>
                    <p>Kenneth has worked on several projects spanning across diverse sectors. Before cofounding Farmgate Africa, Kenneth was Innovation Consultant with the World Bank, where he was responsible for mapping innovations that cuts across agricultural productivity, livestock, market information, financial services, etc. 
                        Kenneth has also worked and consulted for developmental projects funded by Rockefeller Foundation/Sustainable Food Lab (Food Loss and Waste Pilot), Bill and Melinda Gates Foundation/TechnoServe (State Partnership for Agriculture), just to mention a few.</p>
                    <p>Kenneth started his career in Heineken Nigeria (Nigerian Breweries Plc), where he was Project Support Analyst for the Sona Integration Project in 2010-11. 
                        He has worked and consulted for several publicly quoted firms like SCOA Plc, First Aluminum Plc, Accenture, just to mention a few.
                        He is a result driven professional and joins the Farmgate Team as the CEO.</p>
                </div>
            </div>
        </div>
    </div>
</section>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="js/custom.js"></script>
</body>

</html>










