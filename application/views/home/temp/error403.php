
<div class="container-fluid">
       <div class="row">
           <div class="col-12 text-center">
            <h1 class="error-title mt-4">403</h1>
            <h3 class="error-sub-title">Oops!</h3>
            <p class="error-body mb-3">You don't have permission to access this server.</p>
            <a href="<?php echo base_url();?>" class="error-link">Back to Home</a>
           </div>
       </div>
   </div>

