<div class="container-fluid body-bg body-bg-buy">
    <div class="row">
        <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1  search-main-top py-4">
            <div class="row">
                <div class="col-lg-3 col-md-4 dropdown">
                    <button type="button" class="btn btn-lg btn-green btn-none dropdown-toggle pull-mobile" id="dropdownMenuButton" data-toggle="dropdown"
                        aria-haspopup="true">
                        <i class="far fa-list-alt"></i>&nbsp; Farms Categories</button>
                    <div class="dropdown-menu dropdown-width no-padding" aria-labelledby="dropdownMenuButton">
                        <div class="container">
                            <div class="row background">
                                <div class="col-6 category-border">
                                    <div class="overflow my-2">
                                      <?php foreach ($categories as $key => $cat) {?>
                                        <a class="dropdown-item" href="<?php echo base_url('products/category/').$cat->c_id ; ?>"><?php echo $cat->c_name; ?></a>
                                      <?php } ?>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="overflow my-2">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="offset-sm-2 col-sm-8 offset-md-0 offset-lg-1  col-12 buy-remove-bg">
                    <form method="get" action="<?php echo base_url('products/'); ?>" class="search-field" id="prodSearchForm">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <!-- <div class="input-group-text radio-group">
                                    <input class="input" id="type-buy" type="radio" name="type" value="2" checked="checked">
                                    <label class="label label1" for="type-buy">BUY</label>
                                    <input class="input" id="type-sell" type="radio" name="type" value="1">
                                    <label class="label label2" for="type-sell">SELL</label>
                                </div> -->
                            </div>

                            <input type="text" id="search_keyword" name="keyword" class="form-control buy-bg orders" placeholder="What are you looking for?" value="<?php if(!empty($keyword)){ echo $keyword; } ?>"aria-label="Text input with radio button">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-lg search-btn buy-bg">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    <!-- </form> -->
                </div>
            </div>
        </div>
    </div>
</div>
