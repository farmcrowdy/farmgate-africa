<div class="container-fluid footer-bg py-5 no-pad">
    <div class="row">
        <div class="col-sm-10 offset-sm-1 pl-3 pl-md-0">
            <div class="row">
                <div class="col-lg-5 col-md-5">
                    <img src="<?php echo base_url();?>mainasset/fe/images/farmgate-logo2.svg" class="img-fluid img-noPad" alt="Farmgate logo">
                    <p class="pad-right"><br>FarmGate Africa is a company focused on providing major processors and international buyers the opportunity to purchase commodities directly from farming clusters.</p>
                    <ul class="social-icons">
                        <li>
                            <a target="_blank" href="https://web.facebook.com/farmgateafrica/">
                                <img src="<?php echo base_url();?>mainasset/fe/images/icons/fb.svg" alt="facebook icon">
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.twitter.com/farmgateafrica/">
                                <img src="<?php echo base_url();?>mainasset/fe/images/icons/tw.svg" alt="facebook icon">
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.youtube.com/channel/UC_ujWVvgm8-jxeD17SQlMkQ?disable_polymer=true">
                                <img src="<?php echo base_url();?>mainasset/fe/images/icons/yt.svg" alt="facebook icon">
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.instagram.com/farmgateafrica/">
                                <img src="<?php echo base_url();?>mainasset/fe/images/icons/inst.svg" alt="facebook icon">
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.linkedin.com/company/farmgate-africa1/">
                                <img src="<?php echo base_url();?>mainasset/fe/images/icons/lin.svg" alt="facebook icon">
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- <div class="col-lg-2 col-6 offset-md-1 offset-lg-0 col-md-3"> -->
                    <!-- <h4>For Traders</h4>
                    <ul class="footer-subNav">
                        <li><a href="#">Why invest</a></li>
                        <li><a href="#">How it works</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Investor groups</a></li>
                        <li><a href="#">Risk</a></li>
                    </ul> -->
                <!-- </div> -->
                <!-- <div class="col-lg-2 col-6 offset-md-1 offset-lg-0 col-md-3"> -->
                    <!-- <h4>For Farmers</h4>
                    <ul class="footer-subNav">
                        <li><a href="#">How it works</a></li>
                        <li><a href="#">Benefits</a></li>
                        <li><a href="#">Collection Centers</a></li>
                        <li><a href="#">Become a Supplier</a></li>
                    </ul> -->
                <!-- </div> -->
                <div class="col-lg-2 col-6 col-md-3">
                    <h4>Company</h4>
                    <ul class="footer-subNav">
                        <li><a href="<?php echo base_url();?>about">About</a></li>
                        <li><a target="_blank" href="https://medium.com/@farmgate.africa/">Blog</a></li>
                        <li><a href="<?php echo base_url();?>contact">Contact</a></li>
                        <!-- <li><a href="#">We're hiring!</a></li> -->
                    </ul>
                </div>
                <div class="col-lg-2 col-6 offset-md-1 offset-lg-0 col-md-3">
                    <h4>Legal</h4>
                    <ul class="footer-subNav">
                        <!-- <li><a href="#">Disclaimer</a></li> -->
                        <li><a href="<?php echo base_url();?>toc">Terms & Conditions</a></li>
                        <li><a href="<?php echo base_url();?>privacy">Privacy Policy</a></li>
                        <!-- <li><a href="#">Investment Securities</a></li> -->
                    </ul>
                </div>
            </div>
            <div class=" footer-border-bottom"></div>
            <div class="row copyright">
                <div class="col-md-6">
                    <p>A member of the <a href="http://farmcrowdygroup.com/" target="_blank">Farmcrowdy Group</a></p>
                </div>
                <div class="col-md-6 text-md-right">
                    <p>&copy;
                        <?php echo date('Y'); ?> FarmGate Africa - All Rights Reserved
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<!-- <script src="<?php echo base_url();?>mainasset/fe/js/lity.js"></script> -->
<script src="<?php echo base_url();?>mainasset/fe/js/custom.js"></script>
<script>
    AOS.init();
</script>
