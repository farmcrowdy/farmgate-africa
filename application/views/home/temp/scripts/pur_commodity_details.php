<script type="text/javascript">
var open_delimiter = '<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>';
var open_delimiter_success = '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>';
var close_delimiter = '</strong></div>';

var user_id = '<?php if(!empty($user)){echo $user->id; }else{ echo "0";} ?>';
var produce_type_id = '<?php if(!empty($commodity)){echo $commodity->purc_produce_type_id; }else{ echo "0";} ?>';
var produce_id = '<?php if(!empty($commodity)){echo $commodity->purc_produce_id; }else{ echo "0";} ?>';
var commodity_id = '<?php if(!empty($commodity)){echo $commodity->purc_id; }else{ echo "0";} ?>';
var commodity_name = '<?php if(!empty($commodity)){echo $commodity->purc_name; }else{ echo "None";} ?>';



function check()
{
    $('#feedback').empty().fadeIn(200).append(open_delimiter+'<center><p>You are not logged in! Please login or sign up to trade.</p></center>'+close_delimiter);
    // alert("You are not logged in.\nPlease login or sign up to trade.\nThank You");
}

function confirmTrade()
{   
    var quantity = $('.ctnumber7').val();
    quantity = parseInt(quantity);
    var unit = 'unit';
    var leftQuantity = $('#noLeft').text();
    leftQuantity = parseInt(leftQuantity);
    console.log({leftQuantity});
    if(leftQuantity == 0){
        alert('The units left is 0.');
        return false;
    }
    if(leftQuantity < quantity ){
        alert('The quantity you entered is greater than units left!');
        return false;
    }
    if(quantity > 1){
        unit = 'units'; 
    }
    
    var doTrade = confirm("Do you want to buy "+quantity+" "+unit+" of "+commodity_name+" ?");
    if(doTrade){
        trade();
 }
}

function trade()
{
        // Get all the values
       var quantity = $('.ctnumber7').val();

        console.log({
            user_id,
            produce_type_id,
            produce_id,
            commodity_id,
            quantity
        })
        // return false;
        $.ajax({
            type: "post",
            url: "/purchaseAjax",
            data: { "user_id": user_id, 
                    "produce_type_id": produce_type_id , 
                    "produce_id": produce_id,
                    "commodity_id": commodity_id,
                    "quantity": quantity
                },
            success: function(data){
                data = JSON.parse(data);
                console.log(data.qleft);
                $('#i_noLeft').attr('max', data.qleft);
                $('#noLeft').empty().fadeOut(200).fadeIn(200).append(data.qleft);
                alert(data.message);
                window.location.href = '<?php echo base_url('trans-success'); ?>';

            },
            error: function() { alert("Error communicating with our servers."); }
        });

}
</script>
