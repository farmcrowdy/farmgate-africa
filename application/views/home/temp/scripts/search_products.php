<script type="text/javascript">
$(document).ready(function(){

    console.log('We will win');

    // Get all the input fields and search
    $('#sold-out').change(function(){
        search('sold-out ');
    });
    $('#now-selling').change(function(){
        search('now-selling ');
    });
    $('#opening-soon').change(function(){
        search('opening-soon ');
    });
    $('#sortBy').change(function(){
        search('sortBy ');
    });
    $('#keyword_comm').keyup(function(){
        search('keyword_comm ');
    });

    function search(){

        // Get all the values
        var comm_type = $('#comm_type').attr('value');
        var soldOut = $('#sold-out').is(':checked')?'sold-out': "";
        var nowSelling = $('#now-selling').is(':checked')?'now-selling': "";
        var openingSoon = $('#opening-soon').is(':checked')?'opening-soon': "";
        var sortBy  = $('#sortBy').val();
        var keyword = $('#keyword_comm').val();

        console.log({
            comm_type,
            soldOut,
            nowSelling,
            openingSoon,
            sortBy,
            keyword
        })

        $.ajax({
            type: "post",
            url: "/searchCommodities",
            data: {"comm_type": comm_type, "keyword": keyword , "statuses": [openingSoon, nowSelling, soldOut], "sortBy": sortBy },
            success: function(data){
            //    console.log(data);
               $('#purc_search_results').empty().fadeIn(200).append(JSON.parse(data));
            },
            error: function() { alert("Error communicating with our servers."); }
        });


    }

});
</script>
