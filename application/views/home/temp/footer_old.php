<div class="container-fluid">
    <div class="row footer">
        <div class="col-lg-10 offset-lg-1 mt-4">
            <div class="row">
                <div class="col-md-6">
                    <p>Subscribe to receive news first</p>
                    <form class="row">
                        <div class="form-group col-lg-9 col-sm-8 email-input">
                            <input type="email" class="form-control" id="email" placeholder="Email">
                        </div>
                        <div class="col-lg-3 col-sm-4 submit-email">
                            <button type="submit" class="btn ">SUBSCRIBE</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-3 col-6 mt-3">
                    <ul class="footer-list">
                        <li>
                            <a href="#">Home</a>
                        </li>
                        <li>
                            <a href="#">About</a>
                        </li>
                        <li>
                            <a href="#">How it works</a>
                        </li>
                        <li>
                            <a href="#">Compliance</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 col-6  mt-3">
                    <ul class="footer-list">
                        <li>
                            <a href="#">Terms of Service</a>
                        </li>
                        <li>
                            <a href="#">Privacy Policy</a>
                        </li>
                        <li>
                            <a href="#">Support</a>
                        </li>
                        <li>
                            <a href="#">FAQ</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="background:#293737;">
        <div class="col-lg-10 offset-lg-1 my-2">
            <p class="copyrite mb-0"> &copy; <?php echo date('Y'); ?> Farmcrowdy - All rights reserved</p>
        </div>
    </div>
</div>
<!-- ============end of body================= -->


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
<script src="<?php echo base_url();?>mainasset/fe/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>mainasset/fe/js/lity.js"></script>
<script src="<?php echo base_url();?>mainasset/fe/js/sliderjquery.js"></script>
<script src="<?php echo base_url();?>mainasset/fe/js/custom.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        //  carousel center present
        // $('.loop').owlCarousel({
        //     center: true,
        //     items:2,
        //     loop:true,
        //     margin:10,
        //     responsive:{
        //         600:{
        //             items:4
        //         }
        //     }
        // });
        $('.animated-icon3').click(function () {
            $(this).toggleClass('open');
        });
        $('#btn-menu').click(function () {
            $('#btn-menu').toggleClass("nav-button");
        });
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 3,
                    nav: false
                },
                1000: {
                    items: 4,
                    nav: true,
                    loop: false
                }
            }
        })
        // add event listener (next and prev btns)
        var owl = $('.owl-carousel');
        owl.owlCarousel();
        // Go to the next item
        $('.owl-next').click(function () {
            owl.trigger('next.owl.carousel');
        })
        // Go to the previous item
        $('.owl-prev').click(function () {
            // With optional speed parameter
            // Parameters has to be in square bracket '[]'
            owl.trigger('prev.owl.carousel', [300]);
        })
    })
</script>

