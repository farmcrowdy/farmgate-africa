
<div class="container-fluid">
       <div class="row">
           <div class="col-12 text-center">
            <h1 class="error-title mt-4">404</h1>
            <h3 class="error-sub-title">Oops!</h3>
            <p class="error-body mb-3">We can't seem to find the page you are looking for</p>
            <a href="<?php echo base_url();?>" class="error-link">Back to Home</a>
           </div>
       </div>
   </div>