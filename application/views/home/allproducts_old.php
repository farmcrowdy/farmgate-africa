<div class="container">
    <div class="row">
        <ul class="sub-nav py-2">
            <li>
                <a href="<?php echo base_url();?>" class="">Home /</a>
            </li>
            <li>Products</li>
        </ul>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="row">
                <div class="col-12 d-md-none">
                    <span class="move-margin">
                        <a href="#" class="filter-link">Filter &nbsp;
                            <i class="fas fa-angle-right"></i>
                        </a>
                    </span>
                    <span class="number px-2">
                        <?php echo $total_products; ?>
                        <span class="results px-2">Result
                            <?php if($total_products > 1){ echo "s"; } ?></span>
                    </span>
                </div>
                <aside class="col-lg-3 col-md-4 aside-mobile pt-md-3 d-none d-md-block pt-5 animatedSlide slideInFromLeft" id="sidemenuToggle">
                    <div class=" pb-5 close-btn d-md-none">
                        <button type="button" class="close" aria-label="Close">
                            <span aria-hidden="true" class=" hide-sidebar">&times;</span>
                        </button>
                    </div>
                    <p class="text-center individual-product-state">
                        <span class="">
                            <?php echo $total_products; ?></span> Product
                        <?php if($total_products > 1){ echo "s"; } ?> Found
                    </p>
                    <div class="text-center">
                        <a href="javascript:void(0)" class="send-msg-link" id="clearProdSearchForm">Clear Filters</a>
                    </div>
                    <!-- <form method="get" action="<?php echo base_url('products/'); ?>" class=""> -->
                    <div class="accordion mt-4" id="accordionMain">
                        <div class="card">
                            <div class="card-header buy-header text-center" id="headingMain">
                                <h5 class="mb-0">
                                    <button class="btn btn-link button-link collapsed" type="button" data-toggle="collapse" data-target="#collapseMain" aria-expanded="false"
                                        aria-controls="collapseMain">
                                        <i class="fas fa-chevron-up"></i>&nbsp; Location
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseMain" class="collapse show" aria-labelledby="headingMain" data-parent="#accordionMain">
                                <div class="card-body  m-0 p-0">


                                    <div class="accordion" id="accordion">
                                        <div class="card">
                                            <div class="card-header buy-header" id="headingOne">
                                                <h5 class="mb-0">
                                                    <input type="checkbox" class="region" aria-label="Checkbox for following text input" id="checkbox1">
                                                    <button class="btn btn-link <?php $northEast = array(2,5,16,35,36); if(empty(array_intersect($northEast, $search_states))){echo "
                                                        collapsed "; } ?>" type="button" data-toggle="collapse" data-target="#collapseOne"
                                                        aria-expanded="true" aria-controls="collapseOne">North
                                                        East
                                                        <span class="move-icon-right">
                                                            <i class="fas fa-plus"></i>
                                                        </span>
                                                    </button>
                                                </h5>
                                            </div>

                                            <div id="collapseOne" class="collapse <?php if(!empty(array_intersect($northEast, $search_states))){echo " show "; } ?>"
                                                aria-labelledby="headingOne" data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" id="" value="2" <?php if(in_array( "2", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Adamawa</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" id="" value="5" <?php if(in_array( "5", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Bauchi</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" id="" value="16" <?php if(in_array( "16", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Gombe</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" id="" value="35" <?php if(in_array( "35", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Taraba</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" id="" value="36" <?php if(in_array( "36", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Yobe</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header buy-header" id="headingTwo">
                                                <h5 class="mb-0">

                                                    <input type="checkbox" class="region" aria-label="Checkbox for following text input">

                                                    <button class="btn btn-link <?php $northWest = array(18,19,20,22,34,37); if(empty(array_intersect($northWest, $search_states))){echo "
                                                        collapsed "; } ?>" type="button" data-toggle="collapse" data-target="#collapseTwo"
                                                        aria-expanded="false" aria-controls="collapseTwo">North
                                                        West
                                                        <span class="move-icon-right">
                                                            <i class="fas fa-plus"></i>
                                                        </span>
                                                    </button>
                                                </h5>
                                            </div>
                                            <div id="collapseTwo" class="collapse <?php if(!empty(array_intersect($northWest, $search_states))){echo " show "; } ?>"
                                                aria-labelledby="headingTwo" data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" value="18" <?php if(in_array( "18", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Jigawa</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" value="19" <?php if(in_array( "19", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Kaduna</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" value="20" <?php if(in_array( "20", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Kano</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" value="22" <?php if(in_array( "22", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Kebbi</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" value="34" <?php if(in_array( "34", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Sokoto</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" value="37" <?php if(in_array( "37", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Zamfara</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header buy-header" id="headingThree">
                                                <h5 class="mb-0">
                                                    <input type="checkbox" class="region" aria-label="Checkbox for following text input">
                                                    <button class="btn btn-link <?php $northCentral = array(7,23,24,26,27,32,15); if(empty(array_intersect($northCentral, $search_states))){echo "
                                                        collapsed "; } ?>" type="button" data-toggle="collapse" data-target="#collapseThree"
                                                        aria-expanded="false" aria-controls="collapseThree">North
                                                        Central
                                                        <span class="move-icon-right">
                                                            <i class="fas fa-plus"></i>
                                                        </span>
                                                    </button>
                                                </h5>
                                            </div>
                                            <div id="collapseThree" class="collapse <?php if(!empty(array_intersect($northCentral, $search_states))){echo " show
                                                "; } ?>" aria-labelledby="headingThree" data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" value="7" <?php if(in_array( "7", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Benue</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" value="23" <?php if(in_array( "23", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Kogi</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" value="24" <?php if(in_array( "24", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Kwara</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" value="26" <?php if(in_array( "26", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Nasarawa</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" value="27" <?php if(in_array( "27", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Niger</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" value="32" <?php if(in_array( "32", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Plateau</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" value="15" <?php if(in_array( "15", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">FCT</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header buy-header" id="headingFour">
                                                <h5 class="mb-0">
                                                    <input type="checkbox" class="region" aria-label="Checkbox for following text input">
                                                    <button class="btn btn-link <?php $southEast = array(1,4,11,14,17); if(empty(array_intersect($southEast, $search_states))){echo "
                                                        collapsed "; } ?>" type="button" data-toggle="collapse" data-target="#collapseFour"
                                                        aria-expanded="false" aria-controls="collapseFour">South
                                                        East
                                                        <span class="move-icon-right">
                                                            <i class="fas fa-plus"></i>
                                                        </span>
                                                    </button>
                                                </h5>
                                            </div>
                                            <div id="collapseFour" class="collapse <?php if(!empty(array_intersect($southEast, $search_states))){echo " show "; } ?>"
                                                aria-labelledby="headingFour" data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" id="" name="states[]" value="1" <?php if(in_array( "1", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Abia</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" id="" name="states[]" value="4" <?php if(in_array( "4", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Anambra</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" id="" name="states[]" value="11" <?php if(in_array( "11", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Ebonyi</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" id="" name="states[]" value="14" <?php if(in_array( "14", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Enugu</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" id="" name="states[]" value="17" <?php if(in_array( "17", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Imo</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header buy-header" id="headingFive">
                                                <h5 class="mb-0">
                                                    <input type="checkbox" class="region" aria-label="Checkbox for following text input">

                                                    <button class="btn btn-link <?php $southWest = array(13,25,28,29,30,31); if(empty(array_intersect($southWest, $search_states))){echo "
                                                        collapsed "; } ?>" type="button" data-toggle="collapse" data-target="#collapseFive"
                                                        aria-expanded="false" aria-controls="collapseFive">South
                                                        West
                                                        <span class="move-icon-right">
                                                            <i class="fas fa-plus"></i>
                                                        </span>
                                                    </button>
                                                </h5>
                                            </div>
                                            <div id="collapseFive" class="collapse <?php if(!empty(array_intersect($southWest, $search_states))){echo " show "; } ?>"
                                                aria-labelledby="headingFive" data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" id="" value="13" <?php if(in_array( "13", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Ekiti</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" id="" value="25" <?php if(in_array( "25", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Lagos</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" id="" value="28" <?php if(in_array( "28", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Ogun</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" id="" value="29" <?php if(in_array( "29", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Ondo</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" id="" value="30" <?php if(in_array( "30", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Osun</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" id="" value="31" <?php if(in_array( "31", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Oyo</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header buy-header" id="headingSix">
                                                <h5 class="mb-0">
                                                    <input type="checkbox" class="region" aria-label="Checkbox for following text input">

                                                    <button class="btn btn-link <?php $southSouth = array(3,6,9,33,10,12); if(empty(array_intersect($southSouth, $search_states))){echo "
                                                        collapsed "; } ?>" type="button" data-toggle="collapse" data-target="#collapseSix"
                                                        aria-expanded="false" aria-controls="collapseSix">South
                                                        South
                                                        <span class="move-icon-right">
                                                            <i class="fas fa-plus"></i>
                                                        </span>
                                                    </button>
                                                </h5>
                                            </div>
                                            <div id="collapseSix" class="collapse <?php if(!empty(array_intersect($southSouth, $search_states))){echo " show "; } ?>"
                                                aria-labelledby="headingSix" data-parent="#accordion">
                                                <div class="card-body">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" id="" value="3" <?php if(in_array( "3", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Akwa Ibom</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" id="" value="6" <?php if(in_array( "6", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Bayelsa</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" id="" value="9" <?php if(in_array( "9", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Cross River</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" id="" value="33" <?php if(in_array( "33", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Rivers</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" id="" value="10" <?php if(in_array( "10", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Delta</label>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input state" name="states[]" id="" value="12" <?php if(in_array( "12", $search_states)){echo
                                                            "checked"; } ?>>
                                                        <label class="form-check-label pl-4" for="">Edo</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="accordion my-4" id="accordionVolume">
                        <div class="card">
                            <div class="card-header buy-header text-center" id="volume">
                                <h5 class="mb-0">
                                    <button class="btn btn-link button-link " type="button" data-toggle="collapse" data-target="#volumeOne" aria-expanded="true"
                                        aria-controls="volumeOne">
                                        <i class="fas fa-chevron-up"></i>&nbsp; Volume
                                    </button>
                                </h5>
                            </div>

                            <div id="volumeOne" class="collapse show" aria-labelledby="volume" data-parent="#accordionVolume">
                                <div class="card-body row">
                                    <div class="col-10 offset-1">
                                        <p class="price-range">
                                            <label for="amount" style="color:#666666; font-size:1em;">Price range:</label>
                                            <input type="text" name="amount" id="amount" readonly style="border:0; color:#444444; font-size:.8em;">
                                        </p>

                                        <div id="slider-range"></div>
                                        <div class="py-4 select-slide">
                                            <input type="checkbox" name="use_volume" class="form-check-input" id="use_volume" <?php if(!empty($use_volume)){echo
                                                "checked";} ?>>
                                            <label class="form-check-label pl-4" for="">Use Price</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion my-4">
                            <center>
                                <button type="submit" style="padding:5px; margin-bottom:10px" class="nav-link loginClass order-pg product ">Search</button>
                            </center>
                        </div>
                    </div>
                    </form>
                </aside>
                <!-- <div class="col-lg-9 pb-4" style="border:1px solid black"> -->
                <div class="col-lg-9 col-md-8 pb-4">
                    <div class="row">
                        <div class="col-12">
                            <!-- Row -->
                            <div class="row">
                                <!-- column -->
                                <?php if(!empty($products)) : ?>
                                <?php foreach ($products as $product): ?>

                                <div class="col-lg-4 col-md-6 mt-4">
                                    <!-- Card -->
                                    <div class="card">
                                        <a href="<?php echo base_url();?>product/<?php echo $product->prod_slug; ?>">
                                            <div class="pad-product product--status product--status-sold">
                                                <img class="card-img-top img-fluid resize-img-buy" src="<?php echo base_url();?>mainasset/fe/images/products/<?php echo $product->prod_image ; ?>"
                                                    alt="<?php echo $product->prod_name ;?>">
                                                <div class="card-body">
                                                    <h4 class="card-title">
                                                        <?php echo $product->prod_name ;?>
                                                    </h4>
                                                    <p class="card-text small product-id">
                                                        <?php echo $product->prod_slug ;?>
                                                    </p>
                                                    <p class="unit">
                                                        <?php echo $product->prod_quantity ;?>
                                                        <span class="small">
                                                            <?php echo $product->unit ;?></span>
                                                    </p>
                                                    <p class="price-per-mt">&#8358;
                                                        <?php echo $product->prod_price_per_unit ;?>
                                                        <span span class="small">per
                                                            <?php echo $product->unit;?></span>
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <!-- Card -->
                                </div>

                                <?php endforeach ?>
                                <?php else: ?>
                                <div>
                                    <p> No products available </P>
                                </div>
                                <?php endif ?>
                            </div>
                            <!-- Row -->

                        </div>
                    </div>
                    <?php if(!empty($pagination)) echo $pagination; ?>
                    <!-- <nav aria-label="Page navigation">
                            <ul class="pagination mt-4 justify-content-end d-flex">
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">1</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">2</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">3</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav> -->
                </div>
            </div>
        </div>

    </div>
</div>