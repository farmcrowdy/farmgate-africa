<div class="container-fluid">
    <div class="row page-title-bg no-pad">
        <div class="col-12">
            <h4>Commodity 
                
            <?php echo ucwords($commodity_type); ?></h4>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row search-main">
        <div class="col-lg-6 offset-lg-3 col-sm-8 offset-sm-2 col-10 offset-1">
            <div class="container-search">
                <input name="comm_type" id="comm_type" type="hidden" value="<?php echo $commodity_type; ?>" >
                <input name="keyword_comm" id="keyword_comm" class="search__input" type="text" placeholder="Search">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-md-1 pt-5 pb-4 search-output">
            <div class="row">
                <div class="col-md-6 text-center text-md-left">
                    
                </div>

                <div class="col-md-6 col-12 search">
                    <a class="d-md-none filter marketplace-slide" href="#">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate" viewBox="0 0 12 12"
                            width="12" height="12">
                            <defs>
                                <clipPath id="_clipPath_AAq10kbN1And8xOoywWzOb9GmIeelGwV">
                                    <rect width="12" height="12" />
                                </clipPath>
                            </defs>
                            <g clip-path="url(#_clipPath_AAq10kbN1And8xOoywWzOb9GmIeelGwV)">
                                <g>
                                    <g>
                                        <g>
                                            <path d=" M 4.243 11.52 L 4.243 5.634 L 0.129 0.868 C -0.092 0.599 -0.067 0.044 0.502 0.044 L 11.478 0.044 C 12.012 0.044 12.136 0.534 11.851 0.868 L 7.836 5.507 L 7.836 9.149 C 7.836 9.298 7.786 9.424 7.66 9.523 L 5.092 11.893 C 4.795 12.114 4.243 11.995 4.243 11.52 Z  M 1.601 1.044 L 5.119 5.109 C 5.193 5.208 5.243 5.309 5.243 5.433 L 5.243 10.396 L 6.839 8.949 L 6.839 5.334 C 6.839 5.211 6.888 5.109 6.962 5.01 L 10.379 1.044 L 1.601 1.044 L 1.601 1.044 Z "
                                                fill="rgb(136,136,136)" vector-effect="non-scaling-stroke" stroke-width="1" stroke="rgb(136,136,136)"
                                                stroke-linejoin="miter" stroke-linecap="butt" stroke-miterlimit="4" />
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        Filters
                    </a>
                    <select class="custom-select" id="sortBy" name="sortBy">
                        <option value="" selected>Sort by</option>
                        <option value="comm_price_per_unit">Price</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row relative-on-mobile mb-4">
        <div class="offset-md-1 col-md-10 col-12 ">
            <div class="row">
                <div class="col-lg-2 col-md-3 d-hide-filter slider" id="slide-out">
                    <!-- <div class="col-lg-2 col-md-3" id="slide-out"> -->
                    <div class="pt-2 pb-3 d-md-none">
                        <a href="#" class="hide-marketplace">
                            <i class="fas fa-arrow-circle-left fa-2x"></i>
                        </a>
                    </div>
                    <div class="accordion" id="marketplace-filter">
                        <div class="">
                            <div class="" id="filterTwo">
                                <h5 class="mb-0 pt-3">
                                    <a href="javascript:void(0)" class="shop-title" data-toggle="collapse" data-target="#status" aria-expanded="true" aria-controls="status">
                                        Farm Status
                                    </a>
                                </h5>
                            </div>
                            <div id="status" class="collapse show" aria-labelledby="filterTwo" data-parent="">
                                <div class="shop-sub-class">
                                    <?php if (!empty($statuses)): ?>
                                    <?php foreach ($statuses as $status): ?>
                                    <div>
                                        <input type="checkbox" class="form-check-input" id="<?php echo ($status); ?>" value="<?php echo ($status); ?>">
                                        <label class="form-check-label" for="<?php echo ($status); ?>">
                                            <?php echo ucwords(str_replace('-', ' ', $status)); ?></label>
                                    </div>
                                    <?php endforeach?>
                                    <?php endif?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-9 offset-md-1 col-md-8 col-sm-10 offset-sm-1 col-12 pb-5 marketplace">
                    <div id="purc_search_results">
                    <p>Showing <span>
                            <?php if (!empty($total_commodities)) {echo number_format($total_commodities);} else {echo "0";}?></span><span> commodit<?php if (count($commodities) > 1) {echo "ies";} else {echo "y";}?></span> </p>
                        <div class="row pt-3 px-2 px-md-0 pt-md-0">
                            <?php if (!empty($commodities)): ?>
                            <?php foreach ($commodities as $Commodity): ?>
                            <div class="col-lg-4 col-md-6 col-sm-4 col-6">
                                <div class="row products">
                                    <div class="col-12 product-border px-0">
                                        <a href="<?php echo base_url('commodity/') . $Commodity->comm_slug; ?>">
                                            <div class="img-div">
                                                <img src="<?php echo base_url(); ?>/mainasset/fe/images/commodities/<?php echo $Commodity->comm_image; ?>" class="img-fluid product-img w-100"
                                                    alt="Farm produce">
                                                <span class="<?php echo $Commodity->comm_status; ?>">
                                                    <?php echo ucwords(str_replace('-', ' ', ($Commodity->comm_status))); ?></span>

                                            </div>
                                            <div>
                                                <p class="product-name">
                                                    <?php echo ucwords($Commodity->comm_name); ?>
                                                </p>
                                                <?php if($Commodity->comm_commodity_type == "purchase"): ?>
                                                <!-- <p class="product-price"><span>₦
                                                        <?php // echo number_format($Commodity->comm_price_per_unit); ?></span> per
                                                    <?php // echo ucwords($Commodity->unit); ?>
                                                </p> -->
                                                <p class="product-price"><span>
                                                        <?php echo number_format($Commodity->comm_quantity_left_to_buy); ?></span> units available
                                                </p>
                                                <?php else: ?>
                                                <p class="product-price"><span>
                                                        <?php echo number_format($Commodity->comm_quantity_left); ?></span> units available
                                                </p>
                                                <?php endif ?>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach?>
                            <?php else: ?>
                            <p>No Commodity Found</p>
                            <?php endif?>
                        </div>
                        <?php if(!empty($pagination)) echo $pagination; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>
<div class="container-fluid">
    <div class="row subscribe-bg" data-aos="fade-up" data-aos-anchor-placement="center-bottom">
        <div class="col-10 offset-1 offset-md-0 col-md-12 text-center">
            <h4 class="mobile">Get notified when commodities are available to trade </h4>
        </div>
        <form class="form-inline  col-md-6 offset-md-3 col-10 col-sm-8 offset-sm-2 offset-1 col-lg-4 offset-lg-4">
            <div class="input-group w-100 pad-sub mb-3">
                <input type="email" class="form-control email-subscribe" placeholder="Enter your email" aria-label="email" aria-describedby="button-addon2">
                <div class="input-group-append">
                    <button class="btn " type="submit" id="button-addon2">Sign up</button>
                </div>
            </div>
        </form>
    </div>
    <div class="row security-bg security-bg1">
        <div class="col-12" data-aos="fade-up" data-aos-duration="3000">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-12 px-4 px-sm-0 col-md-8 offset-md-2 col-sm-10 offset-sm-1 text-center">
                    <h4>Safe, insured and secured.</h4>
                    <p>Farmgate Trader was built with security of funds and data in mind. We work with a PCIDSS compliant
                        payment processor for the security of data. Our assets are held with reputable SEC registered firms,
                        who ensure investors’ funds are covered by invested asset.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-8 offset-md-2 col-10 offset-1 offset-lg-4 pt-4">
                    <div class="row">
                        <div class="col-md-4 col-6 text-center">
                            <div class=" d-flex justify-content-center">
                                <img src="<?php echo base_url(); ?>/mainasset/fe/images/icons/fund.svg" alt="funds icon">
                            </div>

                            <span>Funds Secured</span>
                        </div>
                        <div class="col-md-4 col-6 text-center">
                            <div class=" d-flex justify-content-center">
                                <img src="<?php echo base_url(); ?>/mainasset/fe/images/icons/data.svg" alt="data icon">
                            </div>
                            <span>Data Protected</span>
                        </div>
                        <div class="col-md-4 offset-3 offset-md-0 col-6 pt-4 pt-md-0 text-center">
                            <div class=" d-flex justify-content-center">
                                <img src="<?php echo base_url(); ?>/mainasset/fe/images/icons/investment.svg" alt="investment icon">
                            </div>
                            <span>Investment Insured</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="js/custom.js"></script>