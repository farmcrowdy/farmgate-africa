<div class="clearfix"></div>
<div class="container-fluid jumbotron-bg">
    <div class="row no-pad">
        <div class="col-md-6 title-left">
            <div class="row">
                <div class="offset-lg-2 col-lg-10">
                    <div class="title-container-left ">
                        <p class="head-title">COMMODITY TRADING</p>
                        <h4>Trade in agricultural commodities in a much smarter way.</h4>
                        <p class="marg-b">Invest in large scale agricultural commodity trading and earn returns on your money.
                        </p>
                        <!-- <a href="<?php // echo base_url('marketplace/?comm_type=trade'); ?>" class="trade-commodity-link">Start
                            Trading</a> -->
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-6 title-right text-md-right">
            <div class="row">
                <div class="col-lg-10">
                    <div class="title-container-right ">
                        <p class="head-title">COMMODITY PURCHASE</p>
                        <h4>Instant access to high grade agricultural commodities</h4>
                        <p class="align-commodity marg-b">Purchase high quality farm produce conveniently and at fair prices
                            directly from smallholder farmers.
                        </p>
                        <!-- <a href="<?php // echo base_url('marketplace/?comm_type=purchase'); ?>" class="buy-commodity-link">Buy
                            Commodities</a> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- <div class="container-fluid products-container products-container1">
    <div class="row">
        <div class="col-lg-10 offset-lg-1 col-md-8 offset-md-2 col-12">
            <div class="row text-center text-lg-left">
                <div class="col-12 text-left">
                    <span class="commodity-trade1">COMMODITY PURCHASE</span>
                </div>
                <div class="col-lg-6 col-md-8 col-9 text-left insurance-container">
                    <h4 class="mobile mobile1">Available to buy</h4>
                </div>
                <div class="col-lg-2 offset-lg-4 col-md-4 col-3 offset-lg-0 text-right">
                    <a href="<?php // echo base_url('marketplace/?comm_type=purchase'); ?>" class="see-all-products">See all</a>
                </div>
            </div>
            <div class="row">
                <?php // if(!empty($pcommodities)): ?>
                <?php // foreach($pcommodities as $pcommodity): ?>
                <div class="col-lg-3 col-sm-4 col-6">
                    <div class="row products">
                        <div class="col-12 product-border px-0">
                            <a href="<?php // echo base_url('commodity/').$pcommodity->comm_slug; ?>">
                                <div class="img-div">
                                    <img src="<?php //  echo base_url();?>mainasset/fe/images/commodities/<?php echo ($pcommodity->comm_image); ?>" class="img-fluid product-img"
                                        alt="Farm produce">
                                    <span class="<?php // echo ($pcommodity->comm_status); ?>">
                                        <?php // echo ucwords(str_replace('-', ' ',$pcommodity->comm_status)); ?></span>
                                </div>
                                <div>
                                    <p class="product-name">
                                        <?php // echo ($pcommodity->comm_name); ?>
                                    </p>

                                    <p class="product-price"><span>
                                            <?php // echo number_format($pcommodity->comm_quantity_left_to_buy); ?></span> units available
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <?php // endforeach ?>
                <?php // endif ?>
            </div>
            <div class="underline"></div>
        </div>
    </div>
</div> -->

<!-- <div class="container-fluid products-container">
    <div class="row">
        <div class="col-lg-10 offset-lg-1 col-md-8 offset-md-2 col-12">
            <div class="row text-center text-lg-left">
                <div class="col-12 text-left">
                    <span class="commodity-trade">COMMODITY TRADING</span>
                </div>
                <div class="col-lg-6 col-md-8 col-9  text-left insurance-container">
                    <h4 class="mobile mobile1">Available to trade</h4>
                </div>
                <div class="col-lg-2 offset-lg-4 col-md-4 col-3 offset-lg-0 text-right">

                    <a href="<?php // echo base_url('marketplace/?comm_type=trade'); ?>" class="see-all-products">See all</a>
                </div>
            </div>
            <div class="row">
                <?php  //if(!empty($icommodities)): ?>
                <?php // foreach($icommodities as $icommodity): ?>
                <div class="col-lg-3 col-sm-4 col-6">
                    <div class="row products">
                        <div class="col-12 product-border px-0">
                            <a href="<?php // echo base_url('commodity/').$icommodity->comm_slug; ?>">
                                <div class="img-div">
                                    <img src="<?php // echo base_url();?>mainasset/fe/images/commodities/<?php echo ($icommodity->comm_image); ?>" class="img-fluid product-img"
                                        alt="Farm produce">
                                    <span class="<?php // echo ($icommodity->comm_status); ?>">
                                        <?php // echo ucwords(str_replace('-', ' ',$icommodity->comm_status)); ?></span>
                                </div>
                                <div>
                                    <p class="product-name">
                                        <?php // echo ($icommodity->comm_name); ?>
                                    </p>
                                    <p class="product-price"><span>
                                            <?php // echo number_format($icommodity->comm_quantity_left); ?></span> units available
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <?php // endforeach ?>
                <?php // endif ?>
            </div>
            
        </div>
    </div>
</div> -->



<div class="container-fluid">
    <div class="row security-bg reveal" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-offset="0">
        <div class="col-12">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-10 offset-1 col-md-8 offset-sm-2 text-center">
                    <h4>Our Numbers</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-sm-10 offset-sm-1 col-12 offset-lg-2 pt-4">
                    <div class="row">
                        <div class="col-md-3 px-lg-4 col-6 col-sm-4 text-center">
                            <div class=" d-flex justify-content-center">
                                <img src="<?php echo base_url();?>mainasset/fe/images/icons/data-lock.svg" alt="funds icon">
                            </div>
                            <p class="traction">25K</p>
                            <p class="span">Metric Tonnes of Commodity Available For Purchase</p>
                        </div>
                        <div class="col-md-3 px-lg-4 col-6 col-sm-4 text-center">
                            <div class=" d-flex justify-content-center">
                                <img src="<?php echo base_url();?>mainasset/fe/images/icons/data-lock.svg" alt="funds icon">
                            </div>
                            <p class="traction">8</p>
                            <p class="span">Commodity Collection Centers</p>
                        </div>
                        <div class="col-md-3 px-lg-4 col-6 col-sm-4 text-center">
                            <div class=" d-flex justify-content-center">
                                <img src="<?php echo base_url();?>mainasset/fe/images/icons/data-lock.svg" alt="data icon">
                            </div>
                            <p class="traction">7.8K</p>
                            <p class="span">Farmers supplying Commodities</p>
                        </div>
                        <div class="col-md-3 px-lg-4 col-6 col-sm-4 offset-sm-4 offset-md-0 text-center">
                            <div class=" d-flex justify-content-center">
                                <img src="<?php echo base_url();?>mainasset/fe/images/icons/data-lock.svg" alt="investment icon">
                            </div>
                            <p class="traction">8K</p>
                            <p class="span">Metric Tonnes of Commodity Available For Trading</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="container-fluid">
    <div class="row reveal">
        <div class="col-md-10 offset-md-1 summary">
            <div class="row">
                <!-- <div class="col-lg-6 col-md-5 offset-1 offset-md-0 " data-aos="fade-right" data-aos-offset="300" data-aos-easing="ease-in-sine"> -->
                <div class="col-lg-6 col-md-5 offset-1 offset-md-0 " d data-aos="fade-down" data-aos-easing="linear" data-aos-duration="1500">
                    <img src="<?php echo base_url();?>mainasset/fe/images/icons/finance1.svg" class="img-fluid mr-auto" alt="finance image">

                </div>
                <div class="col-lg-6 col-md-7 offset-1 col-10 mt-4 mt-md-0 offset-md-0 text-center text-md-left" data-aos="fade-up" data-aos-duration="1000">
                    <p class="sub-title1">COMMODITY TRADING</p>
                    <h4 class="mobile text-left">A wide range of commodities traded across markets globally</h4>
                    <p class="push-left">Invest in commodities backed by confirmed purchase orders.</p>
                    <p><img src="<?php echo base_url();?>mainasset/fe/images/icons/check.svg" alt="checkbox" class="img-fluid pr-2">                        Quick
                        investment turnaround
                    </p>
                    <p><img src="<?php echo base_url();?>mainasset/fe/images/icons/check.svg" alt="checkbox" class="img-fluid pr-2">                        Impressive
                        returns</p>
                    <p class=""><img src="<?php echo base_url();?>mainasset/fe/images/icons/check.svg" alt="checkbox" class="img-fluid pr-2">Tested
                        and Trusted</p>
                    <p class=""><img src="<?php echo base_url();?>mainasset/fe/images/icons/check.svg" alt="checkbox" class="img-fluid pr-2">Impact
                        on smallholder farmers</p>
                    <span class="">
                        <!-- <a href="<?php // echo base_url('marketplace/?comm_type=trade'); ?>" class="start-trading-link">Start
                            Trading</a> -->
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="row border-bottom reveal">
        <div class="col-md-10 offset-md-1 summary">
            <div class="row">
                <div class="col-lg-5 col-md-7 col-10 offset-1 offset-md-0 text-center text-md-left" data-aos="fade-up" data-aos-duration="500">
                    <p class="sub-title2">COMMODITY PURCHASE</p>
                    <h4 class="mobile text-left">Purchase Safe and High Quality Farm Commodities</h4>
                    <p class="push-left">We source high quality produce for millers, food manufacturers, brewers or other
                        bulk commodity buyers in time and at the right price.</p>
                    <span class="">
                        <!-- <a href="<?php // echo base_url('marketplace/?comm_type=trade'); ?>" class="start-trading-link start-trading-link1">Buy
                            Commodities</a> -->
                    </span>
                </div>
                <div class="offset-lg-1 col-md-5 mt-5 col-10 offset-1 offset-md-0 mt-md-0" data-aos="fade-down" data-aos-easing="linear"
                    data-aos-duration="1500">
                    <img src="<?php echo base_url();?>mainasset/fe/images/icons/ppl1.svg" class="img-fluid" alt="finance image">

                </div>
            </div>
        </div>
    </div>
    <div class="row partners-border reveal">
        <div class="col-10 offset-1">
            <div class="row" data-aos="fade-up" data-aos-anchor-placement="center-bottom">

            </div>
        </div>
    </div>
    <div class="row security-bg">
        <div class="col-12">
            <div class="row reveal" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-offset="0">
                <div class="col-lg-6 offset-lg-3 col-10 offset-1 col-md-8 offset-sm-2 text-center">
                    <h4>Safe, insured and secured.</h4>
                    <p>Our assets are held with reputable firms, who ensure that investors’ funds are securely held.</p>
                </div>
            </div>
            <div class="row reveal no-pad">
                <div class="col-lg-4 col-md-8 offset-md-2 col-10 offset-1 offset-lg-4 pt-4">
                    <div class="row">
                        <div class="col-md-4 col-6 text-center">
                            <div class=" d-flex justify-content-center">
                                <img src="<?php echo base_url();?>mainasset/fe/images/icons/fund.svg" alt="funds icon">
                            </div>

                            <span>Funds Secured</span>
                        </div>
                        <div class="col-md-4 col-6 text-center">
                            <div class=" d-flex justify-content-center">
                                <img src="<?php echo base_url();?>mainasset/fe/images/icons/data.svg" alt="data icon">
                            </div>
                            <span>Data Protected</span>
                        </div>
                        <div class="col-md-4 offset-3 offset-md-0 col-6 pt-4 pt-md-0 text-center">
                            <div class=" d-flex justify-content-center">
                                <img src="<?php echo base_url();?>mainasset/fe/images/icons/investment.svg" alt="investment icon">
                            </div>
                            <span>Investment Insured</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row partners-border reveal">
            <div class="col-lg-8 offset-lg-2 text-center partners">
                <h4 class="mobile">Trusted and backed by renowned partners </h4>
                <p class="px-md-5 p-color">Our partners are an integral part of our business. <br>We value their contributions
                    to our growth and development.</p>
                <div class="row px-lg-4 mt-md-5 no-pad">
                    <div class="col-lg-4 offset-lg-4 col-sm-6 offset-sm-3">
                        <div class="row no-pad">
                            <div class="col-6 w-100 d-flex align-items-center justify-content-center">
                                <a href="https://www.leadway.com/" target="_blank">
                                    <img src="<?php echo base_url();?>mainasset/fe/images/leadway.png" alt="Leadway logo">
                                </a>
                            </div>
                            <div class=" col-6 w-100 d-flex align-items-center justify-content-center">
                                <a href="https://paystack.com/" target="_blank">
                                    <img src="<?php echo base_url();?>mainasset/fe/images/paystack.png" alt="Paystack logo">
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-md-3 col-6 w-100 d-flex align-items-center justify-content-center">
                                <a href="#" target="_blank" rel="noopener noreferrer">
                                    <img src="images/gsma.png" alt="GSMA logo">
                                </a>
                            </div>
                            <div class="col-md-3 col-6 w-100 d-flex align-items-center justify-content-center">
                                <a href="#" target="_blank">
                                    <img src="images/guinness.png" alt="Guinness logo">
                                </a>
                            </div>
                            <div class="col-md-3 col-6 w-100 d-flex align-items-center justify-content-center">
                                <a href="#" target="_blank">
                                    <img src="images/afex.png" alt="Afex logo">
                                </a>
                            </div>
                            <div class="col-md-3 col-6 w-100 d-flex align-items-center justify-content-center">
                                <a href="#" target="_blank">
                                    <img src="images/leadway.png" alt="Leadway logo">
                                </a>
                            </div>
                            <div class="col-md-3 col-6 w-100 d-flex align-items-center justify-content-center">
                                <a href="#" target="_blank">
                                    <img src="images/paystack.png" alt="Paystack logo">
                                </a>
                            </div>
                            <div class="col-md-3 col-6 w-100 d-flex align-items-center justify-content-center">
                                <a href="#" target="_blank">
                                    <img src="images/nirsal.png" alt="Nirsal logo">
                                </a>
                            </div>
                            <div class="col-md-3 col-6 w-100 d-flex align-items-center justify-content-center">
                                <a href="#" target="_blank">
                                    <img src="images/natnudo.png" alt="Natnudo logo">
                                </a>
                            </div>
                            <div class="col-md-3 col-6 w-100 d-flex align-items-center justify-content-center">
                                <a href="#" target="_blank">
                                    <img src="images/fc.png" alt="Farmcrowdy logo">
                                </a>
                            </div> -->
                </div>
            </div>
        </div>
    </div>
    <div class="row blog reveal">
        <div class="col-md-10 col-12 offset-md-1">
            <div class="row">
                <div class="col-12 text-center">
                    <h4 class="mobile">From our blog</h4>
                    <p class="no1">Stay informed on the curcial updates of FarmGate Africa and our industry.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-10 col-md-12 offset-1 mb-4 offset-md-0 pr-lg-4">
                    <div class="row blog-border no-pad">
                        <div class="col-md-4 col-12 pl-0">
                            <img src="https://cdn-images-1.medium.com/max/1600/1*Numh1vtlZrimp2x2CgY-Xw.jpeg" class="img-fluid w-100" alt="">
                        </div>
                        <div class="col-md-8 col-12 details-container">
                            <h4>FarmGate Africa’s Journey to 2030</h4>
                            <span>10th November 2018</span>
                            <p data-maxlength="110"> <span class="paragraph p">According to reports obtained from the World Bank, fewer people around the world are living
                                in extreme poverty. While this may be good progress,</span>
                                <a href="https://medium.com/@farmgate.africa/farmgate-africas-journey-to-2030-3ada2569340d" target="_blank">Continue
                                    reading...</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-10 offset-1 col-md-12 mb-4 offset-md-0 pl-lg-4">
                    <div class="row blog-border no-pad">
                        <div class="col-md-4 col-12 pl-0">
                            <img src="https://cdn-images-1.medium.com/max/1600/0*-it-tqe81OgfB5hT" class="img-fluid w-100" alt="">
                        </div>
                        <div class="col-12 col-md-8 details-container">
                            <h4>FarmGate Africa: Improving market access and productivity for African farmers.</h4>
                            <span>10th November 2018</span>
                            <p data-maxlength="110"> <span class="paragraph p">Agriculture is the highest employer of labour in Nigeria. For the past decade, unemployment
                                rates have surged in Nigeria, putting something short of a </span><a href="https://medium.com/@farmgate.africa/farmgate-africa-improving-market-access-and-productivity-for-african-farmers-e4b5445d76b4"
                                    target="_blank">Continue reading...</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 text-center more-posts">
                <a href="https://medium.com/@farmgate.africa/" target="_blank">More updates available here</a>
            </div>
        </div>
    </div>

</div>
<div class="container-fluid reveal">
    <div class="row subscribe-bg">
        <div class="col-10 offset-1 offset-md-0 col-md-12 text-center">
            <h4 class="mobile">Get notified when commodities are available to trade </h4>
        </div>
        <form class="form-inline  col-md-6 offset-md-3 col-10 col-sm-8 offset-sm-2 offset-1 col-lg-4 offset-lg-4">
            <div class="input-group w-100 pad-sub mb-3">
                <input type="email" class="form-control email-subscribe" placeholder="Enter your email" aria-label="email" aria-describedby="button-addon2">
                <div class="input-group-append">
                    <button class="btn " type="submit" id="button-addon2">Sign up</button>
                </div>
            </div>
        </form>

    </div>
</div>