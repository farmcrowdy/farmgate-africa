
    <!-- ============body================= -->
    <div class="container-fluid body-bg">
        <div class="row">
            <div class="col-12 text-center">
                <p class="title">Access to African Commodities Marketplace</p>
                <p class="sub-title">Platform tailored for agro-commodity exchange</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1  search-main-top">
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-0 dropdown">
                        <button type="button" class="btn btn-lg btn-green dropdown-toggle pull-mobile" style="border:1px solid red;" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true">
                            <i class="far fa-list-alt"></i> Farm Categories</button>
                        <div class="dropdown-menu dropdown-width no-padding" aria-labelledby="dropdownMenuButton">
                            <div class="container">
                                <div class="row background">
                                    <div class="col-6 category-border">
                                        <div class="overflow my-2">
                                          <?php foreach ($categories as $key => $cat) {?>
                                            <a class="dropdown-item" href="<?php echo base_url('products/category/').$cat->c_id ; ?>"><?php echo $cat->c_name; ?></a>
                                          <?php } ?>
                                        </div>
                                    </div>
                                    <div class="col-6">
<!--                                         <div class="overflow my-2">
                                            <a class="dropdown-item" href="#">Maize</a>
                                            <a class="dropdown-item" href="#">Rice</a>
                                            <a class="dropdown-item" href="#">Beans</a>
                                            <a class="dropdown-item" href="#">Beans</a>
                                            <a class="dropdown-item" href="#">Beans</a>
                                            <a class="dropdown-item" href="#">Beans</a>
                                            <a class="dropdown-item" href="#">Beans</a>
                                            <a class="dropdown-item" href="#">Beans</a>
                                            <a class="dropdown-item" href="#">Beans</a>
                                            <a class="dropdown-item" href="#">Beans</a>
                                            <a class="dropdown-item" href="#">Beans</a>
                                            <a class="dropdown-item" href="#">Poultry</a>
                                            <a class="dropdown-item" href="#">Cassava</a>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=" offset-sm-2 col-sm-8 offset-md-0 offset-lg-1  col-12">
                        <form class="search-field">
                            <div class="input-group">
                                <!-- <div class="input-group-prepend">
                                    <div class="input-group-text radio-group">
                                        <input class="input" id="type-buy" type="radio" name="type" value="2" checked="checked">
                                        <label class="label label1" for="type-buy">BUY</label>
                                        <input class="input" id="type-sell" type="radio" name="type" value="1">
                                        <label class="label label2" for="type-sell">SELL</label>
                                    </div>
                                </div> -->
                                <input type="text" class="form-control" placeholder="What are you looking for?" aria-label="Text input with radio button">
                                <div class="input-group-append">
                                    <button class="btn btn-lg search-btn">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Contact manager modal -->
    <div class="modal fade contact-manager-modal mt-5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg  ">
            <form class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title pl-md-5 ml-md-3" id="changePasswordLabel">Contact Manager</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body container mt-3">
                    <div class="row py-2">
                        <div class="col-12 col-md-10 offset-md-1">
                            <label for="">Subject</label>
                            <input type="text" class="form-control" id="" aria-describedby="Volume" placeholder="">
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-12 col-md-10 offset-md-1">
                            <label for="">Category</label>
                            <select class="form-control" id="formControlSelect">
                                <option>Client</option>
                                <option>General</option>
                                <option>Issues</option>
                                <option>Suggestions</option>
                            </select>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-12 col-md-10 offset-md-1">
                            <label for="">Describe the question in detail</label>
                            <textarea class="form-control" rows="5" aria-label="With textarea"></textarea>

                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-12 col-md-10 offset-md-1">
                            <p class="profile-file-paceholder mb-0 pb-0 pl-3 dashboard-activity-body"></p>
                            <label for="upload-file-for-manager" class="btn btn-default manager-verify-link" style="color:#888888">
                                <i class="fas fa-upload"></i> Add files</label>
                            <input id="upload-file-for-manager" name="pic" accept="" type="file" class="hidden">
                        </div>
                        <script>
                            $("#upload-file-for-manager").change(function () {
                                $(".profile-file-paceholder").text(this.files[0].name);
                            });
                        </script>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-secondary px-4 py-2" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-profile py-2 px-4">Submit</button>
                </div>
            </form>
        </div>
    </div>

    <!-- carousel -->
    <div class="container-fluid" style="background: #fefefe;">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <p class="section-title-black text-center">Recently added products</p>
                <div class="row">
                    <div class="col-lg-1 carousel-btn">
                        <i class="fas fa-chevron-left owl-prev fa-3x"></i>
                    </div>
                    <div class="col-lg-10">
                        <div class="owl-carousel loop owl-theme owl-loaded" id="owl-main">
                            <div class="owl-stage-outer">
                                <div class="owl-stage mb-4">
                                  <?php foreach ($products as $key => $product) {?>
                                    <div class="owl-item carousel-item-bg">
                                        <a href="<?php echo base_url('product/').$product->prod_slug; ?>">
                                            <img src="<?php echo base_url();?>mainasset/fe/images/products/<?php echo $product->prod_image ;?>" alt="" class="img-fluid">
                                            <p class="text-center carousel-main-title mb-0 pb-0 pt-2"><?php echo $product->prod_name;?></p>
                                            <p class="text-center state">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="11" height="13" viewBox="0 0 11 13">
                                                    <image width="11" height="13" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAANCAMAAABIK2QJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAtFBMVEX///+suxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxYAAAClajiyAAAAOnRSTlMAM5zW48uCG1vt1C848+XJ9Mc8vCcLRf6ixvxJi7/9ZaXBf2Y+ifeEGtKZHD31zBF88Qd6MukJcuo0qxBELQAAAAFiS0dEOzkO9GwAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfiBhQMHCFkZ5EmAAAAeklEQVQI1y2NCQ6CAAwEV8QTRREPQEQRvAVPVPb/D7MVJ2k6STdbADWjbjaaLQjtDpWuBfT6pD0Yks4IBumOJ1OTMw8+g7kkQzlgwWgpviJjrCWepJuI2x32BykJZI4n4Jz9OvOLPriq3lJV3F3xByqeBePX3/H+lLq+wxgQO5e/nRUAAAAASUVORK5CYII="
                                                    />
                                                </svg> <?php echo $product->state ;?></p>
                                            <p class="text-center price py-2">&#8358;<?php echo number_format($product->prod_price_per_unit); ?> /
                                                <span class="small"><?php echo $product->unit;?></span>
                                            </p>
                                        </a>
                                    </div>
                                  <?php } ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1 carousel-btn text-right">
                        <i class="fas fa-chevron-right owl-next fa-3x"></i>
                    </div>
                    <div class="col-12 text-center mt-3 mb-5">
                        <a href="<?php echo base_url('products');?>" class="see-all-products-link">SEE ALL PRODUCTS</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- =========Statistics======== -->
    <div class="container-fluid" style="background:#FAFAFA;">
        <div class="row">
            <div class="col-sm-10 offset-sm-1 col-lg-8 offset-lg-2">
                <p class="section-title-black text-center">Current Statistics</p>
                <div class="row text-center pb-5">
                    <div class="col-md-3 col-6 icon-resize">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate" viewBox="0 0 80 100"
                            width="80" height="100">
                            <defs>
                                <clipPath id="_clipPath_Tz3oQv2sXIPoyXf7ryzkvwQ8qJNKhL70">
                                    <rect width="80" height="100" />
                                </clipPath>
                            </defs>
                            <g clip-path="url(#_clipPath_Tz3oQv2sXIPoyXf7ryzkvwQ8qJNKhL70)">
                                <clipPath id="_clipPath_wRnOw1r5PwkKOZSyjX3rpwVfSnZqCvwO">
                                    <rect x="-1" y="0" width="80" height="100" transform="matrix(1,0,0,1,0,0)" fill="rgb(255,255,255)" />
                                </clipPath>
                                <g clip-path="url(#_clipPath_wRnOw1r5PwkKOZSyjX3rpwVfSnZqCvwO)">
                                    <g id="Group">
                                        <clipPath id="_clipPath_bhTQUkEgPRSjiBiVAOxT8asKsPmM9ayd">
                                            <rect x="2" y="10" width="72" height="90" transform="matrix(1,0,0,1,0,0)" fill="rgb(255,255,255)" />
                                        </clipPath>
                                        <g clip-path="url(#_clipPath_bhTQUkEgPRSjiBiVAOxT8asKsPmM9ayd)">
                                            <g id="Group">
                                                <g id="Group">
                                                    <g id="mtssold">
                                                        <path d=" M 2 64 C 2 44.131 18.131 28 38 28 C 57.869 28 74 44.131 74 64 C 74 83.869 57.869 100 38 100 C 18.131 100 2 83.869 2 64 Z "
                                                            fill="rgb(172,187,22)" fill-opacity="0.13" />
                                                        <g id="sack">
                                                            <g id="Group">
                                                                <path d=" M 56.092 31.117 C 47.004 17.858 42.974 14.151 38 14.151 C 33.026 14.151 28.996 17.858 19.908 31.117 L 56.092 31.117 Z  M 47.075 25.987 C 47.802 25.987 48.394 26.578 48.394 27.306 C 48.394 28.034 47.802 28.625 47.075 28.625 C 46.347 28.625 45.754 28.034 45.754 27.306 C 45.754 26.578 46.347 25.987 47.075 25.987 Z  M 43.774 21.037 C 44.503 21.037 45.095 21.629 45.095 22.357 C 45.095 23.084 44.503 23.676 43.774 23.676 C 43.046 23.676 42.455 23.084 42.455 22.357 C 42.455 21.629 43.046 21.037 43.774 21.037 Z  M 30.576 24.666 C 31.303 24.666 31.895 25.259 31.895 25.987 C 31.895 26.714 31.303 27.306 30.576 27.306 C 29.848 27.306 29.256 26.714 29.256 25.987 C 29.256 25.259 29.848 24.666 30.576 24.666 Z "
                                                                    fill="rgb(255,230,184)" />
                                                                <path d=" M 47.075 28.625 C 47.802 28.625 48.394 28.034 48.394 27.306 C 48.394 26.578 47.802 25.987 47.075 25.987 C 46.347 25.987 45.754 26.578 45.754 27.306 C 45.754 28.034 46.347 28.625 47.075 28.625 Z "
                                                                    fill="rgb(77,61,54)" />
                                                                <path d=" M 43.774 23.676 C 44.503 23.676 45.095 23.084 45.095 22.357 C 45.095 21.629 44.503 21.037 43.774 21.037 C 43.046 21.037 42.455 21.629 42.455 22.357 C 42.455 23.084 43.046 23.676 43.774 23.676 Z "
                                                                    fill="rgb(77,61,54)" />
                                                                <path d=" M 30.576 27.306 C 31.303 27.306 31.895 26.714 31.895 25.987 C 31.895 25.259 31.303 24.666 30.576 24.666 C 29.848 24.666 29.256 25.259 29.256 25.987 C 29.256 26.714 29.848 27.306 30.576 27.306 Z "
                                                                    fill="rgb(77,61,54)" />
                                                                <path d=" M 64.516 80.957 L 62.368 78.809 C 62.146 78.587 62.017 78.289 62.006 77.976 L 60.909 46.428 L 23.796 46.428 C 23.112 46.428 22.559 45.875 22.559 45.192 L 20.084 45.192 C 20.084 45.875 19.53 46.428 18.846 46.428 L 15.092 46.428 L 13.995 77.976 C 13.984 78.289 13.855 78.587 13.633 78.809 L 11.484 80.957 C 10.299 82.142 10.299 84.072 11.485 85.257 C 12.059 85.831 12.823 86.148 13.635 86.148 C 14.447 86.148 15.211 85.831 15.784 85.257 L 16.568 84.474 C 16.887 84.155 17.352 84.035 17.786 84.16 C 23.308 85.754 31.054 86.744 38 86.744 C 44.946 86.744 52.692 85.754 58.214 84.16 C 58.648 84.035 59.114 84.155 59.433 84.474 L 60.216 85.257 C 60.79 85.831 61.553 86.148 62.366 86.148 C 63.178 86.148 63.942 85.831 64.516 85.257 C 65.701 84.072 65.701 82.142 64.516 80.957 Z  M 55.707 55.327 L 50.1 61.101 C 49.867 61.341 49.546 61.476 49.212 61.476 C 48.877 61.476 48.557 61.341 48.324 61.101 L 43.605 56.241 L 38.887 61.101 C 38.653 61.341 38.333 61.476 37.999 61.476 C 37.664 61.476 37.344 61.341 37.111 61.101 L 32.393 56.241 L 27.674 61.101 C 27.441 61.341 27.121 61.476 26.787 61.476 C 26.452 61.476 26.132 61.341 25.899 61.101 L 20.293 55.327 C 19.816 54.836 19.828 54.053 20.319 53.577 C 20.809 53.101 21.592 53.113 22.068 53.603 L 26.786 58.463 L 31.505 53.603 C 31.738 53.363 32.058 53.227 32.392 53.227 C 32.727 53.227 33.047 53.363 33.28 53.603 L 37.998 58.463 L 42.717 53.603 C 42.95 53.363 43.27 53.227 43.605 53.227 C 43.939 53.227 44.26 53.363 44.493 53.603 L 49.212 58.463 L 53.931 53.603 C 54.407 53.113 55.191 53.101 55.681 53.577 C 56.171 54.053 56.183 54.836 55.707 55.327 Z "
                                                                    fill="rgb(252,219,90)" />
                                                                <path d=" M 66.265 79.207 L 64.462 77.403 L 63.378 46.244 C 65.246 45.692 66.613 43.962 66.613 41.919 L 66.613 35.627 C 66.613 33.14 64.59 31.117 62.103 31.117 L 59.087 31.117 C 48.744 15.873 44.167 11.676 38 11.676 C 31.833 11.676 27.256 15.873 16.913 31.117 L 13.897 31.117 C 11.41 31.117 9.387 33.14 9.387 35.627 L 9.387 41.919 C 9.387 43.963 10.754 45.693 12.622 46.244 L 11.538 77.403 L 9.735 79.207 C 7.584 81.357 7.584 84.857 9.735 87.008 C 10.776 88.049 12.161 88.623 13.634 88.623 C 15.108 88.623 16.493 88.049 17.535 87.008 L 17.806 86.736 C 23.477 88.273 31.128 89.219 38 89.219 C 44.872 89.219 52.523 88.273 58.194 86.736 L 58.465 87.007 C 59.507 88.049 60.892 88.622 62.366 88.622 C 63.839 88.622 65.224 88.049 66.265 87.007 C 68.416 84.857 68.416 81.357 66.265 79.207 Z  M 60.216 85.257 L 59.433 84.474 C 59.114 84.155 58.648 84.035 58.214 84.16 C 52.692 85.754 44.946 86.744 38 86.744 C 31.054 86.744 23.308 85.754 17.786 84.16 C 17.352 84.035 16.887 84.155 16.568 84.474 L 15.784 85.257 C 15.21 85.831 14.447 86.148 13.634 86.148 C 12.823 86.148 12.058 85.831 11.484 85.257 C 10.299 84.072 10.299 82.142 11.484 80.957 L 13.633 78.809 C 13.855 78.587 13.984 78.289 13.994 77.976 L 15.092 46.428 L 18.846 46.428 C 19.53 46.428 20.084 45.875 20.084 45.192 C 20.084 44.508 19.53 43.954 18.846 43.954 L 13.897 43.954 C 12.775 43.954 11.862 43.042 11.862 41.919 L 11.862 35.627 C 11.862 34.505 12.775 33.592 13.897 33.592 L 62.103 33.592 C 63.225 33.592 64.138 34.505 64.138 35.627 L 64.138 41.919 C 64.138 43.042 63.225 43.954 62.103 43.954 L 23.796 43.954 C 23.112 43.954 22.559 44.508 22.559 45.192 C 22.559 45.875 23.112 46.428 23.796 46.428 L 60.909 46.428 L 62.006 77.976 C 62.017 78.289 62.146 78.587 62.368 78.809 L 64.516 80.957 C 65.701 82.142 65.701 84.072 64.516 85.257 C 63.942 85.831 63.178 86.148 62.366 86.148 C 61.553 86.148 60.79 85.831 60.216 85.257 Z  M 38 14.151 C 42.974 14.151 47.004 17.858 56.092 31.117 L 19.908 31.117 C 28.996 17.858 33.026 14.151 38 14.151 Z "
                                                                    fill="rgb(70,81,81)" />
                                                                <path d=" M 53.932 53.603 L 49.212 58.463 L 44.494 53.603 C 44.26 53.363 43.94 53.227 43.605 53.227 C 43.271 53.227 42.951 53.363 42.718 53.603 L 37.999 58.463 L 33.28 53.603 C 33.047 53.363 32.728 53.227 32.393 53.227 C 32.058 53.227 31.738 53.363 31.505 53.603 L 26.787 58.463 L 22.068 53.603 C 21.592 53.113 20.809 53.101 20.319 53.577 C 19.829 54.053 19.817 54.836 20.293 55.327 L 25.899 61.101 C 26.132 61.341 26.453 61.476 26.787 61.476 C 27.122 61.476 27.442 61.341 27.674 61.101 L 32.393 56.241 L 37.111 61.101 C 37.345 61.341 37.665 61.476 37.999 61.476 C 38.334 61.476 38.653 61.341 38.887 61.101 L 43.605 56.241 L 48.325 61.101 C 48.558 61.341 48.878 61.476 49.212 61.476 C 49.547 61.476 49.867 61.341 50.101 61.101 L 55.708 55.327 C 56.184 54.836 56.172 54.053 55.681 53.577 C 55.191 53.101 54.408 53.113 53.932 53.603 Z "
                                                                    fill="rgb(77,61,54)" />
                                                                <path d=" M 22.559 45.192 C 22.559 44.508 23.112 43.954 23.796 43.954 L 62.103 43.954 C 63.225 43.954 64.138 43.041 64.138 41.919 L 64.138 35.627 C 64.138 34.505 63.225 33.592 62.103 33.592 L 13.897 33.592 C 12.775 33.592 11.862 34.505 11.862 35.627 L 11.862 41.919 C 11.862 43.041 12.775 43.954 13.897 43.954 L 18.846 43.954 C 19.53 43.954 20.084 44.508 20.084 45.192 L 22.559 45.192 Z "
                                                                    fill="rgb(255,173,97)" />
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        <p class="mb-0 statistics">320,000&plus;</p>
                        <p class="statistics-sub">Metric Tonnes Sold</p>
                    </div>
                    <div class="col-md-3 col-6 icon-resize">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate" viewBox="0 0 80 100"
                            width="80" height="100">
                            <defs>
                                <clipPath id="_clipPath_cQZqRqKViTwXvabLuHvDOy9FyaoaePgM">
                                    <rect width="80" height="100" />
                                </clipPath>
                            </defs>
                            <g clip-path="url(#_clipPath_cQZqRqKViTwXvabLuHvDOy9FyaoaePgM)">
                                <g id="transactions">
                                    <path d=" M 4.332 64.087 C 4.332 44.401 20.314 28.419 40 28.419 C 59.686 28.419 75.668 44.401 75.668 64.087 C 75.668 83.772 59.686 99.755 40 99.755 C 20.314 99.755 4.332 83.772 4.332 64.087 Z "
                                        fill="rgb(172,187,22)" fill-opacity="0.13" />
                                    <g id="money-bag">
                                        <path d=" M 73.163 70.901 C 73.225 70.586 73.333 70.28 73.333 69.954 C 73.333 64.194 68.986 56.749 67.842 54.89 C 67.788 42.359 65.562 33.223 56.406 30.099 C 56.823 28.794 57.577 27.055 58.278 25.452 C 59.622 22.376 60.892 19.471 60.892 17.542 C 60.892 14.905 59.255 12.245 55.594 12.245 C 53.143 12.245 52.231 13.403 51.735 14.395 C 51.709 14.366 51.683 14.335 51.658 14.308 C 50.976 13.536 49.836 12.245 47.122 12.245 C 44.071 12.245 42.09 14.903 41.13 17.081 C 39.891 17.143 38.091 17.437 36.917 18.528 C 36.17 19.223 35.775 20.138 35.775 21.173 C 35.775 22.905 37.02 24.905 38.336 27.021 C 38.991 28.073 39.679 29.201 40.117 30.155 C 31.086 33.328 28.878 42.427 28.825 54.892 C 28.277 55.785 26.983 58.019 25.785 60.704 C 23.127 60.937 20.83 61.457 19.383 61.855 C 18.881 60.11 17.456 58.703 15.568 58.326 L 1.994 55.612 C 1.506 55.515 0.996 55.641 0.61 55.956 C 0.224 56.274 0 56.746 0 57.245 L 0 83.912 C 0 84.834 0.745 85.579 1.667 85.579 L 12.109 85.579 C 14.166 85.579 15.919 84.269 16.674 82.431 C 20.404 84.209 37.665 92.245 45 92.245 C 51.465 92.245 64.771 85.704 73.574 81.376 C 75.872 80.247 77.772 79.313 79.004 78.772 C 79.609 78.505 80 77.906 80 77.245 C 80 74.076 77.294 71.707 73.163 70.901 Z "
                                            fill="rgb(64,76,76)" />
                                        <g id="Group">
                                            <path d=" M 41.167 25.26 C 40.298 23.863 39.108 21.953 39.186 20.971 C 39.681 20.511 41.236 20.361 42.165 20.414 C 42.939 20.509 43.649 19.978 43.876 19.239 C 44.17 18.29 45.396 15.579 47.123 15.579 C 48.334 15.579 48.644 15.932 49.16 16.516 C 49.683 17.107 50.472 17.999 51.963 17.999 C 53.819 17.999 54.421 16.576 54.647 16.041 C 54.699 15.919 54.777 15.733 54.811 15.694 C 54.813 15.693 54.989 15.579 55.594 15.579 C 56.131 15.579 57.559 15.579 57.559 17.542 C 57.559 18.774 56.265 21.736 55.225 24.117 C 54.401 26.001 53.629 27.794 53.172 29.305 C 51.696 29.059 50.104 28.912 48.334 28.912 C 46.518 28.912 44.884 29.063 43.378 29.321 C 42.868 28.029 42.047 26.675 41.167 25.26 Z "
                                                fill="rgb(215,204,200)" />
                                            <path d=" M 32.157 55.37 C 32.157 38.297 36.39 32.245 48.333 32.245 C 60.277 32.245 64.51 38.297 64.51 55.37 C 64.51 55.691 64.603 56.005 64.775 56.274 C 64.827 56.355 70 64.446 70 69.954 C 70 70.168 69.958 70.376 69.876 70.584 C 66.581 70.626 59.96 72.403 56.58 73.374 C 56.127 68.591 49.999 66.898 43.665 65.612 C 40.775 65.026 38.625 63.989 36.544 62.986 C 34.296 61.9 32.125 60.917 29.461 60.679 C 30.695 58.151 31.876 56.295 31.891 56.272 C 32.064 56.004 32.157 55.69 32.157 55.37 Z "
                                                fill="rgb(215,204,200)" />
                                            <path d=" M 48.678 28.912 C 48.626 28.912 48.582 28.92 48.531 28.92 C 48.947 28.923 49.333 28.951 49.731 28.97 C 49.382 28.948 49.046 28.912 48.678 28.912 Z "
                                                fill="rgb(215,204,200)" />
                                        </g>
                                        <g id="Group" style="opacity:0.1;">
                                            <g opacity="0.1">
                                                <path d=" M 55.594 15.579 C 54.989 15.579 54.813 15.693 54.811 15.694 C 54.808 15.698 54.802 15.713 54.799 15.719 C 55.211 15.892 55.628 16.319 55.628 17.542 C 55.628 18.774 54.653 21.736 53.869 24.117 C 53.262 25.961 52.698 27.711 52.351 29.203 C 52.621 29.24 52.909 29.262 53.172 29.305 C 53.629 27.794 54.401 26.001 55.225 24.117 C 56.265 21.736 57.559 18.774 57.559 17.542 C 57.559 15.579 56.131 15.579 55.594 15.579 Z "
                                                    fill="rgb(0,0,0)" />
                                            </g>
                                        </g>
                                        <g id="Group">
                                            <g id="Group" style="opacity:0.2;">
                                                <g opacity="0.2">
                                                    <path d=" M 43.279 25.26 C 42.625 23.863 41.729 21.953 41.787 20.971 C 42.16 20.511 43.331 20.361 44.031 20.414 C 44.615 20.509 45.149 19.978 45.32 19.239 C 45.532 18.331 46.391 15.835 47.604 15.62 C 47.452 15.602 47.315 15.579 47.122 15.579 C 45.396 15.579 44.17 18.29 43.875 19.239 C 43.649 19.978 42.939 20.509 42.165 20.414 C 41.235 20.361 39.681 20.511 39.186 20.971 C 39.108 21.953 40.298 23.863 41.167 25.26 C 42.047 26.675 42.868 28.03 43.378 29.322 C 43.856 29.24 44.371 29.189 44.875 29.13 C 44.488 27.895 43.91 26.606 43.279 25.26 Z "
                                                        fill="rgb(255,255,255)" />
                                                </g>
                                            </g>
                                            <g id="Group" style="opacity:0.2;">
                                                <g opacity="0.2">
                                                    <path d=" M 34.825 58.534 C 34.969 58.24 35.047 57.896 35.047 57.547 C 35.047 38.931 38.563 32.296 48.464 32.25 C 48.418 32.25 48.378 32.245 48.333 32.245 C 36.39 32.245 32.157 38.297 32.157 55.37 C 32.157 55.69 32.064 56.004 31.891 56.272 C 31.876 56.295 30.695 58.151 29.461 60.679 C 30.914 60.809 32.215 61.17 33.467 61.639 C 34.215 59.797 34.815 58.553 34.825 58.534 Z "
                                                        fill="rgb(255,255,255)" />
                                                </g>
                                            </g>
                                        </g>
                                        <g id="Group" style="opacity:0.1;">
                                            <g opacity="0.1">
                                                <path d=" M 64.775 56.274 C 64.603 56.005 64.51 55.691 64.51 55.37 C 64.51 38.781 60.492 32.616 49.309 32.278 C 58.706 32.617 62.08 39.352 62.08 57.547 C 62.08 57.898 62.157 58.242 62.301 58.536 C 62.339 58.613 65.591 65.281 66.455 70.981 C 67.756 70.752 68.953 70.596 69.876 70.584 C 69.958 70.376 70 70.168 70 69.954 C 70 64.446 64.827 56.355 64.775 56.274 Z "
                                                    fill="rgb(0,0,0)" />
                                            </g>
                                        </g>
                                        <path d=" M 48.333 55.579 L 45 55.579 C 44.079 55.579 43.333 56.324 43.333 57.245 C 43.333 58.167 44.079 58.912 45 58.912 L 46.667 58.912 C 46.667 59.833 47.412 60.579 48.334 60.579 C 49.255 60.579 50 59.833 50 58.912 L 50 58.605 C 51.936 57.915 53.333 56.082 53.333 53.912 C 53.333 51.155 51.091 48.912 48.333 48.912 C 47.414 48.912 46.667 48.165 46.667 47.245 C 46.667 46.326 47.414 45.579 48.333 45.579 L 51.666 45.579 C 52.588 45.579 53.333 44.833 53.333 43.912 C 53.333 42.991 52.588 42.245 51.667 42.245 L 50 42.245 C 50 41.324 49.255 40.579 48.333 40.579 C 47.412 40.579 46.667 41.324 46.667 42.245 L 46.667 42.552 C 44.731 43.243 43.333 45.076 43.333 47.245 C 43.333 50.003 45.576 52.245 48.333 52.245 C 49.253 52.245 50 52.993 50 53.912 C 50 54.832 49.253 55.579 48.333 55.579 Z "
                                            fill="rgb(41,55,55)" />
                                        <path d=" M 72.109 78.382 C 64.098 82.32 50.69 88.912 45 88.912 C 38.939 88.912 23.017 81.814 17.396 79.08 C 17.389 79.077 17.383 79.078 17.376 79.075 L 19.337 65.34 C 20.552 64.96 24.262 63.912 28.333 63.912 C 30.794 63.912 32.692 64.829 35.098 65.987 C 37.256 67.029 39.704 68.209 43.001 68.879 C 50.244 70.348 53.333 71.853 53.333 73.912 C 53.333 74.319 53.21 74.57 52.894 74.806 C 51.832 75.598 47.695 76.93 32.142 72.314 C 31.246 72.057 30.329 72.556 30.068 73.439 C 29.808 74.321 30.309 75.248 31.191 75.51 C 38.87 77.786 44.739 78.92 48.9 78.92 C 51.611 78.92 53.6 78.438 54.886 77.476 C 54.997 77.394 55.386 77.205 55.491 77.172 C 58.438 76.266 66.787 73.912 70 73.912 C 72.718 73.912 75.579 74.737 76.419 76.289 C 75.257 76.835 73.789 77.558 72.109 78.382 Z "
                                            fill="rgb(187,155,130)" />
                                        <linearGradient id="_lgradient_0" x1="0%" y1="50%" x2="70.71050419375361%" y2="120.7108520431281%">
                                            <stop offset="2.095486756349756%" stop-opacity="0.1" style="stop-color:rgb(0,0,0)" />
                                            <stop offset="100%" stop-opacity="2.992898254783327e-7" style="stop-color:rgb(0,0,0)" />
                                        </linearGradient>
                                        <path d=" M 19.285 65.701 L 17.376 79.075 C 17.383 79.078 17.389 79.077 17.396 79.08 C 22.282 81.456 34.942 87.124 42.152 88.568 L 19.285 65.701 Z "
                                            fill="url(#_lgradient_0)" />
                                        <path d=" M 12.092 82.245 L 3.316 82.245 L 3.316 59.278 L 14.898 61.594 C 15.764 61.768 16.347 62.589 16.22 63.464 L 13.742 80.814 C 13.625 81.63 12.916 82.245 12.092 82.245 Z "
                                            fill="rgb(252,219,90)" />
                                        <g id="Group" style="opacity:0.1;">
                                            <g opacity="0.1">
                                                <path d=" M 3.316 70.578 L 3.316 82.245 L 12.092 82.245 C 12.916 82.245 13.625 81.63 13.743 80.814 L 15.204 70.578 L 3.316 70.578 L 3.316 70.578 Z "
                                                    fill="rgb(0,0,0)" />
                                            </g>
                                        </g>
                                        <linearGradient id="_lgradient_1" x1="0%" y1="50%" x2="90.63335529227831%" y2="92.25630022214024%">
                                            <stop offset="11.14699436845368%" stop-opacity="0.2" style="stop-color:rgb(255,255,255)" />
                                            <stop offset="100%" stop-opacity="-0.01722144786972715" style="stop-color:rgb(255,255,255)" />
                                        </linearGradient>
                                        <path d=" M 73.163 70.901 C 73.225 70.586 73.333 70.28 73.333 69.954 C 73.333 64.194 68.986 56.749 67.842 54.89 C 67.788 42.359 65.562 33.223 56.406 30.099 C 56.823 28.794 57.577 27.055 58.278 25.452 C 59.622 22.376 60.892 19.471 60.892 17.542 C 60.892 14.905 59.255 12.245 55.594 12.245 C 53.143 12.245 52.231 13.403 51.735 14.395 C 51.709 14.366 51.683 14.335 51.658 14.308 C 50.976 13.536 49.836 12.245 47.122 12.245 C 44.071 12.245 42.09 14.903 41.13 17.081 C 39.891 17.143 38.091 17.437 36.917 18.528 C 36.17 19.223 35.775 20.138 35.775 21.173 C 35.775 22.905 37.02 24.905 38.336 27.021 C 38.991 28.073 39.679 29.201 40.117 30.155 C 31.086 33.328 28.878 42.427 28.825 54.892 C 28.277 55.785 26.983 58.019 25.785 60.704 C 23.127 60.937 20.83 61.457 19.383 61.855 C 18.881 60.11 17.456 58.703 15.568 58.326 L 1.994 55.612 C 1.506 55.515 0.996 55.641 0.61 55.956 C 0.224 56.274 0 56.746 0 57.245 L 0 83.912 C 0 84.834 0.745 85.579 1.667 85.579 L 12.109 85.579 C 14.166 85.579 15.919 84.269 16.674 82.431 C 20.404 84.209 37.665 92.245 45 92.245 C 51.465 92.245 64.771 85.704 73.574 81.376 C 75.872 80.247 77.772 79.313 79.004 78.772 C 79.609 78.505 80 77.906 80 77.245 C 80 74.076 77.294 71.707 73.163 70.901 Z "
                                            fill="url(#_lgradient_1)" vector-effect="non-scaling-stroke" stroke-width="0.289"
                                            stroke="rgb(77,61,54)" stroke-opacity="100" stroke-linejoin="miter" stroke-linecap="butt"
                                            stroke-miterlimit="4" />
                                    </g>
                                </g>
                            </g>
                        </svg>
                        <p class="mb-0 statistics">&dollar;320,000&plus;</p>
                        <p class="statistics-sub">Trade Exchange</p>
                    </div>
                    <div class="col-md-3 col-6 icon-resize">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate" viewBox="0 0 80 100"
                            width="80" height="100">
                            <defs>
                                <clipPath id="_clipPath_bU1vvk3PQGM77f5XMM5UGVM9vhioiNWE">
                                    <rect width="80" height="100" />
                                </clipPath>
                            </defs>
                            <g clip-path="url(#_clipPath_bU1vvk3PQGM77f5XMM5UGVM9vhioiNWE)">
                                <g id="farmer">
                                    <path d=" M 1.431 64.715 C 1.431 50.443 10.028 37.577 23.213 32.116 C 36.398 26.654 51.575 29.673 61.666 39.765 C 71.758 49.857 74.776 65.033 69.315 78.218 C 63.853 91.404 50.986 100 36.715 100 C 17.232 99.99 1.44 84.198 1.431 64.715 L 1.431 64.715 Z "
                                        fill="rgb(172,187,22)" fill-opacity="0.13" />
                                    <g id="farmer-2">
                                        <path d=" M 8.662 75.735 L 8.662 75.735 C 7.886 75.735 7.115 75.604 6.382 75.349 L 6.382 89.501 L 10.952 89.501 L 10.952 75.346 C 10.216 75.603 9.442 75.735 8.662 75.735 Z "
                                            fill="rgb(255,255,255)" />
                                        <g id="Group">
                                            <path d=" M 33.417 60.998 L 33.417 64.179 C 33.417 65.241 32.779 66.199 31.798 66.608 L 21.292 70.986 C 19.044 72.061 17.616 74.336 17.627 76.829 L 17.627 89.499 L 62.368 89.499 L 62.368 76.832 C 62.378 74.339 60.951 72.064 58.703 70.989 L 48.196 66.611 C 47.215 66.203 46.576 65.245 46.577 64.182 L 46.577 60.998 L 33.417 60.998 Z "
                                                fill="rgb(200,170,136)" />
                                            <path d=" M 55.169 89.502 L 55.169 89.502 L 55.169 74.499 C 55.169 72.921 55.691 71.386 56.655 70.135 L 56.655 70.135 L 48.196 66.611 C 47.215 66.203 46.576 65.244 46.577 64.182 L 46.562 70.045 C 46.552 73.662 43.622 76.591 40.006 76.602 L 40.006 76.602 C 36.389 76.591 33.46 73.662 33.449 70.045 L 33.418 64.182 C 33.419 65.244 32.78 66.203 31.799 66.611 L 23.341 70.135 L 23.341 70.135 C 24.304 71.386 24.826 72.921 24.826 74.499 L 24.826 89.502 L 55.17 89.502 L 55.169 89.502 Z "
                                                fill="rgb(45,43,42)" />
                                        </g>
                                        <rect x="29.599" y="82.481" width="20.854" height="7.019" transform="matrix(1,0,0,1,0,0)" fill="rgb(172,187,22)" />
                                        <g id="Group-2">
                                            <path d=" M 46.576 61.917 L 46.576 70.027 C 46.565 73.652 43.629 76.588 40.004 76.599 L 40.004 76.599 C 36.371 76.588 33.428 73.645 33.417 70.012 L 33.417 61.917"
                                                fill="rgb(185,157,137)" />
                                            <path d=" M 54.689 42.259 L 54.689 37.679 C 54.763 32.412 52.008 27.51 47.469 24.836 C 42.931 22.162 37.308 22.128 32.737 24.746 C 28.167 27.364 25.351 32.233 25.362 37.5 L 25.304 37.5 L 25.304 42.256 L 25.304 42.256 C 23.284 42.256 21.647 43.894 21.647 45.913 C 21.647 47.932 23.284 49.569 25.304 49.569 L 25.304 49.569 L 25.304 52.299 L 25.304 52.299 C 25.415 54.486 26.52 56.502 28.303 57.774 L 28.303 57.774 L 37.142 62.827 C 38.864 63.999 41.128 63.999 42.85 62.827 L 51.662 57.774 L 51.662 57.774 C 53.452 56.505 54.566 54.49 54.687 52.299 L 54.687 52.299 L 54.687 49.572 L 54.687 49.572 C 56.706 49.572 58.344 47.935 58.344 45.916 C 58.344 43.897 56.706 42.259 54.687 42.259 L 54.689 42.259 Z "
                                                fill="rgb(194,166,144)" />
                                        </g>
                                        <g id="Group-3">
                                            <path d=" M 72.446 82.502 L 72.446 82.502 C 72.433 82.502 69.438 82.888 67.817 81.278 C 66.196 79.668 66.563 76.671 66.564 76.658 L 66.564 76.658 C 66.588 76.658 69.574 76.275 71.192 77.882 C 72.809 79.489 72.446 82.502 72.446 82.502 Z "
                                                fill="rgb(255,255,255)" />
                                            <path d=" M 73.702 77.88 C 75.32 76.272 78.306 76.653 78.331 76.656 L 78.331 76.656 C 78.331 76.67 78.698 79.666 77.077 81.277 C 75.457 82.888 72.461 82.502 72.448 82.5 L 72.448 82.5 C 72.448 82.5 72.08 79.494 73.702 77.88 Z "
                                                fill="rgb(255,255,255)" />
                                            <path d=" M 72.446 74.539 L 72.446 74.539 C 72.433 74.539 69.438 74.927 67.817 73.316 C 66.196 71.705 66.563 68.709 66.564 68.695 L 66.564 68.695 C 66.588 68.695 69.574 68.311 71.192 69.919 C 72.809 71.527 72.446 74.539 72.446 74.539 Z "
                                                fill="rgb(255,255,255)" />
                                            <path d=" M 73.702 69.918 C 75.32 68.311 78.306 68.691 78.331 68.694 L 78.331 68.694 C 78.331 68.707 78.698 71.703 77.077 73.315 C 75.457 74.927 72.461 74.541 72.448 74.538 L 72.448 74.538 C 72.448 74.538 72.08 71.532 73.702 69.918 Z "
                                                fill="rgb(255,255,255)" />
                                            <path d=" M 72.446 66.577 L 72.446 66.577 C 72.433 66.577 69.438 66.964 67.817 65.354 C 66.196 63.744 66.563 60.746 66.564 60.733 L 66.564 60.733 C 66.588 60.733 69.574 60.35 71.192 61.957 C 72.809 63.565 72.446 66.577 72.446 66.577 Z "
                                                fill="rgb(255,255,255)" />
                                            <path d=" M 73.702 61.954 C 75.32 60.347 78.306 60.727 78.331 60.73 L 78.331 60.73 C 78.331 60.743 78.698 63.739 77.077 65.351 C 75.457 66.963 72.461 66.577 72.448 66.574 L 72.448 66.574 C 72.448 66.574 72.08 63.57 73.702 61.954 Z "
                                                fill="rgb(255,255,255)" />
                                        </g>
                                        <path d=" M 1.654 91.135 L 1.633 91.135 C 0.731 91.135 0 90.404 0 89.502 C 0 88.601 0.731 87.87 1.633 87.87 L 1.654 87.87 C 2.556 87.87 3.287 88.601 3.287 89.502 C 3.287 90.404 2.556 91.135 1.654 91.135 L 1.654 91.135 Z "
                                            fill="rgb(95,81,75)" />
                                        <path d=" M 78.359 91.135 L 78.337 91.135 C 77.436 91.135 76.705 90.404 76.705 89.502 C 76.705 88.601 77.436 87.87 78.337 87.87 L 78.359 87.87 C 78.942 87.87 79.481 88.181 79.773 88.686 C 80.065 89.191 80.065 89.813 79.773 90.319 C 79.481 90.824 78.942 91.135 78.359 91.135 L 78.359 91.135 Z "
                                            fill="rgb(95,81,75)" />
                                        <path d=" M 45.95 47.932 C 44.942 47.932 44.125 47.115 44.125 46.107 C 44.125 45.1 44.942 44.283 45.95 44.283 C 46.958 44.283 47.775 45.1 47.775 46.107 C 47.774 47.115 46.958 47.931 45.95 47.932 L 45.95 47.932 Z "
                                            fill="rgb(95,81,75)" />
                                        <path d=" M 34.043 47.932 C 33.035 47.932 32.218 47.115 32.218 46.107 C 32.218 45.1 33.035 44.283 34.043 44.283 C 35.051 44.283 35.868 45.1 35.868 46.107 C 35.867 47.115 35.05 47.931 34.043 47.932 L 34.043 47.932 Z "
                                            fill="rgb(95,81,75)" />
                                        <path d=" M 40.003 56.835 C 38.22 56.835 36.774 55.389 36.774 53.606 C 36.774 52.704 37.505 51.973 38.407 51.973 L 41.597 51.973 C 42.499 51.973 43.23 52.704 43.23 53.606 C 43.23 55.388 41.786 56.833 40.003 56.835 L 40.003 56.835 Z "
                                            fill="rgb(97,83,77)" />
                                        <path d=" M 8.662 25.346 C 8.028 25.346 7.452 24.979 7.183 24.404 C 6.313 22.541 4.883 21.428 3.357 21.428 C 2.765 21.44 2.213 21.132 1.914 20.622 C 1.614 20.112 1.614 19.479 1.914 18.969 C 2.213 18.459 2.765 18.15 3.357 18.163 C 5.415 18.194 7.356 19.13 8.662 20.72 C 9.969 19.13 11.909 18.195 13.967 18.163 C 14.559 18.15 15.111 18.459 15.41 18.969 C 15.71 19.479 15.71 20.112 15.41 20.622 C 15.111 21.132 14.559 21.44 13.967 21.428 C 12.442 21.428 11.011 22.541 10.141 24.404 C 9.873 24.979 9.296 25.346 8.662 25.346 Z "
                                            fill="rgb(172,187,22)" />
                                        <path d=" M 13.804 56.576 C 13.17 56.576 12.593 56.209 12.325 55.634 C 11.455 53.77 10.024 52.657 8.499 52.657 C 7.907 52.67 7.355 52.362 7.056 51.852 C 6.756 51.341 6.756 50.709 7.056 50.199 C 7.355 49.689 7.907 49.38 8.499 49.393 C 10.557 49.424 12.497 50.359 13.804 51.95 C 15.111 50.36 17.051 49.425 19.109 49.393 C 19.701 49.38 20.253 49.689 20.552 50.199 C 20.852 50.709 20.852 51.341 20.552 51.852 C 20.253 52.362 19.701 52.67 19.109 52.657 C 17.584 52.657 16.153 53.77 15.283 55.634 C 15.015 56.208 14.438 56.576 13.804 56.576 L 13.804 56.576 Z "
                                            fill="rgb(86,97,97)" />
                                        <path d=" M 73.063 18.328 C 72.428 18.327 71.852 17.96 71.583 17.386 C 70.713 15.522 69.282 14.41 67.757 14.41 C 66.855 14.41 66.124 13.679 66.124 12.777 C 66.124 11.876 66.855 11.145 67.757 11.145 C 69.044 12.76 70.997 13.702 73.062 13.702 C 75.128 13.702 77.081 12.76 78.367 11.145 C 79.269 11.145 80 11.876 80 12.777 C 80 13.679 79.269 14.41 78.367 14.41 C 76.842 14.41 75.412 15.522 74.541 17.386 C 74.273 17.96 73.696 18.327 73.063 18.328 Z "
                                            fill="rgb(86,97,97)" />
                                        <path d=" M 72.435 58.13 C 71.534 58.13 70.803 57.399 70.803 56.497 L 70.803 52.934 C 70.803 52.032 71.534 51.301 72.435 51.301 C 73.337 51.301 74.068 52.032 74.068 52.934 L 74.068 56.497 C 74.068 57.399 73.337 58.13 72.435 58.13 L 72.435 58.13 Z "
                                            fill="rgb(252,219,90)" />
                                        <path d=" M 76.381 56.348 C 75.479 56.348 74.748 55.617 74.748 54.715 L 74.748 51.152 C 74.736 50.56 75.044 50.008 75.554 49.709 C 76.065 49.409 76.697 49.409 77.207 49.709 C 77.717 50.008 78.026 50.56 78.013 51.152 L 78.013 54.715 C 78.013 55.616 77.282 56.347 76.381 56.348 L 76.381 56.348 Z "
                                            fill="rgb(172,187,22)" />
                                        <path d=" M 68.49 56.336 C 67.588 56.336 66.857 55.605 66.857 54.703 L 66.857 51.14 C 66.857 50.238 67.588 49.507 68.49 49.507 C 69.392 49.507 70.123 50.238 70.123 51.14 L 70.123 54.703 C 70.123 55.605 69.392 56.336 68.49 56.336 L 68.49 56.336 Z "
                                            fill="rgb(172,187,22)" vector-effect="non-scaling-stroke" stroke-width="0.751" stroke="rgb(172,187,22)"
                                            stroke-linejoin="miter" stroke-linecap="butt" stroke-miterlimit="4" />
                                        <path d=" M 39.882 31.169 C 25.209 31.169 13.313 33.867 13.313 37.194 C 13.313 39.279 18.21 41.174 25.305 42.259 C 23.781 41.676 22.677 40.936 22.677 40.209 C 22.677 38.054 30.382 36.304 39.887 36.304 C 49.392 36.304 57.097 38.052 57.097 40.209 C 57.097 40.935 56.209 41.677 54.689 42.259 C 61.774 41.177 66.45 39.277 66.45 37.194 C 66.45 33.867 54.555 31.169 39.882 31.169 Z "
                                            fill="rgb(191,173,139)" />
                                        <path d=" M 39.882 31.169 C 44.647 31.148 49.407 31.462 54.128 32.108 L 54.128 24.529 C 54.128 24.529 46.865 21.756 39.997 21.756 C 39.997 21.756 25.866 24.529 25.866 24.529 L 25.866 32.076 C 30.511 31.452 35.194 31.149 39.882 31.169 L 39.882 31.169 Z "
                                            fill="rgb(86,97,97)" />
                                        <path d=" M 78.227 82.434 C 80.297 80.376 80.007 76.974 79.954 76.492 C 79.87 75.718 79.256 75.109 78.482 75.03 C 78.198 75 77.912 74.984 77.625 74.983 C 77.839 74.829 78.039 74.658 78.226 74.472 C 80.305 72.406 80.003 68.981 79.953 68.523 C 79.865 67.753 79.253 67.148 78.482 67.068 C 78.345 67.053 78.039 67.024 77.627 67.02 C 77.84 66.866 78.041 66.696 78.228 66.511 C 80.306 64.443 80.005 61.019 79.955 60.561 C 79.867 59.791 79.255 59.186 78.484 59.106 C 77.919 59.043 74.595 58.767 72.551 60.798 L 72.551 60.798 C 72.515 60.834 72.482 60.873 72.447 60.907 C 72.412 60.871 72.378 60.832 72.342 60.798 C 70.294 58.762 66.959 59.044 66.41 59.106 C 65.636 59.185 65.023 59.795 64.939 60.569 C 64.886 61.055 64.598 64.454 66.667 66.511 C 66.853 66.696 67.054 66.866 67.267 67.02 C 66.981 67.022 66.695 67.038 66.41 67.068 C 65.637 67.15 65.026 67.759 64.941 68.531 C 64.887 69.018 64.6 72.417 66.668 74.473 C 66.855 74.658 67.056 74.829 67.269 74.983 C 66.981 74.984 66.694 75 66.407 75.031 C 65.636 75.113 65.026 75.721 64.941 76.492 C 64.888 76.973 64.597 80.377 66.668 82.436 C 67.808 83.476 69.278 84.081 70.819 84.146 L 70.819 87.87 L 64.002 87.87 L 64.002 76.83 C 64.021 73.677 62.195 70.803 59.333 69.481 L 48.827 65.103 C 48.454 64.949 48.211 64.585 48.211 64.181 L 48.211 61.64 L 52.478 59.193 Q 52.523 59.168 52.566 59.139 C 54.779 57.594 56.162 55.121 56.32 52.427 C 56.32 52.386 56.325 52.344 56.325 52.303 L 56.325 50.951 C 57.883 50.447 59.119 49.249 59.672 47.708 C 60.224 46.166 60.031 44.456 59.149 43.077 C 60.76 42.72 62.339 42.229 63.868 41.609 C 66.667 40.423 68.087 38.939 68.087 37.199 C 68.087 34.645 65.126 32.727 59.034 31.346 C 58.008 31.113 56.914 30.902 55.765 30.712 L 55.765 24.533 C 55.765 23.856 55.347 23.249 54.714 23.008 C 54.405 22.891 47.087 20.128 40 20.128 C 32.913 20.128 25.595 22.891 25.287 23.008 C 24.654 23.249 24.236 23.856 24.235 24.533 L 24.235 30.674 C 23.001 30.874 21.832 31.098 20.736 31.346 C 14.644 32.727 11.683 34.642 11.683 37.199 C 11.683 39.827 14.953 41.174 16.028 41.617 C 17.592 42.234 19.204 42.725 20.847 43.083 C 19.967 44.462 19.776 46.171 20.329 47.711 C 20.883 49.25 22.118 50.447 23.674 50.951 L 23.674 52.303 C 23.674 52.344 23.674 52.386 23.679 52.427 C 23.829 55.117 25.202 57.59 27.406 59.14 C 27.435 59.159 27.464 59.177 27.496 59.195 L 31.787 61.648 L 31.787 64.183 C 31.787 64.586 31.545 64.95 31.172 65.105 L 20.666 69.482 C 17.803 70.805 15.978 73.678 15.996 76.832 L 15.996 87.871 L 12.587 87.871 L 12.587 76.422 C 15.481 74.946 17.304 71.973 17.308 68.724 L 17.308 63.86 C 17.308 63.276 16.996 62.737 16.491 62.446 C 15.986 62.154 15.364 62.154 14.859 62.446 C 14.354 62.737 14.042 63.276 14.042 63.86 L 14.042 68.724 C 14.039 71 12.607 73.029 10.463 73.793 C 10.447 73.797 10.336 73.829 10.297 73.842 L 10.297 63.704 C 10.297 62.802 9.566 62.072 8.664 62.072 C 7.762 62.072 7.031 62.802 7.031 63.704 L 7.031 73.842 C 6.995 73.83 6.888 73.8 6.873 73.796 C 4.724 73.034 3.288 71.004 3.285 68.724 L 3.285 63.86 C 3.297 63.268 2.989 62.716 2.479 62.417 C 1.969 62.117 1.336 62.117 0.826 62.417 C 0.316 62.716 0.008 63.268 0.02 63.86 L 0.02 68.724 C 0.023 71.978 1.851 74.954 4.751 76.428 L 4.751 89.503 C 4.751 90.404 5.482 91.135 6.383 91.136 L 73.069 91.136 C 73.763 91.136 74.381 90.697 74.61 90.043 C 74.839 89.388 74.629 88.66 74.087 88.228 L 74.087 84.147 C 75.627 84.082 77.096 83.476 78.234 82.437 C 80.304 80.379 77.012 83.651 78.234 82.437 C 80.304 80.379 77.012 83.651 78.234 82.437 C 80.304 80.379 77.012 83.651 78.234 82.437 C 80.304 80.379 77.012 83.651 78.234 82.437 C 80.304 80.379 77.012 83.651 78.234 82.437 C 80.3 80.378 77.007 83.65 78.227 82.434 Z  M 74.848 63.113 L 74.848 63.113 C 74.856 62.528 75.335 62.059 75.92 62.063 C 76.505 62.066 76.978 62.541 76.979 63.127 C 76.98 63.712 76.508 64.188 75.923 64.194 C 75.915 64.779 75.436 65.248 74.851 65.244 C 74.265 65.241 73.793 64.766 73.792 64.18 C 73.791 63.595 74.262 63.119 74.848 63.113 L 74.848 63.113 Z  M 74.848 71.073 L 74.848 71.073 C 74.856 70.488 75.335 70.02 75.92 70.024 C 76.506 70.028 76.978 70.503 76.979 71.088 C 76.979 71.673 76.508 72.149 75.923 72.155 C 75.914 72.74 75.435 73.209 74.85 73.205 C 74.265 73.201 73.793 72.725 73.792 72.14 C 73.791 71.555 74.262 71.079 74.848 71.073 L 74.848 71.073 Z  M 74.848 79.034 L 74.848 79.034 C 74.856 78.448 75.335 77.979 75.921 77.982 C 76.507 77.986 76.98 78.462 76.98 79.048 C 76.981 79.633 76.509 80.11 75.923 80.115 C 75.914 80.7 75.435 81.169 74.85 81.165 C 74.265 81.161 73.793 80.686 73.792 80.101 C 73.791 79.515 74.262 79.039 74.848 79.034 L 74.848 79.034 Z  M 68.962 80.115 C 68.955 79.631 69.275 79.203 69.741 79.073 C 70.207 78.942 70.702 79.142 70.947 79.56 C 71.192 79.977 71.125 80.507 70.783 80.85 C 70.117 80.802 69.483 80.546 68.968 80.12 L 68.962 80.115 Z  M 68.962 72.155 C 68.955 71.671 69.275 71.243 69.741 71.113 C 70.207 70.983 70.702 71.183 70.947 71.6 C 71.192 72.017 71.125 72.547 70.784 72.89 C 70.117 72.841 69.482 72.585 68.968 72.158 L 68.962 72.155 Z  M 68.962 64.195 C 68.955 63.711 69.275 63.283 69.741 63.152 C 70.207 63.022 70.702 63.222 70.947 63.639 C 71.192 64.057 71.125 64.587 70.783 64.93 C 70.117 64.88 69.482 64.623 68.968 64.196 L 68.962 64.195 Z  M 9.315 87.868 L 8.011 87.868 L 8.011 77.34 C 8.225 77.355 8.44 77.366 8.658 77.366 C 8.876 77.366 9.098 77.355 9.315 77.338 L 9.315 87.867 L 9.315 87.868 Z  M 27.493 25.688 C 29.692 24.956 34.989 23.389 39.992 23.389 C 44.982 23.389 50.289 24.958 52.491 25.689 L 52.491 30.242 C 44.185 29.307 35.802 29.298 27.494 30.215 L 27.494 25.683 L 27.493 25.688 Z  M 21.078 39.779 C 17.082 38.809 15.286 37.724 14.967 37.187 C 15.239 36.737 16.961 35.46 22.164 34.371 C 33.882 32.278 45.878 32.278 57.596 34.371 C 62.828 35.47 64.538 36.751 64.797 37.195 C 64.563 37.598 63.078 38.772 58.703 39.823 C 58.368 37.479 54.99 36.444 52.418 35.857 C 44.139 34.278 35.636 34.278 27.357 35.857 C 24.802 36.438 21.454 37.467 21.078 39.779 Z  M 29.162 56.387 C 27.838 55.404 27.022 53.882 26.936 52.235 L 26.936 49.571 C 26.936 48.67 26.205 47.939 25.304 47.938 C 24.553 47.982 23.839 47.606 23.451 46.961 C 23.062 46.317 23.062 45.511 23.451 44.867 C 23.839 44.222 24.553 43.846 25.304 43.89 C 26.205 43.889 26.936 43.159 26.936 42.257 L 26.936 39.328 C 27.518 39.165 28.209 38.999 29.028 38.84 C 36.217 37.635 43.557 37.635 50.747 38.84 C 51.67 39.018 52.431 39.206 53.056 39.39 L 53.056 42.257 C 53.056 43.159 53.787 43.89 54.689 43.89 C 55.761 43.952 56.599 44.84 56.599 45.914 C 56.599 46.988 55.761 47.876 54.689 47.938 C 53.787 47.938 53.056 48.669 53.056 49.571 L 53.056 52.235 C 52.952 53.882 52.128 55.401 50.803 56.387 L 42.039 61.412 C 42.008 61.429 41.98 61.447 41.951 61.466 C 40.774 62.274 39.22 62.274 38.043 61.466 C 38.013 61.447 37.984 61.429 37.953 61.411 L 29.162 56.387 Z  M 44.943 63.51 L 44.943 70.044 C 44.909 72.752 42.704 74.93 39.995 74.93 C 37.287 74.93 35.082 72.752 35.048 70.044 L 35.048 63.51 L 36.288 64.219 C 38.536 65.708 41.456 65.708 43.703 64.219 L 44.943 63.51 Z  M 23.191 87.868 L 19.257 87.868 L 19.257 76.829 C 19.251 74.996 20.281 73.318 21.918 72.493 L 22.679 72.176 C 23.018 72.903 23.193 73.695 23.191 74.497 L 23.191 87.864 L 23.191 87.868 Z  M 48.818 87.868 L 31.231 87.868 L 31.231 84.113 L 48.821 84.113 L 48.818 87.868 Z  M 53.535 74.501 L 53.535 87.868 L 52.083 87.868 L 52.083 82.481 C 52.083 81.58 51.353 80.849 50.452 80.849 L 29.597 80.849 C 28.696 80.849 27.965 81.58 27.965 82.481 L 27.965 87.868 L 26.456 87.868 L 26.456 74.501 C 26.458 73.268 26.199 72.049 25.695 70.923 L 31.783 68.386 L 31.783 70.043 C 31.783 74.578 35.46 78.255 39.995 78.255 C 44.53 78.255 48.207 74.578 48.207 70.043 L 48.207 68.382 L 54.295 70.919 C 53.792 72.045 53.534 73.266 53.537 74.499 L 53.535 74.501 Z  M 60.733 87.868 L 56.8 87.868 L 56.8 74.501 C 56.798 73.699 56.973 72.907 57.312 72.18 L 58.073 72.497 C 59.71 73.322 60.74 75.001 60.734 76.833 L 60.733 87.868 Z  M 72.448 68.214 C 72.624 68.229 72.867 68.246 73.154 68.25 C 72.941 68.404 72.74 68.574 72.553 68.759 C 72.517 68.795 72.484 68.834 72.45 68.869 C 72.414 68.832 72.381 68.793 72.346 68.759 C 72.159 68.574 71.958 68.404 71.745 68.25 C 72.029 68.247 72.272 68.232 72.444 68.215 L 72.448 68.214 Z  M 72.553 76.721 L 72.553 76.721 C 72.517 76.758 72.484 76.797 72.449 76.83 C 72.414 76.794 72.381 76.755 72.345 76.721 C 72.157 76.535 71.955 76.364 71.74 76.21 C 72.029 76.21 72.273 76.191 72.448 76.176 C 72.624 76.191 72.867 76.206 73.157 76.21 C 72.942 76.365 72.74 76.536 72.552 76.723 L 72.553 76.721 Z "
                                            fill="rgb(86,97,97)" />
                                    </g>
                                </g>
                            </g>
                        </svg>
                        <p class="mb-0 statistics">2,400&plus;</p>
                        <p class="statistics-sub">Buyers and Sellers</p>
                    </div>
                    <div class="col-md-3 col-6 icon-resize">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="isolation:isolate" viewBox="0 0 80 100"
                            width="80" height="100">
                            <defs>
                                <clipPath id="_clipPath_qqkxGtVM8qYa0OcfbNQ2rZX4Qlx1rm3r">
                                    <rect width="80" height="100" />
                                </clipPath>
                            </defs>
                            <g clip-path="url(#_clipPath_qqkxGtVM8qYa0OcfbNQ2rZX4Qlx1rm3r)">
                                <g id="products">
                                    <path d=" M 0 60.729 C 0 44.845 9.568 30.525 24.243 24.447 C 38.917 18.369 55.809 21.728 67.04 32.96 C 78.272 44.191 81.631 61.083 75.553 75.757 C 69.475 90.432 55.155 100 39.271 100 C 17.586 99.99 0.01 82.414 0 60.729 Z "
                                        fill="rgb(172,187,22)" fill-opacity="0.13" />
                                    <path d=" M 5.846 61.034 L 75.426 61.034 C 77.101 61.031 78.458 59.674 78.462 57.999 L 78.462 51.769 C 78.458 50.094 77.101 48.737 75.426 48.733 L 5.846 48.733 C 4.17 48.737 2.813 50.094 2.81 51.769 L 2.81 57.999 C 2.813 59.674 4.17 61.031 5.846 61.034 L 5.846 61.034 Z "
                                        fill="rgb(208,179,144)" />
                                    <path d=" M 75.426 61.034 L 5.846 61.034 C 4.17 61.038 2.813 62.395 2.81 64.07 L 2.81 70.3 C 2.813 71.975 4.17 73.332 5.846 73.336 L 75.426 73.336 C 77.101 73.332 78.458 71.975 78.462 70.3 L 78.462 64.07 C 78.458 62.395 77.101 61.038 75.426 61.034 L 75.426 61.034 Z "
                                        fill="rgb(152,120,102)" />
                                    <path d=" M 75.426 73.336 L 5.846 73.336 C 4.17 73.339 2.813 74.696 2.81 76.371 L 2.81 82.601 C 2.813 84.276 4.17 85.633 5.846 85.637 L 75.426 85.637 C 77.101 85.633 78.458 84.276 78.462 82.601 L 78.462 76.371 C 78.458 74.696 77.101 73.339 75.426 73.336 L 75.426 73.336 Z "
                                        fill="rgb(208,179,144)" />
                                    <path d=" M 23.172 48.733 L 44.179 48.733 C 45.201 46.382 46.46 43.798 47.983 40.971 Q 48.387 40.221 48.817 39.45 C 47.201 34.812 43.216 31.4 38.384 30.52 C 33.552 29.639 28.619 31.425 25.471 35.195 C 22.323 38.966 21.446 44.137 23.174 48.735 L 23.172 48.733 Z "
                                        fill="rgb(255,64,96)" />
                                    <path d=" M 48.816 39.449 C 46.912 42.877 45.381 45.968 44.18 48.733 L 63.786 48.733 Q 64.549 47.473 65.332 46.118 Q 65.948 45.051 66.439 44.086 C 67.87 41.274 68.519 39.081 68.482 37.284 C 68.438 35.097 67.376 33.496 65.468 32.083 C 64.424 31.327 63.305 30.68 62.128 30.155 C 59.95 29.209 58.033 29.089 56.115 30.145 C 55.123 30.723 54.234 31.463 53.486 32.334 C 52.171 33.864 51.016 35.526 50.04 37.291 Q 49.403 38.391 48.816 39.449 Z "
                                        fill="rgb(255,155,86)" />
                                    <path d=" M 56.113 30.144 C 58.03 29.089 59.947 29.208 62.125 30.154 C 63.303 30.68 64.422 31.326 65.466 32.083 C 67.374 33.496 68.435 35.096 68.48 37.283 C 70.211 37.875 72.127 37.44 73.431 36.158 C 74.736 34.877 75.206 32.969 74.645 31.228 C 76.873 30.75 78.463 28.781 78.463 26.503 C 78.463 24.225 76.873 22.256 74.645 21.777 C 74.645 18.853 73.085 16.151 70.553 14.688 C 68.02 13.226 64.9 13.226 62.367 14.688 C 59.835 16.151 58.275 18.853 58.275 21.777 C 56.487 22.162 55.069 23.523 54.612 25.293 C 54.155 27.064 54.736 28.941 56.114 30.143 L 56.113 30.144 Z "
                                        fill="rgb(91,209,133)" />
                                    <path d=" M 68.48 37.284 C 68.517 39.081 67.868 41.275 66.437 44.086 Q 65.944 45.052 65.329 46.118 Q 64.546 47.474 63.784 48.733 L 75.426 48.733 C 79.504 41.099 71.898 37.175 71.898 37.175 C 70.81 37.627 69.595 37.666 68.48 37.284 L 68.48 37.284 Z "
                                        fill="rgb(200,226,124)" />
                                    <path d=" M 17.206 48.733 C 17.206 48.733 16.528 36.385 17.99 28.936 C 18.184 27.865 18.479 26.815 18.871 25.8 L 16.798 23.917 C 13.585 20.998 12.299 32.437 11.564 35.672 C 12.299 39.99 12.726 44.354 12.841 48.733 L 17.206 48.733 Z "
                                        fill="rgb(91,209,133)" />
                                    <path d=" M 12.841 48.733 C 12.726 44.355 12.299 39.991 11.564 35.673 C 10.825 31.259 9.732 26.912 8.296 22.673 C 7.017 18.959 7.614 18.745 5.408 20.688 L 2.81 22.978 C 5.387 29.28 8.382 48.733 8.382 48.733 L 12.841 48.733 Z "
                                        fill="rgb(255,193,109)" />
                                    <path d=" M 17.991 28.937 C 16.529 36.386 17.207 48.734 17.207 48.734 L 23.172 48.734 C 21.422 44.131 22.289 38.943 25.44 35.159 C 28.592 31.375 33.538 29.584 38.381 30.473 C 43.224 31.362 47.212 34.793 48.815 39.449 Q 49.402 38.392 50.037 37.291 C 51.014 35.525 52.169 33.864 53.484 32.334 C 54.232 31.463 55.121 30.723 56.113 30.145 C 54.139 28.423 53.897 25.442 55.566 23.424 C 49.13 11.848 40.634 15.462 38.442 20.288 C 36.249 25.113 30.795 25.793 27.214 23.728 C 23.634 21.664 19.924 20.179 16.692 23.831 C 16.726 23.858 16.76 23.888 16.794 23.919 L 18.867 25.802 C 18.477 26.817 18.184 27.867 17.991 28.937 L 17.991 28.937 Z "
                                        fill="rgb(200,226,124)" />
                                    <path d=" M 77.556 47.724 C 78.283 45.845 78.362 43.777 77.78 41.848 C 77.169 40.024 76.058 38.408 74.574 37.183 C 75.86 35.872 76.523 34.072 76.398 32.24 C 78.6 31.177 80 28.948 80 26.503 C 80 24.057 78.6 21.828 76.398 20.766 C 75.689 15.822 71.455 12.151 66.461 12.151 C 61.467 12.151 57.232 15.822 56.524 20.766 C 56.33 20.862 56.14 20.968 55.957 21.083 C 51.949 14.9 47.561 14.077 45.191 14.173 C 41.673 14.359 38.543 16.463 37.044 19.65 C 36.368 21.239 35.035 22.455 33.392 22.982 C 31.587 23.545 29.629 23.333 27.987 22.396 C 26.165 21.222 24.117 20.441 21.975 20.104 C 19.925 19.886 17.882 20.569 16.376 21.977 C 15.91 21.869 15.424 21.892 14.971 22.045 C 13.293 22.617 12.299 24.874 11.509 28.075 C 10.883 25.647 10.258 23.657 9.748 22.173 C 9.512 21.489 9.341 20.924 9.204 20.47 C 8.842 19.261 8.552 18.305 7.495 18.007 C 6.438 17.708 5.699 18.369 4.76 19.206 L 4.39 19.534 L 1.792 21.823 C 1.299 22.257 1.136 22.955 1.385 23.562 C 3.413 28.531 5.776 42.287 6.577 47.198 L 5.844 47.198 C 3.319 47.201 1.273 49.247 1.271 51.771 L 1.271 58.001 C 1.27 59.121 1.683 60.202 2.43 61.037 C 1.683 61.872 1.27 62.952 1.271 64.072 L 1.271 70.302 C 1.27 71.422 1.683 72.503 2.43 73.338 C 1.683 74.173 1.27 75.254 1.271 76.374 L 1.271 82.603 C 1.274 85.128 3.32 87.173 5.844 87.176 L 75.425 87.176 C 77.949 87.173 79.994 85.127 79.998 82.603 L 79.998 76.374 C 79.998 75.253 79.585 74.173 78.839 73.338 C 79.585 72.503 79.998 71.422 79.998 70.302 L 79.998 64.072 C 79.998 62.952 79.585 61.872 78.839 61.037 C 79.585 60.202 79.998 59.121 79.998 58.001 L 79.998 51.771 C 79.998 50.074 79.058 48.515 77.556 47.724 L 77.556 47.724 Z  M 74.46 47.196 L 66.482 47.196 C 66.542 47.093 66.601 46.99 66.661 46.887 C 68.472 43.748 69.52 41.228 69.879 39.078 C 69.935 39.078 69.991 39.078 70.046 39.078 C 70.608 39.078 71.166 39.003 71.708 38.857 C 73.151 39.781 74.252 41.151 74.845 42.759 C 75.261 44.236 75.123 45.813 74.459 47.196 L 74.46 47.196 Z  M 24.289 47.196 C 22.571 41.03 25.953 34.594 32.004 32.511 C 38.055 30.427 44.682 33.417 47.124 39.333 Q 46.884 39.768 46.652 40.203 L 46.652 40.203 L 46.649 40.211 C 45.328 42.662 44.167 44.998 43.179 47.196 L 24.289 47.196 Z  M 48.595 43.101 L 50.334 44.104 C 51.068 44.521 52.002 44.267 52.424 43.535 C 52.846 42.804 52.6 41.868 51.871 41.441 L 50.053 40.39 Q 50.689 39.241 51.37 38.058 C 52.107 36.755 52.932 35.503 53.84 34.313 L 55.835 35.465 C 56.311 35.744 56.901 35.747 57.38 35.472 C 57.859 35.198 58.153 34.687 58.152 34.135 C 58.151 33.583 57.853 33.074 57.373 32.801 L 56.039 32.031 C 56.275 31.845 56.525 31.676 56.785 31.524 C 56.826 31.504 56.866 31.482 56.906 31.458 C 57.523 31.111 58.218 30.93 58.926 30.931 C 60.41 31.039 61.84 31.531 63.077 32.358 C 65.521 33.77 66.662 35.073 66.896 36.717 C 67.083 38.033 66.709 39.751 65.76 41.915 L 63.405 40.555 C 62.67 40.139 61.737 40.393 61.314 41.124 C 60.892 41.856 61.139 42.791 61.867 43.219 L 64.38 44.668 Q 64.194 45.001 63.997 45.343 C 63.635 45.967 63.273 46.585 62.911 47.19 L 46.557 47.19 Q 47.486 45.22 48.595 43.1 L 48.595 43.101 Z  M 58.597 23.28 C 59.022 23.189 59.389 22.922 59.607 22.546 C 59.824 22.169 59.872 21.718 59.738 21.304 C 59.266 19.828 59.887 18.222 61.229 17.447 C 62.571 16.671 64.272 16.935 65.316 18.081 C 65.607 18.404 66.022 18.588 66.457 18.588 C 66.892 18.588 67.306 18.404 67.598 18.081 C 68.642 16.935 70.343 16.671 71.685 17.447 C 73.027 18.222 73.647 19.828 73.176 21.304 C 73.042 21.718 73.09 22.169 73.307 22.545 C 73.525 22.922 73.891 23.189 74.316 23.28 C 75.836 23.606 76.922 24.949 76.922 26.503 C 76.922 28.058 75.836 29.401 74.316 29.727 C 73.891 29.818 73.524 30.085 73.307 30.462 C 73.089 30.839 73.042 31.29 73.176 31.704 C 73.507 32.732 73.313 33.857 72.657 34.714 C 72.001 35.572 70.966 36.054 69.887 36.003 C 69.576 34.387 68.749 32.915 67.531 31.808 C 68.283 30.833 69.246 30.042 70.348 29.494 C 71.116 29.156 71.472 28.266 71.149 27.493 C 70.826 26.719 69.942 26.347 69.163 26.656 C 67.549 27.416 66.142 28.552 65.059 29.969 Q 64.831 29.83 64.607 29.702 C 64.453 29.612 64.3 29.527 64.149 29.444 C 64.835 27.798 65.116 26.011 64.967 24.233 C 64.904 23.683 64.549 23.208 64.039 22.992 C 63.528 22.776 62.941 22.852 62.502 23.19 C 62.062 23.528 61.839 24.076 61.917 24.625 C 61.993 25.856 61.789 27.088 61.319 28.229 C 60.541 27.991 59.733 27.868 58.919 27.862 C 58.098 27.859 57.282 27.999 56.507 28.275 C 55.927 27.366 55.828 26.23 56.243 25.234 C 56.659 24.238 57.536 23.509 58.591 23.282 L 58.597 23.28 Z  M 26.449 25.058 C 27.954 25.916 29.657 26.366 31.389 26.362 C 32.387 26.364 33.379 26.212 34.33 25.91 C 36.796 25.11 38.802 23.295 39.843 20.922 C 40.868 18.796 42.96 17.39 45.315 17.245 C 48.314 17.122 51.29 19.29 53.758 23.366 C 52.607 25.377 52.647 27.857 53.863 29.829 C 53.33 30.275 52.83 30.759 52.367 31.278 C 52.35 31.296 52.333 31.315 52.316 31.334 C 51.038 32.818 49.907 34.422 48.939 36.124 C 47.44 33.648 45.266 31.651 42.672 30.367 C 43.051 28.97 43.633 27.636 44.4 26.407 C 44.862 25.694 44.659 24.742 43.946 24.28 C 43.233 23.818 42.281 24.021 41.819 24.734 C 40.929 26.149 40.244 27.682 39.784 29.288 C 34.681 27.955 29.251 29.368 25.445 33.02 C 21.639 36.672 20.003 42.039 21.125 47.193 L 18.681 47.193 C 18.563 43.885 18.392 35.644 19.353 29.993 C 20.659 29.505 22.112 29.611 23.333 30.283 C 24.078 30.674 24.998 30.395 25.4 29.656 C 25.801 28.917 25.534 27.992 24.801 27.581 C 23.395 26.801 21.78 26.481 20.183 26.666 Q 20.238 26.521 20.295 26.376 C 20.54 25.778 20.384 25.09 19.905 24.656 L 18.986 23.821 C 20.779 22.624 22.927 23.025 26.449 25.059 L 26.449 25.058 Z  M 13.506 33.699 C 13.81 32.001 14.188 29.888 14.683 28.081 C 14.906 27.04 15.3 26.043 15.848 25.131 L 17.08 26.249 C 15.256 32.051 15.451 43.043 15.601 47.195 L 14.326 47.195 C 14.163 43.349 13.763 39.517 13.128 35.72 C 13.241 35.173 13.369 34.48 13.506 33.7 L 13.506 33.699 Z  M 4.622 23.429 L 6.413 21.852 C 6.529 22.231 6.668 22.669 6.842 23.173 C 9.433 30.933 10.916 39.02 11.248 47.194 L 9.696 47.194 C 9.011 42.941 6.764 29.565 4.622 23.431 L 4.622 23.429 Z  M 4.348 51.768 C 4.349 50.942 5.019 50.272 5.846 50.27 L 75.428 50.27 C 76.255 50.272 76.924 50.942 76.925 51.768 L 76.925 57.998 C 76.924 58.825 76.254 59.495 75.427 59.496 L 5.846 59.496 C 5.019 59.495 4.35 58.825 4.348 57.998 L 4.348 51.768 Z  M 76.924 64.07 L 76.924 70.3 C 76.923 71.127 76.253 71.797 75.426 71.798 L 5.846 71.798 C 5.019 71.797 4.349 71.127 4.348 70.3 L 4.348 64.07 C 4.348 63.243 5.019 62.573 5.846 62.572 L 75.426 62.572 C 76.253 62.573 76.923 63.243 76.924 64.07 L 76.924 64.07 Z  M 76.924 82.601 C 76.923 83.428 76.253 84.098 75.426 84.099 L 5.846 84.099 C 5.019 84.098 4.348 83.428 4.348 82.601 L 4.348 76.371 C 4.348 75.544 5.019 74.874 5.846 74.873 L 75.426 74.873 C 76.253 74.874 76.923 75.544 76.924 76.371 L 76.924 82.601 Z "
                                        fill="rgb(86,97,97)" />
                                    <path d=" M 59.618 38.374 L 59.602 38.365 C 59.126 38.089 58.538 38.088 58.061 38.364 C 57.584 38.639 57.291 39.148 57.293 39.699 C 57.294 40.25 57.59 40.757 58.068 41.031 L 58.084 41.04 C 58.819 41.46 59.756 41.206 60.179 40.472 C 60.602 39.737 60.351 38.799 59.618 38.374 Z "
                                        fill="rgb(86,97,97)" />
                                    <path d=" M 46.598 23.932 C 46.815 23.932 47.03 23.886 47.228 23.797 L 47.286 23.771 C 47.804 23.562 48.168 23.086 48.236 22.531 C 48.303 21.975 48.064 21.427 47.61 21.099 C 47.156 20.771 46.56 20.716 46.054 20.954 L 45.968 20.993 C 45.307 21.29 44.943 22.009 45.095 22.718 C 45.248 23.427 45.875 23.933 46.6 23.933 L 46.598 23.932 Z "
                                        fill="rgb(86,97,97)" />
                                    <path d=" M 10.652 56.421 C 11.274 56.421 11.835 56.046 12.073 55.472 C 12.311 54.897 12.18 54.235 11.74 53.796 C 11.3 53.356 10.638 53.224 10.063 53.463 C 9.489 53.701 9.114 54.262 9.115 54.884 C 9.117 55.732 9.804 56.419 10.652 56.421 L 10.652 56.421 Z "
                                        fill="rgb(86,97,97)" />
                                    <path d=" M 10.652 68.723 C 11.502 68.723 12.19 68.034 12.19 67.185 C 12.19 66.336 11.502 65.647 10.652 65.647 C 9.803 65.647 9.115 66.336 9.115 67.185 C 9.116 68.034 9.804 68.721 10.652 68.723 Z "
                                        fill="rgb(86,97,97)" />
                                    <path d=" M 10.652 77.948 C 9.803 77.948 9.115 78.637 9.115 79.486 C 9.115 80.335 9.803 81.024 10.652 81.024 C 11.502 81.024 12.19 80.335 12.19 79.486 C 12.189 78.637 11.501 77.95 10.652 77.948 Z "
                                        fill="rgb(86,97,97)" />
                                    <path d=" M 70.62 56.421 C 71.242 56.421 71.803 56.046 72.041 55.472 C 72.279 54.897 72.147 54.235 71.707 53.796 C 71.267 53.356 70.606 53.224 70.031 53.463 C 69.456 53.701 69.082 54.262 69.082 54.884 C 69.084 55.732 69.772 56.419 70.62 56.421 L 70.62 56.421 Z "
                                        fill="rgb(86,97,97)" />
                                    <path d=" M 70.62 68.723 C 71.469 68.723 72.158 68.034 72.158 67.185 C 72.158 66.336 71.469 65.647 70.62 65.647 C 69.771 65.647 69.082 66.336 69.082 67.185 C 69.084 68.034 69.771 68.721 70.62 68.723 Z "
                                        fill="rgb(86,97,97)" />
                                    <path d=" M 70.62 77.948 C 69.771 77.948 69.082 78.637 69.082 79.486 C 69.082 80.335 69.771 81.024 70.62 81.024 C 71.469 81.024 72.158 80.335 72.158 79.486 C 72.156 78.637 71.469 77.95 70.62 77.948 Z "
                                        fill="rgb(86,97,97)" />
                                </g>
                            </g>
                        </svg>
                        <p class="mb-0 statistics">230&plus;</p>
                        <p class="statistics-sub">Farm Produce Listed</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- section 1 how it works -->
    <div class="container-fluid">
        <div class="row">
            <!-- <div class="col-lg-3 pl-lg-0 ml-lg-0">
                <img src="mainasset/fe/images/shutterstock_550794592.jpg" alt="Cassava Image" class="img-fluid how-it-works-img">
            </div> -->
            <div class="col-md-7 col-12 how-it-works-img-bg">

                <div class="justify-content-center translate">
                    <p class="section-title-black ">Access the African Commodidites
                        <span class="d-block"> Marketplace for FREE</span>
                    </p>
                    <a href="login.html#register-tab" class="register-link">Register Now</a>
                </div>
            </div>
            <div class="col-md-5 col-12 how-it-works-bg">
                <p class="section-title-white pl-lg-5">Buy and Sell
                    <span class="d-md-block">Commodities Online</span>
                </p>
                <a href="#" class="ml-lg-5 register-link-white">HOW IT WORKS</a>
            </div>
        </div>
    </div>
    <!-- our aim -->
    <div class="our-aim-bg ">
        <div class="container">
            <div class="row text-center text-md-left">
                <div class="col-lg-4 col-6">
                    <div class="row">
                        <div class="col-md-3 col-6 offset-3 offset-md-0">
                            <img src="<?php echo base_url();?>mainasset/fe/images/icons/003-checklist.svg" alt="" class="img-fluid">
                        </div>
                        <div class="col-md-9">
                            <p class="aim-title">Global Commodities Access</p>
                            <p class="aim-subtitle d-none d-md-block">Search our product database for agricultural commodities being sold around the world!</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <div class="row">
                        <div class="col-md-3 col-6 offset-3 offset-md-0">
                            <img src="<?php echo base_url();?>mainasset/fe/images/icons/006-shield.svg" alt="" class="img-fluid">
                        </div>
                        <div class="col-md-9">
                            <p class="aim-title">Enhanced Transparency</p>
                            <p class="aim-subtitle d-none d-md-block">All of our customers must go through our KYC compliance process in order to gain full access
                                to the platform.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <div class="row">
                        <div class="col-md-3 col-6 offset-3 offset-md-0">
                            <img src="<?php echo base_url();?>mainasset/fe/images/icons/005-money.svg" alt="" class="img-fluid">
                        </div>
                        <div class="col-md-9">
                            <p class="aim-title">Buy At Source</p>
                            <p class="aim-subtitle d-none d-md-block">Negotiate a better price directly with the producer.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <div class="row">
                        <div class="col-md-3 col-6 offset-3 offset-md-0">
                            <img src="<?php echo base_url();?>mainasset/fe/images/icons/004-talk.svg" alt="" class="img-fluid">
                        </div>
                        <div class="col-md-9">
                            <p class="aim-title">New Benchmark in Communication</p>
                            <p class="aim-subtitle d-none d-md-block">With our real-time messaging application and contract management system, you can negotiate every
                                step of a transaction without having to make a single phone call.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <div class="row">
                        <div class="col-md-3 col-6 offset-3 offset-md-0">
                            <img src="<?php echo base_url();?>mainasset/fe/images/icons/002-work.svg" alt="" class="img-fluid">
                        </div>
                        <div class="col-md-9">
                            <p class="aim-title">Personal Relationship Manager</p>
                            <p class="aim-subtitle d-none d-md-block">All of our customers are assigned a personal relationship manager that provides support with
                                platform functionality.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <div class="row">
                        <div class="col-md-3 col-6 offset-3 offset-md-0">
                            <img src="<?php echo base_url();?>mainasset/fe/images/icons/001-financial.svg" alt="" class="img-fluid">
                        </div>
                        <div class="col-md-9 col-12">
                            <p class="aim-title">Expand Markets</p>
                            <p class="aim-subtitle d-none d-md-block">Find new buyers and sellers for your products and diversify your client base.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- place full width img -->
    <div class="container-fluid img-bg">
        <div class="row">
            <div class="col-12">
                <img src="<?php echo base_url();?>mainasset/fe/images/bottom.jpg" alt="" class="img-fluid" style="opacity: 0;">
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
