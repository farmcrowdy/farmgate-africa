<div class="container-fluid sub-head-main">
    <div class="row page-title-bg no-pad">
        <div class="col-12 sub-head-main">
            <ul class="page-title-list">
                <li>
                    <a href="<?php echo base_url(); ?>">Home</a> /
                </li>
                <li>
                    <a href="<?php echo base_url('about'); ?>"> About Us</a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-10 offset-lg-1 col-12 col-sm-12 offset-md-0  about-us">
            <div class="row">
                <div class="col-lg-6 col-sm-7">
                    <div class="pl-md-4 pl-sm-3 px-2 pr-sm-0">
                        <h4>We’re changing the way agricultural commodities are traded.</h4>
                        <p>
                            Farmgate Africa is a Nigerian based company focused on providing major processors and international buyers the opportunity
                            to purchase commodities directly from farming clusters. By this, we optimize market access to
                            African farmers and also improve their income. Farmgate Africa brings the farmers closer to the
                            processors and off-takers by eliminating the layers of intermediaries.</p>

                        <p>Our vision is to continuously provide simple, safe, reliable and profitable avenue for agriculture
                            investors while empowering smallholder farmers across the agricultural value chain.</p>

                        <p>FarmGate Africa strives on fast and professional services to all clients for effective and efficient
                            commodity trading. At FarmGate Africa, we gladly share our knowledge and experience with all
                            our clients and producers. Feel free to contact us.</p>
                    </div>

                </div>
                <div class="col-lg-4 col-sm-5 offset-lg-1 pr-md-5 pr-sm-4  pr-5 pr-sm-0">
                    <div>
                        <img src="<?php echo base_url();?>mainasset/fe/images/img1.jpg" class="img-fluid" alt="Farmgate images">
                    </div>
                    <div>
                        <img src="<?php echo base_url();?>mainasset/fe/images/img2.jpg" class="img-fluid" alt="Farmgate images">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <section class="container-fluid">
        <div class="row">
            <div class="col-lg-10 offset-lg-1 col-12 col-sm-12 offset-md-0">
                <div class="row">
                    <div class="col-12">
                        <div class="pl-md-4 pl-sm-3 px-2 pr-sm-0">
                            <h4 class="team-title">Meet Kenneth</h4>
                        </div>
                    </div>
                </div>
                <div class="row no-pad">
                    <div class="col-lg-5 col-md-6 col-sm-8 col-12 profile-card-width">
                        <div class="row no-pad-left">
                            <div class="col-12 team-body ">
                                <div class="row team-sub-body d-flex">
                                    <img src="<?php echo base_url('mainasset/fe/images/kenneth-profile.jpg'); ?>" class="img-fluid team-img" alt="Manager's image">
                                    <div class="align-self-center title-container">
                                        <h4 class="team-name">Kenneth Obiajulu</h4>
                                        <p>Managing Director </p>
                                        <a  data-lity href="<?php echo base_url('kenneth'); ?>" class="load-team">See more</a>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        </div>
</section> -->
<section class="container container_team">
        <div class="row no_row__pad no_row__pad1">
            <div class="col-12">
                <div class="row no_row__pad">
                    <div class="col-12">
                        <div class="pl-md-4 pl-sm-3 px-2 pb-4 pr-sm-0">
                            <h4 class="team-title">Our Team</h4>
                        </div>
                    </div>
                </div>
                <div class="row no_row__pad">
                    <div class="team_container">
                        <div class="team_members">
                            <a data-toggle="modal" data-target=".kenneth">
                                <div class="team-sub-body">
                                    <img src="<?php echo base_url();?>mainasset/fe/images/kenneth.jpg"
                                        class="img-fluid team-img" alt="Manager's image">
                                    <div class=" title-container">
                                        <h4 class="team-name">Kenneth Obiajulu</h4>
                                        <p>Managing Director</p>
                                    </div>
                                </div>
                            </a>
                            <!-- Modal -->
                            <div class="modal fade bd-example-modal-lg kenneth" tabindex="-1" role="dialog"
                                aria-labelledby="kenneth" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content" style="padding:30px;">
                                        <div class="text-right team_close">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <section class="container-fluid">
                                            <div class="row">
                                                <div class="col-12 team-profile-container">
                                                    <div class="row">
                                                        <div class="col-12 col-md-4 px-md-0 text-center">
                                                            <img src="https://www.farmgate.africa/mainasset/fe/images/kenneth-profile.jpg"
                                                                class="img-fluid" alt="team image">
                                                            <h5 class="team-name">Kenneth Obiajulu</h5>
                                                            <p class="person-title">Managing Director </p>
                                                        </div>
                                                        <div class="col-md-8 pl-md-4 scrollable">
                                                            <p>Kenneth is a First-Class Graduate of Economics and
                                                                Business Studies (Marketing Specialization) from
                                                                Redeemer’s University, Postgraduate Diploma Holder from
                                                                the Institute of Marketing of Nigeria, an Associate
                                                                Member of the same Institute and a PhD scholarship
                                                                recipient from the Delta State Government.</p>
                                                            <p>Kenneth has worked on several projects spanning across
                                                                diverse sectors. Before joining the Farmgate team,
                                                                Kenneth was Innovation Consultant with the World Bank,
                                                                where he was responsible for mapping innovations that
                                                                cuts across agricultural productivity, livestock, market
                                                                information, financial services, etc. Kenneth has also
                                                                worked and consulted for developmental projects funded
                                                                by Rockefeller Foundation/Sustainable Food Lab (Food
                                                                Loss and Waste Pilot), Bill and Melinda Gates
                                                                Foundation/TechnoServe (State Partnership for
                                                                Agriculture), just to mention a few</p>

                                                            <p>Kenneth started his career in Heineken Nigeria (Nigerian
                                                                Breweries Plc), where he was Project Support Analyst for
                                                                the Sona Integration Project in 2010-11. He has worked
                                                                and consulted for several publicly quoted firms like
                                                                SCOA Plc, First Aluminum Plc, Accenture, just to mention
                                                                a few. He is a result driven professional and joins the
                                                                Farmgate Team as the CEO.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="team_members">
                            <div class="team-sub-body">
                                <img src="<?php echo base_url();?>mainasset/fe/images/linda.jpg"
                                    class="img-fluid team-img" alt="Manager's image">
                                <div class=" title-container">
                                    <h4 class="team-name">Linda Obi</h4>
                                    <p>Head of Operations &amp; Support Services</p>
                                </div>
                            </div>
                        </div>
                        <div class="team_members">
                            <div class="team-sub-body">
                                <img src="<?php echo base_url();?>mainasset/fe/images/yadua.jpg" class="img-fluid team-img" alt="Manager's image">
                                <div class=" title-container">
                                    <h4 class="team-name">Gabriel Yadua</h4>
                                    <p>Head of Sourcing &amp; Logistics</p>
                                </div>
                            </div>
                        </div>
                        <div class="team_members">
                            <div class="team-sub-body">
                                <img src="<?php echo base_url();?>mainasset/fe/images/emmanuel.jpg" class="img-fluid team-img" alt="Manager's image">
                                <div class=" title-container">
                                    <h4 class="team-name">Emmanuel Torty</h4>
                                    <p>Web Developer</p>
                                </div>
                            </div>
                        </div>
                        <div class="team_members">
                            <div class="team-sub-body">
                                <img src="<?php echo base_url();?>mainasset/fe/images/wura.png" class="img-fluid team-img" alt="Manager's image">
                                <div class=" title-container">
                                    <h4 class="team-name">Iyaniwura Omoyajowo </h4>
                                    <p>Account Officer </p>
                                </div>
                            </div>
                        </div>
                        <div class="team_members">
                            <div class="team-sub-body">
                                <img src="<?php echo base_url();?>mainasset/fe/images/yongo.jpg" class="img-fluid team-img" alt="Manager's image">
                                <div class=" title-container">
                                    <h4 class="team-name">Sughnen Yongo </h4>
                                    <p>Content Developer </p>
                                </div>
                            </div>
                        </div>
                        <div class="team_members">
                            <div class="team-sub-body">
                                <img src="<?php echo base_url();?>mainasset/fe/images/john.jpg" class="img-fluid team-img" alt="Manager's image">
                                <div class=" title-container">
                                    <h4 class="team-name">John Chukwuma</h4>
                                    <p>Field Coordinator</p>
                                </div>
                            </div>
                        </div>
                        <div class="team_members">
                            <div class="team-sub-body">
                                <img src="<?php echo base_url();?>mainasset/fe/images/jane.jpg" class="img-fluid team-img" alt="Manager's image">
                                <div class=" title-container">
                                    <h4 class="team-name">Janet Shoyebo </h4>
                                    <p>Analyst</p>
                                </div>
                            </div>
                        </div>
                        <div class="team_members">
                            <div class="team-sub-body">
                                <img src="<?php echo base_url();?>mainasset/fe/images/pelumi.jpg" class="img-fluid team-img" alt="Manager's image">
                                <div class=" title-container">
                                    <h4 class="team-name">Oluwapelumi Adekoya </h4>
                                    <p>Analyst</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-lg-5 col-md-7 col-sm-9 col-12 profile-card-width">
                        <div class="row no-pad-left">
                            <div class="col-12 team-body ">
                                <div class="row team-sub-body no-pad-left d-flex">
                                    <img src="images/kenneth-profile.jpg" class="img-fluid team-img"
                                        alt="Manager's image">
                                    <div class="align-self-center title-container">
                                        <h4 class="team-name">Kenneth Obiajulu</h4>
                                        <p>Managing Director &amp; Co-Founder</p>
                                        <a data-lity href="kenneth.html" class="load-team">See more</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div> -->
                </div>

            </div>
        </div>
        </div>
    </section>
<div class="container-fluid">
    <div class="row security-bg reveal" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-offset="0">
        <div class="col-12">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-10 offset-1 col-md-8 offset-sm-2 text-center">
                    <h4>Our Numbers</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-sm-10 offset-sm-1 col-12 offset-lg-2 pt-4">
                    <div class="row">
                        <div class="col-md-3 px-lg-4 col-6 col-sm-4 text-center">
                            <div class=" d-flex justify-content-center">
                                <img src="<?php echo base_url();?>mainasset/fe/images/icons/data-lock.svg" alt="funds icon">
                            </div>
                            <p class="traction">25K</p>
                            <p class="span">Metric Tonnes of Commodity Available For Purchase</p>
                        </div>
                        <div class="col-md-3 px-lg-4 col-6 col-sm-4 text-center">
                            <div class=" d-flex justify-content-center">
                                <img src="<?php echo base_url();?>mainasset/fe/images/icons/data-lock.svg" alt="funds icon">
                            </div>
                            <p class="traction">8</p>
                            <p class="span">Commodity Collection Centers</p>
                        </div>
                        <div class="col-md-3 px-lg-4 col-6 col-sm-4 text-center">
                            <div class=" d-flex justify-content-center">
                                <img src="<?php echo base_url();?>mainasset/fe/images/icons/data-lock.svg" alt="data icon">
                            </div>
                            <p class="traction">7.8K</p>
                            <p class="span">Farmers supplying Commodities</p>
                        </div>
                        <div class="col-md-3 px-lg-4 col-6 col-sm-4 offset-sm-4 offset-md-0 text-center">
                            <div class=" d-flex justify-content-center">
                                <img src="<?php echo base_url();?>mainasset/fe/images/icons/data-lock.svg" alt="investment icon">
                            </div>
                            <p class="traction">8K</p>
                            <p class="span">Metric Tonnes of Commodity Available For Trading</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row partners-border reveal">
        <div class="col-lg-8 offset-lg-2 text-center partners">
            <h4 class="mobile">Trusted and backed by renowned partners </h4>
            <p class="px-md-5 p-color">Our partners are an integral part of our business. <br>We value their contributions
                to our growth and development.</p>
            <div class="row px-lg-4 mt-md-5 no-pad">
                <div class="col-lg-4 offset-lg-4 col-sm-6 offset-sm-3">
                    <div class="row no-pad">
                        <div class="col-6 w-100 d-flex align-items-center justify-content-center">
                            <a href="https://www.leadway.com/" target="_blank">
                                <img src="<?php echo base_url();?>mainasset/fe/images/leadway.png" alt="Leadway logo">
                            </a>
                        </div>
                        <div class=" col-6 w-100 d-flex align-items-center justify-content-center">
                            <a href="https://paystack.com/" target="_blank">
                                <img src="<?php echo base_url();?>mainasset/fe/images/paystack.png" alt="Paystack logo">
                            </a>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-md-3 col-6 w-100 d-flex align-items-center justify-content-center">
                                <a href="#" target="_blank" rel="noopener noreferrer">
                                    <img src="images/gsma.png" alt="GSMA logo">
                                </a>
                            </div>
                            <div class="col-md-3 col-6 w-100 d-flex align-items-center justify-content-center">
                                <a href="#" target="_blank">
                                    <img src="images/guinness.png" alt="Guinness logo">
                                </a>
                            </div>
                            <div class="col-md-3 col-6 w-100 d-flex align-items-center justify-content-center">
                                <a href="#" target="_blank">
                                    <img src="images/afex.png" alt="Afex logo">
                                </a>
                            </div>
                            <div class="col-md-3 col-6 w-100 d-flex align-items-center justify-content-center">
                                <a href="#" target="_blank">
                                    <img src="images/leadway.png" alt="Leadway logo">
                                </a>
                            </div>
                            <div class="col-md-3 col-6 w-100 d-flex align-items-center justify-content-center">
                                <a href="#" target="_blank">
                                    <img src="images/paystack.png" alt="Paystack logo">
                                </a>
                            </div>
                            <div class="col-md-3 col-6 w-100 d-flex align-items-center justify-content-center">
                                <a href="#" target="_blank">
                                    <img src="images/nirsal.png" alt="Nirsal logo">
                                </a>
                            </div>
                            <div class="col-md-3 col-6 w-100 d-flex align-items-center justify-content-center">
                                <a href="#" target="_blank">
                                    <img src="images/natnudo.png" alt="Natnudo logo">
                                </a>
                            </div>
                            <div class="col-md-3 col-6 w-100 d-flex align-items-center justify-content-center">
                                <a href="#" target="_blank">
                                    <img src="images/fc.png" alt="Farmcrowdy logo">
                                </a>
                            </div> -->
            </div>
        </div>
    </div>
</div>