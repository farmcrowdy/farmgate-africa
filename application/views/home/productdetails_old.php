
    <div class="container">
        <div class="row">
            <ul class="sub-nav py-2">
                <li>
                    <a href="index.html" class="">Home /</a>
                </li>
                <li>
                    <a href="buy.html" class="">Products /</a>
                </li>
                <li>Product</li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="row">
                    <!-- <div class="col-lg-3 d-fixed" style="border:1px solid red"> -->
                    <div class="col-lg-4 col-md-8 offset-lg-0 offset-md-2 offset-1 col-10 text-center">
                        <div class="pad-product1">
                            <img src="<?php echo base_url();?>mainasset/fe/images/products/<?php echo $product->prod_image; ?>" class="img-fluid" alt="<?php echo $product->prod_name; ?>">
                        </div>
                        <!-- <div class="my-3">
                            <a href="#" class="send-msg-link no1">Make Enquiries</a>
                        </div>
                        <div class="my-4">
                            <a href="#" class="send-msg-link no2">Bookmark</a>
                        </div> -->
                    </div>
                    <!-- <div class="col-lg-9 pb-4" style="border:1px solid black"> -->
                    <div class="col-lg-8 pb-4">
                        <div class="row mb-2 text-center text-md-left">
                            <div class="col-12 col-md-5 col-lg-6 offset-md-1 offset-lg-0">
                                <p class="mb-0 product-name-bold"><?php echo $product->prod_name; ?></p>
                                <span class="product-title unit">Product ID: <?php echo strtoupper($product->prod_slug); ?></span>
                            </div>
                            <div class="col-12 col-md-5 text-md-right  my-3 my-lg-1 col-lg-6">
                            <?php if ($this->session->userdata('loggedIn')) : ?>
                                <?php if($verified == "no"):?>
                                <a href="javascript:void(0)" class="more-samples-link">Get Verfied to request sample</a>
                                <?php else : ?>
                                    <?php if($own_product == FALSE):?>
                                    <a href="<?php echo base_url('sample-request/').$product->prod_slug; ?>" class="more-samples-link" data-lity>REQUEST FOR SAMPLE</a>
                                    <?php endif ?>
                                <?php endif ?>
                            <?php else: ?>
                                <a href="<?php echo base_url('users/login'); ?>" class="more-samples-link">LOGIN TO REQUEST FOR SAMPLE</a>
                            <?php endif ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="tab-content card-body" id="myTabContent">
                                        <div class="tab-pane fade show active" id="specification" role="tabpanel" aria-labelledby="order-specification-tab">
                                            <table class="table no-table-border">
                                                <tbody class=" no-table-border">
                                                    <tr class=" no-table-border">
                                                        <td class="product-title">
                                                            <span class="product-main-title">Volume</span>
                                                            <span class="product-sub-title1"><?php echo $product->prod_quantity; ?>
                                                                <span class="smallMT"><?php echo $product->unit; ?></span>
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <span class="product-main-title">Price</span>
                                                            <span class="product-sub-title2"> <?php echo $product->prod_price_per_unit; ?></span>
                                                        </td>
                                                    </tr>
                                                    <tr class=" no-table-border">
                                                        <td class="product-title">Location</td>
                                                        <td><?php echo $product->state; ?></td>
                                                    </tr>
                                                    <tr class=" no-table-border">
                                                        <td class="product-title">Availability</td>
                                                        <td><?php echo date('d/m/Y',strtotime($product->prod_avail_from)); ?> - <?php echo date('d/m/Y',strtotime($product->prod_avail_to)); ?></td>
                                                    </tr>
                                                    <tr class=" no-table-border">
                                                        <td class="product-title">Category</td>
                                                        <td><?php echo $product->category; ?></td>
                                                    </tr>
                                                    <tr class=" no-table-border">
                                                        <td class="product-title">Description</td>
                                                        <td>
                                                            <div class="10"><?php echo $product->prod_description; ?></div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Row -->
                        </div>
                        <div class="row">
                            <div class="col-12 text-center  mb-3 mt-4">
                            <?php if ($this->session->userdata('loggedIn')) : ?>
                                <?php if($verified == "no"):?>
                                <a href="javascript:void(0)" class="more-samples-link">Get Verfied to request sample</a>
                                <?php else : ?>
                                    <?php if($own_product == FALSE):?>
                                    <a href="<?php echo base_url('sample-request/').$product->prod_slug; ?>" class="more-samples-link" data-lity>REQUEST FOR SAMPLE</a>
                                    <?php endif ?>
                                <?php endif ?>
                            <?php else: ?>
                                <a href="<?php echo base_url('users/login'); ?>" class="more-samples-link">LOGIN TO REQUEST FOR SAMPLE</a>
                            <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="container-fluid contact-bg">
        <div class="col-12 text-center py-5">
            <p class="mt-2 mb-4 product-sub-title1">Need Help? Let’s help you out.</p>
            <a href="#" class="send-msg-link my-4" data-toggle="modal" data-target=".contact-manager-modal">CONTACT SUPPORT DESK</a>
        </div>
    </div>
    <!-- carousel -->
    <div class="container-fluid" style="background: #fefefe;">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <p class="section-title-black text-center">Recently added products</p>
                <div class="row">
                    <div class="col-lg-1 carousel-btn">
                        <i class="fas fa-chevron-left owl-prev fa-3x"></i>
                    </div>
                    <div class="col-lg-10">
                        <div class="owl-carousel loop owl-theme owl-loaded" id="owl-main">
                            <div class="owl-stage-outer">
                                <div class="owl-stage mb-4">
                                  <?php foreach ($products as $key => $product) {?>
                                    <div class="owl-item carousel-item-bg">
                                        <a href="<?php echo base_url('product/').$product->prod_slug; ?>">
                                            <img src="<?php echo base_url();?>mainasset/fe/images/products/<?php echo $product->prod_image ;?>" alt="" class="img-fluid">
                                            <p class="text-center carousel-main-title mb-0 pb-0 pt-2"><?php echo $product->prod_name;?></p>
                                            <p class="text-center state">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="11" height="13" viewBox="0 0 11 13">
                                                    <image width="11" height="13" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAANCAMAAABIK2QJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAtFBMVEX///+suxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxasuxYAAAClajiyAAAAOnRSTlMAM5zW48uCG1vt1C848+XJ9Mc8vCcLRf6ixvxJi7/9ZaXBf2Y+ifeEGtKZHD31zBF88Qd6MukJcuo0qxBELQAAAAFiS0dEOzkO9GwAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfiBhQMHCFkZ5EmAAAAeklEQVQI1y2NCQ6CAAwEV8QTRREPQEQRvAVPVPb/D7MVJ2k6STdbADWjbjaaLQjtDpWuBfT6pD0Yks4IBumOJ1OTMw8+g7kkQzlgwWgpviJjrCWepJuI2x32BykJZI4n4Jz9OvOLPriq3lJV3F3xByqeBePX3/H+lLq+wxgQO5e/nRUAAAAASUVORK5CYII="
                                                    />
                                                </svg> <?php echo $product->state ;?></p>
                                            <p class="text-center price py-2">&#8358;55,000
                                                <span class="small">MT</span>
                                            </p>
                                        </a>
                                    </div>
                                  <?php } ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1 carousel-btn text-right">
                        <i class="fas fa-chevron-right owl-next fa-3x"></i>
                    </div>
                    <div class="col-12 text-center mt-3 mb-5">
                        <a href="<?php echo base_url('products');?>" class="see-all-products-link">SEE ALL PRODUCTS</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
