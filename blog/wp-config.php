<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'fga_wordpress');

/** MySQL database username */
define('DB_USER', 'fgateafrica');

/** MySQL database password */
define('DB_PASSWORD', '@fgate2018');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ykkbkb94rr6ubgnmr82f2hzvcxbafn36t4pgtdawgpqyaa1hiffnpmd5s2k8mgru');
define('SECURE_AUTH_KEY',  'urogv61y0n4uzohrwpossoxnnuvkevoz89ukru4phao0ul5ikfbkfxxfjy785kai');
define('LOGGED_IN_KEY',    'w4zuxtrwweezosnexhwpwmiextlqhzgdlkdn5yle1jfgtzmpympnhzmdon2oqc1r');
define('NONCE_KEY',        'ileyslij2heznatquoeb4vlnne8kekud6zru3hz8tzcr6fbuzf8hzdn1daxszw8l');
define('AUTH_SALT',        't7i7t2hnlq7axdjjudbv1sajvpeqq55awcu5xtleyoldxszvpluz4leo5ii02ock');
define('SECURE_AUTH_SALT', 'nxwcqvuupegocxryjqjbh0k5pprwubpfupdvwepqczvcjr8yqtogevs8yzcf6rkk');
define('LOGGED_IN_SALT',   'bdfxvbtljkunuqx5juheitnhzhpd5vxijfvlr1lu96b5g3unsupz09dq8v6kzhaz');
define('NONCE_SALT',       '43xftztioae4qbrjdmavorwaktkwmvxmz57llcgvhei5vc7pakqovybi4rvvbiqj');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'cdt_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
